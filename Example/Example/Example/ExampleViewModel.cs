﻿using System.Threading.Tasks;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;

namespace Example
{
	public class ExampleViewModel : ViewModelBase
	{
		[Setting]
		public bool AlphaTSlot
		{
			get { return Get( () => AlphaTSlot, false ); }
			set { Set( () => AlphaTSlot, value ); }
		}

		[Setting]
		public bool AlphaCSlot
		{
			get { return Get( () => AlphaCSlot, false ); }
			set { Set( () => AlphaCSlot, value ); }
		}

		[DependsUpon( nameof( AlphaCSlot ) )]
		[DependsUpon( nameof( AlphaTSlot ) )]
		public void WhenSlotChanges()
		{
			AzureClient.Slot = AlphaCSlot ? AzureClient.SLOT.ALPHAC : AzureClient.SLOT.ALPHAT;
		}

		public bool SignedIn
		{
			get { return Get( () => SignedIn, false ); }
			set { Set( () => SignedIn, value ); }
		}

		[Setting]
		public string CompanyName
		{
			get { return Get( () => CompanyName, "" ); }
			set { Set( () => CompanyName, value ); }
		}


		[Setting]
		public string UserName
		{
			get { return Get( () => UserName, "" ); }
			set { Set( () => UserName, value ); }
		}


		[Setting]
		public string Password
		{
			get { return Get( () => Password, "" ); }
			set { Set( () => Password, value ); }
		}


		public void Execute_SignIn()
		{
			Task.Run( () =>
			          {
				          using( var Client = new AzureClient() )
				          {
					          var Ok = Client.LogIn( CompanyName, UserName, Password );
					          Dispatcher.Invoke( () =>
					                             {
						                             if( Ok != AzureClient.AZURE_CONNECTION_STATUS.OK )
							                             MessageBox.Show( "Unable to sign in", "Error", MessageBoxButton.OK );
						                             else
							                             SignedIn = true;
					                             } );
				          }
			          } );
		}


		public string CustomerCodeList
		{
			get { return Get( () => CustomerCodeList, "" ); }
			set { Set( () => CustomerCodeList, value ); }
		}

		public void Execute_GetAddressesFromList()
		{
			var CodeList = CustomerCodeList.Split( ',' );
			Task.Run( () =>
			          {
				          using( var Client = new AzureClient() )
				          {
							  var Addresses = Client.RequestGetCustomerCompanyAddressList( new CompanyByAccountAndCompanyNameList() )
				          }
			          } );
		}
	}
}