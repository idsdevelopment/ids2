﻿#if AZURE || ROUTE_CONSOLIDATION
using System.Net.Http;
using Address = RemoteService.Model.Address;
using DateTime = System.DateTime;
using Role = RemoteService.Model.Role;
using Zone = RemoteService.Model.Zone;
using Roles = RemoteService.Constants.Roles;
using Trip = RemoteService.Model.Trip;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RemoteService.Constants;
using RemoteService.Logging;
using RemoteService.Model;
using RemoteService.Model.Routing;
#if !ANDROID
using Protocol.Data;
using Preferences = Protocol.Data.Preferences;
using Charge = RemoteService.Model.Charge;

#else
using DateTime = System.DateTime;

#endif

namespace RemoteService
{
    public partial class RemmoteService : LogFile,
#if AZURE || ROUTE_CONSOLIDATION
        IAzRemoteService
#else
        IRemoteService
#endif
    {
#if DEBUG
        private const int DEFAULT_TIMEOUT = 30 * 60; // In seconds
#else
        private const int DEFAULT_TIMEOUT = 30 * 60;
#endif
        private readonly IRemoteService Service;
#if AZURE || ROUTE_CONSOLIDATION
        private readonly IAzRemoteService AzService;
#endif

        public RemmoteService()
        {
            LogOut();
        }

        public RemmoteService( IRemoteService service ) : this()
        {
            Service = service;
        }

#if AZURE || ROUTE_CONSOLIDATION

        public RemmoteService( IAzRemoteService service ) : this()
        {
            AzService = service;
            Service = service;
        }

        public RemmoteService( IRemoteService service, IAzRemoteService azRemoteService ) : this()
        {
            AzService = azRemoteService;
            Service = service;
        }

        private bool Stopping;

        ~RemmoteService()
        {
            Stopping = true;
        }

#endif

        public static List<string> ToStringList( IEnumerable<IRemoteObject> objects )
        {
            var Result = new List<string>();

            if( objects != null )
                Result.AddRange( objects.Select( obj => obj.Name ) );

            return Result;
        }

        public static int OperationTimeoutInSeconds { get; set; } = DEFAULT_TIMEOUT;
        public static int TripMonthsBack { get; set; } = 18; // Go back 18 months

        private uint _AveragePing;

        public uint AveragePing
        {
            get
            {
                lock( this )
                    return _AveragePing;
            }

            set
            {
                lock( this )
                    _AveragePing = value;
            }
        }

        // Interface 
        public void SignOut()
        {
            Service.SignOut();
        }

        public void ErrorLog( string errorMessage )
        {
            Service.ErrorLog( errorMessage );
        }

        public void AuditTrailNewOperation( string program, string text, string comment )
        {
            Service.AuditTrailNewOperation( program, text, comment );
        }

        public void AuditTrailModifyOperation( string program, string text, string comment )
        {
            Service.AuditTrailModifyOperation( program, text, comment );
        }

        public void AuditTrailDeleteOperation( string program, string text, string comment )
        {
            Service.AuditTrailDeleteOperation( program, text, comment );
        }

        public void AuditTrailInformOperation( string program, string text, string comment )
        {
            Service.AuditTrailInformOperation( program, text, comment );
        }

        public string AuthTokenAsString()
        {
            return Service.AuthTokenAsString();
        }

        private readonly object PingLock = new object();

        public bool Ping()
        {
            lock( PingLock )
            {
                var StartTime = DateTime.Now;
                try
                {
                    return Service.Ping();
                }
                finally
                {
                    var EndTime = DateTime.Now;

                    var Diff = EndTime - StartTime;


                    var Ms = (uint)Diff.TotalMilliseconds;

                    lock( this )
                        AveragePing = ( AveragePing + Ms ) / 2;
                }
            }
        }
#if !ANDROID // Users
        (User User, bool InError, bool IsTimeError) IAzRemoteService.LogIn( string carrierId, string userName, string password )
        {
            return ( this as IAzRemoteService ).LogIn( carrierId, userName, password );
        }
#endif
        public List<User> GetStaff( User user, bool getAll = false )
        {
            return Service.GetStaff( user, getAll );
        }

        public List<User> GetStaff( bool getAll )
        {
            return Service.GetStaff( CurrentUser, getAll );
        }

        public bool UpdateStaff( User user )
        {
            return Service.UpdateStaff( user );
        }

        public User GetUsersById( object primaryId, object secondaryId )
        {
            return Service.GetUsersById( primaryId, secondaryId );
        }

        public User GetUsersDetails( User user )
        {
            return user.HaveDetails ? user : Service.GetUsersDetails( user );
        }

        public Address GetAddressById( object primaryId, object addressId )
        {
            return Service.GetAddressById( primaryId, addressId );
        }

        public Address GetAddressForAccount( User user, string accountId )
        {
            return Service.GetAddressForAccount( user, accountId );
        }

        public Address GetAddressForAccount( string accountId )
        {
            return Service.GetAddressForAccount( CurrentUser, accountId );
        }

        public Address GetAddressById( object addressId )
        {
            return Service.GetAddressById( 0, addressId );
        }

        public List<string> GetUsersAsStringList( User user )
        {
            return ToStringList( GetStaff( user ) );
        }

        // Addresses

        // Roles
        public List<Role> GetRoles( User user )
        {
            return Service.GetRoles( user );
        }

        public List<string> GetRolesAsStringList( User user )
        {
            return ToStringList( GetRoles( user ) );
        }

        public static List<Roles.ROLE_TYPE> GetRolesAsType( List<string> roles )
        {
            return roles.Select( Roles.ToRoleType ).ToList();
        }

        public List<Roles.ROLE_TYPE> GetRolesAtType( User user )
        {
            return GetRolesAsType( GetRolesAsStringList( user ) );
        }

        // Zones
        public List<Zone> GetAllZones( User user )
        {
            return Service.GetAllZones( user );
        }

        public List<ZoneMember> GetZonesMembers( User user )
        {
            return Service.GetZonesMembers( user );
        }

        public List<ZoneMember> GetZonesMembers()
        {
            return Service.GetZonesMembers( CurrentUser );
        }

        public List<Zone> GetZones()
        {
            return Service.GetAllZones( CurrentUser );
        }

        public List<string> AGetZonesAsStringList( User user )
        {
            return ToStringList( GetAllZones( user ) );
        }

        public List<Charge> GetCharges( User user )
        {
            return Service.GetCharges( user );
        }

        public List<Charge> GetCharges( User user, string tripId )
        {
            return Service.GetCharges( user, tripId );
        }

        public void UpdateCharges( User user, string tripId, List<Charge> charges )
        {
            Service.UpdateCharges( user, tripId, charges );
        }

        public void UpdateCharges( string tripId, List<Charge> charges )
        {
            Service.UpdateCharges( CurrentUser, tripId, charges );
        }

        public List<Charge> GetCharges( string tripId )
        {
            return Service.GetCharges( CurrentUser, tripId );
        }

        public List<Charge> GetCharges()
        {
            return Service.GetCharges( CurrentUser );
        }

        // Drivers
        public List<Driver> GetDrivers( User user )
        {
            return Service.GetDrivers( user );
        }

        public List<Driver> GetDrivers()
        {
            return Service.GetDrivers( CurrentUser );
        }

        public bool UpdateDriverZone( User user, string newCurrentZone )
        {
            return Service.UpdateDriverZone( user, newCurrentZone );
        }

        public List<string> GetDriversAsStringList( User user )
        {
            return ToStringList( GetDrivers( user ) );
        }

        public List<Trip> GetTripsForDispatch( User user )
        {
            return Service.GetTripsForDispatch( user );
        }

        public void DispatchTrip( Driver driver, Trip trip )
        {
            Service.DispatchTrip( driver, trip );
        }

        public List<Trip> GetTripsForDriverDetailed( User user, List<string> tripIds )
        {
            return Service.GetTripsForDriverDetailed( user, tripIds );
        }

        public List<Trip> GetTripsForDriverDetailed( List<string> tripIds )
        {
            return Service.GetTripsForDriverDetailed( CurrentUser, tripIds );
        }

        public List<TripSummary> GetTripsForDriverSummary( User user )
        {
            return Service.GetTripsForDriverSummary( user );
        }

        public List<Trip> GetTripsByDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByDateAndStatus( user, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByDeliveryDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByDeliveryDateAndStatus( user, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByAccountDeliveryDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByAccountDeliveryDateAndStatus( user, account, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByAccountDeliveryDateAndStatus( string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByAccountDeliveryDateAndStatus( CurrentUser, account, fromDate, toDate, fromStatus, toStatus );
        }

        public void RecalculateTrips( User user, string[] tripIds )
        {
            Service.RecalculateTrips( user, tripIds );
        }

        public void DeleteOriginalTrip( User user, Trip trip )
        {
            Service.DeleteOriginalTrip( user, trip );
        }

        public void DeleteOriginalTrip( Trip trip )
        {
            Service.DeleteOriginalTrip( CurrentUser, trip );
        }

        public void RecalculateTrips( string[] tripIds )
        {
            Service.RecalculateTrips( CurrentUser, tripIds );
        }

        public List<Trip> GetTripsByDeliveryDateAndStatus( DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByDeliveryDateAndStatus( CurrentUser, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByDateAndStatus( DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByDateAndStatus( CurrentUser, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByAccountDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByAccountDateAndStatus( user, account, fromDate, toDate, fromStatus, toStatus );
        }

        public List<Trip> GetTripsByAccountDateAndStatus( string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            return Service.GetTripsByAccountDateAndStatus( CurrentUser, account, fromDate, toDate, fromStatus, toStatus );
        }

        public void GetTripsForDriverDetailedAsync( User user, List<string> tripIds, Action<List<Trip>> completedCallbackAction )
        {
            Task.Run( () => { completedCallbackAction( Service.GetTripsForDriverDetailed( user, tripIds ) ); } );
        }

        public void GetTripsForDriverSummaryAsync( User user, Action<List<TripSummary>> completedCallbackAction )
        {
            Task.Run( () => { completedCallbackAction( Service.GetTripsForDriverSummary( user ) ); } );
        }

        public void GetTripsForDriverSummary( User user, Action<List<TripSummary>> completedCallbackAction )
        {
            completedCallbackAction( Service.GetTripsForDriverSummary( user ) );
        }

        public bool UpdateTrip( User user, Trip trip )
        {
            return Service.UpdateTrip( user, trip );
        }

        public List<Trip> UpdateTrips( User user, List<Trip> trips )
        {
            return Service.UpdateTrips( user, trips );
        }

        public bool UpdateConsolidatedTrip( User user, Trip trips )
        {
            return Service.UpdateConsolidatedTrip( user, trips );
        }

        public bool UpdateConsolidatedTrip( Trip trip )
        {
            return Service.UpdateConsolidatedTrip( CurrentUser, trip );
        }

        public List<AuditTrail> GeAuditTrailForTrip( User user, string tripId )
        {
            return Service.GeAuditTrailForTrip( user, tripId );
        }

        public List<AuditTrail> GeAuditTrailForTrip( string tripId )
        {
            return Service.GeAuditTrailForTrip( CurrentUser, tripId );
        }

        public bool UpdateGps( User user, List<Gps> gpsPoints )
        {
            return Service.UpdateGps( user, gpsPoints );
        }

        public void UnPickupable( User user, Trip trip, decimal quantityPickedUp )
        {
            Service.UnPickupable( user, trip, quantityPickedUp );
        }

        public bool UpdateClaim( string authToken, string[] tripIds, string signature, int signatureHeight, int signatureWidth, string podName, DateTime pickupTime )
        {
            return Service.UpdateClaim( authToken, tripIds, signature, signatureHeight, signatureWidth, podName, pickupTime );
        }

        public Dictionary<string, string> GetDevicePreferences( User user )
        {
            return Service.GetDevicePreferences( user );
        }

        public bool PriorityPickupByBarcode( User user, string barcode, DateTime pickupTime )
        {
            return Service.PriorityPickupByBarcode( user, barcode, pickupTime );
        }

        public List<ServiceLevel> GetServiceLevels( User user )
        {
            return Service.GetServiceLevels( user );
        }

        public List<Trip> GetTripsByLocatoion( User user, string locationCode, bool isPickupBarcode, int statusFilter )
        {
            return Service.GetTripsByLocatoion( user, locationCode, isPickupBarcode, statusFilter );
        }

        public List<Trip> GetTripsByLocatoion( string locationCode, bool isPickupBarcode, int statusFilter )
        {
            return Service.GetTripsByLocatoion( CurrentUser, locationCode, isPickupBarcode, statusFilter );
        }

        public List<ServiceLevel> GetServiceLevels()
        {
            return Service.GetServiceLevels( CurrentUser );
        }

        public bool PriorityPickupByBarcode( string barcode )
        {
            return Service.PriorityPickupByBarcode( CurrentUser, barcode, DateTime.Now );
        }

        public bool UpdateGps( List<Gps> gpsPoints )
        {
            return Service.UpdateGps( CurrentUser, gpsPoints );
        }

        public List<Trip> UpdateTrips( List<Trip> trips )
        {
            LockPoll();
            try
            {
                return Service.UpdateTrips( CurrentUser, trips );
            }
            finally
            {
                ReleasePoll();
            }
        }

        public void UpdateTrip( Trip trip )
        {
            LockPoll();
            try
            {
                Service.UpdateTrip( CurrentUser, trip );
            }
            finally
            {
                ReleasePoll();
            }
        }

        public void DispatchTripsAsync( TripDispatchList dispatchList )
        {
            foreach( var D in dispatchList )
            {
                var Disp = D;
                Task.Run( () => { Service.DispatchTrip( Disp.Driver, Disp.Trip ); } );
            }
        }

        public delegate bool DriverTripsChangeDelegate( List<TripSummary> trips );

        private bool InNotifyDriverPoll;
        private readonly object LockPollLockObject = new object();

        private Timer NewTripPollTimer;

        private void LockPoll()
        {
            while( true )
            {
                lock( LockPollLockObject )
                {
                    if( !InNotifyDriverPoll )
                    {
                        InNotifyDriverPoll = true;
                        return;
                    }
                }
                Thread.Sleep( 100 );
            }
        }

        private void ReleasePoll()
        {
            lock( LockPollLockObject )
                InNotifyDriverPoll = false;
        }

        public void NotifyTripsForDriverChange( User user, int intervalInSecontds, DriverTripsChangeDelegate notifyCallback )
        {
            lock( LockObject )
            {
                NewTripPollTimer?.Dispose();
                NewTripPollTimer = null;

                if( notifyCallback != null )
                {
                    NewTripPollTimer = new Timer( timer =>
                                                  {
                                                      if( !InNotifyDriverPoll )
                                                      {
                                                          InNotifyDriverPoll = true;
                                                          try
                                                          {
                                                              GetTripsForDriverSummary( user, trips =>
                                                              {
                                                                  try
                                                                  {
                                                                      if( trips != null ) // No Coms Error
                                                                      {
                                                                          if( !notifyCallback( trips ) )
                                                                          {
                                                                              NewTripPollTimer?.Dispose();
                                                                              NewTripPollTimer = null;
                                                                          }
                                                                      }
                                                                  }
                                                                  finally
                                                                  {
                                                                      InNotifyDriverPoll = false;
                                                                  }
                                                              } );
                                                          }
                                                          catch
                                                          {
                                                              InNotifyDriverPoll = false;
                                                          }
                                                      }
                                                  }, null, 100, intervalInSecontds * 1000 ); // Give time for android to start
                }
            }
        }

        public void NotifyTripsForDriverChange( int intervalInSecontds, DriverTripsChangeDelegate notifyCallback )
        {
            NotifyTripsForDriverChange( CurrentUser, intervalInSecontds, notifyCallback );
        }

        public List<Address> GetAddresses( User user, string companyId )
        {
            return Service.GetAddresses( user, companyId );
        }

        public List<Address> GetAllAddresses( string companyId )
        {
            return Service.GetAddresses( CurrentUser, companyId );
        }

        public List<Address> GetAddressesCloseTo( User user, string companyId, string searchPattern )
        {
            return Service.GetAddressesCloseTo( user, companyId, searchPattern );
        }

        public List<Address> GetAddressesCloseTo( string companyId, string searchPattern )
        {
            return Service.GetAddressesCloseTo( CurrentUser, companyId, searchPattern );
        }

        public void AddAddress( User user, string accountId, Address address )
        {
            Service.AddAddress( user, accountId, address );
        }

        public void AddAddress( string accountId, Address address )
        {
            Service.AddAddress( CurrentUser, accountId, address );
        }

        public void UpdateAddress( User user, string accountId, Address address )
        {
            Service.UpdateAddress( user, accountId, address );
        }

        public void UpdateAddress( string accountId, Address address )
        {
            Service.UpdateAddress( CurrentUser, accountId, address );
        }

        public void RemoveAddress( User user, string accountId, Address address )
        {
            Service.RemoveAddress( user, accountId, address );
        }

        public void RemoveAddress( string accountId, Address address )
        {
            Service.RemoveAddress( CurrentUser, accountId, address );
        }

        public List<Account> GetAllAccounts( User user )
        {
            return Service.GetAllAccounts( user );
        }

        public List<Account> GetAllAccounts()
        {
            return Service.GetAllAccounts( CurrentUser );
        }

        public Model.Routing.RouteSchedule GetRouteSchedule( User user, string companyId, string route )
        {
            return Service.GetRouteSchedule( user, companyId, route );
        }

        public Model.Routing.RouteSchedule GetRouteSchedule( string companyId, string route )
        {
            return Service.GetRouteSchedule( CurrentUser, companyId, route );
        }

        public List<AddressGroup> GetAddressGroups( User user, string companyId )
        {
            return Service.GetAddressGroups( user, companyId );
        }

        public void SetDebugServer( bool debugServer )
        {
            Service.SetDebugServer( debugServer );
        }

        public void AddAddressToGroup( User user, string companyId, string groupName, List<string> addressIds )
        {
            Service.AddAddressToGroup( user, companyId, groupName, addressIds );
        }

        public void AddAddressToGroup( string companyId, string groupName, List<string> addressIds )
        {
            Service.AddAddressToGroup( CurrentUser, companyId, groupName, addressIds );
        }

        public void RemoveAddressFromGroup( User user, string companyId, string groupName, List<string> addressIds )
        {
            Service.RemoveAddressFromGroup( user, companyId, groupName, addressIds );
        }

        public void RemoveAddressFromGroup( string companyId, string groupName, List<string> addressIds )
        {
            Service.RemoveAddressFromGroup( CurrentUser, companyId, groupName, addressIds );
        }

        public List<AddressGroup> GetAddressGroups( string companyId )
        {
            return Service.GetAddressGroups( CurrentUser, companyId );
        }

        public List<Address> GetAddressInGroup( User user, string companyId, string groupName )
        {
            return Service.GetAddressInGroup( user, companyId, groupName );
        }

        public List<Address> GetAddressInGroup( string companyId, string groupName )
        {
            return Service.GetAddressInGroup( CurrentUser, companyId, groupName );
        }

        public Account GetAccount( User user, string accountId )
        {
            return Service.GetAccount( user, accountId );
        }

        public Account GetAccount( string accountId )
        {
            return Service.GetAccount( CurrentUser, accountId );
        }

#if AZURE || ROUTE_CONSOLIDATION
        public bool IsIds => ( AzService != null ) && AzService.IsIds;
        public bool IsNewCarrier => ( AzService != null ) && AzService.IsNewCarrier;

        public bool CreateCarrier()
        {
            return AzService.CreateCarrier();
        }

        public bool StaffMemberExists( string code )
        {
            return AzService.StaffMemberExists( code );
        }

        public bool AddStaffMember( AddStaffMember staffMember )
        {
            return AzService.AddStaffMember( staffMember );
        }

        public StaffList GetStaff()
        {
            return AzService.GetStaff();
        }

        public PackageTypeList GetPackageTypes()
        {
            return AzService.GetPackageTypes();
        }

        public void NotifyTripsChanged( int board, Action<List<Protocol.Data.Trip>> callback )
        {
            Task.Run( () =>
            {
                while( !Stopping )
                {
                    try
                    {
                        AzService.NotifyTripsChanged( board, list =>
                        {
                            if( !Stopping )
                                callback?.Invoke( list );
                        } );
                    }
                    catch( HttpRequestException )
                    {
                    }
                    catch
                    {
                        break;
                    }
                }
            } );
        }

        public Preferences GetUserPreferences()
        {
            return AzService.GetUserPreferences();
        }

        public void UpdatePreference( Preference preference )
        {
            AzService.UpdatePreference( preference );
        }

        public Distances GetDistances( RouteOptimisationCompanyAddresses addresses )
        {
            return AzService.GetDistances( addresses );
        }

        public Protocol.Data.Roles GetRoles()
        {
            return AzService.GetRoles();
        }

        public void AddStaffMemberWithRolesAndZones( AddStaffMemberWithRolesAndZones staffMemberWithRolesAndZones )
        {
            AzService.AddStaffMemberWithRolesAndZones( staffMemberWithRolesAndZones );
        }

        public void AddUpdateZones( Zones zones )
        {
            AzService.AddUpdateZones( zones );
        }

        public string GenerateCustomerCode( string firstName, string lastName, string addressLine1 )
        {
            return AzService.GenerateCustomerCode( firstName, lastName, addressLine1 );
        }

        Zones IAzRemoteService.GetZones()
        {
            return AzService.GetZones();
        }

#endif
    }
}