using System;

namespace RemoteService.Utils
{
    public static class TimeAndDate
    {
        private static readonly DateTime Epoch;

        public static long UnixTicksSeconds( this DateTime time )
        {
            return (long)( time - Epoch ).TotalSeconds;
        }

        public static long UnixTicksMilliSeconds( this DateTime time )
        {
            return (long)( time - Epoch ).TotalMilliseconds;
        }

        public static DateTime NoSeconds( this DateTime time )
        {
            return time.AddSeconds( -time.Second ).AddMilliseconds( -time.Millisecond );
        }

        public static TimeSpan TimeOfDayNoSeconds( this DateTime time )
        {
            return time.NoSeconds().TimeOfDay;
        }

        static TimeAndDate()
        {
            Epoch = new DateTime( 1970, 1, 1 );
        }
    }
}