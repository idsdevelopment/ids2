﻿namespace RemoteService.Constants
{
    internal static class LogFiles
    {
        internal static int DaysToKeep = 30;

        internal static string
            LogPath = @"Ids Client Log\",
            LogFileMask = "yyyy-MM-dd-HH-mm-ss",
            LogExtension = ".log";
    }
}