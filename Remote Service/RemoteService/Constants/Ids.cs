﻿using System;

namespace RemoteService.Constants
{
    public static class RoleTypeExtension
    {
        public static string AsString( this Roles.ROLE_TYPE role )
        {
            return Roles.RoleXlateTable[ (int)role ];
        }
    }

    public static class StatusTypeExtension
    {
        public static string AsString( this Trips.STATUS status )
        {
            return Trips.XlateStatus( status );
        }
    }

    public static class TimeExtension
    {
        public const string PACIFIC_TIME_ID = "America/Vancouver";

        public static System.DateTime PacificTime( this System.DateTime deviceTime )
        {
            var UtcTime = deviceTime.ToUniversalTime();
            var PacificZone = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_TIME_ID );
            return TimeZoneInfo.ConvertTimeFromUtc( UtcTime, PacificZone );
        }
    }

    public static class Ids
    {
        internal const string IDS_DATA_PATH = @"\Ids\";

        public const string RESELLER_ID = "RESELLER_ID";
    }

    public static class Trips
    {
        public enum STATUS
        {
            UNSET,
            NEW,
            ACTIVE,
            DISPATCHED,
            PICKED_UP,
            DELIVERED,
            VERIFIED,
            POSTED,
            DELETED,
            SCHEDULED,
            UNDELIVERED = -3
        }

        public static string XlateStatus( STATUS status )
        {
            switch( status )
            {
            case STATUS.POSTED:
                return "POSTED";
            case STATUS.DELETED:
                return "DELETED";
            case STATUS.DELIVERED:
                return "DELIVERED";
            case STATUS.DISPATCHED:
                return "DISPATCHED";
            case STATUS.NEW:
                return "NEW";
            case STATUS.ACTIVE:
                return "ACTIVE";
            case STATUS.PICKED_UP:
                return "PICKED_UP";
            case STATUS.SCHEDULED:
                return "SCHEDULED";
            case STATUS.UNDELIVERED:
                return "UNDELIVERED";
            case STATUS.UNSET:
                return "UNSET";
            case STATUS.VERIFIED:
                return "VERIFIED";
            default:
                return "????";
            }
        }
    }

    public static class Roles
    {
        public enum ROLE_TYPE
        {
            NONE,
            SUPER,
            WEB_ADMIN,
            WEB_USER,
            ADMIN,
            BILLING,
            CALLTAKER,
            DISPATCHER,
            DRIVER,
            BASIC_USER,
            ROUTE,
            TRAILER
        }

        public const string SUPER = "super",
                            WEB_ADMIN = "web_admin",
                            WEB_USER = "web_user",
                            ADMIN = "admin",
                            BILLING = "billing",
                            CALLTAKER = "calltaker",
                            DISPATCHER = "dispatcher",
                            DRIVER = "driver",
                            ROUTE = "route",
                            TRAILER = "trailer",
                            BASIC_USER = "basic_user";

        public static string[] RoleXlateTable = { SUPER, WEB_ADMIN, WEB_USER, ADMIN, BILLING, CALLTAKER, DISPATCHER, DRIVER, ROUTE, TRAILER, BASIC_USER };

        public static ROLE_TYPE ToRoleType( string role )
        {
            switch( role.Trim() )
            {
            case SUPER:
                return ROLE_TYPE.SUPER;
            case WEB_ADMIN:
                return ROLE_TYPE.WEB_ADMIN;
            case WEB_USER:
                return ROLE_TYPE.WEB_USER;
            case ADMIN:
                return ROLE_TYPE.ADMIN;
            case BILLING:
                return ROLE_TYPE.BILLING;
            case CALLTAKER:
                return ROLE_TYPE.CALLTAKER;
            case DISPATCHER:
                return ROLE_TYPE.DISPATCHER;
            case DRIVER:
                return ROLE_TYPE.DRIVER;
            case ROUTE:
                return ROLE_TYPE.ROUTE;
            case TRAILER:
                return ROLE_TYPE.TRAILER;
            default: //      "basic_user"
                return ROLE_TYPE.BASIC_USER;
            }
        }
    }

    public class ActivePeriods
    {
        public const byte DEFAULT_POLLING_PERIOD = 15,
                          DEFAULT_START_HOUR = 6,
                          DEFAULT_END_HOUR = 21;
    }

    public static class DateTime
    {
        public const string MONDAY_SHORT = "MON",
                            TUESDAY_SHORT = "TUE",
                            WEDNESDAY_SHORT = "WED",
                            THURSDAY_SHORT = "THU",
                            FRIDAY_SHORT = "FRI",
                            SATURDAY_SHORT = "SAT",
                            SUNDAY_SHORT = "SUN";

        public enum WEEK_DAY
        {
            UNKNOWN,
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY,
            SUNDAY
        }

        public static WEEK_DAY ToWeekDay( System.DateTime time )
        {
            switch( time.DayOfWeek )
            {
            case DayOfWeek.Monday:
                return WEEK_DAY.MONDAY;
            case DayOfWeek.Tuesday:
                return WEEK_DAY.TUESDAY;
            case DayOfWeek.Wednesday:
                return WEEK_DAY.WEDNESDAY;
            case DayOfWeek.Thursday:
                return WEEK_DAY.THURSDAY;
            case DayOfWeek.Friday:
                return WEEK_DAY.FRIDAY;
            case DayOfWeek.Saturday:
                return WEEK_DAY.SATURDAY;
            case DayOfWeek.Sunday:
                return WEEK_DAY.SUNDAY;
            default:
                return WEEK_DAY.UNKNOWN;
            }
        }

        public static WEEK_DAY ToWeekDayFromShortText( string weekDay )
        {
            switch( weekDay.Trim().ToUpper() )
            {
            case MONDAY_SHORT:
                return WEEK_DAY.MONDAY;
            case TUESDAY_SHORT:
                return WEEK_DAY.TUESDAY;
            case WEDNESDAY_SHORT:
                return WEEK_DAY.WEDNESDAY;
            case THURSDAY_SHORT:
                return WEEK_DAY.THURSDAY;
            case FRIDAY_SHORT:
                return WEEK_DAY.FRIDAY;
            case SATURDAY_SHORT:
                return WEEK_DAY.SATURDAY;
            case SUNDAY_SHORT:
                return WEEK_DAY.SUNDAY;
            default:
                return WEEK_DAY.UNKNOWN;
            }
        }
    }
}