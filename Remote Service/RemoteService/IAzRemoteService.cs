﻿#if AZURE || ROUTE_CONSOLIDATION
using System;
using System.Collections.Generic;
using Protocol.Data;
using RemoteService.Constants;
using RemoteService.Model;
using Preferences = Protocol.Data.Preferences;
using Roles = Protocol.Data.Roles;
using Trip = Protocol.Data.Trip;

namespace RemoteService
{
    public interface IAzRemoteService : IRemoteService
    {
        bool IsIds { get; }
        bool IsNewCarrier { get; }
        bool CreateCarrier();
        bool StaffMemberExists( string code );
        bool AddStaffMember( AddStaffMember staffMember );
        StaffList GetStaff();
        PackageTypeList GetPackageTypes();

        void NotifyTripsChanged( int board, Action<List<Trip>> callback );

        Preferences GetUserPreferences();
        void UpdatePreference( Preference preference );

        Distances GetDistances( RouteOptimisationCompanyAddresses addresses );

        Roles GetRoles();
        void AddStaffMemberWithRolesAndZones( AddStaffMemberWithRolesAndZones staffMemberWithRolesAndZones );

        Zones GetZones();
        void AddUpdateZones( Zones zones );

        string GenerateCustomerCode( string firstName, string lastName, string addressLine1 );

        new ( User User, bool InError, bool IsTimeError ) LogIn( string carrierId, string userName, string password );
    }
}

#endif