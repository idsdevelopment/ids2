﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RemoteService.Locking;
using RemoteService.Model;

namespace RemoteService
{
    public partial class RemmoteService
    {
        private volatile LockedObject<User> _CurrentUser;
        private volatile LockedObject<Dictionary<string, string>> _Preferences;

        public void LogOut()
        {
            _CurrentUser = new LockedObject<User>();
        }

        public User CurrentUser
        {
            get => _CurrentUser;
            private set => _CurrentUser = value;
        }

        public Dictionary<string, string> Preferences
        {
            get
            {
                while( _Preferences == null )
                    Task.Delay( 100 ).Wait();

                return _Preferences;
            }
            private set => _Preferences = value;
        }

        public List<Trip> GetTripsForDispatch()
        {
            return GetTripsForDispatch( CurrentUser );
        }

        public bool IsDriver { get; private set; }

        // Interface

        public bool HasTimeError { get; set; }

        public User LogIn( string carrierId, string userName, string password )
        {
            carrierId = carrierId.Trim();
            userName = userName.Trim();

            var User = Service.LogIn( carrierId, userName, password );

            if( User != null )
            {
                CurrentUser = User;
                var Result = User.Enabled;
                if( Result )
                {
                    Task.Run( () => { Preferences = GetDevicePreferences( CurrentUser ); } );
                    return User;
                }
            }
            HasTimeError = Service.HasTimeError;
            return null;
        }

        public User LogInAsDriver( string carrierId, string userName, string password )
        {
            IsDriver = true;
            return LogIn( carrierId, userName, password );
        }
    }
}