﻿namespace RemoteService.Model
{
    public class Driver : User
    {
        public Driver()
        {
        }

        public Driver( User user ) : base( user )
        {
        }

        public override string Name
        {
            get
            {
                var Nme = base.Name.Trim();
                if( string.IsNullOrEmpty( Nme ) )
                    Nme = AccountId;
                return Nme;
            }
        }

        public bool IsEnabled { get; set; }
        public string LoginPeriods { get; set; }
    }
}