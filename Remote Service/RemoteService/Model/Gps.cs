using System;

namespace RemoteService.Model
{
    public class Gps : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }

        public DateTime Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}