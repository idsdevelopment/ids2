﻿using RemoteService.Constants;

namespace RemoteService.Model
{
    public class User : IRemoteObject
    {
        public bool Enabled { get; set; }
        public bool HaveDetails { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }

        // Interface
        public object OriginalData { get; set; }
        public string Id { get; set; }

        public string ResellerId { get; set; }

        public string AccountId { get; set; }

        public string UserId { get; set; }

        public virtual string Name
        {
            get
            {
                var DoFirstName = !string.IsNullOrWhiteSpace( FirstName );
                var DoLastName = !string.IsNullOrWhiteSpace( LastName );

                var Nme = "";

                if( DoFirstName )
                {
                    Nme = FirstName;
                    if( DoLastName )
                        Nme += ' ';
                }

                if( DoLastName )
                    Nme += LastName;

                return Nme;
            }
            set { }
        }

        public Address Address { get; set; }

        public string CurrentRole { get; set; }
        public string CurrentZone { get; set; }

        public byte PollingInterval { get; set; }
        public byte StartHour { get; set; }
        public byte EndHour { get; set; }
        public DateTime.WEEK_DAY[] ActiveDays { get; set; }

        public User()
        {
        }

        public User( User user )
        {
            Enabled = user.Enabled;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Password = user.Password;
            Id = user.Id;
            ResellerId = user.ResellerId;
            AccountId = user.AccountId;
            UserId = user.UserId;
            CurrentZone = user.CurrentZone;
        }
    }
}