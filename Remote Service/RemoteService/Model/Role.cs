﻿namespace RemoteService.Model
{
    public class Role : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }

        public string ResellerId { get; set; }

        public string AccountId { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }
    }
}