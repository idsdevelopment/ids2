﻿namespace RemoteService.Model
{
    public class Address : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; } = "";

        public virtual string FormattedAddress
        {
            get
            {
                var Result = "";

                void Action( string spacer, string str )
                {
                    str = str?.Trim();

                    if( !string.IsNullOrEmpty( str ) )
                    {
                        if( !string.IsNullOrEmpty( Result ) )
                            Result += spacer;

                        Result += str;
                    }
                }

                Action( "", Suite );
                Action( "/", AddressLine1 );
                Action( ", ", AddressLine2 );
                Action( ", ", City );
                Action( ", ", Region );
                Action( ", ", PostalCode );
                Action( ", ", Country );

                return Result;
            }
        }

        public string CompanyName { get; set; } = "";

        public string Suite { get; set; } = "";
        public string AddressLine1 { get; set; } = "";
        public string AddressLine2 { get; set; } = "";
        public string Vicinity { get; set; } = "";
        public string City { get; set; } = "";
        public string Region { get; set; } = "";
        public string PostalCode { get; set; } = "";
        public string Country { get; set; } = "";
        public string CountryCode { get; set; } = "";

        public string Phone { get; set; } = "";
        public string Fax { get; set; } = "";
        public string Mobile { get; set; } = "";
        public string EmailAddress { get; set; } = "";

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }

    public class AddressGroup : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}