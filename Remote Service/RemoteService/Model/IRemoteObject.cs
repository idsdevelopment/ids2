﻿namespace RemoteService.Model
{
    public interface IRemoteObject
    {
        object OriginalData { get; set; }

        string Id { get; set; }

        string ResellerId { get; set; }

        string AccountId { get; set; }

        string UserId { get; set; }

        string Name { get; set; }
    }
}