﻿using System;
using System.Text;

namespace RemoteService.Model.Routing
{
    public class RouteSchedule
    {
        public static string MakeScheduleKey( string groupName )
        {
            return '~' + groupName;
        }

        public string ScheduleName { get; protected set; }
        public string CompanyId { get; protected set; }
        public string BillToAccount { get; set; } = "";

        public bool Static { get; set; }

        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }

        public DateTime TimeWindowStart { get; set; }
        public DateTime TimeWindowEnd { get; set; }

        public decimal Commission { get; set; }
        public uint StopCount { get; set; }

        public bool IsNew { get; protected set; }

        public string AsString => $"Route: {ScheduleName}\r\n" +
                                  $"Company: {CompanyId}\r\n" +
                                  $"Bill to: {BillToAccount}\r\n" +
                                  $"Static: {Static}\r\n\r\n" +
                                  $"Monday: {Monday}\r\n" +
                                  $"Tuesday: {Tuesday}\r\n" +
                                  $"Wednesday: {Wednesday}\r\n" +
                                  $"Thursday: {Thursday}\r\n" +
                                  $"Friday: {Friday}\r\n" +
                                  $"Saturday: {Saturday}\r\n" +
                                  $"Sunday: {Sunday}\r\n\r\n" +
                                  $"Earliest pickup time: {TimeWindowStart:T}\r\n" +
                                  $"Latest pickup time: {TimeWindowEnd:T}\r\n" +
                                  $"Commission: {Commission:C}\r\n" +
                                  $"Stop count: {StopCount}";

        public RouteSchedule()
        {
            CompanyId = "";
            ScheduleName = "";
            TimeWindowStart = TimeWindowEnd = DateTime.Now;
            IsNew = true;
        }

        public RouteSchedule( RouteSchedule data )
        {
            IsNew = data.IsNew;
            ScheduleName = data.ScheduleName;
            CompanyId = data.CompanyId;
            BillToAccount = data.BillToAccount;
            Static = data.Static;
            Monday = data.Monday;
            Tuesday = data.Tuesday;
            Wednesday = data.Wednesday;
            Thursday = data.Thursday;
            Friday = data.Friday;
            Saturday = data.Saturday;
            Sunday = data.Sunday;
            TimeWindowEnd = data.TimeWindowEnd;
            TimeWindowStart = data.TimeWindowStart;
            Commission = data.Commission;
            StopCount = data.StopCount;
        }

        public RouteSchedule( string companyId, string scheduleName )
        {
            CompanyId = companyId;
            BillToAccount = companyId;
            ScheduleName = scheduleName.StartsWith( "~" ) ? scheduleName : MakeScheduleKey( scheduleName );
            IsNew = true;
        }

        public RouteSchedule( string companyId, Address addr )
        {
            CompanyId = companyId;
            ScheduleName = addr.CompanyName;
            BillToAccount = addr.EmailAddress;

            Static = addr.Suite.Trim() == "1";

            var Ndx = 0;
            var Values = addr.AddressLine1.Split( new[] { '^' }, StringSplitOptions.RemoveEmptyEntries );

            string StrDays = "",
                   StrTimes = "",
                   StrCommission = "",
                   StrStopCount = "";

            foreach( var Value in Values )
            {
                switch( Ndx++ )
                {
                case 0:
                    StrDays = Value;
                    break;

                case 1:
                    StrTimes = Value;
                    break;

                case 2:
                    StrCommission = Value;
                    break;

                case 3:
                    StrStopCount = Value;
                    break;
                }
            }

            var Days = StrDays.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );
            Ndx = 0;
            foreach( var Day in Days )
            {
                var Set = Day == "1";

                switch( Ndx++ )
                {
                case 0:
                    Monday = Set;
                    break;
                case 1:
                    Tuesday = Set;
                    break;
                case 2:
                    Wednesday = Set;
                    break;
                case 3:
                    Thursday = Set;
                    break;
                case 4:
                    Friday = Set;
                    break;
                case 5:
                    Saturday = Set;
                    break;
                case 6:
                    Sunday = Set;
                    break;
                }
            }


            var Times = StrTimes.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );

            string StrStartTime = "", StrEndTime = "";

            Ndx = 0;
            foreach( var Time in Times )
            {
                switch( Ndx++ )
                {
                case 0:
                    StrStartTime = Time;
                    break;
                case 1:
                    StrEndTime = Time;
                    break;
                }
            }

            if( !DateTime.TryParse( StrStartTime, out var T ) )
                T = DateTime.Now;

            TimeWindowStart = DateTime.MinValue + T.TimeOfDay;

            if( !DateTime.TryParse( StrEndTime, out T ) )
                T = DateTime.Now;

            TimeWindowEnd = DateTime.MinValue + T.TimeOfDay;

            if( !decimal.TryParse( StrCommission, out var C ) )
                C = 0;
            Commission = C;

            if( !uint.TryParse( StrStopCount, out var Sc ) )
                Sc = 0;
            StopCount = Sc;
        }

        public RouteSchedule( Address addr ) : this( addr.AccountId, addr )
        {
        }

        public string ValuesAsString
        {
            get
            {
                var Result = new StringBuilder();

                for( var Ndx = 0; Ndx < 7; Ndx++ )
                {
                    var Set = false;

                    switch( Ndx )
                    {
                    case 0:
                        Set = Monday;
                        break;
                    case 1:
                        Set = Tuesday;
                        break;
                    case 2:
                        Set = Wednesday;
                        break;
                    case 3:
                        Set = Thursday;
                        break;
                    case 4:
                        Set = Friday;
                        break;
                    case 5:
                        Set = Saturday;
                        break;
                    case 6:
                        Set = Sunday;
                        break;
                    }
                    if( Ndx != 0 )
                        Result.Append( ',' );

                    Result.Append( Set ? '1' : '0' );
                }

                var STime = DateTime.MinValue + TimeWindowStart.TimeOfDay;
                var ETime = DateTime.MinValue + TimeWindowEnd.TimeOfDay;

                return Result.Append( '^' ).Append( STime ).Append( ',' ).Append( ETime )
                             .Append( '^' ).Append( Commission )
                             .Append( '^' ).Append( StopCount ).ToString();
            }
        }
    }
}