namespace RemoteService.Model
{
    public class Claim : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }

        public string[] TripIds { get; set; }
        public string Signature { get; set; }
        public string PodName { get; set; }
    }
}