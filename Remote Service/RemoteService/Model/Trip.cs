﻿using System.Collections.Generic;
using RemoteService.Constants;
using DateTime = System.DateTime;

namespace RemoteService.Model
{
    public class TripDispatcher
    {
        public Trip Trip;
        public Driver Driver;
    }

    public class TripDispatchList : List<TripDispatcher>
    {
    }

    public class TripSummary : IRemoteObject
    {
        public object OriginalData { get; set; }

        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }

        public string TripId
        {
            get => Id;
            set => Id = value;
        }

        public Trips.STATUS Status { get; set; }
        public long LastUpdatedTick { get; set; }
        public string CurrentZone { get; set; }
        public string PickupZone { get; set; }
        public string DeliveryZone { get; set; }

        public DateTime UpdatedTime { get; set; }

        public Trip.COMPARISON Compare( TripSummary compareTo )
        {
            var Retval = Trip.Compare( TripId, compareTo.TripId );
            if( Retval != Trip.COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Trip.Compare( Status, compareTo.Status );
            if( Retval != Trip.COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Trip.Compare( LastUpdatedTick, compareTo.LastUpdatedTick );
            if( Retval != Trip.COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Trip.Compare( CurrentZone, compareTo.CurrentZone );
            if( Retval != Trip.COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Trip.Compare( PickupZone, compareTo.PickupZone );
            return Retval != Trip.COMPARISON.EQUAL_TO ? Retval : Trip.Compare( DeliveryZone, compareTo.DeliveryZone );
        }
    }

    public class Trip : IRemoteObject
    {
        public enum UNDELIVERABLE_MODE
        {
            NONE,
            STATUS_CHANGE,
            NEW_TRIP // Residual to new trip
        }

        public object OriginalData { get; set; }

        public Trip()
        {
        }

        public Trip( Trip t )
        {
            OriginalData = t.OriginalData;

            TripId = t.TripId; // Same as Name
            Id = t.TripId;
            ResellerId = t.ResellerId;
            AccountId = t.AccountId;
            UserId = t.UserId;
            Driver = t.Driver;
            Status = t.Status;
            PriorityStatus = t.PriorityStatus;
            Read = t.Read;

            PickupZone = t.PickupZone;
            PickupCompany = t.PickupCompany;
            PickupAddress = t.PickupAddress;
            PickupContact = t.PickupContact;
            PickupNotes = t.PickupNotes;

            DeliveryCompany = t.DeliveryCompany;
            DeliveryAddress = t.DeliveryAddress;
            DeliveryContact = t.DeliveryContact;
            DeliveryZone = t.DeliveryZone;
            DeliveryNotes = t.DeliveryNotes;

            CurrentZone = t.CurrentZone;
            ServiceLevel = t.ServiceLevel;
            PackageType = t.PackageType;
            ReadyTime = t.ReadyTime;
            DueTime = t.DueTime;
            Pieces = t.Pieces;
            Weight = t.Weight;
            PickupNotes = t.PickupNotes;
            DeliveryNotes = t.DeliveryNotes;
            Board = t.Board;
            Pallets = t.Pallets;
            DangerousGoods = t.DangerousGoods;
            Reference = t.Reference;
            PodName = t.PodName;
            Signature = t.Signature;

            DeliveryTime = t.DeliveryTime;
            DeliveryArrivalTime = t.DeliveryArrivalTime;
            PickupTime = t.PickupTime;
            PickupArrivalTime = t.PickupArrivalTime;

            ProcessedByIdsRouteTime = t.ProcessedByIdsRouteTime;
            Route = t.Route;

            TotalAmount = t.TotalAmount;
            TotalTax = t.TotalTax;
            MiscellaneousCharges = t.MiscellaneousCharges;

            InDispute = t.InDispute;
            BillingAccount = t.BillingAccount;
            Invoiced = t.Invoiced;
        }

        public string TripId
        {
            get => Name;
            set => Name = value;
        }

        public enum COMPARISON
        {
            LESS_THAN = -1,
            EQUAL_TO,
            GREATER_THAN,
            NOT_EQUAL
        }

        public static COMPARISON Compare( string a, string b )
        {
            var Cmp = string.CompareOrdinal( a, b );
            if( Cmp < 0 )
                return COMPARISON.LESS_THAN;

            return Cmp > 0 ? COMPARISON.GREATER_THAN : COMPARISON.EQUAL_TO;
        }

        public static COMPARISON Compare( decimal a, decimal b )
        {
            if( a < b )
                return COMPARISON.LESS_THAN;

            return a > b ? COMPARISON.GREATER_THAN : COMPARISON.EQUAL_TO;
        }

        public static COMPARISON Compare( long a, long b )
        {
            if( a < b )
                return COMPARISON.LESS_THAN;

            return a > b ? COMPARISON.GREATER_THAN : COMPARISON.EQUAL_TO;
        }

        public static COMPARISON Compare( Trips.STATUS sA, Trips.STATUS sB )
        {
            var A = (byte)sA;
            var B = (byte)sB;

            if( A < B )
                return COMPARISON.LESS_THAN;

            return A > B ? COMPARISON.GREATER_THAN : COMPARISON.EQUAL_TO;
        }

        public COMPARISON Compare( Trip compareTo )
        {
            var Retval = Compare( TripId, compareTo.TripId );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( ServiceLevel, compareTo.ServiceLevel );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( PackageType, compareTo.PackageType );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( Weight, compareTo.Weight );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( Pieces, compareTo.Pieces );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( DeliveryCompany, compareTo.DeliveryCompany );
            if( Retval != COMPARISON.EQUAL_TO )
                return Retval;

            Retval = Compare( PickupCompany, compareTo.PickupCompany );

            return Retval != COMPARISON.EQUAL_TO ? Retval : COMPARISON.EQUAL_TO;
        }

        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Driver { get; set; }
        public string Name { get; set; }

        public Trips.STATUS Status { get; set; }
        public Trips.STATUS PriorityStatus { get; set; }

        public bool Received { get; set; }
        public bool Read { get; set; }

        public string PickupZone { get; set; }
        public string PickupCompany { get; set; }
        public Address PickupAddress { get; set; }
        public string PickupContact { get; set; }
        public string PickupPhoneNumber { get; set; }
        public string PickupNotes { get; set; }

        public string DeliveryZone { get; set; }
        public string DeliveryCompany { get; set; }
        public Address DeliveryAddress { get; set; }
        public string DeliveryContact { get; set; }
        public string DeliveryPhoneNumber { get; set; }
        public string DeliveryNotes { get; set; }

        public string CurrentZone { get; set; }

        public string ServiceLevel { get; set; }
        public string PackageType { get; set; }

        public DateTime ReadyTime { get; set; }
        public DateTime DueTime { get; set; }
        public DateTime UpdateTime { get; set; }

        public decimal Pieces { get; set; }
        public decimal Weight { get; set; }

        public int Board { get; set; }
        public string Pallets { get; set; }
        public bool DangerousGoods { get; set; }
        public string Reference { get; set; }
        public string PodName { get; set; }

        public string Signature { get; set; } = "";
        public string BillingNote { get; set; } = "";
        public UNDELIVERABLE_MODE UndeliverableMode { get; set; } = UNDELIVERABLE_MODE.NONE;
        public string UndeliverableReason { get; set; }

        public bool RemoteServerWasUpdated { get; set; }

        public DateTime PickupTime { get; set; }
        public DateTime PickupArrivalTime { get; set; }

        public DateTime DeliveryTime { get; set; }
        public DateTime DeliveryArrivalTime { get; set; }

        public DateTime ProcessedByIdsRouteTime { get; set; }

        public string Route { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal MiscellaneousCharges { get; set; }

        public bool InDispute { get; set; }

        public bool Invoiced { get; set; }

        public string BillingAccount { get; set; }
    }
}