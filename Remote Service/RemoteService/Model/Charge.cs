﻿namespace RemoteService.Model
{
    public class Charge : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }

        private decimal _Value;

        public decimal Value
        {
            get => _Value;
            set
            {
                _Value = value;
                IsStringValue = false;
            }
        }

        private string _StringValue;

        public string StringValue
        {
            get => _StringValue;
            set
            {
                _StringValue = value;
                IsStringValue = true;
            }
        }

        public bool ShowToDriver { get; set; }
        public bool IsStringValue { get; private set; }
    }
}