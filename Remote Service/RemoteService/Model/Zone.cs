﻿namespace RemoteService.Model
{
    public class ZoneMember : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }

    public class Zone : ZoneMember
    {
        public int SortIndex { get; set; }
    }
}