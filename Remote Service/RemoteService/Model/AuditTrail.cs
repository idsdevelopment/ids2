﻿using System;

namespace RemoteService.Model
{
    public class AuditTrail : IRemoteObject
    {
        public object OriginalData { get; set; }
        public string Id { get; set; }
        public string ResellerId { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string Change { get; set; }
        public string Action { get; set; }
    }
}