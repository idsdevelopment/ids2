﻿using System.Collections.Generic;
using RemoteService.Constants;
using RemoteService.Model;
using RemoteService.Model.Routing;
using DateTime = System.DateTime;

namespace RemoteService
{
    public interface IRemoteService
    {
        /// <summary>
        ///     Azure entry points
        /// </summary>
        void SignOut();

        void ErrorLog( string errorMessage );

        void AuditTrailNewOperation( string program, string text, string comment );
        void AuditTrailModifyOperation( string program, string text, string comment );
        void AuditTrailDeleteOperation( string program, string text, string comment );
        void AuditTrailInformOperation( string program, string text, string comment );

        /// <summary>
        ///     Ids entry points
        /// </summary>
        /// <returns></returns>
        string AuthTokenAsString();

        bool Ping();

        void SetDebugServer( bool debugServer );

        bool HasTimeError { set; get; }
        User LogIn( string carrierId, string userName, string password );

        List<User> GetStaff( User user, bool getAll = false );
        bool UpdateStaff( User user );

        User GetUsersById( object primaryId, object secondaryId );
        User GetUsersDetails( User user );

        void AddAddressToGroup( User user, string companyId, string groupName, List<string> addressIds );
        void RemoveAddressFromGroup( User user, string companyId, string groupName, List<string> addressIds );
        RouteSchedule GetRouteSchedule( User user, string companyId, string route );

        List<AddressGroup> GetAddressGroups( User user, string companyId );
        List<Address> GetAddressInGroup( User user, string companyId, string groupName );

        List<Account> GetAllAccounts( User user );
        Account GetAccount( User user, string accountId );

        Address GetAddressById( object primaryId, object addressId );
        Address GetAddressForAccount( User user, string accountId );
        List<Address> GetAddressesCloseTo( User user, string companyId, string searchPattern );
        List<Address> GetAddresses( User user, string companyId );
        void AddAddress( User user, string accountId, Address address );
        void UpdateAddress( User user, string accountId, Address address );
        void RemoveAddress( User user, string accountId, Address address );

        List<Role> GetRoles( User user );

        List<Zone> GetAllZones( User user );
        List<ZoneMember> GetZonesMembers( User user );

        List<Charge> GetCharges( User user );
        List<Charge> GetCharges( User user, string tripId );
        void UpdateCharges( User user, string tripId, List<Charge> charges );

        List<Driver> GetDrivers( User user );
        bool UpdateDriverZone( User user, string newCurrentZone );

        List<Trip> GetTripsForDispatch( User user );
        void DispatchTrip( Driver driver, Trip trip );

        List<Trip> GetTripsForDriverDetailed( User user, List<string> tripIds );
        List<TripSummary> GetTripsForDriverSummary( User user );
        List<Trip> GetTripsByDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus );
        List<Trip> GetTripsByAccountDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus );
        List<Trip> GetTripsByDeliveryDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus );
        List<Trip> GetTripsByAccountDeliveryDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus );

        void RecalculateTrips( User user, string[] tripIds );

        void DeleteOriginalTrip( User user, Trip trip );
        bool UpdateTrip( User user, Trip trip );
        List<Trip> UpdateTrips( User user, List<Trip> trips );
        bool UpdateConsolidatedTrip( User user, Trip trips );

        List<AuditTrail> GeAuditTrailForTrip( User user, string tripId );

        bool UpdateGps( User user, List<Gps> gpsPoints );

        void UnPickupable( User user, Trip trip, decimal quantityPickedUp );

        bool UpdateClaim( string authToken, string[] tripIds, string signature, int signatureHeight, int signatureWidth, string podName, DateTime pickupTime );

        Dictionary<string, string> GetDevicePreferences( User user );

        bool PriorityPickupByBarcode( User user, string barcode, DateTime pickupTime );

        List<ServiceLevel> GetServiceLevels( User user );

        List<Trip> GetTripsByLocatoion( User user, string locationCode, bool isPickupBarcode, int statusFilter );
    }
}