﻿using System;
using Utils;

namespace RemoteService.Logging
{
    public class LogFile : ADisposable
    {
        protected object LockObject = new object();

        public static Action<string> LogCallback;

        public LogFile()
        {
        }

        public LogFile( string message ) : this()
        {
            Log( message );
        }

        public LogFile( Exception e ) : this()
        {
            Log( e );
        }

        public void Log( string message )
        {
            LogCallback?.Invoke( message );
        }

        public void Log( Exception e )
        {
            Log( "Client Exception: " + e.Message );
            if( e.InnerException != null )
                Log( "Client Inner Exception: " + e.InnerException.Message );

            Log( "Client Exception String: " + e );
        }

        protected override void OnDispose( bool systemDisposing )
        {
        }
    }
}