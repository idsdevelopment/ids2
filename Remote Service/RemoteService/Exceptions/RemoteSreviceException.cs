﻿using System;
using RemoteService.Logging;

namespace RemoteService.Exceptions
{
    internal class RemoteSreviceException : Exception
    {
        internal RemoteSreviceException( string message ) : base( message )
        {
            new LogFile().Log( message );
        }

        internal RemoteSreviceException( Exception e ) : base( e.Message )
        {
            new LogFile().Log( e );
        }
    }

    internal class NoTokenException : RemoteSreviceException
    {
        internal NoTokenException() : base( "Missing Authorisation Token" )
        {
        }
    }
}