﻿using System;

namespace RemoteService.Locking
{
    public class LockedObject<T>
    {
        private readonly object LockObject = new object();
        private T _Object;

        public T Value
        {
            get
            {
                lock( LockObject )
                    return _Object;
            }

            set
            {
                lock( LockObject )
                    _Object = value;
            }
        }

        public void SetValueAsync( Func<T> func )
        {
            lock( LockObject )
                _Object = func();
        }

        public bool IsNull
        {
            get
            {
                lock( LockObject )
                    return _Object == null;
            }
        }

        public static implicit operator T( LockedObject<T> value )
        {
            return value.Value;
        }

        public static implicit operator LockedObject<T>( T value )
        {
            return new LockedObject<T> { Value = value };
        }
    }
}