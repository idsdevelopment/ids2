﻿using System;
using System.Collections.Generic;
using RemoteService.Constants;
using RemoteService.Model;
using RemoteService.Model.Routing;
using DateTime = System.DateTime;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public string AuthTokenAsString()
        {
            throw new NotImplementedException();
        }

        public List<User> GetStaff( User user, bool getAll = false )
        {
            throw new NotImplementedException();
        }

        public bool UpdateStaff( User user )
        {
            throw new NotImplementedException();
        }

        public User GetUsersById( object primaryId, object secondaryId )
        {
            throw new NotImplementedException();
        }

        public User GetUsersDetails( User user )
        {
            throw new NotImplementedException();
        }

        public void AddAddressToGroup( User user, string companyId, string groupName, List<string> addressIds )
        {
            throw new NotImplementedException();
        }

        public void RemoveAddressFromGroup( User user, string companyId, string groupName, List<string> addressIds )
        {
            throw new NotImplementedException();
        }

        public RouteSchedule GetRouteSchedule( User user, string companyId, string route )
        {
            throw new NotImplementedException();
        }

        public List<AddressGroup> GetAddressGroups( User user, string companyId )
        {
            throw new NotImplementedException();
        }

        public List<Address> GetAddressInGroup( User user, string companyId, string groupName )
        {
            throw new NotImplementedException();
        }

        public List<Account> GetAllAccounts( User user )
        {
            throw new NotImplementedException();
        }

        public Account GetAccount( User user, string accountId )
        {
            throw new NotImplementedException();
        }

        public Address GetAddressById( object primaryId, object addressId )
        {
            throw new NotImplementedException();
        }

        public Address GetAddressForAccount( User user, string accountId )
        {
            throw new NotImplementedException();
        }

        public List<Address> GetAddressesCloseTo( User user, string companyId, string searchPattern )
        {
            throw new NotImplementedException();
        }

        public List<Address> GetAddresses( User user, string companyId )
        {
            throw new NotImplementedException();
        }

        public void AddAddress( User user, string accountId, Address address )
        {
            throw new NotImplementedException();
        }

        public void UpdateAddress( User user, string accountId, Address address )
        {
            throw new NotImplementedException();
        }

        public void RemoveAddress( User user, string accountId, Address address )
        {
            throw new NotImplementedException();
        }

        public List<Role> GetRoles( User user )
        {
            throw new NotImplementedException();
        }

        public List<Zone> GetAllZones( User user )
        {
            throw new NotImplementedException();
        }

        public List<ZoneMember> GetZonesMembers( User user )
        {
            throw new NotImplementedException();
        }

        public List<Charge> GetCharges( User user )
        {
            throw new NotImplementedException();
        }

        public List<Charge> GetCharges( User user, string tripId )
        {
            throw new NotImplementedException();
        }

        public void UpdateCharges( User user, string tripId, List<Charge> charges )
        {
            throw new NotImplementedException();
        }

        public List<Driver> GetDrivers( User user )
        {
            throw new NotImplementedException();
        }

        public bool UpdateDriverZone( User user, string newCurrentZone )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsForDispatch( User user )
        {
            throw new NotImplementedException();
        }

        public void DispatchTrip( Driver driver, Trip trip )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsForDriverDetailed( User user, List<string> tripIds )
        {
            throw new NotImplementedException();
        }

        public List<TripSummary> GetTripsForDriverSummary( User user )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsByDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsByAccountDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsByDeliveryDateAndStatus( User user, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsByAccountDeliveryDateAndStatus( User user, string account, DateTime fromDate, DateTime toDate, Trips.STATUS fromStatus, Trips.STATUS toStatus )
        {
            throw new NotImplementedException();
        }

        public void RecalculateTrips( User user, string[] tripIds )
        {
            throw new NotImplementedException();
        }

        public void DeleteOriginalTrip( User user, Trip trip )
        {
            throw new NotImplementedException();
        }

        public bool UpdateTrip( User user, Trip trip )
        {
            throw new NotImplementedException();
        }

        public List<Trip> UpdateTrips( User user, List<Trip> trips )
        {
            throw new NotImplementedException();
        }

        public bool UpdateConsolidatedTrip( User user, Trip trips )
        {
            throw new NotImplementedException();
        }

        public List<AuditTrail> GeAuditTrailForTrip( User user, string tripId )
        {
            throw new NotImplementedException();
        }

        public bool UpdateGps( User user, List<Gps> gpsPoints )
        {
            throw new NotImplementedException();
        }

        public void UnPickupable( User user, Trip trip, decimal quantityPickedUp )
        {
            throw new NotImplementedException();
        }

        public bool UpdateClaim( string authToken, string[] tripIds, string signature, int signatureHeight, int signatureWidth, string podName, DateTime pickupTime )
        {
            throw new NotImplementedException();
        }

        public bool PriorityPickupByBarcode( User user, string barcode, DateTime pickupTime )
        {
            throw new NotImplementedException();
        }

        public List<ServiceLevel> GetServiceLevels( User user )
        {
            throw new NotImplementedException();
        }

        public List<Trip> GetTripsByLocatoion( User user, string locationCode, bool isPickupBarcode, int statusFilter )
        {
            throw new NotImplementedException();
        }

        public void NotifyTripsChanged( int board, Action<List<Protocol.Data.Trip>> callback )
        {
            throw new NotImplementedException();
        }
    }
}