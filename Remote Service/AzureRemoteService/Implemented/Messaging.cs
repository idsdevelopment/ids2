﻿using System;
using Protocol.Data;
using Utils;

namespace AzureRemoteService
{
    public partial class AzureClient
    {
        public static ( MessagingKeys Keys, bool Ok ) MessagingConnect()
        {
            (MessagingKeys Keys, bool Ok) Retval = ( Keys: null, Ok: false );

            using( var Client = new AzureClient() )
            {
                try
                {
                    var Conn = Client.RequestMessagingConnect().Result;
                    var (Value, Ok) = Encryption.FromTimeLimitedToken( Conn.PrimaryConnectionString );
                    if( Ok )
                    {
                        var SecondaryConnectionString = Encryption.FromTimeLimitedToken( Conn.SecondaryConnectionString );
                        if( SecondaryConnectionString.Ok )
                        {
                            var DispatchSubscriptionName = Encryption.FromTimeLimitedToken( Conn.DispatchSubscriptionName );
                            if( DispatchSubscriptionName.Ok )
                            {
                                var DriversSubscriptionName = Encryption.FromTimeLimitedToken( Conn.DriversSubscriptionName );
                                if( DriversSubscriptionName.Ok )
                                {
                                    var TripsTopic = Encryption.FromTimeLimitedToken( Conn.TripsTopic );
                                    if( TripsTopic.Ok )
                                    {
                                        Conn.PrimaryConnectionString = Value;
                                        Conn.SecondaryConnectionString = SecondaryConnectionString.Value;
                                        Conn.DispatchSubscriptionName = DispatchSubscriptionName.Value;
                                        Conn.DriversSubscriptionName = DriversSubscriptionName.Value;
                                        Conn.TripsTopic = TripsTopic.Value;
                                        Retval.Keys = Conn;
                                        Retval.Ok = true;
                                        return Retval;
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine( "Invalid message keys." );
                }
                catch( Exception E )
                {
                    Console.WriteLine( E );
                    Console.WriteLine( "Cannot connect to messaging" );
                }
            }
            return Retval;
        }
    }
}