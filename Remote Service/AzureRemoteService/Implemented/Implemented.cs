﻿using System.Threading.Tasks;
using Protocol.Data;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public void SetDebugServer( bool debugServer )
        {
            AzureClient._DebugServer = debugServer;
        }

        public bool IsIds
        {
            get
            {
                using( var Client = new AzureClient() )
                    return Client.RequestIsIds().Result;
            }
        }

        public bool IsNewCarrier
        {
            get
            {
                using( var Client = new AzureClient() )
                    return Client.RequestIsNewCarrier().Result;
            }
        }

        public bool CreateCarrier()
        {
            using( var Client = new AzureClient() )
                return Client.RequestCreateCarrier().Result;
        }

        public Distances GetDistances( RouteOptimisationCompanyAddresses addresses )
        {
            using( var Client = new AzureClient() )
                return Client.RequestDistance( addresses ).Result;
        }

        public Roles GetRoles()
        {
            using( var Client = new AzureClient() )
                return Client.RequestRoles().Result;
        }

        public Zones GetZones()
        {
            using( var Client = new AzureClient() )
                return Client.RequestZones().Result;
        }

        public void AddUpdateZones( Zones zones )
        {
            Task.Run( () =>
            {
                using( var Client = new AzureClient() )
                    Client.RequestAddUpdateZones( zones ).Wait();
            } );
        }
    }
}