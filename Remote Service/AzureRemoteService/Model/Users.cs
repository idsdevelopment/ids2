﻿using System;
using System.Threading.Tasks;
using RemoteService;
using RemoteService.Model;
using Utils;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public void SignOut()
        {
            Task.Run( () =>
            {
                using( var Client = new AzureClient() )
                    Client.RequestSignOut().Wait();
            } );
        }

        public bool HasTimeError { get; set; }

        (User User, bool InError, bool IsTimeError) IAzRemoteService.LogIn( string carrierId, string userName, string password )
        {
            (User User, bool InError, bool IsTimeError) RetVal = ( User: null, InError: true, IsTimeError: false );

            using( var Client = new AzureClient() )
            {
                var AuthToken = Client.RequestLogin( Encryption.ToTimeLimitedToken( carrierId ), Encryption.ToTimeLimitedToken( userName ), Encryption.ToTimeLimitedToken( password ), DateTimeOffset.Now.Offset.Hours.ToString(), "Version test",
                                                     Client.DebugServer, Encryption.ToTimeLimitedToken( DateTime.UtcNow.Ticks.ToString() ) ).Result;

                if( !string.IsNullOrWhiteSpace( AuthToken ) )
                {
                    if( AuthToken != AzureClient.TIME_ERROR )
                    {
                        AzureClient.AuthToken = AuthToken;
                        RetVal.User = new IdsRemote.User { Enabled = true };
                        RetVal.InError = false;
                    }
                    else
                        RetVal.IsTimeError = true;
                }
            }
            return RetVal;
        }

        public User LogIn( string carrierId, string userName, string password )
        {
            var Result = ( this as IAzRemoteService ).LogIn( carrierId, userName, password );
            HasTimeError = Result.IsTimeError;
            return Result.User;
        }
    }
}