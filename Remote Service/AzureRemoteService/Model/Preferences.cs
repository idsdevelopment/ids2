﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Protocol.Data;
using RemoteService.Model;
using Preferences = Protocol.Data.Preferences;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public Dictionary<string, string> GetDevicePreferences( User user )
        {
            return new Dictionary<string, string>();
        }

        public Preferences GetUserPreferences()
        {
            using( var Client = new AzureClient() )
                return Client.RequestPreferences().Result;
        }

        public void UpdatePreference( Preference preference )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestUpdatePreference( preference );
            } );
        }
    }
}