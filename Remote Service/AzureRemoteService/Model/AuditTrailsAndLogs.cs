﻿using System.Threading.Tasks;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public void ErrorLog( string errorMessage )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestErrorLog( errorMessage );
            } );
        }

        public void AuditTrailNewOperation( string program, string text, string comment )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestAuditTrailNew( program, text, comment );
            } );
        }

        public void AuditTrailModifyOperation( string program, string text, string comment )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestAuditTrailModify( program, text, comment );
            } );
        }

        public void AuditTrailDeleteOperation( string program, string text, string comment )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestAuditTrailDelete( program, text, comment );
            } );
        }

        public void AuditTrailInformOperation( string program, string text, string comment )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestAuditTrailInform( program, text, comment );
            } );
        }
    }
}