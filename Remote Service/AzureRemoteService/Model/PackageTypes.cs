﻿using Protocol.Data;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public PackageTypeList GetPackageTypes()
        {
            using( var Client = new AzureClient() )
                return Client.RequestPackageTypes().Result;
        }
    }
}