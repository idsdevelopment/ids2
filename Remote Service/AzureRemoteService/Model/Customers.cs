﻿namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public string GenerateCustomerCode( string firstName, string lastName, string addressLine1 )
        {
            using( var Client = new AzureClient() )
                return Client.RequestGenerateLoginCode( firstName, lastName, addressLine1 ).Result;
        }
    }
}