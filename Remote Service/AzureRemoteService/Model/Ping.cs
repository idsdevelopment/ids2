﻿namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public bool Ping()
        {
            try
            {
                using( var Client = new AzureClient() )
                {
                    var Ok = Client.RequestPing().Result;
                    return Ok == "OK";
                }
            }
            catch
            {
            }
            return false;
        }
    }
}