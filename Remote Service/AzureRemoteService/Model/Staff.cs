﻿using System.Threading.Tasks;
using Protocol.Data;

namespace AzureRemoteService
{
    public partial class AzureRemoteService
    {
        public bool StaffMemberExists( string code )
        {
            using( var Client = new AzureClient() )
                return Client.RequestStaffMemberExists( code ).Result;
        }

        public bool AddStaffMember( AddStaffMember staffMember )
        {
            using( var Client = new AzureClient() )
                return Client.RequestAddStaffMember( staffMember ).Result;
        }

        public StaffList GetStaff()
        {
            using( var Client = new AzureClient() )
                return Client.RequestGetStaff().Result;
        }

        public StaffList GetStaffMember( string staffId )
        {
            //           using (var Client = new AzureClient())
            return null;
        }

        public void AddStaffMemberWithRolesAndZones( AddStaffMemberWithRolesAndZones staffMemberWithRolesAndZones )
        {
            Task.Run( async () =>
            {
                using( var Client = new AzureClient() )
                    await Client.RequestAddUpdateStaffMemberWithRolesAndZones( staffMemberWithRolesAndZones );
            } );
        }
    }
}