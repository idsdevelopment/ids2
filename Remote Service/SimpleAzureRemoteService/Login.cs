﻿using System;
using System.Threading.Tasks;
using Protocol.Data;
using Utils;

namespace AzureRemoteService
{
	public partial class Azure
	{
		public enum AZURE_CONNECTION_STATUS
		{
			ERROR,
			TIME_ERROR,
			OK
		}

		private static bool SignedIn;

		public static string CarrierId { get; private set; }
		public static string UserName { get; private set; }
		public static string ProgramName { get; private set; }
		public static DateTimeOffset SignInTime { get; private set; }

		public static async Task<AZURE_CONNECTION_STATUS> LogIn( string pgm, string carrierId, string userName, string password, bool isDriver = false )
		{
			CarrierId   = carrierId;
			UserName    = userName;
			ProgramName = pgm;

			var IsDebug = carrierId.IsDebug();

			var TicksNow = DateTime.UtcNow.Ticks.ToString();

			var Func = isDriver ? (Func<string, string, string, string, string, string, string, string, Task<string>>)Client.RequestLoginAsDriver : Client.RequestLogin;

			var Result = await Func( pgm, Encryption.ToTimeLimitedToken( carrierId ), Encryption.ToTimeLimitedToken( userName ), Encryption.ToTimeLimitedToken( password ), DateTimeOffset.Now.Offset.Hours.ToString(),
			                         isDriver ? "Driver" : "User",
			                         IsDebug ? "t" : "f", Encryption.ToTimeLimitedToken( TicksNow ) );

			if( Result != TIME_ERROR )
			{
				if( Result.IsNotNullOrWhiteSpace() )
				{
					var (Value, Ok) = Encryption.FromTimeLimitedToken( Result );

					if( Ok )
					{
						if( Value.IsNotNullOrWhiteSpace() )
						{
							AuthToken  = Value;
							SignInTime = DateTimeOffset.UtcNow;
							SignedIn   = true;

							DebugServer = IsDebug;

							return AZURE_CONNECTION_STATUS.OK;
						}

						return AZURE_CONNECTION_STATUS.ERROR;
					}

					return AZURE_CONNECTION_STATUS.TIME_ERROR;
				}

				return AZURE_CONNECTION_STATUS.ERROR;
			}

			return AZURE_CONNECTION_STATUS.TIME_ERROR;
		}

		public static async Task SignOut()
		{
			if( SignedIn )
			{
				try
				{
					var Diff = DateTimeOffset.UtcNow - SignInTime;

					await Client.RequestSignOut( new SignOut
					                             {
						                             Hours   = (uint)Diff.Hours,
						                             Minutes = (byte)Diff.Minutes,
						                             Seconds = (byte)Diff.Seconds
					                             } );
				}
				finally
				{
					SignedIn = false;
				}
			}
		}
	}
}