﻿using System;
using System.Collections.Generic;
using Ids;
using Protocol.Data;
using Utils;

namespace AzureRemoteService
{
	public static class Extensions
	{
		public const string DEBUG_PREFIX = "Test";

		public static bool IsDebug( this string account )
		{
			var P = Math.Max( account.LastIndexOf( '/' ), account.LastIndexOf( '\\' ) );

			if( P >= 0 )
				account = account.Substring( P + 1 );

			return ( account.Compare( DEBUG_PREFIX, StringComparison.OrdinalIgnoreCase ) != 0 ) && account.StartsWith( DEBUG_PREFIX, StringComparison.OrdinalIgnoreCase );
		}
	}

	public partial class Azure
	{
		private static InternalClient _Client;

#if DEBUG
		public static volatile SLOT Slot = SLOT.ALPHA_TRW;
#else
		public static volatile SLOT Slot = SLOT.PRODUCTION;
#endif
		public class InternalClient : IdsClient
		{
			private readonly string EndPoint;

			protected override string OnGetAuthToken( string authToken )
			{
				return Encryption.ToTimeLimitedToken( authToken );
			}

			public InternalClient( string endPoint ) : base( endPoint )
			{
				EndPoint = endPoint;
			}

			public InternalClient( string endPoint, string authorisationToken ) : base( endPoint, authorisationToken )
			{
				EndPoint  = endPoint;
				AuthToken = authorisationToken;
			}

#if !NO_LISTENERS
			public void ListenTripUpdate( string connectionType, string userName, Action<List<TripUpdateMessage>> response )
			{
				base.ListenTripUpdate( EndPoint, AuthToken, connectionType, userName, response );
			}
#endif
		}

		public static InternalClient Client
		{
			get
			{
				lock( LockObject )
				{
					return _Client ??= new InternalClient( GetSlot() )
					                   {
#if !XAMARIN
						                   MaxResponseContentBufferSize = 300_000_000,
#endif
					                   };
				}
			}
		}

		private const string PRODUCTION_END_POINT = "https://idsroute.azurewebsites.net/",
		                     ALPHAT_END_POINT = "https://idsroute-alphat.azurewebsites.net",
		                     ALPHA_TRW_END_POINT = "https://idsroute-alphaTRW.azurewebsites.net",
		                     ALPHAC_END_POINT = "https://idsroute-alphac.azurewebsites.net",
		                     BETA_END_POINT = "https://idsroute-beta.azurewebsites.net";

		public const string TIME_ERROR = "TimeError";


		// ReSharper disable once InconsistentNaming
		public enum SLOT
		{
			PRODUCTION,
			ALPHAT,
			ALPHAC,
			BETA,
			ALPHA_TRW
		}

		private static readonly object LockObject = new object();

		public static string AuthToken
		{
			set
			{
				if( string.IsNullOrEmpty( value ) )
					throw new ArgumentException( "Auth token set to empty" );

				Client.AuthorisationToken = value;
			}
			get => Client.AuthorisationToken;
		}

		public static string GetSlot()
		{
			return Slot switch
			       {
				       SLOT.ALPHAT    => ALPHAT_END_POINT,
				       SLOT.ALPHA_TRW => ALPHA_TRW_END_POINT,
				       SLOT.ALPHAC    => ALPHAC_END_POINT,
				       SLOT.BETA      => BETA_END_POINT,
				       _              => PRODUCTION_END_POINT
			       };
		}

		public static bool DebugServer { get; set; }
	}
}