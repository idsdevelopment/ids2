﻿using System.Collections.Generic;
using System.Globalization;
using IdsRemoteService.IdsRemoteServiceReference;
using RemoteService.Model;

namespace IdsRemote
{
    public partial class IdsService
    {
        public void UpdateCharges( RemoteService.Model.User user, string tripId, List<Charge> charges )
        {
            using( var Client = new IdsClient() )
            {
                var L = charges.Count;

                var ChargeIds = new string[ L ];
                var ChargeValues = new string[ L ];

                var Ndx = 0;
                foreach( var Charge in charges )
                {
                    ChargeIds[ Ndx ] = Charge.Name;
                    ChargeValues[ Ndx++ ] = Charge.IsStringValue ? Charge.StringValue : Charge.Value.ToString( CultureInfo.InvariantCulture );
                }

                var Request = new updateTripCharges { authToken = AuthToken, accountId = user.AccountId, tripId = tripId, chargeIds = ChargeIds, chargeValues = ChargeValues };

                Client.updateTripCharges( Request );
            }
        }
    }
}