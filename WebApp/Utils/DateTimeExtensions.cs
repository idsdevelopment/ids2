﻿using System;

namespace Utils
{
	public static class DateTimeExtensions
	{
		public static DateTimeOffset StartOfDay( this DateTime dateTime )
		{
			return dateTime.Date;
		}

		public static DateTimeOffset StartOfDay( this DateTimeOffset dateTime )
		{
			return dateTime.Date;
		}

		public static DateTimeOffset EndOfDay( this DateTime dateTime )
		{
			return new DateTime( dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 999 );
		}

		public static DateTimeOffset EndOfDay( this DateTimeOffset dateTime )
		{
			return dateTime.Date.EndOfDay();
		}
	}
}