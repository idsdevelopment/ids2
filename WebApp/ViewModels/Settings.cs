﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utils;

namespace ViewModels
{
	public partial class AViewModelBase : Dictionary<string, object>
	{
		private IDictionary<string, PropertyInfo> SettingsMap;

		private readonly Func<Task<string>> OnLoadData;
		private readonly Func<string, Task> OnSaveData;


		private void InitialiseSettings()
		{
			SettingsMap = (
				              from PropertyInfo in PropertyMap
				              let Attr = PropertyInfo.Value.GetCustomAttributes( typeof( SettingAttribute ), true )
				              where Attr.Length == 1
				              select PropertyInfo ).ToDictionary( prop => prop.Key, prop => prop.Value );
		}

		protected async void LoadSettings()
		{
			if( !( OnLoadData is null ) )
			{
				try
				{
					var SaveData = await OnLoadData();

					if( SaveData.IsNotNullOrWhiteSpace() )
					{
						var SerializedObject = Encryption.Decrypt( SaveData );
						var Settings         = JsonConvert.DeserializeObject<JObject>( SerializedObject );

						foreach( var (Key, JToken) in Settings )
						{
							if( SettingsMap.TryGetValue( Key, out var Info ) )
							{
								dynamic Value = Convert.ChangeType( JToken, Info.PropertyType );
								Info.SetValue( this, Value );
							}
						}
					}

					Refresh();
				}
				catch
				{
				}
			}
		}

		protected async void SaveSettings()
		{
			if( !( OnSaveData is null ) )
			{
				try
				{
					var Settings = new JObject();

					foreach( var (Key, PropertyInfo) in SettingsMap )
						Settings.Add( new JProperty( Key, PropertyInfo.GetValue( this ) ) );

					var SerializedObject = JsonConvert.SerializeObject( Settings );

					await OnSaveData( Encryption.Encrypt( SerializedObject ) );
				}
				catch
				{
				}
			}
		}
	}
}