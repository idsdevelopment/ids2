﻿using System;
using System.Threading.Tasks;
using AzureRemoteService;

namespace ViewModels.Models.SignIn
{
	public class SignInViewModel : ViewModelBase
	{
		public Action OnSignedIn;

		[Setting]
		public string CarrierId
		{
			get { return Get( () => CarrierId, "" ); }
			set { Set( () => CarrierId, value ); }
		}


		[Setting]
		public string UserName
		{
			get { return Get( () => UserName, "" ); }
			set { Set( () => UserName, value ); }
		}


		public string Password
		{
			get { return Get( () => Password, "" ); }
			set { Set( () => Password, value ); }
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();
			LoadSettings();
		}

		public SignInViewModel( Action refresh, Func<Task<string>> onLoadData, Func<string, Task> onSaveData ) : base( refresh, onLoadData, onSaveData )
		{
		}


	#region Sign In

		public bool DisableSignIn
		{
			get { return Get( () => DisableSignIn, true ); }
			set { Set( () => DisableSignIn, value ); }
		}


		[DependsUpon150( nameof( CarrierId ) )]
		[DependsUpon( nameof( UserName ) )]
		[DependsUpon( nameof( Password ) )]
		public void DoEnable()
		{
			DisableSignIn = ( CarrierId == "" ) || ( UserName == "" ) || ( Password == "" );
			ErrorVisible = false;
		}

		public async void ExecuteSignIn()
		{
			DisableInput = true;

			try
			{
				Azure.Slot = Azure.SLOT.ALPHA_TRW;
				var Result = await Azure.LogIn( Globals.Version.APP_VERSION, CarrierId, UserName, Password );

				if( Result == Azure.AZURE_CONNECTION_STATUS.OK )
				{
					ErrorVisible = false;
					SaveSettings();
					OnSignedIn?.Invoke();
				}
				else
				{
					DisableInput = false;
					ErrorVisible = true;
				}
			}
			catch
			{
				DisableInput = false;
				ErrorVisible = true;
			}
		}

	#endregion

	#region Disable Input

		public bool DisableInput
		{
			get { return Get( () => DisableInput, false ); }
			set { Set( () => DisableInput, value ); }
		}


		public string DisableInputAsString
		{
			get { return Get( () => DisableInputAsString, "false" ); }
			set { Set( () => DisableInputAsString, value ); }
		}


		[DependsUpon( nameof( DisableInput ) )]
		public void WenDisableInputChanges()
		{
			DisableInputAsString = DisableInput.ToString();
		}

	#endregion

	#region Error

		public bool ErrorVisible
		{
			get { return Get( () => ErrorVisible, false ); }
			set { Set( () => ErrorVisible, value ); }
		}


		public string ErrorHiddenAsString
		{
			get { return Get( () => ErrorHiddenAsString, "true" ); }
			set { Set( () => ErrorHiddenAsString, value ); }
		}

		[DependsUpon( nameof( ErrorVisible ) )]
		public void WhenErrorVisibleChanges()
		{
			ErrorHiddenAsString = (!ErrorVisible).ToString();
		}

	#endregion
	}
}