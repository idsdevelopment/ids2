﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels.Models.MainMenu
{
	public class NavMenuViewModel : ViewModelBase
	{
		public static NavMenuViewModel Instance { get; private set; }

		public NavMenuViewModel( Action refresh ) : base( refresh )
		{
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();
			Instance = this;
		}

	#region Sign In

		public bool SignedIn
		{
			get { return Get( () => SignedIn, false ); }
			set { Set( () => SignedIn, value ); }
		}

		public string SignedInAsString
		{
			get { return Get( () => SignedInAsString, "false" ); }
			set { Set( () => SignedInAsString, value ); }
		}


		public string NotSignedInAsString
		{
			get { return Get( () => NotSignedInAsString, "true" ); }
			set { Set( () => NotSignedInAsString, value ); }
		}

		[DependsUpon( nameof( SignedIn ) )]
		public void WhenSignedInChanged()
		{
			var S = SignedIn;
			SignedInAsString    = S.ToString();
			NotSignedInAsString = ( !S ).ToString();
		}

	#endregion
	}
}