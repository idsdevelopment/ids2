﻿using System;
using AzureRemoteService;
using Globals;
using Newtonsoft.Json;
using ViewModels.Roles;

namespace ViewModels.Models.MainMenu
{
	public class MainViewModel : ViewModelBase
	{
		public const string NOT_SIGNED_IN = "Not Signed In";

		public string UserName
		{
			get { return Get( () => UserName, NOT_SIGNED_IN ); }
			set { Set( () => UserName, value ); }
		}

		[DependsUpon( nameof( UserName ) )]
		public async void WhenUserNameChanges()
		{
			var IsPml = Globals.Modifications.IsPml;

			var AdminRole = new Role
			                {
				                Routes      = true,
				                Roles       = true,
				                CanCreate   = true,
				                Preferences = true,
				                Trip        = true,
				                Search      = true,

				                Reports = true,

				                Audit      = true,
				                AuditStaff = true,
				                AuditTrip  = true,

				                Staff = true,

				                Boards        = true,
				                DriversBoard  = true,
				                DispatchBoard = true,
				                RateWizard    = true,

				                Logs       = true,
				                ViewLog    = true,
				                ExploreLog = true,

				                Customers         = true,
				                CustomerAccounts  = true,
				                CustomerAddresses = true,

				                CanEdit   = true,
				                CanDelete = true,
				                CanView   = true,

				                Pml          = IsPml,
				                PmlProducts  = IsPml,
				                PmlShipments = IsPml
			                };

			var JRoles = await Azure.Client.RequestStaffMemberRoles( UserName );

			foreach( var JRole in JRoles )
			{
				Role R;

				var IsAdmin = JRole.IsAdministrator;
				Globals.Users.Security.IsDriver = JRole.IsDriver;
				Globals.Users.Security.IsAdmin  = IsAdmin;

				if( IsAdmin )
					R = AdminRole;
				else
					R = JsonConvert.DeserializeObject<Role>( JRole.JsonObject ) ?? new Role();

				EnablePreferencesAsString = R.Preferences.ToString();
			}
		}

		public MainViewModel( Action refresh ) : base( refresh )
		{
		}

	#region Enables

		public string EnablePreferencesAsString
		{
			get { return Get( () => EnablePreferencesAsString, "false" ); }
			set { Set( () => EnablePreferencesAsString, value ); }
		}

	#endregion

	#region Carrier Id

		public string CarrierId
		{
			get { return Get( () => CarrierId, "" ); }
			set { Set( () => CarrierId, value ); }
		}

		[DependsUpon( nameof( CarrierId ) )]
		public void WhenCarrierIdChanges()
		{
			Company.CarrierId = CarrierId;
		}

	#endregion
	}
}