﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ViewModels
{
	public abstract partial class AViewModelBase : Dictionary<string, object>
	{
		private readonly Action _Refresh;
		private readonly object RefreshTimerLock = new object();
		private Timer RefreshTimer;

		protected void Refresh()
		{
			lock( RefreshTimerLock )
			{
				if( !( _Refresh is null ) )
				{
					void KillTimer()
					{
						RefreshTimer.Change( Timeout.Infinite, Timeout.Infinite );
						RefreshTimer.Dispose();
						RefreshTimer = null;
					}

					if( !( RefreshTimer is null ) )
						KillTimer();

					RefreshTimer = new Timer( state =>
					                          {
						                          lock( RefreshTimerLock )
						                          {
							                          KillTimer();
							                          _Refresh.Invoke();
						                          }
					                          }, null, 100, Timeout.Infinite );
				}
			}
		}

		protected virtual void OnInitialised()
		{
		}

		protected virtual void SetDefaultValues()
		{
			DisableEvents = true;

			try
			{
				InitialiseValues();
				Refresh();
			}
			catch
			{
			}
			finally
			{
				DisableEvents = false;
			}
		}

		protected virtual void OnFetchValues()
		{
		}

		protected new object this[ string ndx ]
		{
			set => base[ ndx ] = value;
			get => base[ ndx ];
		}

		protected AViewModelBase( Action stateChanged, Func<Task<string>> onLoadData, Func<string, Task> onSaveData )
		{
			_Refresh   = stateChanged;
			OnSaveData = onSaveData;
			OnLoadData = onLoadData;

			MapProperties();
			InitialiseSettings();

			// ReSharper disable once VirtualMemberCallInConstructor
			OnFetchValues();

			InitialiseValues();

			MapMethods();
			MapDependsUpon();

			// ReSharper disable once VirtualMemberCallInConstructor
			OnInitialised();
		}

		protected AViewModelBase( Action stateChanged ) : this( stateChanged, null, null )
		{
		}

		protected AViewModelBase() : this( null, null, null )
		{
		}
	}
}