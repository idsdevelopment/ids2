﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ViewModels
{
	public partial class AViewModelBase : Dictionary<string, object>
	{
		protected T Get<T>( string name, T defaultValue )
		{
			if( TryGetValue( name, out var Value ) )
				return (T)Value;

			return defaultValue;
		}

		protected T Get<T>( string name )
		{
			return Get( name, default( T ) );
		}

		protected T Get<T>( Expression<Func<T>> expression, T defaultValue )
		{
			return Get( PropertyName( expression ), defaultValue );
		}

		protected T Get<T>( string name, Func<T> initialValue )
		{
			if( TryGetValue( name, out var Value ) )
				return (T)Value;

			this[ name ] = initialValue();

			return Get<T>( name );
		}

		protected T Get<T>( Expression<Func<T>> expression, Func<T> initialValue )
		{
			return Get( PropertyName( expression ), initialValue );
		}


		protected void Set<T>( Expression<Func<T>> expression, T value )
		{
			var Name = PropertyName( expression );

			if( TryGetValue( Name, out var Value ) )
			{
				if( ( Value is null && value is null ) )
					return;

				if( !( Value is null ) && !( value is null ) && Value.Equals( value ) )
					return;
			}

			this[ Name ] = value;

			RaisePropertyChanged( Name );
		}
	}
}