﻿using System;
using System.Collections.Generic;

namespace ViewModels
{
	public partial class AViewModelBase : Dictionary<string, object>
	{
		[AttributeUsage( AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true )]
		protected class DependsUponAttribute : Attribute
		{
			public string DependencyName { get; }
			public int Delay { get; set; }

			public DependsUponAttribute( string propertyName )
			{
				DependencyName = propertyName;
			}
		}

		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon150Attribute : DependsUponAttribute
		{
			public DependsUpon150Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 150;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon250Attribute : DependsUponAttribute
		{
			public DependsUpon250Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 250;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon350Attribute : DependsUponAttribute
		{
			public DependsUpon350Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 350;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon500Attribute : DependsUponAttribute
		{
			public DependsUpon500Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 500;
			}
		}

		[AttributeUsage( AttributeTargets.Property )]
		protected class SettingAttribute : Attribute
		{
		}
	}
}