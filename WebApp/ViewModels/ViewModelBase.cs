﻿using System;
using System.Threading.Tasks;

namespace ViewModels
{
	public class ViewModelBase : AViewModelBase
	{
		protected ViewModelBase( Action stateChanged ) : base( stateChanged, null, null )
		{
		}

		protected ViewModelBase()
		{
		}

		protected ViewModelBase( Action stateChanged, Func<Task<string>> onLoadData, Func<string, Task> onSaveData ) : base( stateChanged, onLoadData, onSaveData )
		{
		}
	}
}