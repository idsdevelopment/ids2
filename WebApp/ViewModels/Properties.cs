﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ViewModels
{
	public partial class AViewModelBase : Dictionary<string, object>
	{
		private IEnumerable<PropertyInfo> PropertyInfos => _PropertyInfos ??= GetType().GetProperties().ToList();
		private IEnumerable<PropertyInfo> _PropertyInfos;
		private IDictionary<string, PropertyInfo> PropertyMap = new Dictionary<string, PropertyInfo>();
		private IDictionary<string, object> DefaultValues;


		private static string PropertyName<T>( Expression<Func<T>> expression )
		{
			if( !( expression.Body is MemberExpression MemberExpression ) )
				throw new ArgumentException( "expression must be a property expression" );

			return MemberExpression.Member.Name;
		}

		private void MapProperties()
		{
			var FullName = GetType().FullName;

			PropertyMap = ( from P in PropertyInfos
			                where !( P.GetMethod is null ) && P.GetMethod.IsPublic && !( P.SetMethod is null ) && P.SetMethod.IsPublic && ( P.DeclaringType != null ) && ( P.DeclaringType.FullName == FullName )
			                select P ).ToDictionary( prop => prop.Name, prop => prop );
		}

		private void InitialiseValues()
		{
			if( DefaultValues is null )
			{
				DefaultValues = new Dictionary<string, object>();

				foreach( var (Key, PropertyInfo) in PropertyMap )
				{
					var Value = PropertyInfo.GetValue( this );

					if( !( Value is null ) )
						DefaultValues[ Key ] = Value;
				}
			}

			foreach( var Info in PropertyMap )
			{
				var Key = Info.Key;

				if( DefaultValues.TryGetValue( Key, out var Value ) )
					this[ Key ] = Value;
			}
		}
	}
}