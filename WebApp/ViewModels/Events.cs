﻿using System.Collections.Generic;
using System.Threading;

namespace ViewModels
{
	public partial class AViewModelBase : Dictionary<string, object>
	{
		protected bool EventsDisabled => DisableEvents;

		protected bool DisableEvents
		{
			get => _DisableEvents > 0;
			set
			{
				if( value )
					++_DisableEvents;
				else if( _DisableEvents > 0 )
					--_DisableEvents;
			}
		}

		protected int _DisableEvents;

		private void RaisePropertyChanged( string propertyName )
		{
			if( !DisableEvents && PropertyToDependsUponMethodMap.TryGetValue( propertyName, out var Methods ) )
			{
				foreach( var Method in Methods )
				{
					if( Method.Delay > 0 )
					{
						var Name = Method.Name;

						lock( ActiveDelayMap )
						{
							// Restart Timer
							if( ActiveDelayMap.TryGetValue( Name, out var Timer ) )
							{
								ActiveDelayMap.Remove( Name );
								Timer.Change( Timeout.Infinite, Timeout.Infinite );
								Timer.Dispose();
							}

							ActiveDelayMap[ Name ] = new Timer( name =>
							                                    {
								                                    try
								                                    {
									                                    lock( ActiveDelayMap )
									                                    {
										                                    var T = ActiveDelayMap[ Name ];
										                                    ActiveDelayMap.Remove( Name );
										                                    T.Dispose();
									                                    }
#pragma warning disable 8632
									                                    Method.MethodInfo?.Invoke( this, new object?[ 0 ] );
#pragma warning restore 8632
									                                    Refresh();
								                                    }
								                                    catch
								                                    {
								                                    }
							                                    }, Name, Method.Delay, Timeout.Infinite );
						}
					}
					else
#pragma warning disable 8632
						Method.MethodInfo.Invoke( this, new object?[ 0 ] );
#pragma warning restore 8632
				}
			}

			Refresh();
		}
	}
}