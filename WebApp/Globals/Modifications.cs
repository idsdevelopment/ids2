﻿using System;

namespace Globals
{
	public static class Company
	{
		private static string _CarrierId;

		public static string CarrierId
		{
			get => _CarrierId;
			set => _CarrierId = value.Trim();
		}
	}

	public partial class Modifications
	{
		private static bool IsCompany( string companyName, string compareName )
		{
			return string.Compare( companyName, compareName, StringComparison.OrdinalIgnoreCase ) == 0;
		}

		private static bool IsCompany( string companyName )
		{
			return IsCompany( companyName, Company.CarrierId );
		}
	}
}