﻿using Newtonsoft.Json;

namespace Protocol.Data
{
	public class CrashReport
	{
		public string C { get; set; } = "";

		[JsonIgnore]
		public string CrashData
		{
			get => C;
			set => C = value;
		}


		public sbyte TSbyte { get; set; }

		[JsonIgnore]
		public sbyte TimeZone
		{
			get => TSbyte;
			set => TSbyte = value;
		}


		public string A { get; set; } = "";

		[JsonIgnore]
		public string Account
		{
			get => A;
			set => A = value;
		}


		public string U { get; set; } = "";

		[JsonIgnore]
		public string User
		{
			get => U;
			set => U = value;
		}
	}
}