﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class RouteBase : ProgramData
	{
		public RouteBase( string programName ) : base( programName )
		{
		}

		public string CustomerCode { get; set; } = "";
		public string RouteName { get; set; } = "";
	}

	public class Route : RouteBase
	{
		public Route( string programName ) : base( programName )
		{
		}

		public bool Monday { get; set; }
		public bool Tuesday { get; set; }
		public bool Wednesday { get; set; }
		public bool Thursday { get; set; }
		public bool Friday { get; set; }
		public bool Saturday { get; set; }
		public bool Sunday { get; set; }

		public TimeSpan StartTime { get; set; } = new TimeSpan();
		public TimeSpan EndTime { get; set; } = new TimeSpan();

		public bool IsStatic { get; set; }
		public short StaticCount { get; set; }
		public decimal Commission { get; set; }

		public string BillingCompanyCode { get; set; } = "";

		public bool ShowOnMobileApp { get; set; }
	}

	public class RouteAndCompanyAddresses : Route
	{
		public RouteAndCompanyAddresses( string programName ) : base( programName )
		{
		}

		public Companies CompaniesInRoute { get; set; } = new Companies();
	}

	public class RouteCompany : RouteBase
	{
		public RouteCompany( string programName ) : base( programName )
		{
		}

		public string RouteCompanyCode { get; set; } = "";
		public bool Delete { get; set; }
	}

	public class RouteLookup : PreFetch
	{
		public string CustomerCode { get; set; }
	}

	public class RouteOptions
	{
		public RouteOptions()
		{
		}

		public RouteOptions( RouteOptions r )
		{
			Option1 = r.Option1;
			Option2 = r.Option2;
			Option3 = r.Option3;
			Option4 = r.Option4;
			Option5 = r.Option5;
			Option6 = r.Option6;
			Option7 = r.Option7;
			Option8 = r.Option8;
		}

		public bool Option1 { get; set; }
		public bool Option2 { get; set; }
		public bool Option3 { get; set; }
		public bool Option4 { get; set; }
		public bool Option5 { get; set; }
		public bool Option6 { get; set; }
		public bool Option7 { get; set; }
		public bool Option8 { get; set; }
	}

	public class RouteUpdate : RouteBase
	{
		public RouteUpdate( string programName ) : base( programName )
		{
		}

		public string CompanyName { get; set; }

		public RouteOptions Options { get; set; } = new RouteOptions();
	}

	public class RouteCompanySummary : CompanyByAccountSummary
	{
		public RouteCompanySummary()
		{
		}

		public RouteCompanySummary( RouteCompanySummary r ) : base( r )
		{
			Options = new RouteOptions( r.Options );
		}

		public RouteCompanySummary( CompanyByAccountSummary r ) : base( r )
		{
		}

		public RouteOptions Options { get; set; } = new RouteOptions();
	}

	public class RouteCompanySummaryList : List<RouteCompanySummary>
	{
	}

	public class CompanyAndOptions : RouteOptions
	{
		public string CompanyName { get; set; } = "";
	}

	public class RouteCompanyList : ProgramData
	{
		public RouteCompanyList( string programName ) : base( programName )
		{
		}

		public string CustomerCode { get; set; } = "";
		public string RouteName { get; set; } = "";
		public List<CompanyAndOptions> CompanyOptions { get; set; } = new List<CompanyAndOptions>();
	}

	public class RouteSchedule
	{
		public string RouteName { get; set; } = "";

		public bool Monday { get; set; }
		public bool Tuesday { get; set; }
		public bool Wednesday { get; set; }
		public bool Thursday { get; set; }
		public bool Friday { get; set; }
		public bool Saturday { get; set; }
		public bool Sunday { get; set; }

		public TimeSpan StartTime { get; set; }
		public TimeSpan EndTime { get; set; }

		public bool IsStatic { get; set; }
		public short StaticCount { get; set; }
		public decimal Commission { get; set; }

		public string BillingCompanyCode { get; set; } = "";
		public bool ShowOnMobileApp { get; set; }
	}

	public class RouteSummary : RouteOptions
	{
		public string CompanyName { get; set; } = "";
		public string Suite { get; set; } = "";
		public string AddressLine1 { get; set; } = "";
	}

	public class RouteScheduleSummary : RouteSchedule
	{
		public string CompanyName { get; set; } = "";
		public string Suite { get; set; } = "";
		public string AddressLine1 { get; set; } = "";
	}

	public class RouteScheduleAndRoutes : RouteSchedule
	{
		public List<RouteSummary> RouteSummary = new List<RouteSummary>();
	}

	public class CustomerRoutesSchedule
	{
		public List<RouteScheduleAndRoutes> ScheduleAndRoutes = new List<RouteScheduleAndRoutes>();
		public string CustomerCode { get; set; } = "";
		public bool EnableRoutesForDevice { get; set; }
	}

	public class CustomerRoutesScheduleList : List<CustomerRoutesSchedule>
	{
	}

	public class RouteLookupSummaryList : List<RouteSchedule>
	{
	}

	public class RouteScheduleUpdate : RouteSchedule
	{
		public string ProgramName { get; set; }
		public string CustomerCode { get; set; }
	}

	public class EnableRouteForeCustomer
	{
		public string CustomerCode { get; set; } = "";
		public bool Enable { get; set; }
	}

	public class CustomerRouteBasic
	{
		public string N { get; set; } = "";

		[JsonIgnore]
		public string RouteName
		{
			get => N;
			set => N = value;
		}


		public bool E { get; set; }

		[JsonIgnore]
		public bool Enabled
		{
			get => E;
			set => E = value;
		}
	}

	public class CustomerRoutesBasic
	{
		public List<CustomerRouteBasic> R { get; set; } = new List<CustomerRouteBasic>();

		[JsonIgnore]
		public List<CustomerRouteBasic> Routes
		{
			get => R;
			set => R = value;
		}

		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public bool E { get; set; }

		[JsonIgnore]
		public bool EnableRoutesForCustomer
		{
			get => E;
			set => E = value;
		}
	}

	public class CustomersRoutesBasic : List<CustomerRoutesBasic>
	{
	}

	public class AddUpdateRenameRouteBasic
	{
		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public string O { get; set; } = "";

		[JsonIgnore]
		public string OldName
		{
			get => O;
			set => O = value;
		}


		public string N { get; set; } = "";

		[JsonIgnore]
		public string NewName
		{
			get => N;
			set => N = value;
		}


		public bool E { get; set; } = true;

		[JsonIgnore]
		public bool Enabled
		{
			get => E;
			set => E = value;
		}
	}
}