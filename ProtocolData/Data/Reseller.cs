﻿namespace Protocol.Data
{
	public class ResellerCustomerCompany : CompanyBase
	{
		public string CustomerCode { get; set; }

		public ResellerCustomerCompany()
		{
		}

		public ResellerCustomerCompany( Address address ) : base( address )
		{
		}
	}

	public class ResellerCustomerCompanyLookup : ResellerCustomerCompany
	{
		public bool Ok { get; set; }

		public ResellerCustomerCompanyLookup()
		{
		}

		public ResellerCustomerCompanyLookup( Address address ) : base( address )
		{
		}
	}
}