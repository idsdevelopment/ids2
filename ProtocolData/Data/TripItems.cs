﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class TripItem
	{
		public string I { get; set; } = "";

		[JsonIgnore]
		public string ItemCode
		{
			get => I;
			set => I = value;
		}


		public string I1 { get; set; } = "";

		[JsonIgnore]
		public string ItemCode1
		{
			get => I1;
			set => I1 = value;
		}

		public string B { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => B;
			set => B = value;
		}

		public string B1 { get; set; } = "";

		[JsonIgnore]
		public string Barcode1
		{
			get => B1;
			set => B1 = value;
		}


		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}


		public string R { get; set; } = "";

		[JsonIgnore]
		public string Reference
		{
			get => R;
			set => R = value;
		}


		public decimal H { get; set; }

		[JsonIgnore]
		public decimal Height
		{
			get => H;
			set => H = value;
		}


		public decimal Wi { get; set; }

		[JsonIgnore]
		public decimal Width
		{
			get => Wi;
			set => Wi = value;
		}


		public decimal L { get; set; }

		[JsonIgnore]
		public decimal Length
		{
			get => L;
			set => L = value;
		}


		public decimal V { get; set; }

		[JsonIgnore]
		public decimal Volume
		{
			get => V;
			set => V = value;
		}


		public decimal P { get; set; }

		[JsonIgnore]
		public decimal Pieces
		{
			get => P;
			set => P = value;
		}


		public decimal W { get; set; }

		[JsonIgnore]
		public decimal Weight
		{
			get => W;
			set => W = value;
		}


		public decimal Va { get; set; }

		[JsonIgnore]
		public decimal Value
		{
			get => Va;
			set => Va = value;
		}


		public decimal T1 { get; set; }

		[JsonIgnore]
		public decimal Tax1
		{
			get => T1;
			set => T1 = value;
		}


		public decimal T2 { get; set; }

		[JsonIgnore]
		public decimal Tax2
		{
			get => T2;
			set => T2 = value;
		}


		public bool Hd { get; set; }

		[JsonIgnore]
		public bool HasDocuments
		{
			get => Hd;
			set => Hd = value;
		}


		public decimal O { get; set; }

		[JsonIgnore]
		public decimal Original
		{
			get => O;
			set => O = value;
		}


		public decimal Q { get; set; }

		[JsonIgnore]
		public decimal Qh
		{
			get => Q;
			set => Q = value;
		}
	}

	public class TripPackage
	{
		public string P { get; set; } = "";

		[JsonIgnore]
		public string PackageType
		{
			get => P;
			set => P = value;
		}

		public decimal H { get; set; }

		[JsonIgnore]
		public decimal Height
		{
			get => H;
			set => H = value;
		}


		public decimal Wi { get; set; }

		[JsonIgnore]
		public decimal Width
		{
			get => Wi;
			set => Wi = value;
		}


		public decimal L { get; set; }

		[JsonIgnore]
		public decimal Length
		{
			get => L;
			set => L = value;
		}


		public decimal V { get; set; }

		[JsonIgnore]
		public decimal Volume
		{
			get => V;
			set => V = value;
		}


		public decimal Pi { get; set; }

		[JsonIgnore]
		public decimal Pieces
		{
			get => Pi;
			set => Pi = value;
		}


		public decimal W { get; set; }

		[JsonIgnore]
		public decimal Weight
		{
			get => W;
			set => W = value;
		}


		public decimal Va { get; set; }

		[JsonIgnore]
		public decimal Value
		{
			get => Va;
			set => Va = value;
		}


		public decimal T1 { get; set; }

		[JsonIgnore]
		public decimal Tax1
		{
			get => T1;
			set => T1 = value;
		}


		public decimal T2 { get; set; }

		[JsonIgnore]
		public decimal Tax2
		{
			get => T2;
			set => T2 = value;
		}


		public bool Hd { get; set; }

		[JsonIgnore]
		public bool HasDocuments
		{
			get => Hd;
			set => Hd = value;
		}


		public decimal O { get; set; }

		[JsonIgnore]
		public decimal Original
		{
			get => O;
			set => O = value;
		}


		public decimal Q { get; set; }

		[JsonIgnore]
		public decimal Qh
		{
			get => Q;
			set => Q = value;
		}


		public List<TripItem> I { get; set; } = new List<TripItem>();

		[JsonIgnore]
		public List<TripItem> Items
		{
			get => I;
			set => I = value;
		}

		public TripPackage()
		{
		}

		public TripPackage( TripPackage p )
		{
			PackageType = p.PackageType;
			Height = p.Height;
			Width = p.Width;
			Length = p.Length;
			Volume = p.Volume;
			Pieces = p.Pieces;
			Weight = p.Weight;
			Value = p.Value;
			Tax1 = p.Tax1;
			Tax2 = p.Tax2;
			HasDocuments = p.HasDocuments;
			Original = p.Original;
			Qh = p.Qh;
			Items = p.Items;
		}
	}
}