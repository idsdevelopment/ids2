﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class AddUpdateCustomer : ProgramData
	{
		public AddUpdateCustomer( string programName ) : base( programName )
		{
		}


		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public string S { get; set; } = "";

		[JsonIgnore]
		public string SuggestedLoginCode
		{
			get => S;
			set => S = value;
		}


		public string U { get; set; } = "";

		[JsonIgnore]
		public string UserName
		{
			get => U;
			set => U = value;
		}


		public string P { get; set; } = "";

		[JsonIgnore]
		public string Password
		{
			get => P;
			set => P = value;
		}


		public Company Co { get; set; } = new Company();

		[JsonIgnore]
		public Company Company
		{
			get => Co;
			set => Co = value;
		}


		public Company Bc { get; set; } = new Company();

		[JsonIgnore]
		public Company BillingCompany
		{
			get => Bc;
			set => Bc = value;
		}


		public Companies Cs { get; set; } = new Companies();

		[JsonIgnore]
		public Companies Companies
		{
			get => Cs;
			set => Cs = value;
		}
	}

	public class CustomerCodeList : List<string>
	{
		public CustomerCodeList()
		{
		}

		public CustomerCodeList( IEnumerable<string> cList )
		{
			AddRange( cList );
		}
	}

	public class AddCustomerCompany : ProgramData
	{
		public AddCustomerCompany( string programName ) : base( programName )
		{
		}


		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public Company Co { get; set; } = new Company();

		[JsonIgnore]
		public Company Company
		{
			get => Co;
			set => Co = value;
		}
	}

	public class DeleteCustomerCompany : ProgramData
	{
		public DeleteCustomerCompany( string programName ) : base( programName )
		{
		}

		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public Company Co { get; set; } = new Company();

		[JsonIgnore]
		public Company Company
		{
			get => Co;
			set => Co = value;
		}
	}

	public class UpdateCustomerCompany : ProgramData
	{
		public UpdateCustomerCompany( string programName ) : base( programName )
		{
		}

		public string C { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => C;
			set => C = value;
		}


		public Company Co { get; set; } = new Company();

		[JsonIgnore]
		public Company Company
		{
			get => Co;
			set => Co = value;
		}
	}

	public class CustomerLookup : PreFetch
	{
		public bool ByAccountNumber { get; set; }
	}

	public class CustomerLookupSummary : CompanyByAccountSummary
	{
		public bool BoolOption1 { get; set; }
	}

	public class CustomerLookupSummaryList : List<CustomerLookupSummary>
	{
		public CustomerLookupSummaryList()
		{
		}

		public CustomerLookupSummaryList( IEnumerable<CustomerLookupSummary> list )
		{
			AddRange( list );
		}
	}

	public class CustomerCompanies
	{
		public Companies C { get; set; } = new Companies();

		[JsonIgnore]
		public Companies Companies
		{
			get => C;
			set => C = value;
		}


		public string Cu { get; set; } = "";

		[JsonIgnore]
		public string CustomerCode
		{
			get => Cu;
			set => Cu = value;
		}
	}

	public class CustomerCompaniesList : List<CustomerCompanies>
	{
	}
}