﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global


namespace Protocol.Data
{
	public enum STATUS : sbyte
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		INVOICED,
		SCHEDULED,
		PAID,
		DELETED,

		//Must Always Be Last
		UNKNOWN
	}

	public enum STATUS1 : sbyte
	{
		UNSET,
		CLAIMED,
		UNDELIVERED,
		UNDELIVERED_CLAIM
	}

	// Bit Fields
	[Flags]
	public enum STATUS2 : sbyte
	{
		UNSET = 0,
		WAS_UNDELIVERABLE = 1,
		WAS_PICKED_UP = 1 << 1
	}

	public static class TripExtensions
	{
		//Must align to enum STATUS
		public static readonly STATUS[] StatusAsArray =
		{
			STATUS.UNSET,
			STATUS.NEW,
			STATUS.ACTIVE,
			STATUS.DISPATCHED,
			STATUS.PICKED_UP,
			STATUS.DELIVERED,
			STATUS.VERIFIED,
			STATUS.POSTED,
			STATUS.INVOICED,
			STATUS.SCHEDULED,
			STATUS.PAID,
			STATUS.DELETED,

			//Must Always Be Last
			STATUS.UNKNOWN
		};

		//Must align to enum STATUS
		public static readonly string[] StatusAsStringArray =
		{
			"UNSET",
			"NEW",
			"ACTIVE",
			"DISPATCHED",
			"PICKED UP",
			"DELIVERED",
			"VERIFIED",
			"POSTED",
			"INVOICED",
			"SCHEDULED",
			"PAID",
			"DELETED",

			//Must Always Be Last
			"UNKNOWN"
		};

		public static string AsString( this STATUS status )
		{
			if( ( status < STATUS.UNSET ) || ( status > STATUS.UNKNOWN ) )
				status = STATUS.UNKNOWN;

			return StatusAsStringArray[ (int)status ];
		}


		public static STATUS AsStatus( this string txt )
		{
			txt = txt.Trim().ToUpper();

			for( var S = STATUS.UNSET; S < STATUS.UNKNOWN; S++ )
			{
				if( StatusAsStringArray[ (int)S ] == txt )
					return S;
			}

			return STATUS.UNKNOWN;
		}

		public static string AsString( this STATUS1 status )
		{
			return status switch
			       {
				       STATUS1.UNSET       => "UNSET",
				       STATUS1.CLAIMED     => "CLAIMED",
				       STATUS1.UNDELIVERED => "UNDELIVERED",
				       _                   => "UNKNOWN"
			       };
		}
	}


	public class StatusRequest : List<STATUS>
	{
	}

	public class DeviceTripUpdate
	{
		public string p { get; set; } = "";

		[JsonIgnore]
		public string Program
		{
			get => p;
			set => p = value;
		}


		public STATUS St { get; set; } = STATUS.NEW;

		[JsonIgnore]
		public STATUS Status
		{
			get => St;
			set => St = value;
		}


		public STATUS1 St1 { get; set; } = STATUS1.UNSET;

		[JsonIgnore]
		public STATUS1 Status1
		{
			get => St1;
			set => St1 = value;
		}


		public string T { get; set; } = "";

		[JsonIgnore]
		public string TripId
		{
			get => T;
			set => T = value;
		}


		public DateTimeOffset Ut { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset UpdateTime
		{
			get => Ut;
			set => Ut = value;
		}


		public double Ulat { get; set; }

		[JsonIgnore]
		public double UpdateLatitude
		{
			get => Ulat;
			set => Ulat = value;
		}


		public double Ulng { get; set; }

		[JsonIgnore]
		public double UpdateLongitude
		{
			get => Ulng;
			set => Ulng = value;
		}


		public decimal P { get; set; }

		[JsonIgnore]
		public decimal Pieces
		{
			get => P;
			set => P = value;
		}


		public decimal W { get; set; }

		[JsonIgnore]
		public decimal Weight
		{
			get => W;
			set => W = value;
		}


		public string Pn { get; set; } = "";

		[JsonIgnore]
		public string PickupNotes
		{
			get => Pn;
			set => Pn = value;
		}


		public string Dn { get; set; } = "";

		[JsonIgnore]
		public string DeliveryNotes
		{
			get => Dn;
			set => Dn = value;
		}


		public string Bn { get; set; } = "";

		[JsonIgnore]
		public string BillingNotes
		{
			get => Bn;
			set => Bn = value;
		}



		public string U { get; set; } = "";
		[JsonIgnore]
		public string UndeliverableNotes { get => U; set => U = value; }



		public Signature S { get; set; } = new Signature();

		[JsonIgnore]
		public Signature Signature
		{
			get => S;
			set => S = value;
		}


		public string R { get; set; } = "";

		[JsonIgnore]
		public string Reference
		{
			get => R;
			set => R = value;
		}


		public string Pd { get; set; } = "";

		[JsonIgnore]
		public string PopPod
		{
			get => Pd;
			set => Pd = value;
		}

		public DeviceTripUpdate( string program )
		{
			Program = program;
		}
	}

	public class TripUpdate
	{
		public string Pgn { get; set; } = "";

		[JsonIgnore]
		public string Program
		{
			get => Pgn;
			set => Pgn = value;
		}

		public string L { get; set; } = "";

		[JsonIgnore]
		public string Location
		{
			get => L;
			set => L = value;
		}


		public bool Brd { get; set; }

		[JsonIgnore]
		public bool BroadcastToDriver
		{
			get => Brd;
			set => Brd = value;
		}


		public bool Brd1 { get; set; }

		[JsonIgnore]
		public bool BroadcastToDispatchBoard
		{
			get => Brd1;
			set => Brd1 = value;
		}


		public bool Brd2 { get; set; }

		[JsonIgnore]
		public bool BroadcastToDriverBoard
		{
			get => Brd2;
			set => Brd2 = value;
		}


		public string Ti { get; set; } = "";

		[JsonIgnore]
		public string TripId
		{
			get => Ti;
			set => Ti = value;
		}

		public string Ci { get; set; } = "";

		[JsonIgnore]
		public string ContainerId
		{
			get => Ci;
			set => Ci = value;
		}

		public string Bc { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => Bc;
			set => Bc = value;
		}


		public string Ai { get; set; } = "";

		[JsonIgnore]
		public string AccountId
		{
			get => Ai;
			set => Ai = value;
		}


		public STATUS S1 { get; set; } = STATUS.NEW;

		[JsonIgnore]
		public STATUS Status1
		{
			get => S1;
			set => S1 = value;
		}


		public STATUS1 S2 { get; set; } = STATUS1.UNSET;

		[JsonIgnore]
		public STATUS1 Status2
		{
			get => S2;
			set => S2 = value;
		}


		public STATUS2 S3 { get; set; } = STATUS2.UNSET;

		[JsonIgnore]
		public STATUS2 Status3
		{
			get => S3;
			set => S3 = value;
		}


		public string R { get; set; } = "";

		[JsonIgnore]
		public string Reference
		{
			get => R;
			set => R = value;
		}


		public DateTimeOffset Ct { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset CallTime
		{
			get => Ct;
			set => Ct = value;
		}


		public string Cp { get; set; } = "";

		[JsonIgnore]
		public string CallerPhone
		{
			get => Cp;
			set => Cp = value;
		}


		public string Ce { get; set; } = "";

		[JsonIgnore]
		public string CallerEmail
		{
			get => Ce;
			set => Ce = value;
		}


		public DateTimeOffset Pt { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset PickupTime
		{
			get => Pt;
			set => Pt = value;
		}


		public double Plat { get; set; }

		[JsonIgnore]
		public double PickupLatitude
		{
			get => Plat;
			set => Plat = value;
		}


		public double Plng { get; set; }

		[JsonIgnore]
		public double PickupLongitude
		{
			get => Plng;
			set => Plng = value;
		}


		public DateTimeOffset Dt { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset DeliveryTime
		{
			get => Dt;
			set => Dt = value;
		}


		public double Dlat { get; set; }

		[JsonIgnore]
		public double DeliveryLatitude
		{
			get => Dlat;
			set => Dlat = value;
		}


		public double Dlng { get; set; }

		[JsonIgnore]
		public double DeliveryLongitude
		{
			get => Dlng;
			set => Dlng = value;
		}


		public DateTimeOffset Vt { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset VerifiedTime
		{
			get => Vt;
			set => Vt = value;
		}


		public double Vlat { get; set; }

		[JsonIgnore]
		public double VerifiedLatitude
		{
			get => Vlat;
			set => Vlat = value;
		}


		public double Vlng { get; set; }

		[JsonIgnore]
		public double VerifiedLongitude
		{
			get => Vlng;
			set => Vlng = value;
		}


		public DateTimeOffset Clt { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset ClaimTime
		{
			get => Clt;
			set => Clt = value;
		}


		public double Clat { get; set; }

		[JsonIgnore]
		public double ClaimLatitude
		{
			get => Clat;
			set => Clat = value;
		}


		public double Clng { get; set; }

		[JsonIgnore]
		public double ClaimLongitude
		{
			get => Clng;
			set => Clng = value;
		}


		public DateTimeOffset Rt { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset ReadyTime
		{
			get => Rt;
			set => Rt = value;
		}


		public DateTimeOffset Du { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset DueTime
		{
			get => Du;
			set => Du = value;
		}


		public string Dc { get; set; } = "";

		[JsonIgnore]
		public string Driver
		{
			get => Dc;
			set => Dc = value;
		}


		public decimal W { get; set; }

		[JsonIgnore]
		public decimal Weight
		{
			get => W;
			set => W = value;
		}


		public decimal P { get; set; }

		[JsonIgnore]
		public decimal Pieces
		{
			get => P;
			set => P = value;
		}


		public string Py { get; set; } = "";

		[JsonIgnore]
		public string PackageType
		{
			get => Py;
			set => Py = value;
		}


		public string Sv { get; set; } = "";

		[JsonIgnore]
		public string ServiceLevel
		{
			get => Sv;
			set => Sv = value;
		}


		public decimal V { get; set; }

		[JsonIgnore]
		public decimal Volume
		{
			get => V;
			set => V = value;
		}


		public string M { get; set; } = "";

		[JsonIgnore]
		public string Measurement
		{
			get => M;
			set => M = value;
		}


		public decimal Op { get; set; }

		[JsonIgnore]
		public decimal OriginalPieceCount
		{
			get => Op;
			set => Op = value;
		}


		public byte[] Mp { get; set; } = new byte[ 0 ];

		[JsonIgnore]
		public byte[] MissingPieces
		{
			get => Mp;
			set => Mp = value;
		}


		public bool D { get; set; }

		[JsonIgnore]
		public bool DangerousGoods
		{
			get => D;
			set => D = value;
		}


		public string Uc { get; set; } = "";

		[JsonIgnore]

		// ReSharper disable once InconsistentNaming
		public string UNClass
		{
			get => Uc;
			set => Uc = value;
		}


		public bool Hd { get; set; }

		[JsonIgnore]
		public bool HasDocuments
		{
			get => Hd;
			set => Hd = value;
		}


		public string B { get; set; } = "";

		[JsonIgnore]
		public string Board
		{
			get => B;
			set => B = value;
		}


		public string P1 { get; set; } = "";

		[JsonIgnore]

		// ReSharper disable once InconsistentNaming
		public string POP
		{
			get => P1;
			set => P1 = value;
		}


		public string P2 { get; set; } = "";

		[JsonIgnore]

		// ReSharper disable once InconsistentNaming
		public string POD
		{
			get => P2;
			set => P2 = value;
		}


		public string Pa { get; set; } = "";

		[JsonIgnore]
		public string PickupAccountId
		{
			get => Pa;
			set => Pa = value;
		}


		public string Pn { get; set; } = "";

		[JsonIgnore]
		public string PickupCompanyName
		{
			get => Pn;
			set => Pn = value;
		}


		public string Pb { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressBarcode
		{
			get => Pb;
			set => Pb = value;
		}


		public string Ppb { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressPostalBarcode
		{
			get => Ppb;
			set => Ppb = value;
		}


		public string Ps { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressSuite
		{
			get => Ps;
			set => Ps = value;
		}


		public string Pa1 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressAddressLine1
		{
			get => Pa1;
			set => Pa1 = value;
		}


		public string Pa2 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressAddressLine2
		{
			get => Pa2;
			set => Pa2 = value;
		}


		public string Pv { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressVicinity
		{
			get => Pv;
			set => Pv = value;
		}


		public string Pc { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressCity
		{
			get => Pc;
			set => Pc = value;
		}


		public string Pr { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressRegion
		{
			get => Pr;
			set => Pr = value;
		}


		public string Ppc { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressPostalCode
		{
			get => Ppc;
			set => Ppc = value;
		}


		public string Pac { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressCountry
		{
			get => Pac;
			set => Pac = value;
		}


		public string Pcc { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressCountryCode
		{
			get => Pcc;
			set => Pcc = value;
		}


		public string Pp { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressPhone
		{
			get => Pp;
			set => Pp = value;
		}


		public string Pp1 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressPhone1
		{
			get => Pp1;
			set => Pp1 = value;
		}


		public string Pm { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressMobile
		{
			get => Pm;
			set => Pm = value;
		}


		public string Pm1 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressMobile1
		{
			get => Pm1;
			set => Pm1 = value;
		}


		public string Pf { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressFax
		{
			get => Pf;
			set => Pf = value;
		}


		public string Pe { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressEmailAddress
		{
			get => Pe;
			set => Pe = value;
		}


		public string Pe1 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressEmailAddress1
		{
			get => Pe1;
			set => Pe1 = value;
		}


		public string Pe2 { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressEmailAddress2
		{
			get => Pe2;
			set => Pe2 = value;
		}

		public string Puc { get; set; } = "";

		[JsonIgnore]
		public string PickupContact
		{
			get => Puc;
			set => Puc = value;
		}


		public decimal Plt { get; set; }

		[JsonIgnore]
		public decimal PickupAddressLatitude
		{
			get => Plt;
			set => Plt = value;
		}


		public decimal Plg { get; set; }

		[JsonIgnore]
		public decimal PickupAddressLongitude
		{
			get => Plg;
			set => Plg = value;
		}


		public string Pan { get; set; } = "";

		[JsonIgnore]
		public string PickupAddressNotes
		{
			get => Pan;
			set => Pan = value;
		}


		public string Pnt { get; set; } = "";

		[JsonIgnore]
		public string PickupNotes
		{
			get => Pnt;
			set => Pnt = value;
		}


		public string Da { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAccountId
		{
			get => Da;
			set => Da = value;
		}


		public string Dcn { get; set; } = "";

		[JsonIgnore]
		public string DeliveryCompanyName
		{
			get => Dcn;
			set => Dcn = value;
		}


		public string Db { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressBarcode
		{
			get => Db;
			set => Db = value;
		}


		public string Dpb { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressPostalBarcode
		{
			get => Dpb;
			set => Dpb = value;
		}


		public string Ds { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressSuite
		{
			get => Ds;
			set => Ds = value;
		}


		public string Da1 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressAddressLine1
		{
			get => Da1;
			set => Da1 = value;
		}


		public string Da2 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressAddressLine2
		{
			get => Da2;
			set => Da2 = value;
		}


		public string Dv { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressVicinity
		{
			get => Dv;
			set => Dv = value;
		}


		public string Dac { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressCity
		{
			get => Dac;
			set => Dac = value;
		}


		public string Dr { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressRegion
		{
			get => Dr;
			set => Dr = value;
		}


		public string Dpc { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressPostalCode
		{
			get => Dpc;
			set => Dpc = value;
		}


		public string DaC { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressCountry
		{
			get => DaC;
			set => DaC = value;
		}


		public string Dcc { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressCountryCode
		{
			get => Dcc;
			set => Dcc = value;
		}


		public string Dp { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressPhone
		{
			get => Dp;
			set => Dp = value;
		}


		public string Dp1 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressPhone1
		{
			get => Dp1;
			set => Dp1 = value;
		}


		public string Dm { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressMobile
		{
			get => Dm;
			set => Dm = value;
		}


		public string Dm1 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressMobile1
		{
			get => Dm1;
			set => Dm1 = value;
		}


		public string Df { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressFax
		{
			get => Df;
			set => Df = value;
		}


		public string De { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressEmailAddress
		{
			get => De;
			set => De = value;
		}


		public string De1 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressEmailAddress1
		{
			get => De1;
			set => De1 = value;
		}


		public string De2 { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressEmailAddress2
		{
			get => De2;
			set => De2 = value;
		}


		public string Duc { get; set; } = "";

		[JsonIgnore]
		public string DeliveryContact
		{
			get => Duc;
			set => Duc = value;
		}


		public decimal DLt { get; set; }

		[JsonIgnore]
		public decimal DeliveryAddressLatitude
		{
			get => DLt;
			set => DLt = value;
		}


		public decimal Dlg { get; set; }

		[JsonIgnore]
		public decimal DeliveryAddressLongitude
		{
			get => Dlg;
			set => Dlg = value;
		}


		public string Dn { get; set; } = "";

		[JsonIgnore]
		public string DeliveryAddressNotes
		{
			get => Dn;
			set => Dn = value;
		}


		public string Dnt { get; set; } = "";

		[JsonIgnore]
		public string DeliveryNotes
		{
			get => Dnt;
			set => Dnt = value;
		}


		public string Ba { get; set; } = "";

		[JsonIgnore]
		public string BillingAccountId
		{
			get => Ba;
			set => Ba = value;
		}


		public string Bcn { get; set; } = "";

		[JsonIgnore]
		public string BillingCompanyName
		{
			get => Bcn;
			set => Bcn = value;
		}


		public string Bb { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressBarcode
		{
			get => Bb;
			set => Bb = value;
		}


		public string Bpb { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressPostalBarcode
		{
			get => Bpb;
			set => Bpb = value;
		}


		public string Bs { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressSuite
		{
			get => Bs;
			set => Bs = value;
		}


		public string Ba1 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressAddressLine1
		{
			get => Ba1;
			set => Ba1 = value;
		}


		public string Ba2 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressAddressLine2
		{
			get => Ba2;
			set => Ba2 = value;
		}


		public string Bv { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressVicinity
		{
			get => Bv;
			set => Bv = value;
		}


		public string Bac { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressCity
		{
			get => Bac;
			set => Bac = value;
		}


		public string Br1 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressRegion
		{
			get => Br1;
			set => Br1 = value;
		}


		public string Bpc { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressPostalCode
		{
			get => Bpc;
			set => Bpc = value;
		}


		public string BaC { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressCountry
		{
			get => BaC;
			set => BaC = value;
		}


		public string Bcc { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressCountryCode
		{
			get => Bcc;
			set => Bcc = value;
		}


		public string Bp { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressPhone
		{
			get => Bp;
			set => Bp = value;
		}


		public string Bp1 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressPhone1
		{
			get => Bp1;
			set => Bp1 = value;
		}


		public string Bm { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressMobile
		{
			get => Bm;
			set => Bm = value;
		}


		public string Bm1 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressMobile1
		{
			get => Bm1;
			set => Bm1 = value;
		}


		public string Bf { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressFax
		{
			get => Bf;
			set => Bf = value;
		}


		public string Be { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressEmailAddress
		{
			get => Be;
			set => Be = value;
		}


		public string Be1 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressEmailAddress1
		{
			get => Be1;
			set => Be1 = value;
		}


		public string Be2 { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressEmailAddress2
		{
			get => Be2;
			set => Be2 = value;
		}


		public string Buc { get; set; } = "";

		[JsonIgnore]
		public string BillingContact
		{
			get => Buc;
			set => Buc = value;
		}


		public decimal BLt { get; set; }

		[JsonIgnore]
		public decimal BillingAddressLatitude
		{
			get => BLt;
			set => BLt = value;
		}


		public decimal Blg { get; set; }

		[JsonIgnore]
		public decimal BillingAddressLongitude
		{
			get => Blg;
			set => Blg = value;
		}


		public string Bn { get; set; } = "";

		[JsonIgnore]
		public string BillingAddressNotes
		{
			get => Bn;
			set => Bn = value;
		}


		public string Bnt { get; set; } = "";

		[JsonIgnore]
		public string BillingNotes
		{
			get => Bnt;
			set => Bnt = value;
		}


		public bool Rde { get; set; }

		[JsonIgnore]
		public bool ReceivedByDevice
		{
			get => Rde;
			set => Rde = value;
		}


		public bool Rdr { get; set; }

		[JsonIgnore]
		public bool ReadByDriver
		{
			get => Rdr;
			set => Rdr = value;
		}


		public string Cz { get; set; } = "";

		[JsonIgnore]
		public string CurrentZone
		{
			get => Cz;
			set => Cz = value;
		}


		public string Pz { get; set; } = "";

		[JsonIgnore]
		public string PickupZone
		{
			get => Pz;
			set => Pz = value;
		}


		public string Dz { get; set; } = "";

		[JsonIgnore]
		public string DeliveryZone
		{
			get => Dz;
			set => Dz = value;
		}

		public List<TripCharge> Tc { get; set; } = new List<TripCharge>();

		[JsonIgnore]
		public List<TripCharge> TripCharges
		{
			get => Tc;
			set => Tc = value;
		}


		public List<TripPackage> Pty { get; set; } = new List<TripPackage>();

		[JsonIgnore]
		public List<TripPackage> Packages
		{
			get => Pty;
			set => Pty = value;
		}


		public string Cn { get; set; } = "";

		[JsonIgnore]
		public string CallerName
		{
			get => Cn;
			set => Cn = value;
		}

		public bool Iq { get; set; }

		[JsonIgnore]
		public bool IsQuote
		{
			get => Iq;
			set => Iq = value;
		}

		public string Un { get; set; } = "";

		[JsonIgnore]
		public string UndeliverableNotes
		{
			get => Un;
			set => Un = value;
		}


		public string Ex { get; set; } = "";

		[JsonIgnore]
		public string ExtensionAsJson
		{
			get => Ex;
			set => Ex = value;
		}

		public DateTime Lm { get; set; } = DateTime.MinValue;

		[JsonIgnore]
		public DateTime LastModified
		{
			get => Lm;
			set => Lm = value;
		}


		public TripUpdate( string program )
		{
			Program = program;
		}


		public TripUpdate()
		{
		}

		public TripUpdate( TripUpdate t )
		{
			TripId             = t.TripId;
			Location           = t.Location;
			ContainerId        = t.ContainerId;
			Barcode            = t.Barcode;
			AccountId          = t.AccountId;
			Status1            = t.Status1;
			Status2            = t.Status2;
			Status3            = t.Status3;
			Reference          = t.Reference;
			CallTime           = t.CallTime;
			CallerPhone        = t.CallerPhone;
			CallerEmail        = t.CallerEmail;
			ReadyTime          = t.ReadyTime;
			DueTime            = t.DueTime;
			Driver             = t.Driver;
			Weight             = t.Weight;
			Pieces             = t.Pieces;
			PackageType        = t.PackageType;
			ServiceLevel       = t.ServiceLevel;
			Volume             = t.Volume;
			Measurement        = t.Measurement;
			OriginalPieceCount = t.OriginalPieceCount;
			MissingPieces      = t.MissingPieces;
			DangerousGoods     = t.DangerousGoods;
			UNClass            = t.UNClass;
			HasDocuments       = t.HasDocuments;
			Board              = t.Board;

			BillingCompanyName          = t.BillingCompanyName;
			BillingAddressBarcode       = t.BillingAddressBarcode;
			BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
			BillingAddressSuite         = t.BillingAddressSuite;
			BillingAddressAddressLine1  = t.BillingAddressAddressLine1;
			BillingAddressAddressLine2  = t.BillingAddressAddressLine2;
			BillingAddressVicinity      = t.BillingAddressVicinity;
			BillingAddressCity          = t.BillingAddressCity;
			BillingAddressRegion        = t.BillingAddressRegion;
			BillingAddressPostalCode    = t.BillingAddressPostalCode;
			BillingAddressCountry       = t.BillingAddressCountry;
			BillingAddressCountryCode   = t.BillingAddressCountryCode;
			BillingAddressPhone         = t.BillingAddressPhone;
			BillingAddressPhone1        = t.BillingAddressPhone1;
			BillingAddressMobile        = t.BillingAddressMobile;
			BillingAddressMobile1       = t.BillingAddressMobile1;
			BillingAddressFax           = t.BillingAddressFax;
			BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
			BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
			BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
			BillingContact              = t.BillingContact;
			BillingAddressLatitude      = t.BillingAddressLatitude;
			BillingAddressLongitude     = t.BillingAddressLongitude;
			BillingAddressNotes         = t.BillingAddressNotes;
			BillingNotes                = t.BillingNotes;

			PickupCompanyName          = t.PickupCompanyName;
			PickupAddressBarcode       = t.PickupAddressBarcode;
			PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
			PickupAddressSuite         = t.PickupAddressSuite;
			PickupAddressAddressLine1  = t.PickupAddressAddressLine1;
			PickupAddressAddressLine2  = t.PickupAddressAddressLine2;
			PickupAddressVicinity      = t.PickupAddressVicinity;
			PickupAddressCity          = t.PickupAddressCity;
			PickupAddressRegion        = t.PickupAddressRegion;
			PickupAddressPostalCode    = t.PickupAddressPostalCode;
			PickupAddressCountry       = t.PickupAddressCountry;
			PickupAddressCountryCode   = t.PickupAddressCountryCode;
			PickupAddressPhone         = t.PickupAddressPhone;
			PickupAddressPhone1        = t.PickupAddressPhone1;
			PickupAddressMobile        = t.PickupAddressMobile;
			PickupAddressMobile1       = t.PickupAddressMobile1;
			PickupAddressFax           = t.PickupAddressFax;
			PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
			PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
			PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
			PickupContact              = t.PickupContact;
			PickupAddressLatitude      = t.PickupAddressLatitude;
			PickupAddressLongitude     = t.PickupAddressLongitude;
			PickupAddressNotes         = t.PickupAddressNotes;
			PickupNotes                = t.PickupNotes;

			DeliveryCompanyName          = t.DeliveryCompanyName;
			DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
			DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
			DeliveryAddressSuite         = t.DeliveryAddressSuite;
			DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1;
			DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2;
			DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
			DeliveryAddressCity          = t.DeliveryAddressCity;
			DeliveryAddressRegion        = t.DeliveryAddressRegion;
			DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
			DeliveryAddressCountry       = t.DeliveryAddressCountry;
			DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
			DeliveryAddressPhone         = t.DeliveryAddressPhone;
			DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
			DeliveryAddressMobile        = t.DeliveryAddressMobile;
			DeliveryAddressMobile1       = t.DeliveryAddressMobile1;
			DeliveryAddressFax           = t.DeliveryAddressFax;
			DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
			DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
			DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
			DeliveryContact              = t.DeliveryContact;

			DeliveryAddressLatitude  = t.DeliveryAddressLatitude;
			DeliveryAddressLongitude = t.DeliveryAddressLongitude;
			DeliveryAddressNotes     = t.DeliveryAddressNotes;
			DeliveryNotes            = t.DeliveryNotes;

			ReceivedByDevice = t.ReceivedByDevice;
			ReadByDriver     = t.ReadByDriver;

			CurrentZone  = t.CurrentZone;
			PickupZone   = t.PickupZone;
			DeliveryZone = t.DeliveryZone;

			PickupTime      = t.PickupTime;
			PickupLatitude  = t.PickupLatitude;
			PickupLongitude = t.PickupLongitude;

			DeliveryTime      = t.DeliveryTime;
			DeliveryLatitude  = t.DeliveryLatitude;
			DeliveryLongitude = t.DeliveryLongitude;

			VerifiedTime      = t.VerifiedTime;
			VerifiedLatitude  = t.VerifiedLatitude;
			VerifiedLongitude = t.VerifiedLongitude;

			ClaimTime      = t.ClaimTime;
			ClaimLatitude  = t.ClaimLatitude;
			ClaimLongitude = t.ClaimLongitude;

			TripCharges = t.TripCharges;
			Packages    = t.Packages;

			CallerName         = t.CallerName;
			IsQuote            = t.IsQuote;
			UndeliverableNotes = t.UndeliverableNotes;
			LastModified       = t.LastModified;
		}
	}


	public class TripUpdateList
	{
		public List<TripUpdate> Trips { get; set; } = new List<TripUpdate>();

		public string Pg { get; set; } = "";

		[JsonIgnore]
		public string Program
		{
			get => Pg;
			set => Pg = value;
		}
	}

	public class Trip : TripUpdate
	{
		public bool O { get; set; }

		[JsonIgnore]
		public bool Ok
		{
			get => O;
			set => O = value;
		}

		public List<Signature> S { get; set; } = new List<Signature>();

		[JsonIgnore]
		public List<Signature> Signatures
		{
			get => S;
			set => S = value;
		}


		public Trip()
		{
		}

		public Trip( Trip t ) : base( t )
		{
			LastModified = t.LastModified;
		}

		public Trip( TripUpdate t ) : base( t )
		{
		}
	}


	public class TripList : List<Trip>
	{
	}

	public class SearchTripsByStatus
	{
		public string TripId { get; set; } = "";
		public STATUS StartStatus { get; set; }
		public STATUS EndStatus { get; set; }
	}

	public class SearchTrips : SearchTripsByStatus
	{
		public DateTimeOffset FromDate { get; set; } = DateTimeOffset.Now;
		public DateTimeOffset ToDate { get; set; } = DateTimeOffset.Now;
		public bool ByReference { get; set; }
		public string Reference { get; set; } = "";
		public bool ByTripId { get; set; }
		public bool IncludeCharges { get; set; }

		public string CompanyCode { get; set; }
		public string SelectedCompanyPUAddressName { get; set; }
		public string SelectedCompanyDelAddressName { get; set; }
		public string PackageType { get; set; }
		public string ServiceLevel { get; set; }
	}

	public class GetTrip
	{
		public string T { get; set; } = "";

		[JsonIgnore]
		public string TripId
		{
			get => T;
			set => T = value;
		}

		public bool S { get; set; }

		[JsonIgnore]
		public bool Signatures
		{
			get => S;
			set => S = value;
		}
	}
}