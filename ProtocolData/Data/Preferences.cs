﻿#nullable enable

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Preference
	{
		public const byte BOOL      = 0,
		                  STRING    = 1,
		                  INT       = 2,
		                  DECIMAL   = 3,
		                  DATE_TIME = 4;


		public int I { get; set; }

		[JsonIgnore]
		public int Id
		{
			get => I;
			set => I = value;
		}


		public byte Dt { get; set; }

		[JsonIgnore]
		public byte DataType
		{
			get => Dt;
			set => Dt = value;
		}


		public string De { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => De;
			set => De = value;
		}


		public string Dp { get; set; } = "";

		[JsonIgnore]
		public string DevicePreference
		{
			get => Dp;
			set => Dp = value;
		}


		public bool Io { get; set; }

		[JsonIgnore]
		public bool IdsOnly
		{
			get => Io;
			set => Io = value;
		}


		public bool E { get; set; }

		[JsonIgnore]
		public bool Enabled
		{
			get => E;
			set => E = value;
		}


		public int Do { get; set; }

		[JsonIgnore]
		public int DisplayOrder
		{
			get => Do;
			set => Do = value;
		}


		public string Sv { get; set; } = "";

		[JsonIgnore]
		public string StringValue
		{
			get => Sv;
			set => Sv = value;
		}


		public int Iv { get; set; }

		[JsonIgnore]
		public int IntValue
		{
			get => Iv;
			set => Iv = value;
		}


		public decimal Dv { get; set; }

		[JsonIgnore]
		public decimal DecimalValue
		{
			get => Dv;
			set => Dv = value;
		}


		public DateTimeOffset Tv { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset DateTimeValue
		{
			get => Tv;
			set => Tv = value;
		}

		public Preference()
		{
		}

		public Preference( Preference p )
		{
			Id               = p.Id;
			DataType         = p.DataType;
			Description      = p.Description;
			DevicePreference = p.DevicePreference;
			IdsOnly          = p.IdsOnly;
			Enabled          = p.Enabled;
			DisplayOrder     = p.DisplayOrder;
			StringValue      = p.StringValue;
			IntValue         = p.IntValue;
			DecimalValue     = p.DecimalValue;
			DateTimeValue    = p.DateTimeValue;
		}
	}

	public class Preferences : List<Preference>
	{
	}

	public class DevicePreferences
	{
		public bool Pw { get; set; }

		[JsonIgnore]
		public bool EditPiecesWeight
		{
			get => Pw;
			set => Pw = value;
		}


		public bool Er { get; set; }

		[JsonIgnore]
		public bool EditReference
		{
			get => Er;
			set => Er = value;
		}


		public bool L { get; set; }

		[JsonIgnore]
		public bool ByLocation
		{
			get => L;
			set => L = value;
		}


		public bool P { get; set; }

		[JsonIgnore]
		public bool ByPiece
		{
			get => P;
			set => P = value;
		}


		public bool M { get; set; }

		[JsonIgnore]
		public bool AllowManualBarcodeInput
		{
			get => M;
			set => M = value;
		}

		public bool U { get; set; }

		[JsonIgnore]
		public bool AllowUndeliverable
		{
			get => U;
			set => U = value;
		}

		public bool B { get; set; }

		[JsonIgnore]
		public bool UndeliverableBackToDispatch
		{
			get => B;
			set => B = value;
		}


		public bool D { get; set; }

		[JsonIgnore]
		public bool DeleteCannotPickup
		{
			get => D;
			set => D = value;
		}
	}


	public class ClientPreferences
	{
	}
}