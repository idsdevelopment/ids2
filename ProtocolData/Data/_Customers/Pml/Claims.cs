﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace Protocol.Data._Customers.Pml
{
	public class ClaimTrips
	{
		public string P { get; set; } = "";

		[JsonIgnore]
		public string ProgramName
		{
			get => P;
			set => P = value;
		}

		public string U { get; set; } = "";

		[JsonIgnore]
		public string UserName
		{
			get => U;
			set => U = value;
		}

		public List<string> T { get; set; } = new List<string>();

		[JsonIgnore]
		public List<string> TripIds
		{
			get => T;
			set => T = value;
		}

		public string C { get; set; } = "";

		[JsonIgnore]
		public string Cage
		{
			get => C;
			set => C = value;
		}


		public DateTimeOffset Ct { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset ClaimTime
		{
			get => Ct;
			set => Ct = value;
		}


		public double La { get; set; }

		[JsonIgnore]
		public double Latitude
		{
			get => La;
			set => La = value;
		}


		public double Lo { get; set; }

		[JsonIgnore]
		public double Longitude
		{
			get => Lo;
			set => Lo = value;
		}
	}

	public class ClaimSatchels : ClaimTrips
	{
		[JsonIgnore]
		public List<string> SatchelIds
		{
			get => TripIds;
			set => TripIds = value;
		}

		public Signature S { get; set; } = new Signature();

		[JsonIgnore]
		public Signature Signature
		{
			get => S;
			set => S = value;
		}

		public string Po { get; set; } = "";

		[JsonIgnore]
		public string POP
		{
			get => Po;
			set => Po = value;
		}
	}
}