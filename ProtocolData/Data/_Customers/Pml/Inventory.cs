﻿#nullable enable

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data._Customers.Pml
{
	public class PmlInventory : Inventory
	{
		public PmlInventory()
		{
		}

		public PmlInventory( Inventory inv ) : base( inv )
		{
		}
	}

	public class PmlInventoryList : List<PmlInventory>
	{
	}

	public class PmlFindSince
	{
		public bool A { get; set; }

		[JsonIgnore]
		public bool AllZones
		{
			get => A;
			set => A = value;
		}


		public string Z { get; set; } = "";

		[JsonIgnore]
		public string Zones
		{
			get => Z;
			set => Z = value;
		}


		public STATUS Fs { get; set; } = STATUS.UNSET;

		[JsonIgnore]
		public STATUS FromStatus
		{
			get => Fs;
			set => Fs = value;
		}

		public STATUS Ts { get; set; } = STATUS.UNSET;

		[JsonIgnore]
		public STATUS ToStatus
		{
			get => Ts;
			set => Ts = value;
		}


		public string V { get; set; } = "";

		[JsonIgnore]
		public string ServiceLevels
		{
			get => V;
			set => V = value;
		}


		public long S { get; set; }

		[JsonIgnore]
		public long SinceSeconds
		{
			get => S;
			set => S = value;
		}
	}
}