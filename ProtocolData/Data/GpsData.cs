﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class GpsPoint
	{
		public double La { get; set; }

		[JsonIgnore]
		public double Latitude
		{
			get => La;
			set => La = value;
		}


		public double Lo { get; set; }

		[JsonIgnore]
		public double Longitude
		{
			get => Lo;
			set => Lo = value;
		}


		public DateTimeOffset L { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset LocalDateTime
		{
			get => L;
			set => L = value;
		}
	}

	public class GpsPoints : List<GpsPoint>
	{
	}
}