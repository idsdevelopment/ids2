﻿using System;

namespace Protocol.Data
{
	public static class Time
	{
		public static readonly TimeSpan MinDayTimeSpan = new TimeSpan( 0, 0, 0, 0, 0 );
		public static readonly TimeSpan MaxDayTimeSpan = new TimeSpan( 0, 23, 59, 59, 999 );
	}
}