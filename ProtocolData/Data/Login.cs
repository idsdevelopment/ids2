﻿using System;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class SignOut
	{
		public uint H { get; set; }
		public byte M { get; set; }
		public byte S { get; set; }

		[JsonIgnore]
		public uint Hours
		{
			get => H;
			set => H = value;
		}

		[JsonIgnore]
		public byte Minutes
		{
			get => M;
			set => M = value;
		}

		[JsonIgnore]
		public byte Seconds
		{
			get => S;
			set => S = value;
		}

		[JsonIgnore]
		public TimeSpan Time
		{
			set
			{
				H = (uint)value.Hours;
				M = (byte)value.Minutes;
				S = (byte)value.Seconds;
			}
		}
	}
}