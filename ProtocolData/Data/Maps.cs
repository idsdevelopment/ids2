﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Distances : List<double>
	{
		[JsonIgnore]
		public List<double> AsKilometers => this;

		[JsonIgnore]
		public List<double> AsMiles
		{
			get { return this.Select( distance => distance * 0.621371 ).ToList(); }
		}
	}
}