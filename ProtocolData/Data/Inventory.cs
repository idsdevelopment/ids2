﻿#nullable enable

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Inventory
	{
		public string I { get; set; } = "";

		[JsonIgnore]
		public string InventoryCode
		{
			get => I;
			set => I = value;
		}


		public string B { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => B;
			set => B = value;
		}


		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}


		public decimal Pq { get; set; }

		[JsonIgnore]
		public decimal PackageQuantity
		{
			get => Pq;
			set => Pq = value;
		}


		public override bool Equals( object obj )
		{
			if( obj is Inventory Inventory )
			{
				return ( InventoryCode == Inventory.InventoryCode ) && ( Barcode == Inventory.Barcode ) && ( Description == Inventory.Description )
				       && ( PackageQuantity == Inventory.PackageQuantity );
			}

			return false;
		}

		public override int GetHashCode()
		{
			// ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
			return base.GetHashCode();
		}

		public Inventory()
		{
		}

		public Inventory( Inventory i )
		{
			InventoryCode   = i.InventoryCode;
			Barcode         = i.Barcode;
			Description     = i.Description;
			PackageQuantity = i.PackageQuantity;
		}
	}

	public class InventoryList : List<Inventory>
	{
	}


	public class InventoryBarcode
	{
		public bool F { get; set; }

		[JsonIgnore]
		public bool Found
		{
			get => F;
			set => F = value;
		}


		public string B { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => B;
			set => B = value;
		}


		public string O { get; set; } = "";

		[JsonIgnore]
		public string Owner
		{
			get => O;
			set => O = value;
		}


		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}
	}
}