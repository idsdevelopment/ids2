﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Messaging
	{
		public const string DRIVERS_BOARD  = "DRIVERS_BOARD",
		                    DISPATCH_BOARD = "DISPATCH_BOARD",
		                    DRIVERS        = "DRIVERS";
	}


	public class TripUpdateMessage
	{
		[Flags]
		public enum DEVICE_STATUS : ushort
		{
			UNSET,
			RECEIVED_BY_DEVICE = 1,
			READ_BY_DRIVER     = 1 << 1
		}

		public enum UPDATE
		{
			UNSET,
			TRIP_STATUS,
			DEVICE_STATUS,
			TRIP,
			REMOVE
		}


		public UPDATE A { get; set; } = UPDATE.UNSET;

		[JsonIgnore]
		public UPDATE Action
		{
			get => A;
			set => A = value;
		}


		public DEVICE_STATUS Ds { get; set; } = DEVICE_STATUS.UNSET;

		[JsonIgnore]
		public DEVICE_STATUS DeviceStatus
		{
			get => Ds;
			set => Ds = value;
		}

		[JsonIgnore]
		public bool ReceivedByDevice
		{
			get => ( Ds & DEVICE_STATUS.RECEIVED_BY_DEVICE ) != 0;
			set => Ds = value ? Ds | DEVICE_STATUS.RECEIVED_BY_DEVICE : Ds & ~DEVICE_STATUS.RECEIVED_BY_DEVICE;
		}


		[JsonIgnore]
		public bool ReadByDriver
		{
			get => ( Ds & DEVICE_STATUS.READ_BY_DRIVER ) != 0;
			set => Ds = value ? Ds | DEVICE_STATUS.READ_BY_DRIVER : Ds & ~DEVICE_STATUS.READ_BY_DRIVER;
		}


		public STATUS S { get; set; } = STATUS.UNSET;

		[JsonIgnore]
		public STATUS Status
		{
			get => S;
			set => S = value;
		}

		public STATUS1 S1 { get; set; } = STATUS1.UNSET;

		[JsonIgnore]
		public STATUS1 Status1
		{
			get => S1;
			set => S1 = value;
		}

		public List<string> Ti { get; set; } = new List<string>();

		[JsonIgnore]
		public List<string> TripIdList
		{
			get => Ti;
			set => Ti = value;
		}
	}
}