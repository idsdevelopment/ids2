﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Cutoff
	{
		public decimal C { get; set; }

		[JsonIgnore]
		public decimal CutoffAmount
		{
			get => C;
			set => C = value;
		}


		public string P { get; set; } = "";

		[JsonIgnore]
		public string ToPackageType
		{
			get => P;
			set => P = value;
		}


		public int Pi { get; set; }

		[JsonIgnore]
		public int PackageTypeId
		{
			get => Pi;
			set => Pi = value;
		}

		public Cutoff()
		{
		}

		public Cutoff( int id )
		{
			PackageTypeId = id;
		}
	}

	public class PackageType
	{
		public List<Cutoff> Cf { get; set; } = new List<Cutoff>();

		[JsonIgnore]
		public List<Cutoff> Cutoffs
		{
			get => Cf;
			set => Cf = value;
		}


		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}


		public bool Ht { get; set; }

		[JsonIgnore]
		public bool HideOnTripEntry
		{
			get => Ht;
			set => Ht = value;
		}


		public decimal MaW { get; set; }

		[JsonIgnore]
		public decimal MaximumVisibleWeight
		{
			get => MaW;
			set => MaW = value;
		}


		public decimal MiW { get; set; }

		[JsonIgnore]
		public decimal MinimumVisibleWeight
		{
			get => MiW;
			set => MiW = value;
		}


		public string L { get; set; } = "";

		[JsonIgnore]
		public string Label
		{
			get => L;
			set => L = value;
		}


		public string F { get; set; } = "";

		[JsonIgnore]
		public string Formula
		{
			get => F;
			set => F = value;
		}


		public bool HxR { get; set; }

		[JsonIgnore]
		public bool HoursXRate
		{
			get => HxR;
			set => HxR = value;
		}


		public bool WxR { get; set; }

		[JsonIgnore]
		public bool WeightXRate
		{
			get => WxR;
			set => WxR = value;
		}


		public bool PxR { get; set; }

		[JsonIgnore]
		public bool PiecesXRate
		{
			get => PxR;
			set => PxR = value;
		}


		public bool DxR { get; set; }

		[JsonIgnore]
		public bool DistanceXRate
		{
			get => DxR;
			set => DxR = value;
		}


		public bool Er { get; set; }

		[JsonIgnore]
		public bool ExcessRate
		{
			get => Er;
			set => Er = value;
		}


		public bool Up { get; set; }

		[JsonIgnore]
		public bool UsePiecesForCutoffs
		{
			get => Up;
			set => Up = value;
		}


		public bool Cu { get; set; }

		[JsonIgnore]
		public bool Cumulative
		{
			get => Cu;
			set => Cu = value;
		}


		public bool Lc { get; set; }

		[JsonIgnore]
		public bool LimitMaxToNexCutoff
		{
			get => Lc;
			set => Lc = value;
		}


		public bool Se { get; set; }

		[JsonIgnore]
		public bool SimpleEnabled
		{
			get => Se;
			set => Se = value;
		}


		public bool Fe { get; set; }

		[JsonIgnore]
		public bool FormulasEnabled
		{
			get => Fe;
			set => Fe = value;
		}


		public bool Ce { get; set; }

		[JsonIgnore]
		public bool CutoffsEnabled
		{
			get => Ce;
			set => Ce = value;
		}


		public ushort So { get; set; }

		[JsonIgnore]
		public ushort SortOrder
		{
			get => So;
			set => So = value;
		}


		public bool De { get; set; }

		[JsonIgnore]
		public bool Deleted
		{
			get => De;
			set => De = value;
		}


		public PackageType()
		{
		}

		public PackageType( PackageType p )
		{
			Cutoffs.AddRange( p.Cutoffs );
			Description = p.Description;
			HideOnTripEntry = p.HideOnTripEntry;
			MaximumVisibleWeight = p.MaximumVisibleWeight;
			MinimumVisibleWeight = p.MinimumVisibleWeight;
			Label = p.Label;
			Formula = p.Formula;

			HoursXRate = p.HoursXRate;
			WeightXRate = p.WeightXRate;
			PiecesXRate = p.PiecesXRate;
			DistanceXRate = p.DistanceXRate;
			ExcessRate = p.ExcessRate;
			UsePiecesForCutoffs = p.UsePiecesForCutoffs;
			Cumulative = p.Cumulative;
			LimitMaxToNexCutoff = p.LimitMaxToNexCutoff;

			SimpleEnabled = p.SimpleEnabled;
			FormulasEnabled = p.FormulasEnabled;
			CutoffsEnabled = p.CutoffsEnabled;
			SortOrder = p.SortOrder;
			Deleted = p.Deleted;
		}
	}

	public class PackageTypeList : List<PackageType>
	{
	}

	public class PackageTypeNames
	{
		public int I { get; set; } = -1;

		[JsonIgnore]
		public int Id
		{
			get => I;
			set => I = value;
		}


		public string N { get; set; } = "";

		[JsonIgnore]
		public string Name
		{
			get => N;
			set => N = value;
		}
	}

	public class PackageTypeNamesList : List<PackageTypeNames>
	{
	}
}