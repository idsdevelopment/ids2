﻿using Newtonsoft.Json;

namespace Protocol.Data
{
	public class ProgramData
	{
		[JsonProperty]
		public string ProgramName { get; }

		private ProgramData()
		{
			ProgramName = "Not Specified";
		}

		public ProgramData( string programName )
		{
			ProgramName = programName;
		}
	}
}