﻿using System;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class Signature
	{
		public STATUS S { get; set; } = STATUS.UNSET;

		[JsonIgnore]
		public STATUS Status
		{
			get => S;
			set => S = value;
		}

		public STATUS1 S1 { get; set; } = STATUS1.UNSET;

		[JsonIgnore]
		public STATUS1 Status1
		{
			get => S1;
			set => S1 = value;
		}

		public STATUS2 S2 { get; set; } = STATUS2.UNSET;

		[JsonIgnore]
		public STATUS2 Status2
		{
			get => S2;
			set => S2 = value;
		}


		public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

		[JsonIgnore]
		public DateTimeOffset Date
		{
			get => D;
			set => D = value;
		}


		public int H { get; set; }

		[JsonIgnore]
		public int Height
		{
			get => H;
			set => H = value;
		}


		public int W { get; set; }

		[JsonIgnore]
		public int Width
		{
			get => W;
			set => W = value;
		}


		public string P { get; set; } = "";

		[JsonIgnore]
		public string Points
		{
			get => P;
			set => P = value;
		}
	}
}