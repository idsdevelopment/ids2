﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public static class Extensions
	{
		public static bool IsEmpty( this Address address )
		{
			return string.IsNullOrEmpty( address.AddressLine1.Trim() ) && string.IsNullOrEmpty( address.AddressLine2.Trim() );
		}
	}

	public class Address
	{
		public string Si { get; set; } = "";

		[JsonIgnore]
		public string SecondaryId
		{
			get => Si;
			set => Si = value;
		}


		public string B { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => B;
			set => B = value;
		}


		public string Pb { get; set; } = "";

		[JsonIgnore]
		public string PostalBarcode
		{
			get => Pb;
			set => Pb = value;
		}


		public string S { get; set; } = "";

		[JsonIgnore]
		public string Suite
		{
			get => S;
			set => S = value;
		}


		public string A1 { get; set; } = "";

		[JsonIgnore]
		public string AddressLine1
		{
			get => A1;
			set => A1 = value;
		}


		public string A2 { get; set; } = "";

		[JsonIgnore]
		public string AddressLine2
		{
			get => A2;
			set => A2 = value;
		}


		public string V { get; set; } = "";

		[JsonIgnore]
		public string Vicinity
		{
			get => V;
			set => V = value;
		}


		public string C { get; set; } = "";

		[JsonIgnore]
		public string City
		{
			get => C;
			set => C = value;
		}


		public string R { get; set; } = "";

		[JsonIgnore]
		public string Region
		{
			get => R;
			set => R = value;
		}


		public string Pc { get; set; } = "";

		[JsonIgnore]
		public string PostalCode
		{
			get => Pc;
			set => Pc = value;
		}


		public string Co { get; set; } = "";

		[JsonIgnore]
		public string Country
		{
			get => Co;
			set => Co = value;
		}


		public string Cc { get; set; } = "";

		[JsonIgnore]
		public string CountryCode
		{
			get => Cc;
			set => Cc = value;
		}


		public string P { get; set; } = "";

		[JsonIgnore]
		public string Phone
		{
			get => P;
			set => P = value;
		}


		public string P1 { get; set; } = "";

		[JsonIgnore]
		public string Phone1
		{
			get => P1;
			set => P1 = value;
		}


		public string F { get; set; } = "";

		[JsonIgnore]
		public string Fax
		{
			get => F;
			set => F = value;
		}


		public string M { get; set; } = "";

		[JsonIgnore]
		public string Mobile
		{
			get => M;
			set => M = value;
		}


		public string M1 { get; set; } = "";

		[JsonIgnore]
		public string Mobile1
		{
			get => M1;
			set => M1 = value;
		}


		public string E { get; set; } = "";

		[JsonIgnore]
		public string EmailAddress
		{
			get => E;
			set => E = value;
		}


		public string E1 { get; set; } = "";

		[JsonIgnore]
		public string EmailAddress1
		{
			get => E1;
			set => E1 = value;
		}


		public string E2 { get; set; } = "";

		[JsonIgnore]
		public string EmailAddress2
		{
			get => E2;
			set => E2 = value;
		}


		public string N { get; set; } = "";

		[JsonIgnore]
		public string Notes
		{
			get => N;
			set => N = value;
		}


		public decimal Lt { get; set; }

		[JsonIgnore]
		public decimal Latitude
		{
			get => Lt;
			set => Lt = value;
		}


		public decimal Lg { get; set; }

		[JsonIgnore]
		public decimal Longitude
		{
			get => Lg;
			set => Lg = value;
		}

		public Address()
		{
		}

		public Address( Address a )
		{
			SecondaryId = a.SecondaryId;
			Barcode = a.Barcode;
			PostalBarcode = a.PostalBarcode;
			Suite = a.Suite;
			AddressLine1 = a.AddressLine1;
			AddressLine2 = a.AddressLine2;
			Vicinity = a.Vicinity;
			City = a.City;
			Region = a.Region;
			PostalCode = a.PostalCode;
			Country = a.Country;
			CountryCode = a.CountryCode;
			Phone = a.Phone;
			Phone1 = a.Phone1;
			Fax = a.Fax;
			Mobile = a.Mobile;
			Mobile1 = a.Mobile1;
			EmailAddress = a.EmailAddress;
			EmailAddress1 = a.EmailAddress1;
			EmailAddress2 = a.EmailAddress2;
			Notes = a.Notes;
			Latitude = a.Latitude;
			Longitude = a.Longitude;
		}
	}

	public class Addresses : List<Address>
	{
		public Addresses()
		{
		}

		public Addresses( IEnumerable<Address> addresses )
		{
			AddRange( addresses );
		}
	}

	public class AddressIdList : List<string>
	{
		public AddressIdList()
		{
		}

		public AddressIdList( IEnumerable<string> list )
		{
			AddRange( list );
		}
	}
}