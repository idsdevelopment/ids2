﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class LocationDeliveryItem
	{
		public string B { get; set; } = "";

		[JsonIgnore]
		public string Barcode
		{
			get => B;
			set => B = value;
		}


		public decimal Q { get; set; }

		[JsonIgnore]
		public decimal Quantity
		{
			get => Q;
			set => Q = value;
		}
	}

	public class LocationDeliveries
	{
		public string Dr { get; set; } = "";

		[JsonIgnore]
		public string Driver
		{
			get => Dr;
			set => Dr = value;
		}


		public DateTimeOffset D { get; set; } = DateTimeOffset.Now;

		[JsonIgnore]
		public DateTimeOffset DeliveryTime
		{
			get => D;
			set => D = value;
		}


		public string L { get; set; } = "";

		[JsonIgnore]
		public string Location
		{
			get => L;
			set => L = value;
		}


		public double La { get; set; }

		[JsonIgnore]
		public double Latitude
		{
			get => La;
			set => La = value;
		}


		public double Lo { get; set; }

		[JsonIgnore]
		public double Longitude
		{
			get => Lo;
			set => Lo = value;
		}


		public List<LocationDeliveryItem> I { get; set; } = new List<LocationDeliveryItem>();

		[JsonIgnore]
		public List<LocationDeliveryItem> Items
		{
			get => I;
			set => I = value;
		}
	}
}