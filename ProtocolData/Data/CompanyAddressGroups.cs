﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class CompanyAddressGroup
	{
		public int N { get; set; }

		[JsonIgnore]
		public int GroupNumber
		{
			get => N;
			set => N = value;
		}

		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}
	}

	public class CompanyAddressGroups : List<CompanyAddressGroup>
	{
	}


	public class CompanyAddressGroupEntry
	{
		public int G { get; set; }

		[JsonIgnore]
		public int GroupNumber
		{
			get => G;
			set => G = value;
		}


		public string C { get; set; } = "";

		[JsonIgnore]
		public string CompanyName
		{
			get => C;
			set => C = value;
		}
	}

	public class CompaniesWithinAddressGroup
	{
		public int C { get; set; }

		[JsonIgnore]
		public int CompanyNumber
		{
			get => C;
			set => C = value;
		}


		public string D { get; set; } = "";

		[JsonIgnore]
		public string Description
		{
			get => D;
			set => D = value;
		}


		public List<string> L { get; set; } = new List<string>();

		[JsonIgnore]
		public List<string> Companies
		{
			get => L;
			set => L = value;
		}
	}

	public class CompaniesWithinAddressGroups : List<CompaniesWithinAddressGroup>
	{
	}
}