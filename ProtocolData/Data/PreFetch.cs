﻿namespace Protocol.Data
{
	public class PreFetch
	{
		public enum PREFETCH
		{
			FIRST,
			LAST,
			FIND_RANGE,
			FIND_MATCH,
			OFFSET
		}

		public PREFETCH Fetch { get; set; }
		public int PreFetchCount { get; set; }
		public int Offset { get; set; }
		public string Key { get; set; }
	}
}