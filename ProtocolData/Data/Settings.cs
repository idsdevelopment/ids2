﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protocol.Data
{
	public class Settings
	{
		public long LastAccountUpdate { get; set; }
		public long LastTripUpdate { get; set; }
		public long LastAddressUpdate { get; set; }
	}
}