﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Protocol.Data
{
	public class BackupRestore
	{
		public bool R { get; set; }

		[JsonIgnore]
		public bool IsRestore
		{
			get => R;
			set => R = value;
		}


		public string N { get; set; } = "";

		[JsonIgnore]
		public string Name
		{
			get => N;
			set => N = value;
		}

		public bool T { get; set; } = true;

		[JsonIgnore]
		public bool IsTestRestore
		{
			get => T;
			set => T = value;
		}
	}

	public class BackupList : List<string>
	{
	}
}