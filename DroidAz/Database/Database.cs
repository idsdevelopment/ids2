﻿using System;
using System.IO;
using System.Linq;
using SQLite;
using Utils;

namespace Database
{
	public class Database : SQLiteConnection
	{
		private const int DB_VERSION = 10;

		private static string DbPath( string dbName )
		{
			var Folder = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ) );
			return Path.Combine( Folder, dbName );
		}

		private void OpenTable<T>( bool reCreate )
		{
			try
			{
				if( reCreate )
				{
					try
					{
						DropTable<T>();
					}
					catch
					{
					}
				}

				CreateTable<T>();
			}
			catch // Probably restructured table
			{
				DropTable<T>();
				CreateTable<T>();
			}
		}

		public class DbVersion
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public int Version { get; set; } = DB_VERSION;
		}

		public Database( string dbName ) : base( DbPath( dbName ) )
		{
			CreateTable<DbVersion>();

			var Version = ( from V in Table<DbVersion>()
			                select V ).FirstOrDefault();

			var ReCreate = ( Version == null ) || ( Version.Version != DB_VERSION );

			if( ReCreate )
			{
				OpenTable<DbVersion>( true );
				DeleteAll<DbVersion>();
				Insert( new DbVersion { Version = DB_VERSION } );
			}
        }
    }
}