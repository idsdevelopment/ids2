﻿using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using DroidAz.Droid;
using Globals;
using Object = Java.Lang.Object;

namespace DroidAz
{
	[Service]
	public class IdsDroidService : Android.App.Service
	{
		public delegate void OnTripsChangedEvent( int totalTrips, bool hasNew, bool hasDeleted, bool hasChanged );

		private OnTripsChangedEvent _OnTripsChanged;

		private Intent Intent;

		private LocationProvider Locations;

		public IBinder Binder { get; private set; }

		public bool ServiceStarted { get; protected internal set; }

		public OnTripsChangedEvent OnTripsChanged
		{
			get => _OnTripsChanged;
			set
			{
				_OnTripsChanged = value;
			}
		}

		public override StartCommandResult OnStartCommand( Intent intent, StartCommandFlags flags, int startId )
		{
			Intent = intent;

			Locations = new LocationProvider( this )
			            {
			            };

			return StartCommandResult.Sticky;
		}

		public void StopService()
		{
			StopService( Intent );
			ServiceStarted = false;
		}

		public override void OnDestroy()
		{
			Locations?.Dispose();
			Locations = null;
			base.OnDestroy();
		}

		public override IBinder OnBind( Intent intent )
		{
			return Binder = new IdsDroidServiceBinder( this );
		}
	}

	public class IdsDroidServiceBinder : Binder
	{
		public IdsDroidServiceBinder( IdsDroidService srvice )
		{
			Service = srvice;
		}

		public IdsDroidService Service { get; }
	}

	public class IdsDroidServiceConnection : Object, IServiceConnection
	{
		private readonly MainActivity Activity;

		public Action<IdsDroidService> OnConnected;

		public IdsDroidServiceConnection( MainActivity activity )
		{
			Activity = activity;
		}

		public void OnServiceConnected( ComponentName name, IBinder service )
		{
			if( service is IdsDroidServiceBinder ServiceBinder )
			{
				Activity.Binder = ServiceBinder;
				Activity.IsBound = true;
				if( !ServiceBinder.Service.ServiceStarted )
				{
					ServiceBinder.Service.ServiceStarted = true;
					Application.Context.StartService( Droid.AndroidGlobals.Background.ServiceIntent );
					OnConnected?.Invoke( ServiceBinder.Service );
				}
			}
		}

		public void OnServiceDisconnected( ComponentName name )
		{
			Activity.IsBound = false;
		}
	}
}