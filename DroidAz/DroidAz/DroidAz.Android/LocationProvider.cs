﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Provider;
using DroidAz.Droid;
using Object = Java.Lang.Object;
using Thread = System.Threading.Thread;

namespace DroidAz
{
	public class LocationProvider : Object, ILocationListener
	{
		private const int DEFAULT_LOCATION_TICK_IN_SECONDS = 30;

		private readonly Criteria CriteriaForLocationService = new Criteria
		                                                       {
			                                                       Accuracy = Accuracy.Fine
		                                                       };

		private double LastLat,
		               LastLong;

		private readonly Timer LocationTimer;

		private LocationManager Manager;

		private volatile string Provider = "";

		private void GetProvider()
		{
			if( AndroidGlobals.Android.VersionAsInt <= 412 )
				Provider = LocationManager.GpsProvider;
			else
			{
				var AcceptableLocationProviders = Manager.GetProviders( CriteriaForLocationService, true );
				Provider = AcceptableLocationProviders.Any() ? AcceptableLocationProviders.First() : string.Empty;
			}
		}

		private void WaitProvider()
		{
			Task.Run( () =>
			          {
				          while( true )
				          {
					          GetProvider();

					          if( string.IsNullOrWhiteSpace( Provider ) || !Manager.IsProviderEnabled( Provider ) )
						          Thread.Sleep( 100 );
					          else
						          break;
				          }

				          var Act = AndroidGlobals.Debug.Activity;
				          while( Act == null )
				          {
					          Thread.Sleep( 100 );
					          Act = AndroidGlobals.Debug.Activity;
				          }

				          Act.RunOnUiThread( () =>
				                             {
					                             Manager.RequestLocationUpdates( Provider, 1500, 1, this );
				                             } );
			          } );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing && ( Manager != null ) )
			{
				Manager.RemoveUpdates( this );
				Manager.Dispose();
				Manager = null;
			}

			base.Dispose( disposing );
		}

		public LocationProvider()
		{
		}

		public LocationProvider( Context context )
		{
			Manager = (LocationManager)context.GetSystemService( Context.LocationService );

			GetProvider();

			// If GPS is off open up GPS settings
			if( string.IsNullOrEmpty( Provider ) || !Manager.IsProviderEnabled( Provider ) )
			{
				AndroidGlobals.Debug.Activity.RunOnUiThread( () =>
				                                             {
					                                             var GpsSettingsIntent = new Intent( Settings.ActionLocationSourceSettings );
					                                             GpsSettingsIntent.SetFlags( ActivityFlags.NewTask );
					                                             context.StartActivity( GpsSettingsIntent );
				                                             } );
			}

			WaitProvider();
			LocationTimer = new Timer( DEFAULT_LOCATION_TICK_IN_SECONDS * 1000 );
			LocationTimer.Elapsed += ( sender, args ) =>
			                         {
				                         if( ( LastLat != 0 ) && ( LastLong != 0 ) )
				                         {
					                         if( Globals.Settings.GpsEnabled )
						                         Globals.Gps.OnPositionChanged?.Invoke( DateTimeOffset.Now, LastLat, LastLong );
				                         }
			                         };
			LocationTimer.Start();
		}

		// ILocationListener Interface routines
		public void OnLocationChanged( Location location )
		{
			if( Globals.Settings.GpsEnabled )
			{
				LocationTimer.Stop();
				LocationTimer.Start();

				Globals.Gps.OnPositionChanged?.Invoke( DateTimeOffset.Now, LastLat = location.Latitude, LastLong = location.Longitude );
			}
		}

		public void OnProviderDisabled( string provider )
		{
		}

		public void OnProviderEnabled( string provider )
		{
		}

		public void OnStatusChanged( string provider, Availability status, Bundle extras )
		{
		}
	}
}