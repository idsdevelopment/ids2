﻿using System.Threading;
using System.Threading.Tasks;
using Android.Media;

namespace DroidAz.Droid.Globals
{
	public static class Sounds
	{
		public static async void PlaySound( int sound, bool killSound )
		{
			if( !killSound )
			{
				await Task.Run( () =>
				                {
					                var Player = MediaPlayer.Create( MainActivity.Instance, sound );

					                if( Player != null )
					                {
						                Player.Start();

						                do
							                Thread.Sleep( 50 );
						                while( Player.IsPlaying );

						                MainActivity.Instance.RunOnUiThread( () =>
						                                                     {
							                                                     Player.Release();
							                                                     Player = null;
						                                                     } );
					                }
				                } );
			}
		}

		public static void InitSounds()
		{
			global::Globals.Sounds.PlayBadScanSound = () =>
			                                          {
				                                          PlaySound( Resource.Raw.BadScan, false );
			                                          };
		}
	}
}