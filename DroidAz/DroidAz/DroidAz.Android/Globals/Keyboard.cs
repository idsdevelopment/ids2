﻿using Android.Content;
using Android.Views.InputMethods;

namespace DroidAz.Droid.Globals
{
	public static class Keyboard
	{
		public static bool Show
		{
			set
			{
				var Act = MainActivity.Instance;

				Act.RunOnUiThread( () =>
				                   {
					                   var W = Act.Window;
					                   var Focus = W.CurrentFocus;

					                   if( Focus != null )
					                   {
						                   var InputManager = (InputMethodManager)Act.GetSystemService( Context.InputMethodService );

						                   if( !value )
						                   {
							                   // Hide
							                   InputManager.ShowSoftInput( Focus, 0 );
							                   InputManager.ToggleSoftInput( ShowFlags.Forced, HideSoftInputFlags.None );
						                   }
						                   else
							                   InputManager.ShowSoftInput( Focus, ShowFlags.Implicit );
					                   }
				                   } );
			}
		}

		public static bool Hide
		{
			set => Show = !value;
		}
	}
}