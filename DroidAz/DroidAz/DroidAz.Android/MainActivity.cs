﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Globals;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ZXing.Mobile;
using Keyboard = Globals.Keyboard;
using Platform = ZXing.Net.Mobile.Forms.Android.Platform;
using Settings = Android.Provider.Settings;
using Timer = System.Timers.Timer;
using Uri = Android.Net.Uri;

// ReSharper disable InconsistentNaming

namespace DroidAz.Droid
{
    [Activity( Label = "IDS Mobile",
        Icon = "@mipmap/icon",
        Theme = "@style/SplashScreen",
        MainLauncher = true,
        AlwaysRetainTaskState = true,
        LaunchMode = LaunchMode.SingleTask,
        ClearTaskOnLaunch = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden /*,
		ScreenOrientation = ScreenOrientation.Portrait */ )]
    public class MainActivity : FormsAppCompatActivity
    {
        private static Timer BackDebounceTimer;
        private static IdsDroidService Service;

        public static MainActivity Instance { get; private set; }

        private enum PERMISSIONS
        {
            LOCATION_PERMISSION_REQUEST,
            STORAGE_REQUEST,
            INSTALL_REQUEST,
            NO_DOZE
        }

        private Action NextPermissionCallback;

        private bool Paused;

        public IdsDroidServiceBinder Binder;
        public bool IsBound;

        private void AskPermission( string[] permission, PERMISSIONS requestCode, Action completedCallback )
        {
            NextPermissionCallback = completedCallback;

            // Here, thisActivity is the current activity
            if( CheckSelfPermission( permission[ 0 ] ) != Permission.Granted )
            {
                if( !ShouldShowRequestPermissionRationale( permission[ 0 ] ) )
                {
                    RequestPermissions( permission, (int)requestCode );

                    return;
                }
            }

            completedCallback?.Invoke();
        }

        private void DoService()
        {
            AndroidGlobals.Background.ServiceIntent = new Intent( this, typeof( IdsDroidService ) );

            AndroidGlobals.Background.Connection = new IdsDroidServiceConnection( this )
            {
                OnConnected = service =>
                              {
                                  Service = service;
                                  StartService( AndroidGlobals.Background.ServiceIntent );
                              }
            };

            // Keep active across configuration changes use ApplicationContext
            ApplicationContext.BindService( AndroidGlobals.Background.ServiceIntent, AndroidGlobals.Background.Connection, Bind.AutoCreate );
        }

        private void GetPermissions()
        {
            var MajorVersion = AndroidGlobals.Android.MajorVersion;

            void Android_6_Permissions()
            {
                AskPermission( new[] { Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage },
                               PERMISSIONS.STORAGE_REQUEST,
                               () =>
                               {
                                   AskPermission( new[] { Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation },
                                                  PERMISSIONS.LOCATION_PERMISSION_REQUEST,
                                                  DoService );
                               } );
            }

            void Android_7_Permissions()
            {
                if( MajorVersion == 7 )
                {
                    AskPermission( new[] { Manifest.Permission.RequestInstallPackages },
                                   PERMISSIONS.INSTALL_REQUEST,
                                   Android_6_Permissions );
                }
                else
                    Android_6_Permissions();
            }

            async void Android_8_Permissions()
            {
                if( MajorVersion >= 8 )
                {
                    if( !PackageManager.CanRequestPackageInstalls() )
                    {
                        var SettingsIntent = new Intent( Settings.ActionManageUnknownAppSources );
                        SettingsIntent.SetData( Uri.Parse( "package:" + PackageName ) );
                        StartActivity( SettingsIntent );

                        var Ok = await Task.Run( () =>
                                                 {
                                                     while( !Paused )
                                                         Thread.Sleep( 1000 );

                                                     while( Paused ) // Wait for Settings Dialog To Close
                                                         Thread.Sleep( 1000 );

                                                     return PackageManager.CanRequestPackageInstalls();
                                                 } );

                        if( !Ok )
                        {
                            ShowErrorDialogue( "Install" );

                            return;
                        }
                    }
                }

                Android_7_Permissions();
            }

            void Android_9_Permissions()
            {
                if( MajorVersion >= 9 )
                {
                    AskPermission( new[] { Manifest.Permission.RequestIgnoreBatteryOptimizations },
                                   PERMISSIONS.NO_DOZE, Android_8_Permissions );
                }
                else
                    Android_8_Permissions();
            }

            Android_9_Permissions();
        }

        private void ShowErrorDialogue( string text )
        {
            var Alert = new AlertDialog.Builder( this );
            Alert.SetTitle( "Cannot continue" );
            Alert.SetMessage( $"Unable to continue without {text} permission." );

            Alert.SetPositiveButton( "Ok", ( sender, args ) =>
                                           {
                                               Finish();
                                           } );

            var Dialog = Alert.Create();
            Dialog.Show();
        }

        protected override void OnPause()
        {
            Paused = true;
            base.OnPause();
        }

        protected override void OnResume()
        {
            Paused = false;
            base.OnResume();
        }

        public override bool DispatchKeyEvent( KeyEvent e )
        {
            if( e.KeyCode == Keycode.Back )
            {
                // Sometimes gives multiple events
                if( BackButton.AllowBackButton && ( BackDebounceTimer == null ) )
                {
                    BackDebounceTimer = new Timer( 500 );

                    BackDebounceTimer.Elapsed += ( sender, args ) =>
                                                 {
                                                     BackDebounceTimer?.Dispose();
                                                     BackDebounceTimer = null;
                                                 };
                    BackDebounceTimer.Start();
                    BackButton.OnBackKey?.Invoke();
                }

                return true;
            }

            return base.DispatchKeyEvent( e );
        }

        public override void OnRequestPermissionsResult( int requestCode, string[] permissions, Permission[] grantResults )
        {
            if( ( grantResults.Length <= 0 ) || ( grantResults[ 0 ] == Permission.Denied ) )
            {
                string Text;

                switch( (PERMISSIONS)requestCode )
                {
                case PERMISSIONS.LOCATION_PERMISSION_REQUEST:
                    Text = "GPS";

                    break;

                case PERMISSIONS.STORAGE_REQUEST:
                    Text = "Storage";

                    break;

                case PERMISSIONS.INSTALL_REQUEST:
                    Text = "Install";

                    break;

                case PERMISSIONS.NO_DOZE:
                    Text = "Disable Battery Optimisation";

                    break;

                default:
                    return;
                }

                ShowErrorDialogue( Text );
            }
            else
                NextPermissionCallback?.Invoke();
        }

        protected override void OnCreate( Bundle savedInstanceState )
        {
            Instance = this;
            AndroidGlobals.Debug.Activity = this;

            Globals.Sounds.InitSounds();

            Keyboard.ShowHide = show =>
                                {
                                    Globals.Keyboard.Show = show;
                                };

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate( savedInstanceState );

            GetPermissions();

            Forms.Init( this, savedInstanceState );

            Platform.Init();
            MobileBarcodeScanner.Initialize( Application );

            Orientation.ChangeOrientation = orientation =>
                                            {
                                                switch( orientation )
                                                {
                                                case Orientation.ORIENTATION.BOTH:
                                                    RequestedOrientation = ScreenOrientation.Unspecified;
                                                    break;
                                                case Orientation.ORIENTATION.LANDSCAPE:
                                                    RequestedOrientation = ScreenOrientation.Landscape;
                                                    break;
                                                case Orientation.ORIENTATION.PORTRAIT:
                                                    RequestedOrientation = ScreenOrientation.Portrait;
                                                    break;
                                                }
                                            };
            LoadApplication( new App() );
        }
    }
}