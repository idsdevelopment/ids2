﻿using System.Threading;
using Xamarin.Forms;

namespace DroidAz.UWP
{
	public sealed partial class MainPage
	{
		public MainPage()
		{
			InitializeComponent();
			LoadApplication( new DroidAz.App() );
		}
	}
}