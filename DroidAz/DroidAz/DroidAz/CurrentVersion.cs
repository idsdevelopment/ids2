﻿namespace DroidAz
{
	public class CurrentVersion
	{
		public const string VERSION = "1.13";

		public const string APP_VERSION = "DROID Vsn " + VERSION;
	}
}