﻿using System;
using System.Threading;
using System.Threading.Tasks;
using IdsControlLibraryV2.ViewModel;
using WeakEvent;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Mobile;
using Keyboard = Globals.Keyboard;
using Timer = System.Timers.Timer;

namespace DroidAz.Controls
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class BarcodeScanner : ContentView
	{
		private MobileBarcodeScanner Scanner => _Scanner ?? ( _Scanner = new MobileBarcodeScanner() );

		public bool BarcodeAvailable { get; private set; }

		private MobileBarcodeScanner _Scanner;
		private readonly BarcodeScannerViewModel Model;

		private readonly Timer FocusTimer = new Timer( 250 )
		                                    {
			                                    AutoReset = false
		                                    };


		public void Clear()
		{
			ViewModelBase.Dispatcher( () =>
			                          {
				                          BarcodeEntry.Text = "";
				                          Barcode = "";
				                          BarcodeAvailable = true;
			                          } );
		}

		public async Task<string> Scan()
		{
			BarcodeAvailable = false;

			ViewModelBase.Dispatcher( () =>
			                          {
				                          BarcodeEntry.Text = "";
				                          Barcode = "";
				                          Focus();

				                          if( IsVisible )
					                          Keyboard.Show = false;
			                          } );

			return await Task.Run( async () =>
			                       {
				                       while( !BarcodeAvailable )
				                       {
					                       if( IsScanning && IsVisible )
						                       Thread.Sleep( 250 );
					                       else
						                       return "";
				                       }

				                       var Code = "";
				                       var HaveCode = false;

				                       // Dispatcher is Async
				                       ViewModelBase.Dispatcher( () =>
				                                                 {
					                                                 Code = Barcode;
					                                                 IsScanning = false;
					                                                 HaveCode = true;
				                                                 } );

				                       return await Task.Run( () =>
				                                              {
					                                              while( !HaveCode )
						                                              Thread.Sleep( 100 );

					                                              return Code;
				                                              } );
			                       } );
		}

		public new bool Focus()
		{
			return BarcodeEntry.Focus();
		}


		~BarcodeScanner()
		{
			ManualBarcodeInputChange -= ManualBarcodeChangeEvent;
		}

		public BarcodeScanner()
		{
			InitializeComponent();

			ManualBarcodeInputChange += ManualBarcodeChangeEvent;

			BarcodeEntry.Focused += ( sender, args ) =>
			                        {
				                        if( IsVisible )
					                        Keyboard.Show = false;
			                        };

			FocusTimer.Elapsed += ( sender, args ) =>
			                      {
				                      if( IsVisible && IsScanning )
				                      {
					                      ViewModelBase.Dispatcher( () =>
					                                                {
						                                                BarcodeEntry.Focus();
					                                                } );
				                      }
			                      };

			BarcodeEntry.Unfocused += ( sender, args ) =>
			                          {
				                          FocusTimer.Start();
			                          };

			var InBarcodeChange = false;

			Root.BindingContext = Model = new BarcodeScannerViewModel
			                              {
				                              ScanComplete = () =>
				                                             {
					                                             if( !InBarcodeChange )
					                                             {
						                                             InBarcodeChange = true;
						                                             Barcode = BarcodeEntry.Text.Trim();
						                                             BarcodeAvailable = true;
						                                             InBarcodeChange = false;
					                                             }
				                                             },

				                              ExecuteZXingScanner = async () =>
				                                                    {
					                                                    var Tm = new Timer( 3_000 )
					                                                             {
						                                                             AutoReset = true
					                                                             };

					                                                    Tm.Elapsed += ( sender, args ) =>
					                                                                  {
						                                                                  ViewModelBase.Dispatcher( () =>
						                                                                                            {
							                                                                                            Scanner.AutoFocus();
						                                                                                            } );
					                                                                  };

					                                                    try
					                                                    {
						                                                    Tm.Start();

						                                                    var Temp = await Scanner.Scan( new MobileBarcodeScanningOptions() );

						                                                    return Temp?.Text ?? "";
					                                                    }
					                                                    catch
					                                                    {
						                                                    return "";
					                                                    }
					                                                    finally
					                                                    {
						                                                    Tm.Dispose();
					                                                    }
				                                                    }
			                              };

			var Timer = new Timer( 10 )
			            {
				            AutoReset = false
			            };

			Timer.Elapsed += ( sender, args ) =>
			                 {
				                 ViewModelBase.Dispatcher( () =>
				                                           {
					                                           Focus();
				                                           } );
				                 Timer.Dispose();
				                 Timer = null;
			                 };
			Timer.Start();
		}

	#region ManualBarcode

		// ReSharper disable once InconsistentNaming
		private static readonly WeakEventSource<bool> _ManualBarcodeInputChange = new WeakEventSource<bool>();

		public static event EventHandler<bool> ManualBarcodeInputChange
		{
			add => _ManualBarcodeInputChange.Subscribe( value );
			remove => _ManualBarcodeInputChange.Unsubscribe( value );
		}

		private static bool _IsManualBarcode;

		public static bool IsManualBarcode
		{
			get => _IsManualBarcode;
			set
			{
				_IsManualBarcode = value;
				_ManualBarcodeInputChange?.Raise( null, value );
			}
		}

		private void ManualBarcodeChangeEvent( object sender, bool manualBarcode )
		{
			ManualBarcodeInput = manualBarcode;
		}

		public static readonly BindableProperty ManualBarcodeInputProperty = BindableProperty.Create( nameof( ManualBarcodeInput ), typeof( bool ), typeof( BarcodeScanner ), false );

		public bool ManualBarcodeInput
		{
			get => (bool)GetValue( ManualBarcodeInputProperty );
			set
			{
				SetValue( ManualBarcodeInputProperty, value );
				Model.ManualBarcodeInput = value;
			}
		}

	#endregion

	#region Placeholder

		public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create( nameof( Placeholder ), typeof( string ), typeof( BarcodeScanner ), "Barcode", propertyChanged: PlaceholderPropertyChanged );

		private static void PlaceholderPropertyChanged( BindableObject bindable, object oldvalue, object newvalue )
		{
			if( bindable is BarcodeScanner Bc && newvalue is string Pl )
				Bc.BarcodeEntry.Placeholder = Pl;
		}

		public string Placeholder
		{
			get => (string)GetValue( PlaceholderProperty );
			set => SetValue( PlaceholderProperty, value );
		}

	#endregion

	#region Barcode

		public static readonly BindableProperty BarcodeProperty = BindableProperty.Create( nameof( Barcode ), typeof( string ), typeof( BarcodeScanner ), "", BindingMode.OneWayToSource, propertyChanged: BarcodePropertyChanged );

		private static void BarcodePropertyChanged( BindableObject bindable, object oldvalue, object newvalue )
		{
			if( bindable is BarcodeScanner Bc )
				Bc.IsScanning = false;
		}

		public string Barcode
		{
			get => (string)GetValue( BarcodeProperty );
			private set => SetValue( BarcodeProperty, value );
		}

	#endregion


	#region IsScanning

		public static readonly BindableProperty IsScanningProperty = BindableProperty.Create( nameof( IsScanning ), typeof( bool ), typeof( BarcodeScanner ), false, BindingMode.TwoWay, propertyChanged: IsScanningPropertyChanged );

		private static void IsScanningPropertyChanged( BindableObject bindable, object oldvalue, object newvalue )
		{
			if( bindable is BarcodeScanner Bc && newvalue is bool NewValue )
			{
				Keyboard.Show = false;

				if( NewValue )
				{
					Task.Run( async () =>
					          {
						          await Bc.Scan();
					          } );
				}
			}
		}

		public bool IsScanning
		{
			get => (bool)GetValue( IsScanningProperty );
			set => SetValue( IsScanningProperty, value );
		}

	#endregion
	}
}