﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DroidAz.Views;
using IdsControlLibraryV2.ViewModel;
using Utils;

namespace DroidAz.Controls
{
	public class BarcodeScannerViewModel : ViewModelBase
	{
		public string Barcode
		{
			get { return Get( () => Barcode, "" ); }
			set { Set( () => Barcode, value ); }
		}


		public bool EnableAccept
		{
			get { return Get( () => EnableAccept, false ); }
			set { Set( () => EnableAccept, value ); }
		}


		public bool ManualBarcodeInput
		{
			get { return Get( () => ManualBarcodeInput, MainPageViewModel.Instance.ManualBarcodeInput ); }
			set { Set( () => ManualBarcodeInput, value ); }
		}

		public ICommand ZXingScanner => Commands[ nameof( ZXingScanner ) ];

		public ICommand AcceptButton => Commands[ nameof( AcceptButton ) ];
		public Func<Task<string>> ExecuteZXingScanner;

		public Action ScanComplete;


		[DependsUpon( nameof( Barcode ), Delay = 300 )]
		public void WhenBarcodeChanges()
		{
			var Enab = Barcode.IsNotNullOrEmpty();
			EnableAccept = Enab;

			if( !ManualBarcodeInput && Enab )
				ScanComplete?.Invoke();
		}

		public async void Execute_ZXingScanner()
		{
			if( ExecuteZXingScanner != null )
			{
				var Temp = await ExecuteZXingScanner.Invoke();

				Dispatcher( () =>
				            {
					            Barcode = Temp;
				            } );
			}
		}

		public void Execute_AcceptButton()
		{
			ScanComplete?.Invoke();
		}

		public bool CanExecute_AcceptButton()
		{
			return false; // Weird, Xamarin forms takes this as the first value for IsEnabled on a Button
		}
	}
}