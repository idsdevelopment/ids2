﻿using System;
using System.Threading.Tasks;
using AzureRemoteService;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Settings = Globals.Settings;

namespace DroidAz.Gps
{
	public class GpsViewModel : ViewModelBase
	{
		public const int GPS_UPDATE_TIME_LIMIT_IN_MINUTES = 1;
		private static GpsViewModel _Instance;

		private static readonly GpsPoints EmptyGpsPoints = new GpsPoints();

		private static DateTimeOffset LastSend = DateTimeOffset.MinValue;

		[Setting]
		public GpsPoints GpsPoints
		{
			get { return Get( () => GpsPoints, new GpsPoints() ); }
			set { Set( () => GpsPoints, value ); }
		}

		public static GpsViewModel Instance => _Instance ?? ( _Instance = new GpsViewModel() );

		private readonly object LockObject = new object();


		public void Start()
		{
			Settings.GpsEnabled = false;
			Globals.Gps.OnPositionChanged = AddGpsPoint;
		}

		[DependsUpon( nameof( GpsPoints ), Delay = 1000 )]
		public void WhenGpsPointsChange()
		{
			lock( LockObject )
				SaveSettings();
		}


		public void AddGpsPoint( DateTimeOffset time, double latitude, double longitude )
		{
			var GPoint = new GpsPoint
			             {
				             LocalDateTime = time,
				             Latitude = latitude,
				             Longitude = longitude
			             };

			Globals.Gps.CurrentGpsPoint = GPoint;

			if( Settings.GpsEnabled && Distance.IsDistanceOk( latitude, longitude ) )
			{
				lock( LockObject )
				{
					var Temp = GpsPoints;
					GpsPoints = EmptyGpsPoints; // Start timer

					Temp.Add( GPoint );

					GpsPoints = Temp;

					var Now = DateTimeOffset.Now;
					var DoSend = ( Now - LastSend ).TotalMinutes >= GPS_UPDATE_TIME_LIMIT_IN_MINUTES;
					if( !DoSend )
					{
						foreach( var GpsPoint in Temp )
						{
							if( ( Now - GpsPoint.LocalDateTime ).TotalMinutes >= GPS_UPDATE_TIME_LIMIT_IN_MINUTES )
							{
								DoSend = true;

								break;
							}
						}
					}

					if( DoSend )
					{
						Task.Run( () =>
						          {
							          lock( LockObject )
							          {
								          try
								          {
									          Azure.Client.RequestGps( GpsPoints ).Wait();
									          Dispatcher( () =>
									                      {
										                      GpsPoints = new GpsPoints();
									                      } );
								          }
								          catch
								          {
								          }

								          LastSend = Now;
							          }
						          } );
					}
				}
			}
		}
	}
}