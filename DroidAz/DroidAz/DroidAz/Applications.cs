﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using DroidAz.Views;
using DroidAz.Views._Standard.Menu;
using Utils;
using Xamarin.Forms;

namespace DroidAz
{
	public static class Applications
	{
		public const string SIGN_IN = "SignIn",
		                    BLANK_PAGE = "BlankPage",
		                    PICKUP_DELIVER = "PickupDeliver";


		public class MenuEntry
		{
			public string Text { get; set; }
			public string Id = "";
		}


		public class Program
		{
			public Type PageType;
            public Globals.Orientation.ORIENTATION Orientation = Globals.Orientation.ORIENTATION.PORTRAIT;
		}

		public static string CurrentProgram = "";


		private static Dictionary<string, Program> _Programs;

		public static StackLayout LoadArea;
		public static ContentPage MainContentPage;

		public static bool ShowMenu
		{
			get => !( MainPageViewModel.Instance is null ) && MainPageViewModel.Instance.MenuVisible;

			set
			{
				if( !( MainPageViewModel.Instance is null ) )
					MainPageViewModel.Instance.MenuVisible = value;
			}
		}

		public static Dictionary<string, Program> Programs
		{
			get => _Programs;
			set
			{
				_Programs = value;
				var MenuEntries = new ObservableCollection<MenuEntry>();

				foreach( var (Text, Program) in value )
				{
					if( ( Text != BLANK_PAGE ) && ( Text != SIGN_IN ) )
					{
						var Entry = new MenuEntry
						            {
							            Id = Text,
							            Text = Globals.Resources.GetStringResource( Text )
						            };

						MenuEntries.Add( Entry );
					}
				}

				MainPageViewModel.Instance.MenuItemsSource = MenuEntries;
			}
		}

		private static ContentView RunProgram( ContentView program )
		{
			try
			{
				MainPageViewModel.Instance.ShowTripCount = false;
				LoadArea.Children.Clear();
				program.HorizontalOptions = LayoutOptions.FillAndExpand;
				program.VerticalOptions = LayoutOptions.FillAndExpand;
				LoadArea.Children.Add( program );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return program;
		}

		public static void Kill()
		{
			Process.GetCurrentProcess().Kill();
		}

		public static ContentView RunProgram( Type pgm )
		{
			try
			{
				return RunProgram( (ContentView)Activator.CreateInstance( pgm ) );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return null;
		}


		public static ContentView RunProgram( string pgmName )
		{
			try
			{
				if( pgmName.IsNullOrWhiteSpace() )
					pgmName = BLANK_PAGE;

				if( Programs.TryGetValue( pgmName, out var Pgm ) )
				{
					CurrentProgram = pgmName;

					if( pgmName != SIGN_IN )
					{
						new SaveSettingsViewModel
						{
							LastProgram = pgmName
						};

						MainPageViewModel.Instance.CurrentProgramDescription = Globals.Resources.GetStringResource( pgmName );
					}
                    Globals.Orientation.LayoutDirection = Pgm.Orientation;
					return RunProgram( Pgm.PageType );
				}

				return RunProgram( typeof( Views.BlankPage.BlankPage ) );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return null;
		}
	}
}