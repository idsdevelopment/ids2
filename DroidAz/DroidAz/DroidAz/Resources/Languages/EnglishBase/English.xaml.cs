﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Resources.Languages.EnglishBase
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class English : ResourceDictionary
    {
		public English()
		{
			InitializeComponent();
		}
	}
}