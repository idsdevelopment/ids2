﻿using System;
using DroidAz.Views._Standard.Menu;
using DroidAz.Views._Standard.Menu.Pml;
using CarrierPrograms = System.Collections.Generic.Dictionary<string, DroidAz.Applications.Program>;

namespace DroidAz.Modifications
{
	internal struct Menus
	{
		public string CarrierId;
		public CarrierPrograms Programs;
	}

	public static class Users
	{
		private const string PML = "Pml",
		                     PML_TEST = "PmlTest";


		internal static Menus[] CarrierMenus =
		{
			//Default Menu
			new Menus { CarrierId = "", Programs = StandardMenu.Programs },
			new Menus { CarrierId = PML, Programs = PmlMenu.Programs },
			new Menus { CarrierId = PML_TEST, Programs = PmlMenu.Programs }
		};


		private static CarrierPrograms _Programs;

		private static string _CarrierId = "";

		public static string CarrierId
		{
			get => _CarrierId;
			set
			{
				_CarrierId = value = value.Trim().ToUpper();
				foreach( var Menu in CarrierMenus )
				{
					if( string.Compare( Menu.CarrierId, value, StringComparison.OrdinalIgnoreCase ) == 0 )
					{
						_Programs = Menu.Programs;
						break;
					}
				}
			}
		}

        public static string UserName = "";

		public static CarrierPrograms Programs => _Programs ?? CarrierMenus[ 0 ].Programs;
	}
}