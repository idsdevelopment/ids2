﻿using DroidAz.Views;
using Globals;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation( XamlCompilationOptions.Compile )]

namespace DroidAz
{
	public partial class App : Application
	{
		public App()
		{
			Instance = this;
			InitializeComponent();
			MainPage = new MainPage();
		}

		public static App Instance { get; private set; }

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}