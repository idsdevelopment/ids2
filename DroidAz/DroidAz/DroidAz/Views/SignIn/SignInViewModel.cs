﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using AzureRemoteService;
using DroidAz.Views._Standard.Menu;
using DroidAz.Views.BaseViewModels;
using Globals;
using Utils;

namespace DroidAz.Views
{
	public class SignInViewModel : PingViewModel
	{
		public bool OnLine
		{
			get { return Get( () => OnLine ); }
			set { Set( () => OnLine, value ); }
		}

		[Setting]
		public string Account
		{
			get { return Get( () => Account ); }
			set { Set( () => Account, value ); }
		}

		[Setting]
		public string UserName
		{
			get { return Get( () => UserName ); }
			set { Set( () => UserName, value ); }
		}

		public string Password
		{
			get { return Get( () => Password ); }
			set { Set( () => Password, value ); }
		}


		public bool SignInEnabled
		{
			get { return Get( () => SignInEnabled, false ); }
			set { Set( () => SignInEnabled, value ); }
		}

		public ICommand Login
		{
			get
			{
				var Temp = Commands[ nameof( Login ) ];

				return Temp;
			}
		}

		public Action DisplayTimeError,
		              DisplayInvalidSignIn;

		[DependsUpon( nameof( OnLine ) )]
		[DependsUpon( nameof( Account ) )]
		[DependsUpon( nameof( UserName ) )]
		[DependsUpon( nameof( Password ) )]
		public void EnableSignIn()
		{
			SignInEnabled = OnLine && !string.IsNullOrEmpty( Account ) && !string.IsNullOrEmpty( UserName ) && !string.IsNullOrEmpty( Password );
		}


		public async Task Execute_Login()
		{
			Azure.AZURE_CONNECTION_STATUS LIn;
			var UName = UserName.Trim();
			try
			{
				LIn = await Azure.LogIn( CurrentVersion.APP_VERSION, Account.Trim(), UName, Password, true );
			}
			catch( Exception e )
			{
				Console.WriteLine( e );

				return;
			}

			switch( LIn )
			{
			case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
				DisplayTimeError?.Invoke();

				break;

			case Azure.AZURE_CONNECTION_STATUS.ERROR:
				DisplayInvalidSignIn?.Invoke();

				break;

			default:
				await Task.Run( () =>
				                {
					                var AllowManualBarcode = Preferences.GetPreferences().AllowManualBarcodeInput;
					                Dispatcher( () =>
					                            {
						                            MainPageViewModel.Instance.AllowManualBarcode = AllowManualBarcode;
					                            } );
				                } );

				Modifications.Users.CarrierId = Account;
                Modifications.Users.UserName = UName;

				Applications.Programs = Modifications.Users.Programs;

				Settings.GpsEnabled = true;
				MainPageViewModel.Instance.DriverName = UName.Capitalise();
				UpdateViewModel.InitialiseUpdates();
				var LastPgm = new SaveSettingsViewModel().LastProgram;
				Applications.RunProgram( LastPgm );
				Applications.ShowMenu = true;
				SaveSettings();

				break;
			}
		}

		public SignInViewModel()
		{
			OnPing += ( sender, online ) =>
			          {
				          OnLine = online;
			          };

			OnLine = MainPageViewModel.IsOnline;
		}
	}
}