﻿using System.Timers;
using IdsControlLibraryV2.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views
{
    [XamlCompilation( XamlCompilationOptions.Compile )]
    public partial class SignIn : ContentView
    {
        public SignIn()
        {
            InitializeComponent();

            Applications.ShowMenu = false;

            var Model = new SignInViewModel
	                    {
		                    DisplayTimeError = () =>
		                                       {
			                                       MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
			                                                                       Globals.Resources.GetStringResource( "DeviceTimeError" ),
			                                                                       Globals.Resources.GetStringResource( "Cancel" ) );
		                                       },
		                    DisplayInvalidSignIn = () =>
		                                           {
			                                           MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
			                                                                           Globals.Resources.GetStringResource( "InvalidCredentials" ),
			                                                                           Globals.Resources.GetStringResource( "Cancel" ) );
		                                           }
	                    };

	        BindingContext = Model;

	        var Timer = new Timer( 100 )
	                    {
		                    AutoReset = false
	                    };

	        Timer.Elapsed += ( sender, args ) =>
	                         {
								 ViewModelBase.Dispatcher( () =>
								                           {
									                           Password.Focus();
															   Timer.Dispose();
								                           } );
	                         };
			Timer.Start();

        }
    }
}