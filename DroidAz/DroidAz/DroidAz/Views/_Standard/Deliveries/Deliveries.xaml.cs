﻿using System.Threading.Tasks;
using DroidAz.Views._Standard.Details;
using Globals;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Standard.Deliveries
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Deliveries : ContentView
	{
		public Deliveries()
		{
			InitializeComponent();

			var Details = new Details.Details();
			DetailsArea.Content = Details;
			var DetailsViewModel = (DetailsViewModel)Details.BindingContext;

			BindingContext = new DeliveriesViewModel
			                 {
				                 DetailsViewModel = DetailsViewModel,

				                 OnShowDetails = async ( trip, enablePickupDelivery ) =>
				                                 {
					                                 DetailsViewModel.Trip = trip;
					                                 DetailsViewModel.EnablePickupDelivery = enablePickupDelivery;
					                                 RaiseChild( DetailsArea );
					                                 DetailsArea.IsVisible = true;

					                                 await Task.WhenAll(
					                                                    DetailsArea.FadeTo( 1, 500, Easing.CubicIn ),
					                                                    ListViewArea.FadeTo( 0, 500, Easing.CubicOut )
					                                                   );
					                                 ListViewArea.IsVisible = false;
					                                 BackButton.AllowBackButton = true;
				                                 }
			                 };

			LowerChild( DetailsArea );

			DetailsArea.FadeTo( 0, 10, Easing.Linear );

			async Task ShowListView()
			{
				ListView.SelectedItem = null;
				ListViewArea.IsVisible = true;

				await Task.WhenAll(
				                   DetailsArea.FadeTo( 0, 500, Easing.CubicOut ),
				                   ListViewArea.FadeTo( 1, 500, Easing.CubicIn )
				                  );

				LowerChild( DetailsArea );
				RaiseChild( ListViewArea );
				DetailsArea.IsVisible = false;
				BackButton.AllowBackButton = false;
			}

			async Task OnBackButton()
			{
				await ShowListView();
			}

			DetailsViewModel.OnBackButton = OnBackButton;
			BackButton.OnBackKey = OnBackButton;
		}
	}
}