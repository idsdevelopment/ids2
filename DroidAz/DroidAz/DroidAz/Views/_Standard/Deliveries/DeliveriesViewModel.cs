﻿using System;
using System.Windows.Input;
using DroidAz.Views._Standard.Details;
using DroidAz.Views.BaseViewModels;
using Protocol.Data;
using Utils;
using DeviceTripUpdate = DroidAz.Views.BaseViewModels.DeviceTripUpdate;

namespace DroidAz.Views._Standard.Deliveries
{
	public class DeliveriesViewModel : TripsBaseViewModel
	{
		public bool StartScanner
		{
			get { return Get( () => StartScanner, false ); }
			set { Set( () => StartScanner, value ); }
		}


		public TripDisplayEntry SelectedDisplayEntry
		{
			get { return Get( () => SelectedDisplayEntry, new TripDisplayEntry() ); }
			set { Set( () => SelectedDisplayEntry, value ); }
		}

		public DetailsViewModel DetailsViewModel
		{
			get => _DetailsViewModel;
			set
			{
				_DetailsViewModel = value;
				value.OnPickupDeliveryButton = u =>
				                               {
					                               var T = u.Trip;
					                               var TripId = T.TripId;
					                               var Now = DateTimeOffset.Now;

					                               if( Trips.TryGetValue( TripId, out var Trip ) )
					                               {
						                               Trip.Pieces = T.Pieces;
						                               Trip.Weight = T.Weight;
						                               Trip.Reference = T.Reference;

						                               STATUS Status;

						                               if( (STATUS)T.Status < STATUS.PICKED_UP )
						                               {
							                               Status = STATUS.PICKED_UP;
							                               Trip.POP = u.PopPod;
						                               }
						                               else
						                               {
							                               Status = STATUS.VERIFIED;
							                               Trip.POD = u.PopPod;
						                               }

						                               Trip.Status1 = Status;

						                               var S = u.Signature;

						                               var Gps = Globals.Gps.CurrentGpsPoint;

						                               UpdateFromObject( new DeviceTripUpdate
						                                                 {
							                                                 TripUpdate = new Protocol.Data.DeviceTripUpdate( CurrentVersion.APP_VERSION )
							                                                              {
								                                                              TripId = TripId,

								                                                              BillingNotes = Trip.BillingAddressNotes,
								                                                              DeliveryNotes = Trip.DeliveryAddressNotes,
								                                                              PickupNotes = Trip.PickupAddressNotes,
								                                                              Pieces = Trip.Pieces,
								                                                              Weight = Trip.Weight,
								                                                              Reference = Trip.Reference,
								                                                              PopPod = u.PopPod,
								                                                              Signature = new Protocol.Data.Signature
								                                                                          {
									                                                                          Status = Status,
									                                                                          Points = S.Points,
									                                                                          Width = S.Width,
									                                                                          Height = S.Height,
									                                                                          Date = Now
								                                                                          },
								                                                              Status = Trip.Status1,

								                                                              UpdateLatitude = Gps.Latitude,
								                                                              UpdateLongitude = Gps.Longitude,
								                                                              UpdateTime = DateTimeOffset.Now
							                                                              }
						                                                 } );

						                               var Ti = TripDisplayEntries;
						                               for( var I = Ti.Count; --I >= 0; )
						                               {
							                               var Trp = Ti[ I ];
							                               if( Trp.TripId == TripId )
							                               {
								                               Ti.RemoveAt( I );
								                               if( Status < STATUS.DELIVERED )
								                               {
									                               T.Status = (int)Status;
									                               Ti.Insert( I, T );
								                               }

								                               break;
							                               }
						                               }
					                               }
				                               };
			}
		}

		private bool AllowPickupDelivery => ( !ByLocation && !ByPiece )
		                                    || ( ByLocation && Location.Trim().IsNotNullOrEmpty() );


		private DetailsViewModel _DetailsViewModel;

		public OnShowDetailsEvent OnShowDetails;

		public delegate void OnShowDetailsEvent( TripDisplayEntry trip, bool enablePickupDelivery );


		[DependsUpon( nameof( SelectedDisplayEntry ) )]
		public void WhenSelectedDisplayEntryChanges()
		{
			var Selected = SelectedDisplayEntry;
			if( !( Selected is null ) )
			{
				if( !Selected.ReadByDriver )
				{
					var TripId = Selected.TripId;
					UpdateFromObject( new ReadByDriver { TripId = TripId } );
					Selected.ReadByDriver = true;

					foreach( var (Key, Value) in Trips )
					{
						if( Key == TripId )
						{
							Value.ReadByDriver = true;

							break;
						}
					}
				}

				OnShowDetails?.Invoke( Selected, AllowPickupDelivery );
			}
		}

		public DeliveriesViewModel()
		{
			ShowTripCount = true;
			TripCount = 0;
		}

	#region Barcode

		public string Barcode
		{
			get { return Get( () => Barcode, "" ); }
			set { Set( () => Barcode, value ); }
		}

		[DependsUpon( nameof( Barcode ) )]
		public void WhenBarcodeChanges()
		{
			var BCode = Barcode.Trim();
			if( BCode.IsNotNullOrEmpty() )
				Location = BCode;
		}

	#endregion


	#region Location

		public string Location
		{
			get { return Get( () => Location, "" ); }
			set { Set( () => Location, value ); }
		}


		public bool EnableClearLocation
		{
			get { return Get( () => EnableClearLocation, false ); }
			set { Set( () => EnableClearLocation, value ); }
		}

		[DependsUpon( nameof( Location ) )]
		public void WhenLocationChanges()
		{
			var Loc = Location;
			EnableClearLocation = Loc.Trim().IsNotNullOrEmpty();
			LocationFilter = Loc;
			StartScanner = true;
		}

		public ICommand ClearLocationFilter => Commands[ nameof( ClearLocationFilter ) ];

		public bool CanExecute_ClearLocationFilter()
		{
			return false; // Weird, Xamarin forms takes this as the first value for IsEnabled on a Button
		}

		public void Execute_ClearLocationFilter()
		{
			Location = "";
		}

	#endregion
	}
}