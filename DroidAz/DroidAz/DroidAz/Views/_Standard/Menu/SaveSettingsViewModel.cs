﻿using System;
using System.Collections.Generic;
using System.Text;
using IdsControlLibraryV2.ViewModel;

namespace DroidAz.Views._Standard.Menu
{
	public class SaveSettingsViewModel : ViewModelBase
	{
		[Setting]
		public string LastProgram
		{
			get { return Get( () => LastProgram, "" ); }
			set { Set( () => LastProgram, value ); }
		}

		[DependsUpon( nameof( LastProgram ) )]
		public void WhenSettingChanges()
		{
			SaveSettings();
		}
	}
}