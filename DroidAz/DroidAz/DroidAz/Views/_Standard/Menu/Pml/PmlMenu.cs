﻿using System.Collections.Generic;
using DroidAz.Views._Customers.Pml;
using DroidAz.Views._Customers.Pml.Kiosk;

namespace DroidAz.Views._Standard.Menu.Pml
{
	public static class PmlMenu
	{
		private const string RETURNS = "PmlReturns",
                             KIOSK = "PmlKiosk",
                             CLAIMS = "PmlClaims";

        public static Dictionary<string, Applications.Program> Programs = new Dictionary<string, Applications.Program>
		                                                                  {
			                                                                  {
				                                                                  RETURNS,
				                                                                  new Applications.Program
				                                                                  {
					                                                                  PageType = typeof( Returns )
				                                                                  }
			                                                                  },
                                                                              {
                                                                                  CLAIMS,
                                                                                  new Applications.Program
                                                                                  {
                                                                                      PageType = typeof( DriverClaims )
                                                                                  }
                                                                              },
                                                                              {
                                                                                  KIOSK,
				                                                                  new Applications.Program
				                                                                  {
					                                                                  PageType = typeof( Kiosk ),
                                                                                      Orientation = Globals.Orientation.ORIENTATION.LANDSCAPE
				                                                                  }
			                                                                  }
		                                                                  };
	}
}