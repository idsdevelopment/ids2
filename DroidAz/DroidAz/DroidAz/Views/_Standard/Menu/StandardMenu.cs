﻿using System.Collections.Generic;

namespace DroidAz.Views._Standard.Menu
{
	public static class StandardMenu
	{
		public static Dictionary<string, Applications.Program> Programs = new Dictionary<string, Applications.Program>
		                                                                  {
			                                                                  {
				                                                                  Applications.SIGN_IN,
				                                                                  new Applications.Program
				                                                                  {
					                                                                  PageType = typeof( SignIn ),
                                                                                      Orientation = Globals.Orientation.ORIENTATION.BOTH
				                                                                  }
			                                                                  },
			                                                                  {
				                                                                  Applications.BLANK_PAGE,
				                                                                  new Applications.Program
				                                                                  {
					                                                                  PageType = typeof( BlankPage.BlankPage )
				                                                                  }
			                                                                  },
			                                                                  {
				                                                                  Applications.PICKUP_DELIVER,
				                                                                  new Applications.Program
				                                                                  {
					                                                                  PageType = typeof( Deliveries.Deliveries )
				                                                                  }
			                                                                  }
		                                                                  };
	}
}