﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using DroidAz.Views.BaseViewModels;
using DroidAz.Views.Signature;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Utils;
using Xamarin.Forms;

namespace DroidAz.Views._Standard.Details
{
	public class DetailsViewModel : ViewModelBase
	{
		private const int DIMENSIONS_ROW_HEIGHT = 30;


		public bool EnablePickupDelivery
		{
			get { return Get( () => EnablePickupDelivery, false ); }
			set { Set( () => EnablePickupDelivery, value ); }
		}

		public GridLength DimensionsRowHeight
		{
			get { return Get( () => DimensionsRowHeight, new GridLength( DIMENSIONS_ROW_HEIGHT, GridUnitType.Absolute ) ); }
			set { Set( () => DimensionsRowHeight, value ); }
		}


		public ObservableCollection<TripItemsDisplayEntry> TripDisplayItems
		{
			get { return Get( () => TripDisplayItems, new ObservableCollection<TripItemsDisplayEntry>() ); }
			set { Set( () => TripDisplayItems, value ); }
		}


		public TripDisplayEntry Trip
		{
			get { return Get( () => Trip, new TripDisplayEntry() ); }
			set { Set( () => Trip, value ); }
		}


		public bool ShowPopPod
		{
			get { return Get( () => ShowPopPod, true ); }
			set { Set( () => ShowPopPod, value ); }
		}

		public string PopOrPod
		{
			get { return Get( () => PopOrPod, "" ); }
			set { Set( () => PopOrPod, value ); }
		}


		public string PopOrPodText
		{
			get { return Get( () => PopOrPodText, "" ); }
			set { Set( () => PopOrPodText, value ); }
		}

		public bool DisableEditReference
		{
			get { return Get( () => DisableEditReference, DisableEditPiecesWeight() ); }
		}


		public bool DisableEditPieces
		{
			get { return Get( () => DisableEditPieces, DisableEditPiecesWeight() ); }
		}


		public bool DisableEditWeight
		{
			get { return Get( () => DisableEditWeight, !Preferences.EditPiecesWeight ); }
			set { Set( () => DisableEditWeight, value ); }
		}


		public string EditPieces
		{
			get { return Get( () => EditPieces, "0" ); }
			set { Set( () => EditPieces, value ); }
		}

		public string EditWeight
		{
			get { return Get( () => EditWeight, "0" ); }
			set { Set( () => EditWeight, value ); }
		}

		public bool AllowUndeliverable
		{
			get { return Get( () => AllowUndeliverable, true ); }
			set { Set( () => AllowUndeliverable, value ); }
		}

		public bool ShowUndeliverableButton
		{
			get { return Get( () => ShowUndeliverableButton, true ); }
			set { Set( () => ShowUndeliverableButton, value ); }
		}

		private bool DisableEditPiecesWeight()
		{
			return !Preferences.EditRefrence || ( !( TripDisplayItems is null ) && ( TripDisplayItems.Count > 0 ) );
		}

		[DependsUpon( nameof( TripDisplayItems ) )]
		public void WhenTripDisplayItemsChange()
		{
			DimensionsRowHeight = TripDisplayItems.Count > 0 ? new GridLength( DIMENSIONS_ROW_HEIGHT, GridUnitType.Absolute ) : new GridLength( 0, GridUnitType.Absolute );
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();
			MainPageViewModel.Instance.ProgramTitleVisible = false;
		}

		[DependsUpon( nameof( Trip ) )]
		public void WhenTripChanges()
		{
			var T = Trip;

			EditPieces = $"{T.Pieces:N}";
			EditWeight = $"{T.Weight:N}";

			if( T.IsPickup )
			{
				PopOrPodText = Globals.Resources.GetStringResource( "DetailsPop" );
				PopOrPod = T.POP;
			}
			else
			{
				PopOrPodText = Globals.Resources.GetStringResource( "DetailsPod" );
				PopOrPod = T.POD;
			}

			TripDisplayItems = new ObservableCollection<TripItemsDisplayEntry>( T.Items );
		}

		[DependsUpon250( nameof( EditPieces ) )]
		public void WhenEditPiecesChanges()
		{
			if( !decimal.TryParse( EditPieces.ToNumeric( true ), out var Value ) )
				Value = 0;
			Trip.Pieces = Value;
		}

		[DependsUpon250( nameof( EditWeight ) )]
		public void WhenEditWeightChanges()
		{
			if( !decimal.TryParse( EditWeight.ToNumeric( true ), out var Value ) )
				Value = 0;
			Trip.Weight = Value;
		}

		[DependsUpon( nameof( AllowUndeliverable ) )]
		[DependsUpon( nameof( Trip ) )]
		public void WhenAllowUndeliverableChanges()
		{
			ShowUndeliverableButton = AllowUndeliverable && Trip.IsDelivery;
		}

	#region Tab Control

		public bool DetailsTabVisible
		{
			get { return Get( () => DetailsTabVisible, true ); }
			set { Set( () => DetailsTabVisible, value ); }
		}


		public bool ItemsTabVisible
		{
			get { return Get( () => ItemsTabVisible, false ); }
			set { Set( () => ItemsTabVisible, value ); }
		}


		public ICommand ShowEntry => Commands[ nameof( ShowEntry ) ];

		public void Execute_ShowEntry()
		{
			ItemsTabVisible = false;
			DetailsTabVisible = true;
		}

		public ICommand ShowItems => Commands[ nameof( ShowItems ) ];

		public void Execute_ShowItems()
		{
			DetailsTabVisible = false;
			ItemsTabVisible = true;
		}


		public bool EnableShowItems
		{
			get { return Get( () => EnableShowItems, true ); }
			set { Set( () => EnableShowItems, value ); }
		}

	#endregion

	#region Signature

		public Action ClearSignature;

		private Orientation.ORIENTATION SaveOrientation;

		public bool ShowSignButton
		{
			get { return Get( () => ShowSignButton, true ); }
			set { Set( () => ShowSignButton, value ); }
		}


		public bool SignatureVisible
		{
			get { return Get( () => SignatureVisible, false ); }
			set { Set( () => SignatureVisible, value ); }
		}


		public bool DetailsVisible
		{
			get { return Get( () => DetailsVisible, true ); }
			set { Set( () => DetailsVisible, value ); }
		}

		public ICommand OnSignatureClick => Commands[ nameof( OnSignatureClick ) ];

		public void Execute_OnSignatureClick()
		{
			SaveOrientation = Orientation.LayoutDirection;
			Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
			ClearSignature?.Invoke();
			DetailsVisible = false;
			SignatureVisible = true;
		}


		public SignatureResult Signature
		{
			get { return Get( () => Signature, new SignatureResult() ); }
			set { Set( () => Signature, value ); }
		}

		[DependsUpon( nameof( Signature ) )]
		public void WhenSignatureChanges()
		{
			Orientation.LayoutDirection = SaveOrientation;
			DetailsVisible = true;
			SignatureVisible = false;
		}

	#endregion

	#region Back Button

		public bool ShowPickupDeliveryButton
		{
			get { return Get( () => ShowPickupDeliveryButton, true ); }
			set { Set( () => ShowPickupDeliveryButton, value ); }
		}

		public ICommand OnBack => Commands[ nameof( OnBack ) ];

		public void Execute_OnBack()
		{
			if( OnBackButton != null )
			{
				Dispatcher( async () =>
				            {
					            await OnBackButton?.Invoke();
				            } );
			}
		}

		public delegate Task OnBackButtonEvent();


		public OnBackButtonEvent OnBackButton;

	#endregion

	#region Pickup / Delivery Button

		public class TripUpdate
		{
			public bool IsUndeliverable;

			public string PopPod;

			public SignatureResult Signature;
			public TripDisplayEntry Trip;
		}

		public Action<TripUpdate> OnPickupDeliveryButton;
		public ICommand PickupDeliveryButtonClick => Commands[ nameof( PickupDeliveryButtonClick ) ];

		public async Task Execute_PickupDeliveryButtonClick()
		{
			OnPickupDeliveryButton?.Invoke( new TripUpdate
			                                {
				                                Trip = Trip,
				                                PopPod = PopOrPod,
				                                Signature = Signature
			                                } );

			if( OnBackButton != null )
				await OnBackButton.Invoke();
		}

	#endregion
	}
}