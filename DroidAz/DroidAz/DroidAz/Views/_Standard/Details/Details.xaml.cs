﻿using System.Timers;
using DroidAz.Views.BaseViewModels;
using DroidAz.Views.Signature;
using IdsControlLibraryV2.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Standard.Details
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Details : ContentView
	{
		public TripDisplayEntry Trip
		{
			get => Model.Trip;
			set => Model.Trip = value;
		}

		private readonly DetailsViewModel Model;

		private static void FocusEntry( Entry sender )
		{
			var T = new Timer( 20 );

			T.Elapsed += ( o, args ) =>
			             {
				             T.Stop();

				             ViewModelBase.Dispatcher( () =>
				                                       {
					                                       sender.CursorPosition = 0;
					                                       sender.SelectionLength = sender.Text.Length;
					                                       T.Dispose();
				                                       } );
			             };
			T.Start();
		}

		private void Pieces_OnFocused( object sender, FocusEventArgs e )
		{
			FocusEntry( Pieces );
		}

		private void Weight_OnFocused( object sender, FocusEventArgs e )
		{
			FocusEntry( Weight );
		}

		public Details()
		{
			InitializeComponent();
			var Sig = new Signature.Signature();
			SignatureView.Content = Sig;

			if( BindingContext is DetailsViewModel M )
			{
				Model = M;

				M.ClearSignature = () =>
				                   {
					                   Sig.Clear();
				                   };
			}

			Sig.OnOk = signature =>
			           {
				           Model.Signature = signature;
			           };

			Sig.OnCancel = () =>
			               {
				               Model.Signature = new SignatureResult();
			               };
		}
	}
}