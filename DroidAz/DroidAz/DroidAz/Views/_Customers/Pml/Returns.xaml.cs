﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DroidAz.Views.BaseViewModels;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Protocol.Data._Customers.Pml;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Keyboard = Globals.Keyboard;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Returns : ContentView
	{
		private readonly ReturnsViewModel Model;

		private readonly ReturnDetails DetailsPage;
		private readonly ReturnItems ReturnItemsPage;
		private readonly ReturnsAccept ReturnsAcceptPage;
		private readonly FutileReasons FutileReasonsPage;
		private readonly PodAndSign ReturnsSignPage;
		private readonly Satchel SatchelPage;


		private List<TripItemsDisplayEntry> LocationDisplayEntries = new List<TripItemsDisplayEntry>();

		private enum FUTILE_RETURN_PAGE
		{
			NONE,
			ITEMS_PAGE,
			ACCEPT_PAGE
		}

		private FUTILE_RETURN_PAGE FutileReturnPage = FUTILE_RETURN_PAGE.NONE;

		private bool InLocationChange;

		private async Task ShowTripList()
		{
			ListView.SelectedItem = null;
			TripListArea.IsVisible = true;
			Scanner.IsVisible = true;
			ReturnItemsPage.Scanner.IsVisible = false;
			SatchelPage.Scanner.IsScanning = false;

			await Task.WhenAll(
			                   DetailsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   FutileArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   SatchelArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   SignArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   ItemsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   TripListArea.FadeTo( 1, 500, Easing.CubicIn )
			                  );

			RaiseChild( TripListArea );
			ItemsArea.IsVisible = false;
			SatchelArea.IsVisible = false;
			SignArea.IsVisible = false;
			FutileArea.IsVisible = false;
			DetailsArea.IsVisible = false;

			Scanner.IsScanning = true;
		}

		private async Task ShowTripList( List<string> completedTripIds )
		{
			Model.CompleteTrips( completedTripIds );
			await ShowTripList();
		}


		private async Task ShowItems()
		{
			ItemsArea.IsVisible = true;
			Scanner.IsVisible = false;
			ReturnItemsPage.Scanner.IsVisible = true;

			await Task.WhenAll(
			                   DetailsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   TripListArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   AcceptArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   ItemsArea.FadeTo( 1, 500, Easing.CubicIn )
			                  );

			RaiseChild( ItemsArea );
			DetailsArea.IsVisible = false;
			TripListArea.IsVisible = false;
			AcceptArea.IsVisible = false;
		}


		private async Task ShowAccept()
		{
			AcceptArea.IsVisible = true;
			Scanner.IsVisible = false;

			var Sc = ReturnItemsPage.Scanner;
			Sc.IsVisible = false;
			Sc.IsScanning = false;

			await Task.WhenAll(
			                   DetailsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   TripListArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   ItemsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   SignArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   AcceptArea.FadeTo( 1, 500, Easing.CubicIn )
			                  );

			RaiseChild( AcceptArea );

			DetailsArea.IsVisible = false;
			TripListArea.IsVisible = false;
			ItemsArea.IsVisible = false;
			SignArea.IsVisible = false;
		}

		private async Task ShowFutile( FUTILE_RETURN_PAGE returnPage )
		{
			FutileReturnPage = returnPage;

			FutileArea.IsVisible = true;
			Scanner.IsVisible = false;
			ReturnItemsPage.Scanner.IsVisible = true;

			await Task.WhenAll(
			                   TripListArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   ItemsArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   AcceptArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   FutileArea.FadeTo( 1, 500, Easing.CubicIn )
			                  );

			RaiseChild( FutileArea );

			TripListArea.IsVisible = false;
			ItemsArea.IsVisible = false;
			AcceptArea.IsVisible = false;
		}

		private async Task ShowSign()
		{
			SignArea.IsVisible = true;

			RaiseChild( SignArea );

			await Task.WhenAll(
			                   SatchelArea.FadeTo( 0, 500, Easing.CubicOut ),
			                   SignArea.FadeTo( 1, 500, Easing.CubicIn )
			                  );

			SatchelArea.IsVisible = false;
		}

		private async Task ShowSatchel()
		{
			SatchelArea.IsVisible = true;

			RaiseChild( SatchelArea );

			await Task.WhenAll(
			                   AcceptArea.FadeTo( 0, 500, Easing.CubicIn ),
			                   SignArea.FadeTo( 0, 500, Easing.CubicIn ),
			                   SatchelArea.FadeTo( 1, 500, Easing.CubicOut )
			                  );

			AcceptArea.IsVisible = false;
			SignArea.IsVisible = false;
		}


		private async void FutileReturn()
		{
			switch( FutileReturnPage )
			{
			case FUTILE_RETURN_PAGE.ITEMS_PAGE:
				await ShowItems();

				break;

			case FUTILE_RETURN_PAGE.ACCEPT_PAGE:
				await ShowAccept();

				break;
			}
		}

		private async void ProcessFutile( string reason, bool delete )
		{
			var Status = delete ? STATUS.DELETED : STATUS.ACTIVE;

			var TripIds = ( from E in LocationDisplayEntries
			                select E.OriginalTripId ).ToList();

			Model.UpdateFromObject( new FutileUpdate
			                        {
				                        Futile = new Futile
				                                 {
					                                 ProgramName = CurrentVersion.APP_VERSION,
					                                 TripIds = TripIds,
					                                 Reason = reason,
					                                 Status = Status,
					                                 DateTime = DateTimeOffset.Now
				                                 }
			                        } );

			Model.CompleteTrips( TripIds );
			await ShowTripList();
		}

		public Returns()
		{
			InitializeComponent();
			Keyboard.Show = false;

			RaiseChild( TripListArea );

			DetailsPage = new ReturnDetails();
			DetailsArea.Content = DetailsPage;

			ReturnItemsPage = new ReturnItems();
			ItemsArea.Content = ReturnItemsPage;

			ReturnsAcceptPage = new ReturnsAccept();
			AcceptArea.Content = ReturnsAcceptPage;

			FutileReasonsPage = new FutileReasons
			                    {
				                    OnOk = ProcessFutile,
				                    OnCancel = FutileReturn
			                    };

			FutileArea.Content = FutileReasonsPage;

			ReturnsSignPage = new PodAndSign();
			SignArea.Content = ReturnsSignPage;

			SatchelPage = new Satchel();
			SatchelArea.Content = SatchelPage;

			SatchelPage.OnCancel = () =>
			                       {
				                       ReturnItems.CancelReturnDialog( async () =>
				                                                       {
					                                                       await ShowTripList();
				                                                       } );
			                       };

			SatchelPage.OnAccept = async satchel =>
			                       {
				                       ReturnsSignPage.Satchel = satchel;
				                       SatchelPage.Scanner.IsScanning = false;
				                       Scanner.IsScanning = false;
				                       await ShowSign();
			                       };

			ReturnItemsPage.OnBackButton = async () =>
			                               {
				                               await OnBackButton();
			                               };

			ReturnItemsPage.OnNextButton = async ( scanned, items ) =>
			                               {
				                               ReturnsAcceptPage.TotalScanned = scanned;
				                               ReturnsAcceptPage.Items = items;
				                               ReturnsSignPage.Items = items;
				                               await ShowAccept();
			                               };

			ReturnItemsPage.OnFutile = async () =>
			                           {
				                           await ShowFutile( FUTILE_RETURN_PAGE.ITEMS_PAGE );
			                           };

			ReturnsAcceptPage.OnFutile = async () =>
			                             {
				                             await ShowFutile( FUTILE_RETURN_PAGE.ACCEPT_PAGE );
			                             };

			ReturnsAcceptPage.OnAccept = async () =>
			                             {
				                             ReturnsSignPage.Trip = Model.SelectedDisplayEntry;
				                             ReturnsSignPage.TotalScanned = ReturnsAcceptPage.TotalScanned;
				                             Scanner.IsScanning = false;
				                             ReturnItemsPage.Scanner.IsScanning = false;
				                             SatchelPage.Scanner.IsScanning = true;

				                             await ShowSatchel();
			                             };

			ReturnsAcceptPage.OnBack = async () =>
			                           {
				                           await ShowItems();
				                           ReturnItemsPage.ScanItems( false );
			                           };

			ReturnsSignPage.OnBack = async () =>
			                         {
				                         SatchelPage.Scanner.IsScanning = true;
				                         await ShowSatchel();
			                         };

			ReturnsSignPage.OnPickupComplete = async ( tripIds, satchel ) =>
			                                   {
				                                   await ShowTripList();
				                                   Model.UpdateFromObject( new SatchelUpdate { Satchel = satchel } );
			                                   };

			var DetailsViewModel = (ReturnDetailsViewModel)DetailsPage.BindingContext;

			if( BindingContext is ReturnsViewModel M )
			{
				Model = M;
				M.ShowTripCount = true;

				M.DetailsViewModel = DetailsViewModel;

				M.OnShowDetails = async trip =>
				                  {
					                  if( !InLocationChange )
					                  {
						                  DetailsViewModel.Trip = trip;
						                  RaiseChild( DetailsArea );
						                  DetailsArea.IsVisible = true;

						                  await Task.WhenAll(
						                                     DetailsArea.FadeTo( 1, 500, Easing.CubicIn ),
						                                     TripListArea.FadeTo( 0, 500, Easing.CubicOut )
						                                    );

						                  TripListArea.IsVisible = false;
					                  }
				                  };

				M.OnShowItems = async location =>
				                {
					                InLocationChange = true;

					                try
					                {
						                M.ShowScannedCount = true;

						                var Itms = new List<TripItemsDisplayEntry>();
						                var HaveTrip = false;

						                foreach( var Entry in Model.TripDisplayEntries )
						                {
							                if( Entry.PickupBarcode == location && (STATUS)Entry.Status < STATUS.PICKED_UP )
							                {
								                if( !HaveTrip )
								                {
									                HaveTrip = true;
									                Model.SelectedDisplayEntry = Entry;
								                }

								                Itms.AddRange( Entry.Items );
							                }
						                }

						                LocationDisplayEntries = Itms;
						                ReturnItemsPage.Items = Itms;
						                await ShowItems();
						                ReturnItemsPage.ScanItems( true );
					                }
					                finally
					                {
						                InLocationChange = false;
					                }
				                };

				M.OnNoItemsForLocation = () =>
				                         {
					                         ViewModelBase.Dispatcher( async () =>
					                                                   {
						                                                   Sounds.PlayBadScanSound?.Invoke();

						                                                   await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
						                                                                                         Globals.Resources.GetStringResource( "PmlNoPickup" ),
						                                                                                         Globals.Resources.GetStringResource( "Cancel" ) );

						                                                   Scanner.IsScanning = true;
					                                                   } );
				                         };
			}

			async Task OnBackButton()
			{
				Model.ShowTripCount = true;
				Model.Location = "";
				Model.SelectedDisplayEntry = null;
				await ShowTripList();
				BackButton.AllowBackButton = false;
				Scanner.IsScanning = true;
			}

			DetailsViewModel.OnBackButton = OnBackButton;
			BackButton.OnBackKey = OnBackButton;

			DetailsViewModel.OnPickupTrip = async entry =>
			                                {
				                                await ShowItems();
			                                };
			Scanner.IsScanning = true;
		}
	}
}