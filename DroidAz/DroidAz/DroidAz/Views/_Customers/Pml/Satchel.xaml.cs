﻿using System;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Satchel : ContentView
	{
		public Action OnCancel;

		public SatchelViewModel.OnAcceptEvent OnAccept;

		private readonly SatchelViewModel Model;

		public string SatchelId
		{
			get => Model.Satchel;
			set => Model.Satchel = value;
		}

		public Satchel()
		{
			InitializeComponent();

			if( BindingContext is SatchelViewModel M )
			{
				Model = M;

				M.OnBadSatchel = () =>
				                 {
					                 ViewModelBase.Dispatcher( async () =>
					                                           {
						                                           Sounds.PlayBadScanSound?.Invoke();

						                                           await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
						                                                                                 Globals.Resources.GetStringResource( "PmlNotSatchel" ),
						                                                                                 Globals.Resources.GetStringResource( "Cancel" ) );

						                                           Scanner.IsScanning = true;
					                                           } );
				                 };

				M.OnAccept = satchel =>
				             {
					             OnAccept?.Invoke( satchel );
				             };

				M.OnCancel = () =>
				             {
					             OnCancel?.Invoke();
				             };
			}
		}
	}
}