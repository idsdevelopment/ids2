﻿using System;
using System.Collections.Generic;
using DroidAz.Views.BaseViewModels;
using Utils;

namespace DroidAz.Views._Customers.Pml
{
	public class ReturnsViewModel : TripsBaseViewModel
	{
		public TripDisplayEntry SelectedDisplayEntry
		{
			get { return Get( () => SelectedDisplayEntry, new TripDisplayEntry() ); }
			set { Set( () => SelectedDisplayEntry, value ); }
		}

		public ReturnDetailsViewModel DetailsViewModel;
		public OnShowDetailsEvent OnShowDetails;
		public OnShowItemsEvent OnShowItems;

		public delegate void OnShowDetailsEvent( TripDisplayEntry trip );

		public delegate void OnShowItemsEvent( string location );

		[DependsUpon( nameof( SelectedDisplayEntry ) )]
		public void WhenSelectedDisplayEntryChanges()
		{
			var Selected = SelectedDisplayEntry;

			if( !( Selected is null ) )
			{
				if( !Selected.ReadByDriver )
				{
					var TripId = Selected.TripId;
					UpdateFromObject( new ReadByDriver { TripId = TripId } );
					Selected.ReadByDriver = true;

					foreach( var (Key, Value) in Trips )
					{
						if( Key == TripId )
						{
							Value.ReadByDriver = true;

							break;
						}
					}
				}

				OnShowDetails?.Invoke( Selected );
			}
		}

		public void CompleteTrips( List<string> completedTripIds )
		{
			var Trps = Trips;

			foreach( var TripId in completedTripIds )
			{
				if( Trps.ContainsKey( TripId ) )
					Trps.Remove( TripId );
			}

			var DTrips = TripDisplayEntries;

			for( var I = DTrips.Count; --I >= 0; )
			{
				if( completedTripIds.Contains( DTrips[ I ].TripId ) )
					DTrips.RemoveAt( I );
			}
		}

	#region Location

		public Action OnNoItemsForLocation;

		public string Location
		{
			get { return Get( () => Location, "" ); }
			set { Set( () => Location, value ); }
		}


		[DependsUpon250( nameof( Location ) )]
		public void WhenLocationChanges()
		{
			var Loc = Location.Trim();

			if( Loc.IsNotNullOrEmpty() )
			{
				foreach( var Trip in Trips )
				{
					var T = Trip.Value;

					if( ( T.PickupAddressBarcode == Loc ) && ( string.Compare( T.PackageType.Trim(), "Satchel", StringComparison.OrdinalIgnoreCase ) != 0 ) )
					{
						Location = "";
						OnShowItems?.Invoke( Loc );

						return;
					}
				}

				OnNoItemsForLocation?.Invoke();
			}
		}

	#endregion
	}
}