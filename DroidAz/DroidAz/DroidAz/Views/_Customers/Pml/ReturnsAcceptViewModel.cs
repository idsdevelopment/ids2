﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using IdsControlLibraryV2.ViewModel;

namespace DroidAz.Views._Customers.Pml
{
	public class ReturnsAcceptViewModel : ViewModelBase
	{
		public int TotalScanned
		{
			get { return Get( () => TotalScanned, 0 ); }
			set { Set( () => TotalScanned, value ); }
		}

		public ObservableCollection<PmlTripItemsDisplayEntry> Items
		{
			get { return Get( () => Items, new ObservableCollection<PmlTripItemsDisplayEntry>() ); }
			set { Set( () => Items, value ); }
		}


		public PmlTripItemsDisplayEntry SelectedItem
		{
			get { return Get( () => SelectedItem, (PmlTripItemsDisplayEntry)null ); }
			set { Set( () => SelectedItem, value ); }
		}


		[DependsUpon( nameof( Items ) )]
		public void WhenItemsChange()
		{
			if( !( Items is null ) )
			{
				foreach( var Item in Items )
					Item.DontHide = true;
			}
		}

	#region Reset Item

		public Func<Task<bool>> OnResetItem;
		public ICommand RestItem => Commands[ nameof( RestItem ) ];

		public async void Execute_ResetItem()
		{
			var Item = SelectedItem;

			if( !( Item is null ) && !( OnResetItem is null ) && await OnResetItem() )
			{
				Item.Scanned = 0;
				var P = Item.Pieces;
				Item.Balance = P;
				TotalScanned -= (int)P;
			}
		}

	#endregion

	#region Futile Button

		public Action OnFutile;

		public ICommand Futile => Commands[ nameof( Futile ) ];

		public void Execute_Futile()
		{
			OnFutile?.Invoke();
		}

	#endregion

	#region Accept Button

		public Action OnAccept;

		public ICommand Accept => Commands[ nameof( Accept ) ];

		public void Execute_Accept()
		{
			OnAccept?.Invoke();
		}

	#endregion

	#region Back Button

		public Action OnBack;

		public ICommand Back => Commands[ nameof( Back ) ];

		public void Execute_Back()
		{
			OnBack?.Invoke();
		}

	#endregion
	}
}