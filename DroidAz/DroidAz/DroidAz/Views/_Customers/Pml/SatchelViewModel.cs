﻿using System;
using System.Windows.Input;
using IdsControlLibraryV2.ViewModel;
using Utils;

namespace DroidAz.Views._Customers.Pml
{
	public class SatchelViewModel : ViewModelBase
	{
	#region Satchel

		public Action OnBadSatchel;

		public delegate void OnAcceptEvent( string satchel );

		public OnAcceptEvent OnAccept;


		public string Satchel
		{
			get { return Get( () => Satchel, "" ); }
			set { Set( () => Satchel, value ); }
		}

		[DependsUpon250( nameof( Satchel ) )]
		public void WhenSatchelChanges()
		{
			var S = Satchel.Trim();

			if( S.IsNotNullOrEmpty() )
			{
				var Enab = S.StartsWith( "AUDR" );

				if( !Enab )
					OnBadSatchel?.Invoke();
				else
					OnAccept?.Invoke( S );
			}
		}

	#endregion

	#region Cancel Button

		public Action OnCancel;
		public ICommand Cancel => Commands[ nameof( Cancel ) ];

		public void Execute_Cancel()
		{
			OnCancel?.Invoke();
		}

	#endregion
	}
}