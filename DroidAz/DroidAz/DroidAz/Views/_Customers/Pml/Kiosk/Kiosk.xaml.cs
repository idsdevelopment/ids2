﻿using Globals;
using IdsControlLibraryV2.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml.Kiosk
{
    [XamlCompilation( XamlCompilationOptions.Compile )]
    public partial class Kiosk : ContentView
    {
        private readonly KioskViewModel Model;

        public Kiosk()
        {
            InitializeComponent();
            Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;

            void Close()
            {
                ViewModelBase.Dispatcher( () =>
                {
                    Model.MenuButtonsVisible = true;
                    ProgramArea.Content = null;
                } );
            }

            if( BindingContext is KioskViewModel M )
            {
                Model = M;

                M.RunVerify = () =>
                                  {
                                      ViewModelBase.Dispatcher( () =>
                                                                {
                                                                    var V = new Verify();
                                                                    ProgramArea.Content = V;
                                                                    if( V.BindingContext is VerifyViewModel VModel )
                                                                        VModel.OnClose = Close;
                                                                } );
                                  };

                M.RunClaim = () =>
                                 {
                                     ViewModelBase.Dispatcher( () =>
                                     {
                                         var C = new Claim();
                                         ProgramArea.Content = C;
                                         if( C.BindingContext is ClaimViewModel CModel )
                                             CModel.OnClose = Close;
                                     } );
                                 };
            }
        }
    }
}