﻿using IdsControlLibraryV2.ViewModel;
using System;
using System.Windows.Input;

namespace DroidAz.Views._Customers.Pml.Kiosk
{
	public class KioskViewModel : ViewModelBase
	{
		public Action RunVerify,
		              RunClaim;

		public bool MenuButtonsVisible
		{
			get { return Get( () => MenuButtonsVisible, true ); }
			set { Set( () => MenuButtonsVisible, value ); }
		}


        public bool ProgramAreaVisible	
        {
            get { return Get(() => ProgramAreaVisible, false); }
            set { Set(() => ProgramAreaVisible, value); }
        }


		[DependsUpon(nameof(MenuButtonsVisible))]
        public void WhenMenuButtonsVisibleChange()
        {
	        ProgramAreaVisible = !MenuButtonsVisible;
        }


        public ICommand Verify => Commands[ nameof( Verify ) ];
        public void Execute_Verify()
        {
            MenuButtonsVisible = false;
	        RunVerify?.Invoke();
        }

        public ICommand Claim => Commands[ nameof( Claim ) ];
        public void Execute_Claim()
        {
            MenuButtonsVisible = false;
            RunClaim?.Invoke();
        }

        public ICommand Exit => Commands[ nameof( Exit ) ];

        public void Execute_Exit()
        {
            MainPageViewModel.Instance.Execute_SignOut();
        }
    }
}