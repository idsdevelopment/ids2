﻿using AzureRemoteService;
using DroidAz.Droid.Annotations;
using DroidAz.Views.BaseViewModels;
using IdsControlLibraryV2.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Utils;

namespace DroidAz.Views._Customers.Pml.Kiosk
{
    public class KioskTripItemsDisplayEntry : TripItemsDisplayEntry, INotifyPropertyChanged
    {
        public KioskTripItemsDisplayEntry( TripItemsDisplayEntry items ) : base( items )
        {
        }

        public override decimal Balance
        {
            get => base.Balance;
            set
            {
                base.Balance = value;
                OnPropertyChanged();
                OverScanned = value < 0;
                IsVisible = value != 0;
            }
        }


        public override decimal Scanned
        {
            get => base.Scanned;
            set
            {
                base.Scanned = value;
                OnPropertyChanged();
            }
        }

        public override bool OverScanned
        {
            get => base.OverScanned;
            set
            {
                base.OverScanned = value;
                OnPropertyChanged();
            }
        }


        public override bool IsVisible
        {
            get => base.IsVisible;
            set
            {
                base.IsVisible = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

    }

    public class VerifyViewModel : TripsBaseViewModel
    {
        private const string PML_SATCHEL_BARCODE = "PmlSatchelBarcode",
                             BARCODE = "Barcode";

        protected override void OnInitialised()
        {
            base.OnInitialised();
            SetSatchelBarcode();
            CancelButtonVisible = false;
            CompleteButtonVisible = false;
            Scanning = true;
        }

        private void SetSatchelBarcode()
        {
            PlaceholderText = Globals.Resources.GetStringResource( !ScanningItems ? PML_SATCHEL_BARCODE : BARCODE );
        }


        public Action OnClose, OnInvalidAudrNumber, OnNoSatchelsFound;
        public Func<Task> OnVerifyOk;
        public Func<KioskTripItemsDisplayEntry, Task<bool>> OnResetScan;


        #region Barcode

        public string Barcode
        {
            get { return Get( () => Barcode, "" ); }
            set { Set( () => Barcode, value ); }
        }

        [DependsUpon250( nameof( Barcode ) )]
        public async void WhenBarcodeChamges()
        {
            var Code = Barcode;
            if( Code.IsNotNullOrWhiteSpace() )
            {
                if( ScanningItems )
                {
                    foreach( var I in Trip.Items )
                    {
                        if( I.Barcode == Code )
                        {
                            I.Scanned++;
                            I.Balance--;
                            break;
                        }
                    }

                    Scanning = true;

                    foreach( var I in Trip.Items )
                    {
                        if( I.Balance != 0 )
                        {
                            CompleteButtonVisible = false;
                            return;
                        }
                    }

                    CompleteButtonVisible = true;
                }
                else
                {
                    if( !Code.StartsWith( "AUDR" ) )
                        OnInvalidAudrNumber?.Invoke();
                    else
                    {
                        var Satchels = await Azure.Client.RequestGetTripsStartingWith( new Protocol.Data.SearchTripsByStatus
                        {
                            StartStatus = Protocol.Data.STATUS.PICKED_UP,
                            EndStatus = Protocol.Data.STATUS.PICKED_UP,
                            TripId = Code
                        } );

                        if( !( Satchels is null ) && Satchels.Count > 0 )
                        {
                            var Trp = Satchels[ 0 ];
                            if( Trp.PackageType.Compare( "SATCHEL", StringComparison.OrdinalIgnoreCase ) != 0 )
                                OnNoSatchelsFound?.Invoke();
                            else
                            {
                                var T = new TripDisplayEntry( Trp );
                                var Items = T.Items;
                                var Count = Items.Count;
                                for( var I = 0; I < Count; I++ )
                                    Items[ I ] = new KioskTripItemsDisplayEntry( Items[ I ] );

                                Trip = T;

                                CancelButtonVisible = true;
                                Scanning = true;
                                ScanningItems = true;
                                SetSatchelBarcode();
                            }
                        }
                        else
                            OnNoSatchelsFound?.Invoke();
                    }
                }
            }
        }

        #endregion

        #region BarcodeScaner

        public string PlaceholderText
        {
            get { return Get( () => PlaceholderText, "" ); }
            set { Set( () => PlaceholderText, value ); }
        }


        public bool Scanning
        {
            get { return Get( () => Scanning, false ); }
            set { Set( () => Scanning, value ); }
        }


        #endregion

        #region ListView
        public TripDisplayEntry Trip
        {
            get { return Get( () => Trip, new TripDisplayEntry() ); }
            set { Set( () => Trip, value ); }
        }



        public KioskTripItemsDisplayEntry SelectedItem
        {
            get { return Get( () => SelectedItem, (KioskTripItemsDisplayEntry)null ); }
            set { Set( () => SelectedItem, value ); }
        }

        [DependsUpon( nameof( SelectedItem ) )]
        public async void WhenSelectedItemChanges()
        {
            Scanning = false;
            var Item = SelectedItem;
            if( !( Item is null ) && Item.OverScanned )
            {
                if( !( OnResetScan is null ) )
                {
                    if( await OnResetScan( Item ) )
                    {
                        Item.Balance = Item.Pieces;
                        Item.Scanned = 0;
                    }
                }
            }
            SelectedItem = null;
            Scanning = true;
        }

        #endregion


        #region Buttons
        public ICommand Close => Commands[ nameof( Close ) ];
        public void Execute_Close()
        {
            OnClose?.Invoke();
        }

        public ICommand Cancel => Commands[ nameof( Cancel ) ];
        public void Execute_Cancel()
        {
            CompleteButtonVisible = false;
            CancelButtonVisible = false;
            ScanningItems = false;
            SetSatchelBarcode();
            Scanning = true;
        }

        public bool CancelButtonVisible
        {
            get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
            set { Set( () => CancelButtonVisible, value ); }
        }



        public bool CloseButtonVisible
        {
            get { return Get( () => CloseButtonVisible, IsInDesignMode ); }
            set { Set( () => CloseButtonVisible, value ); }
        }


        public bool ScanningItems
        {
            get { return Get( () => ScanningItems, IsInDesignMode ); }
            set { Set( () => ScanningItems, value ); }
        }

        [DependsUpon( nameof( CancelButtonVisible ) )]
        public void WhenCancelButtonVisibleChanges()
        {
            CloseButtonVisible = !CancelButtonVisible;
            SetSatchelBarcode();
        }

        public bool CompleteButtonVisible
        {
            get { return Get( () => CompleteButtonVisible, IsInDesignMode ); }
            set { Set( () => CompleteButtonVisible, value ); }
        }

        public ICommand Complete => Commands[ nameof( Complete ) ];
        public async void Execute_Complete()
        {
            var Point = Globals.Gps.CurrentGpsPoint;

            UpdateFromObject( new PmlVerifySatchel
            {
                ProgramName = $"Kiosk Verify - {CurrentVersion.APP_VERSION}",
                TripId = Trip.TripId,
                UserName = Modifications.Users.UserName,

				DateTime = DateTimeOffset.Now,
                Latitude = Point.Latitude,
                Longitude = Point.Longitude
            } );

            if( !( OnVerifyOk is null ) )
                await OnVerifyOk();

            Execute_Cancel();
        }

        #endregion
    }
}
