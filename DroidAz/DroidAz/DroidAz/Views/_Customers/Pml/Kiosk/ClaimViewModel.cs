﻿using DroidAz.Views.BaseViewModels;
using IdsControlLibraryV2.ViewModel;
using System;
using System.Windows.Input;
using AzureRemoteService;
using Utils;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;

namespace DroidAz.Views._Customers.Pml.Kiosk
{
    public class ClaimViewModel : TripsBaseViewModel
    {
        public Action OnClose;
        public Action<string> OnTripNotFound, OnAlreadyScanned;

        private string ScanCage, ScanShipments, CageBarcode, BarcodeText;

        protected override void OnInitialised()
        {
            base.OnInitialised();

            ScanCage = Globals.Resources.GetStringResource( "PmlScanCage" );
            ScanShipments = Globals.Resources.GetStringResource( "PmlScanShipments" );
            CageBarcode = Globals.Resources.GetStringResource( "PmlCageBarcode" );
            BarcodeText = Globals.Resources.GetStringResource( "Barcode" );

            OTrips = new ObservableCollection<TripDisplayEntry>();
            CloseButtonVisible = true;
            ScanningCage = true;
        }

        #region Cage

        public string Cage
        {
            get { return Get( () => Cage, "123456" ); }
            set { Set( () => Cage, value ); }
        }



        public bool ShowCage
        {
            get { return Get( () => ShowCage, IsInDesignMode ); }
            set { Set( () => ShowCage, value ); }
        }

        #endregion

        public string Caption
        {
            get { return Get( () => Caption, "Caption" ); }
            set { Set( () => Caption, value ); }
        }


        #region Scanner

        public string PlaceholderText
        {
            get { return Get( () => PlaceholderText, "" ); }
            set { Set( () => PlaceholderText, value ); }
        }

        public bool ScanningCage
        {
            get { return Get( () => ScanningCage, false ); }
            set { Set( () => ScanningCage, value ); }
        }

        [DependsUpon( nameof( ScanningCage ) )]
        public void WhenScanningCageChanges()
        {
            if( ScanningCage )
            {
                ShowCage = false;
                Caption = ScanCage;
                PlaceholderText = CageBarcode;
                CloseButtonVisible = true;
            }
            else
            {
                ShowCage = true;
                Cage = Barcode;
                Caption = ScanShipments;
                PlaceholderText = BarcodeText;
                CloseButtonVisible = false;
                CompleteButtonVisible = false;
            }
        }

        public bool Scanning
        {
            get { return Get( () => Scanning, false ); }
            set { Set( () => Scanning, value ); }
        }


        public string Barcode
        {
            get { return Get( () => Barcode, "" ); }
            set { Set( () => Barcode, value ); }
        }

        [DependsUpon350( nameof( Barcode ) )]
        public void WhenBarcodeChanges()
        {
            if( ScanningCage )
            {
                if( Barcode.Trim().IsNotNullOrEmpty() )
                    ScanningCage = false;
            }
            else
            {
                var BCode = Barcode.Trim();
                if( BCode.IsNotNullOrWhiteSpace() )
                {
                    foreach( var T in OTrips )
                    {
                        if( T.TripId == BCode )
                        {
                            Dispatcher( () =>
                            {
                                Globals.Sounds.PlayBadScanSound();
                                OnAlreadyScanned?.Invoke( BCode );
                            } );

                            Scanning = true;
                            return;
                        }
                    }


                    Task.Run( async () =>
                    {
                        var Trip = await Azure.Client.RequestGetTrip( new Protocol.Data.GetTrip { TripId = BCode, Signatures = false } );
                        Dispatcher( () =>
                        {
                            if( !( Trip is null ) && Trip.Ok )
                            {
                                OTrips.Add( new TripDisplayEntry( Trip ) );
                                RaisePropertyChanged( nameof( OTrips ) );
                                CloseButtonVisible = false;
                                CompleteButtonVisible = true;
                            }
                            else
                            {
                                Globals.Sounds.PlayBadScanSound();
                                OnTripNotFound?.Invoke( BCode );
                            }
                        } );
                    } );
                }
            }
            Scanning = true;
        }

        #endregion

        #region Trips

        public ObservableCollection<TripDisplayEntry> OTrips
        {
            get { return Get( () => OTrips, new ObservableCollection<TripDisplayEntry>() ); }
            set { Set( () => OTrips, value ); }
        }

        public TripDisplayEntry SelectedItem
        {
            get { return Get( () => SelectedItem, (TripDisplayEntry)null ); }
            set { Set( () => SelectedItem, value ); }
        }

        #endregion


        #region Buttons

        public bool CloseButtonVisible
        {
            get { return Get( () => CloseButtonVisible, IsInDesignMode ); }
            set { Set( () => CloseButtonVisible, value ); }
        }

        [DependsUpon( nameof( CloseButtonVisible ) )]
        public void WhenCloseButtonVisibleChanges()
        {
            var Vis = !CloseButtonVisible;
            CancelButtonVisible = Vis;
            CompleteButtonVisible = Vis;
        }

        public ICommand Close => Commands[ nameof( Close ) ];
        public void Execute_Close()
        {
            OnClose?.Invoke();
        }


        public bool CancelButtonVisible
        {
            get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
            set { Set( () => CancelButtonVisible, value ); }
        }


        public ICommand Cancel => Commands[ nameof( Cancel ) ];
        public void Execute_Cancel()
        {
            CloseButtonVisible = true;
            OTrips = new ObservableCollection<TripDisplayEntry>();
            ScanningCage = true;
            Scanning = true;
        }

        public bool CompleteButtonVisible
        {
            get { return Get( () => CompleteButtonVisible, IsInDesignMode ); }
            set { Set( () => CompleteButtonVisible, value ); }
        }


        public ICommand Complete => Commands[ nameof( Complete ) ];
        public void Execute_Complete()
        {
            var Trps = OTrips;

            var ClaimTrips = new Protocol.Data._Customers.Pml.ClaimTrips
            {
                ProgramName = $"Kiosk Claim - {CurrentVersion.APP_VERSION}",
                UserName = Modifications.Users.UserName,
                Cage = Cage,
                TripIds = ( from T in Trps
                            select T.TripId ).ToList()
            };

            UpdateFromObject( new PmlClaimTrips { Trips = ClaimTrips } );

            Execute_Cancel();
        }

        #endregion
    }
}
