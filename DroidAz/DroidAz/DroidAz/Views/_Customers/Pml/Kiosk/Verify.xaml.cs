﻿using IdsControlLibraryV2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml.Kiosk
{
    [XamlCompilation( XamlCompilationOptions.Compile )]
    public partial class Verify : ContentView
    {
        public Verify()
        {
            InitializeComponent();
            if( BindingContext is VerifyViewModel Model )
            {
                void ShowError( string errorIId )
                {
                    ViewModelBase.Dispatcher( async () =>
                    {
                        Globals.Sounds.PlayBadScanSound?.Invoke();

                        await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
                                                              Globals.Resources.GetStringResource( errorIId ),
                                                              Globals.Resources.GetStringResource( "Cancel" ) );

                        Scanner.IsScanning = true;
                    } );
                };

                Model.OnInvalidAudrNumber = () =>
                {
                    ShowError( "PmlNotSatchel" );
                };

                Model.OnNoSatchelsFound = () =>
                {
                    ShowError( "PmlNoSatchel" );
                };

                Model.OnResetScan = async item =>
                {
                    var Ok = Globals.Resources.GetStringResource( "Ok" );

                    var Result = await MainPage.Instance.DisplayActionSheet( Globals.Resources.GetStringResource( "PmlResetScan" ),
                                                                             Globals.Resources.GetStringResource( "Cancel" ),
                                                                             Globals.Resources.GetStringResource( Ok ) );
                    return Result == Ok;

                };

                Model.OnVerifyOk = async () =>
                {
                    await MainPage.Instance.DisplayActionSheet( Globals.Resources.GetStringResource( "PmlVerifyOk" ),
                                                                null,
                                                                Globals.Resources.GetStringResource( "Ok" ) );
                };
            }
            Scanner.IsScanning = true;
        }
    }
}