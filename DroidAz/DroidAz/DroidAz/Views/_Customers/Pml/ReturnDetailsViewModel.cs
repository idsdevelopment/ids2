﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DroidAz.Views.BaseViewModels;
using IdsControlLibraryV2.ViewModel;

namespace DroidAz.Views._Customers.Pml
{
	public class ReturnDetailsViewModel : ViewModelBase
	{
		public const string SATCHEL = "Satchel";
		public TripDisplayEntry Trip
		{
			get { return Get( () => Trip, new TripDisplayEntry() ); }
			set { Set( () => Trip, value ); }
		}

		[DependsUpon(nameof(Trip))]
		public void WhenTripChanges()
		{
            EnablePickupButton = string.Compare(Trip.PackageType, SATCHEL, true) != 0;
		}

        #region PickupButton

        public bool EnablePickupButton		
        {
            get { return Get(() => EnablePickupButton, true); }
            set { Set(() => EnablePickupButton, value); }
        }


        public Action<TripDisplayEntry> OnPickupTrip;

		public ICommand OnPickup => Commands[ nameof( OnPickup ) ];

		public void Execute_OnPickup()
		{
			OnPickupTrip?.Invoke( Trip );
		}

		
	#endregion

	#region Back Button

		public ICommand OnBack => Commands[ nameof( OnBack ) ];

		public void Execute_OnBack()
		{
			if( OnBackButton != null )
			{
				Dispatcher( async () =>
				            {
					            await OnBackButton?.Invoke();
				            } );
			}
		}

		public delegate Task OnBackButtonEvent();


		public OnBackButtonEvent OnBackButton;

	#endregion
	}
}