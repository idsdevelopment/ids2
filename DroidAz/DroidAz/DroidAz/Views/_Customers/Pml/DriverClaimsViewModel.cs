﻿using DroidAz.Views.BaseViewModels;
using DroidAz.Views.Signature;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data._Customers.Pml;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Utils;

namespace DroidAz.Views._Customers.Pml
{
    public class DriverClaimsViewModel : TripsBaseViewModel
    {
        private string ScanCage, ScanScatchels, CageBarcode, SatchelBarcode;

        public Func<Task<bool>> OnCancel;
        public Action OnNotASatchel;
        public Action<string> OnAlreadyScanned;
        protected override void OnInitialised()
        {
            base.OnInitialised();

            ScanCage = Globals.Resources.GetStringResource( "PmlScanCage" );
            ScanScatchels = Globals.Resources.GetStringResource( "PmlScanSatchels" );
            CageBarcode = Globals.Resources.GetStringResource( "PmlCageBarcode" );
            SatchelBarcode = Globals.Resources.GetStringResource( "PmlSatchelBarcode" );

            Satchels = new ObservableCollection<TripDisplayEntry>();

            ScanningCage = true;
            PopAndSign = false;
        }

        #region PopAndSign

        public Action ClearSignature;

        private Globals.Orientation.ORIENTATION SaveOrientation;

        public string Pop
        {
            get { return Get( () => Pop, "" ); }
            set { Set( () => Pop, value ); }
        }

        [DependsUpon( nameof( Signature ) )]
        [DependsUpon250( nameof( Pop ) )]
        public void WhenPopChanges()
        {
            CompleteEnable = Pop.IsNotNullOrWhiteSpace() && !( Signature is null );
        }


        public bool PopAndSign
        {
            get { return Get( () => PopAndSign, IsInDesignMode ); }
            set { Set( () => PopAndSign, value ); }
        }

        public bool NotPopAndSign
        {
            get { return Get( () => NotPopAndSign, IsInDesignMode ); }
            set { Set( () => NotPopAndSign, value ); }
        }

        [DependsUpon( nameof( PopAndSign ) )]
        public void WhenPopAndSignChanges()
        {
            NotPopAndSign = !PopAndSign;
        }

        public bool CompleteEnable
        {
            get { return Get( () => CompleteEnable, false ); }
            set { Set( () => CompleteEnable, value ); }
        }

        public ICommand Complete => Commands[ nameof( Complete ) ];
        public void Execute_Complete()
        {
            var Now = DateTimeOffset.Now;
            var Gps = Globals.Gps.CurrentGpsPoint;
            var Sig = Signature;

            var Satchels = new ClaimSatchels
            {
                ProgramName = $"Driver Claims - {CurrentVersion.APP_VERSION}",
                UserName = Modifications.Users.UserName,
                ClaimTime = Now,
                Latitude = Gps.Latitude,
                Longitude = Gps.Longitude,
                Cage = Cage,
                POP = Pop,
                Signature = new Protocol.Data.Signature
                {
                    Date = Now,
                    Height = Sig.Height,
                    Width = Sig.Width,
                    Points = Sig.Points,
                    Status = Protocol.Data.STATUS.PICKED_UP,
                    Status1 = Protocol.Data.STATUS1.CLAIMED
                },

                SatchelIds = ( from S in this.Satchels
                               select S.TripId ).ToList()
            };

            UpdateFromObject( new PmlClaimSatchel { Satchels = Satchels } );

            ScanningCage = true;
        }


        public ICommand Back => Commands[ nameof( Back ) ];
        public void Execute_Back()
        {
            PopAndSign = false;
            Scanning = true;
        }

        public SignatureResult Signature
        {
            get { return Get( () => Signature, (SignatureResult)null ); }
            set { Set( () => Signature, value ); }
        }


        public bool SignatureNotVisible
        {
            get { return Get( () => SignatureNotVisible, true ); }
            set { Set( () => SignatureNotVisible, value ); }
        }

        public bool SignatureVisible
        {
            get { return Get( () => SignatureVisible, false ); }
            set { Set( () => SignatureVisible, value ); }
        }

        [DependsUpon( nameof( SignatureVisible ) )]
        public void WhenSignatureVisibleChanges()
        {
            SignatureNotVisible = !SignatureVisible;
        }


        public ICommand Sign => Commands[ nameof( Sign ) ];
        public void Execute_Sign()
        {
            SaveOrientation = Globals.Orientation.LayoutDirection;
            Globals.Orientation.LayoutDirection = Globals.Orientation.ORIENTATION.LANDSCAPE;
            ClearSignature?.Invoke();
            SignatureVisible = true;
        }


        public void OnSignatureOk( SignatureResult signature )
        {
            SignatureVisible = false;
            Globals.Orientation.LayoutDirection = SaveOrientation;
            Signature = signature;
        }

        public void OnSignatureCancel()
        {
            OnSignatureOk( null );
        }

        #endregion

        #region Cage

        public bool ShowCage
        {
            get { return Get( () => ShowCage, IsInDesignMode ); }
            set { Set( () => ShowCage, value ); }
        }

        public string Cage
        {
            get { return Get( () => Cage, "" ); }
            set { Set( () => Cage, value ); }
        }


        public bool ScanningCage
        {
            get { return Get( () => ScanningCage, false ); }
            set { Set( () => ScanningCage, value ); }
        }

        [DependsUpon( nameof( ScanningCage ) )]
        public void WhenScanningCageChanges()
        {
            if( ScanningCage )
            {
                Caption = ScanCage;
                CancelButtonVisible = false;
                NextButtonVisible = false;
                PopAndSign = false;
                PlaceholderText = CageBarcode;
                ShowCage = false;
                Satchels = new ObservableCollection<TripDisplayEntry>();
            }
            else
            {
                Caption = ScanScatchels;
                CancelButtonVisible = true;
                PlaceholderText = SatchelBarcode;
                ShowCage = true;
            }
            Scanning = true;
        }

        #endregion

        #region Scanner


        public string Caption
        {
            get { return Get( () => Caption, "Caption" ); }
            set { Set( () => Caption, value ); }
        }


        public string PlaceholderText
        {
            get { return Get( () => PlaceholderText, "" ); }
            set { Set( () => PlaceholderText, value ); }
        }


        public string Barcode
        {
            get { return Get( () => Barcode, "" ); }
            set { Set( () => Barcode, value ); }
        }


        [DependsUpon( nameof( Barcode ) )]
        public void WhenBarcodeChanges()
        {
            Scanning = false;

            var BCode = Barcode.Trim();
            if( BCode.IsNotNullOrWhiteSpace() )
            {
                if( ScanningCage )
                {
                    Cage = BCode;
                    ScanningCage = false;
                }
                else
                {
                    if( !BCode.StartsWith( "AUDR" ) )
                        OnNotASatchel?.Invoke();
                    else
                    {
                        foreach( var S in Satchels )
                        {
                            if( S.TripId == BCode )
                            {
                                OnAlreadyScanned?.Invoke( BCode );
                                Scanning = true;
                                return;
                            }
                        }

                        Task.Run( async () =>
                        {
                            var Satchel = await AzureRemoteService.Azure.Client.RequestGetTrip( new Protocol.Data.GetTrip
                            {
                                Signatures = false,
                                TripId = BCode
                            } );

                            Dispatcher( () =>
                            {
                                if( !( Satchel is null ) && Satchel.Ok )
                                {
                                    Satchels.Add( new TripDisplayEntry( Satchel ) );
                                    RaisePropertyChanged( nameof( Satchels ) );
                                    NextButtonVisible = true;
                                }
                                else
                                    OnNotASatchel?.Invoke();
                            } );
                        } );
                    }
                    Scanning = true;
                }
            }
            Scanning = true;
        }

        public bool Scanning
        {
            get { return Get( () => Scanning, false ); }
            set { Set( () => Scanning, value ); }
        }

        #endregion

        #region ListView

        public ObservableCollection<TripDisplayEntry> Satchels
        {
            get { return Get( () => Satchels, new ObservableCollection<TripDisplayEntry>() ); }
            set { Set( () => Satchels, value ); }
        }

        #endregion


        #region Buttons

        public bool CancelButtonVisible
        {
            get { return Get( () => CancelButtonVisible, IsInDesignMode ); }
            set { Set( () => CancelButtonVisible, value ); }
        }

        public ICommand Cancel => Commands[ nameof( Cancel ) ];

        public async void Execute_Cancel()
        {
            if( !( OnCancel is null ) && await OnCancel() )
            {
                ScanningCage = true;
            }
        }


        public bool NextButtonVisible
        {
            get { return Get( () => NextButtonVisible, IsInDesignMode ); }
            set { Set( () => NextButtonVisible, value ); }
        }


        public ICommand Next => Commands[ nameof( Next ) ];
        public void Execute_Next()
        {
            PopAndSign = true;
            SignatureVisible = false;
        }

        #endregion
    }
}
