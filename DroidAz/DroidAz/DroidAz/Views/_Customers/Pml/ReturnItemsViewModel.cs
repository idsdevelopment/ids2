﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DroidAz.Droid.Annotations;
using DroidAz.Views.BaseViewModels;
using IdsControlLibraryV2.ViewModel;
using Utils;

namespace DroidAz.Views._Customers.Pml
{
	public class PmlTripItemsDisplayEntry : TripItemsDisplayEntry, INotifyPropertyChanged
	{
		public const string UPLIFT = "UPLIFT",
		                    UPPACK = "UPPACK",
		                    UPPC = "UPPC",
		                    UPTOTAL = "UPTOTAL",
		                    RETURN = "RETURN";

		public bool IsUplift => string.Compare( ItemCode, UPLIFT, StringComparison.OrdinalIgnoreCase ) == 0;
		public bool IsUpPack => string.Compare( ItemCode, UPPACK, StringComparison.OrdinalIgnoreCase ) == 0;
		public bool IsUpPc => string.Compare( ItemCode, UPPC, StringComparison.OrdinalIgnoreCase ) == 0;
		public bool IsUpTotal => string.Compare( ItemCode, UPTOTAL, StringComparison.OrdinalIgnoreCase ) == 0;
		public bool IsReturn => string.Compare( ItemCode, RETURN, StringComparison.OrdinalIgnoreCase ) == 0;
		public bool MoreToDo => Balance > 0;

		public bool DontHide
		{
			get => _DontHide;

			set
			{
				_DontHide = value;
				OnPropertyChanged( nameof( IsVisible ) );
			}
		}

		public override decimal Scanned
		{
			get => _Scanned;
			set
			{
				_Scanned = value;
				OnPropertyChanged();
			}
		}

		public override decimal Balance
		{
			get => _Balance;
			set
			{
				_Balance = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( IsVisible ) );
				OnPropertyChanged( nameof( RowHeight ) );
			}
		}

		public override bool IsVisible
		{
			get => ( ( MoreToDo && IsReturn ) || DontHide ) && !IsUpTotal;
			set { }
		}

		public int RowHeight => IsVisible ? -1 : 0;

		private bool _DontHide;

		private decimal _Scanned;

		private decimal _Balance;


		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public PmlTripItemsDisplayEntry()
		{
		}

		public PmlTripItemsDisplayEntry( TripItemsDisplayEntry e ) : base( e )
		{
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class ReturnItemsViewModel : ViewModelBase
	{
		public ObservableCollection<PmlTripItemsDisplayEntry> Items
		{
			get { return Get( () => Items, new ObservableCollection<PmlTripItemsDisplayEntry>() ); }
			set { Set( () => Items, value ); }
		}


		public bool IsScanning
		{
			get { return Get( () => IsScanning, false ); }
			set { Set( () => IsScanning, value ); }
		}


		public int TotalScanned
		{
			get { return Get( () => TotalScanned, 0 ); }
			set { Set( () => TotalScanned, value ); }
		}


		public int TotalOfAllScanned
		{
			get { return Get( () => TotalOfAllScanned, 0 ); }
			set { Set( () => TotalOfAllScanned, value ); }
		}

		public bool EnableNext
		{
			get { return Get( () => EnableNext, false ); }
			set { Set( () => EnableNext, value ); }
		}


		private decimal ScanLimit;
		public Action NotAuthorized;

		public Action OnBackButton;

		private void DoEnableNext()
		{
			EnableNext = Items.All( item => !item.IsReturn || !item.MoreToDo );
		}


		[DependsUpon( nameof( TotalOfAllScanned ) )]
		public void WhenTotalScannedChanges()
		{
			Dispatcher( () =>
			            {
				            MainPageViewModel.Instance.ScannedCount = TotalOfAllScanned;
			            } );
		}

		public void StartScanning( bool resetScan )
		{
			if( resetScan )
			{
				TotalScanned = 0;
				TotalOfAllScanned = 0;
			}

			var Limit = decimal.MaxValue;

			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach( var Item in Items )
			{
				if( Item.IsUpTotal )
					Limit = Math.Min( Limit, Item.Pieces );
			}

			ScanLimit = Limit;
			DoEnableNext();
			IsScanning = false;
			IsScanning = true;
		}

	#region Back Button

		public ICommand BackButton => Commands[ nameof( BackButton ) ];

		public void Execute_BackButton()
		{
			OnBackButton?.Invoke();
		}

	#endregion

	#region ItemCode

		public string ItemCode
		{
			get { return Get( () => ItemCode, "" ); }
			set { Set( () => ItemCode, value ); }
		}

		[DependsUpon150( nameof( ItemCode ) )]
		public void WenItemCodeChanges()
		{
			var Barcode = ItemCode.Trim();

			var Ok = true;

			try
			{
				if( Barcode.IsNotNullOrEmpty() )
				{
					// Do returns first
					foreach( var Item in Items )
					{
						if( ( Item.Barcode == Barcode ) && Item.IsReturn && Item.MoreToDo )
						{
							Item.Scanned++;
							Item.Balance--;
							TotalOfAllScanned++;

							return;
						}
					}

					// Do UpPacks
					foreach( var Item in Items )
					{
						if( ( Item.Barcode == Barcode ) && Item.IsUpPack && Item.MoreToDo )
						{
							Item.Scanned++;
							Item.Balance--;
							TotalOfAllScanned++;

							return;
						}
					}

					if( TotalScanned >= ScanLimit )
						Ok = false;
					else
					{
						//Check all pieces before uplifts
						foreach( var Item in Items )
						{
							if( ( Item.Barcode == Barcode ) && Item.IsUpPc && Item.MoreToDo )
							{
								Item.Scanned++;
								Item.Balance--;

								TotalScanned++;
								TotalOfAllScanned++;

								return;
							}
						}

						// Do Uplifts
						foreach( var Item in Items )
						{
							if( ( Item.Barcode == Barcode ) && Item.IsUplift && Item.MoreToDo )
							{
								string Prefix;

								var Len = Barcode.Length;

								switch( Len )
								{
								case 8:
									Prefix = Barcode.Substring( 0, 3 );

									break;

								case 13:
									Prefix = Barcode.Substring( 0, 7 );

									break;

								default:
									Prefix = "";

									break;
								}

								switch( Prefix )
								{
							#if DEBUG
								case "":
							#endif
								case "9310704":
								case "8710632":
								case "932":
								case "933":
								case "934":

									Item.Scanned++;
									Item.Balance--;

									TotalScanned++;
									TotalOfAllScanned++;

									return;
								}
							}
						}

						Ok = false;
					}
				}
			}
			finally
			{
				if( !Ok )
					NotAuthorized?.Invoke();

				DoEnableNext();
				IsScanning = true;
			}
		}

	#endregion

	#region Next Button

		public delegate void NextButtonEvent( int totalScanned, ObservableCollection<PmlTripItemsDisplayEntry> items );

		public NextButtonEvent OnNextButton;

		public ICommand Next => Commands[ nameof( Next ) ];

		public void Execute_Next()
		{
			OnNextButton?.Invoke( TotalOfAllScanned, Items );
		}

	#endregion

	#region Futile Button

		public Action OnFutile;

		public ICommand Futile => Commands[ nameof( Futile ) ];

		public void Execute_Futile()
		{
			OnFutile?.Invoke();
		}

	#endregion
	}
}