﻿using IdsControlLibraryV2.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
    [XamlCompilation( XamlCompilationOptions.Compile )]
    public partial class DriverClaims : ContentView
    {
        private readonly string Ok, Cancel, CancelClaim, NoASatchel, Error, AlreadyScanned, Satchel;

        public DriverClaims()
        {
            InitializeComponent();

            Ok = Globals.Resources.GetStringResource( "Ok" );
            Cancel = Globals.Resources.GetStringResource( "Cancel" );
            CancelClaim = Globals.Resources.GetStringResource( "PmlCancelClaim" );
            NoASatchel = Globals.Resources.GetStringResource( "PmlNotSatchel" );
            Satchel = Globals.Resources.GetStringResource( "PmlSatchel" );

            Error = Globals.Resources.GetStringResource( "Error" );
            AlreadyScanned = Globals.Resources.GetStringResource( "PmlAlreadyScanned" );

            if( BindingContext is DriverClaimsViewModel Model )
            {
                Model.OnCancel = async () =>
                {
                    var Result = await MainPage.Instance.DisplayActionSheet( CancelClaim, Cancel, Ok );
                    return Result == Ok;
                };

                Model.OnNotASatchel = () =>
                {
                    ViewModelBase.Dispatcher( async () =>
                    {
                        Globals.Sounds.PlayBadScanSound();
                        await MainPage.Instance.DisplayAlert( Error, NoASatchel, Cancel );
                    } );
                };

                Model.OnAlreadyScanned = satchel =>
                {
                    ViewModelBase.Dispatcher( async () =>
                    {
                        Globals.Sounds.PlayBadScanSound();
                        await MainPage.Instance.DisplayAlert( Error, $"{Satchel}: {satchel}\r\n{AlreadyScanned}", Cancel );
                    } );
                };

                var Sig = new Signature.Signature();
                SignatureView.Content = Sig;

                Model.ClearSignature = () =>
                                   {
                                       Sig.Clear();
                                   };

                Sig.OnCancel = () =>
                               {
                                   Model.OnSignatureCancel();
                               };

                Sig.OnOk = signature =>
                           {
                               Model.OnSignatureOk( signature );
                           };


            }
        }
    }
}