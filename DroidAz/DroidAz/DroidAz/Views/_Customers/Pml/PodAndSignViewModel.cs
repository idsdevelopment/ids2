﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DroidAz.Views.BaseViewModels;
using DroidAz.Views.Signature;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Utils;

namespace DroidAz.Views._Customers.Pml
{
	public class PodAndSignViewModel : ViewModelBase
	{
		private const string RETURN = "Return",
		                     UP = "Up";

		public ObservableCollection<PmlTripItemsDisplayEntry> Items
		{
			get { return Get( () => Items, new ObservableCollection<PmlTripItemsDisplayEntry>() ); }
			set { Set( () => Items, value ); }
		}

		public int TotalScanned
		{
			get { return Get( () => TotalScanned, 0 ); }
			set { Set( () => TotalScanned, value ); }
		}


		public bool EnablePickup
		{
			get { return Get( () => EnablePickup, false ); }
			set { Set( () => EnablePickup, value ); }
		}

		public string Satchel
		{
			get { return Get( () => Satchel, "" ); }
			set { Set( () => Satchel, value ); }
		}

	#region Trip

		public TripDisplayEntry Trip
		{
			get { return Get( () => Trip, new TripDisplayEntry() ); }
			set { Set( () => Trip, value ); }
		}

		[DependsUpon( nameof( Trip ) )]
		public void WhenTripChanges()
		{
			POP = "";
		}

	#endregion


	#region POP

		// ReSharper disable once InconsistentNaming
		public string POP
		{
			get { return Get( () => POP, "" ); }
			set { Set( () => POP, value ); }
		}

		[DependsUpon250( nameof( POP ) )]
		public void WhenPopChanges()
		{
			var Pop = POP;

			if( Pop.IsNotNullOrEmpty() && !( Signature is null ) )
			{
				foreach( var C in Pop )
				{
					if( ( C >= '0' ) && ( C <= '9' ) )
					{
						EnablePickup = false;

						return;
					}
				}

				EnablePickup = true;
			}
			else
				EnablePickup = false;
		}

	#endregion

	#region Back Button

		public Action OnBack;
		public ICommand Back => Commands[ nameof( Back ) ];

		public void Execute_Back()
		{
			OnBack?.Invoke();
		}

	#endregion

	#region Sign Button

		public Action ClearSignature;

		private Orientation.ORIENTATION SaveOrientation;

		public bool SignatureVisible
		{
			get { return Get( () => SignatureVisible, false ); }
			set { Set( () => SignatureVisible, value ); }
		}


		public bool DetailsVisible
		{
			get { return Get( () => DetailsVisible, true ); }
			set { Set( () => DetailsVisible, value ); }
		}

		public ICommand Sign => Commands[ nameof( Sign ) ];

		public void Execute_Sign()
		{
			SaveOrientation = Orientation.LayoutDirection;
			Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
			ClearSignature?.Invoke();
			SignatureVisible = true;
			DetailsVisible = false;
		}

		public SignatureResult Signature
		{
			get { return Get( () => Signature, (SignatureResult)null ); }
			set { Set( () => Signature, value ); }
		}

		[DependsUpon( nameof( Signature ) )]
		public void WhenSignatureChanges()
		{
			Orientation.LayoutDirection = SaveOrientation;
			SignatureVisible = false;
			DetailsVisible = true;
			WhenPopChanges();
		}

		public void OnSignatureOk( SignatureResult signature )
		{
			SignatureVisible = false;
			DetailsVisible = true;
			Orientation.LayoutDirection = SaveOrientation;
			Signature = signature;
		}

		public void OnSignatureCancel()
		{
			OnSignatureOk( null );
		}

	#endregion

	#region Pickup Button

		public Action<List<string>, Protocol.Data._Customers.Pml.Satchel> OnPickupComplete;

		public ICommand Pickup => Commands[ nameof( Pickup ) ];

		public void Execute_Pickup()
		{
			var GpsPoint = Globals.Gps.CurrentGpsPoint;

			// Gather the original trip ids.
			var TripIds = ( from I in Items
			                group I.OriginalTripId by I.OriginalTripId
			                into G
			                select G.FirstOrDefault()
			              ).ToList();

			var Si = Signature;

			var S = new Protocol.Data._Customers.Pml.Satchel
			        {
				        SatchelId = Satchel,
				        ProgramName = CurrentVersion.APP_VERSION,
				        TripIds = TripIds,
				        PickupTime = DateTimeOffset.Now,
				        PickupLatitude = GpsPoint.Latitude,
				        PickupLongitude = GpsPoint.Longitude,
				        POP = POP,

				        Signature = new Protocol.Data.Signature
				                    {
					                    Status = STATUS.PICKED_UP,
					                    Date = DateTimeOffset.Now,
					                    Height = Si.Height,
					                    Width = Si.Width,
					                    Points = Si.Points
				                    }
			        };

			Signature = null;

            var Package = new TripPackage();

			S.Packages.Add( Package );

			var PItems = Package.Items;

			var HasReturn = false;

			foreach( var I in Items )
			{
				if( I.Pieces > 0 )
				{
					HasReturn |= I.ItemCode.Compare( RETURN, StringComparison.OrdinalIgnoreCase ) == 0;

					PItems.Add( new TripItem
					            {
						            Barcode = I.Barcode,
						            ItemCode = I.ItemCode,
						            Description = I.Description,
						            Height = I.Height,
						            Length = I.Length,
						            Original = I.Pieces,
						            Pieces = I.Scanned
					            } );

					Package.Pieces += I.Pieces;
				}
			}

			Package.PackageType = HasReturn ? RETURN : UP;

			OnPickupComplete?.Invoke( TripIds, S );
		}

	#endregion
	}
}