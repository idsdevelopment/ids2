﻿using System;
using System.Collections.ObjectModel;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class ReturnsAccept : ContentView
	{
		public int TotalScanned
		{
			set => Model.TotalScanned = value;
			get => Model.TotalScanned;
		}

		public ObservableCollection<PmlTripItemsDisplayEntry> Items
		{
			set => Model.Items = value;
		}

		private readonly ReturnsAcceptViewModel Model;

		public Action OnBack;

		public Action OnFutile,
		              OnAccept;

		public ReturnsAccept()
		{
			InitializeComponent();

			if( BindingContext is ReturnsAcceptViewModel M )
			{
				Model = M;

				M.OnFutile = () =>
				             {
					             OnFutile?.Invoke();
				             };

				M.OnAccept = () =>
				             {
					             OnAccept?.Invoke();
				             };

				M.OnBack = () =>
				           {
					           OnBack?.Invoke();
				           };

				M.OnResetItem = async () => await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Confirm" ),
				                                                                  Globals.Resources.GetStringResource( "PmlResetScan" ),
				                                                                  Globals.Resources.GetStringResource( "Ok" ),
				                                                                  Globals.Resources.GetStringResource( "Cancel" ) );
			}
		}

	#region Double Tap

		// Could not get Gesture Recogniser to work,
		private bool EventSet;

		private readonly Timer TappedTimer = new Timer( 400 )
		                                     {
			                                     AutoReset = false
		                                     };


		private void Cell_OnTapped( object sender, EventArgs e )
		{
			if( !EventSet )
			{
				EventSet = true;

				TappedTimer.Elapsed += ( o, args ) =>
				                       {
					                       TappedTimer.Stop();
				                       };
			}

			if( !TappedTimer.Enabled )
				TappedTimer.Start();
			else // Second tap if timer is enabled
			{
				TappedTimer.Stop();
				Model.Execute_ResetItem();
			}
		}

	#endregion
	}
}