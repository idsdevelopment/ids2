﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DroidAz.Views.BaseViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class PodAndSign : ContentView
	{
		public TripDisplayEntry Trip
		{
			get => Model.Trip;
			set => Model.Trip = value;
		}

		public ObservableCollection<PmlTripItemsDisplayEntry> Items
		{
			get => Model.Items;
			set => Model.Items = value;
		}


		public int TotalScanned
		{
			get => Model.TotalScanned;
			set => Model.TotalScanned = value;
		}

		public string Satchel
		{
			get => Model.Satchel;
			set => Model.Satchel = value;
		}

		private readonly PodAndSignViewModel Model;

		public Action OnBack;
		public Action<List<string>, Protocol.Data._Customers.Pml.Satchel> OnPickupComplete;

		public PodAndSign()
		{
			InitializeComponent();

			var Sig = new Signature.Signature();
			SignatureView.Content = Sig;

			if( BindingContext is PodAndSignViewModel M )
			{
				Model = M;

				M.ClearSignature = () =>
				                   {
					                   RaiseChild( SignatureView );
					                   Sig.Clear();
				                   };

				M.OnBack = () =>
				           {
					           OnBack?.Invoke();
				           };

				M.OnPickupComplete = ( tripIds, satchel ) =>
				                     {
					                     OnPickupComplete?.Invoke( tripIds, satchel );
				                     };

				Sig.OnCancel = () =>
				               {
					               M.OnSignatureCancel();
				               };

				Sig.OnOk = signature =>
				           {
					           M.OnSignatureOk( signature );
				           };
			}
		}
	}
}