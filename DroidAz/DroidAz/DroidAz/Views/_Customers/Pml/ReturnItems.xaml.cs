﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DroidAz.Views.BaseViewModels;
using Globals;
using IdsControlLibraryV2.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class ReturnItems : ContentView
	{
		public List<TripItemsDisplayEntry> Items
		{
			get => _Trip;
			set
			{
				_Trip = value;

				var Itms = new ObservableCollection<PmlTripItemsDisplayEntry>();

				foreach( var Entry in value )
					Itms.Add( new PmlTripItemsDisplayEntry( Entry ) );

				Model.Items = Itms;
			}
		}

		private readonly ReturnItemsViewModel Model;

		private List<TripItemsDisplayEntry> _Trip;

		public Action OnBackButton,
		              OnFutile;


		public ReturnItemsViewModel.NextButtonEvent OnNextButton;


		public static void CancelReturnDialog( Action onReturnCanceled )
		{
			ViewModelBase.Dispatcher( async () =>
			                          {
				                          var Ok = await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Confirm" ),
				                                                                         Globals.Resources.GetStringResource( "PmlCancelReturn" ),
				                                                                         Globals.Resources.GetStringResource( "Ok" ),
				                                                                         Globals.Resources.GetStringResource( "Cancel" ) );

				                          if( Ok )
					                          onReturnCanceled?.Invoke();
			                          } );
		}

		public void ScanItems( bool resetScan )
		{
			Model.StartScanning( resetScan );
		}

		public ReturnItems()
		{
			InitializeComponent();

			if( BindingContext is ReturnItemsViewModel M )
			{
				Model = M;

				M.OnNextButton = ( scanned, items ) =>
				                 {
					                 OnNextButton?.Invoke( scanned, items );
				                 };

				M.NotAuthorized = () =>
				                  {
					                  ViewModelBase.Dispatcher( async () =>
					                                            {
						                                            Sounds.PlayBadScanSound?.Invoke();

						                                            await MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Error" ),
						                                                                                  Globals.Resources.GetStringResource( "PmlNotAuthorized" ),
						                                                                                  Globals.Resources.GetStringResource( "Cancel" ) );
					                                            } );
				                  };

				M.OnBackButton = () =>
				                 {
					                 ViewModelBase.Dispatcher( () =>
					                                           {
						                                           CancelReturnDialog( OnBackButton );
					                                           } );
				                 };

				M.OnFutile = () =>
				             {
					             OnFutile?.Invoke();
				             };
			}
		}
	}
}