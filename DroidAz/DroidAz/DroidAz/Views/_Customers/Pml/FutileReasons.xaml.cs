﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views._Customers.Pml
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class FutileReasons : ContentView
	{
		private readonly FutileReasonsViewModel Model;

		public Action OnCancel;
		public FutileReasonsViewModel.OkEvent OnOk;

		public FutileReasons()
		{
			InitializeComponent();

			if( BindingContext is FutileReasonsViewModel M )
			{
				Model = M;

				M.OnOk = ( reason, delete ) =>
				         {
					         OnOk?.Invoke( reason, delete );
				         };

				M.OnCancel = () =>
				             {
					             OnCancel?.Invoke();
				             };
			}
		}
	}
}