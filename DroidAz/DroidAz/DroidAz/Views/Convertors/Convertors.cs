﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace DroidAz.Views.Convertors
{
	public class BoolReadColorConvertor : IValueConverter
	{
		private static Color? UnReadColor,
		                      ReadColor;

		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( ReadColor is null )
			{
				var Res = Application.Current.Resources;
				ReadColor = Res.TryGetValue( "ReadColor", out var ResValue ) ? (Color)ResValue : Color.Red;
				UnReadColor = Res.TryGetValue( "UnReadColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
			}

			if( value is bool Read )
			{
				if( Read )
					return ReadColor;
			}

			return UnReadColor;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}

	public class BoolReadTextColorConvertor : IValueConverter
	{
		private static Color? UnReadColor,
		                      ReadColor;

		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( ReadColor is null )
			{
				var Res = Application.Current.Resources;
				ReadColor = Res.TryGetValue( "ReadTextColor", out var ResValue ) ? (Color)ResValue : Color.Red;
				UnReadColor = Res.TryGetValue( "UnReadTextColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
			}

			if( value is bool Read )
			{
				if( Read )
					return ReadColor;
			}

			return UnReadColor;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}


	public class BoolToPickupDeliveryColorConvertor : IValueConverter
	{
		private static Color? PickupColor,
		                      DeliveryColor;

		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( DeliveryColor is null )
			{
				var Res = Application.Current.Resources;
				PickupColor = Res.TryGetValue( "PickupColor", out var ResValue ) ? (Color)ResValue : Color.Red;
				DeliveryColor = Res.TryGetValue( "DeliveryColor", out var ResValue1 ) ? (Color)ResValue1 : Color.Purple;
			}

			if( value is bool IsPickup )
				return IsPickup ? PickupColor : DeliveryColor;

			return Color.Red;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}

	public class BoolToPickupDeliveryButtonTexConvertor : IValueConverter
	{
		private const string QM = "????";

		private static string PickupText,
		                      DeliverText;

		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( PickupText is null )
			{
				var Res = Application.Current.Resources;
				PickupText = Res.TryGetValue( "DetailsPickup", out var ResValue ) ? (string)ResValue : QM;
				DeliverText = Res.TryGetValue( "DetailsDeliver", out var ResValue1 ) ? (string)ResValue1 : QM;
			}

			if( value is bool IsPickup )
				return IsPickup ? PickupText : DeliverText;

			return QM;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}

    public class BoolToErrorBackgroundConvertor : IValueConverter
    {
        private static Color? ErrorBackgroundColor;

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( ErrorBackgroundColor is null)
            {
                var Res = Application.Current.Resources;
                ErrorBackgroundColor = Res.TryGetValue( "ErrorBackground", out var ResValue ) ? (Color)ResValue : Color.Red;
            }

            return value is bool Error && Error ? ErrorBackgroundColor : (object)Color.Transparent;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}