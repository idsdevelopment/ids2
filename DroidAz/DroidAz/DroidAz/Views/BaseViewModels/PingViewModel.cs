﻿using System;
using IdsControlLibraryV2.ViewModel;
using WeakEvent;

namespace DroidAz.Views.BaseViewModels
{
	public class PingViewModel : ViewModelBase
	{
		private readonly WeakEventSource<bool> _OnPing = new WeakEventSource<bool>();

		public event EventHandler<bool> OnPing
		{
			add => _OnPing.Subscribe( value );
			remove => _OnPing.Unsubscribe( value );
		}


		~PingViewModel()
		{
			MainPageViewModel.OnPingChange -= MainPageViewModelOnOnPingChange;
		}

		public PingViewModel()
		{
			MainPageViewModel.OnPingChange += MainPageViewModelOnOnPingChange;
		}

		private void MainPageViewModelOnOnPingChange( object sender, bool online )
		{
			_OnPing?.Raise( this, online );
		}
	}
}