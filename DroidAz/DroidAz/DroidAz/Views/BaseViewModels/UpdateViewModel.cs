﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AzureRemoteService;
using Newtonsoft.Json;
using Protocol.Data._Customers.Pml;
using Utils;
using File = System.IO.File;

namespace DroidAz.Views.BaseViewModels
{
    public abstract class IdBase
    {
        public Guid Id { get; } = Guid.NewGuid();
    }

    public class ReceivedByDevice : IdBase
    {
        public string TripId;
    }

    public class ReadByDriver : IdBase
    {
        public string TripId;
    }

    public class DeviceTripUpdate : IdBase
    {
        public Protocol.Data.DeviceTripUpdate TripUpdate;
    }


    public class FutileUpdate : IdBase
    {
        public Futile Futile;
    }

    public class SatchelUpdate : IdBase
    {
        public Satchel Satchel;
    }

    public class PmlClaimSatchel : IdBase
    {
        public ClaimSatchels Satchels;
    }

    public class PmlClaimTrips : IdBase
    {
        public ClaimTrips Trips;
    }


    public class PmlVerifySatchel : IdBase
    {
        public string ProgramName,
                      UserName,
                      TripId;

        public DateTimeOffset DateTime;

        public double Latitude,
                      Longitude;
    }


    public abstract class UpdateViewModel : PingViewModel
    {
        public class SaveEntry
        {
            public Type Type { get; set; }
            public string EntryAsJson { get; set; }
        }

        public class SaveList : List<SaveEntry>
        {
        }

        private static readonly List<IdBase> UpdatesList = new List<IdBase>();

        private static string BaseFolder,
                              _SaveFileName;

        private static readonly object LockObject = new object();

        // ReSharper disable once NotAccessedField.Local
        private static Timer UpdateTimer;
        private static volatile bool InUpdate;

        private static string SaveFileName
        {
            get
            {
                if( _SaveFileName.IsNullOrEmpty() )
                {
                    var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
                    BaseFolder = $"{AppData}\\{ProductName}";

                    if( !Directory.Exists( BaseFolder ) )
                        Directory.CreateDirectory( BaseFolder );

                    _SaveFileName = $"{BaseFolder}\\UpdateList.{SETTINGS_FILE_EXTENSION}";
                }

                return _SaveFileName;
            }
        }

        private static void SaveUpdateList()
        {
            lock( LockObject )
            {
                try
                {
                    var SaveList = new SaveList();

                    foreach( var Entry in UpdatesList )
                    {
                        SaveList.Add( new SaveEntry
                        {
                            Type = Entry.GetType(),
                            EntryAsJson = JsonConvert.SerializeObject( Entry )
                        } );
                    }

                    var SerializedObject = JsonConvert.SerializeObject( SaveList );
                    File.WriteAllText( SaveFileName, SerializedObject );
                }
                catch( Exception Exception )
                {
                    Console.WriteLine( Exception );
                }
            }
        }


        private static void LoadUpdateList()
        {
            lock( LockObject )
            {
                var FileName = SaveFileName;
                var Temp = new List<IdBase>();

                if( File.Exists( FileName ) )
                {
                    var SerializedObject = File.ReadAllText( FileName );
                    var Lst = JsonConvert.DeserializeObject<SaveList>( SerializedObject );

                    foreach( var Entry in Lst )
                    {
                        var Obj = JsonConvert.DeserializeObject( Entry.EntryAsJson, Entry.Type );

                        if( Obj is IdBase IdBase )
                        {
                            var Found = false;

                            foreach( var Base in UpdatesList )
                            {
                                if( Base.Id == IdBase.Id )
                                {
                                    Found = true;

                                    break;
                                }
                            }

                            if( !Found )
                                Temp.Add( IdBase );
                        }
                    }

                    foreach( var IdBase in UpdatesList )
                        Temp.Add( IdBase );

                    UpdatesList.Clear();
                    UpdatesList.AddRange( Temp );
                }
            }
        }

        // ReSharper disable once NotAccessedField.Local
        private static void Update()
        {
            if( InUpdate )
                return;

            InUpdate = true;

            Task.Run( () =>
                      {
                          Monitor.Enter( LockObject );

                          try
                          {
                              while( UpdatesList.Count > 0 )
                              {
                                  try
                                  {
                                      var UpdObj = UpdatesList[ 0 ];

                                      Monitor.Exit( LockObject );

                                      switch( UpdObj )
                                      {
                                      case ReceivedByDevice ReceivedByDevice:
                                          Azure.Client.RequestTripReceivedByDevice( CurrentVersion.APP_VERSION, ReceivedByDevice.TripId ).Wait();

                                          break;

                                      case ReadByDriver ReadByDriver:
                                          Azure.Client.RequestTripReadByDriver( CurrentVersion.APP_VERSION, ReadByDriver.TripId ).Wait();

                                          break;

                                      case DeviceTripUpdate DeviceTripUpdate:
                                          Azure.Client.RequestUpdateTripFromDevice( DeviceTripUpdate.TripUpdate ).Wait();

                                          break;

                                      case FutileUpdate FutileUpdate:
                                          Azure.Client.RequestPML_Futile( FutileUpdate.Futile ).Wait();

                                          break;

                                      case SatchelUpdate SatchelUpdate:
                                          Azure.Client.RequestPML_UpdateSatchel( SatchelUpdate.Satchel ).Wait();

                                          break;

                                      case PmlClaimSatchel Satchels:
                                          Azure.Client.RequestPML_ClaimSatchels( Satchels.Satchels ).Wait();

                                          break;

                                      case PmlClaimTrips Trips:
                                          Azure.Client.RequestPML_ClaimTrips( Trips.Trips ).Wait();

                                          break;

                                      case PmlVerifySatchel Vs:
                                          Azure.Client.RequestPML_VerifySatchel( new VerifySatchel
                                          {
                                              DateTime = Vs.DateTime,
                                              Latitude = Vs.Latitude,
                                              Longitude = Vs.Longitude,
                                              ProgramName = Vs.ProgramName,
                                              TripId = Vs.TripId,
                                              UserName = Vs.UserName
                                          } ).Wait();

                                          break;
                                      }
                                  }
                                  catch
                                  {
                                      return;
                                  }

                                  Monitor.Enter( LockObject );

                                  UpdatesList.RemoveAt( 0 );
                                  SaveUpdateList();
                              }

                              Monitor.Exit( LockObject );
                          }
                          finally
                          {
                              InUpdate = false;
                          }
                      } );
        }

        public static void InitialiseUpdates()
        {
            LoadUpdateList();

            UpdateTimer = new Timer( state =>
                                     {
                                         Update();
                                     }, null, 10_000, 10_000 );
        }

        public void UpdateFromObject( IdBase updateObject )
        {
            lock( LockObject )
            {
                UpdatesList.Add( updateObject );
                SaveUpdateList();
            }

            Update();
        }
    }
}