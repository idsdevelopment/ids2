﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using Preferences = Globals.Preferences;

// ReSharper disable VirtualMemberCallInConstructor
// ReSharper disable InconsistentNaming

namespace DroidAz.Views.BaseViewModels
{
	public class TripDisplayEntry : INotifyPropertyChanged
	{
		public string TripId { get; set; } = "";

		public bool HasItems => Items.Count > 0;

		public int Status
		{
			get => _Status;
			set
			{
				_Status = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( IsPickup ) );
				OnPropertyChanged( nameof( IsDelivery ) );
			}
		}

		public string AccountId { get; set; } = "";

		public string Reference
		{
			get => _Reference;
			set
			{
				_Reference = value;
				OnPropertyChanged();
			}
		}

		public bool ReadByDriver
		{
			get => _ReadByDriver;
			set
			{
				_ReadByDriver = value;
				OnPropertyChanged();
			}
		}

		public bool ReceivedByDevice { get; set; }
		public DateTimeOffset DueTime { get; set; }
		public DateTimeOffset CallDate { get; set; }

		public string PickupBarcode { get; set; } = "";
		public string PickupCompany { get; set; } = "";
		public string PickupSuite { get; set; } = "";
		public string PickupAddress { get; set; } = "";

		public string DisplayPickupAddress => _DisplayPickupAddress.IsNotNullOrWhiteSpace() ? _DisplayPickupAddress
			                                      : ( _DisplayPickupAddress = $"{PickupSuite} {PickupAddress}" ).Trim();

		public string PickupNotes { get; set; } = "";
		public string PickupContact { get; set; } = "";

		public string DeliveryBarcode { get; set; } = "";
		public string DeliveryCompany { get; set; } = "";
		public string DeliverySuite { get; set; } = "";
		public string DeliveryAddress { get; set; } = "";

		public string DisplayDeliveryAddress => _DisplayDeliveryAddress.IsNotNullOrWhiteSpace() ? _DisplayDeliveryAddress
			                                        : ( _DisplayDeliveryAddress = $"{DeliverySuite} {DeliveryAddress}" ).Trim();


		public string DeliveryNotes { get; set; } = "";
		public string DeliveryContact { get; set; } = "";

		public bool IsPickup => Status < (int)STATUS.PICKED_UP;

		public bool IsDelivery => !IsPickup;

		public string ServiceLevel { get; set; } = "";
		public string PackageType { get; set; } = "";

		public decimal Weight { get; set; }
		public decimal Pieces { get; set; }

		public string POP { get; set; }
		public string POD { get; set; }

		public string DisplayCompany => IsPickup ? PickupCompany : DeliveryCompany;

		public string DisplayAddress => IsPickup ? DisplayPickupAddress : DisplayDeliveryAddress;


		public string DisplayLocation
		{
			get
			{
				var Loc = IsPickup ? PickupLocation : DeliveryLocation;

				if( Loc.IsNullOrWhiteSpace() )
					Loc = DefaultLocation;

				return Loc;
			}
		}

		public List<TripItemsDisplayEntry> Items { get; set; } = new List<TripItemsDisplayEntry>();

		private readonly string DefaultLocation = "";

		private readonly string PickupLocation = "";

		private readonly string DeliveryLocation = "";

		private string _DisplayPickupAddress;
		private string _DisplayDeliveryAddress;


		private bool _ReadByDriver;

		private int _Status;

		private string _Reference = "";

		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}


		public static List<TripItemsDisplayEntry> ConvertItems( string tripId, List<TripItem> items )
		{
			return ( from I in items
			         select new TripItemsDisplayEntry( tripId, I ) ).ToList();
		}

        protected virtual List<TripItem> FlattenItems( string tripId, List<TripPackage> packages )
        {
            var Items = new List<TripItem>();

            foreach( var Package in packages )
            {
                foreach( var Item in Package.Items )
                    Items.Add( Item );
            }

            Items = (from I in Items
                     orderby I.ItemCode, I.Barcode
                     select I).ToList();

            for (var I = Items.Count; --I > 0;)
            {
                var I1 = Items[I];
                var I2 = Items[I - 1];

                if (I1.ItemCode == I2.ItemCode && I1.Barcode == I2.Barcode)
                {
                    I2.Pieces += I1.Pieces;
                    I2.Weight += I1.Weight;
                    Items.RemoveAt(I);
                }
            }

            return Items;
        }


        private List<TripItemsDisplayEntry> InternalFlattenItems( string tripId, List<TripPackage> packages )
		{
			return ConvertItems( tripId, FlattenItems( tripId, packages) );
		}

        public TripDisplayEntry()
		{
		}

		public TripDisplayEntry( TripUpdate t )
		{
			DefaultLocation = t.Location.Trim();
			PickupLocation = t.PickupAddressBarcode.Trim();
			DeliveryLocation = t.DeliveryAddressBarcode.Trim();

			TripId = t.TripId;
			Status = (int)t.Status1;

			AccountId = t.AccountId;
			Reference = t.Reference;

			ReadByDriver = t.ReadByDriver;
			ReceivedByDevice = t.ReceivedByDevice;

			DueTime = t.DueTime;
			CallDate = t.CallTime;

			PickupBarcode = t.PickupAddressBarcode;
			PickupCompany = t.PickupCompanyName;
			PickupSuite = t.PickupAddressSuite;
			PickupAddress = t.PickupAddressAddressLine1;

			PickupNotes = t.PickupAddressNotes;
			PickupContact = t.PickupContact;

			DeliveryBarcode = t.DeliveryAddressBarcode;
			DeliveryCompany = t.DeliveryCompanyName;
			DeliverySuite = t.DeliveryAddressSuite;
			DeliveryAddress = t.DeliveryAddressAddressLine1;

			DeliveryNotes = t.DeliveryAddressNotes;
			DeliveryContact = t.DeliveryContact;

			ServiceLevel = t.ServiceLevel;
			PackageType = t.PackageType;

			POP = t.POP;
			POD = t.POD;

			Weight = t.Weight;
			Pieces = t.Pieces;

			Items = InternalFlattenItems( TripId, t.Packages );
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class TripItemsDisplayEntry
	{
		public string ItemCode { get; set; }
		public string Barcode { get; set; }
		public string Description { get; set; }
		public decimal Weight { get; set; }
		public decimal Pieces { get; set; }
		public decimal Length { get; set; }
		public decimal Width { get; set; }
		public decimal Height { get; set; }
		public decimal Original { get; set; }
		public decimal QH { get; set; }
		public virtual decimal Scanned { get; set; }
		public virtual decimal Balance { get; set; }

        public virtual bool IsVisible { get; set; } = true;
        public virtual bool OverScanned { get; set; } = false;

        public string OriginalTripId { get; } = "";

		public TripItemsDisplayEntry()
		{
		}

		public TripItemsDisplayEntry( TripItemsDisplayEntry e )
		{
			OriginalTripId = e.OriginalTripId;
			ItemCode = e.ItemCode;
			Barcode = e.Barcode;
			Description = e.Description;
			Weight = e.Weight;
			Pieces = e.Pieces;
			Balance = e.Balance;
			Length = e.Length;
			Width = e.Width;
			Height = e.Height;
			Original = e.Original;
			QH = e.QH;
			Scanned = e.Scanned;
			IsVisible = e.IsVisible;
		}

		public TripItemsDisplayEntry( string tripId, TripItem i ) : this( tripId )
		{
			ItemCode = i.ItemCode;
			Barcode = i.Barcode;
			Description = i.Description;
			Height = i.Height;
			Length = i.Length;
			Original = i.Original;
			Pieces = i.Pieces;
			Balance = i.Pieces;
			QH = i.Qh;
			Weight = i.Weight;
			Width = i.Width;
		}

		public TripItemsDisplayEntry( string tripId )
		{
			OriginalTripId = tripId;
		}
	}

	public abstract class TripsBaseViewModel : UpdateViewModel
	{
		public bool ByLocation => Preferences.ByLocation;
		public bool ByPiece => Preferences.ByPiece;

		public bool ByLocationWithoutPieces => ByLocation && !ByPiece;

		public bool ShowTripCount
		{
			get => MainPageViewModel.Instance.ShowTripCount;
			set
			{
				Dispatcher( () =>
				            {
					            MainPageViewModel.Instance.ShowTripCount = value;
				            } );
			}
		}


		protected int TripCount
		{
			get => MainPageViewModel.Instance.TripCount;
			set
			{
				Dispatcher( () =>
				            {
					            MainPageViewModel.Instance.TripCount = value;
				            } );
			}
		}


		public bool ShowScannedCount
		{
			get => MainPageViewModel.Instance.ShowTripCount;
			set
			{
				Dispatcher( () =>
				            {
					            MainPageViewModel.Instance.ShowScannedCount = value;
				            } );
			}
		}


		protected int ScannedCount
		{
			get => MainPageViewModel.Instance.TripCount;
			set
			{
				Dispatcher( () =>
				            {
					            MainPageViewModel.Instance.ScannedCount = value;
				            } );
			}
		}


		public ObservableCollection<TripDisplayEntry> TripDisplayEntries
		{
			get { return Get( () => TripDisplayEntries, new ObservableCollection<TripDisplayEntry>() ); }
			set { Set( () => TripDisplayEntries, value ); }
		}


		[Setting]
		public Dictionary<string, Trip> Trips
		{
			get { return Get( () => Trips, new Dictionary<string, Trip>() ); }
			set { Set( () => Trips, value ); }
		}

		private readonly object LockObject = new object();

		private bool LastPing, HasClient, DontCreateClient = true;

		private void CreateClient()
		{
			LastPing = true;

			if( !HasClient && !DontCreateClient )
			{
				Azure.Client.ListenTrip( Messaging.DRIVERS, MainPageViewModel.Instance.DriverName.ToLower(), trips =>
				                                                                                             {
					                                                                                             Dispatcher( () =>
					                                                                                                         {
						                                                                                                         if( !( trips is null ) )
						                                                                                                         {
							                                                                                                         foreach( var Trip in trips )
							                                                                                                         {
								                                                                                                         var (OldTrip, Operation) = UpdateTrip( Trip );

								                                                                                                         MainPageViewModel.Instance.TripCount = Trips.Count;
								                                                                                                         UpdateTrip( Operation, OldTrip, Trip );
								                                                                                                         FilterAndSortTrips();
							                                                                                                         }
						                                                                                                         }
					                                                                                                         } );
				                                                                                             } );
				HasClient = true;
			}
		}

		private ( Trip OldTrip, NMD Operation) UpdateTrip( Trip trip )
		{
			if( !trip.ReceivedByDevice && ( trip.Status1 >= STATUS.DISPATCHED ) && ( trip.Status1 <= STATUS.PICKED_UP ) )
			{
				trip.ReceivedByDevice = true;

				UpdateFromObject( new ReceivedByDevice
				                  {
					                  TripId = trip.TripId
				                  } );
			}

			var Result = ( OldTrip: (Trip)null, Operation: NMD.NONE );

			lock( LockObject )
			{
				var Id = trip.TripId;
				var Trps = Trips;

				if( ( trip.Status1 < STATUS.DISPATCHED ) || ( trip.Status1 >= STATUS.DELIVERED ) )
				{
					if( Trps.TryGetValue( Id, out var DTrip ) )
					{
						Trps.Remove( Id );
						Result.OldTrip = DTrip;
						Result.Operation = NMD.DELETE;
					}
				}
				else if( Trps.TryGetValue( Id, out var Trip ) )
				{
					if( trip.LastModified > Trip.LastModified )
					{
						Trps[ Id ] = trip;
						Result.OldTrip = Trip;
						Result.Operation = NMD.MODIFY;
					}
				}
				else
				{
					Trps.Add( Id, trip );
					Result.Operation = NMD.NEW;
				}

				Trips = Trps;
				TripCount = Trps.Count;
			}

			return Result;
		}

		protected virtual void OnModifyTrip( Trip oldTrip, Trip newTrip )
		{
			lock( LockObject )
			{
				var TripId = oldTrip.TripId;

				for( var I = TripDisplayEntries.Count; --I >= 0; )
				{
					if( TripDisplayEntries[ I ].TripId == TripId )
					{
						TripDisplayEntries.RemoveAt( I );
						TripDisplayEntries.Insert( I, new TripDisplayEntry( newTrip ) );
					}
				}
			}
		}

		protected virtual void OnNewTrip( Trip newTrip )
		{
			lock( LockObject )
				TripDisplayEntries.Add( new TripDisplayEntry( newTrip ) );
		}

		protected virtual void OnDeleteTrip( Trip oldTip, Trip newTrip )
		{
			lock( LockObject )
			{
				var TripId = oldTip.TripId;

				for( var I = TripDisplayEntries.Count; --I >= 0; )
				{
					if( TripDisplayEntries[ I ].TripId == TripId )
					{
						TripDisplayEntries.RemoveAt( I );

						break;
					}
				}
			}
		}

		protected enum NMD
		{
			NONE,
			NEW,
			MODIFY,
			DELETE
		}


		protected override void OnInitialised()
		{
			OnPing += ( sender, online ) =>
			          {
				          if( online )
				          {
					          if( !LastPing ) // Was offLine
						          CreateClient();

					          LastPing = true;
				          }
				          else
					          LastPing = false;
			          };

			base.OnInitialised();

			Task.Run( async () =>
			          {
				          var Finished = false;

				          var Tps = await Azure.Client.RequestGetTripsForDriver( "" );

				          Dispatcher( () =>
				                      {
					                      foreach (var Tp in Tps)
					                      {
						                      var (OldTrip, Operation) = UpdateTrip(Tp);
						                      UpdateTrip(Operation, OldTrip, Tp);
					                      }

                                          FilterAndSortTrips();
					                      MainPageViewModel.Instance.TripCount = Trips.Count;

					                      Finished = true;
					                      DontCreateClient = false;
				                      } );

				          while( !Finished )
					          Thread.Sleep( 100 );

				          CreateClient();
			          } );
		}

		protected void UpdateTrip( NMD operation, Trip oldTrip, Trip newTrip )
		{
			switch( operation )
			{
			case NMD.NEW:
				OnNewTrip( newTrip );

				break;

			case NMD.MODIFY:
				OnModifyTrip( oldTrip, newTrip );

				break;

			case NMD.DELETE:
				OnDeleteTrip( oldTrip, newTrip );

				break;
			}
		}

		protected TripsBaseViewModel()
		{
			MainPageViewModel.Instance.OnSortFilterChange += ( sender, tuple ) =>
			                                                 {
				                                                 OnSortFilterChange( tuple.visibility, tuple.primarySort, tuple.secondarySort );
			                                                 };
		}

	#region LocationFilter

		public string LocationFilter
		{
			get { return Get( () => LocationFilter, "" ); }
			set { Set( () => LocationFilter, value ); }
		}

		[DependsUpon( nameof( LocationFilter ) )]
		public void WhenLocationFilterChanges()
		{
			var Trps = new ObservableCollection<TripDisplayEntry>();
			var Filter = LocationFilter;
			var EmptyLocationFilter = Filter.IsNullOrEmpty();

			// ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
			foreach( var Trip in Trips )
			{
				var Trp = new TripDisplayEntry( Trip.Value );

				if( EmptyLocationFilter || ( Trp.DisplayLocation == Filter ) )
					Trps.Add( Trp );
			}

			TripDisplayEntries = Trps;
		}

	#endregion

	#region Sorting

		private static int Compare( MainPageViewModel.SORT sort, TripDisplayEntry t1, TripDisplayEntry t2 )
		{
			switch( sort )
			{
			case MainPageViewModel.SORT.ACCOUNT_ID:
				return string.Compare( t1.AccountId, t2.AccountId, StringComparison.InvariantCulture );

			case MainPageViewModel.SORT.COMPANY:
				return (STATUS)t1.Status == STATUS.PICKED_UP
					       ? string.Compare( t1.DeliveryCompany, t2.DeliveryCompany, StringComparison.InvariantCulture )
					       : string.Compare( t1.PickupCompany, t2.PickupCompany, StringComparison.InvariantCulture );

			case MainPageViewModel.SORT.DUE_DATE_TIME:
				return DateTimeOffset.Compare( t1.DueTime, t2.DueTime );

			case MainPageViewModel.SORT.NEW:
				return !t1.ReadByDriver ? 1 : 0;

			case MainPageViewModel.SORT.REFERENCE:
				return string.Compare( t1.Reference, t2.Reference, StringComparison.InvariantCulture );

			case MainPageViewModel.SORT.STATUS:
				return t1.Status - t2.Status;

			default:
				return 0;
			}
		}


		protected virtual void OnSortFilterChange( MainPageViewModel.VISIBILITY visibility, MainPageViewModel.SORT primarySort, MainPageViewModel.SORT secondarySort )
		{
			ObservableCollection<TripDisplayEntry> SortTrips( List<TripDisplayEntry> trips )
			{
				trips.Sort( ( t1, t2 ) =>
				            {
					            var Retval = Compare( primarySort, t1, t2 );

					            if( Retval == 0 )
						            Retval = Compare( secondarySort, t1, t2 );

					            return Retval;
				            } );

				return new ObservableCollection<TripDisplayEntry>( trips );
			}

			TripDisplayEntries = SortTrips( ( from Kv in Trips
			                                  let T = Kv.Value
			                                  where ( visibility == MainPageViewModel.VISIBILITY.ALL ) || ( ( visibility == MainPageViewModel.VISIBILITY.DELIVERIES ) && ( T.Status1 == STATUS.PICKED_UP ) )
			                                                                                           || ( ( visibility == MainPageViewModel.VISIBILITY.NEW ) && !T.ReadByDriver )
			                                                                                           || ( ( visibility == MainPageViewModel.VISIBILITY.PICKUPS ) && ( ( T.Status1 == STATUS.ACTIVE ) || ( T.Status1 == STATUS.DISPATCHED ) ) )
			                                  select new TripDisplayEntry( T ) ).ToList() );
		}

		protected void FilterAndSortTrips()
		{
			var Filters = MainPageViewModel.Instance.Filters;
			OnSortFilterChange( Filters.Visibility, Filters.PrimarySort, Filters.SecondarySort );
		}

	#endregion
	}
}