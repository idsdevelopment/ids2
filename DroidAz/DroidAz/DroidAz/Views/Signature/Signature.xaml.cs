﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views.Signature
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Signature : ContentView
	{
		public delegate void OnCancelEvent();
		public delegate void OnOkEvent( SignatureResult signature );

		public OnCancelEvent OnCancel;
		public OnOkEvent OnOk;

		public void Clear()
		{
			SignaturePad.Clear();
		}

		public Signature()
		{
			InitializeComponent();
			var Model = new SignatureViewModel
			            {
				            OnGetPoints = () => SignaturePad.Points,
				            OnOk = signature =>
				                   {
					                   OnOk?.Invoke( signature );
				                   },

				            OnCancel = () =>
				                       {
					                       OnCancel?.Invoke();
				                       }
			            };

			BindingContext = Model;
		}
	}
}