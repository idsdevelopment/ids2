﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using AzureRemoteService;
using DroidAz.Controls;
using DroidAz.Gps;
using IdsControlLibraryV2.ViewModel;
using Utils;
using WeakEvent;

namespace DroidAz.Views
{
	public class MainPageViewModel : ViewModelBase
	{
		// ReSharper disable once InconsistentNaming
		private static readonly WeakEventSource<bool> _OnPingChange = new WeakEventSource<bool>();
		public static MainPageViewModel Instance { get; private set; }

		public static bool IsOnline { get; private set; }

		private readonly Timer PingTimer = new Timer( 15000 );

		public Func<Task<bool>> DisplaySignOutAlert;

		public Func<bool, Task> OnShowMenu;


		protected override void OnInitialised()
		{
			base.OnInitialised();
			BarcodeScanner.IsManualBarcode = ManualBarcodeInput;
		}

		public static event EventHandler<bool> OnPingChange
		{
			add => _OnPingChange.Subscribe( value );
			remove => _OnPingChange.Unsubscribe( value );
		}

		public async Task DoPing()
		{
			var Ok = "";

			try
			{
				Ok = await Azure.Client.RequestPing();
			}
			catch
			{
			}

			var OLine = Ok == "OK";

			Dispatcher( () =>
			            {
				            IsOnline = OLine;
				            OnLine = OLine;
				            OffLine = !OLine;
				            _OnPingChange?.Raise( this, OLine );
			            } );
		}

		public MainPageViewModel()
		{
			Instance = this;

			if( !IsInDesignMode )
			{
				PingTimer.Elapsed += async ( sender, args ) =>
				                     {
					                     await DoPing();
				                     };

				PingTimer.Start();

				Task.Run( () =>
				          {
				          #pragma warning disable 4014
					          DoPing();
				          #pragma warning restore 4014
				          } );
				GpsViewModel.Instance.Start();
			}
		}

	#region ToolBar

		public bool ProgramTitleVisible
		{
			get { return Get( () => ProgramTitleVisible, true ); }
			set { Set( () => ProgramTitleVisible, value ); }
		}

		public string DriverName
		{
			get { return Get( () => DriverName, "IDS Mobile" ); }
			set { Set( () => DriverName, value ); }
		}


		public bool ShowTripCount
		{
			get { return Get( () => ShowTripCount, false ); }
			set { Set( () => ShowTripCount, value ); }
		}


		public int TripCount
		{
			get { return Get( () => TripCount, 0 ); }
			set { Set( () => TripCount, value ); }
		}


		public bool ShowScannedCount
		{
			get { return Get( () => ShowScannedCount, false ); }
			set { Set( () => ShowScannedCount, value ); }
		}


		public int ScannedCount
		{
			get { return Get( () => ScannedCount, 0 ); }
			set { Set( () => ScannedCount, value ); }
		}


		public bool OnLine
		{
			get { return Get( () => OnLine ); }
			set { Set( () => OnLine, value ); }
		}

		public bool OffLine
		{
			get { return Get( () => OffLine, true ); }
			set { Set( () => OffLine, value ); }
		}

		private bool InCountChange;

		[DependsUpon( nameof( ShowTripCount ) )]
		public void WhenShowTripCountChanges()
		{
			if( ShowTripCount && !InCountChange )
			{
				InCountChange = true;
				ShowScannedCount = false;
				InCountChange = false;
			}
		}

		[DependsUpon( nameof( ShowScannedCount ) )]
		public void WhenShowScannedCountChanges()
		{
			if( ShowScannedCount && !InCountChange )
			{
				InCountChange = true;
				ShowTripCount = false;
				InCountChange = false;
				ScannedCount = 0;
			}
		}

	#endregion

	#region Sorting / Visibility

		public enum VISIBILITY
		{
			ALL,
			PICKUPS,
			DELIVERIES,
			NEW
		}

		public enum SORT
		{
			STATUS,
			TRIP_ID,
			DUE_DATE_TIME,
			ACCOUNT_ID,
			COMPANY,
			REFERENCE,
			NEW
		}

		public struct FilterSet
		{
			public VISIBILITY Visibility;

			public SORT PrimarySort,
			            SecondarySort;
		}

		private readonly WeakEventSource<(VISIBILITY visibility, SORT primarySort, SORT secondarySort)> _OnSortFilterChange = new WeakEventSource<(VISIBILITY visibility, SORT primarySort, SORT secondarySort)>();

		public event EventHandler<(VISIBILITY visibility, SORT primarySort, SORT secondarySort)> OnSortFilterChange
		{
			add => _OnSortFilterChange.Subscribe( value );
			remove => _OnSortFilterChange.Unsubscribe( value );
		}

		public FilterSet Filters => new FilterSet
		                            {
			                            Visibility = (VISIBILITY)Visibility,
			                            PrimarySort = (SORT)PrimarySort,
			                            SecondarySort = (SORT)SecondarySort
		                            };

		[Setting]
		public int Visibility
		{
			get { return Get( () => Visibility, 0 ); }
			set { Set( () => Visibility, value ); }
		}


		[Setting]
		public int PrimarySort
		{
			get { return Get( () => PrimarySort, 0 ); }
			set { Set( () => PrimarySort, value ); }
		}


		[Setting]
		public int SecondarySort
		{
			get { return Get( () => SecondarySort, 0 ); }
			set { Set( () => SecondarySort, value ); }
		}

		[DependsUpon( nameof( Visibility ) )]
		[DependsUpon( nameof( PrimarySort ) )]
		[DependsUpon( nameof( SecondarySort ) )]
		public void SortSave()
		{
			SaveSettings();
			_OnSortFilterChange?.Raise( this, ( (VISIBILITY)Visibility, (SORT)PrimarySort, (SORT)SecondarySort ) );
		}

        #endregion

        #region Menu


        public string VersionNumber
        {
            get { return Get(() => VersionNumber, CurrentVersion.VERSION ); }
            set { Set(() => VersionNumber, value); }
        }


        public bool AllowManualBarcode
		{
			get { return Get( () => AllowManualBarcode, false ); }
			set { Set( () => AllowManualBarcode, value ); }
		}


		[Setting]
		public bool ManualBarcodeInput
		{
			get { return Get( () => ManualBarcodeInput, false ); }
			set { Set( () => ManualBarcodeInput, value ); }
		}

		[DependsUpon500( nameof( ManualBarcodeInput ) )]
		public void WhenManualBarcodeInputChanges()
		{
			SaveSettings();
			BarcodeScanner.IsManualBarcode = ManualBarcodeInput;
		}

		public ICommand ShowMenu => Commands[ nameof( ShowMenu ) ];

		public void Execute_ShowMenu()
		{
			IsMenuOpen = !IsMenuOpen;
		}

		public bool MenuVisible
		{
			get { return Get( () => MenuVisible, IsInDesignMode ); }
			set { Set( () => MenuVisible, value ); }
		}

		public Action<bool> OnMenuVisible;

		[DependsUpon( nameof( MenuVisible ) )]
		public void WhenMenuVisibleChanges()
		{
			OnMenuVisible?.Invoke( MenuVisible );
		}


		public bool MenuEnabled
		{
			get { return Get( () => MenuEnabled, true ); }
			set { Set( () => MenuEnabled, value ); }
		}

		public bool IsMenuOpen
		{
			get { return Get( () => IsMenuOpen, true ); }
			set { Set( () => IsMenuOpen, value ); }
		}

		[DependsUpon( nameof( IsMenuOpen ), Delay = 250 )]
		public async void WhenMenuOpenChanges()
		{
			if( !IsInDesignMode )
			{
				MenuEnabled = false;

				try
				{
					if( !( OnShowMenu is null ) )
						await OnShowMenu.Invoke( IsMenuOpen );
				}
				finally
				{
					MenuEnabled = true;
				}
			}
		}

		public string CurrentProgramDescription
		{
			get { return Get( () => CurrentProgramDescription, "" ); }
			set { Set( () => CurrentProgramDescription, value ); }
		}

		public ObservableCollection<Applications.MenuEntry> MenuItemsSource
		{
			get { return Get( () => MenuItemsSource, new ObservableCollection<Applications.MenuEntry>() ); }
			set { Set( () => MenuItemsSource, value ); }
		}


		[Setting]
		public int MenuSelectedIndex
		{
			get { return Get( () => MenuSelectedIndex, -1 ); }
			set { Set( () => MenuSelectedIndex, value ); }
		}


		public Applications.MenuEntry SelectItemMenuEntry
		{
			get { return Get( () => SelectItemMenuEntry, new Applications.MenuEntry { Text = "" } ); }
			set { Set( () => SelectItemMenuEntry, value ); }
		}


		[DependsUpon( nameof( MenuSelectedIndex ) )]
		public void WhenMenuSelectedIndexChanges()
		{
			var Ndx = MenuSelectedIndex;

			if( Ndx >= 0 )
			{
				var Items = MenuItemsSource;

				if( Ndx < Items.Count )
					SelectItemMenuEntry = Items[ Ndx ];
			}
		}


		[DependsUpon( nameof( SelectItemMenuEntry ) )]
		public void WhenMenuSelectionChanges()
		{
			var Entry = SelectItemMenuEntry;

			if( Entry.Id.IsNotNullOrWhiteSpace() )
			{
				var Txt = Entry.Id;
				var Ndx = 0;
				var SelNdx = -1;

				foreach( var MenuEntry in MenuItemsSource )
				{
					if( Txt == MenuEntry.Id )
					{
						SelNdx = Ndx;

						break;
					}

					++Ndx;
				}

				CurrentProgramDescription = Entry.Text;
				MenuSelectedIndex = SelNdx;
				SaveSettings();
				IsMenuOpen = false;
				Applications.RunProgram( Entry.Id );
			}
		}

		public ICommand SignOut => Commands[ nameof( SignOut ) ];

		public async void Execute_SignOut()
		{
			if( !( DisplaySignOutAlert is null ) && await DisplaySignOutAlert() )
				Applications.Kill();
		}

	#endregion
	}
}