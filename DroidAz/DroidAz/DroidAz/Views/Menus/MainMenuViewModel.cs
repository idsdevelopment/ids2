﻿using System;
using System.Collections.Generic;
using System.Text;
using IdsControlLibraryV2.ViewModel;

namespace DroidAz.Views.Menus
{
    public class MainMenuViewModel : ViewModelBase
    {
        public object SelectedItem
        {
            get { return Get(() => SelectedItem); }
            set { Set(() => SelectedItem, value); }
        }
    }
}