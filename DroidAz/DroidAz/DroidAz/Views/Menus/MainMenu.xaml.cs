﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DroidAz.Views.Menus
{
    [XamlCompilation( XamlCompilationOptions.Compile )]
    public partial class MainMenu : ContentView
    {
        public MainMenu()
        {
            InitializeComponent();
        }
    }
}