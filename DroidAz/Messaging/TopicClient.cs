﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ids;
using Microsoft.Azure.ServiceBus;
using Protocol.Data;
using Utils;
using WeakEvent;
using Message = Microsoft.Azure.ServiceBus.Message;

namespace Messaging
{
	public class TopicClient
	{
		private SubscriptionClient SubscriptionClient;

		private readonly WeakEventSource<Trip> _OnReceivedTrip = new WeakEventSource<Trip>();
		private readonly WeakEventSource<TripStatusUpdate> _OnDeviceStatusUpdate = new WeakEventSource<TripStatusUpdate>();

		public event EventHandler<Trip> OnReceivedTrip
		{
			add => _OnReceivedTrip.Subscribe( value );
			remove => _OnReceivedTrip.Unsubscribe( value );
		}

		public event EventHandler<TripStatusUpdate> OnTripStatusUpdate
		{
			add => _OnDeviceStatusUpdate.Subscribe( value );
			remove => _OnDeviceStatusUpdate.Unsubscribe( value );
		}


		public class DispatcherClient : TopicClient
		{
			public DispatcherClient( IdsClient client, string userName, string board = "" ) : base( client, userName, MessagingRequest.MESSAGING_CONNECTION_TYPE.DISPATCH_BOARD, board )
			{
			}
		}

		public class DriverClient : TopicClient
		{
			public DriverClient( IdsClient client, string userName, string board = "" ) : base( client, userName, MessagingRequest.MESSAGING_CONNECTION_TYPE.DRIVER, board )
			{
			}
		}

		public async Task Close()
		{
			if( !( SubscriptionClient is null ) )
				await SubscriptionClient.CloseAsync();
		}

		public TopicClient( IdsClient client, string userName, MessagingRequest.MESSAGING_CONNECTION_TYPE connectionType, string board = "" )
		{
			Task.Run( async () =>
			          {
				          var Keys = await client.RequestMessagingConnect( new MessagingRequest
				                                                           {
					                                                           ConnectionType = connectionType,
					                                                           Board = board
				                                                           } );
				          if( Keys.Connected )
				          {
					          var Pk = Encryption.Decrypt( Keys.Pk );
					          var Id = Encryption.Decrypt( Keys.Id );
					          var Sub = Encryption.Decrypt( Keys.Sub );

					          SubscriptionClient = new SubscriptionClient( Pk, Id, Sub );

					          // Configure the message handler options in terms of exception handling, number of concurrent messages to deliver, etc.
					          var MessageHandlerOptions = new MessageHandlerOptions( ExceptionHandler.ExceptionReceivedHandler )
					                                      {
						                                      // Maximum number of concurrent calls to the callback ProcessMessagesAsync(), set to 1 for simplicity.
						                                      // Set it according to how many messages the application wants to process in parallel.
						                                      MaxConcurrentCalls = 5,

						                                      // Indicates whether the message pump should automatically complete the messages after returning from user callback.
						                                      // False below indicates the complete operation is handled by the user callback as in ProcessMessagesAsync().
						                                      AutoComplete = false
					                                      };

					          // Register the function that processes messages.
					          SubscriptionClient.RegisterMessageHandler( ProcessMessagesAsync, MessageHandlerOptions );
				          }
			          } );
		}

		private async Task ProcessMessagesAsync( Message message, CancellationToken token )
		{
			try
			{
				// Process the message.
				var Body = Encoding.UTF8.GetString( message.Body );

				var Message = Protocol.Data.Message.FromJson( Body );

				var T = Message.Type;
				if( T == typeof( Trip ).FullName )
					OnTripReceived( Message<Trip>.ToTMessageClass( Message ) );
				else if( T == typeof( TripStatusUpdate ).FullName )
					OnUpdateDeviceStatus( Message<TripStatusUpdate>.ToTMessageClass( Message ) );
			}
			catch
			{
			}
			finally
			{
				// Complete the message so that it is not received again.
				// This can be done only if the queue Client is created in ReceiveMode.PeekLock mode (which is the default).
				await SubscriptionClient.CompleteAsync( message.SystemProperties.LockToken );

				// Note: Use the cancellationToken passed as necessary to determine if the queueClient has already been closed.
				// If queueClient has already been closed, you can choose to not call CompleteAsync() or AbandonAsync() etc.
				// to avoid unnecessary exceptions.
			}
		}


		private void OnTripReceived( Trip t )
		{
			_OnReceivedTrip?.Raise( this, t );
		}

		private void OnUpdateDeviceStatus( TripStatusUpdate status )
		{
			_OnDeviceStatusUpdate?.Raise( this, status );
		}
	}
}