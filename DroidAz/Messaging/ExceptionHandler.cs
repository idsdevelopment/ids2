﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;

namespace Messaging
{
	internal static class ExceptionHandler
	{
		// Use this handler to examine the exceptions received on the message pump.
		internal static Task ExceptionReceivedHandler( ExceptionReceivedEventArgs exceptionReceivedEventArgs )
		{
			/*
		#if DEBUG
			Console.WriteLine( $"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}." );
			var Context = exceptionReceivedEventArgs.ExceptionReceivedContext;
			Console.WriteLine( "Exception context for troubleshooting:" );
			Console.WriteLine( $"- Endpoint: {Context.Endpoint}" );
			Console.WriteLine( $"- Entity Path: {Context.EntityPath}" );
			Console.WriteLine( $"- Executing Action: {Context.Action}" );
		#endif
		*/
			return Task.CompletedTask;
		}
	}
}