﻿using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmbeddedResources
{
	[ContentProperty( "ResourceId" )]
	public class EmbeddedImageExtension : IMarkupExtension
	{
		public string ResourceId { get; set; }

		public object ProvideValue( IServiceProvider serviceProvider )
		{
			var RetVal = string.IsNullOrWhiteSpace( ResourceId ) ? null : ImageSource.FromResource( ResourceId, typeof( EmbeddedImageExtension ).GetTypeInfo().Assembly );
			return RetVal;
		}
	}
}