﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
	public static class Keyboard
	{
		private static bool _Show = true;
		public static Action<bool> ShowHide;

		public static bool Show
		{
			get => _Show;
			set
			{
				_Show = value;
				ShowHide?.Invoke( value );
			}
		}
	}
}