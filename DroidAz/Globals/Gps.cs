﻿using System;
using Protocol.Data;

namespace Globals
{
	public static class Gps
	{
		public delegate void PositionChanged( DateTimeOffset time, double latitude, double longitude );
		public static PositionChanged OnPositionChanged;

		private static readonly object GLoclObject = new object();

		private static GpsPoint _CurrentGpsPoint = new GpsPoint();


		public static GpsPoint CurrentGpsPoint
		{
			get
			{
				lock (GLoclObject)
					return _CurrentGpsPoint;
			}

			set
			{
				lock (GLoclObject)
					_CurrentGpsPoint = value;
			}
		}
	}
}