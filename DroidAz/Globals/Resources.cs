﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Globals
{
	public static class  Resources
	{
		public static object GetResource( string resourceName )
		{
                return !Application.Current.Resources.TryGetValue( resourceName, out var ResValue ) ? resourceName : ResValue;
		}

		public static string GetStringResource( string resourceName )
		{
			return (string)GetResource( resourceName );
		}
	}
}