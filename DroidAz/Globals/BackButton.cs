﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
   public static class BackButton
    {
	    public static bool AllowBackButton = false;
	    public static Func<Task> OnBackKey;
    }
}
