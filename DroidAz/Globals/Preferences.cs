﻿using System.Threading;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;

namespace Globals
{
	public static class Preferences
	{
		private static DevicePreferences Prefs;
		private static readonly object LockObject = new object();
		private static volatile bool GetingPreferences;

		public static bool EditPiecesWeight => GetPreferences().EditPiecesWeight;
		public static bool EditRefrence => GetPreferences().EditReference;
		public static bool ByLocation => GetPreferences().ByLocation;
		public static bool ByPiece => GetPreferences().ByPiece;
		public static bool AllowManualBarcode => GetPreferences().AllowManualBarcodeInput;

		public static DevicePreferences GetPreferences()
		{
			lock( LockObject )
			{
				if( Prefs is null )
				{
					if( !GetingPreferences )
					{
						GetingPreferences = true;
						Task.Run( async () =>
						          {
							          var Temp = await Azure.Client.RequestDevicePreferences();
							          Prefs = Temp;
							          GetingPreferences = false;
						          } );
					}
					else
						return Prefs;
				}
			}

			while( GetingPreferences )
				Thread.Sleep( 100 );

			return Prefs;
		}
	}
}
