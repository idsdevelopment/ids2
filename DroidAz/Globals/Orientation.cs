﻿using System;

namespace Globals
{
	public static class Orientation
	{
		private static ORIENTATION _LayoutDirection = ORIENTATION.PORTRAIT;
		public enum ORIENTATION
		{
            BOTH,
			PORTRAIT,
			LANDSCAPE
		}

		public static ORIENTATION LayoutDirection
		{
			get => _LayoutDirection;
			set
			{
                if( value != _LayoutDirection )
                {
                    _LayoutDirection = value;
                    ChangeOrientation?.Invoke( value );
                }
			}
		}

		public static Action<ORIENTATION> ChangeOrientation;
	}
}