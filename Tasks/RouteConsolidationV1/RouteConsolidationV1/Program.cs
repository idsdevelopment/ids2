﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureRemoteService;
using RouteConsolidationV1.Properties;

namespace RouteConsolidationV1
{
	public class Program
	{
		private const string CLEAR_CACHE = "/CC",
		                     THREADS = "/THREADS=",
		                     SLOT = "/SLOT=",
		                     MAIN_SERVER = "/M",
		                     DEDUG_SERVER = "/D";

		// Slots
		private const string ALPHAT = "ALPHAT",
		                     ALPHAC = "ALPHAC",
		                     BETA = "BETA",
		                     GAMMA = "GAMMA",
		                     PRIMARY = "PRIMARY";

		private static string ServerName;
		private static bool HaveServer;

		private static int MaxThreads = 1;

		static void Main( string[] args )
		{
			AzureClient.Slot = AzureClient.SLOT.PRODUCTION;

			foreach( var A in args.Select( arg => arg.Trim() ) )
			{
				switch( A )
				{
				case CLEAR_CACHE:
					var Def = Settings.Default;
					Def.AddressCache = "";
					Def.Save();
					Console.WriteLine( "Address cache cleared" );
					break;

				case DEDUG_SERVER:
					Globals.Settings.Debug = true;
					ServerName = "JBTEST2";
					HaveServer = true;
					break;

				case MAIN_SERVER:
					Globals.Settings.Debug = false;
					HaveServer = true;
					break;
				default:
					if( A.StartsWith( THREADS ) )
					{
						var Temp = A.Remove( 0, THREADS.Length ).Trim();
						if( !int.TryParse( Temp, out MaxThreads ) )
							throw new Exception( "Invalid thread number." );
					}
					else if( A.StartsWith( SLOT ) )
					{
						switch( A.Remove( 0, SLOT.Length ).Trim().ToUpper() )
						{
						case ALPHAT:
							AzureClient.Slot = AzureClient.SLOT.ALPHAT;
							break;
						case ALPHAC:
							AzureClient.Slot = AzureClient.SLOT.ALPHAC;
							break;
						case BETA:
							AzureClient.Slot = AzureClient.SLOT.BETA;
							break;
						case GAMMA:
							AzureClient.Slot = AzureClient.SLOT.GAMMA;
							break;

						default:
							throw new Exception( "Invalid slot." );
						}
					}

					break;
				}
			}
		}
	}
}