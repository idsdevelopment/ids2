﻿namespace Ids1Import
{
	public static class Globals
	{
		public static string SourceCompany,
		                     Company,
		                     UserName,
		                     Password;

		public static int MaxThreads = 1,
		                  Days = 60;
	}
}