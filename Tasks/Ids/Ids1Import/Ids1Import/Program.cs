﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzureRemoteService;

#if !DEBUG
	using Microsoft.Azure.WebJobs;
#endif

namespace Ids1Import
{
	// To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
	internal class Program
	{
		public const string PROGRAM = "IDS 1 Import";

		private const string COMPANY_ID = "/C=",
		                     SOURCE_COMPANY_ID = "/SC=",
		                     USER_ID = "/U=",
		                     PASSWORD_ID = "/P=",
		                     THREADS = "/THREADS=",
		                     SLOT = "/SLOT=",
		                     DAYS = "/DAYS=";

		// Slots
		private const string ALPHAT = "ALPHAT",
		                     ALPHAC = "ALPHAC",
		                     BETA = "BETA",
		                     ALPHA_TRW = "ALPHA_TRW",
		                     // ReSharper disable once UnusedMember.Local
		                     PRIMARY = "PRIMARY";


		// Options
		private const string ACCOUNTS_ONLY = "/A";


		private static readonly HashSet<Functions.IMPORT_OPTIONS> Options = new HashSet<Functions.IMPORT_OPTIONS>();

		// Please set the following connection strings in app.config for this WebJob to run:
		// AzureWebJobsDashboard and AzureWebJobsStorage
		public static void Main( string[] args )
		{
			foreach( var A in args.Select( arg => arg.Trim() ) )
			{
				var UOption = A.ToUpper().Trim();

				if( UOption == ACCOUNTS_ONLY )
					Options.Add( Functions.IMPORT_OPTIONS.ACCOUNTS_ONLY );

				else if( A.StartsWith( SOURCE_COMPANY_ID ) )
					Globals.SourceCompany = A.Remove( 0, SOURCE_COMPANY_ID.Length ).Trim().ToLower();

				else if( A.StartsWith( COMPANY_ID ) )
					Globals.Company = A.Remove( 0, COMPANY_ID.Length ).Trim();

				else if( A.StartsWith( USER_ID ) )
					Globals.UserName = A.Remove( 0, USER_ID.Length ).Trim();

				else if( A.StartsWith( PASSWORD_ID ) )
					Globals.Password = A.Remove( 0, PASSWORD_ID.Length ).Trim();
				else if( A.StartsWith( THREADS ) )
				{
					var Temp = A.Remove( 0, THREADS.Length ).Trim();
					if( !int.TryParse( Temp, out Globals.MaxThreads ) )
						throw new Exception( "Invalid thread number." );
				}
				else if( A.StartsWith( DAYS ) )
				{
					var Temp = A.Remove( 0, DAYS.Length ).Trim();
					if( !int.TryParse( Temp, out Globals.Days ) )
						throw new Exception( "Invalid number of days." );
				}
				else if( A.StartsWith( SLOT ) )
				{
					switch( A.Remove( 0, SLOT.Length ).Trim().ToUpper() )
					{
					case ALPHAT:
						Azure.Slot = Azure.SLOT.ALPHAT;
						break;
					case ALPHAC:
						Azure.Slot = Azure.SLOT.ALPHAC;
						break;
					case BETA:
						Azure.Slot = Azure.SLOT.BETA;
						break;
					case ALPHA_TRW:
						Azure.Slot = Azure.SLOT.ALPHA_TRW;
						break;

					default:
						throw new Exception( "Invalid slot." );
					}
				}
			}

		#if !DEBUG
            var Config = new JobHostConfiguration();

			if( Config.IsDevelopment )
				Config.UseDevelopmentSettings();

			Config.UseTimers();

			var Host = new JobHost( Config );
			// The following code ensures that the WebJob will be running continuously
			Host.RunAndBlock();
		#else
			Functions.DoImport( Options ).Wait();
		#endif
		}
	}
}