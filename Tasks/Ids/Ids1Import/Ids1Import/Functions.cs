﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AzureRemoteService;
using Ids1DirectAccess;
using Ids1DirectAccess.Accounts;
using Ids1DirectAccess.Addresses;
using Ids1DirectAccess.Charges;
using Ids1DirectAccess.Database;
using Ids1DirectAccess.Trips;
using Microsoft.Azure.WebJobs;
using Protocol.Data;
using Utils;

namespace Ids1Import
{
	public class Functions
	{
		private enum IMPORT_STATE
		{
			LOGIN,
			GET_SETTINGS,
			CHARGES,
			ACCOUNTS,
			ADDRESSES,
			TRIPS
		}

		private const int TAKE = 2000;
		private const int RETRY_DELAY_IN_MS = 10000;

		private static bool InImport;

		private static IMPORT_STATE State = IMPORT_STATE.LOGIN;

		private static Settings Settings = new Settings();

		private static async Task DoLogin()
		{
			Console.WriteLine( "Logging into Azure" );
			if( await Azure.LogIn( "Ids1 Import", Globals.Company, Globals.UserName, Globals.Password ) != Azure.AZURE_CONNECTION_STATUS.OK )
				throw new Exception( "Cannot log into Azure" );
		}


		private static async Task GetAzureSettings()
		{
			Console.WriteLine( "Getting import settings from Azure" );
			Settings = await Azure.Client.RequestImportSettings();
			if( Settings is null )
				throw new Exception( "Cannot get settings" );
		}


		private static void DoCharges()
		{
			Console.WriteLine( "Importing charges from IDS1" );
			try
			{
				using( var Connection = Ids1.Connect() )
				{
					var Charges = Connection.GetCharges( Globals.SourceCompany );
					var UpdateCharges = new Charges();

					foreach( var Charge in Charges )
					{
						UpdateCharges.Add( new Charge
						                   {
							                   AccountId = Charge.accountId?.Trim() ?? "",
							                   AlwaysApplied = Charge.alwaysApplied,
							                   ChargeId = Charge.chargeId,
							                   ChargeType = (short)Charge.chargeType,
							                   DisplayOnDriversScreen = Charge.displayOnDriverScreens,
							                   DisplayOnEntry = Charge.displayOnWebEntry,
							                   DisplayOnInvoice = Charge.displayOnInvoice,
							                   DisplayOnWaybill = Charge.displayOnWaybill,
							                   DisplayType = (short)Charge.displayType,
							                   ExcludeFromFuelSurcharge = ( Charge.excludeFromFuelSurcharge != null ) && Charge.excludeFromFuelSurcharge.Value,
							                   ExcludeFromTaxes = ( Charge.excludeFromTaxes != null ) && Charge.excludeFromTaxes.Value,
							                   Formula = "",
							                   IsText = false,
							                   IsDiscountable = ( Charge.isDiscountable != null ) && Charge.isDiscountable.Value,
							                   IsFormula = false,
							                   IsFuelSurcharge = ( Charge.isFuelSurcharge != null ) && Charge.isFuelSurcharge.Value,
							                   IsMultiplier = ( Charge.isMultiplier != null ) && Charge.isMultiplier.Value,
							                   IsOverride = ( Charge.isOverride != null ) && Charge.isOverride.Value,
							                   IsTax = ( Charge.isTax != null ) && Charge.isTax.Value,
							                   IsVolumeCharge = false,
							                   IsWeightCharge = false,
							                   Label = Charge.chargeLabel,
							                   Max = (decimal)( Charge.max ?? 0 ),
							                   Min = (decimal)( Charge.min ?? 0 ),
							                   OverridenPackage = Charge.overriddenPackage,
							                   SortIndex = (short)Charge.sortIndex,
							                   Text = Charge.defaultValue
						                   } );
					}

					Console.WriteLine( "Updating charges in Azure" );
					Azure.Client.RequestAddUpdateCharges( UpdateCharges ).Wait();
				}
			}
			catch( Exception Exception )
			{
				throw new Exception( "Cannot get charges", Exception );
			}
		}

		private static async Task UpdateAzureSettings()
		{
			Console.WriteLine( "Updating settings in Azure" );
			while( true )
			{
				try
				{
					await Azure.Client.RequestUpdateImportSettings( Settings );
					return;
				}

				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
					Thread.Sleep( RETRY_DELAY_IN_MS );
				}
			}
		}

		private static void DoAddresses()
		{
			Console.WriteLine( "Updating customer addresses from IDS1" );

			var AddressLastUpdate = Settings.LastAddressUpdate;
			using( var Connection = Ids1.Connect() )
			{
				while( true )
				{
					var Addresses = Connection.GetAddresses( Globals.SourceCompany, AddressLastUpdate, TAKE );

					foreach( var Address in Addresses )
					{
						var CustCode = Address.accountId.Trim();

						WriteConsole( $"Customer: {CustCode}" );

						AddressLastUpdate = Math.Max( Address.lastUpdated, AddressLastUpdate );

						Azure.Client.RequestAddCustomerCompany( new AddCustomerCompany( Program.PROGRAM )
						                                        {
							                                        Company = new Company
							                                                  {
								                                                  CompanyName = Address.description,
								                                                  AddressLine1 = Address.street,
								                                                  City = Address.city,
								                                                  Region = Address.province,
								                                                  PostalCode = Address.pc,
								                                                  Country = Address.country,
								                                                  Password = Encryption.ToTimeLimitedToken( "" )
							                                                  },

							                                        CustomerCode = CustCode
						                                        } ).Wait();
					}

					if( Addresses.Count < TAKE )
					{
						Settings.LastAddressUpdate = Math.Max( AddressLastUpdate, Settings.LastAccountUpdate );
						break;
					}
				}
			}
		}


		private static void EraseToEndOfLine()
		{
			var CurrentLeft = Console.CursorLeft;
			var CurrentTop = Console.CursorTop;
			Console.Write( new string( ' ', Console.WindowWidth - CurrentLeft ) );
			Console.SetCursorPosition( CurrentLeft, CurrentTop );
		}

		private static void WriteConsole( string text )
		{
			Console.Write( "\r" );
			EraseToEndOfLine();
			Console.Write( text );
		}

		private static STATUS ConvertStatus( trip t )
		{
			if( t.invoiced ?? false )
				return STATUS.INVOICED;
			return (STATUS)t.status;
		}

		private static void DoTrips()
		{
			Console.WriteLine( "\r\nUpdating trips from IDS1" );
			var TripsLastUpdate = Settings.LastTripUpdate;

			var StartDate = DateTime.Now.AddDays( -Globals.Days ).Date;
			using( var Connection = Ids1.Connect() )
			{
				while( true )
				{
					var Trips = Connection.GetTripsDetailedByDateCreatedAndLastUpdated( Globals.SourceCompany, StartDate, TripsLastUpdate, TAKE );

					var TripUpdateList = new TripUpdateList();

					foreach( var Trip in Trips )
					{
						var T = Trip.Trip;

						WriteConsole( $"Customer: {T.accountId}  TripId: {T.subId}" );

						var Pa = Trip.PickupAddress;
						var Da = Trip.DeliveryAddress;

						var Upd = T.lastUpdated ?? 0;
						TripsLastUpdate = Math.Max( Upd, TripsLastUpdate );

						try
						{
							TripUpdate Tu;
							TripUpdateList.Trips.Add( Tu = new TripUpdate
							                         {
								                         Barcode = "",
								                         Board = "",

								                         CallTime = T.callTime.AsPacificStandardTime(),
								                         DueTime = T.deadlineTime.AsPacificStandardTime(),
								                         ReadyTime = T.readyTime.AsPacificStandardTime(),
								                         PickupTime = T.pickupTime.AsPacificStandardTime(),
								                         DeliveryTime = T.deliveredTime.AsPacificStandardTime(),

								                         DangerousGoods = T.dangerousGoods,
								                         Driver = T.driver ?? "",
								                         Pieces = T.pieces,
								                         Reference = T.clientReference ?? "",
								                         AccountId = T.accountId ?? "",
								                         Status1 = ConvertStatus( T ),
								                         TripId = T.subId,
								                         Weight = (decimal)T.weight,
								                         ServiceLevel = T.serviceLevel,
								                         PackageType = T.packageType,

								                         DeliveryAddressAddressLine1 = Da.street ?? "",
								                         DeliveryAddressCity = Da.city ?? "",
								                         DeliveryAddressCountry = Da.country ?? "",
								                         DeliveryAddressNotes = Da.notes ?? "",
								                         DeliveryAddressPostalCode = Da.pc ?? "",
								                         DeliveryAddressRegion = Da.province ?? "",
								                         DeliveryAddressSuite = Da.suite ?? "",
								                         DeliveryCompanyName = T.deliveryCompany ?? "",

								                         PickupAddressAddressLine1 = Pa.street ?? "",
								                         PickupAddressCity = Pa.city ?? "",
								                         PickupAddressCountry = Pa.country ?? "",
								                         PickupAddressNotes = Pa.notes ?? "",
								                         PickupAddressPostalCode = Pa.pc ?? "",
								                         PickupAddressRegion = Pa.province ?? "",
								                         PickupAddressSuite = Pa.suite ?? "",
								                         PickupCompanyName = T.pickupCompany ?? ""
							                         } );

							foreach( var TripCharge in Trip.Charges )
							{
								var Text = decimal.TryParse( TripCharge.value, out var Qty ) ? "" : TripCharge.value;

								Tu.TripCharges.Add( new TripCharge
								                    {
									                    ChargeId = TripCharge.chargeId ?? "",
									                    Quantity = Qty,
									                    Text = Text,
									                    Value = (decimal)( TripCharge.result ?? 0 )
								                    } );
							}
						}
						catch // Probably a date error
						{
						}
					}

					Azure.Client.RequestAddUpdateTrips( TripUpdateList ).Wait();

					if( Trips.Count < TAKE )
					{
						Settings.LastTripUpdate = Math.Max( TripsLastUpdate, Settings.LastTripUpdate );
						break;
					}
				}
			}
		}

		public enum IMPORT_OPTIONS
		{
			ACCOUNTS_ONLY
		}


		public static void DoAccounts()
		{
			Console.WriteLine( "Importing accounts from IDS1" );

			var AccountsLastUpdate = Settings.LastAccountUpdate;
			using( var Connection = Ids1.Connect() )
			{
				while( true )
				{
					var Accounts = Connection.GetAccountsDetailedByLastUpdated( Globals.SourceCompany, AccountsLastUpdate, TAKE );

					foreach( var AccountDetailed in Accounts )
					{
						var Upd = AccountDetailed.Account.lastUpdated ?? 0;
						AccountsLastUpdate = Math.Max( Upd, AccountsLastUpdate );

						var A = AccountDetailed.Account;
						var Adr = AccountDetailed.Address;
						var Bil = AccountDetailed.BillingAddress;
						var CustCode = A.subId.Trim();

						WriteConsole( $"Customer: {CustCode}" );
						Azure.Client.RequestAddUpdateCustomer( new AddUpdateCustomer( Program.PROGRAM )
						                                       {
							                                       CustomerCode = CustCode,
							                                       Password = Encryption.ToTimeLimitedToken( "" ),
							                                       UserName = A.adminUserId,
							                                       SuggestedLoginCode = A.adminUserId,

							                                       Company = new Company
							                                                 {
								                                                 UserName = A.adminUserId.Trim(),
								                                                 CompanyName = A.companyName,
								                                                 AddressLine1 = Adr.street,
								                                                 City = Adr.city,
								                                                 Region = Adr.province,
								                                                 PostalCode = Adr.pc,
								                                                 Country = Adr.country,
								                                                 Fax = A.companyFax,
								                                                 Phone = A.companyPhone
							                                                 },

							                                       BillingCompany = new Company
							                                                        {
								                                                        UserName = A.adminUserId.Trim(),
								                                                        CompanyName = A.companyName,
								                                                        AddressLine1 = Bil.street,
								                                                        City = Bil.city,
								                                                        Region = Bil.province,
								                                                        PostalCode = Bil.pc,
								                                                        Country = Bil.country,
								                                                        Fax = A.companyFax,
								                                                        Phone = A.companyPhone
							                                                        }
						                                       } ).Wait();
					}

					if( Accounts.Count < TAKE )
					{
						Settings.LastAccountUpdate = Math.Max( AccountsLastUpdate, Settings.LastAccountUpdate );
						return;
					}
				}
			}
		}

		public static async Task<bool> DoOptions( HashSet<IMPORT_OPTIONS> options )
		{
			var RetVal = false;
			if( options.Count > 0 )
			{
				RetVal = true;
				await DoLogin();

				foreach( var Option in options )
				{
					switch( Option )
					{
					case IMPORT_OPTIONS.ACCOUNTS_ONLY:
						DoAccounts();
						await UpdateAzureSettings();
						break;
					}
				}
			}

			return RetVal;
		}

		public static async Task DoImport( HashSet<IMPORT_OPTIONS> options )
		{
			if( !InImport )
			{
				InImport = true;
				ServicePointManager.DefaultConnectionLimit = 60;
				if( !await DoOptions( options ) )
				{
					try
					{
						while( true )
						{
							try
							{
								switch( State )
								{
								case IMPORT_STATE.LOGIN:
									await DoLogin();
									State = IMPORT_STATE.GET_SETTINGS;
									continue;

								case IMPORT_STATE.GET_SETTINGS:
									await GetAzureSettings();
									State = IMPORT_STATE.CHARGES;
									continue;

								case IMPORT_STATE.CHARGES:
									DoCharges();
									await UpdateAzureSettings();
									State = IMPORT_STATE.ACCOUNTS;
									continue;

								case IMPORT_STATE.ACCOUNTS:
									DoAccounts();
									await UpdateAzureSettings();
									State = IMPORT_STATE.ADDRESSES;
									continue;

								case IMPORT_STATE.ADDRESSES:
									DoAddresses();
									await UpdateAzureSettings();
									State = IMPORT_STATE.TRIPS;
									continue;

								case IMPORT_STATE.TRIPS:
									DoTrips();
									await UpdateAzureSettings();
									State = IMPORT_STATE.LOGIN;
									continue;
								}
							}

							catch( Exception Exception )
							{
								Console.WriteLine( Exception );
								Thread.Sleep( RETRY_DELAY_IN_MS );
							}
						}
					}
					finally
					{
						InImport = false;
					}
				}
			}
		}

		public static void ProcessTimerFunction( [TimerTrigger( "* 0/5 0 ? * * *", RunOnStartup = false )]
		                                         TimerInfo timerInfo, TextWriter log )
		{
		}

/*
		// This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage( [QueueTrigger( "queue" )] string message, TextWriter log )
		{
			log.WriteLine( message );
		}
		*/
	}
}