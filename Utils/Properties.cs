﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Utils
{
	public class PropertyDifferences
	{
		public PropertyInfo Info;
		public string ParantClass = "";

		public object Value1,
		              Value2;
	}

	public static class PropExtension
	{
		public static string ByteArrayToString( this byte[] b )
		{
			var Result = new StringBuilder();
			var L = b.Length;

			for( var I = 0; I < L; I++ )
			{
				if( I != 0 )
					Result.Append( "," );
				Result.Append( b[ I ].ToString() );
			}

			return Result.ToString();
		}
	}


	public class Properties
	{
		private const string NULL = "null";

		private static bool IsEqual( object o, object o1 )
		{
			switch( o )
			{
			case byte[] B1 when o1 is byte[] B2:
				return B1.SequenceEqual( B2 );

			default:
				return Equals( o, o1 );
			}
		}

		public static bool IsList( object o )
		{
			if( o is IList )
			{
				var Type = o.GetType();

				if( Type.IsGenericType )
				{
					var Gt = Type.GetGenericTypeDefinition();

					if( Gt != null )
						return Gt.IsAssignableFrom( typeof( List<> ) );
				}
			}

			return false;
		}

		public static bool IsDictionary( object o )
		{
			if( o is IDictionary )
			{
				var Type = o.GetType();

				if( Type.IsGenericType )
				{
					var Gt = Type.GetGenericTypeDefinition();

					if( Gt != null )
						return Gt.IsAssignableFrom( typeof( Dictionary<,> ) );
				}
			}

			return false;
		}

		public static bool IsClass( object o )
		{
			return ( o != null ) && !( o is string ) && o.GetType().IsClass;
		}

		public static bool IsStruct( object o )
		{
			if( o != null )
			{
				var T = o.GetType();

				return !T.IsPrimitive && T.IsValueType;
			}

			return false;
		}

		public static bool IsClassOrStruct( object o )
		{
			if( ( o != null ) && !( o is string ) )
			{
				var T = o.GetType();

				return T.IsClass || ( !T.IsPrimitive && T.IsValueType );
			}

			return false;
		}

		public static bool IsDateTime( object o )
		{
			return o is DateTimeOffset || o is DateTime;
		}

		public static bool AreTheSame( object a, object b )
		{
			var A = a.GetType();
			var B = b.GetType();

			if( ( A is null && B is null ) || ( A == B ) )
				return true; // Either both are null or they are the same type

			if( A.IsSubclassOf( B ) || B.IsSubclassOf( A ) )
				return true; // One inherits from the other

			return A.BaseType == B.BaseType; // They have the same immediate parent
		}

		public static List<PropertyDifferences> GetDifferences( object before, object after, string parentClass = "" )
		{
			if( AreTheSame( before, after ) )
			{
				var Result = new List<PropertyDifferences>();

				var Before = GetValues( before, parentClass );
				var After = GetValues( after, parentClass );

				// Walk the two list in parrallel
				using( var BeforeEnumerator = Before.GetEnumerator() )
				{
					using( var AfterEnumerator = After.GetEnumerator() )
					{
						while( true )
						{
							PropertyDifferences BeforeValue,
							                    AfterValue;

							var AfterOk = AfterEnumerator.MoveNext();
							AfterValue = AfterOk ? AfterEnumerator.Current : null;

							GetBefore:
							var BeforeOk = BeforeEnumerator.MoveNext();
							BeforeValue = BeforeOk ? BeforeEnumerator.Current : null;

							if( BeforeOk && AfterOk ) // Both list still tracking
							{
								var LType = BeforeValue.Info.PropertyType;
								var RType = AfterValue.Info.PropertyType;

								// Still in the same object type
								if( BeforeValue.ParantClass == AfterValue.ParantClass )
								{
									if( !IsEqual( BeforeValue.Value1, AfterValue.Value1 ) )
									{
										BeforeValue.Value2 = AfterValue.Value1;
										Result.Add( BeforeValue );
									}
								}
								else
								{
									do
									{
										AfterValue.Value1 = "";
										Result.Add( AfterValue );

										AfterOk = AfterEnumerator.MoveNext();

										if( !AfterOk )
											goto GetBefore;

										AfterValue = AfterEnumerator.Current;
									} while( BeforeValue.ParantClass == AfterValue.ParantClass );

									goto GetBefore;
								}

								continue;
							}

							if( BeforeOk )
							{
								do
								{
									BeforeValue.Value2 = "";
									Result.Add( BeforeValue );
								} while( BeforeEnumerator.MoveNext() );
							}
							else if( AfterOk )
							{
								do
								{
									AfterValue.Value1 = "";
									Result.Add( AfterValue );
								} while( AfterEnumerator.MoveNext() );
							}

							break;
						}
					}
				}

				return Result;
			}

			throw new ArgumentException( $"{nameof( Properties )}.{nameof( GetDifferences )}: Not of the same type ({before.GetType().FullName}, {after.GetType().FullName})" );
		}


		// Just list the values
		public static List<PropertyDifferences> GetValues( object o1, string parentClass = "" )
		{
			var Result = new List<PropertyDifferences>();

			foreach( var Property in o1.GetType().GetProperties() )
			{
				var Value1 = Property.GetValue( o1 );

				void Add( object a )
				{
					Result.Add( new PropertyDifferences
					            {
						            Info = Property,
						            ParantClass = parentClass,
						            Value1 = a,
						            Value2 = a
					            } );
				}

				switch( Value1 )
				{
				case byte[] B1:
					Add( B1 );

					break;

				case string S:
					Add( S );

					break;

				case IEnumerable E1:
					var DidAdd = false;
					var Ndx = 0;

					foreach( var V in (IEnumerable)Value1 )
					{
						DidAdd = true;
						Result.AddRange( GetValues( V, $"{Property.Name}[{Ndx++}]." ) );
					}

					if( !DidAdd )
						Add( "" ); // Keep lines in sync

					break;

				case Guid G:
					Add( G );

					break;

				case DateTime Dt:
					Add( Dt );

					break;

				case DateTimeOffset Dto:
					Add( Dto );

					break;

				case decimal Dec:
					Add( Dec );

					break;

				default:
					if( Value1 is null )
						Add( NULL );
					else if( IsClassOrStruct( Value1 ) )
						Result.AddRange( GetValues( Value1, $"{Property.Name}." ) );
					else
						Add( Value1 );

					break;
				}
			}

			return Result;
		}
	}
}