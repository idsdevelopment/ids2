﻿using System;

namespace Utils
{
	public static class TimeSpansExtensions
	{
		public static TimeSpan DbTimeSpan( this TimeSpan t )
		{
			var Dt = new DateTime( t.Ticks );

			return new TimeSpan( 0, Dt.Hour, Dt.Minute, Dt.Second, Dt.Millisecond );
		}

		public static TimeSpan TimeOfDayNoSeconds( this DateTime time )
		{
			return time.NoSeconds().TimeOfDay;
		}

		public static TimeSpan TimeOfDayNoSeconds( this TimeSpan time )
		{
			return new DateTime( time.Ticks ).TimeOfDayNoSeconds();
		}

		public static string ToIso8601( this TimeSpan time )
		{
			return time.Days == 0 ? $"PT{time.Hours}H{time.Minutes}M{time.Seconds}.{time.Milliseconds}S"
				       : $"P{time.Days}DT{time.Hours}H{time.Minutes}M{time.Seconds}.{time.Milliseconds}S";
		}
	}
}