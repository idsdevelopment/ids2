﻿using System;

namespace Utils
{
	public static class DateTimeExtensions
	{
		public const string PACIFIC_STANDARD_TIME = "Pacific Standard Time";

		public const string MONDAY_SHORT = "MON",
		                    TUESDAY_SHORT = "TUE",
		                    WEDNESDAY_SHORT = "WED",
		                    THURSDAY_SHORT = "THU",
		                    FRIDAY_SHORT = "FRI",
		                    SATURDAY_SHORT = "SAT",
		                    SUNDAY_SHORT = "SUN";

		public enum WEEK_DAY : byte
		{
			UNKNOWN,
			MONDAY,
			TUESDAY,
			WEDNESDAY,
			THURSDAY,
			FRIDAY,
			SATURDAY,
			SUNDAY
		}

		// ReSharper disable once InconsistentNaming
		public static readonly TimeSpan END_OF_DAY = new TimeSpan( 0, 23, 59, 59, 999 );
		public static readonly TimeSpan START_OF_DAY = new TimeSpan();

		private static readonly DateTime Epoch;


		public static DateTimeOffset NowKindUnspecified => DateTimeOffset.Now.KindUnspecified();
		public static DateTimeOffset UtcNowKindUnspecified => DateTimeOffset.UtcNow.KindUnspecified();

		public static TimeZoneInfo EasternTimeZoneInfo => TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_STANDARD_TIME );

		static DateTimeExtensions()
		{
			Epoch = new DateTime( 1970, 1, 1 );
		}

		public static string ShowAsLocalTime( this DateTimeOffset t, bool asLocal )
		{
			if( asLocal )
			{
				t = t.ToLocalTime();

				return $"{t:d} {t:h:mm:ss tt}";
			}

			return $"{t:d} {t:h:mm:ss tt zz}";
		}

		public static WEEK_DAY ToWeekDay( DateTime time )
		{
			switch( time.DayOfWeek )
			{
			case DayOfWeek.Monday:
				return WEEK_DAY.MONDAY;

			case DayOfWeek.Tuesday:
				return WEEK_DAY.TUESDAY;

			case DayOfWeek.Wednesday:
				return WEEK_DAY.WEDNESDAY;

			case DayOfWeek.Thursday:
				return WEEK_DAY.THURSDAY;

			case DayOfWeek.Friday:
				return WEEK_DAY.FRIDAY;

			case DayOfWeek.Saturday:
				return WEEK_DAY.SATURDAY;

			case DayOfWeek.Sunday:
				return WEEK_DAY.SUNDAY;

			default:
				return WEEK_DAY.UNKNOWN;
			}
		}

		public static WEEK_DAY ToWeekDayFromShortText( string weekDay )
		{
			switch( weekDay.Trim().ToUpper() )
			{
			case MONDAY_SHORT:
				return WEEK_DAY.MONDAY;

			case TUESDAY_SHORT:
				return WEEK_DAY.TUESDAY;

			case WEDNESDAY_SHORT:
				return WEEK_DAY.WEDNESDAY;

			case THURSDAY_SHORT:
				return WEEK_DAY.THURSDAY;

			case FRIDAY_SHORT:
				return WEEK_DAY.FRIDAY;

			case SATURDAY_SHORT:
				return WEEK_DAY.SATURDAY;

			case SUNDAY_SHORT:
				return WEEK_DAY.SUNDAY;

			default:
				return WEEK_DAY.UNKNOWN;
			}
		}

		public static DateTimeOffset ToEndOfDay( this DateTimeOffset date )
		{
			return date.Date.Add( END_OF_DAY );
		}

		public static DateTime ToEndOfDay( this DateTime date )
		{
			return date.Date.Add( END_OF_DAY );
		}


		public static DateTimeOffset ServerTimeToLocalTime( this DateTime serverTime )
		{
			DateTimeOffset RetVal;

			if( serverTime.Date == DateTime.MinValue )
				RetVal = DateTimeOffset.MinValue;
			else
				RetVal = serverTime.Date < Epoch.Date ? serverTime : serverTime.ToLocalTime();

			var Temp = DateTime.SpecifyKind( RetVal.DateTime, DateTimeKind.Unspecified );

			RetVal = new DateTimeOffset( Temp, new TimeSpan( 0 ) );

			return RetVal;
		}

		public static DateTime LocalTimeToServerTime( this DateTimeOffset serverTime )
		{
			var Temp = DateTime.SpecifyKind( serverTime.DateTime, DateTimeKind.Local );

			return Temp < Epoch.Date ? serverTime.DateTime : Temp.ToLocalTime();
		}


		public static DateTimeOffset KindUnspecified( this DateTimeOffset time )
		{
			return new DateTimeOffset( DateTime.SpecifyKind( time.DateTime, DateTimeKind.Unspecified ), new TimeSpan( 0 ) );
		}

		public static string ToTzDateTime( this DateTimeOffset date )
		{
			return date.ToString( "dd/MM/yyyy hh:mm tt zz" );
		}

		public static long UnixTicksSeconds( this DateTime time )
		{
			return (long)( time - Epoch ).TotalSeconds;
		}

		public static long UnixTicksSeconds( this DateTimeOffset time )
		{
			return (long)( time - Epoch ).TotalSeconds;
		}

		public static long UnixTicksMilliSeconds( this DateTime time )
		{
			return (long)( time - Epoch ).TotalMilliseconds;
		}

		public static long UnixTicksMilliSeconds( this DateTimeOffset time )
		{
			return (long)( time - Epoch ).TotalMilliseconds;
		}

		public static DateTime NoSeconds( this DateTime time )
		{
			return time.AddSeconds( -time.Second ).AddMilliseconds( -time.Millisecond );
		}

		public static DateTimeOffset AsPacificStandardTime( this DateTime time )
		{
			var UtcTime     = DateTime.SpecifyKind( time.ToUniversalTime(), DateTimeKind.Unspecified ).AddHours( EasternTimeZoneInfo.BaseUtcOffset.Hours );
			var PacificTime = new DateTimeOffset( UtcTime, EasternTimeZoneInfo.BaseUtcOffset );

			if( EasternTimeZoneInfo.IsDaylightSavingTime( PacificTime ) )
				PacificTime = PacificTime.AddHours( 1 );

			return PacificTime;
		}

		public static DateTimeOffset FromPacificStandardTime( this DateTime time )
		{
			var PacificTime = new DateTimeOffset( time, EasternTimeZoneInfo.BaseUtcOffset );

			if( EasternTimeZoneInfo.IsDaylightSavingTime( PacificTime ) )
				PacificTime = PacificTime.AddHours( -1 );

			var LocalTime = PacificTime.ToLocalTime();

			return LocalTime;
		}


		public static DateTimeOffset AsPacificStandardTime( this DateTime? deviceTime )
		{
			return deviceTime?.AsPacificStandardTime() ?? DateTimeOffset.MinValue;
		}

		public static DateTimeOffset AsPacificStandardTime( this DateTimeOffset deviceTime )
		{
			return deviceTime.DateTime.AsPacificStandardTime();
		}

		public static DateTimeOffset AsPacificStandardTime( this DateTimeOffset? deviceTime )
		{
			return deviceTime?.AsPacificStandardTime() ?? DateTimeOffset.MinValue;
		}

		public static DateTimeOffset StartOfDay( this DateTimeOffset date )
		{
			return date.Date;
		}

		public static DateTimeOffset EndOfDay( this DateTimeOffset date )
		{
			return date.Date.Add( new TimeSpan( 23, 59, 59, 999 ) );
		}
	}
}