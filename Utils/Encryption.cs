﻿using System;
using System.Text;

namespace Utils
{
	public class Encryption
	{
	#if DEBUG
		public const int DEFAULT_TIME_LIMIT_IN_SECONDS = 10 * 60;
	#else
		public const int DEFAULT_TIME_LIMIT_IN_SECONDS = 6 * 60; // Longer than the default network timeout of 5 minutes
	#endif

		private static readonly Random Rand = new Random();

		public static string Encrypt( string str )
		{
			var L = str.Length;

			if( L > 0 )
			{
				var Buffer = Encoding.ASCII.GetBytes( str );
				L = Buffer.Length;

				var B2 = new byte[ L + 2 ];

				lock( Rand )
				{
					B2[ 0 ] = (byte)Rand.Next( 256 );
					B2[ 1 ] = (byte)Rand.Next( 256 );
				}

				System.Buffer.BlockCopy( Buffer, 0, B2, 2, L );

				L = B2.Length;

				unchecked
				{
					var Ndx = -1;

					for( var I = 0; I < L; I++ )
						B2[ I ] ^= (byte)Ndx--;
				}

				// Forward propagation
				for( var I = 1; I < L; I++ )
					B2[ I ] ^= B2[ I - 1 ];

				return Convert.ToBase64String( B2 );
			}

			return str;
		}

		public static string ToTimeLimitedToken( string value, int allowedTimeInSeconds )
		{
			var Expires = DateTime.UtcNow.AddSeconds( allowedTimeInSeconds );

			return Encrypt( $"{Expires.Ticks}^{value}" );
		}

		public static string ToTimeLimitedToken( string value )
		{
			return ToTimeLimitedToken( value, DEFAULT_TIME_LIMIT_IN_SECONDS );
		}

		public static (string Value, bool Ok, bool timeError, bool Base64Error, long Diff, long expires) FromTimeLimitedTokenDetailed( string token )
		{
			var (Value, Ok, Base64Error) = DecryptDetailed( token );

			if( Ok )
			{
				var P = Value.IndexOf( '^' );

				if( P > 0 )
				{
					var V = Value.Substring( 0, P );
					Ok = long.TryParse( V, out var FutureTicks );

					if( Ok )
					{
						var NowTicks  = DateTime.UtcNow.Ticks;
						var DiffTicks = FutureTicks - NowTicks;

						if( DiffTicks > 0 )
						{
							try
							{
								return ( Value: Value.Substring( P + 1 ), Ok: true, false, false, DiffTicks, FutureTicks );
							}
							catch // Empty string
							{
								return ( Value: "", Ok: true, false, false, DiffTicks, FutureTicks );
							}
						}

						return ( "", false, true, false, DiffTicks, FutureTicks );
					}
				}

				return ( "", false, true, false, 0, 0 );
			}

			return ( Value, false, false, Base64Error, 0, 0 );
		}

		public static (string Value, bool Ok) FromTimeLimitedToken( string token )
		{
			var (Value, Ok, _, _, _, _) = FromTimeLimitedTokenDetailed( token );

			return ( Value, Ok );
		}

		public static (string Value, bool Ok, bool Base64Error) DecryptDetailed( string str )
		{
			var Retval = ( Value: "", Ok: false, Base64Error: false );

			try
			{
				var L = str.Length;

				if( L > 0 )
				{
					var Buffer = Convert.FromBase64String( str );

					L = Buffer.Length;

					// Undo forward propagation
					for( var I = L - 1; I > 0; I-- )
						Buffer[ I ] ^= Buffer[ I - 1 ];

					unchecked
					{
						var Ndx = -1;

						for( var I = 0; I < L; I++ )
							Buffer[ I ] ^= (byte)Ndx--;
					}

					var B2 = new byte[ L - 2 ];
					System.Buffer.BlockCopy( Buffer, 2, B2, 0, B2.Length );

					Retval.Value = Encoding.ASCII.GetString( B2 );
					Retval.Ok    = true;
				}
			}
			catch( FormatException )
			{
				Retval.Base64Error = true;
			}
			catch
			{
			}

			return Retval;
		}

		public static string Decrypt( string str )
		{
			return DecryptDetailed( str ).Value;
		}
	}
}