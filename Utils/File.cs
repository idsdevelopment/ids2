﻿using System.Linq;
using System.Text;

namespace Utils
{
	public class File
	{
		private static readonly string[] InvalidFileParts =
		{
			@"\",
			":",
			"*",
			"?",
			"\"",
			"<",
			">",
			"|",
			"CON",
			"PRN",
			"AUX",
			"CLOCK$",
			"NUL",
			"COM0",
			"COM1",
			"COM2",
			"COM3",
			"COM4",
			"COM5",
			"COM6",
			"COM7",
			"COM8",
			"COM9",
			"LPT0",
			"LPT1",
			"LPT2",
			"LPT3",
			"LPT4",
			"LPT5",
			"LPT6",
			"LPT7",
			"LPT8",
			"LPT9"
		};

		public static string MakeValidFileName( string fname, string replacement = "_" )
		{
			return InvalidFileParts.Aggregate( fname, ( current, invalid ) => current.Replace( invalid, replacement ) );
		}

		public static string MakeIdentFileName( string fname, char replacement = '_', bool allowDot = false )
		{
			var L = fname.Length;

			var Result = new StringBuilder();

			for( var I = 0; I < L; )
			{
				var C = fname[ I++ ];

				if( C == '.' )
					Result.Append( allowDot ? C : replacement );
				else if( ( ( C >= 'A' ) && ( C <= 'Z' ) ) || ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( ( C >= '0' ) && ( C <= '9' ) ) || ( C == '_' ) )
					Result.Append( C );
				else
					Result.Append( replacement );
			}

			return MakeValidFileName( Result.ToString() );
		}
	}
}