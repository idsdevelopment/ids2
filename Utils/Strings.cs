﻿using System;
using System.Globalization;
using System.Text;

namespace Utils
{
	public static class Strings
	{
		public static string TrimToLower( this string txt )
		{
			return txt.Trim().ToLower();
		}

		public static bool Contains( this string text, string value, StringComparison stringComparison )
		{
			return text.IndexOf( value, stringComparison ) >= 0;
		}

		public static string SubStr( this string str, int startIndex, int length )
		{
			try
			{
				return ( startIndex + length ) > str.Length ? str.Substring( startIndex ) : str.Substring( startIndex, length );
			}
			catch
			{
				return "";
			}
		}

		public static string ToNumeric( this string txt, bool allowDecimal = false )
		{
			var HasDecimal = !allowDecimal;

			if( HasDecimal ) // Remove anything from the decimal point onwards
			{
				var Pos = txt.IndexOf( '.' );

				if( Pos >= 0 )
					txt = txt.Substring( 0, Pos );
			}

			var HasSign  = false;
			var HasDigit = false;

			var Result = new StringBuilder();

			foreach( var C in txt )
			{
				switch( C )
				{
				case '+':
				case '-':
					if( !HasSign )
					{
						if( C != '+' )
							Result.Append( C );
					}

					break;

				case '.':
					if( !HasDecimal )
					{
						HasDecimal = true;
						Result.Append( HasDigit ? "." : "0." );
					}

					break;

				default:
					if( ( C >= '0' ) && ( C <= '9' ) )
					{
						HasDigit = true;
						Result.Append( C );

						break;
					}
					else
						continue;
				}

				HasSign = true;
			}

			return Result.ToString().TrimEnd( '.', '-' );
		}


		public static bool ToBool( this string value )
		{
			switch( value.Trim().ToLower() )
			{
			case "true":
			case "t":
				return true;

			default:
				return false;
			}
		}

		public static int Compare( this string str1, string str2, StringComparison strCmp = StringComparison.InvariantCulture )
		{
			return string.Compare( str1, str2, strCmp );
		}

		public static bool IsNullOrEmpty( this string str )
		{
			return string.IsNullOrEmpty( str );
		}

		public static bool IsNotNullOrEmpty( this string str )
		{
			return !string.IsNullOrEmpty( str );
		}

		public static bool IsNullOrWhiteSpace( this string str )
		{
			return string.IsNullOrWhiteSpace( str );
		}

		public static bool IsNotNullOrWhiteSpace( this string str )
		{
			return !string.IsNullOrWhiteSpace( str );
		}


		public static bool IsAlphaNumeric( this char c )
		{
			return ( ( c >= 'A' ) && ( c <= 'Z' ) ) || ( ( c >= 'a' ) && ( c <= 'z' ) ) || ( ( c >= '0' ) && ( c <= '9' ) );
		}

		public static string Capitalise( this string value )
		{
			return CultureInfo.CurrentCulture.TextInfo.ToTitleCase( value.ToLower() );
		}

		public static string ToAlphaNumeric( this string txt )
		{
			var Result = new StringBuilder();

			if( txt.IsNotNullOrWhiteSpace() )
			{
				foreach( var C in txt )
				{
					if( C.IsAlphaNumeric() )
						Result.Append( C );
				}
			}

			return Result.ToString();
		}

		public static string ToIdent( this string value )
		{
			var First  = true;
			var Retval = new StringBuilder();

			foreach( var C in value )
			{
				if( ( ( C >= 'A' ) && ( C <= 'Z' ) ) || ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( C == '_' ) )
					Retval.Append( C );

				else if( !First && ( C >= '0' ) && ( C <= '9' ) )
					Retval.Append( C );
				else
					Retval.Append( '_' );

				First = false;
			}

			return Retval.ToString();
		}

		public static bool IsInteger( this string value )
		{
			return int.TryParse( value, out _ );
		}

		public static string Between( this string text, string first, string last, StringComparison culture = StringComparison.Ordinal )
		{
			var First = text.IndexOf( first, culture );

			if( First >= 0 )
			{
				First += first.Length;

				var Last = text.LastIndexOf( last, culture );

				if( ( Last >= 0 ) && ( Last > First ) )
					return text.Substring( First, Last - First );
			}

			return null;
		}

		public static string TrimStart( this string str, string toStrip, StringComparison culture = StringComparison.Ordinal, bool removeLeadingSpaces = true )
		{
			if( removeLeadingSpaces )
				str = str.TrimStart();

			var L = toStrip.Length;

			return string.Compare( str, 0, toStrip, 0, L, culture ) == 0 ? str.Substring( L ) : str;
		}

		public static string Pack( this string str, char packChar = ' ' )
		{
			return string.Join( packChar.ToString(), str.Split( new[] { packChar }, StringSplitOptions.RemoveEmptyEntries ) );
		}
	}
}