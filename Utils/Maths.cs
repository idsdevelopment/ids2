﻿using System;

namespace Utils
{
	public static class Maths
	{
		private static readonly char[] B36 =
		{
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y', 'Z'
		};

		private static string ToBase36( this ulong number )
		{
			var Result = "";

			while( number > 0 )
			{
				Result = B36[ number % 36 ] + Result;
				number /= 36;
			}

			if( Result.IsNullOrWhiteSpace() )
				Result = "0";

			return Result;
		}


		//returns a uniformly random ulong between ulong.Min inclusive and ulong.Max inclusive
		public static ulong NextULong( this Random rng )
		{
			var Buffer = new byte[ 8 ];
			rng.NextBytes( Buffer );

			return BitConverter.ToUInt64( Buffer, 0 );
		}

		//returns a uniformly random ulong between ulong.Min and Max without modulo bias
		public static ulong NextULong( this Random rng, ulong max, bool inclusiveUpperBound = false )
		{
			return rng.NextULong( ulong.MinValue, max, inclusiveUpperBound );
		}

		//returns a uniformly random ulong between Min and Max without modulo bias
		public static ulong NextULong( this Random rng, ulong min, ulong max, bool inclusiveUpperBound = false )
		{
			var Range = max - min;

			if( inclusiveUpperBound )
			{
				if( Range == ulong.MaxValue )
					return rng.NextULong();

				Range++;
			}

			if( Range <= 0 )
				throw new ArgumentOutOfRangeException( "Max must be greater than min when inclusiveUpperBound is false, and greater than or equal to when true", "max" );

			var Limit = ulong.MaxValue - ( ulong.MaxValue % Range );
			ulong ULong;

			do
				ULong = rng.NextULong();
			while( ULong > Limit );

			return ( ULong % Range ) + min;
		}

		//returns a uniformly random long between long.Min inclusive and long.Max inclusive
		public static long NextLong( this Random rng )
		{
			var Buffer = new byte[ 8 ];
			rng.NextBytes( Buffer );

			return BitConverter.ToInt64( Buffer, 0 );
		}

		//returns a uniformly random long between long.Min and Max without modulo bias
		public static long NextLong( this Random rng, long max, bool inclusiveUpperBound = false )
		{
			return rng.NextLong( long.MinValue, max, inclusiveUpperBound );
		}

		//returns a uniformly random long between Min and Max without modulo bias
		public static long NextLong( this Random rng, long min, long max, bool inclusiveUpperBound = false )
		{
			var Range = (ulong)( max - min );

			if( inclusiveUpperBound )
			{
				if( Range == ulong.MaxValue )
					return rng.NextLong();

				Range++;
			}

			if( Range <= 0 )
				throw new ArgumentOutOfRangeException( "Max must be greater than min when inclusiveUpperBound is false, and greater than or equal to when true", "max" );

			var Limit = ulong.MaxValue - ( ulong.MaxValue % Range );
			ulong ULong;

			do
				ULong = rng.NextULong();
			while( ULong > Limit );

			return (long)( ( ULong % Range ) + (ulong)min );
		}
	}
}