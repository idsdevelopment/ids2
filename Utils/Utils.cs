﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Utils
{
	public static class Util
	{
		public static string AddPathSeparator( string fullPath )
		{
			return fullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar ) +
			       Path.DirectorySeparatorChar;
		}

		public static string NormaliseString( string str )
		{
			return Regex.Replace( str, @"\s+", " " );
		}

		public static bool IsIdent( string id )
		{
			var First = true;

			foreach( var C in id )
			{
				if( ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( ( C >= 'A' ) && ( C <= 'Z' ) ) || ( ( C >= '0' ) && ( C <= '9' ) && !First ) )
					First = false;
				else
					return false;
			}

			return true;
		}

		/// <summary>
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public static DateTimeOffset GetStartOfDay( DateTimeOffset dateTime )
		{
			return new DateTime( dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, 0 );
		}

		/// <summary>
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public static DateTimeOffset GetEndOfDay( DateTimeOffset dateTime )
		{
			return new DateTime( dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 999 );
		}
	}
}