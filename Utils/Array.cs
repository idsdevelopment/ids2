using System;

namespace Utils
{
	public static class ArrayExtensions
	{
		/// <summary>
		/// Copy Array ( Same as new C# .. )
		/// to is Exclusive
		/// </summary>
		/// <param name="array"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns>object[]</returns>
		public static object[] Range( this object[] array, int from, int to )
		{
			var Len  = ( to - @from );
			var Temp = new object[ Len ];
			Array.Copy( array, from, Temp, 0, Len );

			return Temp;
		}
	}
}