﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// ReSharper disable ExplicitCallerInfoArgument

namespace Utils
{
	public class Logging : IDisposable
	{
		public const string LOG_FILE_NAME = "Ids2ClientLog.Txt",
		                    LOG_FILE_SEARCH_PATTERN = "*-" + LOG_FILE_NAME,
		                    IDS = "Ids",
		                    DATE_TIME_FORMAT = "yyyy-MM-dd-hh-mm-ss";

		public const uint RETENTION_DAYS = 30;

		public string Log => Writer.Log;

		private class LogWriter : TextWriter
		{
			public override Encoding Encoding { get; } = Encoding.ASCII;


			internal string Log
			{
				get
				{
					while( true )
					{
						try
						{
							lock( LockObject )
							{
								OutputStream.Flush();
								FileStream.Flush();
							}

							break;
						}
						catch // Probably in use by another thread
						{
							Thread.Sleep( 100 );
						}
					}

					var P = FileStream.Position;

					try
					{
						FileStream.Position = 0;
						var L = FileStream.Length;
						var Buffer = new byte[ L ];
						FileStream.Read( Buffer, 0, (int)L );

						return Encoding.GetString( Buffer );
					}
					finally
					{
						FileStream.Position = P;
					}
				}
			}

			private readonly FileStream FileStream;
			private readonly TextWriter OldWriter;
			private readonly StreamWriter OutputStream;
			private readonly object LockObject = new object();


			public override void Write( char value )
			{
				OldWriter.Write( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.Write( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}

			public override void Write( string value )
			{
				OldWriter.Write( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.Write( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}

			public override void WriteLine()
			{
				OldWriter.WriteLine();
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.WriteLine();
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}

			public override void WriteLine( string value )
			{
				OldWriter.WriteLine( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.WriteLine( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}

			public override void Close()
			{
				lock( LockObject )
					OutputStream.Close();
			}

			public LogWriter( string logFileName, TextWriter oldWriter )
			{
				OldWriter = oldWriter;

				// Needs a FileStream so it can send the crash log to the Azure server
				OutputStream = new StreamWriter( FileStream = new FileStream( logFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read ) );
				FileStream.Seek( 0, SeekOrigin.End ); // Append
			}
		}

		private readonly TextWriter OldOutput;
		private LogWriter Writer;

		/// <summary>
		///     cjt Utility method to write out a line in the current log.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="methodName"></param>
		/// <param name="className"></param>
		public static void WriteLogLine( string message, string methodName, string className )
		{
			var Now = DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.ffff" );
			var Line = Now + "; " + message + "; Method:" + methodName + "; Class:" + className;
			Console.WriteLine( Line );
		}

		/// <summary>
		///     From
		///     https://docs.microsoft.com/en-us/dotnet/api/system.runtime.compilerservices.callermembernameattribute?redirectedfrom=MSDN
		///     &view=netframework-4.7.2
		/// </summary>
		/// <param name="message"></param>
		/// <param name="memberName"></param>
		/// <param name="sourceFilePath"></param>
		/// <param name="sourceLineNumber"></param>
		public static void WriteLogLine( string message,
		                                 [CallerMemberName] string memberName = "",
		                                 [CallerLineNumber] int sourceLineNumber = 0,
		                                 [CallerFilePath] string sourceFilePath = "" )
		{
			sourceFilePath = sourceFilePath.Substring( sourceFilePath.LastIndexOf( '\\' ) + 1 );
			var Now = DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.ffff" );
			var Line = Now + "; " + message + "; Method:" + memberName + "; File:" + sourceFilePath + "; Line:" + sourceLineNumber;

			while( true )
			{
				try
				{
					Console.WriteLine( Line );

					break;
				}
				catch // Probably in use by another thread
				{
					Thread.Sleep( 100 );
				}
			}
		}

		public static void WriteLogLine( Exception e,
		                                 [CallerMemberName] string memberName = "",
		                                 [CallerLineNumber] int sourceLineNumber = 0,
		                                 [CallerFilePath] string sourceFilePath = "" )
		{
			while( !( e is null ) )
			{
				WriteLogLine( e.Message, memberName, sourceLineNumber, sourceFilePath );
				e = e.InnerException;
			}
		}

		public Action<Exception> OnUnhandledException;

		public Logging( string logFileName = LOG_FILE_NAME, uint retentionDays = RETENTION_DAYS )
		{
			var DocsDir = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ), IDS );
			Directory.CreateDirectory( DocsDir );

			Task.Run( () =>
			          {
				          var Now = DateTime.Now;
				          var Day30Ticks = new TimeSpan( (int)retentionDays, 0, 0, 0 ).Ticks;

				          // Clean up old log files
				          var Files = Directory.GetFiles( DocsDir, LOG_FILE_SEARCH_PATTERN );

				          foreach( var F in Files )
				          {
					          var FName = Path.GetFileName( F );
					          var P = FName.LastIndexOf( '-' );

					          if( P > 0 )
					          {
						          var TimeStr = FName.Substring( 0, P );

						          try
						          {
							          var Date = DateTime.ParseExact( TimeStr, DATE_TIME_FORMAT, CultureInfo.InvariantCulture );

							          if( ( Now - Date ).Ticks > Day30Ticks )
								          System.IO.File.Delete( F );
						          }
						          catch
						          {
						          }
					          }
				          }
			          } );

			OldOutput = Console.Out;

			LogWriter NewWriter()
			{
				//var DocsFileName = Path.Combine( DocsDir, $"{DateTime.Now:yyyy-MM-dd-hh-mm-ss}-{logFileName}" );
				var DocsFileName = Path.Combine( DocsDir, $"{DateTime.Now:yyyy-MM-dd}-{logFileName}" );

				return Writer = new LogWriter( DocsFileName, OldOutput );
			}

			Console.SetOut( NewWriter() );

			AppDomain.CurrentDomain.UnhandledException += ( sender, args ) =>
			                                              {
				                                              var E = (Exception)args.ExceptionObject;
				                                              Console.WriteLine( $@"Unhandled exception : {E.Message}" );
				                                              var Terminated = args.IsTerminating ? "Yes" : "No";
				                                              Console.WriteLine( $@"Application terminated: {Terminated}" );
				                                              Writer.Flush();

				                                              if( args.IsTerminating )
					                                              Writer.Close();

				                                              Task.Run( () =>
				                                                        {
					                                                        OnUnhandledException?.Invoke( E );
				                                                        } );

				                                              Thread.Sleep( 5000 );
			                                              };
		}

		public void Dispose()
		{
			Writer.Close();
			Console.SetOut( OldOutput );
		}
	}
}