﻿using System.Collections.Generic;
using Service.Interfaces;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		internal class Polling
		{
			public bool WaitForUpdate
			{
				get => _WaitForUpdate;
				set
				{
					AbortGetTrips  = value;
					_WaitForUpdate = value;
				}
			}

			private readonly Db<TClient, TAuthToken, TOriginalTripType> Owner;
			private bool _WaitForUpdate;

			private bool InPollingEvent;
			public bool AbortGetTrips;

			public void PollingEvent()
			{
				if( ( Owner != null ) && Owner.LoggedIn && !InPollingEvent )
				{
					InPollingEvent = true;

					try
					{
						if( !Owner.OutFifoHasRecords )
						{
							var Ok    = false;
							var Trips = new List<ITrip>();

							while( !WaitForUpdate )
							{
								var (List, Ok1) = Owner.Service.GetTrips( Owner.IgnoreTripAddress ).Result;

								if( AbortGetTrips )
								{
									AbortGetTrips = false;

									return;
								}

								Ok = Ok1;
								Trips.AddRange( List );

								break;
							}

							if( Ok )
								Owner.PushIn( Trips );
						}

						Owner.NotifyInFifoUpdate(); // Keep things ticking over
						Owner.NotifyOutFifoUpdate();
						Owner.NotifyClaimUpdate();
						Owner.NotifyTripStatusUpdate();
						Owner.NotifyBarcodeUpdate();
						Owner.NotifyGpsUpdate();

						//Metro
						Owner.NotifyUpdateReturnsWithBin();
						Owner.NotifyUpdateLoadingTrailer();
						Owner.NotifyRbhUnLoadTrailer();
						Owner.NotifyRbhUpdateDestructionTrailer();
						Owner.NotifyEditCreateReturnsWithBin();
						Owner.NotifyRbhWaybill();
						Owner.NotifyRcClaimUpdate();
					}
					finally
					{
						InPollingEvent = false;
					}
				}
			}

			internal Polling( Db<TClient, TAuthToken, TOriginalTripType> owner, int pollingIntervalInSeconds )
			{
				Owner = owner;
				owner.Service.AddTimedEvent( "Polling", PollingEvent, pollingIntervalInSeconds * 1000 );
			}
		}
	}
}