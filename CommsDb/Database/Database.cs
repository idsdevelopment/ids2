﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Service.Abstracts;
using Service.Interfaces;
using SQLite;

// ReSharper disable StaticMemberInGenericType

namespace CommsDb.Database
{
	// ReSharper disable once UnusedTypeParameter
	public partial class Db<TClient, TAuthToken, TOriginalTripType> : SQLiteConnection where TClient : new()
	{
		private const int DB_VERSION = 14;

		public class DbVersion
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public int Version { get; set; } = DB_VERSION;
		}

		public static readonly STATUS[] DefaultStatusFilter =
		{
			STATUS.NEW,
			STATUS.ACTIVE,
			STATUS.DISPATCHED,
			STATUS.PICKED_UP,
			STATUS.DELIVERED
		};

		public AIService<TClient, TAuthToken> Service { get; private set; }

		public STATUS[] StatusFilterSet
		{
			get => _StatusFilterSet;
			set
			{
				if( ( value == null ) || ( value.Length == 0 ) )
					_StatusFilterSet = DefaultStatusFilter;
				else
					_StatusFilterSet = value;
			}
		}

		public bool IgnoreTripAddress
		{
			get => _IgnoreTripAddress;
			set
			{
				DeleteAllLocalTrips();

				if( ( ServicePolling != null ) && ( _IgnoreTripAddress != value ) )
					ServicePolling.AbortGetTrips = true;
				_IgnoreTripAddress = value;
			}
		}

		public bool InCheckForTrips { get; private set; }

		public int PollingIntervalInSeconds
		{
			get => _PollingIntervalInSeconds;
			set
			{
				_PollingIntervalInSeconds = value;

				Task.Run( () =>
				          {
					          ServicePolling = new Polling( this, value );
				          } );
			}
		}

		public Configuration Config
		{
			get
			{
				SetDatabaseLock();

				try
				{
					if( _Config == null )
					{
						_Config = ( from Cfg in Table<Configuration>()
						            select Cfg ).FirstOrDefault();

						if( _Config == null )
						{
							_Config = new Configuration();
							Insert( _Config );
						}
					}

					return _Config;
				}
				finally
				{
					ResetDatabaseLock();
				}
			}

			set
			{
				SetDatabaseLock();

				try
				{
					Update( value );
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		public string AuthTokenAsString => Service.AuthTokenAsString;

		private STATUS[] _StatusFilterSet = DefaultStatusFilter;

		private bool _IgnoreTripAddress;

		private volatile bool DatabaseLock;

		// ReSharper disable once NotAccessedField.Local
		private Polling ServicePolling;

		private int _PollingIntervalInSeconds;

		private Configuration _Config;

		public readonly object LockObject = new object();

		public bool LoggedIn;

		public OnNotifyTripChangeEvent OnNotifyTripChange;

		public delegate void OnNotifyTripChangeEvent( int totalTrips, bool hasNew, bool hasModify, bool hasDelete );

		private static string DbPath( string dbName )
		{
#if XAMARIN
			var Folder = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ) );
#else
			var Folder = Environment.GetFolderPath( Environment.SpecialFolder.Personal );
#endif
			return Path.Combine( Folder, dbName );
		}

		private void OpenTable<T>( bool reCreate )
		{
			try
			{
				if( reCreate )
				{
					try
					{
						DropTable<T>();
					}
					catch
					{
					}
				}

				CreateTable<T>();
			}
			catch // Probably restructured table
			{
				DropTable<T>();
				CreateTable<T>();
			}
		}

		private void ResetDatabaseLock()
		{
			lock( LockObject )
				DatabaseLock = false;
#if DEBUG
			PrevCaller = Caller;
			Caller     = "None";
#endif
		}

		public void CheckForTrips()
		{
			Task.Run( () =>
			          {
				          try
				          {
					          ServicePolling?.PollingEvent();
				          }
				          finally
				          {
					          InCheckForTrips = false;
				          }
			          } );
		}

		public void ConnectService( AIService<TClient, TAuthToken> service )
		{
			Service = service;
		}

		public Db( string dbName ) : base( DbPath( dbName ) )
		{
			CreateTable<DbVersion>();

			var Version = ( from V in Table<DbVersion>()
			                select V ).FirstOrDefault();

			var ReCreate = ( Version == null ) || ( Version.Version != DB_VERSION );

			if( ReCreate )
			{
				OpenTable<DbVersion>( true );
				DeleteAll<DbVersion>();
				Insert( new DbVersion { Version = DB_VERSION } );
			}

			OpenTable<Configuration>( ReCreate );
			OpenTable<InTripFifo>( ReCreate );
			OpenTable<OutTripFifo>( ReCreate );
			OpenTable<Trip>( ReCreate );
			OpenTable<Gps>( ReCreate );
			OpenTable<Zone>( ReCreate );
			OpenTable<Charge>( ReCreate );
			OpenTable<Claim>( ReCreate );
			OpenTable<TripStatus>( ReCreate );
			OpenTable<BarcodeFifo>( ReCreate );
			OpenTable<Pharmacy>( ReCreate );
			OpenTable<RouteNames>( ReCreate );
			OpenTable<ReturnBinUpdate>( ReCreate );
			OpenTable<RbhLoadingTrailer>( ReCreate );
			OpenTable<RbhUnLoadTrailer>( ReCreate );
			OpenTable<RbnDestructionTrailer>( ReCreate );
			OpenTable<ReturnsEditCreate>( ReCreate );
			OpenTable<RcClaim>( ReCreate );
			OpenTable<RbhForeignWaybill>( ReCreate );
		}

		// Maintain processing order
#if DEBUG
		private string PrevCaller,
		               Caller;

		private void SetDatabaseLock( [CallerMemberName] string callerName = "" )
		{
			if( DatabaseLock )
				Console.WriteLine( $"Locked by: {Caller}, Waiting for lock:{callerName}" );
			else
			{
				PrevCaller = Caller;
				Caller     = callerName;
				Console.WriteLine( $"{PrevCaller}, {Caller}" );
			}
#else
        private void SetDatabaseLock()
        {
#endif
			while( true )
			{
				if( !DatabaseLock )
				{
					lock( LockObject )
					{
						if( !DatabaseLock )
						{
							DatabaseLock = true;

							return;
						}
					}
				}

				Thread.Sleep( 10 );
			}
		}
	}
}