﻿using SQLite;
using Utils;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class Configuration
		{
			[ PrimaryKey ]
			[ AutoIncrement ]
			public int Id { get; set; }

			public bool UseTestingServer { get; set; }
			public byte ManualScan { get; set; }
			public byte Mode { get; set; }

			[ MaxLength( 30 ) ]
			public string AccountId { get; set; } = "";

			[ MaxLength( 30 ) ]
			public string UserName { get; set; } = "";

			[ MaxLength( 30 ) ]
			public string AlternateUserName { get; set; } = "";

			[ MaxLength( 30 ) ]
			public string Password { get; set; } = "";

			public byte VisibilityFilter { get; set; }
			public byte Sort1Filter { get; set; }
			public byte Sort2Filter { get; set; }

			public byte[] ByteActiveDays { get; set; }

			public bool InstallNeeded { get; set; }

			public ushort PollingInterval { get; set; }
			public byte StartHour { get; set; }
			public byte EndHour { get; set; }

			[ MaxLength( 30 ) ]
			public string ClaimsZoneFilter { get; set; }

			public bool PmlAuditLandscape { get; set; }
			public bool PickupDeliveriesLandscape { get; set; }
			public bool ClaimsLandscape { get; set; }


			[Ignore ]
			public DateTimeExtensions.WEEK_DAY[] ActiveDays
			{
				get
				{
					var L = ByteActiveDays.Length;

					var Result = new DateTimeExtensions.WEEK_DAY[ L ];
					for( var I = 0; I < L; I++ )
						Result[ I ] = (DateTimeExtensions.WEEK_DAY)ByteActiveDays[ I ];
					return Result;
				}
				set
				{
					var L = value.Length;
					ByteActiveDays = new byte[ L ];
					for( var I = 0; I < L; I++ )
						ByteActiveDays[ I ] = (byte)value[ I ];
				}
			}
		}
	}
}