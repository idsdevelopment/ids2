﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommsDb.Abstracts;
using Service.Interfaces;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class Claim : AIClaim
		{
		}

		public class Claims : IClaims
		{
			[ PrimaryKey ]
			[ AutoIncrement ]
			public int Id { get; set; }

			public string DriverAuthToken { get; set; }
			public IList<string> TripIds { get; set; }
			public string Pod { get; set; }
			public string Signature { get; set; }
			public int SignatureHeight { get; set; }
			public int SignatureWidth { get; set; }
			public DateTimeOffset PickupTime { get; set; }
		}

		private bool InNotifyClaims;

		private void NotifyClaimUpdate()
		{
			if( !InNotifyClaims )
			{
				InNotifyClaims = true;

				Task.Run( async () =>
				{
					try
					{
						Claim Rec;
						SetDatabaseLock();
						try
						{
							Rec = ( from C in Table<Claim>()
									select C ).FirstOrDefault();
						}
						finally
						{
							ResetDatabaseLock();
						}

						if( Rec != null )
						{
							ServicePolling.WaitForUpdate = true;
							try
							{
								WaitUntilOutFifoIsEmpty();

								List<Claim> Recs;

								SetDatabaseLock();
								try
								{
									Recs = ( from C in Table<Claim>()
											 where C.PickupTime == Rec.PickupTime
											 select C ).ToList();
								}
								finally
								{
									ResetDatabaseLock();
								}

								if( Recs.Any() )
								{
									var Claims = new Claims
												 {
													 DriverAuthToken = Rec.DriverAuthToken,
													 Pod = Rec.Pod,
													 Signature = Rec.Signature,
													 SignatureHeight = Rec.SignatureHeight,
													 SignatureWidth = Rec.SignatureWidth,
													 PickupTime = Rec.PickupTime,
													 TripIds = ( from R in Recs
																 select R.TripId ).ToList()
												 };
									try
									{
										if( await Service.UpdateClaim( Claims ) )
										{
											SetDatabaseLock();
											try
											{
												foreach( var Claim in Recs )
													Delete( Claim );
											}
											finally
											{
												ResetDatabaseLock();
											}
										}
									}
									catch( Exception Exception )
									{
										Console.WriteLine( Exception );
									}
								}
							}
							finally
							{
								ServicePolling.WaitForUpdate = false;
							}
						}
					}
					finally
					{
						InNotifyClaims = false;
					}
				} );
			}
		}

		public void AddClaim( string authToken, IList<string> tripIds, string pod, string signature, int signatureHeight, int signatureWidth, DateTime pickupTime )
		{
			SetDatabaseLock();
			try
			{
				foreach( var Rec in tripIds.Select( tripId => new Claim
															  {
																  DriverAuthToken = authToken,
																  Pod = pod,
																  Signature = signature,
																  SignatureHeight = signatureHeight,
																  SignatureWidth = signatureWidth,
																  TripId = tripId,
																  PickupTime = pickupTime
															  } ) )
					Insert( Rec );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyClaimUpdate();
			}
		}
	}
}