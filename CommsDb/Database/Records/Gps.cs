﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Service.Interfaces;
using SQLite;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public class Points
	{
		public class Point
		{
			public double Latitude { get; set; }

			public double Longitude { get; set; }

			public override string ToString()
			{
				return Latitude + "," + Longitude;
			}

			public bool IsZero => (Latitude == 0) && (Longitude == 0);
		}

		public Point From = new Point(),
		             To = new Point();

		public bool IsZero => From.IsZero && To.IsZero;

		public enum MEASUREMENT
		{
			KILOMETRES,
			MILES,
			NAUTICAL_MILES,
			METRES
		}

		public double DistanceTo(MEASUREMENT measurement = MEASUREMENT.KILOMETRES)
		{
			var Rlat1 = Math.PI * From.Latitude / 180;
			var Rlat2 = Math.PI * To.Latitude / 180;
			var Theta = From.Longitude - To.Longitude;
			var Rtheta = Math.PI * Theta / 180;
			var Dist = Math.Sin(Rlat1) * Math.Sin(Rlat2) + Math.Cos(Rlat1) * Math.Cos(Rlat2) * Math.Cos(Rtheta);
			Dist = Math.Acos(Dist);
			Dist = Dist * 180 / Math.PI;
			Dist = Dist * 60 * 1.1515;

			if (double.IsNaN(Dist))
				Dist = 0;

			return measurement switch
			       {
				       MEASUREMENT.KILOMETRES     => ( Dist * 1.609344 ),
				       MEASUREMENT.METRES         => ( Dist * 1.609344 * 1000 ),
				       MEASUREMENT.NAUTICAL_MILES => ( Dist * 0.8684 ),
				       _                          => Dist
			       };
		}
	}
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public const int GPS_UPDATE_TIME_LIMIT_IN_MINUTES = 1;

		public class Gps
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public DateTimeOffset DateTime { get; set; }
			public double Latitude { get; set; }
			public double Longitude { get; set; }
		}

		private bool InUpdateGps;

		private void NotifyGpsUpdate()
		{
			if( !InUpdateGps )
			{
				InUpdateGps = true;
				Task.Run( async () =>
				          {
					          try
					          {
						          SetDatabaseLock();
						          var Recs = ( from G in Table<Gps>()
						                       orderby G.DateTime
						                       select G ).Take( 200 ).ToList();
						          ResetDatabaseLock();

						          var C = Recs.Count;
						          if( C != 0 )
						          {
							          if( Recs.Count < 100 )
							          {
								          var Now = DateTimeOffset.UtcNow;

								          var Rec = ( from R in Recs
								                      where ( Now - R.DateTime ).TotalMinutes > GPS_UPDATE_TIME_LIMIT_IN_MINUTES
								                      select R ).FirstOrDefault();

								          if( Rec == null )
									          return;
							          }

							          if( await Service.UpdateGps( ( from R in Recs
							                                         select new GpsPoint { Longitude = R.Longitude, Latitude = R.Latitude, Time = R.DateTime } ).ToList() ) )
							          {
								          SetDatabaseLock();
								          foreach( var Rec in Recs )
									          Delete( Rec );
								          ResetDatabaseLock();
							          }
						          }
					          }
					          finally
					          {
						          InUpdateGps = false;
					          }
				          } );
			}
		}

		public void AddGpsPoint( DateTimeOffset date, double latitude, double longitude )
		{
			SetDatabaseLock();
			Insert( new Gps { DateTime = date, Latitude = latitude, Longitude = longitude } );
			ResetDatabaseLock();
			NotifyGpsUpdate();
		}
	}
}