﻿using System.Collections.Generic;
using System.Linq;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class ReturnBinUpdate
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public string Bin { get; set; }
			public string Reference { get; set; }
		}

		public class ReturnsEditCreate : ReturnBinUpdate
		{
		}

		public class RefId
		{
			internal int Id;
			internal string Reference;
		}


		private bool InNotifyUpdateReturnsWithBin;

		private bool InNotifyEditCreateReturnsWithBin;

		private async void NotifyUpdateReturnsWithBin()
		{
			if( !InNotifyUpdateReturnsWithBin )
			{
				InNotifyUpdateReturnsWithBin = true;

				try
				{
					var (Bin, References, Ok) = RbhGetUpdateReturnsWithBin();

					if( Ok )
					{
						try
						{
							await Service.RbhUpdateReturnsWithBin( ( from R in References
							                                   select R.Reference ).ToList(), Bin );

							SetDatabaseLock();

							try
							{
								var Ret = Table<ReturnBinUpdate>();

								foreach( var RefId in References )
								{
									var Rec = ( from R in Ret
									            where R.Id == RefId.Id
									            select R ).FirstOrDefault();

									if( !( Rec is null ) )
										Delete( Rec );
								}
							}
							finally
							{
								ResetDatabaseLock();
							}
						}
						catch
						{
						}
					}
				}
				finally
				{
					InNotifyUpdateReturnsWithBin = false;
				}
			}
		}

		private async void NotifyEditCreateReturnsWithBin()
		{
			if( !InNotifyEditCreateReturnsWithBin )
			{
				InNotifyEditCreateReturnsWithBin = true;

				try
				{
					var (Bin, References, Ok) = RbhGetEditCreateReturnsWithBin();

					if( Ok )
					{
						try
						{
							await Service.RbhUpdateEditCreateWithBin( ( from R in References
							                                      select R.Reference ).ToList(), Bin );

							SetDatabaseLock();

							try
							{
								var Ret = Table<ReturnsEditCreate>();

								foreach( var RefId in References )
								{
									var Rec = ( from R in Ret
									            where R.Id == RefId.Id
									            select R ).FirstOrDefault();

									if( !( Rec is null ) )
										Delete( Rec );
								}
							}
							finally
							{
								ResetDatabaseLock();
							}
						}
						catch
						{
						}
					}
				}
				finally
				{
					InNotifyEditCreateReturnsWithBin = false;
				}
			}
		}


		public void RbhEditCreateReturnsWithBin( string bin, List<string> references )
		{
			SetDatabaseLock();

			try
			{
				foreach( var Reference in references )
				{
					Insert( new ReturnsEditCreate
					        {
						        Bin       = bin,
						        Reference = Reference
					        } );
				}
			}
			finally
			{
				ResetDatabaseLock();
				NotifyEditCreateReturnsWithBin();
			}
		}


		public void RbhUpdateReturnsWithBin( string bin, List<string> references )
		{
			SetDatabaseLock();

			try
			{
				foreach( var Reference in references )
				{
					Insert( new ReturnBinUpdate
					        {
						        Bin       = bin,
						        Reference = Reference
					        } );
				}
			}
			finally
			{
				ResetDatabaseLock();
				NotifyUpdateReturnsWithBin();
			}
		}

		public ( string Bin, List<RefId> References, bool Ok ) RbhGetUpdateReturnsWithBin()
		{
			SetDatabaseLock();

			try
			{
				var Ret = Table<ReturnBinUpdate>();

				var Bin = ( from R in Ret
				            select R.Bin ).FirstOrDefault();

				if( !( Bin is null ) )
				{
					var References = ( from R in Ret
					                   where R.Bin == Bin
					                   select new RefId { Id = R.Id, Reference = R.Reference } ).ToList();

					return ( Bin, References, true );
				}
			}
			finally
			{
				ResetDatabaseLock();
			}

			return ( "", new List<RefId>(), false );
		}

		public (string Bin, List<RefId> References, bool Ok) RbhGetEditCreateReturnsWithBin()
		{
			SetDatabaseLock();

			try
			{
				var Ret = Table<ReturnsEditCreate>();

				var Bin = ( from R in Ret
				            select R.Bin ).FirstOrDefault();

				if( !( Bin is null ) )
				{
					var References = ( from R in Ret
					                   where R.Bin == Bin
					                   select new RefId { Id = R.Id, Reference = R.Reference } ).ToList();

					return ( Bin, References, true );
				}
			}
			finally
			{
				ResetDatabaseLock();
			}

			return ( "", new List<RefId>(), false );
		}
	}
}

