﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RbhUnLoadTrailer
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public string ProBill { get; set; }
			public string References { get; set; }
		}

		private bool InNotifyRbhUnLoadTrailer;

		private async void NotifyRbhUnLoadTrailer()
		{
			if( !InNotifyRbhUnLoadTrailer )
			{
				InNotifyRbhUnLoadTrailer = true;
				SetDatabaseLock();

				try
				{
					var T = Table<RbhUnLoadTrailer>();

					var Rec = ( from R in T
					            select R ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						ResetDatabaseLock();

						var References = Rec.References.Split( ',', StringSplitOptions.RemoveEmptyEntries ).ToList();
						await Service.RbhUnloadTrailer( References );

						SetDatabaseLock();
						Delete( Rec );
					}
				}
				finally
				{
					ResetDatabaseLock();
					InNotifyRbhUnLoadTrailer = false;
				}
			}
		}

		public void RbhUpdateRbhUnLoadTrailer( string proBill, List<string> references )
		{
			var References = string.Join( ',', references );

			SetDatabaseLock();

			try
			{
				Insert( new RbhUnLoadTrailer
				        {
					        References = References,
					        ProBill    = proBill
				        } );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyRbhUnLoadTrailer();
			}
		}
	}
}