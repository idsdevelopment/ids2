﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RbhLoadingTrailer
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public string ProBill { get; set; }
			public string References { get; set; }
			public string Pop { get; set; }
			public string Signature { get; set; }
			public int SigHeight { get; set; }
			public int SigWidth { get; set; }
		}

		private bool InNotifyUpdateLoadingTrailer;

		private async void NotifyUpdateLoadingTrailer()
		{
			if( !InNotifyUpdateLoadingTrailer )
			{
				InNotifyUpdateLoadingTrailer = true;
				SetDatabaseLock();

				try
				{
					var T = Table<RbhLoadingTrailer>();

					var Rec = ( from R in T
					            select R ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						ResetDatabaseLock();

						var References = Rec.References.Split( ',', StringSplitOptions.RemoveEmptyEntries ).ToList();

						if( await Service.RbhLoadingTrailer( Rec.ProBill, Rec.Pop, Rec.SigHeight, Rec.SigWidth, Rec.Signature, References ) )
						{
							SetDatabaseLock();
							Delete( Rec );
						}
					}
				}
				finally
				{
					ResetDatabaseLock();
					InNotifyUpdateLoadingTrailer = false;
				}
			}
		}

		public void RbhUpdateLoadingTrailer( string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			var References = new StringBuilder();

			foreach( var Reference in references )
				References.Append( $"{Reference}," );

			SetDatabaseLock();

			try
			{
				Insert( new RbhLoadingTrailer
				        {
					        References = References.ToString(),
					        Pop        = pop,
					        ProBill    = proBill,
					        Signature  = signature,
					        SigWidth   = sigWidth,
					        SigHeight  = sigHeight
				        } );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyUpdateLoadingTrailer();
			}
		}
	}
}