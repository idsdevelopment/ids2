﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RbhForeignWaybill
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			public string Waybills { get; set; }
			public string Bins { get; set; }
		}

		private bool InNotifyRbhWaybill;

		public void RbhUpdateForeignWaybill( List<string> waybills, List<string> bins )
		{
			Insert( new RbhForeignWaybill
			        {
				        Waybills = string.Join( ',', waybills ),
				        Bins     = string.Join( ',', bins )
			        } );

			NotifyRbhWaybill();
		}

		public async void NotifyRbhWaybill()
		{
			if( !InNotifyRbhWaybill )
			{
				InNotifyRbhWaybill = true;
				SetDatabaseLock();

				try
				{
					var Rec = ( from W in Table<RbhForeignWaybill>()
					            select W ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						ResetDatabaseLock();

						static List<string> Split( string txt )
						{
							return txt.Split( ',', StringSplitOptions.RemoveEmptyEntries ).ToList();
						}

						var Ok = await Service.RbhForeignWaybill( Split( Rec.Waybills ), Split( Rec.Bins ) );

						SetDatabaseLock();

						if( Ok )
							Delete( Rec );
					}
				}
				finally
				{
					ResetDatabaseLock();
					InNotifyRbhWaybill = false;
				}
			}
		}
	}
}