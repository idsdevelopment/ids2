﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Abstracts;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public Task<List<AIService<TClient, TAuthToken>.Missing>> RbhFindMissingShipments( List<string> references )
		{
			return Service.RbhFindMissingShipments( references );
		}

		public Task<List<string>> RbhUnloadTrailerFindBins( string proBill )
		{
			return Service.RbhUnloadTrailerFindBins( proBill );
		}
	}
}