﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RcClaim : Claim
		{
		}

		public class RcClaims : Claims
		{
		}

		private bool InNotifyRcClaims;

		private void NotifyRcClaimUpdate()
		{
			if( !InNotifyRcClaims )
			{
				InNotifyRcClaims = true;

				Task.Run( async () =>
				          {
					          try
					          {
						          RcClaim Rec;
						          SetDatabaseLock();

						          try
						          {
							          Rec = ( from C in Table<RcClaim>()
							                  select C ).FirstOrDefault();
						          }
						          finally
						          {
							          ResetDatabaseLock();
						          }

						          if( Rec != null )
						          {
							          ServicePolling.WaitForUpdate = true;

							          try
							          {
								          WaitUntilOutFifoIsEmpty();

								          List<RcClaim> Recs;

								          SetDatabaseLock();

								          try
								          {
									          Recs = ( from C in Table<RcClaim>()
									                   where C.PickupTime == Rec.PickupTime
									                   select C ).ToList();
								          }
								          finally
								          {
									          ResetDatabaseLock();
								          }

								          if( Recs.Any() )
								          {
									          var Claims = new RcClaims
									                       {
										                       DriverAuthToken = Rec.DriverAuthToken,
										                       Pod             = Rec.Pod,
										                       Signature       = Rec.Signature,
										                       SignatureHeight = Rec.SignatureHeight,
										                       SignatureWidth  = Rec.SignatureWidth,
										                       PickupTime      = Rec.PickupTime,
										                       TripIds = ( from R in Recs
										                                   select R.TripId ).ToList()
									                       };

									          try
									          {
										          if( await Service.RbhUpdateRcClaim( Claims ) )
										          {
											          SetDatabaseLock();

											          try
											          {
												          foreach( var Claim in Recs )
													          Delete( Claim );
											          }
											          finally
											          {
												          ResetDatabaseLock();
											          }
										          }
									          }
									          catch( Exception Exception )
									          {
										          Console.WriteLine( Exception );
									          }
								          }
							          }
							          finally
							          {
								          ServicePolling.WaitForUpdate = false;
							          }
						          }
					          }
					          finally
					          {
						          InNotifyRcClaims = false;
					          }
				          } );
			}
		}

		public void AddRcClaim( string authToken, IList<string> tripIds, string pod, string signature, int signatureHeight, int signatureWidth, DateTime pickupTime )
		{
			SetDatabaseLock();

			try
			{
				foreach( var Rec in tripIds.Select( tripId => new RcClaim
				                                              {
					                                              DriverAuthToken = authToken,
					                                              Pod             = pod,
					                                              Signature       = signature,
					                                              SignatureHeight = signatureHeight,
					                                              SignatureWidth  = signatureWidth,
					                                              TripId          = tripId,
					                                              PickupTime      = pickupTime
				                                              } ) )
					Insert( Rec );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyRcClaimUpdate();
			}
		}
	}
}