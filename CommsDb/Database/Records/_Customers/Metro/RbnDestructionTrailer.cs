﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RbnDestructionTrailer : RbhLoadingTrailer
		{
		}

		private bool InNotifyUpdateDestructionTrailer;

		private async void NotifyRbhUpdateDestructionTrailer()
		{
			if( !InNotifyUpdateDestructionTrailer )
			{
				InNotifyUpdateDestructionTrailer = true;
				SetDatabaseLock();

				try
				{
					var T = Table<RbnDestructionTrailer>();

					var Rec = ( from R in T
					            select R ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						ResetDatabaseLock();

						var References = Rec.References.Split( ',', StringSplitOptions.RemoveEmptyEntries ).ToList();

						if( await Service.RbhDestructionTrailer( Rec.ProBill, Rec.Pop, Rec.SigHeight, Rec.SigWidth, Rec.Signature, References ) )
						{
							SetDatabaseLock();
							Delete( Rec );
						}
					}
				}
				finally
				{
					ResetDatabaseLock();
					InNotifyUpdateDestructionTrailer = false;
				}
			}
		}

		public void RbhUpdateDestructionTrailer( string trailer, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			var References = new StringBuilder();

			foreach( var Reference in references )
				References.Append( $"{Reference}," );

			SetDatabaseLock();

			try
			{
				Insert( new RbnDestructionTrailer
				        {
					        References = References.ToString(),
					        Pop        = pop,
					        ProBill    = trailer,
					        Signature  = signature,
					        SigWidth   = sigWidth,
					        SigHeight  = sigHeight
				        } );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyRbhUpdateDestructionTrailer();
			}
		}
	}
}