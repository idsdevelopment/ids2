﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data.Customers.Priority;
using SQLite;
using Utils;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		private class PriorityLocations
		{
			internal string Code, Location, XlateName;
		}

		public class Pharmacy
		{
			[PrimaryKey]
			public string Name { get; set; }

			public string LocationCode { get; set; }
		}

		private static readonly PriorityLocations[] PLocations =
		{
			new PriorityLocations { Code = "Livonia", Location = "793", XlateName = "Livonia" },
			new PriorityLocations { Code = "Cincinnati", Location = "495", XlateName = "Cincinnati" },
			new PriorityLocations { Code = "Ashland", Location = "496", XlateName = "Ashland" },
			new PriorityLocations { Code = "Perrysburg", Location = "743", XlateName = "Perrysburg" },
			new PriorityLocations { Code = "Hilliard", Location = "742", XlateName = "Hilliard" },
			new PriorityLocations { Code = "Wadsworth", Location = "623", XlateName = "Wadsworth" },
			new PriorityLocations { Code = "Grandrapids", Location = "788", XlateName = "Grandrapids" },
			new PriorityLocations { Code = "Beattyville", Location = "653", XlateName = "Beattyville" },
			new PriorityLocations { Code = "Dayton", Location = "606", XlateName = "Dayton" },
			new PriorityLocations { Code = "Escanaba", Location = "735", XlateName = "Escanaba" },
			new PriorityLocations { Code = "Lexington", Location = "677", XlateName = "Lexington" },
			new PriorityLocations { Code = "West Branch", Location = "790", XlateName = "WestBranch" },
			new PriorityLocations { Code = "Heartland (Stats Only)", Location = "766", XlateName = "Toledo" },
			new PriorityLocations { Code = "Akron", Location = "864", XlateName = "Akron" },

			new PriorityLocations { Code = "Griffith", Location = "754", XlateName = "griffith" },
			new PriorityLocations { Code = "South Bend", Location = "764", XlateName = "southbend" },
			new PriorityLocations { Code = "Fort Wayne", Location = "760", XlateName = "fortwayne" },
			new PriorityLocations { Code = "Indianapolis", Location = "705", XlateName = "indianapolis" },
			new PriorityLocations { Code = "Henderson", Location = "780", XlateName = "henderson" },
			new PriorityLocations { Code = "Louisville", Location = "854", XlateName = "louisville" },

			new PriorityLocations { Code = "Des Plaines", Location = "717", XlateName = "DesPlaines" },
			new PriorityLocations { Code = "Peoria", Location = "797", XlateName = "Peoria" },
			new PriorityLocations { Code = "Decatur", Location = "798", XlateName = "Decatur" },
			new PriorityLocations { Code = "Quad Cities", Location = "859", XlateName = "QuadCities" },

			new PriorityLocations { Code = "Infusion", Location = "888", XlateName = "Infusion" },

			new PriorityLocations { Code = "Minnesota", Location = "464", XlateName = "Minnesota" },
        };

		private static string DoLocation( string name )
		{
			name = name.ToUpper();

			foreach( var PLocation in PLocations )
			{
				if( PLocation.Code.ToUpper() == name )
					return PLocation.Location.ToUpper();
			}

			return "";
		}

		public void AddPharmacyRouteNames( PriorityDeviceRoutes pharmacyRouteNames )
		{
			SetDatabaseLock();
			var Missing = new HashSet<string>();
			foreach( var Location in PLocations )
				Missing.Add( Location.Code.ToUpper() );

			try
			{
				DeleteAll<RouteNames>();
				DeleteAll<Pharmacy>();

				void DoLoc( string name, IEnumerable<string> routeNames )
				{
					var Location = DoLocation( name );

					Insert( new Pharmacy
					        {
						        Name = name.ToUpper(),
						        LocationCode = Location
					        } );

					foreach( var RouteName in routeNames )
						Insert( new RouteNames { PharmacyName = name, RouteName = RouteName } );
				}

				foreach( var PharmacyRouteName in pharmacyRouteNames )
				{
					var PName = "";
					var Pharmacy = PharmacyRouteName.Pharmacy;

					foreach( var PLocation in PLocations )
					{
						if( Pharmacy.Compare( PLocation.XlateName, StringComparison.OrdinalIgnoreCase ) == 0 )
						{
							PName = PLocation.Code;
							break;
						}
					}

					if( PName.IsNotNullOrWhiteSpace() )
					{
						Missing.Remove( PName.ToUpper() );
						DoLoc( PName, PharmacyRouteName.RouteNames );
					}
				}

				var DontUse = new List<string> { "Don't use routes" };
				foreach( var M in Missing )
					DoLoc( M, DontUse );
			}
			finally
			{
				ResetDatabaseLock();
			}
		}

		public Dictionary<string, string> GetPharmacyLocations()
		{
			return ( from Pharmacy in from Pharmacy in Table<Pharmacy>()
			                          select Pharmacy
			         where !Pharmacy.Name.IsNullOrWhiteSpace() && !Pharmacy.LocationCode.IsNullOrWhiteSpace()
			         select Pharmacy ).ToDictionary( pharmacy => pharmacy.Name, pharmacy => pharmacy.LocationCode );
		}


		public Dictionary<string, List<string>> GetPharmaciesRoutes()
		{
			var Result = new Dictionary<string, List<string>>();
			SetDatabaseLock();
			try
			{
				// Had to split into separate queries, had problem with anonymous class
				var Pharmacies = ( from Pharmacy in Table<Pharmacy>()
				                   select Pharmacy ).ToList();

				var Routes = ( from R in Table<RouteNames>()
				               select R ).ToList();

				foreach( var Pharmacy in Pharmacies )
				{
					var Rts = new List<string>();
					foreach( var Route in Routes )
					{
						if( string.Equals( Pharmacy.Name, Route.PharmacyName, StringComparison.CurrentCultureIgnoreCase ) )
							Rts.Add( Route.RouteName );
					}

					Result.Add( Pharmacy.Name, Rts );
				}
			}
			finally
			{
				ResetDatabaseLock();
			}

			return Result;
		}
	}
}