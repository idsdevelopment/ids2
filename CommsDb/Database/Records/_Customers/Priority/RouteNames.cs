﻿using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class RouteNames
		{
			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			[Indexed]
			public string PharmacyName { get; set; }

			public string RouteName { get; set; }
		}
	}
}