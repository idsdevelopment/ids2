﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType> : SQLiteConnection where TClient : new()
	{
		private bool InBarcodeNotify;

		public class BarCode
		{
			public BarCode()
			{
			}

			public BarCode( BarCode barcode )
			{
				Code = barcode.Code;
				Quantity = barcode.Quantity;
			}

			[PrimaryKey]
			[AutoIncrement]
			public int Id { get; set; }

			[Indexed]
			public string Code { get; set; }

			public decimal Quantity { get; set; }

			public DateTimeOffset PickupTime { get; set; }

			public string PackageType { get; set; }
			public string ServiceLevel { get; set; }
		}

		public void NotifyBarcodeUpdate()
		{
			if( !InBarcodeNotify )
			{
				InBarcodeNotify = true;
				Task.Run( async () =>
				          {
					          try
					          {
						          if( OutBarcodeFifoHasRecords )
						          {
							          ServicePolling.WaitForUpdate = true;
							          WaitUntilOutFifoIsEmpty();

							          while( true )
							          {
								          var Rec = OutBarcodeFifo;
								          if( Rec != null )
								          {
									          if( await Service.CreateTripFromBarcode( Rec.Code, Rec.Quantity, Rec.PickupTime, Rec.PackageType, Rec.ServiceLevel ) )
										          Rec.OnSuccess?.Invoke();
								          }
								          else
									          break;
							          }
						          }
					          }
					          finally
					          {
						          ServicePolling.WaitForUpdate = false;
						          InBarcodeNotify = false;
					          }
				          } );
			}
		}

		public void CreateTripFromUpdateBarcode( string barcode, decimal quantity, DateTimeOffset pickupTime, string packageType, string serviceLevel, bool notify = true )
		{
			var Date = new DateTimeOffset( pickupTime.DateTime, new TimeSpan( 0 ) );
			OutBarcodeFifo = new BarcodeFifo { Code = barcode, Quantity = quantity, PickupTime = Date, ServiceLevel = serviceLevel, PackageType = packageType };
		}

		public void CreateTripFromUpdateBarcode( List<string> barcodes, DateTimeOffset pickupTime, string packageType, string serviceLevel )
		{
			foreach( var Barcode in barcodes )
				CreateTripFromUpdateBarcode( Barcode, 1, pickupTime, packageType, serviceLevel, false );

			NotifyBarcodeUpdate();
		}
	}
}