﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommsDb.Abstracts;
using Service.Interfaces;
using SQLite;
using Utils;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class Trip : AITrip<TOriginalTripType>
		{
			[PrimaryKey]
			public string DbTripId
			{
				get => TripId;
				set => TripId = value;
			}

			public Trip()
			{
			}

			public Trip( Trip t ) : base( t )
			{
			}

			public Trip( ITrip t ) : base( t )
			{
			}
		}

		private List<Trip> FilteredTrips
		{
			get
			{
				SetDatabaseLock();

				try
				{
					return ( from T in Table<Trip>()
					         select T ).Where( trip => StatusFilterSet.Any( status => trip.Status == status ) ).ToList();
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		private List<Trip> UnFilteredTrips
		{
			get
			{
				SetDatabaseLock();

				try
				{
					return ( from T in Table<Trip>()
					         select T ).ToList();
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		public bool HasTrips => FilteredTrips.Count > 0;

		public List<Trip> Trips => FilteredTrips;

		public int TripCount
		{
			get
			{
				SetDatabaseLock();

				try
				{
					return ( from T in Table<Trip>()
					         select T ).Count();
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		private readonly object PushInLockObject = new object();

		private bool HadOutFifoRecord;

		private bool InOutNotify;

		internal DateTime LastOutFifoUpdate = DateTime.MinValue;

		public Action NotifyOutFifoEmpty;

		public Task DeleteAllTripsAsync()
		{
			return Task.Run( () =>
			                 {
				                 SetDatabaseLock();

				                 try
				                 {
					                 DeleteAll<InTripFifo>();
					                 DeleteAll<Trip>();
				                 }
				                 finally
				                 {
					                 ResetDatabaseLock();
				                 }
			                 } );
		}

		public void DeleteAllLocalTrips()
		{
			SetDatabaseLock();

			try
			{
				DeleteAll<InTripFifo>();
				DeleteAll<Trip>();
			}
			finally
			{
				ResetDatabaseLock();
			}
		}

		public Task DeleteAllLocalTripsAsync()
		{
			return Task.Run( DeleteAllLocalTrips );
		}

		internal void NotifyInFifoUpdate()
		{
			lock( PushInLockObject )
			{
				var HasNew        = false;
				var HasModify     = false;
				var HasDelete     = false;
				var NeedOutNotify = false;

				SetDatabaseLock();

				try
				{
					var Recs = ( from I in Table<InTripFifo>()
					             orderby I.Id
					             select I ).ToList();

					foreach( var Rec in Recs )
					{
						var Rc = (Trip)new Trip().Copy( Rec );

						if( Rc.Status >= STATUS.DISPATCHED )
						{
							var DoUpdate = !Rc.ReceivedByDevice;
							Rc.ReceivedByDevice = true;

							if( DoUpdate )
							{
								ResetDatabaseLock();
								OutFifo = new OutTripFifo( Rc );
								SetDatabaseLock();
								NeedOutNotify = true;
							}
						}

						switch( Rec.Operation )
						{
						case InTripFifo.OPERATION.ADD:
							Insert( Rc, typeof( Trip ) );
							HasNew = true;

							break;

						case InTripFifo.OPERATION.DELETE:
							var DbRec = ( from T in Table<Trip>()
							              where T.TripId == Rc.TripId
							              select T ).FirstOrDefault();

							if( DbRec != null )
							{
								Delete( DbRec );
								HasDelete = true;
							}

							break;

						case InTripFifo.OPERATION.REPLACE:
							DbRec = ( from Dbr in Table<Trip>()
							          where Dbr.TripId == Rc.TripId
							          select Dbr ).FirstOrDefault();

							if( DbRec != null )
							{
								DbRec.Copy( Rc );
								Update( DbRec );
								HasModify = true;
							}

							break;
						}

						Delete( Rec );
					}
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
				finally
				{
					ResetDatabaseLock();

					try
					{
						if( OnNotifyTripChange != null )
						{
							Task.Run( () =>
							          {
								          OnNotifyTripChange( TripCount, HasNew, HasModify, HasDelete );
							          } );
						}

						if( NeedOutNotify )
							NotifyOutFifoUpdate();
					}
					catch( Exception Exception )
					{
						Console.WriteLine( Exception );
					}
				}
			}
		}

		internal void NotifyOutFifoUpdate()
		{
			lock( PushInLockObject )
			{
				if( !InOutNotify )
				{
					InOutNotify = true;

					Task.Run( async () =>
					          {
						          try
						          {
							          while( true )
							          {
								          var Rec = OutFifo;

								          if( Rec != null )
								          {
									          HadOutFifoRecord = true;

									          var Status = await Service.UpdateTrip( Rec, Rec.PreviousUpdateStatus );

									          if( Status == UPDATE_STATUS.OK )
									          {
										          Console.WriteLine( $"OutFifo: {Rec.TripId}" );
										          LastOutFifoUpdate = DateTime.Now;
										          Rec.OnSuccess?.Invoke(); // Delete the record from the out fifo

										          continue;
									          }

									          Rec.PreviousUpdateStatus = Status;
									          Update( Rec );
								          }
								          else if( NotifyOutFifoEmpty.IsNotNull() && HadOutFifoRecord )
								          {
									          HadOutFifoRecord = false;

									          try
									          {
										          NotifyOutFifoEmpty();
									          }
									          catch // Program may have been closed
									          {
									          }
									          finally
									          {
										          NotifyOutFifoEmpty = null;
									          }
								          }
								          else
									          HadOutFifoRecord = false;

								          break;
							          }
						          }
						          finally
						          {
							          InOutNotify = false;
						          }
					          } );
				}
			}
		}

		public ITrip GeTrip( string tripId )
		{
			SetDatabaseLock();

			try
			{
				return ( from Trip in Table<Trip>()
				         where Trip.TripId == tripId
				         select Trip ).FirstOrDefault();
			}
			finally
			{
				ResetDatabaseLock();
			}
		}

		public async Task<(ITrip Trip, bool Ok )> GetRemoteTrip( string tripId )
		{
			var (Trip, Ok) = await Service.GetTrip( tripId, IgnoreTripAddress );

			return Ok ? ( Trip, Ok: true ) : ( Trip: (ITrip)null, Ok: false );
		}


		public void PushIn( IList<ITrip> trips )
		{
			lock( PushInLockObject )
			{
				var DbTrips = UnFilteredTrips;

				List<OutTripFifo> OutFifoTrips;

				SetDatabaseLock();

				try
				{
					OutFifoTrips = ( from F in Table<OutTripFifo>()
					                 select F ).ToList();
				}
				finally
				{
					ResetDatabaseLock();
				}

				bool IsFifoModify( ITrip t )
				{
					return ( from F in OutFifoTrips
					         where F.TripId == t.TripId
					         select F ).FirstOrDefault() != null;
				}

				// Do Deletes First
				foreach( var DbTrip in from DbTrip in DbTrips
				                       where trips.All( trip => trip.TripId != DbTrip.TripId )
				                       select DbTrip )
				{
					InFifo = new InTripFifo( DbTrip )
					         {
						         Operation = InTripFifo.OPERATION.DELETE
					         };
				}

				var NewTrips = new List<ITrip>();

				// New Trips
				foreach( var Trip in ( from Trip in trips
				                       where DbTrips.All( dbTrip => dbTrip.TripId != Trip.TripId )
				                       select Trip ).Where( trip => !IsFifoModify( trip ) ) )
				{
					NewTrips.Add( Trip );

					InFifo = new InTripFifo( Trip )
					         {
						         Operation = InTripFifo.OPERATION.ADD
					         };
				}

				var NotNewTrips = ( from T in DbTrips
				                    where NewTrips.All( newTrip => newTrip.TripId != T.TripId )
				                    select T ).ToList();

				// Modified Trips
				foreach( var NotNewTrip in NotNewTrips )
				{
					foreach( var Trip in trips )
					{
						if( Trip.TripId == NotNewTrip.TripId )
						{
							var Equal = NotNewTrip.EqualTo( Trip );

							if( !Equal )
							{
								InFifo = new InTripFifo( Trip )
								         {
									         Operation = InTripFifo.OPERATION.REPLACE
								         };
							}
						}
					}
				}
			}

			NotifyInFifoUpdate(); // Fifo might still have trips
		}

		public async Task<(CONTAINER_STATUS Status, string ContainerId)> GetTripsInContainer( string containerId )
		{
			DeleteAllLocalTrips();
			var (List, ContainerId, Status) = await Service.GetTripsInContainer( containerId );

			if( Status == CONTAINER_STATUS.OK )
				PushIn( List );

			return ( Status, ContainerId );
		}

		public void UpdateRemoteTrip( ITrip trip )
		{
			OutFifo = new OutTripFifo( trip );
			NotifyOutFifoUpdate();
		}

		public void UpdateTrip( ITrip trip, bool delete = false )
		{
			SetDatabaseLock();

			var Rec = ( from T in Table<Trip>()

			            // ReSharper disable once ImplicitlyCapturedClosure
			            where T.TripId == trip.TripId
			            select T ).FirstOrDefault();

			if( Rec != null )
			{
				if( delete )
					Delete( Rec );
				else
				{
					Rec.Copy( trip );
					Update( Rec );
				}

				ResetDatabaseLock();
				UpdateRemoteTrip( trip );
			}
			else
			{
				ResetDatabaseLock();

				// If creating trip don't add to local database
				switch( trip.Status )
				{
				case STATUS.CREATE_NEW_TRIP:
				case STATUS.CREATE_NEW_TRIP_AS_PICKED_UP:
				case STATUS.CREATE_NEW_TRIP_AS_DISPATCHED:
				// Send Deletions
				case STATUS.DELETED:
					UpdateRemoteTrip( trip );

					break;
				}
			}
		}

		public List<Trip> GetDispatchedTripsByCurrentZone( string currentZone )
		{
			currentZone = currentZone.Trim();

			SetDatabaseLock();

			try
			{
				return ( from T in Table<Trip>()
				         where ( T.CurrentZone == currentZone ) && ( T.Status == STATUS.DISPATCHED )
				         select T ).ToList();
			}
			finally
			{
				ResetDatabaseLock();
			}
		}

		public Task ChangeCurrentZoneAsync( List<Trip> trips, string newCurrentZone )
		{
			return Task.Run( () =>
			                 {
				                 newCurrentZone = newCurrentZone.Trim();

				                 try
				                 {
					                 var  UpdTime = DateTime.Now;
					                 Trip Trip    = null;

					                 foreach( var T in trips )
					                 {
						                 SetDatabaseLock();

						                 try
						                 {
							                 Trip = ( from T1 in Table<Trip>()
							                          where T1.TripId == T.TripId
							                          select T1 ).FirstOrDefault();

							                 if( Trip != null )
							                 {
								                 Trip.CurrentZone = newCurrentZone;
								                 Trip.UpdatedTime = UpdTime;
								                 Update( Trip );
							                 }
						                 }
						                 finally
						                 {
							                 ResetDatabaseLock();

							                 if( Trip != null )
								                 OutFifo = new OutTripFifo( Trip );
						                 }
					                 }
				                 }
				                 finally
				                 {
					                 NotifyOutFifoUpdate();
				                 }
			                 } );
		}
	}
}