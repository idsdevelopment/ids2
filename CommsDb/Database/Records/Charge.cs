﻿using System.Collections.Generic;
using System.Linq;
using Service.Interfaces;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class Charge : ICharge
		{
			public string TripId { get; set; }
			public string Name { get; set; }
			public string Label { get; set; }
			public decimal Value { get; set; }
		}

		public List<Charge> GetChargesForTrip( string tripId )
		{
			return ( from C in Table<Charge>()
					 where C.TripId == tripId
					 select C ).ToList();
		}
	}
}