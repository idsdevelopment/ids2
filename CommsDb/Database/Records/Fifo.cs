﻿using System;
using System.Linq;
using System.Threading;
using CommsDb.Abstracts;
using Service.Interfaces;
using SQLite;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		internal class TripFifo : AITrip<TOriginalTripType>
		{
			[ PrimaryKey ]
			[ AutoIncrement ]
			public int Id { get; set; }

			public UPDATE_STATUS PreviousUpdateStatus { get; set; } = UPDATE_STATUS.UNKNOWN;

			internal TripFifo()
			{
			}

			internal TripFifo( ITrip t )
			{
				this.Copy( t );
			}

			internal Action OnSuccess;
		}

		internal class InTripFifo : TripFifo
		{
			public enum OPERATION
			{
				ADD,
				DELETE,
				REPLACE
			}

			public OPERATION Operation { get; set; }

			public InTripFifo()
			{
			}

			public InTripFifo( ITrip t ) : base( t )
			{
			}
		}

		internal class OutTripFifo : TripFifo
		{
			public OutTripFifo()
			{
			}

			public OutTripFifo( ITrip t ) : base( t )
			{
			}
		}

		internal class BarcodeFifo : BarCode
		{
			internal Action OnSuccess;
		}

		internal InTripFifo InFifo
		{
			get
			{
				SetDatabaseLock();
				try
				{
					var Rec = ( from I in Table<InTripFifo>()
								orderby I.Id
								select I ).FirstOrDefault();
					if( Rec != null )
						Delete( Rec );
					return Rec;
				}
				finally
				{
					ResetDatabaseLock();
				}
			}

			set
			{
				SetDatabaseLock();
				try
				{
					Insert( value );
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		internal bool OutFifoHasRecords
		{
			get
			{
				SetDatabaseLock();
				try
				{
					return ( from O in Table<OutTripFifo>()
							 select O ).FirstOrDefault() != null;
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		internal void WaitUntilOutFifoIsEmpty()
		{
			while( OutFifoHasRecords )
				Thread.Sleep( 200 );

			// Give the server time to update the last message
			if( new TimeSpan( ( DateTime.Now - LastOutFifoUpdate ).Ticks ).Milliseconds < 1000 )
				Thread.Sleep( 1000 );
		}

		internal OutTripFifo OutFifo
		{
			get
			{
				SetDatabaseLock();
				try
				{
					var Rec = ( from I in Table<OutTripFifo>()
								orderby I.Id
								select I ).FirstOrDefault();

					if( Rec != null )
					{
						Rec.OnSuccess = () =>
						{
							SetDatabaseLock();
							try
							{
								Delete( Rec );
							}
							finally
							{
								ResetDatabaseLock();
							}
						};
					}
					return Rec;
				}
				finally
				{
					ResetDatabaseLock();
				}
			}

			set
			{
				SetDatabaseLock();
				try
				{
					Insert( value );
				}
				finally
				{
					ResetDatabaseLock();
				}
			}
		}

		internal bool OutBarcodeFifoHasRecords => ( from B in Table<BarcodeFifo>()
												   select B ).FirstOrDefault() != null;

		internal BarcodeFifo OutBarcodeFifo
		{
            get
			{
				SetDatabaseLock();
				try
				{
					var Rec = ( from B in Table<BarcodeFifo>()
								orderby B.Id
								select B ).FirstOrDefault();
					if( Rec != null )
					{
						Rec.OnSuccess = () =>
						{
							SetDatabaseLock();
							try
							{
								Delete( Rec );
							}
							finally
							{
								ResetDatabaseLock();
							}
						};
					}
					return Rec;
				}
				finally
				{
					ResetDatabaseLock();
				}
			}

			set
			{
				SetDatabaseLock();
				try
				{
					Insert( value );
				}
				finally
				{
					ResetDatabaseLock();
					NotifyBarcodeUpdate();
				}
			}
		}
	}
}