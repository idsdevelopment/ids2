﻿using System.Collections;
using System.Linq;
using CommsDb.Abstracts;

// ReSharper disable UnusedTypeParameter

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class Zone : AIZone
		{
		}

		public async void GetZones()
		{
			UpdateZones( await Service.GetZones() );
		}

		public bool HasZone( string zone )
		{
			return ( from Z in Table<Zone>()
					 select Z ).FirstOrDefault() != null;
		}

		public void UpdateZones( IList zones )
		{
			SetDatabaseLock();
			try
			{
				DeleteAll<Zone>();
				foreach( var Zone in zones )
					Insert( Zone );
			}

			finally
			{
				ResetDatabaseLock();
			}
		}
	}
}