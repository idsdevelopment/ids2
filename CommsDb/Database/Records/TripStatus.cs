﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interfaces;
using SQLite;

namespace CommsDb.Database
{
	public partial class Db<TClient, TAuthToken, TOriginalTripType>
	{
		public class TripStatus : ITripStatus
		{
			[ PrimaryKey ]
			[ AutoIncrement ]
			public int Id { get; set; }

			public string TripId { get; set; }
			public STATUS Status { get; set; }
			public DateTimeOffset UpdateTime { get; set; }
		}

		public void NotifyTripStatusUpdate()
		{
			Task.Run( async () =>
			{
				SetDatabaseLock();
				try
				{
					var Recs = ( from S in Table<TripStatus>()
								 select S ).ToList();

					// Can't  do cast in database query
					var SRecs = ( from R in Recs
								  select (ITripStatus)R ).ToList();

					ResetDatabaseLock();

					if( SRecs.Count > 0 )
					{
						ServicePolling.WaitForUpdate = true;
						try
						{
							WaitUntilOutFifoIsEmpty();

							var Ok = await Service.UpdateTripStatus( SRecs );
							SetDatabaseLock();
							if( Ok )
							{
								foreach( var Rec in Recs )
									Delete( Rec );
							}
						}
						finally
						{
							ServicePolling.WaitForUpdate = false;
						}
					}
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
				finally
				{
					ResetDatabaseLock();
				}
			} );
		}

		public void UpdateTripStatus( List<TripStatus> tripStatus )
		{
			SetDatabaseLock();
			try
			{
				foreach( var Status in tripStatus )
					Insert( Status );
			}
			finally
			{
				ResetDatabaseLock();
				NotifyTripStatusUpdate();
			}
		}
	}
}