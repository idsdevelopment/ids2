﻿using System;
using Service.Interfaces;
using SQLite;

namespace CommsDb.Abstracts
{
	public abstract class AIClaim : IClaim
	{
		[ PrimaryKey ]
		[ AutoIncrement ]
		public int Id { get; set; }

		[Indexed]
		public DateTime PickupTime { get; set; }

        public string DriverAuthToken { get; set; }
		public string TripId { get; set; }
		public string Pod { get; set; }
		public string Signature { get; set; }
		public int SignatureHeight { get; set; }
		public int SignatureWidth { get; set; }
	}
}