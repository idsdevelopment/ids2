﻿using System;
using Newtonsoft.Json;
using Service.Interfaces;
using SQLite;

#pragma warning disable 660,661

namespace CommsDb.Abstracts
{
	public abstract class AITrip<TOriginalTripType> : ITrip
	{
		public string TripId { get; set; }
		public string AccountId { get; set; }
		public string AccountNumber { get; set; }
		public string BillingAccount { get; set; }
		public string CallerEmailAddress { get; set; }

		public string PickupCompanyName { get; set; }
		public string PickupSuite { get; set; }
		public string PickupAddressLine1 { get; set; }
		public string PickupAddressLine2 { get; set; }
		public string PickupVicinity { get; set; }
		public string PickupCity { get; set; }
		public string PickupRegion { get; set; }
		public string PickupPostalCode { get; set; }
		public string PickupCountry { get; set; }
		public string PickupCountryCode { get; set; }
		public string PickupPhone { get; set; }
		public string PickupFax { get; set; }
		public string PickupMobile { get; set; }
		public string PickupEmailAddress { get; set; }
		public string PickupContact { get; set; }
		public decimal PickupLatitude { get; set; }
		public decimal PickupLongitude { get; set; }
		public string PickupBarcode { get; set; }

		public string DeliveryCompanyName { get; set; }
		public string DeliverySuite { get; set; }
		public string DeliveryAddressLine1 { get; set; }
		public string DeliveryAddressLine2 { get; set; }
		public string DeliveryVicinity { get; set; }
		public string DeliveryCity { get; set; }
		public string DeliveryRegion { get; set; }
		public string DeliveryPostalCode { get; set; }
		public string DeliveryCountry { get; set; }
		public string DeliveryCountryCode { get; set; }
		public string DeliveryPhone { get; set; }
		public string DeliveryFax { get; set; }
		public string DeliveryMobile { get; set; }
		public string DeliveryEmailAddress { get; set; }
		public string DeliveryContact { get; set; }
		public decimal DeliveryLatitude { get; set; }
		public decimal DeliveryLongitude { get; set; }
		public string DeliveryBarcode { get; set; }

		public STATUS Status { get; set; }
		public string ServiceLevel { get; set; }
		public string PackageType { get; set; }

		public bool ReceivedByDevice { get; set; }
		public bool ReadByDriver { get; set; }

		public string Reference { get; set; }

		public DateTimeOffset PickupTime { get; set; }
		public DateTimeOffset DeliveryTime { get; set; }
		public DateTimeOffset DueTime { get; set; }
		public DateTimeOffset ReadyTime { get; set; }
		public DateTimeOffset UpdatedTime { get; set; }
		public DateTimeOffset PickupArrivalTime { get; set; }
		public DateTimeOffset DeliveryArrivalTime { get; set; }

		public decimal Weight { get; set; }
		public decimal Pieces { get; set; }
		public decimal OriginalPieceCount { get; set; }

		public string CurrentZone { get; set; }
		public string PickupZone { get; set; }
		public string DeliveryZone { get; set; }

		public string BillingNote { get; set; }
		public string PickupNote { get; set; }
		public string DeliveryNote { get; set; }

		public string PodName { get; set; }
		public string Signature { get; set; }
		public bool ShowChargeAmount { get; set; }
		public decimal ChargeAmount { get; set; }
		public string Pallets { get; set; }
		public UNDELIVERABLE_MODE UndeliverableMode { get; set; }
		public string UndeliverableReason { get; set; }

		[MaxLength( 255 )]
		public byte[] MissingPieces { get; set; } = new byte[ 255 ];

		[Ignore]
		public object OriginalObject
		{
			get => JsonConvert.DeserializeObject<TOriginalTripType>( OriginalDataAsJSON );
			set => OriginalDataAsJSON = JsonConvert.SerializeObject( value );
		}

		public string Driver { get; set; }

		public string OriginalDataAsJSON { get; set; }
		public string OriginalPickupAddressId { get; set; }
		public string OriginalDeliveryAddressId { get; set; }

		public bool Hidden; // Not property, doesn't go to database

		protected AITrip()
		{
		}

		protected AITrip( ITrip t )
		{
			TripId         = t.TripId;
			AccountId      = t.AccountId;
			AccountNumber  = t.AccountNumber;
			BillingAccount = t.BillingAccount;
			Driver         = t.Driver;
			Status         = t.Status;

			CallerEmailAddress = t.CallerEmailAddress;

			ServiceLevel        = t.ServiceLevel;
			PackageType         = t.PackageType;
			ReceivedByDevice    = t.ReceivedByDevice;
			ReadByDriver        = t.ReadByDriver;
			Reference           = t.Reference;
			PickupTime          = t.PickupTime;
			DeliveryTime        = t.DeliveryTime;
			DueTime             = t.DueTime;
			ReadyTime           = t.ReadyTime;
			UpdatedTime         = t.UpdatedTime;
			PickupArrivalTime   = t.PickupArrivalTime;
			DeliveryArrivalTime = t.DeliveryArrivalTime;

			Weight             = t.Weight;
			Pieces             = t.Pieces;
			OriginalPieceCount = t.OriginalPieceCount;

			CurrentZone        = t.CurrentZone;
			PickupZone         = t.PickupZone;
			DeliveryZone       = t.DeliveryZone;
			BillingNote        = t.BillingNote;
			PickupNote         = t.PickupNote;
			DeliveryNote       = t.DeliveryNote;
			PodName            = t.PodName;
			Signature          = t.Signature;
			ShowChargeAmount   = t.ShowChargeAmount;
			ChargeAmount       = t.ChargeAmount;
			Pallets            = t.Pallets;
			UndeliverableMode  = t.UndeliverableMode;
			OriginalDataAsJSON = t.OriginalDataAsJSON;

			PickupCompanyName  = t.PickupCompanyName;
			PickupSuite        = t.PickupSuite;
			PickupAddressLine1 = t.PickupAddressLine1;
			PickupAddressLine2 = t.PickupAddressLine2;
			PickupVicinity     = t.PickupVicinity;
			PickupCity         = t.PickupCity;
			PickupRegion       = t.PickupRegion;
			PickupPostalCode   = t.PickupPostalCode;
			PickupCountry      = t.PickupCountry;
			PickupCountryCode  = t.PickupCountryCode;
			PickupPhone        = t.PickupPhone;
			PickupFax          = t.PickupFax;
			PickupMobile       = t.PickupMobile;
			PickupEmailAddress = t.PickupEmailAddress;
			PickupContact      = t.PickupContact;
			PickupLatitude     = t.PickupLatitude;
			PickupLongitude    = t.PickupLongitude;
			PickupBarcode      = t.PickupBarcode ?? "";

			DeliveryCompanyName  = t.DeliveryCompanyName;
			DeliverySuite        = t.DeliverySuite;
			DeliveryAddressLine1 = t.DeliveryAddressLine1;
			DeliveryAddressLine2 = t.DeliveryAddressLine2;
			DeliveryVicinity     = t.DeliveryVicinity;
			DeliveryCity         = t.DeliveryCity;
			DeliveryRegion       = t.DeliveryRegion;
			DeliveryPostalCode   = t.DeliveryPostalCode;
			DeliveryCountry      = t.DeliveryCountry;
			DeliveryCountryCode  = t.DeliveryCountryCode;
			DeliveryPhone        = t.DeliveryPhone;
			DeliveryFax          = t.DeliveryFax;
			DeliveryMobile       = t.DeliveryMobile;
			DeliveryEmailAddress = t.DeliveryEmailAddress;
			DeliveryContact      = t.DeliveryContact;
			DeliveryLatitude     = t.DeliveryLatitude;
			DeliveryLongitude    = t.DeliveryLongitude;
			DeliveryBarcode      = t.DeliveryBarcode ?? "";

			OriginalDeliveryAddressId = t.OriginalDeliveryAddressId;
			OriginalPickupAddressId   = t.OriginalPickupAddressId;
		}

		protected AITrip( AITrip<TOriginalTripType> t ) : this( (ITrip)t )
		{
			Hidden        = t.Hidden;
			MissingPieces = t.MissingPieces;
		}
	}
}