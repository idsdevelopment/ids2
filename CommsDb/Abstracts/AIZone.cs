﻿using Service.Interfaces;
using SQLite;

namespace CommsDb.Abstracts
{
	public abstract class AIZone : IZone
	{
		[ PrimaryKey ]
		public string ZoneName { get; set; } = "";
	}
}