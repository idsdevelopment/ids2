﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Service.Interfaces;

namespace Service.Abstracts
{
	public abstract partial class AIService<TClient, TAuthToken> : IService<TAuthToken> where TClient : new()
	{
		public const int DEFAULT_PING_INTERVAL_IN_SECONDS = 15;

		public class Missing
		{
			public string Reference,
			              CompanyName,
			              TripId,
			              MercuryTripId;
		}

		protected TClient NewClient => new TClient();

		public bool OnLine { get; private set; }
		public Dictionary<string, string> Preferences => _Preferences ??= GetPreferences( AuthToken ).Result;

		public string AuthTokenAsString => AuthToken != null ? AuthToken.ToString() : "";

		public List<IEvent> EventQueue { get; } = new List<IEvent>();

		private class TimedEvent : ITimedEvent
		{
			public DateTime DueTime { get; set; }
			public TimeSpan Interval { get; internal set; }

			public Action Event { get; set; }

			public bool Disabled
			{
				get => _Disabled;
				set
				{
					_Disabled = value;

					if( !Disabled )
						DueTime = DateTime.Now.Add( Interval );
				}
			}

			public string EventID { get; set; }
			private bool _Disabled;
		}

		private Dictionary<string, string> _Preferences;
		private bool AddedPing;
		public TAuthToken AuthToken;

		private bool InCycle;

		public Action<bool> OnPing;

		public abstract Task<bool> RbhForeignWaybill( TAuthToken authToken, List<string> waybills, List<string> bins );

		public abstract Task<List<Missing>> RbhFindMissingShipments( TAuthToken authToken, List<string> references );
		public abstract Task<bool> RbhLoadingTrailer( TAuthToken authToken, string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references );
		public abstract Task<List<string>> RbhUnloadTrailerFindBins( TAuthToken authToken, string proBill );
		public abstract Task RbhUnloadTrailer( TAuthToken authToken, List<string> references );
		public abstract Task<bool> RbhDestructionTrailer( TAuthToken authToken, string trailer, string pop, int sigHeight, int sigWidth, string signature, List<string> references );

		public abstract Task<bool> UpdateClaim( IClaims claims );
		public abstract Task<bool> RbhUpdateRcClaim( IClaims claims );
		public abstract Task<List<ITrip>> GetTripsAtLocation( TAuthToken authToken, string locationBarcode );

		public Task<bool> RbhForeignWaybill( List<string> waybills, List<string> bins )
		{
			return RbhForeignWaybill( AuthToken, waybills, bins );
		}

		public void ClearPreferences()
		{
			_Preferences = null;
		}


		public async Task<(CONNECTION_STATUS Status, IUser User)> Login( string carrierId, string userName, string password )
		{
			var (Status, AuthToken1, User) = await Connect( carrierId, userName, password );

			if( Status == CONNECTION_STATUS.OK )
				AuthToken = AuthToken1;

			return ( Status, User );
		}

		public Task<List<Missing>> RbhFindMissingShipments( List<string> references )
		{
			return RbhFindMissingShipments( AuthToken, references );
		}

		public Task<bool> RbhLoadingTrailer( string proBill, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			return RbhLoadingTrailer( AuthToken, proBill, pop, sigHeight, sigWidth, signature, references );
		}

		public Task<bool> RbhDestructionTrailer( string trailer, string pop, int sigHeight, int sigWidth, string signature, List<string> references )
		{
			return RbhDestructionTrailer( AuthToken, trailer, pop, sigHeight, sigWidth, signature, references );
		}

		public Task<List<string>> RbhUnloadTrailerFindBins( string proBill )
		{
			return RbhUnloadTrailerFindBins( AuthToken, proBill );
		}

		public Task RbhUnloadTrailer( List<string> references )
		{
			return RbhUnloadTrailer( AuthToken, references );
		}

		public Task<(List<ITrip> Trips, string ContainerId, CONTAINER_STATUS Status)> GetTripsInContainer( string containerCode )
		{
			return GetTripsInContainer( AuthToken, containerCode );
		}

		public Task<List<ITrip>> GetTripsAtLocation( string locationBarcode )
		{
			return GetTripsAtLocation( AuthToken, locationBarcode );
		}

		public Task<Dictionary<string, string>> GetPreferences()
		{
			return GetPreferences( AuthToken );
		}

		public Task<(ITrip Trip, bool Ok)> GetTrip( string tripId, bool ignoreAddress = false )
		{
			return GetTrip( AuthToken, tripId, ignoreAddress );
		}

		public Task<(List<ITrip> Trips, bool Ok)> GetTrips( bool ignoreAddress = false )
		{
			return GetTrips( AuthToken, ignoreAddress );
		}

		public Task<UPDATE_STATUS> UpdateTrip( ITrip trip, UPDATE_STATUS previousUpdateStatus )
		{
			return UpdateTrip( AuthToken, trip, previousUpdateStatus );
		}

		public Task<bool> UpdateTripStatus( List<ITripStatus> tripStatus )
		{
			return UpdateTripStatus( AuthToken, tripStatus );
		}

		public Task<List<IZone>> GetZones()
		{
			return GetZones( AuthToken );
		}

		public Task<bool> CreateTripFromBarcode( string barcode, decimal quantity, DateTimeOffset pickupTime, string packageType, string serviceLevel )
		{
			return CreateTripFromBarcode( AuthToken, barcode, quantity, pickupTime, packageType, serviceLevel );
		}

		public Task<bool> UpdateGps( IList<GpsPoint> points )
		{
			return UpdateGps( AuthToken, points );
		}

		public void CycleEventQueue()
		{
			if( !InCycle )
			{
				InCycle = true;

				try
				{
					var Now       = DateTime.Now;
					var QueueCopy = new List<IEvent>();

					lock( EventQueue )
						QueueCopy.AddRange( EventQueue );

					try
					{
						foreach( var Event in QueueCopy )
						{
							if( !Event.Disabled )
							{
								if( Event is ITimedEvent TimedEvent && ( TimedEvent.DueTime > Now ) )
									continue;

								Event.Disabled = true;

								new TaskFactory().StartNew( @event =>
								                            {
									                            var Evnt = (IEvent)@event;

									                            {
										                            try
										                            {
											                            Evnt.Event();

											                            if( @event is ITimedEvent Te )
												                            Te.DueTime = Te.DueTime.Add( Te.Interval );
										                            }
										                            finally
										                            {
											                            Evnt.Disabled = false;
										                            }
									                            }
								                            }, Event );
							}
						}
					}
					catch
					{
					}
				}
				finally
				{
					InCycle = false;
				}
			}
		}

		public void AddEvent( IEvent e )
		{
			lock( EventQueue )
			{
				foreach( var Event in EventQueue )
				{
					if( Event.EventID == e.EventID )
						return;
				}

				EventQueue.Add( e );
			}
		}

		public void RemoveEvent( IEvent e )
		{
			lock( EventQueue )
				EventQueue.Remove( e );
		}

		public void AddTimedEvent( string eventId, Action action, TimeSpan interval, bool dueNow = false )
		{
			AddEvent( new TimedEvent
			          {
				          Interval = interval,
				          DueTime  = DateTime.Now.Add( interval ),
				          Event    = action,
				          EventID  = eventId
			          } );

			if( dueNow )
				action();
		}

		public void AddTimedEvent( string eventId, Action action, int intervalInMilliSeconds, bool dueNow = false )
		{
			AddTimedEvent( eventId, action, new TimeSpan( 0, 0, 0, 0, intervalInMilliSeconds ), dueNow );
		}

		public void StartPing( int pingIntervalInSeconds = DEFAULT_PING_INTERVAL_IN_SECONDS )
		{
			AddTimedEvent( "Ping", async () =>
			                       {
				                       var Status = await Ping();

				                       if( Status != PING_STATUS.UNKNOWN )
				                       {
					                       OnLine = Status == PING_STATUS.ONLINE;
					                       OnPing?.Invoke( OnLine );
				                       }
			                       }, pingIntervalInSeconds * 1000, true );
		}

		public void StartPing( int pingIntervalInSeconds, Action<bool> onPing )
		{
			if( !AddedPing )
			{
				AddedPing = true;
				OnPing    = onPing;
				StartPing( pingIntervalInSeconds );
			}
		}

		public void StartPing( Action<bool> onPing )
		{
			if( !AddedPing )
			{
				OnPing = onPing;
				StartPing();
			}
		}

		protected AIService()
		{
			ServicePointManager.DefaultConnectionLimit = 64;
		}

		public abstract Task<PING_STATUS> Ping();

		public abstract Task<(CONNECTION_STATUS Status, TAuthToken AuthToken, IUser User)> Connect( string carrierId, string userName, string password );

		public abstract Task<Dictionary<string, string>> GetPreferences( TAuthToken authToken );

		public abstract Task<(List<ITrip> Trips, bool Ok)> GetTrips( TAuthToken authToken, bool ignoreAddress = false );
		public abstract Task<(ITrip Trip, bool Ok)> GetTrip( TAuthToken authToken, string tripId, bool ignoreAddress = false );

		public abstract Task<UPDATE_STATUS> UpdateTrip( TAuthToken authToken, ITrip trip, UPDATE_STATUS previousUpdateStatus );
		public abstract Task<List<IZone>> GetZones( TAuthToken authToken );
		public abstract Task<bool> UpdateTripStatus( TAuthToken authToken, List<ITripStatus> tripStatus );
		public abstract Task<bool> CreateTripFromBarcode( TAuthToken authToken, string barcode, decimal quantity, DateTimeOffset pickupTime, string packageType, string serviceLevel );
		public abstract Task<(List<ITrip> Trips, string ContainerId, CONTAINER_STATUS Status)> GetTripsInContainer( TAuthToken authToken, string containerCode );

		public abstract Task<bool> UpdateGps( TAuthToken authToken, IList<GpsPoint> points );
	}
}