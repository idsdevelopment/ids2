﻿namespace Service.Interfaces
{
	public interface IUser
	{
		string UserName { get; set; }
		string CarrierId { get; set; }

		ushort PollingInterval { get; set; }
		byte StartHour { get; set; }
		byte EndHour { get; set; }
		byte[] ActiveDays { get; set; }
		string Fax { get; set; }
	}
}