﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
	public enum CONNECTION_STATUS
	{
		BAD,
		OK,
		TIME_ERROR
	}

	public enum PING_STATUS
	{
		UNKNOWN,
		ONLINE,
		OFFLINE,
	}


	public enum UPDATE_STATUS
	{
		UNKNOWN,
		FAIL1,
		FAIL2,
		OK
	}

	public enum CONTAINER_STATUS
	{
		BAD,
		OK,
		EMPTY,
		DELETED
	}

	public interface IService<TAuthToken> : IGps<TAuthToken>
	{
		Task<PING_STATUS> Ping();

		Task<(CONNECTION_STATUS Status, TAuthToken AuthToken, IUser User)> Connect( string carrierId, string userName, string password );
		Task<Dictionary<string, string>> GetPreferences( TAuthToken authToken );

		Task<(List<ITrip> Trips, bool Ok)> GetTrips( TAuthToken authToken, bool ignoreAddress = false );
		Task<(ITrip Trip, bool Ok)> GetTrip( TAuthToken authToken, string tripId, bool ignoreAddress = false );
		Task<UPDATE_STATUS> UpdateTrip( TAuthToken authToken, ITrip trip, UPDATE_STATUS previousUpdateStatus );
		Task<List<IZone>> GetZones( TAuthToken authToken );
		Task<bool> UpdateTripStatus( TAuthToken authToken, List<ITripStatus> tripStatus );
		Task<bool> CreateTripFromBarcode( TAuthToken authToken, string barcode, decimal quantity, DateTimeOffset pickupTime, string packageType, string serviceLevel );
		Task<(List<ITrip> Trips, string ContainerId, CONTAINER_STATUS Status)> GetTripsInContainer( TAuthToken authToken, string containerCode );
	}
}