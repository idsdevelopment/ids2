﻿using System;

namespace Service.Interfaces
{
	public static class TripExtensions
	{
		public static bool EqualTo( this ITrip t1, ITrip t2 )
		{
			return ( t1 != null ) && ( t2 != null )
			                      && ( t1.Status == t2.Status )
			                      && ( t1.TripId == t2.TripId )
			                      && ( t1.PickupTime == t2.PickupTime )
			                      && ( t1.DeliveryTime == t2.DeliveryTime )
			                      && ( t1.Weight == t2.Weight )
			                      && ( t1.Pieces == t2.Pieces )
			                      && ( t1.ReceivedByDevice == t2.ReceivedByDevice )
			                      && ( t1.ReadByDriver == t2.ReadByDriver )
			                      && ( t1.CurrentZone == t2.CurrentZone )
			                      && ( t1.BillingNote == t2.BillingNote )
			                      && ( t1.CallerEmailAddress == t2.CallerEmailAddress )
			                      && ( t1.PickupCompanyName == t2.PickupCompanyName )
			                      && ( t1.PickupSuite == t2.PickupSuite )
			                      && ( t1.PickupAddressLine1 == t2.PickupAddressLine1 )
			                      && ( t1.PickupAddressLine2 == t2.PickupAddressLine2 )
			                      && ( t1.PickupVicinity == t2.PickupVicinity )
			                      && ( t1.PickupCity == t2.PickupCity )
			                      && ( t1.PickupRegion == t2.PickupRegion )
			                      && ( t1.PickupPostalCode == t2.PickupPostalCode )
			                      && ( t1.PickupCountry == t2.PickupCountry )
			                      && ( t1.PickupCountryCode == t2.PickupCountryCode )
			                      && ( t1.PickupPhone == t2.PickupPhone )
			                      && ( t1.PickupFax == t2.PickupFax )
			                      && ( t1.PickupMobile == t2.PickupMobile )
			                      && ( t1.PickupEmailAddress == t2.PickupEmailAddress )
			                      && ( t1.PickupContact == t2.PickupContact )
			                      && ( t1.PickupLatitude == t2.PickupLatitude )
			                      && ( t1.PickupLongitude == t2.PickupLongitude )
			                      && ( t1.PickupBarcode == t2.PickupBarcode )
			                      && ( t1.DeliveryCompanyName == t2.DeliveryCompanyName )
			                      && ( t1.DeliverySuite == t2.DeliverySuite )
			                      && ( t1.DeliveryAddressLine1 == t2.DeliveryAddressLine1 )
			                      && ( t1.DeliveryAddressLine2 == t2.DeliveryAddressLine2 )
			                      && ( t1.DeliveryVicinity == t2.DeliveryVicinity )
			                      && ( t1.DeliveryRegion == t2.DeliveryRegion )
			                      && ( t1.DeliveryCity == t2.DeliveryCity )
			                      && ( t1.DeliveryPostalCode == t2.DeliveryPostalCode )
			                      && ( t1.DeliveryCountry == t2.DeliveryCountry )
			                      && ( t1.DeliveryCountryCode == t2.DeliveryCountryCode )
			                      && ( t1.DeliveryPhone == t2.DeliveryPhone )
			                      && ( t1.DeliveryFax == t2.DeliveryFax )
			                      && ( t1.DeliveryMobile == t2.DeliveryMobile )
			                      && ( t1.DeliveryEmailAddress == t2.DeliveryEmailAddress )
			                      && ( t1.DeliveryContact == t2.DeliveryContact )
			                      && ( t1.DeliveryLatitude == t2.DeliveryLatitude )
			                      && ( t1.DeliveryLongitude == t2.DeliveryLongitude )
			                      && ( t1.DeliveryBarcode == t2.DeliveryBarcode )
			                      && ( t1.DueTime == t2.DueTime )
			                      && ( t1.ReadyTime == t2.ReadyTime )
			                      && ( t1.PickupZone == t2.PickupZone )
			                      && ( t1.DeliveryZone == t2.DeliveryZone )
			                      && ( t1.PickupNote == t2.PickupNote )
			                      && ( t1.DeliveryNote == t2.DeliveryNote )
			                      && ( t1.ShowChargeAmount == t2.ShowChargeAmount )
			                      && ( t1.ChargeAmount == t2.ChargeAmount )
			                      && ( t1.Pallets == t2.Pallets )
			                      && ( t1.UndeliverableReason == t2.UndeliverableReason )
			                      && ( t1.AccountId == t2.AccountId )
			                      && ( t1.AccountNumber == t2.AccountNumber )
			                      && ( t1.BillingAccount == t2.BillingAccount )
			                      && ( t1.ServiceLevel == t2.ServiceLevel )
			                      && ( t1.PackageType == t2.PackageType )
			                      && ( t1.Reference == t2.Reference )
			                      && ( t1.PickupArrivalTime == t2.PickupArrivalTime )
			                      && ( t1.DeliveryArrivalTime == t2.DeliveryArrivalTime );
		}

		public static ITrip Copy( this ITrip t, ITrip t1 )
		{
			t.TripId = t1.TripId;
			t.Status = t1.Status;
			t.Driver = t1.Driver;

			t.ReceivedByDevice    = t1.ReceivedByDevice;
			t.ReadByDriver        = t1.ReadByDriver;
			t.PickupTime          = t1.PickupTime;
			t.DeliveryTime        = t1.DeliveryTime;
			t.PickupArrivalTime   = t1.PickupArrivalTime;
			t.DeliveryArrivalTime = t1.DeliveryArrivalTime;
			t.CallerEmailAddress  = t1.CallerEmailAddress;

			t.Weight             = t1.Weight;
			t.Pieces             = t1.Pieces;
			t.OriginalPieceCount = t1.OriginalPieceCount;

			t.OriginalDataAsJSON = t1.OriginalDataAsJSON;
			t.CurrentZone        = t1.CurrentZone;
			t.BillingNote        = t1.BillingNote;

			t.PickupCompanyName  = t1.PickupCompanyName;
			t.PickupSuite        = t1.PickupSuite;
			t.PickupAddressLine1 = t1.PickupAddressLine1;
			t.PickupAddressLine2 = t1.PickupAddressLine2;
			t.PickupVicinity     = t1.PickupVicinity;
			t.PickupCity         = t1.PickupCity;
			t.PickupRegion       = t1.PickupRegion;
			t.PickupPostalCode   = t1.PickupPostalCode;
			t.PickupCountry      = t1.PickupCountry;
			t.PickupCountryCode  = t.PickupCountryCode;
			t.PickupPhone        = t1.PickupPhone;
			t.PickupFax          = t1.PickupFax;
			t.PickupMobile       = t1.PickupMobile;
			t.PickupEmailAddress = t1.PickupEmailAddress;
			t.PickupContact      = t1.PickupContact;
			t.PickupLatitude     = t1.PickupLatitude;
			t.PickupLongitude    = t1.PickupLongitude;
			t.PickupBarcode      = t1.PickupBarcode;
			t.PickupNote         = t1.PickupNote;

			t.DeliveryCompanyName  = t1.DeliveryCompanyName;
			t.DeliverySuite        = t1.DeliverySuite;
			t.DeliveryAddressLine1 = t1.DeliveryAddressLine1;
			t.DeliveryAddressLine2 = t1.DeliveryAddressLine2;
			t.DeliveryVicinity     = t1.DeliveryVicinity;
			t.DeliveryRegion       = t1.DeliveryRegion;
			t.DeliveryCity         = t1.DeliveryCity;
			t.DeliveryPostalCode   = t1.DeliveryPostalCode;
			t.DeliveryCountry      = t1.DeliveryCountry;
			t.DeliveryCountryCode  = t.DeliveryCountryCode;
			t.DeliveryPhone        = t1.DeliveryPhone;
			t.DeliveryFax          = t1.DeliveryFax;
			t.DeliveryMobile       = t1.DeliveryMobile;
			t.DeliveryEmailAddress = t1.DeliveryEmailAddress;
			t.DeliveryContact      = t1.DeliveryContact;
			t.DeliveryLatitude     = t1.DeliveryLatitude;
			t.DeliveryLongitude    = t1.DeliveryLongitude;
			t.DeliveryBarcode      = t1.DeliveryBarcode;
			t.DeliveryNote         = t1.DeliveryNote;

			t.DueTime     = t1.DueTime;
			t.ReadyTime   = t1.ReadyTime;
			t.UpdatedTime = t1.UpdatedTime;

			t.PickupZone   = t1.PickupZone;
			t.DeliveryZone = t1.DeliveryZone;

			t.ShowChargeAmount = t1.ShowChargeAmount;
			t.ChargeAmount     = t1.ChargeAmount;
			t.Pallets          = t1.Pallets;

			t.UndeliverableMode   = t1.UndeliverableMode;
			t.UndeliverableReason = t1.UndeliverableReason;

			t.AccountId      = t1.AccountId;
			t.AccountNumber  = t1.AccountNumber;
			t.BillingAccount = t1.BillingAccount;

			t.ServiceLevel = t1.ServiceLevel;
			t.PackageType  = t1.PackageType;

			t.Reference = t1.Reference;
			t.PodName   = t1.PodName;
			t.Signature = t1.Signature;

			t.OriginalDeliveryAddressId = t1.OriginalDeliveryAddressId;
			t.OriginalPickupAddressId   = t1.OriginalPickupAddressId;

			return t;
		}
	}

	public enum STATUS : sbyte
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		DELETED,
		SCHEDULED,
		UNDELIVERED = -3,
		CREATE_NEW_TRIP = -4,
		CREATE_NEW_TRIP_AS_PICKED_UP = -5,
		CREATE_NEW_TRIP_AS_DISPATCHED = -6
	}

	public enum UNDELIVERABLE_MODE
	{
		NONE,
		STATUS_CHANGE,
		NEW_TRIP // Residual to new trip
	}

	public interface ITrip
	{
		object OriginalObject { get; set; }

		string Driver { get; set; }

		// ReSharper disable once InconsistentNaming
		string OriginalDataAsJSON { get; set; }
		string OriginalPickupAddressId { get; set; }
		string OriginalDeliveryAddressId { get; set; }

		STATUS Status { get; set; }
		string ServiceLevel { get; set; }
		string PackageType { get; set; }
		string Reference { get; set; }
		bool ReceivedByDevice { get; set; }
		bool ReadByDriver { get; set; }

		string TripId { get; set; }
		string AccountId { get; set; }
		string AccountNumber { get; set; }
		string BillingAccount { get; set; }
		string CallerEmailAddress { get; set; }

		string PickupCompanyName { get; set; }
		string PickupSuite { get; set; }
		string PickupAddressLine1 { get; set; }
		string PickupAddressLine2 { get; set; }
		string PickupVicinity { get; set; }
		string PickupCity { get; set; }
		string PickupRegion { get; set; }
		string PickupPostalCode { get; set; }
		string PickupCountry { get; set; }
		string PickupCountryCode { get; set; }
		string PickupPhone { get; set; }
		string PickupFax { get; set; }
		string PickupMobile { get; set; }
		string PickupEmailAddress { get; set; }
		string PickupContact { get; set; }
		decimal PickupLatitude { get; set; }
		decimal PickupLongitude { get; set; }
		string PickupBarcode { get; set; }

		string DeliveryCompanyName { get; set; }
		string DeliverySuite { get; set; }
		string DeliveryAddressLine1 { get; set; }
		string DeliveryAddressLine2 { get; set; }
		string DeliveryVicinity { get; set; }
		string DeliveryCity { get; set; }
		string DeliveryRegion { get; set; }
		string DeliveryPostalCode { get; set; }
		string DeliveryCountry { get; set; }
		string DeliveryCountryCode { get; set; }
		string DeliveryPhone { get; set; }
		string DeliveryFax { get; set; }
		string DeliveryMobile { get; set; }
		string DeliveryEmailAddress { get; set; }
		string DeliveryContact { get; set; }
		decimal DeliveryLatitude { get; set; }
		decimal DeliveryLongitude { get; set; }
		string DeliveryBarcode { get; set; }

		DateTimeOffset PickupTime { get; set; }
		DateTimeOffset DeliveryTime { get; set; }
		DateTimeOffset DueTime { get; set; }
		DateTimeOffset ReadyTime { get; set; }
		DateTimeOffset UpdatedTime { get; set; }
		DateTimeOffset PickupArrivalTime { get; set; }
		DateTimeOffset DeliveryArrivalTime { get; set; }

		decimal Weight { get; set; }
		decimal Pieces { get; set; }
		decimal OriginalPieceCount { get; set; }

		string CurrentZone { get; set; }
		string PickupZone { get; set; }
		string DeliveryZone { get; set; }
		string PodName { get; set; }
		string Signature { get; set; }

		string BillingNote { get; set; }
		string PickupNote { get; set; }
		string DeliveryNote { get; set; }

		bool ShowChargeAmount { get; set; }
		decimal ChargeAmount { get; set; }

		string Pallets { get; set; }

		UNDELIVERABLE_MODE UndeliverableMode { get; set; }
		string UndeliverableReason { get; set; }
	}

	public static class Xlate
	{
		public static string Status( STATUS status )
		{
			switch( status )
			{
			case STATUS.POSTED:
				return "POSTED";

			case STATUS.DELETED:
				return "DELETED";

			case STATUS.DELIVERED:
				return "DELIVERED";

			case STATUS.DISPATCHED:
				return "DISPATCHED";

			case STATUS.NEW:
				return "NEW";

			case STATUS.ACTIVE:
				return "ACTIVE";

			case STATUS.PICKED_UP:
				return "PICKED_UP";

			case STATUS.SCHEDULED:
				return "SCHEDULED";

			case STATUS.UNDELIVERED:
				return "UNDELIVERED";

			case STATUS.UNSET:
				return "UNSET";

			case STATUS.VERIFIED:
				return "VERIFIED";

			default:
				return "????";
			}
		}
	}
}