﻿namespace Service.Interfaces
{
	public interface ICharge
	{
		string TripId { get; set; }

		string Name { get; set; }

		string Label { get; set; }

		decimal Value { get; set; }
	}
}