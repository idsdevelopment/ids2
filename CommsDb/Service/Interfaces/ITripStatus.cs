﻿using System;

namespace Service.Interfaces
{
	public interface ITripStatus
	{
		string TripId { get; set; }
		STATUS Status { get; set; }
		DateTimeOffset UpdateTime { get; set; }
	}
}