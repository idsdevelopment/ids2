﻿using System;
using System.Collections.Generic;

namespace Service.Interfaces
{
	public interface IClaim
	{
		string DriverAuthToken { get; set; }
		string TripId { get; set; }
		string Pod { get; set; }
		string Signature { get; set; }
		int SignatureHeight { get; set; }
		int SignatureWidth { get; set; }
		DateTime PickupTime { get; set; }
	}

    public interface IClaims
	{
		string DriverAuthToken { get; set; }
		IList<string> TripIds { get; set; }
		string Pod { get; set; }
		string Signature { get; set; }
		int SignatureHeight { get; set; }
		int SignatureWidth { get; set; }
		DateTimeOffset PickupTime { get; set; }
	}
}