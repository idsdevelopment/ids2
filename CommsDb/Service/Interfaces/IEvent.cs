﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
	public interface IEvent
	{
		Action Event { get; set; }
		bool Disabled { get; set; }
		string EventID { get; set; }
	}

	public interface ITimedEvent : IEvent
	{
		DateTime DueTime { get; set; }
		TimeSpan Interval { get; }
	}
}