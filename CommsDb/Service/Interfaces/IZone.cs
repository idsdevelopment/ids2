﻿namespace Service.Interfaces
{
	public interface IZone
	{
		string ZoneName { get; set; }
	}
}