﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Interfaces;

namespace Service.Abstracts
{
	public abstract partial class AIService<TClient, TAuthToken> : IService<TAuthToken> where TClient : new()
	{
		public abstract Task RbhUpdateReturnsWithBin( TAuthToken authToken, List<string> references, string bin );
		public abstract Task RbhUpdateEditCreateWithBin( TAuthToken authToken, List<string> references, string bin );

		public Task RbhUpdateReturnsWithBin( List<string> references, string bin )
		{
			return RbhUpdateReturnsWithBin( AuthToken, references, bin );
		}

		public Task RbhUpdateEditCreateWithBin( List<string> references, string bin )
		{
			return RbhUpdateEditCreateWithBin( AuthToken, references, bin );
		}
	}
}