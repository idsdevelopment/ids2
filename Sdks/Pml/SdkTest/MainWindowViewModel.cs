﻿using IdsControlLibraryV2.ViewModel;
using PmlSdk;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Utils;
using Utils.Csv;

namespace SdkTest
{
    public class MainWindowViewModel : ViewModelBase
	{
		public const string AUTH = "PmlTest-admin-admin";

	#region Routes

		public ICommand UpdateRoutes => Commands[ nameof( UpdateRoutes ) ];

		public void Execute_UpdateRoutes()
		{
			Task.Run( () =>
			          {
				          var Csv = new Csv();

				          Csv.AppendRow();
				          var Row = Csv[ 0 ];
				          Row[ 0 ] = "3000";
				          Row[ 1 ] = "VWDermit";
				          Row[ 2 ] = "Metro";
				          Row[ 3 ] = "AU21250";

				          Csv.AppendRow();
				          Row = Csv[ 1 ];
				          Row[ 0 ] = "3585";
				          Row[ 1 ] = "VWDermit";
				          Row[ 2 ] = "Regional";
				          Row[ 3 ] = "AU26450";

				          Csv.AppendRow();
				          Row = Csv[ 2 ];
				          Row[ 0 ] = "2640";
				          Row[ 1 ] = "VWDermit";
				          Row[ 2 ] = "Regional";
				          Row[ 3 ] = "AU26530";

				          Csv.AppendRow();
				          Row = Csv[ 3 ];
				          Row[ 0 ] = "7310";
				          Row[ 1 ] = "TAS";
				          Row[ 2 ] = "TAS";
				          Row[ 3 ] = "AU95220";

				          Csv.AppendRow();
				          Row = Csv[ 4 ];
				          Row[ 0 ] = "3030";
				          Row[ 1 ] = "VWDermit";
				          Row[ 2 ] = "Metro";
				          Row[ 3 ] = "AU21260";

//				          Ids.importPostalCodesToRoutes( AUTH, Csv.ToString() );
			          } );
		}

	#endregion

	#region Create Trip

		public ICommand Create => Commands[ nameof( Create ) ];

		public void Execute_Create()
		{
			const string EMAIL = "terry@idsapp.com";

			Task.Run( () =>
			          {
				          string Aurt()
				          {
					          var Rand = new Random();

					          return $"AURT{Rand.Next( 1000000 ):D6}";
				          }

				          var Now = DateTime.Now.AsPacificStandardTime().DateTime;

				          Ids.updateTripDetailedQuick( new remoteTripDetailed
				                                       {
					                                       status = STATUS.ACTIVE.ToInt(),

					                                       accountId = "pmloz",

					                                       deliveryCompany = "Melbourne Warehouse (MelWare)",
					                                       deliveryStreet = "1 Innovation Court",
					                                       deliveryCity = "Derrimut",
					                                       deliveryProvince = "Victoria",
					                                       deliveryCountry = "Australia",
					                                       deliveryPc = "3030",

					                                       pickupCompany = "7 ELEVEN 267 BROWNS (18748)",
					                                       pickupStreet = "267 BROWNS PLAINS RD",
					                                       pickupCity = "BROWNS PLAINS",
					                                       pickupProvince = "Queensland",
					                                       pickupCountry = "Australia",
					                                       pickupPc = "4118",
					                                       pickupEmail = EMAIL,

					                                       billingNotes = Aurt(),

					                                       pieces = 2,
					                                       weight = 1,
					                                       clientReference = "9310704639309",
					                                       packageType = "Longbeach Original Yellow 20's + Firm Filter (PK)",
					                                       serviceLevel = "return",

					                                       callTime = Now,
					                                       deadlineTime = Now,
					                                       readyTime = Now
				                                       }, AUTH, true );
/*
				          Ids.updateTripDetailedQuick( new remoteTripDetailed
				                                       {
					                                       status = STATUS.ACTIVE.ToInt(),

					                                       accountId = "pmloz",

					                                       deliveryCompany = "Melbourne Warehouse (MelWare)",
					                                       deliveryStreet = "1 Innovation Court",
					                                       deliveryCity = "Derrimut",
					                                       deliveryProvince = "Victoria",
					                                       deliveryCountry = "Australia",
					                                       deliveryPc = "3030",

					                                       pickupCompany = "7 ELEVEN 267 BROWNS (18748)",
					                                       pickupStreet = "267 BROWNS PLAINS RD",
					                                       pickupCity = "BROWNS PLAINS",
					                                       pickupProvince = "Queensland",
					                                       pickupCountry = "Australia",
					                                       pickupPc = "4118",
					                                       pickupEmail = EMAIL,

					                                       billingNotes = Aurt(),

					                                       pieces = 10,
					                                       weight = 1,
					                                       clientReference = "",
					                                       packageType = "",
					                                       serviceLevel = "Uptotal",

					                                       callTime = Now,
					                                       deadlineTime = Now,
					                                       readyTime = Now
				                                       }, AUTH, true );

				          Ids.updateTripDetailedQuick( new remoteTripDetailed
				                                       {
					                                       status = STATUS.ACTIVE.ToInt(),

					                                       accountId = "pmloz",

					                                       deliveryCompany = "Melbourne Warehouse (MelWare)",
					                                       deliveryStreet = "1 Innovation Court",
					                                       deliveryCity = "Derrimut",
					                                       deliveryProvince = "Victoria",
					                                       deliveryCountry = "Australia",
					                                       deliveryPc = "3030",

					                                       pickupCompany = "COLES NARRABRI # 876 (36830)",
					                                       pickupStreet = "154 MAITLAND ST",
					                                       pickupCity = "NARRABRI",
					                                       pickupProvince = "New South Wales",
					                                       pickupCountry = "Australia",
					                                       pickupPc = "2390",
					                                       pickupEmail = EMAIL,

					                                       billingNotes = Aurt(),

					                                       pieces = 20,
					                                       weight = 1,
					                                       clientReference = "9310704639507",
					                                       packageType = "Longbeach Fine Silver 20's + Firm Filter (PK)",
					                                       serviceLevel = "Uppack",

					                                       callTime = Now,
					                                       deadlineTime = Now,
					                                       readyTime = Now
				                                       }, AUTH, true );

				          Ids.updateTripDetailedQuick( new remoteTripDetailed
				                                       {
					                                       status = STATUS.ACTIVE.ToInt(),

					                                       accountId = "pmloz",

					                                       deliveryCompany = "Melbourne Warehouse (MelWare)",
					                                       deliveryStreet = "1 Innovation Court",
					                                       deliveryCity = "Derrimut",
					                                       deliveryProvince = "Victoria",
					                                       deliveryCountry = "Australia",
					                                       deliveryPc = "3030",

					                                       pickupCompany = "COLES NARRABRI # 876 (36830)",
					                                       pickupStreet = "154 MAITLAND ST",
					                                       pickupCity = "NARRABRI",
					                                       pickupProvince = "New South Wales",
					                                       pickupCountry = "Australia",
					                                       pickupPc = "2390",
					                                       pickupEmail = EMAIL,

					                                       billingNotes = Aurt(),

					                                       pieces = 10,
					                                       weight = 1,
					                                       clientReference = "",
					                                       packageType = "",
					                                       serviceLevel = "Uptotal",

					                                       callTime = Now,
					                                       deadlineTime = Now,
					                                       readyTime = Now
				                                       }, AUTH, true );
*/

			          } );

		}

		public ICommand GetTrips => Commands[ nameof( GetTrips ) ];

		public void Execute_GetTrips()
		{
			Task.Run( () =>
			          {
				          var Temp = Ids.pmlFindTripsDetailedUpdatedSince( AUTH, "", "ALL", (int)STATUS.ACTIVE, (int)STATUS.DELIVERED, "Return", 5 * 60 * 60 );
			          } );
		}

	#endregion
	}
}