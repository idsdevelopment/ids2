﻿using System;

namespace PmlSdk
{
	public class TimeException : Exception
	{
		public TimeException() : base( "The machine time is out by more than + or - 10 seconds" )
		{
		}
	}

	public class LoginException : Exception
	{
		public LoginException() : base( "Invalid user name or password" )
		{
		}
	}
}