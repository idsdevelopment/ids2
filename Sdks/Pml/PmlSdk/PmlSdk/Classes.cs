﻿using System;
using System.Collections.Generic;
using Protocol.Data;
using Utils;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace PmlSdk
{
	public enum STATUS
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		DELETED,
		SCHEDULED,
		UNDELIVERED = -3
	}

	public class remoteTripDetailed
	{
		private const string RETURN = "Return";

		public string tripId { get; set; } = "";

		public string internalId
		{
			get => tripId;
			set => tripId = value;
		}

		public string resellerId { get; set; } = "";

		public long invoiceId { get; set; }

		public bool invoiced { get; set; }

		public string serviceLevel { get; set; } = "";

		public string packageType { get; set; } = "";

		public string callTakerId { get; set; } = "";

		public int status { get; set; }

		public bool modified { get; set; } // Artifact

		public string currentZone { get; set; } = "";

		public string accountId { get; set; } = "";

		public string caller { get; set; } = "";

		public string callerPhone { get; set; } = "";

		public string callerEmail { get; set; } = "";

		public int pieces { get; set; }

		public float weight { get; set; }

		public float declaredValueAmount { get; set; }

		public DateTime callTime { get; set; } = DateTime.MinValue;

		public bool callTimeSpecified { get; set; }

		public DateTime readyTime { get; set; } = DateTime.MinValue;

		public bool readyTimeSpecified { get; set; }

		public DateTime deadlineTime { get; set; } = DateTime.MinValue;

		public bool deadlineTimeSpecified { get; set; }

		public string driver { get; set; } = "";

		public string billingNotes { get; set; } = "";

		public bool returnTrip { get; set; }

		public bool dangerousGoods { get; set; }

		public DateTime pickupTime { get; set; } = DateTime.MinValue;

		public bool pickupTimeSpecified { get; set; }

		public string pickupCompany { get; set; } = "";

		public string pickupNotes { get; set; } = "";

		public string pickupContact { get; set; } = "";

		public string pickupPhone { get; set; } = "";

		public string pickupZone { get; set; } = "";

		public string pickupEmail { get; set; } = "";

		public string pickupAddressId { get; set; } = "";

		public string pickupResellerId { get; set; } = "";

		public string pickupAccountId { get; set; } = "";

		public string pickupDescription { get; set; } = "";

		public string pickupSuite { get; set; } = "";

		public string pickupStreet { get; set; } = "";

		public string pickupCity { get; set; } = "";

		public string pickupProvince { get; set; } = "";

		public string pickupPc { get; set; } = "";

		public string pickupCountry { get; set; } = "";

		public string deliveryCompany { get; set; } = "";

		public string deliveryNotes { get; set; } = "";

		public string deliveryContact { get; set; } = "";

		public string deliveryPhone { get; set; } = "";

		public string deliveryZone { get; set; } = "";

		public string deliveryEmail { get; set; } = "";

		public string deliveryAddressId { get; set; } = "";

		public string deliveryResellerId { get; set; } = "";

		public string deliveryAccountId { get; set; } = "";

		public string deliveryDescription { get; set; } = "";

		public string deliverySuite { get; set; } = "";

		public string deliveryStreet { get; set; } = "";

		public string deliveryCity { get; set; } = "";

		public string deliveryProvince { get; set; } = "";

		public string deliveryPc { get; set; } = "";

		public string deliveryCountry { get; set; } = "";

		public float deliveryAmount { get; set; }

		public float miscAmount { get; set; }

		public float insuranceAmount { get; set; }

		public float weightAmount { get; set; }

		public DateTime deliveredTime { get; set; } = DateTime.MinValue;

		public bool deliveredTimeSpecified { get; set; }

		public int waitTimeMins { get; set; }

		public float waitTimeAmount { get; set; }

		public float carChargeAmount { get; set; }

		public float redirectAmount { get; set; }

		public bool skipUpdateCommonAddress { get; set; }

		public float noGoodsAmount { get; set; }

		public float disbursementAmount { get; set; }

		public float totalAmount { get; set; }

		public float totalTaxAmount { get; set; }

		public float discountAmount { get; set; }

		public float totalFixedAmount { get; set; }

		public bool POD { get; set; }

		public bool mailDrop { get; set; }

		public string readyTimeString { get; set; } = "";

		public string clientReference { get; set; } = "";

		public bool isDriverPaid { get; set; }

		public bool isCashTrip { get; set; }

		public bool isCashTripReconciled { get; set; }

		public string podName { get; set; } = "";

		public DateTime driverAssignTime { get; set; } = DateTime.MinValue;

		public bool driverAssignTimeSpecified { get; set; }

		public DateTime pickupArriveTime { get; set; } = DateTime.MinValue;

		public bool pickupArriveTimeSpecified { get; set; }

		public DateTime deliveryArriveTime { get; set; } = DateTime.MinValue;

		public bool deliveryArriveTimeSpecified { get; set; }

		public long lastUpdated { get; set; }

		public float totalPayrollAmount { get; set; }

		public string pallets { get; set; } = "";

		public float length { get; set; }

		public float width { get; set; }

		public float height { get; set; }

		public float weightOrig { get; set; }

		public string sigFilename { get; set; } = "";

		public int board { get; set; }

		public bool disputed { get; set; }

		public bool carCharge { get; set; }

		public bool redirect { get; set; }

		public string sortOrder { get; set; } = "";

		public bool dontFinalizeFlag { get; set; }

		public bool dontFinalizeFlagSpecified { get; set; }

		public DateTime dontFinalizeDate { get; set; } = DateTime.MinValue;

		public bool dontFinalizeDateSpecified { get; set; }

		public bool readyToInvoiceFlag { get; set; }

		public bool readyToInvoiceFlagSpecified { get; set; }

		public string stopGroupNumber { get; set; } = "";

		public DateTime driverPaidCommissionDate { get; set; } = DateTime.MinValue;

		public bool driverPaidCommissionDateSpecified { get; set; }

		public bool modifiedTripFlag { get; set; }

		public bool modifiedTripFlagSpecified { get; set; }

		public DateTime modifiedTripDate { get; set; } = DateTime.MinValue;

		public bool modifiedTripDateSpecified { get; set; }

		public DateTime receivedByIdsRouteDate { get; set; } = DateTime.MinValue;

		public bool receivedByIdsRouteDateSpecified { get; set; }

		public DateTime processedByIdsRouteDate { get; set; } = DateTime.MinValue;

		public bool processedByIdsRouteDateSpecified { get; set; }

		public int priorityStatus { get; set; }

		public bool priorityStatusSpecified { get; set; }

		public long priorityInvoiceId { get; set; }

		public bool priorityInvoiceIdSpecified { get; set; }

		internal TripUpdate AsTrip
		{
			get
			{
				var (DAdr1, DAdr2) = SplitAddress( deliveryStreet );
				var (PAdr1, PAdr2) = SplitAddress( pickupStreet );
				var (Status1, Status2) = status.ToStatus().ToStatus();
				var (PuCo, PuLoc) = SplitCompany( pickupCompany );
				var (DelCo, DelLoc) = SplitCompany( deliveryCompany );
				var SLevel = ( serviceLevel ?? "" ).Trim();

				var MasterServiceLevel = SLevel.StartsWith( "Up", StringComparison.OrdinalIgnoreCase ) ? "Up" : "Return";

				var Temp = new TripUpdate
				           {
					           Program = Ids.SDK,
					           TripId = tripId ?? "",
					           AccountId = accountId ?? "",

					           CallTime = callTime.FromPacificStandardTime(),
					           ReadyTime = readyTime.FromPacificStandardTime(),
					           DueTime = deadlineTime.FromPacificStandardTime(),

					           CallerEmail = callerEmail ?? "",
					           CallerName = caller ?? "",
					           CallerPhone = callerPhone ?? "",
					           CurrentZone = ( currentZone.IsNotNullOrEmpty() ? currentZone : pickupZone ) ?? "",
					           DangerousGoods = dangerousGoods,

					           BillingNotes = billingNotes ?? "",

					           DeliveryCompanyName = DelCo,
					           DeliveryAddressBarcode = DelLoc,
					           DeliveryAccountId = deliveryAccountId ?? "",
					           DeliveryAddressAddressLine1 = DAdr1,
					           DeliveryAddressAddressLine2 = DAdr2,
					           DeliveryAddressCity = deliveryCity ?? "",
					           DeliveryAddressCountry = deliveryCountry ?? "",
					           DeliveryAddressEmailAddress = deliveryEmail,
					           DeliveryAddressPhone = deliveryPhone ?? "",
					           DeliveryAddressNotes = deliveryNotes ?? "",
					           DeliveryAddressPostalCode = deliveryPc ?? "",
					           DeliveryAddressRegion = deliveryProvince ?? "",
					           DeliveryAddressSuite = deliverySuite ?? "",
					           DeliveryNotes = deliveryDescription ?? "",
					           DeliveryContact = deliveryContact ?? "",
					           DeliveryTime = deliveredTime.FromPacificStandardTime(),
					           DeliveryZone = deliveryZone ?? "",

					           PickupCompanyName = PuCo,
					           PickupAddressBarcode = PuLoc,
					           PickupAccountId = pickupAccountId ?? "",
					           PickupAddressAddressLine1 = PAdr1,
					           PickupAddressAddressLine2 = PAdr2,
					           PickupAddressCity = pickupCity ?? "",
					           PickupAddressCountry = pickupCountry ?? "",
					           PickupAddressEmailAddress = pickupEmail,
					           PickupAddressPhone = pickupPhone ?? "",
					           PickupAddressNotes = pickupNotes ?? "",
					           PickupAddressPostalCode = pickupPc ?? "",
					           PickupAddressRegion = pickupProvince ?? "",
					           PickupAddressSuite = pickupSuite ?? "",
					           PickupNotes = pickupDescription ?? "",
					           PickupContact = pickupContact ?? "",
					           PickupTime = deliveredTime.FromPacificStandardTime(),
					           PickupZone = pickupZone ?? "",

					           Reference = clientReference ?? "",
					           Status1 = Status1,
					           Status2 = Status2,
					           ServiceLevel = MasterServiceLevel,
					           PackageType = RETURN,
					           Pieces = pieces,
					           Weight = (decimal)weight,

					           Packages = new List<TripPackage>
					                      {
						                      new TripPackage
						                      {
							                      PackageType = RETURN,
							                      Pieces = pieces,
							                      Weight = (decimal)weight,
							                      Items = new List<TripItem>
							                              {
								                              new TripItem
								                              {
									                              Barcode = clientReference,
									                              ItemCode = SLevel,
									                              Description = packageType,
									                              Pieces = pieces,
									                              Weight = (decimal)weight
								                              }
							                              }
						                      }
					                      }
				           };

				return Temp;
			}
		}

		private static (string Adr1, string Adr2) SplitAddress( string adr )
		{
			if( !( adr is null ) )
			{
				var Lines = adr.Replace( '\r', '\n' ).Replace( "\n\n", "\n" ).Split( new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );

				switch( Lines.Length )
				{
				case 0:
					break;

				case 1:
					return ( Lines[ 0 ], "" );

				default:
					return ( Lines[ 0 ], Lines[ 1 ] );
				}
			}

			return ( "", "" );
		}

		private static (string CoName, string Location) SplitCompany( string coName )
		{
			var Result = ( CoName: coName, Location: "" );

			var P = coName.LastIndexOf( '(' );

			if( P >= 0 )
			{
				var p2 = coName.LastIndexOf( ')' );

				if( ( p2 >= 0 ) && ( p2 > P ) )
				{
					Result.Location = coName.SubStr( P + 1, p2 - P - 1 );
					Result.CoName = coName.SubStr( 0, P );
				}
			}

			return Result;
		}

		public remoteTripDetailed()
		{
		}

		public remoteTripDetailed( TripUpdate t )
		{
			tripId = t.TripId ?? "";
			serviceLevel = t.ServiceLevel ?? "";
			packageType = t.PackageType ?? "";
			status = t.ToStatus();
			currentZone = t.CurrentZone ?? "";
			accountId = t.AccountId ?? "";
			pieces = (int)t.Pieces;
			weight = (float)t.Weight;

			callTime = t.CallTime.AsPacificStandardTime().DateTime;
			readyTime = t.ReadyTime.AsPacificStandardTime().DateTime;
			deadlineTime = t.DueTime.AsPacificStandardTime().DateTime;
			pickupTime = t.PickupTime.AsPacificStandardTime().DateTime;
			pickupArriveTime = t.PickupTime.AsPacificStandardTime().DateTime;
			deliveredTime = t.DeliveryTime.AsPacificStandardTime().DateTime;

			driver = t.Driver ?? "";
			billingNotes = t.BillingAddressNotes ?? "";

			pickupAccountId = t.PickupAccountId ?? "";
			pickupPhone = t.PickupAddressPhone ?? "";
			pickupZone = t.PickupZone;
			pickupCompany = t.PickupCompanyName ?? "";
			pickupContact = t.PickupContact ?? "";
			pickupNotes = t.PickupAddressNotes ?? "";
			pickupEmail = t.PickupAddressEmailAddress ?? "";
			pickupCity = t.PickupAddressCity ?? "";
			pickupCountry = t.PickupAddressCountry ?? "";
			pickupPc = t.PickupAddressPostalCode ?? "";
			pickupProvince = t.PickupAddressRegion ?? "";
			pickupStreet = $"{t.PickupAddressAddressLine1 ?? ""}\r\n{t.PickupAddressAddressLine2 ?? ""}";
			pickupSuite = t.PickupAddressSuite ?? "";
			pickupDescription = t.PickupNotes;

			deliveryAccountId = t.DeliveryAccountId ?? "";
			deliveryPhone = t.DeliveryAddressPhone ?? "";
			deliveryZone = t.DeliveryZone;
			deliveryCompany = t.DeliveryCompanyName ?? "";
			deliveryContact = t.DeliveryContact ?? "";
			deliveryNotes = t.DeliveryAddressNotes ?? "";
			deliveryEmail = t.DeliveryAddressEmailAddress ?? "";
			deliveryCity = t.DeliveryAddressCity ?? "";
			deliveryCountry = t.DeliveryAddressCountry ?? "";
			deliveryPc = t.DeliveryAddressPostalCode ?? "";
			deliveryProvince = t.DeliveryAddressRegion ?? "";
			deliveryStreet = $"{t.DeliveryAddressAddressLine1 ?? ""}\r\n{t.DeliveryAddressAddressLine2 ?? ""}";
			deliverySuite = t.DeliveryAddressSuite ?? "";
			deliveryDescription = t.DeliveryNotes;

			podName = t.POD ?? "";

			caller = t.CallerName ?? "";
			callerEmail = t.CallerEmail ?? "";
			callerPhone = t.CallerPhone ?? "";

			clientReference = t.Reference ?? "";
			dangerousGoods = t.DangerousGoods;
		}
	}
}