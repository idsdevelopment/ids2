﻿using System;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using Protocol.Data._Customers.Pml;

// ReSharper disable InconsistentNaming

namespace PmlSdk
{
	public static class Ids
	{
		internal const string SDK = "PML SDK";

		public enum LOGIN_STATUS
		{
			OK,
			INVALID,
			TIME_OUT_OF_SYNC
		}

		private static bool LoggedIn;

		private static async Task<LOGIN_STATUS> LoginAsync( string authToken )
		{
			if( !LoggedIn )
			{
			#if DEBUG
				Azure.Slot = Azure.SLOT.ALPHA_TRW;
			#endif

				var Parts = authToken.Split( new[] { '-' }, StringSplitOptions.None );

				if( Parts.Length == 3 )
				{
					var Ok = await Azure.LogIn( SDK, Parts[ 0 ], Parts[ 1 ], Parts[ 2 ] );

					switch( Ok )
					{
					case Azure.AZURE_CONNECTION_STATUS.ERROR:
						return LOGIN_STATUS.INVALID;

					case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
						return LOGIN_STATUS.TIME_OUT_OF_SYNC;

					default:
						LoggedIn = true;

						break;
					}
				}
			}

			return LOGIN_STATUS.OK;
		}

		private static void Login( string authToken )
		{
			switch( LoginAsync( authToken ).Result )
			{
			case LOGIN_STATUS.INVALID:
				throw new LoginException();

			case LOGIN_STATUS.TIME_OUT_OF_SYNC:
				throw new TimeException();
			}
		}


		public static int ToInt( this STATUS s )
		{
			return (int)s;
		}

		public static STATUS ToStatus( this int i )
		{
			return (STATUS)i;
		}

		public static (Protocol.Data.STATUS Status1, STATUS1 Status2 ) ToStatus( this STATUS s )
		{
			switch( s )
			{
			case STATUS.ACTIVE:
				return ( Protocol.Data.STATUS.ACTIVE, STATUS1.UNSET );

			case STATUS.DELETED:
				return ( Protocol.Data.STATUS.DELETED, STATUS1.UNSET );

			case STATUS.DELIVERED:
				return ( Protocol.Data.STATUS.DELIVERED, STATUS1.UNSET );

			case STATUS.DISPATCHED:
				return ( Protocol.Data.STATUS.DISPATCHED, STATUS1.UNSET );

			case STATUS.NEW:
				return ( Protocol.Data.STATUS.NEW, STATUS1.UNSET );

			case STATUS.PICKED_UP:
				return ( Protocol.Data.STATUS.PICKED_UP, STATUS1.UNSET );

			case STATUS.POSTED:
				return ( Protocol.Data.STATUS.POSTED, STATUS1.UNSET );

			case STATUS.SCHEDULED:
				return ( Protocol.Data.STATUS.SCHEDULED, STATUS1.UNSET );

			case STATUS.UNDELIVERED:
				return ( Protocol.Data.STATUS.DELIVERED, STATUS1.UNDELIVERED );

			case STATUS.VERIFIED:
				return ( Protocol.Data.STATUS.VERIFIED, STATUS1.UNSET );

			default:
				return ( Protocol.Data.STATUS.UNSET, STATUS1.UNSET );
			}
		}


		public static STATUS ToStatus( Protocol.Data.STATUS s, STATUS1 s1 )
		{
			if( s1 == STATUS1.UNDELIVERED )
				return STATUS.UNDELIVERED;

			switch( s )
			{
			case Protocol.Data.STATUS.ACTIVE:
				return STATUS.ACTIVE;

			case Protocol.Data.STATUS.DELETED:
				return STATUS.DELETED;

			case Protocol.Data.STATUS.DELIVERED:
				return STATUS.DELIVERED;

			case Protocol.Data.STATUS.DISPATCHED:
				return STATUS.DISPATCHED;

			case Protocol.Data.STATUS.NEW:
				return STATUS.NEW;

			case Protocol.Data.STATUS.PICKED_UP:
				return STATUS.PICKED_UP;

			case Protocol.Data.STATUS.POSTED:
				return STATUS.POSTED;

			case Protocol.Data.STATUS.SCHEDULED:
				return STATUS.SCHEDULED;

			case Protocol.Data.STATUS.VERIFIED:
				return STATUS.VERIFIED;

			default:
				return STATUS.UNSET;
			}
		}

		public static async Task<remoteTripDetailed[]> pmlFindTripsDetailedUpdatedSinceAsync( string authToken,
		                                                                                      string zones,
		                                                                                      string allKey,
		                                                                                      int lowStatus,
		                                                                                      int upperStatus,
		                                                                                      string serviceLevels,
		                                                                                      long sinceSeconds )
		{
			if( await LoginAsync( authToken ) == LOGIN_STATUS.OK )
			{
				var Temp = await Azure.Client.RequestPML_FindTripsDetailedUpdatedSince( new PmlFindSince
				                                                                        {
					                                                                        FromStatus = (Protocol.Data.STATUS)lowStatus,
					                                                                        ToStatus = (Protocol.Data.STATUS)upperStatus,
					                                                                        ServiceLevels = serviceLevels,
					                                                                        SinceSeconds = sinceSeconds,

					                                                                        AllZones = allKey == "ALL",
					                                                                        Zones = zones
				                                                                        } );
				var Count = Temp.Count;

				if( Count > 0 )
				{
					var Result = new remoteTripDetailed[ Count ];

					for( var I = 0; I < Count; I++ )
						Result[ I ] = new remoteTripDetailed( Temp[ I ] );

					return Result;
				}
			}

			return new remoteTripDetailed[ 0 ];
		}

		public static remoteTripDetailed[] pmlFindTripsDetailedUpdatedSince( string authToken,
		                                                                     string zones,
		                                                                     string allKey,
		                                                                     int lowStatus,
		                                                                     int upperStatus,
		                                                                     string serviceLevels,
		                                                                     long sinceSeconds )
		{
			return pmlFindTripsDetailedUpdatedSinceAsync( authToken, zones, allKey, lowStatus, upperStatus, serviceLevels, sinceSeconds ).Result;
		}

		public static int ToStatus( this TripUpdate t )
		{
			return ToStatus( t.Status1, t.Status2 ).ToInt();
		}

		public static async Task<remoteTripDetailed> updateTripDetailedQuickAsync( string authToken, remoteTripDetailed trip, bool broadcastUpdateMessage )
		{
			if( await LoginAsync( authToken ) == LOGIN_STATUS.OK )
			{
				var Trip = trip.AsTrip;
				Trip.BroadcastToDispatchBoard = broadcastUpdateMessage;
				Trip.Program = SDK;
				await Azure.Client.RequestPML_updateTripDetailedQuick( Trip );
				trip.lastUpdated = DateTime.UtcNow.Ticks;
			}

			return null;
		}

		public static remoteTripDetailed updateTripDetailedQuick( remoteTripDetailed trip, string authToken, bool broadcastUpdateMessage )
		{
			return updateTripDetailedQuickAsync( authToken, trip, broadcastUpdateMessage ).Result;
		}
	}
}