﻿using System;
using System.IO;
using System.Threading;
using SendReceive.Glue;

namespace SendReceive
{
    internal class Program
    {
        private static void Main( string[] args )
        {
            try
            {
                string InFile, OutPath;

                switch( args.Length )
                {
                default:
                    throw new Exception( "Incorrect command line args" );
                case 1:
                    InFile = args[ 0 ];
                    OutPath = Path.GetDirectoryName( InFile );
                    break;
                case 2:
                    InFile = args[ 0 ];
                    OutPath = args[ 1 ];
                    break;
                }
                var Parser = new OtJ().Execute( InFile, OutPath );
                if( Parser.HasErrors )
                {
                    foreach( var Error in Parser.ErrorList )
                        Console.WriteLine( Error );

                    throw new Exception();
                }

                Environment.ExitCode = 0;
            }
            catch( Exception e )
            {
                Console.WriteLine( e.Message );

                while( !Console.KeyAvailable )
                    Thread.Sleep( 250 );

                Environment.ExitCode = 900;
            }
        }
    }
}