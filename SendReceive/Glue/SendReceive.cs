﻿using System;
using System.Collections.Generic;
using System.IO;
using Antlr4.Runtime;
using SendReceive.Code;
using SendReceive.Parser;

namespace SendReceive.Parser
{
    public partial class SendReceiveParser
    {
        internal bool HasErrors;
        internal List<string> SyntaxErrors = new List<string>();
    }
}

namespace SendReceive.Glue
{
    internal class OtJ
    {
        private SendReceiveParser Parser;

        internal bool HasErrors => ( Parser == null ) || Parser.HasErrors;
        internal List<string> ErrorList => Parser.SyntaxErrors ?? new List<string>();

        internal class ErrorListener : BaseErrorListener
        {
            private readonly SendReceiveParser Parser;

            public ErrorListener( SendReceiveParser parser )
            {
                Parser = parser;
            }

            public override void SyntaxError( IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg,
                                              RecognitionException e )
            {
                Parser.HasErrors = true;
                Parser.SyntaxErrors.Add( $"Line: {line},  Position: {charPositionInLine},  Symbol: '{offendingSymbol.Text.Trim()}'  Error: {msg.Trim()}" );
            }
        }

        internal OtJ Execute( string inFile, string outPath )
        {
            try
            {
                if( !File.Exists( inFile ) )
                    throw new ArgumentException( "Missing source file." );

                var Text = File.ReadAllText( inFile );

                var InStream = new AntlrInputStream( Text );

                var OutPath = Path.GetDirectoryName( Path.GetFullPath( outPath ) );
                if( OutPath != null )
                {
                    Directory.CreateDirectory( OutPath );

                    Parser = new SendReceiveParser( new CommonTokenStream( new SendReceiveLexer( InStream ) ) );

                    Parser.RemoveErrorListeners();
                    Parser.AddErrorListener( new ErrorListener( Parser ) );

                    var Tree = Parser.parse();
                    if( !Parser.HasErrors )
                    {
                        try
                        {
                            var Result = new SendReceiveVisitors().Visit( Tree );
                            new CodeGenerator( Result, outPath, inFile ).Generate();
                        }
                        catch( Exception E )
                        {
                            Parser.SyntaxErrors.Add( E.Message );
                        }
                    }
                }
                else
                    throw new ArgumentException( "Cannot create output file." );
            }
            catch( Exception E )
            {
                Console.WriteLine( E.Message );
            }
            return this;
        }
    }
}