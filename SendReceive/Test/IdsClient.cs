//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     SendReceive: 1.0.5
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from D:\Ids Source\CSharp\Visual Studio\2017\SendReceive\SendReceive\Test\Protocol.proto


using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Threading;


namespace Ids
{
    public class IdsClient : HttpClient
    {
        public string AuthorisationToken { get; private set; } = "";

        public IdsClient( string endPoint )
        {
            Timeout = new TimeSpan( 0, 0, 900 );
            BaseAddress = new Uri( endPoint );
            var DefA = DefaultRequestHeaders.Accept;
            DefA.Clear();
            DefA.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );
        }

        private static Exception ShowException( Exception e )
        {
            Console.WriteLine( e.Message );
            if( e.InnerException != null )
                Console.WriteLine( e.InnerException.Message );
            return e;
        }

        public IdsClient( string endPoint, string authorisationToken ) : this( endPoint )
        {
            AuthorisationToken = authorisationToken;
        }

        private async Task<string> Post( string path, string json )
        {
            var Retries = 1;
            while( true )
            {
                try
                {
                    var Resp = await PostAsync( path, new StringContent( json ) );
                    Resp.EnsureSuccessStatusCode();
                    return await Resp.Content.ReadAsStringAsync();
                }
                catch( HttpRequestException E )
                {
                    if( Retries-- > 0 )
                    {
                        Thread.Sleep( 10 );
                        continue;
                    }
                    throw ShowException( E );
                }
                catch( Exception E )
                {
                    throw ShowException( E );
                }
            }
        }

        private async Task<TResponse> Post<TRequest,TResponse>( TRequest requestObject, string action )
        {
            var Retries = 1;
            while( true )
            {
                try
                {
                    var Json = JsonConvert.SerializeObject( requestObject );
                    var Auth = $"?Auth={Uri.EscapeUriString( AuthorisationToken )}";
                    var RespStr = await Post( $"Actions/Ajax.ajax/{action}{Auth}", Json );
                    return JsonConvert.DeserializeObject<TResponse>( RespStr );
                }
                catch( Exception E )
                {
                    if( Retries-- > 0 )
                    {
                        Thread.Sleep( 10 );
                        continue;
                    }
                    throw ShowException( E );
                }
            }
        }

        
        		/// <summary>
        		/// xxxxxxxx
        		/// </summary>
        public async Task<string> RequestPing()
        {
            return await Post<string,string>( "", "Ping" );
        }

        
        		/// <summary>
        		/// yyyyyyyyyyyyyyyyyyy
        		/// </summary>
        public async Task<string> RequestLogin( string carrierId, string userName, string password )
        {
            var Args = new Dictionary<string, string>();
            Args.Add( "carrierId", carrierId );
            Args.Add( "userName", userName );
            Args.Add( "password", password );
            return AuthorisationToken = await Post<Dictionary<string, string>,string>( Args, "Login" );
        }

        public async Task RequestGps( string requestObject )
        {
            await Post<string,string>( requestObject, "Gps" );
            return;
        }

    }
}
