parser grammar SendReceiveParser;

options
	{ 
		tokenVocab=SendReceiveLexer; 		// Use tokens from lexer
	}	

parse: name base settings? usings protocols EOF
	;

usings: BEGIN_USING USING* END_USING
	;

using: USING
	;

name: NAME IDENT SEMICOLON
	;

base: BASE PATH_NAME SEMICOLON
	;

protocols: protocolAttribute BEGIN_PROTOCOL protocol* END_PROTOCOL
	;

protocolAttribute: ( AUTH_TOKEN )?
    ;

protocol: protocolObjectAttribute protocolType SEPARATOR protocolType SEMICOLON
	;

protocolObjectAttribute: protocolSummary ( ( ATTRIBUTE ( COMMA  NO_AUTH_TOKEN )? ) | AUTH_TOKEN  )?
	;

protocolSummary: SUMMARY?
	;

protocolType: IDENT | protocolCommand
	;
	
protocolCommand: PROTOCOL_COMMAND args?
	;				  			

args: BEGIN_ARGS ( (ARG COMMA)* ARG  )* END_ARGS
	 ;

settings: BEGIN_SETTINGS ( setting SEMICOLON )* END_SETTINGS
	;

setting:  MAX_REQUESTS NUMBER 
		| REQUEST_TIMEOUT NUMBER
		| AUTH_TOKEN
		| PAGE IDENT
		| MAX_CONNECTIONS NUMBER
		| EXECUTION_TIMEOUT NUMBER
		| RETRIES NUMBER
		| RETRY_DELAY NUMBER
	;

