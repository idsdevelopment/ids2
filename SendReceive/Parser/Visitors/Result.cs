﻿using System.Collections.Generic;
using System.Linq;
using SendReceive.Code;

namespace SendReceive.Parser.Visitors
{
    internal class ProtocolEntry
    {
        internal string Class;
        internal bool IsCommand, IsVoid;
        internal List<string> Args = new List<string>();
    }

    internal class Protocol
    {
        internal string Summary;

        internal string AttributeName;

        internal ProtocolEntry Request = new ProtocolEntry(),
                               Response = new ProtocolEntry();

        internal bool AurhorisationProtocol;
        internal bool NoAurhorisationRequited;
    }

    internal class Result
    {
        internal string Name, Path;
        internal List<Protocol> Protocols;
        internal List<string> Usings;
        internal bool IsCommand;
        internal List<string> Args;

        internal int MaxRequests = -1;
        internal int RequestTimeout = 60;
        internal int MaxConnections;
        internal int ExecutionTimeout;
        internal int Retries;
        internal int RetryDelay;

        internal string Summary;

        internal bool RequiresAuthorisation;
        internal bool Temp, TempNoAuth;

        public string Page = "Index.ajax";

        public string AuthorisationType => ( from Protocol in Protocols where Protocol.AurhorisationProtocol select Protocol.Response.Class.Trim() ).FirstOrDefault();

        internal bool HasCommands => Protocols.Any( protocol => protocol.Request.IsCommand || protocol.Response.IsCommand );
        internal bool HasCommandResponse => Protocols.Any( protocol => protocol.Response.IsCommand );

        internal List<string> NoAuthList
        {
            get
            {
                var Result = new List<string>();

                foreach( var Protocol in Protocols )
                {
                    if( Protocol.AurhorisationProtocol || Protocol.NoAurhorisationRequited )
                    {
                        var Request = Protocol.Request;

                        string CName;
                        if( Protocol.AurhorisationProtocol )
                            CName = Request.Class.Trim( '"' );
                        else
                        {
                            CName = !string.IsNullOrWhiteSpace( Protocol.AttributeName )
                                ? Protocol.AttributeName
                                : $"{CodeGenerator.DoAttribute( Protocol, Protocol.Request )}{CodeGenerator.DoAttribute( Protocol, Protocol.Response )}";
                        }

                        Result.Add( CName );
                    }
                }

                return Result;
            }
        }
    }
}