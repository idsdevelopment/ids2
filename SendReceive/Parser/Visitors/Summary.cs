﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitProtocolSummary( SendReceiveParser.ProtocolSummaryContext context )
        {
            var Result = new Result();

            var Summary = context.SUMMARY();
            if (Summary != null)
                Result.Summary = Summary.GetText().Trim();

            return Result;
        }
    }
}
