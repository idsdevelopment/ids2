﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitParse( SendReceiveParser.ParseContext context )
        {
            var Result = VisitName( context.name() );
            Result.Path = VisitBase( context.@base() ).Path;

            var Settings = VisitSettings( context.settings() );
            Result.MaxRequests = Settings.MaxRequests;
            Result.RequestTimeout = Settings.RequestTimeout;
            Result.MaxConnections = Settings.MaxConnections;
            Result.ExecutionTimeout = Settings.ExecutionTimeout;
            Result.Retries = Settings.Retries;
            Result.RetryDelay = Settings.RetryDelay;

            Result.Page = Settings.Page;

            Result.Usings = VisitUsings( context.usings() ).Usings;
            Result.Protocols = VisitProtocols( context.protocols() ).Protocols;

            return Result;
        }
    }
}