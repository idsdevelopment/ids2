﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitSettings( SendReceiveParser.SettingsContext context )
        {
            var Result = new Result();
            var Settings = context.setting();
            if( Settings != null )
            {
                foreach( var Setting in Settings )
                {
                    var MaxR = Setting.MAX_REQUESTS();
                    if( MaxR != null )
                        int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.MaxRequests );
                    else
                    {
                        var RequestTimeout = Setting.REQUEST_TIMEOUT();
                        if( RequestTimeout != null )
                            int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.RequestTimeout );
                        else
                        {
                            var Page = Setting.PAGE();
                            if( Page != null )
                            {
                                var Id = Setting.IDENT();
                                if( Id != null )
                                    Result.Page = Id.GetText().Trim();
                            }
                            else
                            {
                                var Connections = Setting.MAX_CONNECTIONS();
                                if( Connections != null )
                                    int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.MaxConnections );
                                else
                                {
                                    var ExecutionTimeout = Setting.EXECUTION_TIMEOUT();
                                    if( ExecutionTimeout != null )
                                        int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.ExecutionTimeout );
                                    else
                                    {
                                        var Retries = Setting.RETRIES();
                                        if( Retries != null )
                                            int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.Retries );
                                        else
                                        {
                                            var Delay = Setting.RETRY_DELAY();
                                            if( Delay != null )
                                                int.TryParse( Setting.NUMBER().GetText().Trim(), out Result.RetryDelay );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Result;
        }
    }
}