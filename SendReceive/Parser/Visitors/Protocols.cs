﻿using System;
using System.Collections.Generic;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitProtocolCommand( SendReceiveParser.ProtocolCommandContext context )
        {
            var Result = VisitArgs( context.args() );
            Result.Name = context.PROTOCOL_COMMAND().GetText().Trim();
            Result.IsCommand = true;
            return Result;
        }

        public override Result VisitProtocolType( SendReceiveParser.ProtocolTypeContext context )
        {
            var Command = context.protocolCommand();
            return Command != null ? VisitProtocolCommand( Command ) : new Result { Name = context.IDENT().GetText().Trim() };
        }


        private static void DoProtocol( ProtocolEntry entry, Result value )
        {
            var Name = value.Name;
            if( Name == "void" )
            {
                entry.IsVoid = true;
                Name = "string";
            }

            entry.Class = Name;

            if( value.IsCommand )
            {
                entry.IsCommand = true;
                entry.Args = value.Args;
            }
        }

        public override Result VisitProtocol( SendReceiveParser.ProtocolContext context )
        {
            var Result = new Result { Protocols = new List<Protocol>() };
            var Proto = new Protocol();

            var Attributes = context.protocolObjectAttribute();
            if( Attributes != null )
            {
                var A = VisitProtocolObjectAttribute( Attributes );
                Proto.AttributeName = A.Name;
                Proto.AurhorisationProtocol = A.Temp;
                Proto.NoAurhorisationRequited = A.TempNoAuth;
                Proto.Summary = A.Summary;
            }

            var P = context.protocolType();
            if( P != null )
            {
                DoProtocol( Proto.Request, VisitProtocolType( P[ 0 ] ) );
                DoProtocol( Proto.Response, VisitProtocolType( P[ 1 ] ) );

                Result.Protocols.Add( Proto );
            }
            return Result;
        }

        public override Result VisitProtocols( SendReceiveParser.ProtocolsContext context )
        {
            var Result = new Result
                         {
                             Protocols = new List<Protocol>(),
                             RequiresAuthorisation = VisitProtocolAttribute( context.protocolAttribute() ).RequiresAuthorisation
                         };

            foreach( var ProtocolContext in context.protocol() )
                Result.Protocols.AddRange( VisitProtocol( ProtocolContext ).Protocols );

            return Result;
        }
    }
}