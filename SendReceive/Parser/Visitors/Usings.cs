﻿using System.Collections.Generic;
using System.Linq;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitUsing( SendReceiveParser.UsingContext context )
        {
            var Result = new Result();
            var U = context.USING();
            if( U != null )
                Result.Name = U.GetText();

            return Result;
        }

        public override Result VisitUsings( SendReceiveParser.UsingsContext context )
        {
            var Usings = new List<string>();
            var U = context.USING();
            if( U != null )
                Usings.AddRange( U.Select( node => node?.GetText().Trim() ).Where( txt => !string.IsNullOrWhiteSpace( txt ) ) );
            return new Result { Usings = Usings };
        }
    }
}