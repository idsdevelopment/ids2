﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        public override Result VisitProtocolObjectAttribute( SendReceiveParser.ProtocolObjectAttributeContext context )
        {
            var Result = VisitProtocolSummary( context.protocolSummary() );
            var Auth = context.AUTH_TOKEN();
            if( Auth != null )
            {
                Result.Temp = true;
                Result.Name = Auth.GetText().Trim();
            }
            else
            {
                Result.TempNoAuth = context.NO_AUTH_TOKEN() != null;

                var Attr = context.ATTRIBUTE();

                if( Attr != null )
                    Result.Name = Attr.GetText().Trim();
            }
            return Result;
        }

        public override Result VisitProtocolAttribute( SendReceiveParser.ProtocolAttributeContext context )
        {
            return new Result { RequiresAuthorisation = context.AUTH_TOKEN() != null };
        }
    }
}