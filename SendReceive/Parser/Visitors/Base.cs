﻿using System;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
    internal partial class SendReceiveVisitors
    {
        internal class NoBaseException : ArgumentException
        {
            internal NoBaseException() : base( "Invalid path/base defined" )
            {
            }
        }

        public override Result VisitBase( SendReceiveParser.BaseContext context )
        {
            var Path = context.PATH_NAME();
            if( Path == null )
                throw new NoBaseException();
            var PName = Path.GetText();
            if( string.IsNullOrWhiteSpace( PName ) )
                throw new NoBaseException();
            return new Result { Path = PName };
        }
    }
}