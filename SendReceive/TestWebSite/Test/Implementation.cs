﻿namespace Ids
{
    partial class IdsServer
    {
        partial class IdsServerImplementation
        {
            public bool ValidateAuthorisation( string authToken )
            {
                return true;
            }

            public string ResponseAuth( string carrierId, string userName, string password )
            {
                return "ABCDEFGHIJK";
            }


            public string Responsevoidstring( string requestObject )
            {
                return "voidstring_Ok";
            }

            public string ResponsePing( string requestObject )
            {
                return "Ping_Ok";
            }

            public string ResponseTestString( string requestObject )
            {
                return requestObject == "xxx" ? "TestString_Ok" : "TestString_Bad";
            }

            public object Responsexobject( string a, string b, string c )
            {
                return a == "1" && b == "2" && c == "3" ? "xobject_Ok" : "xobject_Bad";
            }
        }
        
    }
}