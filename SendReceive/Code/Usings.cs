﻿namespace SendReceive.Code
{
    internal partial class CodeGenerator
    {
        private void ClientUsings()
        {
            WriteLine( "using System;" );
            WriteLine( "using System.Collections.Generic;" );
            WriteLine( "using System.Net.Http;" );
            WriteLine( "using Newtonsoft.Json;" );
            WriteLine( "using System.Threading.Tasks;" );
            WriteLine( "using System.Net.Http.Headers;" );
            if( Result.RetryDelay > 0 )
                WriteLine( "using System.Threading;" );

            NewLine();

            foreach( var Using in Result.Usings )
                WriteLine( $"using {Using}" );

            NewLine();
        }

        private void ServerUsings()
        {
            WriteLine( "using System;" );
            WriteLine( "using System.Net;" );
            WriteLine( "using System.Collections.Generic;" );
            WriteLine( "using System.Text;" );
            WriteLine( "using System.Web;" );
            WriteLine( "using Newtonsoft.Json;" );
            WriteLine( "using System.Threading.Tasks;" );

            NewLine();

            foreach( var Using in Result.Usings )
                WriteLine( $"using {Using}" );

            NewLine();
        }
    }
}