﻿using System.Text;
using SendReceive.Parser.Visitors;

namespace SendReceive.Code
{
    internal partial class CodeGenerator
    {
        private static string ResponseType( ProtocolEntry entry )
        {
            return entry.IsCommand ? "CommandResponse" : entry.Class;
        }

        internal static string DoAttribute( Protocol protocol, ProtocolEntry entry )
        {
            string Name;

            if( !string.IsNullOrWhiteSpace( protocol.AttributeName ) )
                Name = protocol.AttributeName.Trim();
            else
                Name = entry.IsVoid ? "void" : entry.Class.Trim( '"' );

            return Name;
        }

        private void DoCommandResponse()
        {
            if( Result.HasCommandResponse )
            {
                WriteLine( "public class CommandResponse" );
                IndentInBrace();
                try
                {
                    WriteLine( "public string Command;" );
                    WriteLine( "public KeyValuePair<string,string> Args;" );
                }
                finally
                {
                    IndentOutBrace();
                    NewLine();
                }
            }
        }

        private void Client()
        {
            var Name = $"{Result.Name}Client";
            using( OpenStream( Name ) )
            {
                ClientUsings();

                Class( Name, () =>
                {
                    var AuthType = Result.AuthorisationType;
                    if( AuthType != null )
                    {
                        Result.RequiresAuthorisation = true;
                        WriteLine( $"public {AuthType} AuthorisationToken {{ get; set; }} = \"\";" );
                        NewLine();
                    }

                    DoCommandResponse();

                    WriteLine( $"public {Name}( string endPoint )" );
                    IndentInBrace();

                    try
                    {
                        if( Result.ExecutionTimeout > 0 )
                            WriteLine( $"Timeout = new TimeSpan( 0, 0, {Result.ExecutionTimeout} );" );

                        WriteLine( "BaseAddress = new Uri( endPoint );" );
                        WriteLine( "var DefA = DefaultRequestHeaders.Accept;" );
                        WriteLine( "DefA.Clear();" );
                        WriteLine( "DefA.Add( new MediaTypeWithQualityHeaderValue( \"application/json\" ) );" );
                    }
                    finally
                    {
                        IndentOutBrace();
                        NewLine();
                    }

                    WriteLine( "private static Exception ShowException( Exception e )" );
                    IndentInBrace();
                    WriteLine( "Console.WriteLine( e.Message );" );
                    WriteLine( "if( e.InnerException != null )" );
                    IndentIn();
                    WriteLine( "Console.WriteLine( e.InnerException.Message );" );
                    IndentOut();
                    WriteLine( "return e;" );
                    IndentOutBrace();
                    NewLine();

                    WriteLine( $"public {Name}( string endPoint, string authorisationToken ) : this( endPoint )" );
                    IndentInBrace();
                    WriteLine( "AuthorisationToken = authorisationToken;" );
                    IndentOutBrace();
                    NewLine();

                    WriteLine( "private async Task<string> Post( string path, string json )" );
                    IndentInBrace();

                    var DoingRetries = Result.Retries > 0;
                    if( DoingRetries )
                    {
                        WriteLine( $"var Retries = {Result.Retries};" );
                        WriteLine( "while( true )" );
                        IndentInBrace();
                    }

                    WriteLine( "try" );
                    IndentInBrace();
                    WriteLine( "var Resp = await PostAsync( path, new StringContent( json ) );" );
                    WriteLine( "Resp.EnsureSuccessStatusCode();" );
                    WriteLine( "return await Resp.Content.ReadAsStringAsync();" );
                    IndentOutBrace();

                    if( DoingRetries )
                    {
                        WriteLine( "catch( HttpRequestException E )" );
                        IndentInBrace();
                        WriteLine( "if( Retries-- > 0 )" );

                        var DoingDealy = Result.RetryDelay > 0;
                        if( DoingDealy )
                        {
                            IndentInBrace();
                            WriteLine( $"Thread.Sleep( {Result.RetryDelay} );" );
                        }
                        else
                            IndentIn();

                        WriteLine( "continue;" );

                        if( DoingDealy )
                            IndentOutBrace();
                        else
                            IndentOut();

                        WriteLine( "throw ShowException( E );" );
                        IndentOutBrace();
                    }
                    WriteLine( "catch( Exception E )" );
                    IndentInBrace();
                    WriteLine( "throw ShowException( E );" );
                    IndentOutBrace();
                    IndentOutBrace();
                    if( DoingRetries )
                        IndentOutBrace();
                    NewLine();

                    WriteLine( "private async Task<TResponse> Post<TRequest,TResponse>( TRequest requestObject, string action )" );
                    IndentInBrace();
                    try
                    {
                        if( DoingRetries )
                        {
                            WriteLine( $"var Retries = {Result.Retries};" );
                            WriteLine( "while( true )" );
                            IndentInBrace();
                        }
                        WriteLine( "try" );
                        IndentInBrace();
                        WriteLine( "var Json = JsonConvert.SerializeObject( requestObject );" );

                        string AuthToken;

                        if( Result.RequiresAuthorisation )
                        {
                            WriteLine( "var Auth = $\"?Auth={Uri.EscapeUriString( AuthorisationToken )}\";" );
                            AuthToken = "{Auth}";
                        }
                        else
                            AuthToken = "";

                        WriteLine( $"var RespStr = await Post( $\"{Result.Path}/{Result.Page}/{{action}}{AuthToken}\", Json );" );
                        WriteLine( "return JsonConvert.DeserializeObject<TResponse>( RespStr );" );
                        IndentOutBrace();
                        WriteLine( "catch( Exception E )" );
                        IndentInBrace();

                        if( DoingRetries )
                        {
                            WriteLine("if( Retries-- > 0 )");
                            var DoingDealy = Result.RetryDelay > 0;
                            if (DoingDealy)
                            {
                                IndentInBrace();
                                WriteLine($"Thread.Sleep( {Result.RetryDelay} );");
                                WriteLine("continue;");
                                IndentOutBrace();
                            }
                            else
                            {
                                IndentIn();
                                WriteLine("continue;");
                                IndentOut();
                            }
                        }

                        WriteLine( "throw ShowException( E );" );
                        IndentOutBrace();
                    }
                    finally
                    {
                        if( DoingRetries )
                            IndentOutBrace();

                        IndentOutBrace();
                        NewLine();
                    }

                    foreach( var Protocol in Result.Protocols )
                    {
                        Summary( Protocol.Summary );

                        var Request = Protocol.Request;
                        var Response = Protocol.Response;

                        string Nme;
                        if( Protocol.AurhorisationProtocol )
                            Nme = Request.Class.Trim( '"' );
                        else
                        {
                            Nme = !string.IsNullOrWhiteSpace( Protocol.AttributeName )
                                ? Protocol.AttributeName
                                : DoAttribute( Protocol, Request ) + DoAttribute( Protocol, Response );
                        }

                        var Args = new StringBuilder();

                        if( Request.IsCommand )
                        {
                            Args.Append( ' ' );
                            var First = true;
                            foreach( var Arg in Request.Args )
                            {
                                if( !First )
                                    Args.Append( ", " );
                                Args.Append( "string " );
                                Args.Append( Arg );
                                First = false;
                            }
                            Args.Append( ' ' );
                        }
                        else
                            Args.Append( Request.IsVoid ? "" : $" {Request.Class} requestObject " );

                        WriteLine( Response.IsVoid
                                       ? $"public async Task Request{Nme}({Args})"
                                       : $"public async Task<{ResponseType( Response )}> Request{Nme}({Args})" );

                        IndentInBrace();
                        try
                        {
                            var AuthToken = Protocol.AurhorisationProtocol ? "AuthorisationToken = " : "";
                            if( Request.IsCommand )
                            {
                                WriteLine( "var Args = new Dictionary<string, string>();" );
                                {
                                    foreach( var Arg in Request.Args )
                                        WriteLine( $"Args.Add( \"{Arg}\", {Arg.Trim( '"' )} );" );
                                }
                                if( Response.IsVoid )
                                {
                                    WriteLine( $"await Post<Dictionary<string, string>,{ResponseType( Response )}>( Args, \"{Nme}\" );" );
                                    WriteLine( "return;" );
                                }
                                else
                                    WriteLine( $"return {AuthToken}await Post<Dictionary<string, string>,{ResponseType( Response )}>( Args, \"{Nme}\" );" );
                            }
                            else
                            {
                                var ReqObj = Request.IsVoid ? "\"\"" : "requestObject";
                                if( Response.IsVoid )
                                {
                                    WriteLine( $"await Post<{ResponseType( Request )},{ResponseType( Response )}>( {ReqObj}, \"{Nme}\" );" );
                                    WriteLine( "return;" );
                                }
                                else
                                    WriteLine( $"return {AuthToken}await Post<{ResponseType( Request )},{ResponseType( Response )}>( {ReqObj}, \"{Nme}\" );" );
                            }
                        }
                        finally
                        {
                            IndentOutBrace();
                            NewLine();
                        }
                    }
                }, "HttpClient" );
            }
        }
    }
}