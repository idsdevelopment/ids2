﻿using System.IO;

namespace SendReceive.Code
{
    internal partial class CodeGenerator
    {
        private FileStream OpenStream( string fileName )
        {
            FileStream F;
            OutFile = F = new FileStream( $"{OutPath}{Path.DirectorySeparatorChar}{fileName}.cs", FileMode.Create, FileAccess.Write, FileShare.None );
            AutoGenerated();
            return F;
        }
    }
}