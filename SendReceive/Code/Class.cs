﻿using System;

namespace SendReceive.Code
{
    internal partial class CodeGenerator
    {
        private void Class( string name, Action classBody, string derivedFrom = null, bool partial = false, bool @abstract = false )
        {
            WriteLine( $"namespace {Result.Name}" );

            IndentInBrace();

            try
            {
                var Partial = partial ? "partial " : "";
                var Abs = @abstract ? "abstract " : "";
                var DerivedFrom = string.IsNullOrWhiteSpace( derivedFrom ) ? "" : $" : {derivedFrom}";
                WriteLine( $"public {Partial}{Abs}class {name}{DerivedFrom}" );
                IndentInBrace();

                try
                {
                    classBody();
                }
                finally
                {
                    IndentOutBrace();
                }
            }
            finally
            {
                IndentOutBrace();
            }
        }

        private void Summary( string summary )
        {
            if( !string.IsNullOrEmpty( summary ) )
            {
                WriteLine( "" );
                var Temp = summary.Replace( "\r", "" ).Split( new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );
                foreach( var S in Temp )
                    WriteLine( $"\t\t/// {S.Trim()}" );
            }
        }
    }
}