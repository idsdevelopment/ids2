﻿using System.IO;
using System.Text;
using SendReceive.Parser.Visitors;

namespace SendReceive.Code
{
    internal partial class CodeGenerator
    {
        private const int INDENT = 4;

        private readonly Result Result;
        private readonly string OutPath;
        private Stream OutFile;

        private int Indent;
        private string IndentStr = string.Empty;
        private readonly string SourceFile;

        internal CodeGenerator( Result result, string outPath, string sourceFile )
        {
            Result = result;
            OutPath = outPath;
            SourceFile = sourceFile;
        }

        private void DoIndent( int indent )
        {
            Indent += indent;
            if( Indent < 0 )
                Indent = 0;

            if( Indent == 0 )
                IndentStr = string.Empty;
            else
            {
                var Builder = new StringBuilder();
                for (var I = 0; I < Indent; I++)
                    Builder.Append(" ");
                IndentStr = Builder.ToString();
            }
        }

        private void IndentIn()
        {
            DoIndent( INDENT );
        }

        private void IndentOut()
        {
            DoIndent( -INDENT );
        }

        private void IndentInBrace()
        {
            WriteLine("{");
            IndentIn();
        }

        private void IndentOutBrace()
        {
            IndentOut();
            WriteLine("}");
        }

        private void WriteLine( string text )
        {
            var Buffer = Encoding.UTF8.GetBytes( $"{IndentStr}{text}\r\n" );
            OutFile.Write( Buffer, 0, Buffer.Length );
        }


        private void NewLine()
        {
            var Buffer = Encoding.UTF8.GetBytes("\r\n");
            OutFile.Write(Buffer, 0, Buffer.Length);
        }

        internal void Generate()
        {
            Client();
            Server();
        }
    }
}