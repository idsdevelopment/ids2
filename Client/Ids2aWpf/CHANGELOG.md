# Changelog for Ids2Wpf

Based on https://keepachangelog.com/en/1.0.0/

Types of changes within each release:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## [Unreleased]
## 2020-04-16
### Added
- Staff - Activated 'Delete'.  It doesn't actually delete the user, just disables it.
## 2020-04-09
### Added
- Staff - Basic validation (staff id and passwords).
- Staff - Dialog is displayed when a staff member is added or updated.
### Changed
- Staff - Cancel now clears everything, including the roles.
- Program's name in ALT-TAB is now 'IDS Shipments' to match the window's title.
- Passwords are being loaded, and the 'reveal' button works.
## 2020-03-17
### Changed
- 'Staff' tab filtering is working.
## 2020-03-10

### Changed
- Program is identified as 'IDS Core' in alt-tab list, rather than MainWindow.
- Starting to rework 'Staff' tab to match the others.  Toolbar/menu is done.
### Changed - Shipments
- New 'Save' button is always enabled, like the toolbar button.

## 2020-03-09
### Added - Shipments
- Added 'Save' button to right-hand side.
### Changed - Shipments
- Region filter now sees addresses that have abbreviations instead of full names.
- Filters and selected address are cleared when a filter is changed.
### Fixed - Shipments
- Changing the region filter didn't reload the unfiltered list first.
- Location Barcode filter had duplicated results.

### Fixed - I2P-2
- Exception thrown when there are duplicate sort orders.

## 2020-03-06
### Changed
#### I2P-18
- Changed the Preferences tab to *not* save automatically, but to behave like the Service Level and Package Type sections in RateWizard.  A confirmation dialog is displayed on saving.
The help button works, but goes to the general IDS Core tutorials page.

### Fixed
#### I2P-4
- Audit trail isn't always loading.

## 2020-02-20
### Fixed
#### I2P-4
- Addresses are reloaded in open search and shipment entry tabs when an address is added or updated.
- DeliveryNotes was being saved in DeliveryAddressNotes.

### Fixed
#### Shipment Entry
- Delivery postal/zip wasn't loaded on selection.


## 2020-02-11
### Changed
- Dispatch and Drivers boards - on starting up, create a Trip Entry tab if one doesn't exist, like Search Trips.
- Dispatch and Drivers board - double-clicking a trip loads it into the Trip Entry tab.
### Fixed
- Trip Entry - The primary package type wasn't being saved.


## 2020-02-06
### Fixed
- Rate Wizard - The list of Package Types wasn't updated after saving.
- Rate Wizard - The list of Service Levels wasn't updated after saving.
### Added 
- Shipments (PML) - Added Startrack service level.
## 2020-01-29
### Added 
- Rate Wizard - Colour selection to service levels.  **Note** - not connected to dispatch boards yet.

### Fixed
- Rate Wizard -Service Levels weren't sorted properly.


## 2020-01-23
### Fixed
-- Trip Entry - New didn't clear all of the field.
-- Search Trips - Exception on cancelling EditSelectedTrips.

## 2020-01-14
### Fixed
- AddressBook - Exception on clearing address if there were no groups.
- Shipment Entry - Account Phone is loaded when the account is selected in the combo box.
## 2019-12-10
### Fixed
- Shipment Entry - Addresses were being duplicated.  Now, they are only added if the company name doesn't exist.  To modify an address, use the Address Book.
- Shipment Entry - status label at top is updated when a trip is saved. Eg. if the trip has been dispatched from here, the Status shows DISPATCHED.
- Shipment Entry - if the trip that has been loaded has the prov/state abbreviation, it is changed to the full name.
## 2019-12-09
### Added
- EditSelectedDialog has a status-changing combo box.
## 2019-12-05
- I2P-10 - Changed context menu.  'Open in Shipment Entry' loads the first selected trip into the Shipment Entry tab.  'Edit Selected' opens the trips in the EditSelectedTrips dialog.
- I2P-10 - New help uri for Search Trips.
- All Ratewizard help uris are set to the same page.
- I2P-26 - New Help uri.
- PML Report handles group selection.
## 2019-12-04
- ML-311 - Added Help button to login page.
- PML report is basically working.  Have yet to test all combinations of the settings.

## 2019-11-28
### Fixed
- Database/Model/Databases/Carrier/Zones.cs didn't delete zones.
- RateWizard's Service Levels failed to load if there were duplicate SortOrders.
### Added
- RateWizard's Zone wizard now works.  The db is updated, but they aren't connected to the rest of the Client.

## 2019-11-25
### Added
`PML Reports` 

- Dialog is displayed on errors and success.
- Put in Toolbar/Menu swapping.
### Fixed
Server-side changes on Friday broke the addresses.
Working save.

## 2019-11-22
`I2P-6` PML Shipments
### Fixed 
- PML Addresses weren't visible.
`I2P-4` Shipment Entry
### Fixed
- The Package Type for each line was defaulting to the first package type in the combobox.

`PML Reports` 
### Added
- Starting to build the generated Excel file.

## 2019-11-20
### Added
Starting to build the Report dialog.

`I2P-4` Shipment Entry
### Fixed
- Address names and Account list are no longer lower case.
### Removed
- Took out the Default Address checkboxes for the moment.

## 2019-11-19
### Added
- Reports tab with basic UI.  No backing report yet.

## 2019-11-15
`I2P-6` PML Shipments
### Fixed
- Pickup Location Barcode in the trip is set to the PickupAddress' LocationBarcode.  Delivery Location Barcode has also been fixed.

`Shipment Entry`
### Fixed
- CustomerPhone was uneditable.

`I2P-6`
### Added
- ReadyTime and DueTime are set to now.
- Double-clicking an address selects it.

### Changed
- Quantity has been changed to an IntegerUpDown with a minimum of 1.
- Uppack shipments have 9999 pieces.

`Address Book`
### Added

- Import - a message is displayed showing what headers are incorrect and what they should be.

## 2019-11-13
`I2P-4` Shipment Entry && `I2P-10` Search Shipments

- Customer Phone is now loaded.
- Fixed a periodic bug in the loading of shipments from the search tab that resulted in either some fields not being populated, or the application freezing.

## 2019-11-08
`I2P-4` Shipment Entry

### Fixed
- Cloned trips have a CallTime of now.
- Cloned trips are set to status `New`.
- Occasionally the pickup or delivery company names don't load.
- The ShipmentId field is _always_ readonly.
- When an account is selected, if there is no ShipmentId (`New Shipment` hasn't been clicked), a new ShipmentId is fetched.
- When `New Shipment` is clicked, if the current trip hasn't been saved, a dialog is displayed asking whether or not to save it first that includes a **Cancel** button.

### Changed
- Trimming all string fields when building trip.

## 2019-11-06
`I2P-4` Shipment Entry

### Fixed
- Package Weight has been changed to a DecimalUpDown with a minimum value of 0.1 to prevent it being set to 0.

## 2019-11-06
`I2P-4` Shipment Entry

### Fixed
- \#1 Call date and time changing to 01-01-01.
- Weight and pieces don't change to 0.
- Package items aren't duplicated on repetitive saves.
## 2019-11-06
### Changed
- `Address Book` - Adding address file import and update mechanism works.

## 2019-11-05
### Added
- `Address Book` - Adding address file import and update mechanism.  Not fully finished.

### Changed
- `Address Book` - Delete now handles multiple addresses.

## 2019-10-31
### Fixed
- `Shipment Entry` - fixed bug with New Trip (I2P-4).

## 2019-10-29
### Changed
- `Search Shipments` - Converted to use Terry's DataGrid with children.  

## 2019-10-24
### Fixed
**I2P-10**

- `Search Shipments` - List didn't display the driver.
` `Search Shipments` - Loading the latest version of a trip doesn't work if the trip is in storage.  Checks that there is a TripId in the returned copy and ignores it if there isn't.

## 2019-10-23
### Fixed
- `Trip Entry` - UI locked up when opening 1 trip after another in the Search Shipments tab. 

## 2019-10-23
### Changed
- `EditPackagesInventory` - When items are added to the selectged list, their checkboxes are checked.  This will cut down on the number of steps required by Linfox.
- `EditPackagesInventory` - If no items are checked, the service level combo and button are disabled.

## 2019-10-22
### Fixed
- `PML Shipments` - Addresses didn't have Location barcodes.

### Added
- `EditPackagesInventory` - UI now allows the user to set a service level for the items.
- `Trip Entry` - TripItems now have a service level in their ItemCode field.


## 2019-10-21
### Changed
- `PML Shipments` - Updated help url.

### Fixed
- `PML Shipments` - Regions list was getting extra blank lines.

## 2019-10-21
### Added
- `Address Book` - added Location Barcode column to address list.  Filter by Location Barcode works.

## 2019-10-21
### Changed
- `PML` Inventory removed from PML menu.
- `PML Products` - new URI for help.

### Added
- `PML Products` - context menu for Save and clear.

### Fixed
- Inventory.PML_AddUpdateInventory was missing BinX, BinY, and BinZ fields when creating an Inventory item from a PmlInventory object.

### Fixed
- Shipment was not putting the package types and items into the trip record.

## 2019-10-18
### Changed
- `Trip Entry` - When loading a trip's inventory, it looks them up by barcode instead of description.  In addition, the first one is selected.

### Fixed
- `Trip Entry` - New shipment didn't display a package row. 
- `Trip Entry` - Audit for a shipment loads.


## 2019-10-18
### Changed
- `PML Shipments` can now be loaded from the Search tab.  The items for uppc and uppack shipments are put into the trip's TripPackage.

## 2019-10-15
### Changed
- `Trip Entry` package handling has been rewritten to match Terry's new design.  Saving is still not completed.

## 2019-10-09
### Added
- Dialog for editing the contents of a selected TripItem is finished.  A listview displays the inventory of a selected TripItem, but doesn't allow changes to be made.  The selected inventory items _aren't_ saved yet.

## 2019-10-07
### Added
- Dialog for editing the contents of a selected TripItem. 
- basic UI works.

## 2019-10-07
### Added
- trw - Added filter to dispatch


## 2019-10-04
### Added
- trw - Added broadcasting to Trip Entry

## 2019-10-04
### Added
- Started to add Inventory grid to `Shipment Entry`.
- Edit button to Search Shipments.

### Changed
- Editing (either by button or right-click menu) a single trip loads it like double-clicking it.  Multiple trips are loaded into the Edit Trips dialog.

### Fixed
- Thread access bug in PML/Shipments.
- Loading the same trip in `Shipment Entry` sometimes hung my client.
- Cloning a trip sets the status to **NEW**;
- Fixed bug when cloning trips for pickup or delivery.


## 2019-10-03
### Added
- trw - Copy shipment id to clipboard in search trips
- trw - Added staff id to trip audit trail

`Shipment Entry` I2P-4

### Fixed
- CallDate and other date controls sometimes showed 01/01/0001.
- POP displayed, not POD.

### Changed
- Added missing fields newly added to db.

## 2019-09-30
`Address Book` I2P-1

### Fixed
- `Company Name` and `Location Barcode` *must* be unique ; user is warned if they aren't.
- Filter by Location Barcode works.

## 2019-09-27
`Address Book` I2P-1

### Fixed
- The filter's Prov/State dropdown only adds an empty line if there isn't one already.
- Clicking the `Edit` buttons or the `Edit` menu item without selecting an address no longer crashes.
- `Clear` didn't remove the error backgrounds from the required fields.
- Clicking `Modify Group` after cancelling a previous modify attempt didn't load the group until another group had been selected.
- Saving an address didn't necessarily save the address fields.
- Deleting an address now works, as long as one has been selected in the list.
- The `Prov/State` combo is populated with those of the country found in the first address.
- Deleting an address didn't delete it from any associated groups.  There was a bug in `CompanyAddressGroups.DeleteCompanyAddressGroupEntry` in `Azure Web Service`.
- The `Location Barcode` and `Contact Name` weren't being loaded, although they had been saved.

### Added
- Help button in toolbar.


## 2019-09-23

### Added
`Uplift Products`
- When any Products are added, updated, or deleted, any open Uplift Shipments tabs are updated.

## 2019-09-19
`Uplift Products`

### Added
- Finished the UI layout and buttons are largely wired up.
- PmlInventory items are loaded and can be edited.
- Filter (with multiple words) works.
- Saving and Deleting works.

## 2019-09-17
`Uplift Shipments`

### Added
- Added filter for `Package Type` combo box.  Works with multiple words.
- Added Help Toolbar item.
- Added shipment description to help users.

### Fixed 
- Problems when selecting a Product in ProductList.

## 2019-09-16
`Uplift Shipments`

### Added
- Shipments are created for single addresses and groups but are *not* saved.
- Addresses and groups can be selected.
- Company name and location barcode filtering works.
- Shipment Details fields are selectively enabled as the service level changes.
- Toolbar Clear button works.

## 2019-09-03
### Added
- Shipments page for uplifts.  Basic layout but no functionality.
- Products page for uplifts.  Basic layout but no functionality.

## 2019-08-30
### Changed
- Azure Web Services - Location barcode is saved when an address is updated.
- AddressBook - Location Barcodes are being saved and loaded.
- AddressBook - Addresses are saved with whatever groups are selected.  The list in the `Groups` is updated with the new count.


## 2019-08-29
### Added
- AddressBook Groups - Adding and removing addresses in a group works but no saving.
- AddressBook address list - Edit, Delete, Select All, and Select None buttons added and working.
- AddressBook Groups - Delete works.
- AddressBook Addresses - Member of Groups and Available Groups lists - Groups can be moved between the 2 lists but are not saved.


## 2019-08-23
### Changed
- Backed out the code i had written yesterday to get a list of groups as Terry rewrote this section.
- Groups are loaded and can be created.  No companies attached yet.

## 2019-08-21
`Address Book`

- Starting to modify to match the one in Uplift.  The new filters are in place.

## 2019-08-20
### Changed
`Search Trips `

- Redesigned the tools at the top and added Package Type, Service Level, Pickup Address, and Delivery Address.
- Reworked the enabling logic of the controls depending upon whether it is a date search or not.

## 2019-08-15
### Fixed
EditSelectedTrips - POD wasn't working and CallTime was always being set to Now.

### Changed
SearchTrips - when double-clicking a trip, the latest version is loaded from the db.  This is also done when using the context menu (Edit Selected Trips).
The backing list is also updated.

### Added
TripEntry - POP and POP signature.
EditSelectedTrips - POP name.

## 2019-08-13a
### Added
- New IsQuote, UndeliverableNotes fields to db.

## 2019-08-13
### Added
- New IsQuote, UndeliverableNotes fields to db.
- New CallerName field to db.
### Fixed
- Minor changes to Trip Entry and Account Details.
- Minor bugs in Search Trips.

## 2019-08-08

### Added
File menu has a `Logs` section which allow the user to view the current log file or open Explorer to the <MyDocuments>\Ids directory.
### Fixed
`Account Details`

For some reason, the method UpdateCompany in Company.cs was forcing the CompanyName to upper case.  


## 2019-08-07a
### Changed
`Contact Email` in `Address Book` is connected.

## 2019-08-07
### Added
`Address Book` - Notes field and context menu.
`Account Details` - Missing toolbar labels and partial context menu.

### Changed
`Contact Name` in `Address Book` is connected.  Had to modify ResellerCustomers.cs in Azure Web Service project.
	

## 2019-08-02
### Fixed
`Search Trips`

- Scrolling was broken for large results.
- `Clear` sets the from and to dates to today.

### Changes
`Trip Entry`

- Weight fields are restricted to floats ; pieces fields are restricted to integers.  

`Search Trips`

- `Clear` sets the from and to dates to today.
- `From` and `To` are set to the beginning and end of the selected days, respectively.

## 2019-08-01
### Changed
- Added `Totals` line to package types list.
- Only the last TripItem line has an 'Add' button.
- Totals are working in `Trip Entry`.


## 2019-07-30

### Fixed
`Trip Entry` 

- Call Time is now set properly.
- Notes fields accept 'Enter'.
- Notes fields wrap properly.
- Context menu for Save, New, and Clear.
- New addresses are created.

### Changed

- Added more space between toolbar sections and other tweaks.
- Toolbar buttons have been re-styled to minimise their Padding.  This appears in `Search Trips`, `Trip Entry`, `Accounts', 'Address Book`, and `Rate Wizard`.
- `Address Book` now starts as disabled until the accounts have been loaded.

## 2019-07-29
### Added
Help button to main menu.

### Changed
Moved `Rate Wizard` into the `File` menu.
`Rate Wizard` - Applying Gord's changes from **ML-269**.
`Main Menu` - Removed `Log Out` button, icons, and `Show Icons` context menu.

## 2019-07-26
### Changed
Added icon to `Select Waybill Printer` in `Shipment Entry`.

## 2019-07-24
### Changed
Login TextBoxes automatically select all text.
`Address Book`

Delete works.
Description is readonly and disabled when editing an address.
Updating works.

## 2019-07-19
### Changed
`Address Book` - moved the buttons into a ToolBar/Menu to match the other pages.

## 2019-07-18
### Fixed
The regions in `Account Details` are now loading correctly.


## 2019-07-17
### Changed
`Address Book`

Countries and Regions are loaded using case-insensitive methods in order to deal with dirty data.  Countries are also found by abbreviations.

New addresses are now saved and added to the list.

`Shipment Entry`

Problem - Nav is getting this message
2019-07-11 14:17:41.1753; Found 9 Entries; Method: GetAuditLines; File: TripEntryModel.cs; Line: 1891
Unhandled exception : Not enough quota is available to process this command
i've truncated `Entry.PartitionKey`, `Entry.Operation`, `Entry.Data` when constructing the AuditLine to 1024 characters each, as the error seems to suggest that Windows doesn't have enough room to handle the data.

## 2019-07-12
### Changed
`Export Addresses` in `Address Book` page works.
`Address Book` button and menu item in `Accounts` page works
`Address Book` validation is in place.

## 2019-07-11
### Changed
Removed `Select AddressBook` ; following Gord's design, there are only address books for individual accounts.

## 2019-07-10
### Changed
`Address Book`

The address list is now loaded with company names and addresses.
The address filter works.

## 2019-07-02
### Fixed
Pickup and Delivery company names are now loaded when a trip in Search is double-clicked.

## 2019-06-28
### Changed
`Account Details` is disabled until the account names have been loaded.  Followed Terry's technique in `TripEntry`.
Lots of minor changes.

`Trip Entry`  Lots of minor changes.


## 2019-06-21
### Fixed
`Account Details`

- New accounts are added to the list.
- The filter works.

- Open `Trip Entry` pages have their accounts list updated when an account is saved.

## 2019-06-20
### Fixed
New addresses in `Shipment Entry` are saved with the trip but aren't visible in the UI until a new page is loaded.
New Customers are saved in the `Account Details` page.


## 2019-06-14
### Fixed
Billing and Delivery company names weren't being selected when a trip was double-clicked in Search.

## 2019-06-13
###**ML-260**
### Changed
`Search Shipments`

- Converted top of window to toolbar, matching `Shipment Entry`.

### Added
- Help button that opens search entry url in a browser.

## 2019-06-12
### Changed
- Package Row fields auto-select text on focus.

###**ML-260**

### Changed
`Trip Entry`

- Tab title changed to `Shipment Entry`.
- New Trip icon changed.
- Find Trip icon and text changed.
- Swap icon changed.
- Address book icon changed.
- Trip changed to Shipment.

`Search Trips`

- Tab title changed to `Search Shipments`.

`Main Window` menu

- Changed `Trip` to `Shipment`.
- Changed `Boards` to `Dispatch`.

### Added
`Trip Entry`

- Help icon that opens shipment entry url in browser.

## 2019-06-11
### Added
- `Trip Entry` now creates and saves trips with trip items.  It also loads and displays trips with trip items.

## 2019-06-07
### Changed
- Regions and countries in Trip Entry are now combo boxes.
- Regions and countries in Account Details are now combo boxes.
- Saving an Account is beginning to work. 

### Added
- AccountId field is readonly if an account is loaded and editable if the page has been cleared.

## 2019-05-30
**Customer Account Details**

- Now loads PrimaryCompany for selected name.
- Clear works.


## 2019-05-29
### Added
- Customer `Account Details` page has been largely laid out.

## 2019-05-27
### Added
- Started on `Accounts` per Eddy's instructions.  Beginning page loads.
- Connected remaining menu items in `Trip Entry` to toolbar methods.
- `Accounts` menu is connected.


## 2019-05-24
### Added
- Started on `Multiple Package Types` in Trip Entry. 

## 2019-05-23
### Removed
- Per Gord - removed `Clipboard` toolbar section.

## 2019-05-22
### Added
- Beginnings of Address Book.

## 2019-05-21a
### Added
- `Reveal Password` button to login page.

## 2019-05-21
### Changed
- Clone, Clone Pickup, and Clone Return check for modified trips and offers to save them first.

## 2019-05-16
### Added
- Trip Entry's swap function works.
- There are tooltips for the buttons on the menubar.
- Clone, Clone Pickup, and Clone Return work.

## 2019-05-15a
### Added
- Trip Entry's `Find Trip` works.

## 2019-05-15
### Added
- Trip Audit is working, albeit with LOGIN data rather than TRIP history, because the latter doesn't exist yet.
## 2019-05-10a
### Fixed
- Double-clicking a trip in Search if there wasn't a Trip Entry tab open tried to open a new Search tab.
## 2019-05-10
### Added
- Basic tracking works.  Only pulls items from the trip record (trip creation, ready time, picked up, and delivered).

## 2019-05-07
### Added
- Weighbill loads for currently loaded trip.  The account is printed, but can't find the phone number yet.
### Fixed
- `Search Trips` date selection didn't work - don't know when it stopped.

## 2019-04-25
### Added
- `Rate Wizard` Service Levels and Package Types tabs are working.  The Model keeps track of pending changes and the UI is updated.

## 2019-04-23
- Finishing `Rate Wizard` Service Levels.  Saving isn't working yet.

## 2019-04-15
### Added
- `Rate Wizard` page opens.  Starting to build the interface.

## 2019-04-15
### Changed
- Connected GetServiceLevels to the server.  After populating the table, they are now returned and displayed.

## 2019-04-05
### Added
- There is a validation system in place for TripEntry.  The code is written up in **Notes_On_UI_Validation.md**.
- The call time widget shows 15 minute periods in the dropdown.
- There is a menu that replicates the toolbar buttons.  A context menu in the toolbar (or double-clicking it) hides the toolbar and shows the menu.  A `View` menu item shows the toolbar and hides the menu.
- Trips are created and updated.


## 2019-03-26a
### Added
- cjt Added a driver dropdown to TripEntry so that the trip can be dispatched immediately.

### Changed
- cjt Changed `Service` label in TripEntry to `Details`.


## 2019-03-26
### Added
- cjt PackageTypes are loaded either in their sort order or alphabetically.

## 2019-03-25
### Added
- cjt Toolbar to TripEntry using Gord's icons.
- cjt `New` button fetches a new tripid.  If the `Account` isn't empty, the user is asked if it just wants a new tripid.  This replicates IDS1 behaviour.
- cjt `Clear` works.


## 2019-02-22
### Added
- cjt Most of the basic fields in TripEntry are in place and being populated when a trip is loaded from the search screen.

## 2019-02-20
### Added
- cjt Trip details (status, driver, t-pu, t-del) at the top of the trip entry screen.

## 2019-02-19
### Added
- cjt Missed the Account column.
- cjt Address filter works.

## 2019-02-14
### Added
- cjt Nested a Filter for Addresses beneath the Company filter.  It is populated when the selected company changes.  The search doesn't make use of it yet.


## 2019-02-07
### Added
- cjt Trips that have one of the 4 (POD doesn't exist in the Trips table) fields modified are now updated in the ListView and saved to the database.  
- cjt Missed Calltime from the changeable fields.

## 2019-02-05a
### Added
- cjt Populated drivers dropdown with staff members that have a role of `Driver`.
- cjt Changing EditSearchTrips to have a 2-stage commit.
		1. First select the fields to change and `Select` them.
		2. Click `Ok` to apply the changes.

## 2019-02-05
### Added
- cjt Adding a dialog that allows the 4 editable (for the moment) fields in the search trips list to be modified.  It will be possible to modify multiple trips at once.

## 2019-02-01
### Added
- cjt When a SearchTrips tab is opened, it checks to see if there is a TripEntry tab open, and, if there isn't, it opens one.
- cjt TripId and AccountId are loaded in the Trip Entry tab.
- cjt Double-clicking a trip either opens a new Trip Entry tab if none are open or, goes to the first open one.  
### Changed
- cjt Rest of TextBoxes in TripEntry are attached to TextBoxesEditableEnable.  TripId is uneditable.

## 2019-01-31
### Added
- cjt New program `TripEntry` with menu item to create it.  Opens as a tab. 
- cjt Beginning of TripEntry layout according to `IDS2LiteJan52016.pdf`.

## 2019-01-30
### Changed
- cjt Reordering columns works.  Had to change headers from Buttons to GridViewColumnHeaders.

## 2019-01-29
### Added
- cjt Trip Search list has a context menu that allows the toggling of visible columns.  The list is automatically updated.

## 2019-01-24
### Changed
- cjt Reworking columns for trip search screen to match those in design document.  Some are deactivated because there are no matching columns in the Trips table.
- cjt Filtering only works with the visible columns.  The mechanism for selecting the columns hasn't been built yet.

## 2019-01-21
### Changed
- cjt Local logging is now flushed after every write.
- cjt Now 1 file per day is created.  **Caveat:** if the program is left running overnight, the previous day's log will still be used because the new log is created on startup. 
- cjt `Close` button on login screen is enabled so that the user can close the program without putting anything in the password field.

### Added
- cjt Utility method to Utils.Logging: WriteLogLine.  Replicates log line format from IdsDispatch.  It is improved, as it includes the source file name and the line number.
- cjt Added a `Filter` textbox to the search trips screen.  The filter is applied as typed.  It also keeps the current sort order.
- cjt Added `Clear` button to reset the screen.

## 2019-01-14
### Fixed
- cjt Bug in GetCompanyNames.
- cjt Added icon to project.

## 2019-01-08
### Changed
- cjt - Starting to modify Trip Search.  Added Start and End status combos.
