﻿using System;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Messaging
{
	public class Utils
	{
		public class AccessData
		{
			public string Uri,
			              KeyValue,
			              KeyName,
			              Token,
			              RestApiKey,
			              ResourceName,
			              SubscriptionId;
		}

		public static string GetSasToken( string baseAddress, string sasKeyName, string sasKeyValue )
		{
			var FromEpochStart = DateTime.UtcNow - new DateTime( 1970, 1, 1 );
			var Expiry = Convert.ToString( (int)FromEpochStart.TotalSeconds + 3600 );
			var StringToSign = WebUtility.UrlEncode( baseAddress ) + "\n" + Expiry;
			var Hmac = new HMACSHA256( Encoding.UTF8.GetBytes( sasKeyValue ) );

			var Signature = Convert.ToBase64String( Hmac.ComputeHash( Encoding.UTF8.GetBytes( StringToSign ) ) );
			var SasToken = string.Format( CultureInfo.InvariantCulture, "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}",
			                              WebUtility.UrlEncode( baseAddress ), WebUtility.UrlEncode( Signature ), Expiry, sasKeyName );
			return SasToken;
		}

		public static AccessData GetSasToken( AccessData accessData )
		{
			accessData.Token = GetSasToken("RootManageSharedAccessKey", accessData.KeyName, accessData.KeyValue );
			return accessData;
		}
	}

	public static class AzureExtensions
	{
		private const string END_POINT = "Endpoint=sb:",
		                     SHARED_KEY = "SharedAccessKey=",
		                     KEY_NAME = "SharedAccessKeyName=";

		public static Utils.AccessData SplitAzureKey( this string txt )
		{
			var Result = new Utils.AccessData();

			var Temp = txt.Split( ';' );
			foreach( var S in Temp )
			{
				if( S.StartsWith( END_POINT, StringComparison.InvariantCultureIgnoreCase ) )
					Result.Uri = "https:" + S.Substring( END_POINT.Length ).Trim();
				else if( S.StartsWith( SHARED_KEY, StringComparison.InvariantCultureIgnoreCase ) )
					Result.KeyValue = S.Substring( SHARED_KEY.Length ).Trim();
				else if( S.StartsWith( KEY_NAME, StringComparison.InvariantCultureIgnoreCase ) )
					Result.KeyName = S.Substring( KEY_NAME.Length ).Trim();
			}

			return Result;
		}

		public static Utils.AccessData SplitAzureKey( this string txt, string subscriptionId, string restApiKey, string resourceName )
		{
			var Temp = txt.SplitAzureKey();
			Temp.SubscriptionId = subscriptionId;
			Temp.RestApiKey = restApiKey;
			Temp.ResourceName = resourceName;
			return Temp;
		}
	}
}