﻿using System.Collections.Generic;

namespace Ids2aWpf
{
	internal static partial class Globals
	{
		public static partial class Terminologies
		{
			public static DictPath PmlTerminology => new DictPath
			                                         {
				                                         BasePath = "Pml",
				                                         Dictionaries = new List<string>()
			                                         };

			public const string PML = "PML";
		}
	}
}