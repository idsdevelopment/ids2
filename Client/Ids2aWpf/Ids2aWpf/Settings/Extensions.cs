﻿using System.Windows;
using System.Windows.Data;

namespace Ids2aWpf.Setting
{
	public class SettingBindingExtension : Binding
	{
		public SettingBindingExtension()
		{
			Initialize();
		}

		public SettingBindingExtension( string path ) : base( path )
		{
			Initialize();
		}

		private void Initialize()
		{
			Source = Properties.Settings.Default;
			Mode = BindingMode.TwoWay;
		}
	}
}