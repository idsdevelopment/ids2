﻿using System.Windows.Controls;

namespace ViewModels.AuditTrails
{
	/// <summary>
	///     Interaction logic for Trip.xaml
	/// </summary>
	public partial class Trip : Page
	{
		public Trip()
		{
			Initialized += ( sender, args ) =>
			               {
				               if( DataContext is TripViewModel Model )
				               {
					               Model.ExecuteAuditTrip = () =>
					                                        {
						                                        Viewer.GetLog();
					                                        };
				               }
			               };
			InitializeComponent();
		}
	}
}