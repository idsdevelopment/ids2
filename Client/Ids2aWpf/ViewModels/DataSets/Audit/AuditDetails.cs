﻿using Protocol.Data;
using System;
using System.Collections.Generic;

namespace ViewModels.Reporting
{
    public partial class DataSets
    {
        public class AuditDetails
        {
            private Trip trip;
            private DateTimeOffset date;
            private string user;
            private string action;
            private string description;

            public AuditDetails(Trip trip, DateTimeOffset date, string user, string action, string description)
            {
                this.trip = trip;
                this.date = date;
                this.user = user;
                this.action = action;
                this.description = description;
            }

            public AuditDetails(Trip trip, AuditLine line)
            {
                this.trip = trip;
                this.date = line.DateOfEvent;
                this.user = line.User;
                this.action = line.Action;
                this.description = line.Description;
            }

            public string TripId => "Audit Trail for item: " + this.trip.TripId;
            public string DateOfEvent => this.date.ToString("yyyy/MM/dd hh:mm tt");
            public string User => this.user;
            public string Action => this.action;
            public string Description => this.description;
        }


        public class AuditDetailsList : List<AuditDetails>
        {
            public AuditDetailsList(Trip trip, List<AuditLine> lines)
            {
                if (lines != null)
                {
                    foreach (AuditLine line in lines)
                    {
                        Add(new AuditDetails(trip, line));
                    }
                }
            }
        }
        public class AuditLine
        {
            private readonly DateTimeOffset date;
            private readonly string user;
            private readonly string action;
            private readonly string description;

            public AuditLine(DateTimeOffset date, string user, string action, string description)
            {
                this.date = date;
                this.user = user;
                this.action = action;
                this.description = description;
            }

            public DateTimeOffset DateOfEvent => this.date;
            public string User => this.user;
            public string Action => this.action;
            public string Description => this.description;

        }
    }
}
