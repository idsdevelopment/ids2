﻿using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModels.Reporting
{
    public partial class DataSets
    {
        public class TrackingDetails
        {
            private Trip trip;
            private DateTimeOffset date;
            private string location;
            private string description;

            // private Dictionary<DateTimeOffset, string> events;
            //private List<string> events = new List<string>();

            public TrackingDetails(Trip trip, DateTimeOffset date, string location, string description)
            {
                this.trip = trip;
                this.date = date;
                this.location = location;
                this.description = description;
            }

            public string TripId => "Detailed Trip Information for Trip " + this.trip.TripId;
            public string DateOfEvent => this.date.ToString("MM/dd/yyyy");
            public string TimeOfEvent => this.date.ToString("HH:mm");
            public string Location => this.location;
            public string Description => this.description;

        }

        /// <summary>
        /// Terry suggested this
        /// </summary>
        public class TrackingDetailsList : List<TrackingDetails>
        {
            public TrackingDetailsList(Trip trip)
            {
                if (trip != null)
                {
                    BuildEvents(trip);
                }
            }

            /// <summary>
            /// Creates the lines for display.
            /// </summary>
            private void BuildEvents(Trip trip)
            {
                //StringBuilder line = new StringBuilder();
                if (trip.CallTime > DateTimeOffset.MinValue)
                {
                    Add(new TrackingDetails(trip, trip.CallTime, FormatAddress(trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion), "Shipping details submitted"));
                }
                if (trip.ReadyTime > DateTimeOffset.MinValue)
                {
                    Add(new TrackingDetails(trip, trip.ReadyTime, FormatAddress(trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion), "Trip ready for pickup"));
                }
                // TODO
                //if (trip.DriverAssignTime > DateTimeOffset.MinValue)
                //{
                //    line.Clear();
                //    line.Append(trip.DriverAssignTime.ToString("yyyy-MM-dd"));
                //    line.Append("\t").Append(trip.DriverAssignTime.ToString("HH-mm"));
                //    events.Add(line.ToString());
                //}
                if (trip.Status1 >= STATUS.PICKED_UP && trip.Status1 < STATUS.DELETED)
                {
                    Add(new TrackingDetails(trip, trip.PickupTime, FormatAddress(trip.PickupCompanyName, trip.PickupAddressCity, trip.PickupAddressRegion), "Shipment picked up"));
                }
                if (trip.Status1 >= STATUS.DELIVERED && trip.Status1 < STATUS.DELETED)
                {
                    Add(new TrackingDetails(trip, trip.DeliveryTime, FormatAddress(trip.DeliveryCompanyName, trip.DeliveryAddressCity, trip.DeliveryAddressRegion), "Shipment delivered"));
                }

            }
            private string FormatAddress(string companyName, string city, string region)
            {
                StringBuilder sb = new StringBuilder(companyName).Append("\n");

                if (!string.IsNullOrEmpty(city))
                {
                    sb.Append(city).Append(" ");
                }
                if (!string.IsNullOrEmpty(region))
                {
                    sb.Append(", ").Append(region).Append(" ");
                }

                return sb.ToString();
            }
        }

    }
}
