﻿using AzureRemoteService;
using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;
using ViewModels.ReportBrowser;

namespace ViewModels.Reporting
{
    public partial class DataSets
    {
        public class LinfoxShipmentReportDetails
        {
            private ReportSettings reportSettings;
            //private List<Trip> trips = new List<Trip>();
            private TripList trips = new TripList();
            public LinfoxShipmentReportDetails(ReportSettings rs)
            {
                reportSettings = rs;
                LoadData();

                Logging.WriteLogLine("Found " + trips.Count + " trips");
            }

            private void LoadData()
            {
                SearchTrips();
            }

            private void SearchTrips()
            {
                Task.WaitAll(Task.Run(async () =>
                {
                    SearchTrips st = new Protocol.Data.SearchTrips
                    {
                        FromDate = reportSettings.SelectedFrom,
                        ToDate = reportSettings.SelectedTo,
                        ByTripId = false,
                        StartStatus = GetStatusForString(reportSettings.SelectedFromStatus),
                        EndStatus = GetStatusForString(reportSettings.SelectedToStatus),
                        CompanyCode = reportSettings.CompanyCode,
                    };
                    if (reportSettings.SelectedPackageType != ReportSettings.All)
                    {
                        st.PackageType = reportSettings.SelectedPackageType;
                    }
                    trips = await Azure.Client.RequestSearchTrips(st);
                }));
            }

            private STATUS GetStatusForString(string status)
            {
                STATUS result = STATUS.UNSET;

                switch (status)
                {
                    case "UNSET":
                        result = STATUS.UNSET;
                        break;
                    case "NEW":
                        result = STATUS.NEW;
                        break;
                    case "ACTIVE":
                        result = STATUS.ACTIVE;
                        break;
                    case "DISPATCHED":
                        result = STATUS.DISPATCHED;
                        break;
                    case "PICKED_UP":
                        result = STATUS.PICKED_UP;
                        break;
                    case "DELIVERED":
                        result = STATUS.DELIVERED;
                        break;
                    case "VERIFIED":
                        result = STATUS.VERIFIED;
                        break;
                    case "POSTED":
                        result = STATUS.POSTED;
                        break;
                    case "INVOICED":
                        result = STATUS.INVOICED;
                        break;
                    case "PAID":
                        result = STATUS.PAID;
                        break;
                    case "DELETED":
                        result = STATUS.DELETED;
                        break;
                }

                return result;
            }
        }

        /// <summary>
        /// Main entry.
        /// </summary>
        public class LinfoxShipmentReportDetailsList : List<LinfoxShipmentReportDetails>
        {
            public LinfoxShipmentReportDetailsList(ReportSettings reportSettings)
            {
                if (reportSettings != null)
                {
                    Logging.WriteLogLine("Building report: " + reportSettings.Description);

                    Add(new LinfoxShipmentReportDetails(reportSettings));
                }
                else
                {
                    Logging.WriteLogLine("ERROR : reportSettings is null");
                }
            }
        }
    }
}
