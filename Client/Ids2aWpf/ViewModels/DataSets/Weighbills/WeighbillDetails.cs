﻿using System.Collections.Generic;
using System;
using Protocol.Data;

namespace ViewModels.Reporting
{
	public partial class DataSets
	{
		//private static Random random = new Random();

		public class WeighbillDetails
		{
			private Trip trip;
			private int page;
			private bool isCOD;
			private string codAmount;
			private bool isShowTripIdPlusPieceCount;
			private string resellerName;
			private string resellerPhone;
			
			public WeighbillDetails(int _page)
			{
				this.page = _page;
			}

			public WeighbillDetails(int _page, Trip _trip, bool _isCOD, string _codAmount, bool _showTripIdPlusPieceCount, string _resellerName, string _resellerPhone)
			{
				this.page = _page;
				this.trip = _trip;
				this.isCOD = _isCOD;
				this.codAmount = _codAmount;
				this.isShowTripIdPlusPieceCount = _showTripIdPlusPieceCount;
				this.resellerName = _resellerName;
				this.resellerPhone = _resellerPhone;

				// this.CarrierName = "Intermountain Express Transport\n3059 West Parkway Blvd\nWest Valley City, UT. 84119";
			}

			private string GetFormattedPickupAddress()
			{
				string address = "";
				address += this.trip.PickupCompanyName + "\n";
				if (!string.IsNullOrEmpty(this.trip.PickupAddressSuite))
				{
					address += this.trip.PickupAddressSuite + " ";
				}

				address += this.trip.PickupAddressAddressLine1 + "\n";
				address += this.trip.PickupAddressCity + ", " + this.trip.PickupAddressRegion + ". " + this.trip.PickupAddressPostalCode;

				return address;
			}

			private string GetFormattedDeliveryAddress()
			{
				string address = "";
				address += this.trip.DeliveryCompanyName + "\n";
				if (!string.IsNullOrEmpty(this.trip.DeliveryAddressSuite))
				{
					address += this.trip.DeliveryAddressSuite + " ";
				}

				address += this.trip.DeliveryAddressAddressLine1 + "\n";
				address += this.trip.DeliveryAddressCity + ", " + this.trip.DeliveryAddressRegion + ". " + this.trip.DeliveryAddressPostalCode;

				return address;
			}

			private string GetShipmentDetails()
			{
				string details = "Date: " + this.trip.CallTime.ToString() + "\n"
					+ "Ref: " + this.trip.Reference + "\n"
					// + this.trip.pickupNotes + "\n"  // Removed 20170809 re: Eddy
					+ "Weight: " + this.trip.Weight;

				return details;
			}

			private string GetCODLabel()
			{
				string codLabel = string.Empty;
				if (this.isCOD)
				{
					codLabel = "COD";
				}

				return codLabel;
			}

			private string GetCODAmount()
			{
				string codAmount = string.Empty;
				if (this.isCOD)
				{
					float value;
					if (float.TryParse(this.codAmount, out value))
					{
						codAmount = String.Format("{0:C}", value);
					}
					else
					{
						codAmount = "$" + this.codAmount;
						if (codAmount.IndexOf(".") < 0)
						{
							codAmount += ".00";
						}
					}

				}

				return codAmount;
			}

			private string GetTripId()
			{
				string tripId = this.trip.TripId;
				if (this.isShowTripIdPlusPieceCount)
				{
					tripId += "-" + this.page;
				}

				return tripId;
			}

			private string GetBarcode()
			{
				string tripId = this.trip.TripId;
				if (this.isShowTripIdPlusPieceCount)
				{
					tripId += "-" + this.page;
				}

                // TODO Kludge
				//var server = Ids2Tracking.MainWindow.CURRENT_SERVER_NAME;
				var server = "jbtest2";
				tripId = "http://" + server + ".internetdispatcher.org:8080/barbecue/barcode?type=Code128&height=30&width=2&resolution=100&drawText=true&headless=false&data=" + tripId;

				// return "[" + tripId + "]";
				return tripId;
			}

			//private Random random = new Random();

			public string RandomItem => page + "";
			public string CarrierName = ""; // => "Intermountain Express Transport\n3059 West Parkway Blvd\nWest Valley City, UT. 84119"; // => this.trip.resellerId;
			public string PuAddress => this.GetFormattedPickupAddress(); // "From: Pickup Address";
			public string Service => this.trip.ServiceLevel; // "Service Type";
			public string PuContact => "Pickup Contact";
			public string DelContact => "Delivery Contact";
			public string DelPhone => "Delivery Phone";
			public string DelAddress => this.GetFormattedDeliveryAddress(); // "To: Delivery Address";
			public string ShipmentDetails => this.GetShipmentDetails();
			public string Calltime => this.trip.CallTime.ToString(); //"Date: Calltime";
			public string Weight => this.trip.Weight + ""; // Weight: 1.0";
			public string Barcode => this.GetBarcode(); // this.trip.tripId; // + "-" + this.page;
			public string Tripid => "Waybill #: " + this.GetTripId(); // this.trip.tripId; // + "-" + this.page;
			public string DelZone => this.trip.DeliveryZone; //"Delivery Zone";
			public string DelNotes => this.trip.DeliveryAddressNotes; // "Delivery Notes";
			public string PieceCount => "No. of Pieces: 1 of 1";
			public string ClientRef => "Ref: " + this.trip.Reference;
			public string Description => this.trip.PickupAddressNotes;
			public string CODLabel => this.GetCODLabel();
			public string CODAmount => this.GetCODAmount();

			public string ResellerName => this.resellerName;
			public string ResellerPhone => this.resellerPhone;

			public override string ToString()
			{
				// return base.ToString();
				return $"CARRIER_NAME: {CarrierName}\nPU_ADDRESS: {PuAddress}\nSERVICE: {Service}\nPU_CONTACT: {PuContact}\n";
			}
		}

		// Terry suggested having this
		public class WeighbillDetailsList : List<WeighbillDetails>
		{
/*
			public WeighbillDetailsList( IEnumerable<Trip> tripList )
			{
				foreach( var Trip in tripList )
					Add( new WeighbillDetails( Trip ) );
			}

			public WeighbillDetailsList( IEnumerable<WeighbillDetails> weighbillDetailsList )
			{
				foreach( var Wbd in weighbillDetailsList )
					Add( Wbd );
			}
*/
/*            public WeighbillDetailsList()
			{
				Add(new WeighbillDetails(1));
				Add(new WeighbillDetails(2));
				Add(new WeighbillDetails(3));
				Add(new WeighbillDetails(4));
			}

			public WeighbillDetailsList(int pageCount)
			{
				if (pageCount > 0)
				{
					for(int page = 0; page < pageCount; page++)
					{
						Add(new WeighbillDetails(page));
					}
				}
			}
*/

			public WeighbillDetailsList(Trip _trip, bool isCOD, string codAmount, bool showTripIdPlusPieceCount, string resellerName, string resellerPhone)
			{
				if (_trip != null)
				{
					for (int page = 1; page <= _trip.Pieces; page++)
					{
						Add(new WeighbillDetails(page, _trip, isCOD, codAmount, showTripIdPlusPieceCount, resellerName, resellerPhone));
					}
				}
			}
		}
	}
}