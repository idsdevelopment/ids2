﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using IdsControlLibraryV2.Virtualisation;
using Protocol.Data;
using Utils;

namespace ViewModels.Settings
{
	public class DisplayPreferences : Preference
	{
		public new bool Enabled
		{
			get => base.Enabled;
			set
			{
				base.Enabled = value;
				SetCurrent();
			}
		}

		public new string StringValue
		{
			get => base.StringValue;
			set
			{
				base.StringValue = value;
				SetCurrent();
			}
		}

		public new int IntValue
		{
			get => base.IntValue;
			set
			{
				base.IntValue = value;
				SetCurrent();
			}
		}

		public new DateTimeOffset DateTimeValue
		{
			get => base.DateTimeValue;
			set
			{
				base.DateTimeValue = value;
				SetCurrent();
			}
		}

		public new decimal DecimalValue
		{
			get => base.DecimalValue;
			set
			{
				base.DecimalValue = value;
				SetCurrent();
			}
		}

		public bool ShowBool => DataType == BOOL;
		public bool ShowString => DataType == STRING;
		public bool ShowInt => DataType == INT;
		public bool ShowDecimal => DataType == DECIMAL;
		public bool ShowDateTime => DataType == DATE_TIME;

		public FontWeight Bold => IdsOnly ? FontWeights.Bold : FontWeights.Normal;
		private readonly SettingsViewModel Owner;

		private void SetCurrent()
		{
			ViewModelBase.Dispatcher.InvokeAsync( () =>
			                                      {
				                                      Owner.SelectedPreferences = this;
			                                      } );
		}

		internal DisplayPreferences( SettingsViewModel owner, Preference preference ) : base( preference )
		{
			Owner = owner;
		}
	}

	public class OPreferences : AsyncObservableCollection<DisplayPreferences>
	{
		private readonly SettingsViewModel Owner;

		public void Add( Preference preference )
		{
			base.Add( new DisplayPreferences( Owner, preference ) );
		}

		public OPreferences( SettingsViewModel owner )
		{
			Owner = owner;
		}
	}

	public class SettingsViewModel : ViewModelBase
	{

		private readonly Dictionary<String, Preference> dictPreferences = new Dictionary<string, Preference>();
		private readonly Dictionary<String, Preference> dictChangedPreferences = new Dictionary<string, Preference>();

		public OPreferences Items
		{
			get { return Get( () => Items ); }
			set { Set( () => Items, value ); }
		}

		public OPreferences OrigItems
		{
			get { return Get(() => OrigItems); }
			set { Set(() => OrigItems, value); }
		}

		public DisplayPreferences SelectedPreferences
		{
			get { return Get( () => SelectedPreferences ); }
			set { Set( () => SelectedPreferences, value ); }
		}


		public string HelpUri
		{
			// TODO This needs to be updated to the correct url
			get { return Get(() => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/661192705/IDS+Core+Tutorials"); }
		}


		//[DependsUpon350( nameof( SelectedPreferences ) )]
		//public async Task WhenSelectedPreferencesChanges()
		//{
		//	await Azure.Client.RequestUpdatePreference( SelectedPreferences );
		//}

		[DependsUpon(nameof(SelectedPreferences))]
		public void WhenSelectedPreferencesChanges()
		{
			ArePendingChangesBool = true;
			Logging.WriteLogLine("Preference changed: " + SelectedPreferences.Description);
			dictChangedPreferences.Add(SelectedPreferences.Description, SelectedPreferences);
		}


		public SettingsViewModel()
		{
			var Temp = Items = new OPreferences( this );
			Task.Run( async () =>
			          {
				          Preferences Prefs;

				          if( !IsInDesignMode )
					          Prefs = await Azure.Client.RequestPreferences();
				          else
					          Prefs = new Preferences();

				          foreach( var Preference in Prefs )
						  {
							  Logging.WriteLogLine("Loading Preference: " + Preference.Description);
							  if (!dictPreferences.ContainsKey(Preference.Description))
							  {
								  dictPreferences.Add(Preference.Description, Preference);
							  }
							  else
							  {
								  Logging.WriteLogLine("ERROR - Skipping Preference: " + Preference.Description + " already found");
							  }
					          Temp.Add( Preference );
							  //OrigItems.Add(Preference);
						  }

						  OrigItems = new OPreferences(this);
						  foreach (var pref in Items)
						  {
							  OrigItems.Add(pref);
						  }
						  Logging.WriteLogLine("OrigItems.Count: " + OrigItems.Count);
					  } );
		}



		#region Save and Cancel


		public bool ArePendingChangesBool
		{
			get { return Get(() => ArePendingChangesBool, false); }
			set { Set(() => ArePendingChangesBool, value); }
		}
		
		[DependsUpon(nameof(ArePendingChangesBool))]
		public string ArePendingChangesString
		{
			get
			{				
				string visibility = "Collapsed";
				if (ArePendingChangesBool)
				{
					visibility = "Visible";
				}
				return visibility;
			}
		}

		[DependsUpon(nameof(ArePendingChangesBool))]
		public void WhenArePendingChangesChanges()
		{
			Logging.WriteLogLine("ArePendingChanges changed to " + ArePendingChangesBool);
		}

		public void Execute_Save()
		{
			string names = string.Empty;
			foreach (string key in dictChangedPreferences.Keys)
			{
				names += key + ", ";
			}
			names = names.Substring(0, names.Length - 2);

			Logging.WriteLogLine("Saving preferences: " + dictChangedPreferences.Count + " changed: " + names);
			//await Azure.Client.RequestUpdatePreference(SelectedPreferences);
			Task.Run(async () =>
			{
				if (!IsInDesignMode)
				{
					foreach (string key in dictChangedPreferences.Keys)
					{
						Logging.WriteLogLine("Saving preference: " + key);
						await Azure.Client.RequestUpdatePreference(dictChangedPreferences[key]);
					}
				}

				// Reload the backup collection for Execute_Cancel
				OrigItems = new OPreferences(this);
				foreach (var item in Items)
				{
					OrigItems.Add(item);
				}

				// KLUDGE - seems to be needed to get ArePendingChangesBool to wake up
				Items = new OPreferences(this);
				foreach (var item in OrigItems)
				{
					Items.Add(item);
				}

				// Finally, reset the collection and flag
				dictChangedPreferences.Clear();
				ArePendingChangesBool = false;
			});
		}

		public void Execute_Cancel()
		{
			Logging.WriteLogLine("Reverting to last-loaded preferences");
			
			Items = new OPreferences(this);
			foreach (var item in OrigItems)
			{
				Items.Add(item);
			}
			
			// Finally, reset the collection and flag
			dictChangedPreferences.Clear();
			ArePendingChangesBool = false;
		}

		#endregion
	}
}