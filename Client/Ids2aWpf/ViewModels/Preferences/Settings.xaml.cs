﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Utils;

namespace ViewModels.Settings
{
	public partial class Settings
	{
		public Settings()
		{
			InitializeComponent();
		}


		private void TextBox_TextChanged( object sender, TextChangedEventArgs e )
		{
			if( sender is TextBox Sender )
			{
				var Text = Sender.Text;

				var Txt = new StringBuilder();
				foreach( var C in Text )
				{
					if( ( C >= '0' ) && ( C <= '9' ) )
						Txt.Append( C );
				}
				Sender.Text = Txt.ToString();
				e.Handled = true;
			}
		}

		private void BtnHelp_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (DataContext is SettingsViewModel Model)
			{
				Logging.WriteLogLine("Opening " + Model.HelpUri);
				var uri = new Uri(Model.HelpUri);
				Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
			}
		}

		private void BtnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (DataContext is SettingsViewModel Model)
			{
				PreferenceListBox.ItemsSource = null;
				Model.Execute_Cancel();

				PreferenceListBox.ItemsSource = Model.Items;
			}
		}

		private void BtnSave_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			MessageBoxResult mbr = MessageBox.Show((string)Application.Current.TryFindResource("SettingsDialogSave"),
																	  (string)Application.Current.TryFindResource("SettingsDialogSaveTitle"),
																	  MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (mbr == MessageBoxResult.Yes)
			{
				if (DataContext is SettingsViewModel Model)
				{
					Model.Execute_Save();
				}
			}
		}

		private void Toolbar_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void MenuItem_Click_1(object sender, System.Windows.RoutedEventArgs e)
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void MenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			menuMain.Visibility = Visibility.Collapsed;
			toolbar.Visibility = Visibility.Visible;
		}
	}
}