﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Utils;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using Role = IdsRemoteServiceControlLibraryV2.Role;

namespace ViewModels.Staff
{
	public class MaintainStaffViewModel : ViewModelBase
	{
		private const string PROGRAM_NAME = "MaintainStaff";

		private bool InCheckId;
		private Protocol.Data.Staff OriginalStaff;
		private bool ReloadOld;

		private Timer ShowUpdateTimer;

		private void Clear()
		{
			SelectedStaff = null;

			Enabled = true;

			Password = "";

			FirstName = "";
			LastName = "";
			MiddleNames = "";

			Phone = "";
			Mobile = "";
			Email = "";

			Suite = "";
			Street = "";
			Street1 = "";
			City = "";
			Region = "";
			Country = "";
			PostalCode = "";

			EmergencyFirstName = "";
			EmergencyLastName = "";
			EmergencyPhone = "";
			EmergencyMobile = "";
			EmergencyEmail = "";

			MainCommission1 = 0;
			MainCommission2 = 0;
			MainCommission3 = 0;

			OverrideCommission1 = 0;
			OverrideCommission2 = 0;
			OverrideCommission3 = 0;
			OverrideCommission4 = 0;
			OverrideCommission5 = 0;
			OverrideCommission6 = 0;
			OverrideCommission7 = 0;
			OverrideCommission8 = 0;
			OverrideCommission9 = 0;
			OverrideCommission10 = 0;
			OverrideCommission11 = 0;
			OverrideCommission12 = 0;
		}

		protected override void OnInitialised()
		{
			DoEnableStaffNotes();
		}


		public string HelpUri
		{
			get { return Get(() => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638910469/Adding+new+Staff+Member"); }
		}

		public bool ShowZones
		{
			get { return Get( () => ShowZones, IsInDesignMode ); }
			set { Set( () => ShowZones, value ); }
		}


		public StaffLookupSummary StaffLookupSummary
		{
			get { return Get( () => StaffLookupSummary, new StaffLookupSummary() ); }
			set { Set( () => StaffLookupSummary, value ); }
		}

		[DependsUpon350( nameof( StaffLookupSummary ) )]
		public async Task WhenStaffLookupSummaryChanges()
		{
			if( !IsInDesignMode )
			{
				DisableDelay( nameof( DoEnableSave ) );
				EnableSave = false;
				DisableEvent( nameof( DoEnableSave ) );
				EnableDelay( nameof( DoEnableSave ) );

				var Rec = StaffLookupSummary;
				FirstName = Rec.FirstName;
				LastName = Rec.LastName;
				Street = Rec.AddressLine1;

				try
				{
					if( !ReloadOld || OriginalStaff.IsNull() )
						OriginalStaff = await Azure.Client.RequestGetStaffMember( StaffId );

					ReloadOld = false;
					var S = OriginalStaff;

					if( S.IsNotNull() && S.Ok )
					{
						var (Value, Ok) = Encryption.FromTimeLimitedToken( S.Password );
						if( Ok )
						{
							var A = S.Address;

							Dispatcher.Invoke( () =>
							                   {
								                   // Wait for password check to complete before enabling save
								                   WaitFor( new[]
								                            {
									                            new WaitForArg
									                            {
										                            MethodName = nameof( DoEnableSave ),
										                            PropertyNames = new[]
										                                            {
											                                            nameof( PasswordOk )
										                                            }
									                            }
								                            }
								                          );

								                   Enabled = S.Enabled;

								                   Password = Value;
								                   MiddleNames = S.MiddleNames;

								                   Phone = A.Phone;
								                   Mobile = A.Mobile;
								                   Email = A.EmailAddress;

								                   Suite = A.Suite;
								                   Street = A.AddressLine1;
								                   Street1 = A.AddressLine2;
								                   City = A.City;
								                   Region = A.Region;
								                   Country = A.Country;
								                   PostalCode = A.PostalCode;

								                   EmergencyFirstName = S.EmergencyFirstName;
								                   EmergencyLastName = S.EmergencyLastName;
								                   EmergencyPhone = S.EmergencyPhone;
								                   EmergencyMobile = S.EmergencyMobile;
								                   EmergencyEmail = S.EmergencyEmail;

								                   MainCommission1 = S.MainCommission1;
								                   MainCommission2 = S.MainCommission2;
								                   MainCommission3 = S.MainCommission3;

								                   OverrideCommission1 = S.OverrideCommission1;
								                   OverrideCommission2 = S.OverrideCommission2;
								                   OverrideCommission3 = S.OverrideCommission3;
								                   OverrideCommission4 = S.OverrideCommission4;
								                   OverrideCommission5 = S.OverrideCommission5;
								                   OverrideCommission6 = S.OverrideCommission6;
								                   OverrideCommission7 = S.OverrideCommission7;
								                   OverrideCommission8 = S.OverrideCommission8;
								                   OverrideCommission9 = S.OverrideCommission9;
								                   OverrideCommission10 = S.OverrideCommission10;
								                   OverrideCommission11 = S.OverrideCommission11;
								                   OverrideCommission12 = S.OverrideCommission12;
							                   } );
						}
					}
				}
				finally
				{
					EnableEvent( nameof( DoEnableSave ) );
				}
			}
		}


		public bool ShowUpdated
		{
			get { return Get( () => ShowUpdated, IsInDesignMode ); }
			set { Set( () => ShowUpdated, value ); }
		}

		[DependsUpon( nameof( ShowUpdated ) )]
		public void WhenShowUpdatedChanged()
		{
			if( ShowUpdated )
			{
				if( ShowUpdateTimer.IsNull() )
				{
					ShowUpdateTimer = new Timer( 5000 ) { AutoReset = false };
					ShowUpdateTimer.Elapsed += ( sender, args ) =>
					                           {
						                           Dispatcher.Invoke( () =>
						                                              {
							                                              ShowUpdated = false;
						                                              } );
					                           };
				}

				ShowUpdateTimer.Start();
			}
		}

		public bool InAdd
		{
			get { return Get( () => InAdd, IsInDesignMode ); }
			set { Set( () => InAdd, value ); }
		}


		public bool InEdit
		{
			get { return Get( () => InEdit, true ); }
			set { Set( () => InEdit, value ); }
		}


		public bool EnableSave
		{
			get { return Get( () => EnableSave, false ); }
			set { Set( () => EnableSave, value ); }
		}


		public bool EnableCancel
		{
			get { return Get( () => EnableCancel, false ); }
			set { Set( () => EnableCancel, value ); }
		}


		public string LookupStaffId
		{
			get { return Get( () => LookupStaffId, "" ); }
			set { Set( () => LookupStaffId, value ); }
		}

		public string StaffId
		{
			get { return Get( () => StaffId, "" ); }
			set { Set( () => StaffId, value ); }
		}

		[DependsUpon( nameof( StaffId ) )]
		public void WhenStaffIdChanges()
		{
			LookupStaffId = StaffId;
		}


		public string NewStaffId
		{
			get { return Get( () => NewStaffId, "" ); }
			set { Set( () => NewStaffId, value ); }
		}

		[DependsUpon350( nameof( NewStaffId ) )]
		public async Task WhenNewStaffIdChanges()
		{
			if( !IsInDesignMode && !InCheckId )
			{
				InCheckId = true;
				try
				{
					var HasId = await Azure.Client.RequestStaffMemberExists( NewStaffId );
					Dispatcher.Invoke( () =>
					                   {
						                   AlreadyCreated = HasId;
					                   } );
				}
				finally
				{
					InCheckId = false;
				}
			}
		}


		public string Password
		{
			get { return Get( () => Password, "" ); }
			set { Set( () => Password, value ); }
		}

		public string PasswordConfirmation
		{
			get { return Get(() => PasswordConfirmation, ""); }
			set { Set(() => PasswordConfirmation, value); }
		}

		public bool PasswordOk
		{
			get { return Get( () => PasswordOk, !IsInDesignMode ); }
			set { Set( () => PasswordOk, value ); }
		}

		[DependsUpon350( nameof( Password ) )]
		public async Task WhenPasswordChanges()
		{
			if( !IsInDesignMode )
			{
				var Ok = await Azure.Client.RequestIsPasswordValid( Encryption.ToTimeLimitedToken( Password ) );
				Dispatcher.Invoke( () =>
				                   {
					                   PasswordOk = Ok;
				                   } );
			}
		}

		public bool Enabled
		{
			get { return Get( () => Enabled, true ); }
			set { Set( () => Enabled, value ); }
		}

		public string FirstName
		{
			get { return Get( () => FirstName, "" ); }
			set { Set( () => FirstName, value ); }
		}

		public string LastName
		{
			get { return Get( () => LastName, "" ); }
			set { Set( () => LastName, value ); }
		}


		public string MiddleNames
		{
			get { return Get( () => MiddleNames, "" ); }
			set { Set( () => MiddleNames, value ); }
		}

		public string Phone
		{
			get { return Get( () => Phone, "" ); }
			set { Set( () => Phone, value ); }
		}

		public string Mobile
		{
			get { return Get( () => Mobile, "" ); }
			set { Set( () => Mobile, value ); }
		}

		public string Email
		{
			get { return Get( () => Email, "" ); }
			set { Set( () => Email, value ); }
		}


		public string Suite
		{
			get { return Get( () => Suite, "" ); }
			set { Set( () => Suite, value ); }
		}


		public string Street
		{
			get { return Get( () => Street, "" ); }
			set { Set( () => Street, value ); }
		}


		public string Street1
		{
			get { return Get( () => Street1, "" ); }
			set { Set( () => Street1, value ); }
		}

		public string City
		{
			get { return Get( () => City, "" ); }
			set { Set( () => City, value ); }
		}

		public string Region
		{
			get { return Get( () => Region, "" ); }
			set { Set( () => Region, value ); }
		}

		public string Country
		{
			get { return Get( () => Country, "" ); }
			set { Set( () => Country, value ); }
		}

		public string PostalCode
		{
			get { return Get( () => PostalCode, "" ); }
			set { Set( () => PostalCode, value ); }
		}


		public string EmergencyFirstName
		{
			get { return Get( () => EmergencyFirstName, "" ); }
			set { Set( () => EmergencyFirstName, value ); }
		}

		public string EmergencyLastName
		{
			get { return Get( () => EmergencyLastName, "" ); }
			set { Set( () => EmergencyLastName, value ); }
		}

		public string EmergencyPhone
		{
			get { return Get( () => EmergencyPhone, "" ); }
			set { Set( () => EmergencyPhone, value ); }
		}

		public string EmergencyMobile
		{
			get { return Get( () => EmergencyMobile, "" ); }
			set { Set( () => EmergencyMobile, value ); }
		}


		public string EmergencyEmail
		{
			get { return Get( () => EmergencyEmail, "" ); }
			set { Set( () => EmergencyEmail, value ); }
		}


		public decimal MainCommission1
		{
			get { return Get( () => MainCommission1, 0 ); }
			set { Set( () => MainCommission1, value ); }
		}

		public decimal MainCommission2
		{
			get { return Get( () => MainCommission2, 0 ); }
			set { Set( () => MainCommission2, value ); }
		}

		public decimal MainCommission3
		{
			get { return Get( () => MainCommission3, 0 ); }
			set { Set( () => MainCommission3, value ); }
		}


		public decimal OverrideCommission1
		{
			get { return Get( () => OverrideCommission1, 0 ); }
			set { Set( () => OverrideCommission1, value ); }
		}

		public decimal OverrideCommission2
		{
			get { return Get( () => OverrideCommission2, 0 ); }
			set { Set( () => OverrideCommission2, value ); }
		}

		public decimal OverrideCommission3
		{
			get { return Get( () => OverrideCommission3, 0 ); }
			set { Set( () => OverrideCommission3, value ); }
		}

		public decimal OverrideCommission4
		{
			get { return Get( () => OverrideCommission4, 0 ); }
			set { Set( () => OverrideCommission4, value ); }
		}

		public decimal OverrideCommission5
		{
			get { return Get( () => OverrideCommission5, 0 ); }
			set { Set( () => OverrideCommission5, value ); }
		}

		public decimal OverrideCommission6
		{
			get { return Get( () => OverrideCommission6, 0 ); }
			set { Set( () => OverrideCommission6, value ); }
		}

		public decimal OverrideCommission7
		{
			get { return Get( () => OverrideCommission7, 0 ); }
			set { Set( () => OverrideCommission7, value ); }
		}

		public decimal OverrideCommission8
		{
			get { return Get( () => OverrideCommission8, 0 ); }
			set { Set( () => OverrideCommission8, value ); }
		}

		public decimal OverrideCommission9
		{
			get { return Get( () => OverrideCommission9, 0 ); }
			set { Set( () => OverrideCommission9, value ); }
		}

		public decimal OverrideCommission10
		{
			get { return Get( () => OverrideCommission10, 0 ); }
			set { Set( () => OverrideCommission10, value ); }
		}

		public decimal OverrideCommission11
		{
			get { return Get( () => OverrideCommission11, 0 ); }
			set { Set( () => OverrideCommission11, value ); }
		}

		public decimal OverrideCommission12
		{
			get { return Get( () => OverrideCommission12, 0 ); }
			set { Set( () => OverrideCommission12, value ); }
		}


		public bool AlreadyCreated
		{
			get { return Get( () => AlreadyCreated, IsInDesignMode ); }
			set { Set( () => AlreadyCreated, value ); }
		}


		public bool EnableStaffNotes
		{
			get { return Get( () => EnableStaffNotes, false ); }
			set { Set( () => EnableStaffNotes, value ); }
		}

		[DependsUpon250( nameof( StaffId ) )]
		[DependsUpon( nameof( InEdit ) )]
		public void DoEnableStaffNotes()
		{
			EnableStaffNotes = InEdit && StaffId.IsNotNullOrWhiteSpace();
		}

		public Role ChangedRole
		{
			get { return Get( () => ChangedRole, new Role() ); }
			set { Set( () => ChangedRole, value ); }
		}


		public List<Protocol.Data.Role> SelectedRoles
		{
			get { return Get( () => SelectedRoles, new List<Protocol.Data.Role>() ); }
			set { Set( () => SelectedRoles, value ); }
		}


		public void Execute_AddStaff()
		{
			InAdd = true;
			InEdit = false;
			Clear();
			SetNewStaffFocus?.Invoke();
		}

		public async Task Execute_Cancel()
		{
			DisableDelay( nameof( DoEnableSave ) );
			InAdd = false;
			InEdit = true;
			AlreadyCreated = false;
			EnableSave = false;
			if( StaffId.IsNotNullOrWhiteSpace() )
			{
				DisableEvent( nameof( DoEnableSave ) ); // Re-enabled in WhenStaffLookupSummaryChanges
				ReloadOld = true;
				await WhenStaffLookupSummaryChanges();
				Refresh?.Invoke();
			}
			else
			{
				Clear();
				EnableDelay( nameof( DoEnableSave ) );
			}
		}

		public void Execute_Clear()
		{
			Clear();
		}

		//
		//
		//	Enable save
		//
		[DependsUpon( nameof( InAdd ) )]
		[DependsUpon( nameof( InEdit ) )]
		[DependsUpon( nameof( AlreadyCreated ) )]
		[DependsUpon( nameof( ChangedRole ) )]
		//
		[DependsUpon250( nameof( Enabled ) )] // Delay applies to all
		[DependsUpon( nameof( PasswordOk ) )]
		[DependsUpon( nameof( FirstName ) )]
		[DependsUpon( nameof( LastName ) )]
		[DependsUpon( nameof( MiddleNames ) )]
		[DependsUpon( nameof( Phone ) )]
		[DependsUpon( nameof( Mobile ) )]
		[DependsUpon( nameof( Email ) )]
		//
		[DependsUpon( nameof( Suite ) )]
		[DependsUpon( nameof( Street ) )]
		[DependsUpon( nameof( Street1 ) )]
		[DependsUpon( nameof( City ) )]
		[DependsUpon( nameof( Region ) )]
		[DependsUpon( nameof( Country ) )]
		[DependsUpon( nameof( PostalCode ) )]
		//
		[DependsUpon( nameof( EmergencyFirstName ) )]
		[DependsUpon( nameof( EmergencyLastName ) )]
		[DependsUpon( nameof( EmergencyPhone ) )]
		[DependsUpon( nameof( EmergencyMobile ) )]
		[DependsUpon( nameof( EmergencyEmail ) )]
		//
		[DependsUpon( nameof( MainCommission1 ) )]
		[DependsUpon( nameof( MainCommission2 ) )]
		[DependsUpon( nameof( MainCommission3 ) )]
		//
		[DependsUpon( nameof( OverrideCommission1 ) )]
		[DependsUpon( nameof( OverrideCommission2 ) )]
		[DependsUpon( nameof( OverrideCommission3 ) )]
		[DependsUpon( nameof( OverrideCommission4 ) )]
		[DependsUpon( nameof( OverrideCommission5 ) )]
		[DependsUpon( nameof( OverrideCommission6 ) )]
		[DependsUpon( nameof( OverrideCommission7 ) )]
		[DependsUpon( nameof( OverrideCommission8 ) )]
		[DependsUpon( nameof( OverrideCommission9 ) )]
		[DependsUpon( nameof( OverrideCommission10 ) )]
		[DependsUpon( nameof( OverrideCommission11 ) )]
		[DependsUpon( nameof( OverrideCommission12 ) )]
		public void DoEnableSave()
		{
			if( PasswordOk )
			{
				EnableSave = ( InEdit && !StaffId.IsNullOrEmpty() )
				             || ( InAdd && !AlreadyCreated && !NewStaffId.IsNullOrEmpty() && !FirstName.IsNullOrEmpty() && !LastName.IsNullOrEmpty() );

				EnableCancel = EnableSave || InAdd;
			}
			else
			{
				EnableSave = false;
				EnableCancel = true;
			}
		}

		public async Task Execute_Save()
		{
			if( !IsInDesignMode )
			{
				EnableSave = false;

				var UpdateData = new UpdateStaffMemberWithRolesAndZones( PROGRAM_NAME, new Protocol.Data.Staff
				                                                                       {
					                                                                       StaffId = InAdd ? NewStaffId : StaffId,
					                                                                       Enabled = Enabled,
					                                                                       Password = Encryption.ToTimeLimitedToken( Password ),

					                                                                       Address = new Address
					                                                                                 {
						                                                                                 Suite = Suite,
						                                                                                 AddressLine1 = Street,
						                                                                                 AddressLine2 = Street1,
						                                                                                 City = City,
						                                                                                 Country = Country,
						                                                                                 PostalCode = PostalCode,
						                                                                                 EmailAddress = Email,
						                                                                                 Phone = Phone,
						                                                                                 Mobile = Mobile,
						                                                                                 Region = Region
					                                                                                 },

					                                                                       EmergencyEmail = EmergencyEmail,
					                                                                       EmergencyFirstName = EmergencyFirstName,
					                                                                       EmergencyLastName = EmergencyLastName,
					                                                                       EmergencyMobile = EmergencyMobile,
					                                                                       EmergencyPhone = EmergencyPhone,
					                                                                       FirstName = FirstName,
					                                                                       LastName = LastName,
					                                                                       MiddleNames = MiddleNames,

					                                                                       MainCommission1 = MainCommission1,
					                                                                       MainCommission2 = MainCommission2,
					                                                                       MainCommission3 = MainCommission3,

					                                                                       OverrideCommission1 = OverrideCommission1,
					                                                                       OverrideCommission2 = OverrideCommission2,
					                                                                       OverrideCommission3 = OverrideCommission3,
					                                                                       OverrideCommission4 = OverrideCommission4,
					                                                                       OverrideCommission5 = OverrideCommission5,
					                                                                       OverrideCommission6 = OverrideCommission6,
					                                                                       OverrideCommission7 = OverrideCommission7,
					                                                                       OverrideCommission8 = OverrideCommission8,
					                                                                       OverrideCommission9 = OverrideCommission9,
					                                                                       OverrideCommission10 = OverrideCommission10,
					                                                                       OverrideCommission11 = OverrideCommission11,
					                                                                       OverrideCommission12 = OverrideCommission12
				                                                                       }
				                                                       )
				                 {
					                 Roles = SelectedRoles
				                 };

				await Azure.Client.RequestUpdateStaffMember( UpdateData );


				LoadStaff();

				Dispatcher.Invoke(() =>
				{

					string title = string.Empty;
					string message = string.Empty;
					if (SelectedStaff != null)
					{
						// Updated
						title = (string)Application.Current.TryFindResource("StaffUpdatedTitle");
						message = (string)Application.Current.TryFindResource("StaffUpdatedMessage");
						// MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
					}
					else
					{
						// New
						title = (string)Application.Current.TryFindResource("StaffAddedTitle");
						message = (string)Application.Current.TryFindResource("StaffAddedMessage");
					}
					message = message.Replace("@1", StaffId);
					MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);


					StaffId = string.Empty;
					Password = string.Empty;
					PasswordConfirmation = string.Empty;

					Clear();

				});

				ShowUpdated = true;
			}
		}

		public void Execute_Delete()
		{
			if (SelectedStaff != null)
			{
				SelectedStaff.Enabled = false;
				var UpdateData = new UpdateStaffMemberWithRolesAndZones(PROGRAM_NAME, SelectedStaff);

				if (!IsInDesignMode)
				{
					Task.Run(async () =>
					{
						await Azure.Client.RequestUpdateStaffMember(UpdateData);
					});

					Dispatcher.Invoke(() =>
					{
						StaffId = string.Empty;
						Password = string.Empty;
						PasswordConfirmation = string.Empty;

						Clear();

						LoadStaff();
					});
				}
			}
		}

		public Action Refresh;
		public Action SetNewStaffFocus;

		#region StaffList

		private List<Protocol.Data.Staff> StaffMembers = new List<Protocol.Data.Staff>();
		private readonly SortedDictionary<string, List<Protocol.Data.Role>> StaffMembersRoles = new SortedDictionary<string, List<Protocol.Data.Role>>();

		//public List<DisplayStaff> FilteredStaffList
		public List<Protocol.Data.Staff> FilteredStaffList
		{
			get { return Get(() => FilteredStaffList, LoadStaff); }
			set { Set(() => FilteredStaffList, value); }
		}

		//private List<DisplayStaff> LoadStaff()
		public List<Protocol.Data.Staff> LoadStaff()
		{
			//List<DisplayStaff> staff = new List<DisplayStaff>();
			List<Protocol.Data.Staff> staff = new List<Protocol.Data.Staff>();
			Dictionary<string, List<Protocol.Data.Role>> sdRoles = new Dictionary<string, List<Protocol.Data.Role>>();

			if (!IsInDesignMode)
			{
				Task.Run(async () =>
				{
					StaffList response = await Azure.Client.RequestGetStaff();
					if (response != null)
					{
						StaffMembersRoles.Clear();
						SortedDictionary<string, Protocol.Data.Staff> sd = new SortedDictionary<string, Protocol.Data.Staff>();
						foreach (var st in response)
						{
							Logging.WriteLogLine("Found staff member: " + st.StaffId);
							// Decrypt and store in the staff object
							var (Value, Ok) = Encryption.FromTimeLimitedToken(st.Password);
							if (Ok)
							{
								st.Password = Value;
								//Logging.WriteLogLine("with pwd: " + st.Password);
							}

							//var tmp = await Azure.Client.RequestGetStaffMember(st.StaffId);

							sd.Add(st.StaffId, st);

							var roles = await Azure.Client.RequestStaffMemberRoles(st.StaffId);
							if (roles != null)
							{
								List<Protocol.Data.Role> found = new List<Protocol.Data.Role>();
								foreach (var role in roles)
								{
									found.Add(role);
								}
								StaffMembersRoles.Add(st.StaffId, found);
							}
						}

						Dispatcher.Invoke(() =>
						{
							StaffMembers.Clear();
							List<Protocol.Data.Staff> tmp = new List<Protocol.Data.Staff>();

							foreach (var key in sd.Keys)
							{
								staff.Add(sd[key]);
								StaffMembers.Add(sd[key]);
								tmp.Add(sd[key]);
								

								//DisplayStaff ds = new DisplayStaff(sd[key]);
								//staff.Add(ds);
							}
							StaffMembers = tmp;

							IsFilterRoleShowAllSelected = true;
							IsFilterEnabledShowAllSelected = true;

							ApplyFilter("");

							//string title = string.Empty;
							//string message = string.Empty;
							//if (SelectedStaff != null)
							//{
							//	// Updated
							//	title = (string)Application.Current.TryFindResource("StaffUpdatedTitle");
							//	message = (string)Application.Current.TryFindResource("StaffUpdated");
							//}
							//else
							//{
							//	// New
							//	title = (string)Application.Current.TryFindResource("StaffAddedTitle");
							//	message = (string)Application.Current.TryFindResource("StaffAdded");
							//}
							//MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);

							//Clear();
						});
					}
				});
			}

			return staff;
		}


		public Protocol.Data.Staff SelectedStaff
		{
			get { return Get(() => SelectedStaff); }
			set { Set(() => SelectedStaff, value); }
		}

		[DependsUpon(nameof(SelectedStaff))]
		public void WhenSelectedStaffChanges()
		{
			if (SelectedStaff != null)
			{
				Logging.WriteLogLine("Selected Staff: " + SelectedStaff.StaffId);
			}
		}


		public string SelectedFilterRole
		{
			get { return Get(() => SelectedFilterRole, ""); }
			set { Set(() => SelectedFilterRole, value); }
		}


		[DependsUpon(nameof(SelectedFilterRole))]
		public void WhenSelectedFilterRoleChanges()
		{
			Logging.WriteLogLine("Selected filter role: " + SelectedFilterRole);
		}

		public ObservableCollection<string> AvailableRoles
		{
			get { return Get(() => AvailableRoles, LoadRoles); }
			set { Set(() => AvailableRoles, value); }
		}

		private ObservableCollection<string> LoadRoles()
		{
			ObservableCollection<string> roles = new ObservableCollection<string>();

			if (!IsInDesignMode)
			{
				Task.Run(async () =>
				{
					var rolesResponse = await Azure.Client.RequestRoles();
					if (rolesResponse != null)
					{
						SortedSet<string> sl = new SortedSet<string>();
						ObservableCollection<string> roles = new ObservableCollection<string>();
						foreach (var role in rolesResponse)
						{
							Logging.WriteLogLine("Found role: " + role.Name);
							sl.Add(role.Name);
						}
						foreach (string role in sl)
						{
							roles.Add(role);
						}
						AvailableRoles = roles;
					}
				});
			}

			return roles;
		}

		public void Execute_LoadStaffMember()
		{
			if (SelectedStaff != null)
			{
				StaffId = SelectedStaff.StaffId;
				Password = SelectedStaff.Password;
				// PasswordConfirmation = SelectedStaff.Password;
				FirstName = SelectedStaff.FirstName;
				LastName = SelectedStaff.LastName;
				MiddleNames = SelectedStaff.MiddleNames;

				Suite = SelectedStaff.Address.Suite;
				Street = SelectedStaff.Address.AddressLine1;
				Street1 = SelectedStaff.Address.AddressLine2;
				City = SelectedStaff.Address.City;
				Region = SelectedStaff.Address.Region;
				Country = SelectedStaff.Address.Country;
				PostalCode = SelectedStaff.Address.PostalCode;
				
				EmergencyFirstName = SelectedStaff.EmergencyFirstName;
				EmergencyLastName = SelectedStaff.EmergencyLastName;
				EmergencyPhone = SelectedStaff.EmergencyPhone;
				EmergencyMobile = SelectedStaff.EmergencyMobile;
				EmergencyEmail = SelectedStaff.EmergencyEmail;

				MainCommission1 = SelectedStaff.MainCommission1;
				MainCommission2 = SelectedStaff.MainCommission2;
				MainCommission3 = SelectedStaff.MainCommission3;
				OverrideCommission1 = SelectedStaff.OverrideCommission1;
				OverrideCommission2 = SelectedStaff.OverrideCommission2;
				OverrideCommission3 = SelectedStaff.OverrideCommission3;
				OverrideCommission4 = SelectedStaff.OverrideCommission4;
				OverrideCommission5 = SelectedStaff.OverrideCommission5;
				OverrideCommission6 = SelectedStaff.OverrideCommission6;
				OverrideCommission7 = SelectedStaff.OverrideCommission7;
				OverrideCommission8 = SelectedStaff.OverrideCommission8;
				OverrideCommission9 = SelectedStaff.OverrideCommission9;
				OverrideCommission10 = SelectedStaff.OverrideCommission10;
				OverrideCommission11 = SelectedStaff.OverrideCommission11;
				OverrideCommission12 = SelectedStaff.OverrideCommission12;


				EnableSave = true;
				EnableCancel = true;
			}
		}


		#endregion

		#region StaffList filter


		public bool IsFilterRoleShowAllSelected
		{
			get { return Get(() => IsFilterRoleShowAllSelected, true); }
			set { Set(() => IsFilterRoleShowAllSelected, value); }
		}


		public bool IsFilterRoleSelectSelected
		{
			get { return Get(() => IsFilterRoleSelectSelected, false); }
			set { Set(() => IsFilterRoleSelectSelected, value); }
		}


		public bool IsFilterEnabledShowAllSelected
		{
			get { return Get(() => IsFilterEnabledShowAllSelected, true); }
			set { Set(() => IsFilterEnabledShowAllSelected, value); }
		}


		public bool IsFilterEnabledShowEnabledSelected
		{
			get { return Get(() => IsFilterEnabledShowEnabledSelected, false); }
			set { Set(() => IsFilterEnabledShowEnabledSelected, value); }
		}

		public bool IsFilterEnabledShowDisabledSelected
		{
			get { return Get(() => IsFilterEnabledShowDisabledSelected, false); }
			set { Set(() => IsFilterEnabledShowDisabledSelected, value); }
		}

		[DependsUpon(nameof(IsFilterRoleShowAllSelected))]
		[DependsUpon(nameof(IsFilterRoleSelectSelected))]
		[DependsUpon(nameof(IsFilterEnabledShowAllSelected))]
		[DependsUpon(nameof(IsFilterEnabledShowEnabledSelected))]
		[DependsUpon(nameof(IsFilterEnabledShowDisabledSelected))]
		public void WhenFilterRadioButtonsChange()
		{
			Logging.WriteLogLine("Changing filter");

			if (IsFilterRoleShowAllSelected)
			{
				SelectedFilterRole = string.Empty;
			}
			else
			{
				if (SelectedFilterRole.IsNullOrWhiteSpace())
				{
					SelectedFilterRole = AvailableRoles[0];
				}
			}

			List<Protocol.Data.Staff> staff = new List<Protocol.Data.Staff>();
			foreach (var sm in StaffMembers)
			{
				if (TestMatch(sm)) {
					staff.Add(sm);
				}
			}

			FilteredStaffList = staff;
		}

		public void ApplyFilter(string filter)
		{
			Logging.WriteLogLine("Filtering list for " + filter);

			List<Protocol.Data.Staff> staff = new List<Protocol.Data.Staff>();
			foreach (var sm in StaffMembers)
			{
				if (sm.StaffId.ToLower().Contains(filter) || sm.FirstName.ToLower().Contains(filter))
				{
					if (TestMatch(sm))
					{
						staff.Add(sm);
					}
				}
			}

			FilteredStaffList = staff;
		}

		private bool TestMatch(Protocol.Data.Staff staff)
		{
			bool matches;
			bool matchesRole = false;
			bool matchesEnabled = false;

			if (staff.Enabled)
			{
				if (IsFilterEnabledShowAllSelected || IsFilterEnabledShowEnabledSelected)
				{
					matchesEnabled = true;
				}
			}
			else
			{
				if (IsFilterEnabledShowAllSelected || IsFilterEnabledShowDisabledSelected)
				{
					matchesEnabled = true;
				}
			}

			if (IsFilterRoleShowAllSelected)
			{
				matchesRole = true;
			}
			else
			{
				// What role is selected?
				// What roles does the user have?
				if (StaffMembersRoles.ContainsKey(staff.StaffId))
				{
					foreach (var role in StaffMembersRoles[staff.StaffId])
					{
						if (SelectedFilterRole.IsNotNullOrWhiteSpace() && role.Name == SelectedFilterRole)
						{
							matchesRole = true;
							break;
						}
					}
				}
			}

			matches = matchesRole && matchesEnabled;
			Logging.WriteLogLine(staff.StaffId + " matches: " + matches);

			return matches;
		}

		#endregion
	}

    /// <summary>
    /// Utility class for displaying in Staff tab list.
    /// </summary>
    public class DisplayStaff
	{
		public string StaffId = "StaffId";
		public string FullName = "FullName";

		public Protocol.Data.Staff staff = null;

		public DisplayStaff()
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="st"></param>
		public DisplayStaff(Protocol.Data.Staff st)
		{
			staff = st;
			StaffId = st.StaffId;
			if (st.FirstName.IsNotNullOrWhiteSpace())
			{
				FullName = st.FirstName.Trim() + " ";
			}
			if (st.LastName.IsNotNullOrWhiteSpace())
			{
				FullName += st.LastName.Trim();
			}
			Logging.WriteLogLine("Created new DisplayStaff: StaffId: " + StaffId + ", FullName: " + FullName);
		}
	}
}