﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Utils;
using ViewModels.Dialogues;

namespace ViewModels.Staff
{
	/// <summary>
	///     Interaction logic for MaintainStaff.xaml
	/// </summary>
	public partial class MaintainStaff : Page
	{
		public MaintainStaff()
		{
			Initialized += ( sender, args ) =>
			               {
				               if( DataContext is MaintainStaffViewModel M )
				               {
					               M.SetNewStaffFocus = () =>
					                                    {
						                                    tbStaffId.Focus();
					                                    };
					               M.Refresh = () =>
					                           {
						                           RoleListBox.Refresh();
					                           };
				               }							   
			               };

			InitializeComponent();

			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.LoadStaff();
				Model.ApplyFilter("");
			}

			rbStaffListShowAll_Checked(null, null);
			rbStaffListShowEnabledAll_Checked(null, null);
			rbStaffListShowAll.IsChecked = true;
			rbStaffListShowEnabledAll.IsChecked = true;

		}

		private bool Notes_OnBeforeDeleteNote( string noteName )
		{
			var Txt = Globals.Dictionary.AsString( "DeleteNote" );
			return new Confirm( $"{Txt}{noteName}" ).ShowDialog();
		}

		private void BtnHelp_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Logging.WriteLogLine("Opening " + Model.HelpUri);
				var uri = new Uri(Model.HelpUri);
				Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
			}
		}

		private void Toolbar_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void MenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			menuMain.Visibility = Visibility.Collapsed;
			toolbar.Visibility = Visibility.Visible;
		}

		private void MenuItem_Click_1(object sender, RoutedEventArgs e)
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void rbStaffListShowAll_Checked(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.IsFilterRoleShowAllSelected = true;
				Model.IsFilterRoleSelectSelected = false;
			}
		}

		private void rbStaffListShowAll_Unchecked(object sender, RoutedEventArgs e)
		{
			//if (DataContext is MaintainStaffViewModel Model)
			//{
			//	Model.IsFilterRoleShowAllSelected = false;
			//	Model.IsFilterRoleSelectSelected = true;
			//}
		}

		private void StaffListShowSelectRole_Checked(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.IsFilterRoleShowAllSelected = false;
				Model.IsFilterRoleSelectSelected = true;
			}
		}
		
		private void rbStaffListShowSelectRole_Unchecked(object sender, RoutedEventArgs e)
		{
			//if (DataContext is MaintainStaffViewModel Model)
			//{
			//	Model.IsFilterRoleShowAllSelected = true;
			//	Model.IsFilterRoleSelectSelected = false;
			//}
		}

		private void rbStaffListShowEnabledAll_Checked(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.IsFilterEnabledShowAllSelected = true;
				Model.IsFilterEnabledShowEnabledSelected = false;
				Model.IsFilterEnabledShowDisabledSelected = false;
			}
		}


		private void rbStaffListShowEnabledAll_Unchecked(object sender, RoutedEventArgs e)
		{
			//if (DataContext is MaintainStaffViewModel Model)
			//{
			//	Model.IsFilterEnabledShowAllSelected = false;
			//}
		}

		
		private void rbStaffListShowEnabledEnabled_Checked(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.IsFilterEnabledShowEnabledSelected = true;
				Model.IsFilterEnabledShowAllSelected = false;
				Model.IsFilterEnabledShowDisabledSelected = false;
			}
		}


		private void rbStaffListShowEnabledEnabled_Unchecked(object sender, RoutedEventArgs e)
		{
			//if (DataContext is MaintainStaffViewModel Model)
			//{
			//	Model.IsFilterEnabledShowEnabledSelected = false;
			//}
		}
		

		private void rbStaffListShowEnabledDisabled_Checked(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.IsFilterEnabledShowDisabledSelected = true;
				Model.IsFilterEnabledShowAllSelected = false;
				Model.IsFilterEnabledShowEnabledSelected = false;
			}
		}
		
		
		private void rbStaffListShowEnabledDisabled_Unchecked(object sender, RoutedEventArgs e)
		{
			//if (DataContext is MaintainStaffViewModel Model)
			//{
			//	Model.IsFilterEnabledShowDisabledSelected = false;
			//}
		}

		private void tbStaffListFilter_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.ApplyFilter(tbStaffListFilter.Text.Trim());
			}
		}

		private void tbStaffListFilter_LostFocus(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.ApplyFilter(tbStaffListFilter.Text.Trim().ToLower());
			}
		}

		private void btnStaffEdit_Click(object sender, RoutedEventArgs e)
		{
			lvStaff_MouseDoubleClick(null, null);
		}

		private void btnDeleteStaff_Click(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				string title = (string)Application.Current.TryFindResource("StaffDeletedConfirmationTitle");
				string message = (string)Application.Current.TryFindResource("StaffDeletedConfirmationMessage");

				string name = Model.StaffId + " (" + Model.SelectedStaff.FirstName + " " + Model.SelectedStaff.LastName + ")";
				message = message.Replace("@1", name);
				MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (mbr == MessageBoxResult.Yes)
				{
					Logging.WriteLogLine("Deleting name: " + name);
					Model.Execute_Delete();
					
					title = (string)Application.Current.TryFindResource("StaffDeletedTitle");
					message = (string)Application.Current.TryFindResource("StaffDeletedMessage");

					MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
				}
			}
			
		}

		private void lvStaff_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.Execute_LoadStaffMember();
				pbStaffPassword.Password = Model.Password;
				//var (Value, Ok) = Encryption.FromTimeLimitedToken(Model.Password);
				//if (Ok)
				//{
				//	pbStaffPassword.Password = Value;
				//	//tbStaffPassword.Text = Model.Password.ToString();
				//	pbStaffPasswordConfirmation.Password = Value;
				//	//tbStaffPasswordConfirmation.Text = Model.Password.ToString();
				//}

			}
		}

		private void cbRoles_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.WhenFilterRadioButtonsChange();
			}
		}

		private void imageRevealPassword_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.imageRevealPassword.Source = new BitmapImage(new Uri("eye-open-24.png", UriKind.RelativeOrAbsolute));
			this.tbStaffPassword.Text = this.pbStaffPassword.Password;
			this.pbStaffPassword.Visibility = Visibility.Hidden;
			this.tbStaffPassword.Visibility = Visibility.Visible;

			this.tbStaffPasswordConfirmation.Text = this.pbStaffPasswordConfirmation.Password;
			this.pbStaffPasswordConfirmation.Visibility = Visibility.Hidden;
			this.tbStaffPasswordConfirmation.Visibility = Visibility.Visible;
		}

		private void imageRevealPassword_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			this.imageRevealPassword.Source = new BitmapImage(new Uri("eye-closed-24.png", UriKind.RelativeOrAbsolute));
			this.tbStaffPassword.Text = this.pbStaffPassword.Password;
			this.pbStaffPassword.Visibility = Visibility.Visible;
			this.tbStaffPassword.Visibility = Visibility.Hidden;

			this.tbStaffPasswordConfirmation.Text = this.pbStaffPasswordConfirmation.Password;
			this.pbStaffPasswordConfirmation.Visibility = Visibility.Visible;
			this.tbStaffPasswordConfirmation.Visibility = Visibility.Hidden;
		}

		private void imageRevealPassword_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			imageRevealPassword_MouseLeave(null, null);
		}

		private void pbStaffPassword_PasswordChanged(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.Password = pbStaffPassword.Password;
			}
		}

		private void pbStaffPasswordConfirmation_PasswordChanged(object sender, RoutedEventArgs e)
		{
			if (DataContext is MaintainStaffViewModel Model)
			{
				Model.PasswordConfirmation = pbStaffPasswordConfirmation.Password;
			}
		}

		private void btnCancel1_Click(object sender, RoutedEventArgs e)
		{
			btnCancel_Click(sender, e);
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			tbStaffId.Text = "";
			pbStaffPassword.Password = "";
			pbStaffPasswordConfirmation.Password = "";

			ClearErrors();

			if (DataContext is MaintainStaffViewModel Model)
			{
				// Seems to be necessary to ensure that the SelectedRoles are cleared
				Model.StaffId = string.Empty;
			}
		}

		private async void btnSave1_Click(object sender, RoutedEventArgs e)
		{
			string errors = ValidateStaffMember();
			if (errors.IsNullOrWhiteSpace())
			{
				if (DataContext is MaintainStaffViewModel Model)
				{
					// Seems to be necessary to ensure that the SelectedRoles are cleared
					// Model.StaffId = string.Empty;
					await Model.Execute_Save();
				}

				tbStaffId.Text = "";
				pbStaffPassword.Password = "";
				pbStaffPasswordConfirmation.Password = "";

			}
			else
			{
				Logging.WriteLogLine("Errors found: " + errors);

				string title = (string)Application.Current.TryFindResource("StaffValidationTitle");
				string message = (string)Application.Current.TryFindResource("StaffValidationMessage") + "\r\n"
					+ (string)Application.Current.TryFindResource("StaffValidationMessage1") + "\r\n"
					+ errors;

				MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private string ValidateStaffMember()
		{
			StringBuilder errors = new StringBuilder();
			if (tbStaffId.Text.IsNullOrWhiteSpace())
			{
				errors.Append((string)Application.Current.TryFindResource("StaffValidationStaffIdEmpty")).Append("\r\n");
				tbStaffId.Background = Brushes.Yellow;
			}
			if (pbStaffPassword.Password.IsNullOrWhiteSpace())
			{
				errors.Append((string)Application.Current.TryFindResource("StaffValidationPasswordEmpty")).Append("\r\n");
				pbStaffPassword.Background = Brushes.Yellow;
			}
			if (pbStaffPasswordConfirmation.Password.IsNullOrWhiteSpace())
			{
				errors.Append((string)Application.Current.TryFindResource("StaffValidationPasswordConfirmationEmpty")).Append("\r\n");
				pbStaffPasswordConfirmation.Background = Brushes.Yellow;
			}
			if (pbStaffPassword.Password.IsNotNullOrWhiteSpace() && pbStaffPasswordConfirmation.Password.IsNotNullOrWhiteSpace())
			{
				if (pbStaffPassword.Password.Trim() != pbStaffPasswordConfirmation.Password.Trim())
				{
					errors.Append((string)Application.Current.TryFindResource("StaffValidationPasswordAndPasswordConfirmationDontMatch")).Append("\r\n");
					pbStaffPassword.Background = Brushes.Yellow;
					pbStaffPasswordConfirmation.Background = Brushes.Yellow;
				}
			}

			return errors.ToString();
		}

		private void ClearErrors()
		{
			tbStaffId.Background = Brushes.White;
			pbStaffPassword.Background = Brushes.White;
			pbStaffPasswordConfirmation.Background = Brushes.White;
		}
	}
}