﻿#nullable enable

using System;
using System.Threading;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;

namespace ViewModels.BaseViewModels
{
	public abstract class AMessagingViewModel : ViewModelBase, IDisposable
	{
		protected string                   UserName { get; set; }
		private   CancellationTokenSource? Token;

		private static async Task<Trip?> GetTrip( string tripId )
		{
			while( true )
			{
				try
				{
					return await Azure.Client.RequestGetTrip( new GetTrip
					                                          {
						                                          TripId = tripId
					                                          } );
				}
				catch
				{
					Thread.Sleep( 10_000 );
				}
			}
		}

		protected abstract void OnUpdateTrip( Trip trip );

		protected abstract void OnRemoveTrip( string tripId );

		protected abstract void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, bool receivedByDevice, bool readByDriver );

		protected virtual void Dispose( bool disposing )
		{
			if( !( Token is null ) )
			{
				Azure.Client.CancelTripUpdateListen( Token );
				Token = null;
			}
		}

		~AMessagingViewModel()
		{
			Dispose( false );
		}


		protected AMessagingViewModel( string board )
		{
			UserName = $"{Globals.CurrentUser.UserName}--{Guid.NewGuid():D}";

			if( !IsInDesignMode )
			{
				Azure.Client.ListenTripUpdate( board, UserName, async tripUpdateMessages =>
				                                                {
					                                                if( !( tripUpdateMessages is null ) )
					                                                {
						                                                foreach( var UpdateMessage in tripUpdateMessages )
						                                                {
							                                                switch( UpdateMessage.Action )
							                                                {
							                                                case TripUpdateMessage.UPDATE.TRIP_STATUS:
							                                                case TripUpdateMessage.UPDATE.DEVICE_STATUS:
								                                                foreach( var TripId in UpdateMessage.TripIdList )
									                                                OnUpdateStatus( TripId, UpdateMessage.Status, UpdateMessage.Status1, UpdateMessage.ReceivedByDevice, UpdateMessage.ReadByDriver );

								                                                break;

							                                                case TripUpdateMessage.UPDATE.TRIP:
								                                                foreach( var TripId in UpdateMessage.TripIdList )
								                                                {
									                                                var Trip = await GetTrip( TripId );

									                                                if( !( Trip is null ) )
										                                                OnUpdateTrip( Trip );
								                                                }

								                                                break;

							                                                case TripUpdateMessage.UPDATE.REMOVE:
								                                                foreach( var TripId in UpdateMessage.TripIdList )
									                                                OnRemoveTrip( TripId );

								                                                break;
							                                                }
						                                                }
					                                                }
				                                                } );
			}
		}

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}
	}
}