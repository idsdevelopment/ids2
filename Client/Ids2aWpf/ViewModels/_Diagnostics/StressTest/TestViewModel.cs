﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Protocol.Data;

namespace ViewModels._Diagnostics.StressTest
{
	public class TripDisplayEntry : Trip, INotifyPropertyChanged
	{
		public new string TripId
		{
			get => base.TripId;
			set
			{
				base.TripId = value;
				OnPropertyChanged();
			}
		}

		public STATUS Status
		{
			get => Status1;
			set
			{
				Status1 = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( StatusAsString ) );
			}
		}

		public new string Driver
		{
			get => base.Driver;
			set
			{
				base.Driver = value;
				OnPropertyChanged();
			}
		}

		public string StatusAsString => Status1.AsString();


		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class TestViewModel : ViewModelBase
	{
		private const string GO = "Go",
		                     STOP = "Stop";

		public ObservableCollection<string> Drivers
		{
			get { return Get( () => Drivers, new ObservableCollection<string>() ); }
			set { Set( () => Drivers, value ); }
		}


		public ObservableCollection<TripDisplayEntry> Trips
		{
			get { return Get( () => Trips, new ObservableCollection<TripDisplayEntry>() ); }
			set { Set( () => Trips, value ); }
		}


		public string GoButtonCaption
		{
			get { return Get( () => GoButtonCaption, GO ); }
			set { Set( () => GoButtonCaption, value ); }
		}


		public bool Running
		{
			get { return Get( () => Running, false ); }
			set { Set( () => Running, value ); }
		}

		private readonly Random DriverRand = new Random();

		private int MaxDriverRand;

		protected override void OnInitialised()
		{
			base.OnInitialised();
			Drivers = new ObservableCollection<string>();
			Trips   = new ObservableCollection<TripDisplayEntry>();

			Task.Run( async () =>
			          {
				          var Drvs = await Azure.Client.RequestGetDrivers();

				          MaxDriverRand = Drvs.Count;

				          Dispatcher.Invoke( () =>
				                             {
					                             foreach( var Drv in Drvs )
						                             Drivers.Add( Drv.StaffId );
				                             } );
			          } );
		}

		[DependsUpon( nameof( Running ) )]
		public void WhenRunningChanges()
		{
			if( Running )
			{
				GoButtonCaption = STOP;
				Trips           = new ObservableCollection<TripDisplayEntry>();

				Task.Run( async () =>
				          {
					          while( Running )
					          {
						          for( var I = 1000; I-- > 0; )
						          {
							          var TripId = await Azure.Client.RequestGetNextTripId();

							          var Ndx = DriverRand.Next( MaxDriverRand );
							          var Now = DateTimeOffset.Now;

							          var Trip = new TripDisplayEntry
							                     {
								                     BroadcastToDispatchBoard = true,
								                     TripId                   = TripId,
								                     Driver                   = Drivers[ Ndx ],
								                     PickupCompanyName        = "Pickup Company",
								                     DeliveryCompanyName      = "Delivery Company",
								                     DueTime                  = Now,
								                     ReadyTime                = Now,
								                     PickupZone               = "Pickup Zone",
								                     DeliveryZone             = "Delivery Zone",
								                     ServiceLevel             = "Service Level",
								                     Status1                  = STATUS.ACTIVE
							                     };

							          Dispatcher.Invoke( () =>
							                             {
								                             Trips.Add( Trip );
							                             } );

							          await Azure.Client.RequestAddUpdateTrip( Trip );
						          }

						          Running = false;
					          }
				          } );
			}
			else
				GoButtonCaption = GO;
		}
	}
}