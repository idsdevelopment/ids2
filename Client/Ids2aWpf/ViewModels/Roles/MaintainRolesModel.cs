﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Protocol.Data;
using Utils;

// ReSharper disable InconsistentNaming

namespace ViewModels.Roles
{
	public class CheckableRole : INotifyPropertyChanged
	{
		public bool IsRename => OriginalName.IsNotNullOrWhiteSpace() && ( Name != OriginalName );

		public string Name
		{
			get => _Name;
			set
			{
				_Name = value;
				OnPropertyChanged();
			}
		}

		public bool Checked
		{
			get => _Checked;
			set
			{
				_Checked = value;
				OnPropertyChanged();
			}
		}

		public bool IsAdmin
		{
			get => _IsAdmin;
			set
			{
				_IsAdmin = value;
				OnPropertyChanged();
			}
		}

		public bool IsDriver
		{
			get => _IsDriver;
			set
			{
				_IsDriver = value;
				OnPropertyChanged();
			}
		}

		private bool _Checked;

		private bool _IsAdmin;

		private bool _IsDriver;

		private string _Name;

		public MaintainRolesModel.OnAddRoleEvent OnAddRole;
		public OnChangedEvent OnChanged;

		public string OriginalName;
		public Role Role;

		public delegate Task OnChangedEvent( string propertyName, CheckableRole checkableRole );

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
			OnChanged?.Invoke( propertyName, this );
		}

		public CheckableRole( string name, string json )
		{
			OriginalName = name;
			Name         = name;
			Role         = json.IsNotNullOrWhiteSpace() ? Role.FromJson( json ) : new Role();
		}

		public CheckableRole( string name ) : this( name, "" )
		{
		}

		public CheckableRole( Protocol.Data.Role role ) : this( role.Name, role.JsonObject )
		{
			IsAdmin  = role.IsAdministrator;
			IsDriver = role.IsDriver;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}


	public class MaintainRolesModel : ViewModelBase
	{
		public ObservableCollection<CheckableRole> Roles
		{
			get { return Get( () => Roles, new ObservableCollection<CheckableRole>() ); }
			set { Set( () => Roles, value ); }
		}


		public CheckableRole SelectedRole
		{
			get { return Get( () => SelectedRole, new CheckableRole( "", "" ) ); }
			set { Set( () => SelectedRole, value ); }
		}

		public bool EnableReports
		{
			get { return Get( () => EnableReports, false ); }
			set { Set( () => EnableReports, value ); }
		}

		public bool EnableEdit
		{
			get { return Get( () => EnableEdit, false ); }
			set { Set( () => EnableEdit, value ); }
		}


		public bool EnableRateWizard
		{
			get { return Get( () => EnableRateWizard, false ); }
			set { Set( () => EnableRateWizard, value ); }
		}


		public bool EnableBoards
		{
			get { return Get( () => EnableBoards, false ); }
			set { Set( () => EnableBoards, value ); }
		}


		public bool EnableDriversBoard
		{
			get { return Get( () => EnableDriversBoard, false ); }
			set { Set( () => EnableDriversBoard, value ); }
		}


		public bool EnableDispatchBoard
		{
			get { return Get( () => EnableDispatchBoard, false ); }
			set { Set( () => EnableDispatchBoard, value ); }
		}


		public bool EnablePreferences
		{
			get { return Get( () => EnablePreferences, false ); }
			set { Set( () => EnablePreferences, value ); }
		}


		public bool EnableRoles
		{
			get { return Get( () => EnableRoles, false ); }
			set { Set( () => EnableRoles, value ); }
		}


		public bool EnableTrip
		{
			get { return Get( () => EnableTrip, false ); }
			set { Set( () => EnableTrip, value ); }
		}


		public bool EnableSearch
		{
			get { return Get( () => EnableSearch, false ); }
			set { Set( () => EnableSearch, value ); }
		}


		public bool EnableAudit
		{
			get { return Get( () => EnableAudit, false ); }
			set { Set( () => EnableAudit, value ); }
		}


		public bool EnableAuditStaff
		{
			get { return Get( () => EnableAuditStaff, false ); }
			set { Set( () => EnableAuditStaff, value ); }
		}


		public bool EnableAuditTrip
		{
			get { return Get( () => EnableAuditTrip, false ); }
			set { Set( () => EnableAuditTrip, value ); }
		}


		public bool EnableStaff
		{
			get { return Get( () => EnableStaff, false ); }
			set { Set( () => EnableStaff, value ); }
		}


		public bool EnableRoutes
		{
			get { return Get( () => EnableRoutes, false ); }
			set { Set( () => EnableRoutes, value ); }
		}

		public ICommand AddRole => Commands[ nameof( AddRole ) ];


		public bool EnableDeleteButton
		{
			get { return Get( () => EnableDeleteButton, false ); }
			set { Set( () => EnableDeleteButton, value ); }
		}


		public bool EnableLogs
		{
			get { return Get( () => EnableLogs, false ); }
			set { Set( () => EnableLogs, value ); }
		}


		public bool EnableViewCurrentLog
		{
			get { return Get( () => EnableViewCurrentLog, false ); }
			set { Set( () => EnableViewCurrentLog, value ); }
		}


		public bool EnableExploreLog
		{
			get { return Get( () => EnableExploreLog, false ); }
			set { Set( () => EnableExploreLog, value ); }
		}


		public bool EnableCustomers
		{
			get { return Get( () => EnableCustomers, false ); }
			set { Set( () => EnableCustomers, value ); }
		}


		public bool EnableCustomerAccounts
		{
			get { return Get( () => EnableCustomerAccounts, false ); }
			set { Set( () => EnableCustomerAccounts, value ); }
		}


		public bool EnableCustomerAddresses
		{
			get { return Get( () => EnableCustomerAddresses, false ); }
			set { Set( () => EnableCustomerAddresses, value ); }
		}


		public ICommand DeleteSelectedRoles => Commands[ nameof( DeleteSelectedRoles ) ];
		private bool InLoadMenu;

		public OnAddRoleEvent OnAddRole;

		public delegate void OnAddRoleEvent( CheckableRole newRole );

		private Role GetRole()
		{
			return new Role
			       {
				       CanView   = true,
				       CanCreate = true,
				       CanEdit   = true,
				       CanDelete = true,

				       Routes      = EnableRoutes,
				       Roles       = EnableRoles,
				       Trip        = EnableTrip,
				       Preferences = EnablePreferences,
				       Search      = EnableSearch,
				       Staff       = EnableStaff,

				       Reports = EnableReports,

				       Audit      = EnableAudit,
				       AuditStaff = EnableAuditStaff,
				       AuditTrip  = EnableAuditTrip,

				       Boards        = EnableBoards,
				       DriversBoard  = EnableDriversBoard,
				       DispatchBoard = EnableDispatchBoard,
				       RateWizard    = EnableRateWizard,

				       Logs       = EnableLogs,
				       ViewLog    = EnableViewCurrentLog,
				       ExploreLog = EnableExploreLog,

				       Customers         = EnableCustomers,
				       CustomerAccounts  = EnableCustomerAccounts,
				       CustomerAddresses = EnableCustomerAddresses,

				       Pml          = EnablePML,
				       PmlProducts  = EnablePML_Products,
				       PmlShipments = EnablePML_Shipments
			       };
		}

		private void SetupAdmin( bool isAdmin )
		{
			InLoadMenu = true;

			EnableEdit = !isAdmin;

			EnablePreferences = isAdmin;
			EnableRoles       = isAdmin;
			EnableTrip        = isAdmin;
			EnableSearch      = isAdmin;

			EnableReports = isAdmin;

			EnableAudit      = isAdmin;
			EnableAuditStaff = isAdmin;
			EnableAuditTrip  = isAdmin;

			EnableStaff  = isAdmin;
			EnableRoutes = isAdmin;

			EnableBoards        = isAdmin;
			EnableDriversBoard  = isAdmin;
			EnableDispatchBoard = isAdmin;
			EnableRateWizard    = isAdmin;

			EnableLogs           = isAdmin;
			EnableViewCurrentLog = isAdmin;
			EnableExploreLog     = isAdmin;

			EnableCustomers         = isAdmin;
			EnableCustomerAccounts  = isAdmin;
			EnableCustomerAddresses = isAdmin;

			EnablePML           = isAdmin;
			EnablePML_Products  = isAdmin;
			EnablePML_Shipments = isAdmin;

			InLoadMenu = false;
		}


		private async Task OnChanged( string propertyName, CheckableRole c )
		{
			if( !IsInDesignMode && c.Name.IsNotNullOrWhiteSpace() )
			{
				switch( propertyName )
				{
				case nameof( CheckableRole.IsAdmin ):
					SetupAdmin( c.IsAdmin );
					await WhenMenuChanges();

					break;

				case nameof( CheckableRole.Checked ):
					if( Roles.Any( Role => Role.Checked ) )
					{
						EnableDeleteButton = true;

						return;
					}

					EnableDeleteButton = false;

					break;

				default:
					if( c.IsRename )
					{
						await Azure.Client.RequestRenameRole( new RoleRename
						                                      {
							                                      NewName = c.Name,
							                                      Name    = c.OriginalName,

							                                      IsAdministrator = c.IsAdmin,
							                                      IsDriver        = c.IsDriver,
							                                      JsonObject      = GetRole().ToJson
						                                      } );
					}
					else
					{
						await Azure.Client.RequestAddUpdateRole( new Protocol.Data.Role
						                                         {
							                                         Name            = c.Name,
							                                         IsAdministrator = c.IsAdmin,
							                                         IsDriver        = c.IsDriver,
							                                         JsonObject      = GetRole().ToJson
						                                         } );
					}

					break;
				}
			}
		}

		[DependsUpon( nameof( SelectedRole ) )]
		public void WhenSelectedRoleChanges()
		{
			InLoadMenu = true;

			var Sr = SelectedRole;

			if( Sr.IsNotNull() && Sr.Role.IsNotNull() )
			{
				EnableEdit = true;
				var R = Sr.Role;

				EnablePreferences = R.Preferences;
				EnableRoles       = R.Roles;

				EnableTrip = R.Trip;

				EnableSearch = R.Search;

				EnableReports = R.Reports;

				EnableAudit      = R.Audit;
				EnableAuditStaff = R.AuditStaff;
				EnableAuditTrip  = R.AuditTrip;

				EnableStaff  = R.Staff;
				EnableRoutes = R.Routes;

				EnableBoards        = R.Boards;
				EnableDispatchBoard = R.DispatchBoard;
				EnableDriversBoard  = R.DriversBoard;

				EnableRateWizard = R.RateWizard;

				EnableLogs           = R.Logs;
				EnableViewCurrentLog = R.ViewLog;
				EnableExploreLog     = R.ExploreLog;

				EnableCustomers         = R.Customers;
				EnableCustomerAccounts  = R.CustomerAccounts;
				EnableCustomerAddresses = R.CustomerAddresses;

				EnablePML           = R.Pml;
				EnablePML_Products  = R.PmlProducts;
				EnablePML_Shipments = R.PmlShipments;
			}
			else
			{
				EnableEdit = false;

				EnablePreferences = false;
				EnableRoles       = false;

				EnableTrip   = false;
				EnableSearch = false;

				EnableReports = false;

				EnableAudit      = false;
				EnableAuditStaff = false;
				EnableAuditTrip  = false;

				EnableStaff  = false;
				EnableRoutes = false;

				EnableBoards        = false;
				EnableDispatchBoard = false;
				EnableDriversBoard  = false;

				EnableRateWizard = false;

				EnableLogs           = false;
				EnableViewCurrentLog = false;
				EnableExploreLog     = false;

				EnableCustomers         = false;
				EnableCustomerAccounts  = false;
				EnableCustomerAddresses = false;

				EnablePML           = false;
				EnablePML_Products  = false;
				EnablePML_Shipments = false;
			}

			InLoadMenu = false;
		}

		[DependsUpon( nameof( EnablePreferences ) )]
		[DependsUpon( nameof( EnableRoles ) )]
		[DependsUpon( nameof( EnableTrip ) )]
		[DependsUpon( nameof( EnableSearch ) )]
		[DependsUpon( nameof( EnableReports ) )]
		[DependsUpon( nameof( EnableAudit ) )]
		[DependsUpon( nameof( EnableAuditStaff ) )]
		[DependsUpon( nameof( EnableAuditTrip ) )]
		[DependsUpon( nameof( EnableStaff ) )]
		[DependsUpon( nameof( EnableRoutes ) )]
		[DependsUpon( nameof( EnableBoards ) )]
		[DependsUpon( nameof( EnableDriversBoard ) )]
		[DependsUpon( nameof( EnableDispatchBoard ) )]
		[DependsUpon( nameof( EnableRateWizard ) )]
		[DependsUpon( nameof( EnableLogs ) )]
		[DependsUpon( nameof( EnableViewCurrentLog ) )]
		[DependsUpon( nameof( EnableExploreLog ) )]
		[DependsUpon( nameof( EnableCustomers ) )]
		[DependsUpon( nameof( EnableCustomerAccounts ) )]
		[DependsUpon( nameof( EnableCustomerAddresses ) )]
		[DependsUpon( nameof( EnablePML ) )]
		[DependsUpon( nameof( EnablePML_Products ) )]
		[DependsUpon( nameof( EnablePML_Shipments ) )]
		public async Task WhenMenuChanges()
		{
			if( !InLoadMenu )
			{
				var Sr = SelectedRole;
				var R  = SelectedRole.Role;

				R.Preferences = EnablePreferences;
				R.Roles       = EnableRoles;
				R.Trip        = EnableTrip;
				R.Search      = EnableSearch;

				R.Reports = EnableReports;

				R.Audit      = EnableAudit;
				R.AuditStaff = EnableAuditStaff;
				R.AuditTrip  = EnableAuditTrip;

				R.Staff         = EnableStaff;
				R.Routes        = EnableRoutes;
				R.Boards        = EnableBoards;
				R.DriversBoard  = EnableDriversBoard;
				R.DispatchBoard = EnableDispatchBoard;
				R.RateWizard    = EnableRateWizard;

				R.Logs       = EnableLogs;
				R.ViewLog    = EnableViewCurrentLog;
				R.ExploreLog = EnableExploreLog;

				R.Customers         = EnableCustomers;
				R.CustomerAccounts  = EnableCustomerAccounts;
				R.CustomerAddresses = EnableCustomerAddresses;

				R.Pml          = EnablePML;
				R.PmlProducts  = EnablePML_Products;
				R.PmlShipments = EnablePML_Shipments;

				await OnChanged( "", Sr );
			}
		}

		public void Execute_AddRole()
		{
			var R = new CheckableRole( "" ) { OnChanged = OnChanged };
			Roles.Add( R );
			SelectedRole = R;
			OnAddRole?.Invoke( R );
		}

		public async Task Execute_DeleteSelectedRoles()
		{
			var DRoles = new DeleteRoles();
			var R      = Roles;

			for( var I = R.Count; --I >= 0; )
			{
				var Rl = R[ I ];

				if( Rl.Checked )
				{
					DRoles.Add( Rl.Name );
					R.RemoveAt( I );
				}

				await Azure.Client.RequestDeleteRoles( DRoles );
			}
		}

		public MaintainRolesModel()
		{
			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          var Temp = await Azure.Client.RequestRoles();

					          var Rls = new ObservableCollection<CheckableRole>();

					          foreach( var Role in Temp )
					          {
						          if( !Role.Mandatory )
						          {
							          var R = new CheckableRole( Role ) { OnChanged = OnChanged };
							          Rls.Add( R );
						          }
					          }

					          Dispatcher.Invoke( () =>
					                             {
						                             Roles = Rls;
					                             } );
				          } );
			}
		}

		#region Pml


		public bool IsPml
		{
			get { return Get( () => IsPml, false ); }
			set { Set( () => IsPml, value ); }
		}

		public bool EnablePML
		{
			get { return Get( () => EnablePML, false ); }
			set { Set( () => EnablePML, value ); }
		}


		public bool EnablePML_Products
		{
			get { return Get( () => EnablePML_Products, false ); }
			set { Set( () => EnablePML_Products, value ); }
		}


		public bool EnablePML_Shipments
		{
			get { return Get( () => EnablePML_Shipments, false ); }
			set { Set( () => EnablePML_Shipments, value ); }
		}

	#endregion
	}
}