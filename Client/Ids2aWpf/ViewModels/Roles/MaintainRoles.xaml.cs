﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ViewModels.Roles
{
    /// <summary>
    /// Interaction logic for MaintainRoles.xaml
    /// </summary>
    public partial class MaintainRoles : Page
    {
	
        public MaintainRoles()
        {
	        Loaded += ( sender, args ) =>
	                  {
		                  if( DataContext is MaintainRolesModel Model )
		                  {
			                  Model.OnAddRole = newRole =>
			                                    {
													Dg.ScrollIntoView( newRole );
			                                    };
		                  }
	                  };

            InitializeComponent();
	
        }

        private void Dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
