﻿#nullable enable

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.ViewModel;
using IdsUpdater;
using Utils;
using ViewModels.StartPage;

namespace ViewModels.MainWindow
{
	public partial class MainWindowModel : ViewModelBase
	{
		public enum LANGUAGE
		{
			DEFAULT,
			AUSTRALIAN,
			CANADIAN,
			US
		}

		public bool IsIds
		{
			get { return Get( () => IsIds, IsInDesignMode ); }
			set { Set( () => IsIds, value ); }
		}

		public string VersionNumber
		{
			get { return Get( () => VersionNumber, Globals.CurrentVersion.IDS_VERSION ); }
			set { Set( () => VersionNumber, value ); }
		}

		public string HelpUri
		{
			get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/overview" ); }
		}

		public string Slot
		{
			get { return Get( () => Slot, "" ); }
			set { Set( () => Slot, value ); }
		}


		[Setting]
		public bool ShowIcons
		{
			get { return Get( () => ShowIcons, false ); }
			set { Set( () => ShowIcons, value ); }
		}


		public bool RolesVisible
		{
			get { return Get( () => RolesVisible, true ); }
			set { Set( () => RolesVisible, value ); }
		}


		public bool PreferencesVisible
		{
			get { return Get( () => PreferencesVisible, true ); }
			set { Set( () => PreferencesVisible, value ); }
		}


		public bool ReportsVisible
		{
			get { return Get( () => ReportsVisible, true ); }
			set { Set( () => ReportsVisible, value ); }
		}


		public bool AuditVisible
		{
			get { return Get( () => AuditVisible, true ); }
			set { Set( () => AuditVisible, value ); }
		}


		public bool AuditStaffVisible
		{
			get { return Get( () => AuditStaffVisible, true ); }
			set { Set( () => AuditStaffVisible, value ); }
		}


		public bool AuditTripVisible
		{
			get { return Get( () => AuditTripVisible, true ); }
			set { Set( () => AuditTripVisible, value ); }
		}


		public bool StaffVisible
		{
			get { return Get( () => StaffVisible, true ); }
			set { Set( () => StaffVisible, value ); }
		}


		public bool TripsVisible
		{
			get { return Get( () => TripsVisible, true ); }
			set { Set( () => TripsVisible, value ); }
		}


		public bool SearchVisible
		{
			get { return Get( () => SearchVisible, true ); }
			set { Set( () => SearchVisible, value ); }
		}


		public bool RoutesVisible
		{
			get { return Get( () => RoutesVisible, true ); }
			set { Set( () => RoutesVisible, value ); }
		}

		public bool BoardsVisible
		{
			get { return Get( () => BoardsVisible, true ); }
			set { Set( () => BoardsVisible, value ); }
		}

		public bool DispatchBoardVisible
		{
			get { return Get( () => DispatchBoardVisible, true ); }
			set { Set( () => DispatchBoardVisible, value ); }
		}

		public bool DriversBoardVisible
		{
			get { return Get( () => DriversBoardVisible, true ); }
			set { Set( () => DriversBoardVisible, value ); }
		}


		public bool RateWizardVisible
		{
			get { return Get( () => RateWizardVisible, true ); }
			set { Set( () => RateWizardVisible, value ); }
		}


		public bool LogsVisible
		{
			get { return Get( () => LogsVisible, true ); }
			set { Set( () => LogsVisible, value ); }
		}


		public bool ViewLogsVisible
		{
			get { return Get( () => ViewLogsVisible, true ); }
			set { Set( () => ViewLogsVisible, value ); }
		}

		public bool ExploreLogsVisible
		{
			get { return Get( () => ExploreLogsVisible, true ); }
			set { Set( () => ExploreLogsVisible, value ); }
		}


		public bool CustomersVisible
		{
			get { return Get( () => CustomersVisible, true ); }
			set { Set( () => CustomersVisible, value ); }
		}


		public bool CustomerAccountsVisible
		{
			get { return Get( () => CustomerAccountsVisible, true ); }
			set { Set( () => CustomerAccountsVisible, value ); }
		}


		public bool CustomerAddressesVisible
		{
			get { return Get( () => CustomerAddressesVisible, true ); }
			set { Set( () => CustomerAddressesVisible, value ); }
		}

		public LANGUAGE Language
		{
			get { return Get( () => Language, LANGUAGE.DEFAULT ); }
			set { Set( () => Language, value ); }
		}


		[Setting]
		public bool LanguageDefault
		{
			get { return Get( () => LanguageDefault, true ); }
			set { Set( () => LanguageDefault, value ); }
		}

		[Setting]
		public bool LanguageAustralian
		{
			get { return Get( () => LanguageAustralian, false ); }
			set { Set( () => LanguageAustralian, value ); }
		}


		[Setting]
		public bool LanguageCanadian
		{
			get { return Get( () => LanguageCanadian, false ); }
			set { Set( () => LanguageCanadian, value ); }
		}


		[Setting]
		public bool LanguageUs
		{
			get { return Get( () => LanguageUs, false ); }
			set { Set( () => LanguageUs, value ); }
		}

		public PageTabControl TabControl
		{
			get { return Get( () => TabControl ); }
			set { Set( () => TabControl, value ); }
		}

		public string UserName
		{
			get { return Get( () => UserName ); }
			set { Set( () => UserName, value ); }
		}

		public string Account
		{
			get { return Get( () => Account ); }
			set { Set( () => Account, value ); }
		}


		public bool UpdateAvailable
		{
			get { return Get( () => UpdateAvailable, IsInDesignMode ); }
			set { Set( () => UpdateAvailable, value ); }
		}

		public ObservableCollection<PageTabItem> ProgramTabItems
		{
			get { return Get( () => ProgramTabItems ); }
			set { Set( () => ProgramTabItems, value ); }
		}

		public ICommand Install => Commands[ nameof( Install ) ];

		private Updater? Updater;

		private readonly ObservableCollection<PageTabItem> ProgramTabs = new ObservableCollection<PageTabItem>();

		private bool InLanguageChange;

		private bool InLanguageChangeEvent;

		public OnChangeLanguageEvent? OnLanguageChange;

		public OnLoginEvent? OnLogin;

		public OnSignOutEvent? OnSignOut,
		                       OnExit;

		public delegate void OnChangeLanguageEvent( LANGUAGE language );

		public delegate void OnLoginEvent( string companyName, string userName );

		public delegate void OnSignOutEvent();

		private ( PageTabItem? Tab, bool Running) IsProgramRunning( string pgmName )
		{
			foreach( var Tab in ProgramTabs )
			{
				if( Tab.Tag is string Prog && ( Prog == pgmName ) )
					return ( Tab, true );
			}

			return ( null, false );
		}


		private (PageTabItem? Tab, Page? Page) RunProgram( string pgmName, bool isPrimary = true )
		{
			try
			{
				if( Globals.Programs.TryGetValue( pgmName, out var Pgm ) )
				{
					Logging.WriteLogLine( $"\r\n----------------------------------------------------------------\r\nStarted program: {pgmName}" );

					var Page = (Page)Activator.CreateInstance( Pgm.PageType );

					Page.Loaded += ( sender, args ) =>
					               {
						               switch( Page.DataContext )
						               {
						               case StartPageModel Context:
							               Context.OnLogin = async ( companyName, userName ) =>
							                                 {
								                                 if( Globals.Security.IsIds )
								                                 {
									                                 if( Globals.Security.IsNewCarrier )
									                                 {
										                                 Dispatcher.Invoke( () => { RunProgram( Globals.CARRIER_CREATE_WIZARD ); } );

										                                 return;
									                                 }
								                                 }

								                                 await EnableMenuOptions( companyName, userName );
								                                 OnLogin?.Invoke( companyName, userName );
							                                 };

							               break;

						               case ViewModelBase Model:
							               Model.SetSecurity( Pgm.CanEdit, Pgm.CanDelete, Pgm.CanCreate );

							               break;
						               }
					               };

					var Header = Globals.FindStringResource( Page.Title );

					var PrimaryTab = new PageTabItem
					                 {
						                 Tag        = pgmName,
						                 Header     = Header,
						                 AllowClose = Pgm.AllowClose,
						                 Content    = Page
					                 };
					ProgramTabs.Add( PrimaryTab );

					// Run related programs
					foreach( var Program in Pgm.RelatedPrograms )
					{
						if( !IsProgramRunning( Program ).Running )
							RunProgram( Program, false );
					}

					if( isPrimary && TabControl.IsNotNull() )
						TabControl.SelectedItem = PrimaryTab;

					return ( PrimaryTab, Page );
				}
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return ( null, null );
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();

			Updater = new Updater( Globals.CurrentVersion.CURRENT_VERSION, "CurrentVersion.txt", $"Client/{Slot}", "IdsClientSetup.exe", ( updater, newVersionNumber ) => { Dispatcher.InvokeAsync( () => { UpdateAvailable = true; } ); } );
		}

		[DependsUpon( nameof( Language ) )]
		public void WhenLanguageTypeChange()
		{
			switch( Language )
			{
			case LANGUAGE.DEFAULT:
				LanguageDefault = true;

				break;

			case LANGUAGE.AUSTRALIAN:
				LanguageAustralian = true;

				break;

			case LANGUAGE.CANADIAN:
				LanguageCanadian = true;

				break;

			case LANGUAGE.US:
				LanguageUs = true;

				break;
			}
		}

		[DependsUpon( nameof( LanguageDefault ) )]
		public void WhenLanguageDefaultChanges()
		{
			if( !InLanguageChange )
			{
				InLanguageChange = true;

				try
				{
					LanguageAustralian = false;
					LanguageCanadian   = false;
					LanguageUs         = false;
				}
				finally
				{
					InLanguageChange = false;
				}
			}
		}

		[DependsUpon( nameof( LanguageAustralian ) )]
		public void WhenLanguageAustralianChanges()
		{
			if( !InLanguageChange )
			{
				InLanguageChange = true;

				try
				{
					LanguageDefault  = false;
					LanguageCanadian = false;
					LanguageUs       = false;
				}
				finally
				{
					InLanguageChange = false;
				}
			}
		}

		[DependsUpon( nameof( LanguageCanadian ) )]
		public void WhenLanguageCanadianChanges()
		{
			if( !InLanguageChange )
			{
				InLanguageChange = true;

				try
				{
					LanguageDefault    = false;
					LanguageAustralian = false;
					LanguageUs         = false;
				}
				finally
				{
					InLanguageChange = false;
				}
			}
		}


		[DependsUpon( nameof( LanguageUs ) )]
		public void WhenLanguageUsChanges()
		{
			if( !InLanguageChange )
			{
				InLanguageChange = true;

				try
				{
					LanguageDefault    = false;
					LanguageAustralian = false;
					LanguageCanadian   = false;
				}
				finally
				{
					InLanguageChange = false;
				}
			}
		}

		[DependsUpon( nameof( LanguageDefault ) )]
		[DependsUpon( nameof( LanguageAustralian ) )]
		[DependsUpon( nameof( LanguageCanadian ) )]
		[DependsUpon( nameof( LanguageUs ) )]
		public void WhenLanguageChanges()
		{
			if( !InLanguageChange && !InLanguageChangeEvent && ( OnLanguageChange != null ) )
			{
				InLanguageChangeEvent = true;

				try
				{
					LANGUAGE L;

					if( LanguageAustralian )
						L = LANGUAGE.AUSTRALIAN;
					else if( LanguageCanadian )
						L = LANGUAGE.CANADIAN;
					else if( LanguageUs )
						L = LANGUAGE.US;
					else
						L = LANGUAGE.DEFAULT;

					Language = L;

					OnLanguageChange( L );
				}
				finally
				{
					InLanguageChangeEvent = false;
				}
			}
		}

		public void RunProgram( string pgmName, object arg )
		{
			var (Tab, Running) = IsProgramRunning( pgmName );

			var Page = !Running ? RunProgram( pgmName, !( arg is null ) ).Page : Tab?.Content;

			if( !( Page is null ) )
			{
				Page.Tag = arg;

				if( Page.DataContext.IsNotNull() && Page.DataContext is ViewModelBase Model )
				{
					if( !( Tab is null ) )
						Tab.IsSelected = true;

					Model.OnArgumentChange( arg );
				}
			}
		}

		public void CloseAllPrograms()
		{
			Dispatcher.Invoke( () =>
			                   {
				                   for( var I = ProgramTabs.Count; --I >= 0; )
					                   ProgramTabs.Remove( ProgramTabs[ I ] );
			                   } );
		}

		public void CloseProgram( Page page )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   foreach( var ProgramTab in ProgramTabs )
				                   {
					                   if( Equals( ProgramTab.Content, page ) )
					                   {
						                   ProgramTabs.Remove( ProgramTab );

						                   break;
					                   }
				                   }
			                   } );
		}

		public void CloseProgram( ViewModelBase model )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   foreach( var ProgramTab in ProgramTabs )
				                   {
					                   if( ProgramTab.Content is Page Page && ( Page.DataContext == model ) )
					                   {
						                   ProgramTabs.Remove( ProgramTab );

						                   break;
					                   }
				                   }
			                   } );
		}

		[DependsUpon( "TabControl" )]
		public void WhenTabControlChanges()
		{
			TabControl.ItemsSource = ProgramTabs;
			RunProgram( Globals.LOGIN );
		}

		public void Execute_FileExit()
		{
			OnExit?.Invoke();
		}

		public void Execute_SignOut()
		{
			OnSignOut?.Invoke();
		}

		public void Execute_SearchTrips()
		{
			RunProgram( Globals.SEARCH_TRIPS );
		}

		public void Execute_DispatchBoard()
		{
			RunProgram( Globals.DISPATCH_BOARD );
		}

		public void Execute_DriversBoard()
		{
			RunProgram( Globals.DRIVERS_BOARD );
		}

		public void Execute_MaintainStaff()
		{
			RunProgram( Globals.MAINTAIN_STAFF );
		}

		public void Execute_MaintainSettings()
		{
			RunProgram( Globals.MAINTAIN_SETTINGS );
		}

		public void Execute_MaintainRoles()
		{
			RunProgram( Globals.MAINTAIN_ROLES );
		}

		public void Execute_StaffAudit()
		{
			RunProgram( Globals.STAFF_AUDIT_TRAIL );
		}

		public void Execute_TripAudit()
		{
			RunProgram( Globals.TRIP_AUDIT_TRAIL );
		}

		public void Execute_TripEntry()
		{
			RunProgram( Globals.TRIP_ENTRY );
		}

		public void Execute_MaintainRoutes()
		{
			RunProgram( Globals.PRIORITY_ROUTES );
		}

		public void Execute_RateWizard()
		{
			RunProgram( Globals.RATE_WIZARD );
		}

		public void Execute_Accounts()
		{
			RunProgram( Globals.ACCOUNTS );
		}

		public void Execute_AddressBook()
		{
			RunProgram( Globals.ADDRESS_BOOK );
		}

		public void Execute_ShowHelp()
		{
			Logging.WriteLogLine( "Opening " + HelpUri );
			var Uri = new Uri( HelpUri );
			Process.Start( new ProcessStartInfo( Uri.AbsoluteUri ) );
		}

		public void Execute_ViewCurrentLog()
		{
			var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
			var FileName     = AppDataLocal + "\\Ids\\" + DateTime.Now.ToString( "yyyy-MM-dd" ) + "-Ids2ClientLog.Txt";
			Logging.WriteLogLine( "Opening current log: " + FileName );
			Process.Start( FileName );
		}

		public void Execute_ExploreLogDirectory()
		{
			var AppDataLocal = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
			var Directory    = AppDataLocal + "\\Ids\\";

			try
			{
				Process.Start( "explorer", Directory );
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( "ERROR : Opening " + Directory + " : " + E );
			}
		}

		public void Execute_PmlInventory()
		{
			RunProgram( Globals.PML_INVENTORY );
		}

		public void Execute_UpliftProducts()
		{
			RunProgram( Globals.UPLIFT_PRODUCTS );
		}

		public void Execute_UpliftShipments()
		{
			RunProgram( Globals.UPLIFT_SHIPMENTS );
		}

		public void Execute_ReportBrowser()
		{
			RunProgram( Globals.REPORT_BROWSER );
		}


		public void Execute_StressTest()
		{
			RunProgram( Globals.STRESS_TEST );
		}

		public void Execute_Install()
		{
			CloseAllPrograms();
			OnSignOut?.Invoke();
			Updater?.RunInstall();
			Application.Current.Shutdown();
		}

		public MainWindowModel()
		{
			UserName        = Globals.Dictionary.AsString( "NotSignedIn" );
			ProgramTabItems = ProgramTabs;

			if( !IsInDesignMode )
				HideMenuOptions();
		}

	#region Pml

		public bool PmlVisible
		{
			get { return Get( () => PmlVisible, IsInDesignMode ); }
			set { Set( () => PmlVisible, value ); }
		}


		public bool PmlProductsVisible
		{
			get { return Get( () => PmlProductsVisible, true ); }
			set { Set( () => PmlProductsVisible, value ); }
		}


		public bool PmlShipmentsVisible
		{
			get { return Get( () => PmlShipmentsVisible, true ); }
			set { Set( () => PmlShipmentsVisible, value ); }
		}

	#endregion

	#region Ids / Diagnostics

		public void Execute_ServerTime()
		{
			RunProgram( Globals.SERVER_TIME );
		}

		public void Execute_BackupRestore()
		{
			RunProgram( Globals.BACKUP_RESTORE );
		}

	#endregion

	#region Show Menu

		public bool ShowMenu
		{
			get { return Get( () => ShowMenu, IsInDesignMode ); }
			set { Set( () => ShowMenu, value ); }
		}


		[DependsUpon( nameof( ShowMenu ) )]
		public void WhenShowMenuChanges()
		{
			IsIds = Globals.Security.IsIds;
		}

	#endregion
	}
}