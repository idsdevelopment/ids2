﻿using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Newtonsoft.Json;
using ViewModels.Roles;

namespace ViewModels.MainWindow
{
	public partial class MainWindowModel : ViewModelBase
	{
		private void HideMenuOptions()
		{
			PreferencesVisible = false;
			RolesVisible       = false;
			TripsVisible       = false;
			SearchVisible      = false;

			ReportsVisible = false;

			AuditVisible      = false;
			AuditStaffVisible = false;
			AuditTripVisible  = false;

			StaffVisible  = false;
			RoutesVisible = false;

			BoardsVisible        = false;
			DispatchBoardVisible = false;
			DriversBoardVisible  = false;
			RateWizardVisible    = false;

			LogsVisible        = false;
			ViewLogsVisible    = false;
			ExploreLogsVisible = false;

			CustomersVisible         = false;
			CustomerAccountsVisible  = false;
			CustomerAddressesVisible = false;

			PmlVisible = false;
		}

		public async Task EnableMenuOptions( string company, string userName )
		{
			HideMenuOptions();

			if( Globals.Modifications.IsPriority )
			{
				RoutesVisible      = true;
				LogsVisible        = true;
				ViewLogsVisible    = true;
				ExploreLogsVisible = true;
			}
			else
			{
				var IsPml = Globals.Modifications.IsPml;

				var Roles = await Azure.Client.RequestStaffMemberRoles( userName );

				Dispatcher.Invoke( () =>
				                   {
					                   if( Globals.Security.IsIds )
					                   {
						                   PreferencesVisible = true;
						                   RolesVisible       = true;
						                   TripsVisible       = true;
						                   SearchVisible      = true;

						                   ReportsVisible = true;

						                   AuditVisible      = true;
						                   AuditStaffVisible = true;
						                   AuditTripVisible  = true;

						                   StaffVisible  = true;
						                   RoutesVisible = true;

						                   BoardsVisible        = true;
						                   DispatchBoardVisible = true;
						                   DriversBoardVisible  = true;
						                   RateWizardVisible    = true;

						                   LogsVisible        = true;
						                   ViewLogsVisible    = true;
						                   ExploreLogsVisible = true;

						                   CustomersVisible         = true;
						                   CustomerAccountsVisible  = true;
						                   CustomerAddressesVisible = true;

						                   PmlVisible          = true;
						                   PmlProductsVisible  = true;
						                   PmlShipmentsVisible = true;
					                   }
					                   else
					                   {
						                   var AdminRole = new Role
						                                   {
							                                   Routes      = true,
							                                   Roles       = true,
							                                   CanCreate   = true,
							                                   Preferences = true,
							                                   Trip        = true,
							                                   Search      = true,

							                                   Reports = true,

							                                   Audit      = true,
							                                   AuditStaff = true,
							                                   AuditTrip  = true,

							                                   Staff = true,

							                                   Boards        = true,
							                                   DriversBoard  = true,
							                                   DispatchBoard = true,
							                                   RateWizard    = true,

							                                   Logs       = true,
							                                   ViewLog    = true,
							                                   ExploreLog = true,

							                                   Customers         = true,
							                                   CustomerAccounts  = true,
							                                   CustomerAddresses = true,

							                                   CanEdit   = true,
							                                   CanDelete = true,
							                                   CanView   = true,

							                                   Pml          = IsPml,
							                                   PmlProducts  = IsPml,
							                                   PmlShipments = IsPml
						                                   };

						                   PmlVisible = Globals.Modifications.IsPml;

						                   foreach( var Role in Roles )
						                   {
							                   try
							                   {
								                   Role R;

								                   if( Role.IsAdministrator )
									                   R = AdminRole;
								                   else
									                   R = JsonConvert.DeserializeObject<Role>( Role.JsonObject ) ?? new Role();

								                   PreferencesVisible |= R.Preferences;
								                   RolesVisible       |= R.Roles;
								                   TripsVisible       |= R.Trip;
								                   SearchVisible      |= R.Search;

								                   ReportsVisible |= R.Reports;

								                   AuditVisible      |= R.Audit;
								                   AuditStaffVisible |= R.AuditStaff;
								                   AuditTripVisible  |= R.AuditTrip;

								                   StaffVisible  |= R.Staff;
								                   RoutesVisible |= R.Routes;

								                   BoardsVisible        |= R.Boards;
								                   DispatchBoardVisible |= R.DispatchBoard;
								                   DriversBoardVisible  |= R.DriversBoard;
								                   RateWizardVisible    |= R.RateWizard;

								                   LogsVisible        |= R.Logs;
								                   ViewLogsVisible    |= R.ViewLog;
								                   ExploreLogsVisible |= R.ExploreLog;

								                   CustomersVisible         |= R.Customers;
								                   CustomerAccountsVisible  |= R.CustomerAccounts;
								                   CustomerAddressesVisible |= R.CustomerAddresses;

								                   if( IsPml )
								                   {
									                   PmlVisible          = R.Pml;
									                   PmlProductsVisible  = R.PmlProducts;
									                   PmlShipmentsVisible = R.PmlShipments;
								                   }
								                   else
								                   {
									                   PmlVisible          = false;
									                   PmlProductsVisible  = false;
									                   PmlShipmentsVisible = false;
								                   }

								                   if( Role.IsAdministrator )
									                   break;
							                   }
							                   catch
							                   {
							                   }
						                   }

						                   RoutesVisible = false;
					                   }
				                   } );
			}
		}
	}
}