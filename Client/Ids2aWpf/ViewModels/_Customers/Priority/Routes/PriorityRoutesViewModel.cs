﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Protocol.Data;
using Utils;

// ReSharper disable InconsistentNaming

namespace ViewModels._Customers.Priority.Routes
{
	public class RouteEntry : INotifyPropertyChanged
	{
		private bool _Delete,
		             _EnabledOnDevice,
		             _OriginalEnabledOnDevice;

		public delegate Task OnChangeEvent( RouteEntry entry );
		public delegate void OnDeleteChangeEvent( RouteEntry entry );

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public string RouteName
		{
			get => _RouteName;
			set
			{
				_RouteName = value;
				OnPropertyChanged();
				OnRouteChange?.Invoke( this );
			}
		}

		public string OriginalRoutName
		{
			get => _OriginalRoutName;
			set
			{
				_OriginalRoutName = value;
				_RouteName = value;
			}
		}

		public bool EnabledOnDevice
		{
			get => _EnabledOnDevice;
			set
			{
				_EnabledOnDevice = value;
				OnPropertyChanged();
				OnEnableChange?.Invoke( this );
			}
		}

		public bool OriginalEnabledOnDevice
		{
			get => _OriginalEnabledOnDevice;
			set
			{
				_OriginalEnabledOnDevice = value;
				_EnabledOnDevice = value;
				OnPropertyChanged();
			}
		}

		public bool Delete
		{
			get => _Delete;
			set
			{
				_Delete = value;
				OnPropertyChanged();
				OnDeleteChange?.Invoke( this );
			}
		}

		public string _OriginalRoutName,
		              _RouteName;

		public OnDeleteChangeEvent OnDeleteChange;

		public OnChangeEvent OnEnableChange,
		                     OnRouteChange;

		public event PropertyChangedEventHandler PropertyChanged;
	}


	public class PriorityRoutesViewModel : ViewModelBase
	{
		private const string PROGRAM_NAME = "Maintain Routes";

		private readonly Dictionary<string, CustomerRoutesBasic> CustomerRoutes = new Dictionary<string, CustomerRoutesBasic>();

		private void OnDeleteChange( RouteEntry entry )
		{
			var Enab = false;
			foreach( var RouteEntry in Routes )
			{
				Enab |= RouteEntry.Delete;
				if( Enab )
					break;
			}

			EnableDelete = Enab;
		}

		public delegate void BeginEditRouteEvent( RouteEntry item );


		public bool EnableEdit
		{
			get { return Get( () => EnableEdit, false ); }
			set { Set( () => EnableEdit, value ); }
		}

		public ObservableCollection<string> Pharmacies
		{
			get { return Get( () => Pharmacies, new ObservableCollection<string>() ); }
			set { Set( () => Pharmacies, value ); }
		}


		public string SelectedPharmacy
		{
			get { return Get( () => SelectedPharmacy, "" ); }
			set { Set( () => SelectedPharmacy, value ); }
		}


		public ObservableCollection<RouteEntry> Routes
		{
			get { return Get( () => Routes, new ObservableCollection<RouteEntry>() ); }
			set { Set( () => Routes, value ); }
		}


		public bool EnableRoutesForPharmacy
		{
			get { return Get( () => EnableRoutesForPharmacy, IsInDesignMode ); }
			set { Set( () => EnableRoutesForPharmacy, value ); }
		}

		[DependsUpon( nameof( EnableRoutesForPharmacy ) )]
		public async Task WhenEnableRoutesForPharmacyChanges()
		{
			var Phar = SelectedPharmacy;
			var Enab = EnableRoutesForPharmacy;

			if( Phar.IsNotNullOrWhiteSpace() )
			{
				if( CustomerRoutes.TryGetValue( Phar, out var CustRoutes ) )
					CustRoutes.EnableRoutesForCustomer = Enab;

				await Azure.Client.RequestEnableRouteForCustomer( new EnableRouteForeCustomer
				                                                  {
					                                                  Enable = Enab,
					                                                  CustomerCode = Phar
				                                                  } );
			}
		}

		public bool EnableDelete
		{
			get { return Get( () => EnableDelete, false ); }
			set { Set( () => EnableDelete, value ); }
		}

		[DependsUpon( nameof( EnableDelete ) )]
		public void WhenEnableDeleteChanges()
		{
			EnableAdd = !EnableDelete;
		}

		public bool EnableSave
		{
			get { return Get( () => EnableSave, false ); }
			set { Set( () => EnableSave, value ); }
		}


		[DependsUpon( nameof( SelectedPharmacy ) )]
		public void WhenSelectedPharmacyChanges()
		{
			var Pharmacy = SelectedPharmacy;
			if( Pharmacy.IsNotNull() )
			{
				var Temp = new ObservableCollection<RouteEntry>();
				if( CustomerRoutes.TryGetValue( Pharmacy, out var Rts ) )
				{
					EnableRoutesForPharmacy = Rts.EnableRoutesForCustomer;

					foreach( var Rt in Rts.Routes )
					{
						var Item = new RouteEntry
						           {
							           OriginalRoutName = Rt.RouteName,
							           EnabledOnDevice = Rt.Enabled,
							           OnEnableChange = Save,
							           OnRouteChange = Save,
							           OnDeleteChange = OnDeleteChange
						           };
						Temp.Add( Item );
					}
				}

				Routes = Temp;
				EnableAdd = true;
			}
			else
				EnableAdd = false;
		}


		public RouteEntry DataGridSelectedItem
		{
			get { return Get( () => DataGridSelectedItem, new RouteEntry() ); }
			set { Set( () => DataGridSelectedItem, value ); }
		}


		public bool InAdd
		{
			get { return Get( () => InAdd, false ); }
			set { Set( () => InAdd, value ); }
		}

		[DependsUpon( nameof( InAdd ) )]
		public void WhenInAddChanges()
		{
			EnableAdd = !InAdd;
		}

		public bool EnableAdd
		{
			get { return Get( () => EnableAdd, false ); }
			set { Set( () => EnableAdd, value ); }
		}

		public void Execute_AddRoute()
		{
			InAdd = true;
			EnableAdd = false;

			var Itm = new RouteEntry
			          {
				          OriginalRoutName = "",
				          EnabledOnDevice = true,
				          OnDeleteChange = OnDeleteChange,
				          OnEnableChange = Save,
				          OnRouteChange = Save
			          };
			Routes.Add( Itm );
			DataGridSelectedItem = Itm;
			BeginEditRoute?.Invoke( Itm );
		}


		public async Task Execute_DeleteRoutes()
		{
			EnableDelete = false;

			var ToDelete = ( from R in Routes
			                 where R.Delete
			                 select R ).ToList();

			for( var I = Routes.Count; --I >= 0; )
			{
				if( Routes[ I ].Delete )
					Routes.RemoveAt( I );
			}

			// Clean up internal cache
			var Phar = SelectedPharmacy;
			if( CustomerRoutes.TryGetValue( Phar, out var RoutesBasic ) )
			{
				var DeleteNames = from D in ToDelete
				                  select D.RouteName;

				RoutesBasic.Routes = ( from R in RoutesBasic.Routes
				                       where !DeleteNames.Contains( R.RouteName )
				                       select R ).ToList();
			}

			foreach( var Entry in ToDelete )
				await Azure.Client.RequestDeleteRoute( PROGRAM_NAME, Phar, Entry.RouteName );
		}

		public async Task Save( RouteEntry entry )
		{
			if( entry.IsNotNull() )
			{
				var Phar = SelectedPharmacy;
				if( Phar.IsNotNullOrWhiteSpace() )
				{
					var OName = entry.OriginalRoutName;
					var NName = entry.RouteName;

					if( OName.IsNullOrWhiteSpace() && NName.IsNullOrWhiteSpace() )
						return;

					if( !CustomerRoutes.TryGetValue( Phar, out var CustRoutes ) )
					{
						// Shouldn't happen
						CustomerRoutes.Add( Phar, CustRoutes = new CustomerRoutesBasic() );
					}

					var RouteBasics = CustRoutes.Routes;

					var Rec = ( from R in RouteBasics
					            where R.RouteName == entry.OriginalRoutName
					            select R ).FirstOrDefault();

					if( Rec is null )
					{
						Rec = new CustomerRouteBasic
						      {
							      RouteName = entry.RouteName,
							      Enabled = entry.EnabledOnDevice
						      };
						RouteBasics.Add( Rec );
						CustRoutes.Routes = ( from R in RouteBasics
						                      orderby R.RouteName
						                      select R ).ToList();
					}
					else
					{
						Rec.RouteName = entry.RouteName;
						Rec.Enabled = entry.EnabledOnDevice;
					}

					entry.OriginalRoutName = NName;
					entry.OriginalEnabledOnDevice = entry.EnabledOnDevice;

					AddUpdateRenameRouteBasic UpdateRec;

					// New Route
					if( OName.IsNullOrWhiteSpace() && NName.IsNotNullOrWhiteSpace() )
					{
						UpdateRec = new AddUpdateRenameRouteBasic
						            {
							            CustomerCode = Phar,
							            NewName = NName,
							            OldName = "",
							            Enabled = entry.EnabledOnDevice
						            };
					}
					else // Modify / Rename
					{
						UpdateRec = new AddUpdateRenameRouteBasic
						            {
							            CustomerCode = Phar,
							            NewName = NName,
							            OldName = OName,
							            Enabled = entry.EnabledOnDevice
						            };
					}

					await Azure.Client.RequestAddUpdateRenameRouteBasic( UpdateRec );
					WhenEnableDeleteChanges();
					BeginEditRoute?.Invoke( null );
                }
			}
		}

		public BeginEditRouteEvent BeginEditRoute;

		public PriorityRoutesViewModel()
		{
			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          var CustomerCodeList = from C in await Azure.Client.RequestGetCustomerCodeList()
					                                 orderby C
					                                 select C;

					          var Pharms = new ObservableCollection<string>();
					          foreach( var C in CustomerCodeList )
						          Pharms.Add( C );

					          var List = await Azure.Client.RequestGetCustomersRoutesBasic( new CustomerCodeList( Pharms ) );

					          foreach( var R in List )
						          CustomerRoutes.Add( R.CustomerCode, R );

					          Dispatcher.Invoke( () =>
					                             {
						                             Pharmacies = Pharms;
						                             EnableEdit = true;
					                             } );
				          } );
			}
		}
	}
}