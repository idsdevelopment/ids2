﻿using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.Utils;

namespace ViewModels._Customers.Priority.Routes
{
	/// <summary>
	///     Interaction logic for Routes.xaml
	/// </summary>
	public partial class Routes : Page
	{
		private PriorityRoutesViewModel Model;

		private void ComboBox_KeyDown( object sender, KeyEventArgs e )
		{
			if( !CustomerComboBox.IsDropDownOpen )
				CustomerComboBox.IsDropDownOpen = true;
		}

		public Routes()
		{
			Initialized += ( sender, args ) =>
			               {
				               if( DataContext is PriorityRoutesViewModel M )
				               {
					               Model = M;
					               M.BeginEditRoute += item =>
					                                   {
						                                   Grid.UpdateLayout();
						                                   Grid.Focus();
						                                   var Count = Grid.Items.Count;
						                                   if( Count > 0 )
						                                   {
							                                   var Cell = Grid.GetCell( Count - 1, 0 );
							                                   if( !( Cell is null ) )
								                                   Cell.FindChild<TextBox>()?.Focus();
						                                   }
					                                   };
				               }
			               };
			InitializeComponent();
		}
	}
}