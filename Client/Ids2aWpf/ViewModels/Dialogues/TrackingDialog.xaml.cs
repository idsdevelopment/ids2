﻿using Microsoft.Reporting.WinForms;
using Protocol.Data;
using System;
using System.Reflection;
using System.Windows;

namespace ViewModels.Dialogues
{
    /// <summary>
    /// Interaction logic for TrackingDialog.xaml
    /// </summary>
    public partial class TrackingDialog : Window
    {
        public TrackingDialog(Trip trip)
        {
            InitializeComponent();

            var title = (string)Application.Current.TryFindResource("DialoguesTrackingDialogTitle");
            Title = title.Replace("@1", trip.TripId);

            TrackingReportViewer.Reset();
            TrackingReportViewer.ProcessingMode = ProcessingMode.Local;

            var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.TrackingReport.rdlc";
            var Local = TrackingReportViewer.LocalReport;
            Local.EnableExternalImages = true; // Needed for loading the barcode
            Local.ReportEmbeddedResource = Rep;

            var ReportDataSource = new ReportDataSource
            {
                Name = "DataSet1",
                // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
                // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
                // Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
                //Value = new Reporting.PickupManifestList(account, response, from, to)
                Value = new Reporting.DataSets.TrackingDetailsList(trip)
            };

            Local.DataSources.Clear();
            Local.DataSources.Add(ReportDataSource);

            try
            {
                TrackingReportViewer.RefreshReport();
                //this.reportLoaded = true;
                this.btnPrint.IsEnabled = true;
            }
            catch (Exception exception)
            {
                Utils.Logging.WriteLogLine("ERROR: Trying to generate report: " + exception);
            }
        }

        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            var Report = TrackingReportViewer.LocalReport;
            var Settings = Report.GetDefaultPageSettings();

            Utils.Logging.WriteLogLine("settings paper size: " + Settings.PaperSize);
            Utils.Logging.WriteLogLine("settings margins: " + Settings.Margins);
            TrackingReportViewer.PageCountMode = PageCountMode.Actual;
            Utils.Logging.WriteLogLine("this.ManifestViewer.GetTotalPages(): " + TrackingReportViewer.GetTotalPages());
            try
            {
                TrackingReportViewer.PrintDialog();
                //var result = TrackingReportViewer.PrintDialog();
                //if (result == System.Windows.Forms.DialogResult.OK)
                //{
                //    // printDialog.
                //}
            }
            catch (Exception exception)
            {
                Utils.Logging.WriteLogLine("Exception: " + "Message: " + exception.Message + ", " + exception.StackTrace);
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
