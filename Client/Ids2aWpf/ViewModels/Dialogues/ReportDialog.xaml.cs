﻿using Microsoft.ReportingServices.ReportProcessing.ReportObjectModel;
using System;
using System.Reflection;
using System.Windows;
using Utils;
using ViewModels.ReportBrowser;
using Microsoft.Reporting.WinForms;
using ViewModels.Reporting;
using ViewModels.ReportViewer;
using DataSets = ViewModels.Reporting.DataSets;

namespace ViewModels.Dialogues
{
    /// <summary>
    /// Interaction logic for ReportDialog.xaml
    /// </summary>
    public partial class ReportDialog : Window
    {
        private readonly ReportSettings reportSettings;
        
        // TODO Add to here
        private readonly string LinfoxReportName = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox");

        public ReportDialog(ReportSettings reportSettings)
        {
            //LinfoxReportName = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox");

            WindowStartupLocation = WindowStartupLocation.CenterOwner;

            InitializeComponent();

            this.reportSettings = reportSettings;

            Logging.WriteLogLine("Setting report to " + reportSettings.Description);

            Title = (string)Application.Current.TryFindResource("ReportDialogTitle");
            Title = Title.Replace("@1", reportSettings.Description);

            ReportViewer.Reset();
            //ReportViewer.ProcessingMode = ProcessingMode.Local;
            //ReportViewer.PrinterSettings.PrinterName = printer;

            // string Report = "Weighbills.ShipNow.rdlc";
            //var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.ShipNow.rdlc";
            //var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";
            //var Rep = MainWindow.REPORT_BASE_NAME + ".Reports.Zebra_Intermountain.rdlc";
            //var Rep = "Ids2Wpf.ViewModels.Reports.Zebra_Intermountain.rdlc";

            //var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.Zebra_Intermountain.rdlc";

            (string Rep, ReportDataSource ReportDataSource) = PickReport(reportSettings.Description);
            if (Rep.IsNotNullOrWhiteSpace() && ReportDataSource != null)
            {
                var Local = ReportViewer.LocalReport;
                Local.EnableExternalImages = true; // Needed for loading the barcode
                Local.ReportEmbeddedResource = Rep;



                //var ReportDataSource = new ReportDataSource
                //{
                //    Name = "DataSet1",
                //    // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
                //    // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
                //    //Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
                //    Value = new DataSets.LinfoxShipmentReportDetailsList(reportSettings)
                //};
                Local.DataSources.Clear();
                Local.DataSources.Add(ReportDataSource);

                //System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
                //setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
                //setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
                //setup.Landscape = false;
                //this.ReportViewer.SetPageSettings(setup);

                // From https://stackoverflow.com/questions/44390663/c-using-reportviewer-to-print-on-a-custom-size-paper
                //System.Drawing.Printing.PageSettings setup = new System.Drawing.Printing.PageSettings();
                //setup.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
                //setup.Landscape = false;
                //setup.PaperSize = new System.Drawing.Printing.PaperSize("CustomType", 400, 590);
                //this.ReportViewer.SetPageSettings(setup);

                // Trying DataTable
                //Local.DataSources.Clear();
                //Local.DataSources.Add(mainDataSource);

                try
                {
                    /*
					tmp = (Reporting.DataSets.WeighbillDetailsList)this.ReportViewer.LocalReport.DataSources.First().Value;
					tmp2 = tmp.First();
					Console.WriteLine("ReportDataSource.Value from ReportViewer.LocalReport: " + tmp2.ToString());
				    */
                    // this.ReportViewer.LocalReport.Refresh();

                    ReportViewer.RefreshReport();
                }
                catch (Exception E)
                {
                    Utils.Logging.WriteLogLine("Exception caught: " + E.ToString());
                }
            }

        }    

        /// <summary>
        /// Picks the correct settings based upon the report's description.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        private (string report, ReportDataSource rds) PickReport(string description)
        {
            string report = string.Empty;
            ReportDataSource rds = null;
            
            if (description == LinfoxReportName)
            {
                report = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.LinfoxShipmentReport.rdlc";

                rds = new ReportDataSource
                {
                    Name = "DataSet1",
                    // Value = new DataSets.WeighbillDetailsList(this.trip.pieces)
                    // Value = new DataSets.WeighbillDetailsList( trip, isCOD, codAmount, isShowTripIdPlusPieceCount )
                    //Value = new DataSets.WeighbillDetailsList(trip, isCOD, codAmount, isShowTripIdPlusPieceCount, resellerName, resellerPhone)
                    Value = new DataSets.LinfoxShipmentReportDetailsList(reportSettings)
                };
            }
            
            return (report, rds);
        }
    }
}
