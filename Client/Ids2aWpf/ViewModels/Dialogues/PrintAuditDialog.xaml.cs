﻿using Microsoft.Reporting.WinForms;
using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows;
using static ViewModels.Reporting.DataSets;

namespace ViewModels.Dialogues
{
    /// <summary>
    /// Interaction logic for PrintAuditDialog.xaml
    /// </summary>
    public partial class PrintAuditDialog : Window
    {
        public PrintAuditDialog(Trip trip, List<AuditLine> auditLines, string title = "Audit for Item")
        {
            InitializeComponent();

            Title = title;

            //var title = (string)Application.Current.TryFindResource("DialoguesTrackingDialogTitle");
            //Title = title.Replace("@1", trip.TripId);

            AuditReportViewer.Reset();
            AuditReportViewer.ProcessingMode = ProcessingMode.Local;

            // TODO 
            var Rep = Assembly.GetExecutingAssembly().GetName().Name + ".Reports.TripAudit.rdlc";  
            var Local = AuditReportViewer.LocalReport;
            Local.EnableExternalImages = true; // Needed for loading the barcode
            Local.ReportEmbeddedResource = Rep;

            var ReportDataSource = new ReportDataSource
            {
                Name = "DataSet1",
                Value = new Reporting.DataSets.AuditDetailsList(trip, auditLines)
            };

            Local.DataSources.Clear();
            Local.DataSources.Add(ReportDataSource);

            try
            {
                AuditReportViewer.RefreshReport();
                //this.reportLoaded = true;
                this.btnPrint.IsEnabled = true;
            }
            catch (Exception exception)
            {
                Utils.Logging.WriteLogLine("ERROR: Trying to generate report: " + exception);
            }

        }

        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            var Report = AuditReportViewer.LocalReport;
            var Settings = Report.GetDefaultPageSettings();

            Utils.Logging.WriteLogLine("settings paper size: " + Settings.PaperSize);
            Utils.Logging.WriteLogLine("settings margins: " + Settings.Margins);
            AuditReportViewer.PageCountMode = PageCountMode.Actual;
            Utils.Logging.WriteLogLine("this.ManifestViewer.GetTotalPages(): " + AuditReportViewer.GetTotalPages());
            try
            {
                AuditReportViewer.PrintDialog();
            }
            catch (Exception exception)
            {
                Utils.Logging.WriteLogLine("Exception: " + "Message: " + exception.Message + ", " + exception.StackTrace);
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
