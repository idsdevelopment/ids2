﻿using IdsControlLibraryV2.ViewModel;

namespace ViewModels.Dialogues
{
	public class ErrorModel : ViewModelBase
	{
		public ErrorModel()
		{
			Title = Globals.Dictionary.AsString( "Error" );
		}

		public string Title
		{
			get { return Get( () => Title ); }
			set { Set( () => Title, value ); }
		}

		public string Text
		{
			get { return Get( () => Text, "Default Text" ); }
			set { Set( () => Text, value ); }
		}
	}
}