﻿using AzureRemoteService;
using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using Utils;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;
using Microsoft.Win32;
using System.IO;
using System.Linq;

namespace ViewModels.ReportBrowser.CreateExcel
{
    class CreateLinfoxShipmentReport
    {
        private ReportSettings rs = null;
        private TripList trips = null;
        //private SortedDictionary<string, Trip> sdTrips = new SortedDictionary<string, Trip>();
        private readonly Dictionary<string, Dictionary<string, List<Trip>>> dictTripsByOperationAndRegion = new Dictionary<string, Dictionary<string, List<Trip>>>();
        private List<Trip> tripsInOrder = new List<Trip>();

        private ExcelPackage package = null;
        private ExcelWorksheet worksheet = null;
        private readonly Color PrimaryHeaderBg = ColorTranslator.FromHtml("#305496");
        private readonly Color SecondaryHeaderBg = ColorTranslator.FromHtml("#00b0f0");

        public CreateLinfoxShipmentReport()
        {
            //Task.WaitAll(Task.Run(async () =>
            Task.WaitAll(Task.Run(() =>
            {
                Groups = LoadGroups();
                LoadGroupNames();
            }));
        }

        public (bool success, List<string> errors, string fileName) Run(ReportSettings rs)
        {
            bool success;
            List<string> errors = new List<string>();
            string fileName = string.Empty;

            this.rs = rs;

            trips = SearchTrips();
            Logging.WriteLogLine("Found " + trips.Count + " trips");
            if (trips.Count > 0)
            {
                //trips.Sort((x, y) => DateTimeOffset.Compare(x.CallTime, y.CallTime));
                tripsInOrder = trips.OrderBy(x => x.CallTime).ToList();

                LoadTripsByPickupZone();

                (success, errors, fileName) = BuildReport();

                if (success)
                {
                    bool giveup = false;
                    while (!giveup)
                    {
                        try
                        {
                            // TODO Ask the user for directory to save the file
                            SaveFileDialog dlg = new SaveFileDialog
                            {
                                DefaultExt = ".xlsx",
                                Filter = "Microsoft Excel 2007-2013 XML (*.xlsx)|*.xlsx",
                                FileName = fileName
                            };
                            bool? result = dlg.ShowDialog();
                            if (result == true)
                            {
                                fileName = dlg.FileName;

                                var fi = new FileInfo(fileName);

                                package.SaveAs(fi);
                                giveup = true; // Don't try again
                            }
                            else
                            {
                                giveup = true;
                                // Cancelled
                                success = false;
                                errors.Add((string)Application.Current.TryFindResource("ReportBrowserReportsErrorsCancelled"));
                            }
                        }
                        catch
                        {
                            //Dispatcher.Invoke(() =>
                            //{
                            //    string message = "";
                            //});
                            string title = (string)Application.Current.TryFindResource("ReportBrowserReportsErrorsCantSaveReportTitle");
                            string message = (string)Application.Current.TryFindResource("ReportBrowserReportsErrorsCantSaveReport");
                            message += "\n\t" + fileName + "\n";
                            message += (string)Application.Current.TryFindResource("ReportBrowserReportsErrorsCantSaveReport2");

                            MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (mbr == MessageBoxResult.No)
                            {
                                Logging.WriteLogLine("Can't save the file - giving up");
                                giveup = true;
                                success = false;
                                errors.Add((string)Application.Current.TryFindResource("ReportBrowserReportsErrorsCancelled"));
                            }
                        }
                    }
                }
            }
            else
            {
                success = false;
                errors.Add((string)Application.Current.TryFindResource("ReportBrowserReportsErrorsNoShipmentsFound"));
            }

            return (success, errors, fileName);
        }

        #region Data

        //public List<CompanyAddressGroup> Groups
        //{
        //    get { return Get(() => Groups, LoadGroups); }
        //    set { Set(() => Groups, value); }
        //}

        //public List<string> GroupNames
        //{
        //    get { return Get(() => GroupNames, new List<string>()); }
        //    set { Set(() => GroupNames, value); }
        //}

        private List<CompanyAddressGroup> Groups;
        private List<string> GroupNames = new List<string>();

        /// <summary>
        /// Holds a map of group names and their companies.
        /// </summary>
        private readonly SortedDictionary<string, List<string>> GroupsAndMembers = new SortedDictionary<string, List<string>>();

        #endregion

        #region Cell Assignments
        // Summary section
        private readonly string CellSummaryNotYetCompleted = "B3";
        private readonly string CellSummaryNotYetCompletedPercentage = "C3";
        private readonly string CellSummaryFutileNoStock = "B4";
        private readonly string CellSummaryFutileNoStockPercentage = "C4";
        private readonly string CellSummaryFutileNotReady = "B5";
        private readonly string CellSummaryFutileNotReadyPercentage = "C5";
        private readonly string CellSummaryComplete = "B6";
        private readonly string CellSummaryCompletePercentage = "C6";
        private readonly string CellSummaryVerified = "B7";
        private readonly string CellSummaryVerifiedPercentage = "C7";
        //private readonly string CellSummaryTotal = "B8";
        //private readonly string CellSummaryTotalPercentage = "C8";

        // Small section to the right
        private readonly string CellSummary2Attended = "F4";
        private readonly string CellSummary2Verified = "F5";

        // PickedUp section
        //private readonly string CellPickedUpNotYetCompleted = "B13";
        //private readonly string CellPickedUpFutileNotReady = "B14";
        //private readonly string CellPickedUpFutileNoStock = "B15";
        //private readonly string CellPickedUpComplete = "B16";
        //private readonly string CellPickedUpVerified = "B17";
        //private readonly string CellPickedUpNotYetAssigned = "B18";
        //private readonly string CellPickedUpTotal = "B19";

        // Total section
        //private readonly string CellTotalNotYetCompleted = "B21";
        //private readonly string CellTotalStoresAttended = "B22";
        //private readonly string CellTotalTotal = "B23";
        //private readonly string CellTotalPercentageToBeCompleted = "B24";
        //private readonly string CellTotalPercentageCompleted = "B25";

        #endregion

        #region Utilities

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private TripList SearchTrips()
        {
            TripList trips = new TripList();
            Task.WaitAll(Task.Run(async () =>
            {
                SearchTrips st = new SearchTrips
                {
                    FromDate = rs.SelectedFrom,
                    ToDate = rs.SelectedTo,
                    ByTripId = false,
                    StartStatus = GetStatusForString(rs.SelectedFromStatus),
                    EndStatus = GetStatusForString(rs.SelectedToStatus)
                };
                if (rs.SelectedPackageType != ReportSettings.All)
                {
                    st.PackageType = rs.SelectedPackageType;
                }
                TripList raw = await Azure.Client.RequestSearchTrips(st);
                foreach (Trip trip in raw)
                {
                    if (trip.PickupZone.IsNotNullOrWhiteSpace())
                    {
                        if (IsTripInSelectedGroups(trip))
                        {
                            if (rs.SelectedOperations[0] == ReportSettings.All)
                            {
                                trips.Add(trip);
                            }
                            else 
                            {
                                (string op, _) = ExtractOperation(trip.PickupZone);
                                foreach (string operation in rs.SelectedOperations)
                                {
                                    if (operation == op)
                                    {
                                        trips.Add(trip);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Logging.WriteLogLine("Skipped trip with no pickupzone: " + trip.TripId);
                    }
                }
            }));

            return trips;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trip"></param>
        /// <returns></returns>
        private bool IsTripInSelectedGroups(Trip trip)
        {
            bool yes = false;

            if (rs.SelectedGroups[0] == ReportSettings.All)
            {
                yes = true;
            }
            else
            {
                string companyName = trip.PickupCompanyName;
                Logging.WriteLogLine("Looking for " + companyName);
                foreach (string groupName in rs.SelectedGroups)
                {
                    string gn = groupName.Substring(0, groupName.IndexOf('\t')).Trim();
                    if (GroupsAndMembers.ContainsKey(gn))
                    {
                        List<string> gns = GroupsAndMembers[gn];
                        foreach (string cn in gns)
                        {
                            if (cn.Trim() == companyName)
                            {
                                Logging.WriteLogLine("Found it in " + gn);
                                yes = true;
                                break;
                            }
                        }
                        if (yes)
                        {
                            break;
                        }
                    }
                }
            }

            return yes;
        }

        private List<CompanyAddressGroup> LoadGroups()
        {
            Logging.WriteLogLine("Loading groups");
            List<CompanyAddressGroup> ags = new List<CompanyAddressGroup>();

            Task.Run(async () =>
            {
                CompanyAddressGroups cags = null;

                try
                {
                    cags = await Azure.Client.RequestGetCompanyAddressGroups();
                }
                catch (Exception e)
                {
                    Logging.WriteLogLine("Exception thrown: " + e.ToString());
                }

                if (cags != null)
                {
                    foreach (CompanyAddressGroup cag in cags)
                    {
                        ags.Add(cag);
                    }

                    Logging.WriteLogLine("Found " + ags.Count + " groups");

                    Groups = ags;
                    LoadGroupNames();
                }
            });

            return ags;
        }


        private void LoadGroupNames()
        {
            Task.Run(async () =>
            {
                SortedDictionary<string, string> sd = new SortedDictionary<string, string>();
                SortedDictionary<string, int> counts = new SortedDictionary<string, int>();
                GroupsAndMembers.Clear();

                foreach (CompanyAddressGroup cag in Groups)
                {
                    if (!sd.ContainsKey(cag.Description.ToLower()))
                    {
                        sd.Add(cag.Description.ToLower(), cag.Description);

                        // Now get the number of companies in this group
                        try
                        {
                            CompaniesWithinAddressGroups companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup(cag.GroupNumber);
                            if (companies.Count > 0 && companies[0].Companies != null)
                            {
                                int count = companies[0].Companies.Count;
                                counts.Add(cag.Description.ToLower(), count);

                                SortedDictionary<string, string> sd1 = new SortedDictionary<string, string>();

                                foreach (string company in companies[0].Companies)
                                {
                                    Logging.WriteLogLine("Adding: " + company.ToLower());
                                    if (!sd1.ContainsKey(company.ToLower()))
                                    {
                                        sd1.Add(company.ToLower().Trim(), company);
                                    }
                                    else
                                    {
                                        Logging.WriteLogLine("Duplicate found: " + company);
                                    }
                                }

                                List<string> sorted = new List<string>();

                                foreach (string key in sd1.Keys)
                                {
                                    sorted.Add(sd1[key]);
                                }

                                GroupsAndMembers.Add(cag.Description, sorted);
                            }
                        }
                        catch (Exception e)
                        {
                            Logging.WriteLogLine("Exception thrown: " + e.ToString());
                        }
                    }
                }

                string members = (string)Application.Current.TryFindResource("AddressBookGroupsMembers");
                string labelSingle = (string)Application.Current.TryFindResource("AddressBookGroupsMembersCountSingle");
                string labelNotSingle = (string)Application.Current.TryFindResource("AddressBookGroupsMembersCountNotSingle");
                List<string> list = new List<string>();

                foreach (string key in sd.Keys)
                {
                    string line = sd[key] + " \t(" + counts[key] + " ";

                    if (counts[key] != 1)
                    {
                        //line = sd[key] + " \t(" + counts[key] + " members)"; 
                        line += labelNotSingle + ")";
                    }
                    else
                    {
                        line += labelSingle + ")";
                    }

                    list.Add(line);
                }

                GroupNames = list;

                //if (SelectedAddress == null)
                //{
                //    LoadAddressesGroupNames();
                //    Loaded = true;
                //}
                //else
                //{
                //    Loaded = true;
                //}

            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private STATUS GetStatusForString(string status)
        {
            STATUS result = STATUS.UNSET;

            switch (status)
            {
                case "UNSET":
                    result = STATUS.UNSET;
                    break;
                case "NEW":
                    result = STATUS.NEW;
                    break;
                case "ACTIVE":
                    result = STATUS.ACTIVE;
                    break;
                case "DISPATCHED":
                    result = STATUS.DISPATCHED;
                    break;
                case "PICKED_UP":
                    result = STATUS.PICKED_UP;
                    break;
                case "DELIVERED":
                    result = STATUS.DELIVERED;
                    break;
                case "VERIFIED":
                    result = STATUS.VERIFIED;
                    break;
                case "POSTED":
                    result = STATUS.POSTED;
                    break;
                case "INVOICED":
                    result = STATUS.INVOICED;
                    break;
                case "PAID":
                    result = STATUS.PAID;
                    break;
                case "DELETED":
                    result = STATUS.DELETED;
                    break;
            }

            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        private List<string> FindRegionsForOperation(string operation)
        {
            List<string> regions = new List<string>();

            foreach (Zone zone in rs.Zones)
            {
                (string op, string region) = ExtractOperation(zone.Name);
                if (op == operation)
                {
                    regions.Add(region);
                }
            }

            return regions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zone"></param>
        /// <returns></returns>
        private (string operation, string region) ExtractOperation(string zone)
        {
            string operation = string.Empty;
            string region = string.Empty;

            if (zone.Contains("-"))
            {
                string[] pieces = zone.Split('-');
                operation = pieces[0];
                region = pieces[1];
            }

            return (operation, region);
        }


        /// <summary>
        /// Creates a filename from the report name and the date.
        /// </summary>
        /// <returns></returns>
        private string BuildFileName()
        {
            string reportName = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox");
            string[] pieces = reportName.Split(' ');
            string baseName = string.Join("_", pieces);

            baseName = baseName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".xlsx";

            Logging.WriteLogLine("baseName: " + baseName);

            return baseName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="op"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        private List<Trip> FindTripsForOperationAndRegion(string op, string region)
        {
            List<Trip> found = null;

            if (dictTripsByOperationAndRegion[op] != null && dictTripsByOperationAndRegion[op][region] != null)
            {
                found = dictTripsByOperationAndRegion[op][region];
            }

            return found;
        }

        private void LoadTripsByPickupZone()
        {
            // Build the dictionaries
            foreach (Zone zone in rs.Zones)
            {
                (string o, string r) = ExtractOperation(zone.Name);
                if (o.IsNotNullOrWhiteSpace() && r.IsNotNullOrWhiteSpace())
                {
                    // Dictionary<operation, Dictionary<region, List<Trip>>>
                    if (!dictTripsByOperationAndRegion.ContainsKey(o)) {
                        dictTripsByOperationAndRegion.Add(o, new Dictionary<string, List<Trip>>());
                    }
                    if (!dictTripsByOperationAndRegion[o].ContainsKey(r))
                    {
                        dictTripsByOperationAndRegion[o].Add(r, new List<Trip>());
                    }
                }
            }

            foreach (Trip trip in trips)
            {
                (string o, string r) = ExtractOperation(trip.PickupZone);
                if (o.IsNotNullOrWhiteSpace() && r.IsNotNullOrWhiteSpace())
                {
                    if (dictTripsByOperationAndRegion[o] != null && dictTripsByOperationAndRegion[o][r] != null)
                    {
                        Logging.WriteLogLine("Adding trip" + trip.TripId + " to operation: " + o + ", region: " + r);
                        dictTripsByOperationAndRegion[o][r].Add(trip);
                    }
                }
                else
                {
                    Logging.WriteLogLine("Skipping trip " + trip.TripId + " with a bad PickupZone: " + trip.PickupZone);
                }
            }
        }


        private string ConvertColRowToAddress(int col, int row)
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string address = alphabet.Substring(col - 1, 1);
            address += row;

            return address;
        }

        #endregion

        #region Create Data and Populate

        /// <summary>
        /// Generates the summary data and populates the appropriate fields.
        /// Note: the percentage fields are formatted as percentages so no need to convert.
        /// </summary>
        private void CreateSummaryData()
        {
            int totalTrips = trips.Count;

            decimal notYetCompleted = 0;
            decimal futileStoreNotReady = 0;
            decimal futileNoStockToCollect = 0;
            decimal completePickedUp = 0;
            decimal verified = 0;
            decimal notYetAssigned = 0;

            foreach (Trip trip in trips)
            {
                switch (trip.Status1)
                {
                    case STATUS.DISPATCHED:
                        ++notYetCompleted;
                        break;
                    case STATUS.DELETED:
                        ++futileNoStockToCollect;
                        break;
                    case STATUS.ACTIVE:
                        ++futileStoreNotReady;
                        break;
                    case STATUS.PICKED_UP:
                        ++completePickedUp;
                        break;
                    case STATUS.VERIFIED:
                        ++verified;
                        break;
                    case STATUS.NEW:
                        ++notYetAssigned;
                        break;
                }

            }

            worksheet.Cells[CellSummaryNotYetCompleted].Value = notYetCompleted;
            worksheet.Cells[CellSummaryFutileNoStock].Value = futileNoStockToCollect;
            worksheet.Cells[CellSummaryFutileNotReady].Value = futileStoreNotReady;
            worksheet.Cells[CellSummaryComplete].Value = completePickedUp;
            worksheet.Cells[CellSummaryVerified].Value = verified;


            // Percentages
            if (notYetCompleted > 0)
            {
                decimal cellNotYetCompleted = notYetCompleted / totalTrips;
                worksheet.Cells[CellSummaryNotYetCompletedPercentage].Value = cellNotYetCompleted;
            }
            else
            {
                worksheet.Cells[CellSummaryNotYetCompletedPercentage].Value = "0%";
            }
            if (futileNoStockToCollect > 0)
            {
                decimal cellFutileNoStock = futileNoStockToCollect / totalTrips;
                worksheet.Cells[CellSummaryFutileNoStockPercentage].Value = cellFutileNoStock;
            }
            else
            {
                worksheet.Cells[CellSummaryFutileNoStockPercentage].Value = "0%";
            }
            if (futileStoreNotReady > 0)
            {
                decimal cellFutileStoreNotReady = futileStoreNotReady / totalTrips;
                worksheet.Cells[CellSummaryFutileNotReadyPercentage].Value = cellFutileStoreNotReady;
            }
            else
            {
                worksheet.Cells[CellSummaryFutileNotReadyPercentage].Value = "0%";
            }
            if (completePickedUp > 0)
            {
                decimal cellCompletePickedUp = completePickedUp / totalTrips;
                worksheet.Cells[CellSummaryCompletePercentage].Value = cellCompletePickedUp;
            }
            else
            {
                worksheet.Cells[CellSummaryCompletePercentage].Value = "0%";
            }
            if (verified > 0)
            {
                decimal cellVerified = verified / totalTrips;
                worksheet.Cells[CellSummaryVerifiedPercentage].Value = cellVerified;
            }
            else
            {
                worksheet.Cells[CellSummaryVerifiedPercentage].Value = "0%";
            }

            // Now work out the percentages for small grid to the right

            decimal cellSummaryAttended = (futileNoStockToCollect + futileStoreNotReady + completePickedUp) / totalTrips;
            if (cellSummaryAttended > 0)
            {
                worksheet.Cells[CellSummary2Attended].Value = cellSummaryAttended; 
            }
            else
            {
                worksheet.Cells[CellSummary2Attended].Value = "0%";
            }
            decimal cellSummaryVerified = verified / totalTrips;
            if (cellSummaryVerified > 0)
            {
                worksheet.Cells[CellSummary2Verified].Value = cellSummaryVerified; 
            }
            else
            {
                worksheet.Cells[CellSummary2Verified].Value = "0%";
            }
        }

        private void CreatePickedUpData()
        {
            int col = 1;
            int totalCols = 0;
            int startingRow = 13;
            int row = startingRow;

            decimal totalNotYetCompleted = 0;
            decimal totalAttended = 0;

            // First, get the operations and regions
            foreach (string op in rs.SelectedOperations)
            {
                Logging.WriteLogLine("operation: " + op);
                List<string> regions = FindRegionsForOperation(op);
                if (regions.Count > 0)
                {
                    foreach (string region in regions)
                    {
                        Logging.WriteLogLine("region: " + region);
                        row = startingRow; // Start at top again
                        ++col; // Next column

                        decimal cellNotYetCompleted = 0;
                        decimal cellFutileNotReady = 0;
                        decimal cellFutileNoStock = 0;
                        decimal cellCompletePickedUp = 0;
                        decimal cellVerified = 0;
                        decimal cellNotYetAssigned = 0;
                        // decimal cellNotYetCompleted = 0;
                        decimal cellAttended = 0;
                        decimal cellToBeCompletedPercentage = 0;
                        decimal cellCompletedPercentage = 0;
                        List<Trip> tripsForOperationAndRegion = FindTripsForOperationAndRegion(op, region);
                        Logging.WriteLogLine("Found " + tripsForOperationAndRegion.Count + " trips for " + op + "-" + region);
                        if (tripsForOperationAndRegion.Count > 0)
                        {
                            foreach (Trip trip in tripsForOperationAndRegion)
                            {
                                switch (trip.Status1)
                                {
                                    case STATUS.DISPATCHED:
                                        ++cellNotYetCompleted;
                                        break;
                                    case STATUS.DELETED:
                                        ++cellFutileNoStock;
                                        break;
                                    case STATUS.ACTIVE:
                                        ++cellFutileNotReady;
                                        break;
                                    case STATUS.PICKED_UP:
                                        ++cellCompletePickedUp;
                                        break;
                                    case STATUS.VERIFIED:
                                        ++cellVerified;
                                        break;
                                    case STATUS.NEW:
                                        ++cellNotYetAssigned;
                                        break;
                                }
                            }
                        }

                        // Populate the cells
                        worksheet.Cells[row++, col].Value = cellNotYetCompleted;
                        worksheet.Cells[row++, col].Value = cellFutileNotReady;
                        worksheet.Cells[row++, col].Value = cellFutileNoStock;
                        worksheet.Cells[row++, col].Value = cellCompletePickedUp;
                        worksheet.Cells[row++, col].Value = cellVerified;
                        worksheet.Cells[row++, col].Value = cellNotYetAssigned;

                        row += 2;  // Skip over total row and blank row

                        worksheet.Cells[row++, col].Value = cellNotYetCompleted;
                        //cellAttended = cellNotYetCompleted + cellFutileNotReady + cellFutileNoStock + cellCompletePickedUp + cellVerified;
                        cellAttended = cellFutileNotReady + cellFutileNoStock + cellCompletePickedUp + cellVerified;
                        worksheet.Cells[row++, col].Value = cellAttended;

                        ++row; // Skip over total row
                        /*
                        nyc	    5				            4       0
                        c	    0				            1       5

                        %nyc	100% nyc / total * 100		80%     0%
                        %c	    0%   c   / total * 100		20%     100%
                         */

                        if (cellNotYetCompleted != 0 && tripsForOperationAndRegion.Count != 0) {
                            cellToBeCompletedPercentage = cellNotYetCompleted / tripsForOperationAndRegion.Count;
                        }
                        if (cellToBeCompletedPercentage > 0)
                        {
                            worksheet.Cells[row++, col].Value = cellToBeCompletedPercentage;
                        }
                        else
                        {
                            worksheet.Cells[row++, col].Value = "0%";
                        }

                        if (cellNotYetCompleted != 0 && tripsForOperationAndRegion.Count != 0)
                        {
                            //cellCompletedPercentage = (trips.Count - cellNotYetCompleted) / trips.Count;
                            cellCompletedPercentage = cellAttended / tripsForOperationAndRegion.Count;
                        }
                        if (cellCompletedPercentage > 0)
                        {
                            worksheet.Cells[row++, col].Value = cellCompletedPercentage;
                        }
                        else
                        {
                            worksheet.Cells[row++, col].Value = "0%";
                        }

                        totalNotYetCompleted += cellNotYetCompleted;
                        totalAttended += cellAttended;

                        totalCols = col + 1;
                    }
                }
            }

            // Percentage totals for TOTAL column
            row = 24;
            col = totalCols;

            if (totalNotYetCompleted > 0)
            {
                worksheet.Cells[row++, col].Value = totalNotYetCompleted / trips.Count;
            }
            else
            {
                worksheet.Cells[row++, col].Value = "0%";
            }
            if (totalAttended > 0)
            {
                worksheet.Cells[row, col].Value = totalAttended / trips.Count;
            }
            else
            {
                worksheet.Cells[row, col].Value = "0%";
            }
        }


        #endregion

        #region Build Sections
        /// <summary>
        /// Notes and changes from scope (Gord):
        /// - Satchel trips are _NOT_excluded from the list of trips.
        /// - 6. “Not Yet Assigned” will be shipments with an IDS status of New - NOT Dispatched.
        /// </summary>
        /// <returns></returns>
        private (bool success, List<string> errors, string fileName) BuildReport()
        {
            bool success = true;
            List<string> errors = new List<string>();
            string fileName = BuildFileName();

            package = new ExcelPackage();
            // Add a new worksheet to the empty workbook
            string reportName = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox");
            worksheet = package.Workbook.Worksheets.Add(reportName);

            // Turns on readonly status for cells marked 'Locked'
            //worksheet.Protection.IsProtected = true;

            // Sections
            try
            {
                BuildSummarySection();
                BuildPickedUpSection();
                BuildTripsSection();
            }
            catch (Exception e)
            {
                success = false;
                errors.Add(e.Message);
            }
            if (success)
            {
                CreateSummaryData();
                CreatePickedUpData();

                // IMPORTANT Do this last, after the sheet has been fully populated
                worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
            }

            return (success, errors, fileName);
        }


        

        /// <summary>
        /// First header.
        /// Starts at A1.
        /// </summary>
        private void BuildSummarySection()
        {
            // Section Header
            worksheet.Cells["A1"].Value = "Summary";
            worksheet.Cells["A1:A1"].Style.Font.Bold = true;

            //Add the headers
            worksheet.Cells["A2"].Value = "Status";
            worksheet.Cells["B2"].Value = "P-Up";
            worksheet.Cells["C2"].Value = "%";

            using (var range = worksheet.Cells[2, 1, 2, 3]) // A2..C2
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(PrimaryHeaderBg);
                range.Style.Font.Color.SetColor(Color.White);
            }

            worksheet.Cells["A3"].Value = "Not Yet Completed";
            worksheet.Cells["A4"].Value = "Futile - No Stock to Collect";
            worksheet.Cells["A5"].Value = "Futile - Store not Ready";
            worksheet.Cells["A6"].Value = "Complete (Picked Up)";
            worksheet.Cells["A7"].Value = "Verified";

            worksheet.Cells["A8"].Value = "Total";
            worksheet.Cells["A8:C8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet.Cells["A8:C8"].Style.Font.Bold = true;

            using (var range = worksheet.Cells["A2:C8"])
            {
                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }
            using (var range = worksheet.Cells["B2:C8"])
            {
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }

            worksheet.Cells[8, 2].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(3, 2, 7, 2).Address);
            worksheet.Cells[8, 3].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(3, 3, 7, 3).Address);
            worksheet.Cells[3, 3, 8, 3].Style.Numberformat.Format = "#.##%";
            //worksheet.Cells[8, 2].Style.Locked = true;
            //worksheet.Cells[8, 3].Style.Locked = true;

            //            
            // Small section to right
            //
            worksheet.Cells["E4"].Value = "Attended";
            worksheet.Cells["E5"].Value = "Verified";
            worksheet.Cells["E4:E5"].Style.Font.Bold = true;
            worksheet.Cells["E4:F5"].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            using (var range = worksheet.Cells["E4:F5"])
            {
                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }

            using (var range = worksheet.Cells["F4:F5"])
            {
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.Numberformat.Format = "#.##%";
            }

        }

        /// <summary>
        /// Starts at [A11].
        /// 
        /// </summary>
        private void BuildPickedUpSection()
        {
            worksheet.Cells[11, 1].Value = "PickedUp";
            worksheet.Cells[11, 1].Style.Font.Bold = true;

            int startingCol = 2;
            int row = 11;
            int col = startingCol;
            foreach (string op in rs.SelectedOperations)
            {
                int regionCount = FindRegionsForOperation(op).Count;
                if (regionCount == 1)
                {
                    worksheet.Cells[row, col].Value = op;
                    worksheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    col += 1;
                }
                else
                {
                    worksheet.Cells[row, col, row, col + regionCount - 1].Merge = true;
                    worksheet.Cells[row, col].Value = op;
                    worksheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    col += regionCount; // Skip to next set
                }
            }
                        
            using (var range = worksheet.Cells[row, startingCol, row, col - 1])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(SecondaryHeaderBg);
                range.Style.Font.Color.SetColor(Color.White);

                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }

            using (var range = worksheet.Cells[row, startingCol + 1, row, col - 1])
            {
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }

            row = 12;
            worksheet.Cells[row, 1].Value = "Status";

            col = startingCol;
            foreach (string op in rs.SelectedOperations)
            {
                List<string> regions = FindRegionsForOperation(op);
                foreach (string region in regions)
                {
                    worksheet.Cells[row, col].Value = region;
                    worksheet.Cells.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    ++col;
                }
            }
            int totalCols = col;

            worksheet.Cells[row, col].Value = "TOTAL";

            using (var range = worksheet.Cells[row, 1, row, col])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(PrimaryHeaderBg);
                range.Style.Font.Color.SetColor(Color.White);
            }

            int startingRow = row;
            ++row;
            startingCol = 1;
            worksheet.Cells[row++, startingCol].Value = "Not Yet Completed";
            worksheet.Cells[row++, startingCol].Value = "Futile - Store not Ready";
            worksheet.Cells[row++, startingCol].Value = "Futile - No stock to Collect";
            worksheet.Cells[row++, startingCol].Value = "Complete (Picked Up)";
            worksheet.Cells[row++, startingCol].Value = "Verified";
            worksheet.Cells[row++, startingCol].Value = "Not Yet Assigned";
            worksheet.Cells[row, startingCol].Value = "Total";
            using (var range = worksheet.Cells[row, startingCol, row, totalCols])
            {
                range.Style.Font.Bold = true;
            }

            using (var range = worksheet.Cells[startingRow, startingCol, row, totalCols])
            {
                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }

            // Right-hand totals
            // TODO need to fix formula
            //worksheet.Cells[13, totalCols].Formula = "SUM(B13:F13)";
            string startAddress = ConvertColRowToAddress(2, 13);
            string endAddress = ConvertColRowToAddress(totalCols - 1, 13);
            string formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[13, totalCols].Formula = formula;
            
            //worksheet.Cells[14, totalCols].Formula = "SUM(B14:F14)";
            startAddress = ConvertColRowToAddress(2, 14);
            endAddress = ConvertColRowToAddress(totalCols - 1, 14);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[14, totalCols].Formula = formula;

            //worksheet.Cells[15, totalCols].Formula = "SUM(B15:F15)";
            startAddress = ConvertColRowToAddress(2, 15);
            endAddress = ConvertColRowToAddress(totalCols - 1, 15);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[15, totalCols].Formula = formula;

            //worksheet.Cells[16, totalCols].Formula = "SUM(B16:F16)";
            startAddress = ConvertColRowToAddress(2, 16);
            endAddress = ConvertColRowToAddress(totalCols - 1, 16);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[16, totalCols].Formula = formula;

            //worksheet.Cells[17, totalCols].Formula = "SUM(B17:F17)";
            startAddress = ConvertColRowToAddress(2, 17);
            endAddress = ConvertColRowToAddress(totalCols - 1, 17);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[17, totalCols].Formula = formula;

            //worksheet.Cells[18, totalCols].Formula = "SUM(B18:F18)";
            startAddress = ConvertColRowToAddress(2, 18);
            endAddress = ConvertColRowToAddress(totalCols - 1, 18);

            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[18, totalCols].Formula = formula;

            // Bottom totals
            //worksheet.Cells[19, 2].Formula = "SUM(B13:B18)";
            //worksheet.Cells[19, 3].Formula = "SUM(C13:C18)";
            //worksheet.Cells[19, 4].Formula = "SUM(D13:D18)";
            //worksheet.Cells[19, 5].Formula = "SUM(E13:E18)";
            //worksheet.Cells[19, 6].Formula = "SUM(F13:F18)";
            //worksheet.Cells[19, 7].Formula = "SUM(G13:G18)";
            for (int i = 2; i <= totalCols; i++)
            {
                startAddress = ConvertColRowToAddress(i, 13);
                endAddress = ConvertColRowToAddress(i, 18);
                formula = "SUM(" + startAddress + ":" + endAddress + ")";
                worksheet.Cells[19, i].Formula = formula;
            }

            //
            // Totals section
            //

            row += 2;
            startingRow = row;
            worksheet.Cells[row++, startingCol].Value = "Not Yet Completed";
            worksheet.Cells[row++, startingCol].Value = "Stores Attended";
            worksheet.Cells[row++, startingCol].Value = "Total";
            using (var range = worksheet.Cells[row - 1, startingCol, row - 1, totalCols + 1])
            {
                range.Style.Font.Bold = true;
            }
            worksheet.Cells[row++, startingCol].Value = "% To be Completed";
            worksheet.Cells[row++, startingCol].Value = "% Completed";
            using (var range = worksheet.Cells[startingRow, startingCol, row - 1, totalCols])
            {
                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }

            using (var range = worksheet.Cells[13, 2, row - 1, totalCols])
            {
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }

            // Right-hand totals
            //worksheet.Cells[21, totalCols].Formula = "SUM(B21:F21)";
            startAddress = ConvertColRowToAddress(2, 21);
            endAddress = ConvertColRowToAddress(totalCols - 1, 21);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[21, totalCols].Formula = formula;

            //worksheet.Cells[22, totalCols].Formula = "SUM(B22:F22)";
            startAddress = ConvertColRowToAddress(2, 22);
            endAddress = ConvertColRowToAddress(totalCols - 1, 22);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[22, totalCols].Formula = formula;

            // Totals row
            //worksheet.Cells[23, 2].Formula = "SUM(B21:B22)";            
            //worksheet.Cells[23, 3].Formula = "SUM(C21:C22)";
            //worksheet.Cells[23, 4].Formula = "SUM(D21:D22)";
            //worksheet.Cells[23, 5].Formula = "SUM(E21:E22)";
            //worksheet.Cells[23, 6].Formula = "SUM(F21:F22)";
            //worksheet.Cells[23, 7].Formula = "SUM(G21:G22)";
            for (int i = 2; i < totalCols; i++)
            {
                startAddress = ConvertColRowToAddress(i, 21);
                endAddress = ConvertColRowToAddress(i, 22);
                formula = "SUM(" + startAddress + ":" + endAddress + ")";
                worksheet.Cells[23, i].Formula = formula;
            }
            startAddress = ConvertColRowToAddress(totalCols, 21);
            endAddress = ConvertColRowToAddress(totalCols, 22);
            formula = "SUM(" + startAddress + ":" + endAddress + ")";
            worksheet.Cells[23, totalCols].Formula = formula;


            // Percentage totals
            //worksheet.Cells[24, 7].Formula = "SUM(B24:G24)";
            //worksheet.Cells[25, 7].Formula = "SUM(B25:G25)";

            using (var range = worksheet.Cells[24, 2, 25, totalCols])
            {
                range.Style.Numberformat.Format = "#.##%";
            }
        }

        /// <summary>
        /// Starts at A28.
        /// 
        /// </summary>
        private void BuildTripsSection()
        {
            // Build header
            int startingCol = 1;
            int row = 28;
            int col = startingCol;
            int totalCols = 13;
            worksheet.Cells[row, col++].Value = "Date Entered";
            worksheet.Cells[row, col++].Value = "Billing Notes";
            worksheet.Cells[row, col++].Value = "Driver";
            worksheet.Cells[row, col++].Value = "Operation";
            worksheet.Cells[row, col++].Value = "Region";
            worksheet.Cells[row, col++].Value = "P/U Co";
            worksheet.Cells[row, col++].Value = "P/U";
            worksheet.Cells[row, col++].Value = "Pcity";
            worksheet.Cells[row, col++].Value = "Status";
            worksheet.Cells[row, col++].Value = "Days Old";
            worksheet.Cells[row, col++].Value = "Days Since Status";
            worksheet.Cells[row, col++].Value = "Date of PU";
            worksheet.Cells[row, col++].Value = "PU Notes";
            using (var range = worksheet.Cells[row, startingCol, row, totalCols])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(PrimaryHeaderBg);
                range.Style.Font.Color.SetColor(Color.White);

                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                range.Style.Border.BorderAround(ExcelBorderStyle.Thick);
            }


            // Now for the trips
            int startingRow = 29;
            row = startingRow;
            foreach (Trip trip in tripsInOrder)
            {
                Logging.WriteLogLine("Adding trip: " + trip.TripId + " - CallTime: " + trip.CallTime);
                worksheet.Cells[row, 1].Value = trip.CallTime.ToString("dd/MM/yyyy");
                worksheet.Cells[row, 2].Value = trip.BillingNotes;
                worksheet.Cells[row, 3].Value = trip.Driver;
                    
                (string op, string region) = ExtractOperation(trip.PickupZone);
                worksheet.Cells[row, 4].Value = op;
                worksheet.Cells[row, 5].Value = region;
                    
                worksheet.Cells[row, 6].Value = trip.PickupCompanyName;
                worksheet.Cells[row, 7].Value = trip.PickupAddressAddressLine1;
                worksheet.Cells[row, 8].Value = trip.PickupAddressCity;
                worksheet.Cells[row, 9].Value = trip.Status1;
                    
                DateTimeOffset now = DateTimeOffset.Now;
                int daysOld = (now - trip.CallTime).Days;
                worksheet.Cells[row, 10].Value = daysOld;
                if (daysOld > rs.SelectedExceedDays)
                {
                    worksheet.Cells[row, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, 10].Style.Fill.BackgroundColor.SetColor(Color.Red);
                }
                        
                int daysOldStatus = 0;
                switch (trip.Status1)
                {
                    case STATUS.DISPATCHED:
                        // TODO
                        break;
                    case STATUS.PICKED_UP:
                        if (trip.PickupTime < now)
                        {
                            daysOldStatus = (now - trip.PickupTime).Days;
                        }
                        break;
                    case STATUS.VERIFIED:
                        if (trip.VerifiedTime < now)
                        {
                            daysOldStatus = (now - trip.VerifiedTime).Days;
                        }
                        break;
                    case STATUS.DELETED:
                        // TODO
                        break;
                }
                worksheet.Cells[row, 11].Value = daysOldStatus;
                if (daysOldStatus > rs.SelectedExceedDays)
                {
                    worksheet.Cells[row, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, 11].Style.Fill.BackgroundColor.SetColor(Color.Red);
                }

                worksheet.Cells[row, 12].Value = trip.PickupTime.ToString("dd/MM/yyyy");
                worksheet.Cells[row, 12].Value = trip.PickupNotes;

                ++row;
            }
        }

        #endregion


    }
}
