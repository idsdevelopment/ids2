﻿using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Utils;
using ViewModels.Dialogues;
using ViewModels.ReportBrowser.CreateExcel;

namespace ViewModels.ReportBrowser
{
    class ReportBrowserModel : ViewModelBase
    {
        public static readonly string PROGRAM = "ReportBrowser (" + Globals.CurrentVersion.APP_VERSION + ")";

        public bool Loaded
        {
            get { return Get(() => Loaded, false); }
            set { Set(() => Loaded, value); }
        }

        protected override void OnInitialised()
        {
            base.OnInitialised();
            Loaded = false;

            Dispatcher.Invoke(() =>
            {
                LoadReports();
                LoadGroups();
                LoadZones();

                Loaded = true;
            });

            //Task.WaitAll(Task.Run(() =>
            //{
            //    Dispatcher.Invoke(() =>
            //    {
            //        LoadReports();
            //        LoadGroups();
            //        LoadZones();

            //        Loaded = true;
            //    });
            //}));
        }


        #region Data

        public List<ReportSettings> Reports
        {
            //get { return Get(() => Reports, new SortedSet<ReportSettings>()); }
            get { return Get(() => Reports, new List<ReportSettings>()); }
            set { Set(() => Reports, value); }
        }


        public List<string> ReportNames
        {
            get { return Get(() => ReportNames, new List<string>()); }
            set { Set(() => ReportNames, value); }
        }


        public ReportSettings SelectedReportSettings
        {
            //get { return Get(() => SelectedReportSettings, new ReportSettings()); }
            get { return Get(() => SelectedReportSettings); }
            set { Set(() => SelectedReportSettings, value); }
        }


        [DependsUpon(nameof(SelectedReportSettings))]
        public void WhenSelectedReportSettingsChanges()
        {
            if (SelectedReportSettings != null)
            {
                Logging.WriteLogLine("Setting SelectedReportSettings to " + SelectedReportSettings.Description);
                Dispatcher.Invoke(() =>
                {
                    IsRunButtonEnabled = true;
                    // Turn on/off the fields
                    ShowSelectFrom = SelectedReportSettings.ShowSelectFrom;
                    ShowSelectTo = SelectedReportSettings.ShowSelectTo;
                    ShowSelectPackageType = SelectedReportSettings.ShowSelectPackageType;
                    ShowSelectOperation = SelectedReportSettings.ShowSelectOperation;
                    ShowSelectGroup = SelectedReportSettings.ShowSelectGroup;
                    ShowFromStatus = SelectedReportSettings.ShowSelectFromStatus;
                    ShowToStatus = SelectedReportSettings.ShowSelectToStatus;
                    ShowSelectExceedDays = SelectedReportSettings.ShowSelectExceedDays;

                    SelectedFrom = SelectedTo = DateTimeOffset.Now;
                });
            }
            else
            {
                IsRunButtonEnabled = false;
            }
        }

        [DependsUpon(nameof(SelectedReportSettings))]
        public bool IsRunButtonEnabled
        {
            get { return Get(() => IsRunButtonEnabled, false); }
            set { Set(() => IsRunButtonEnabled, value); }
        }


        public DateTimeOffset SelectedFrom
        {
            get { return Get(() => SelectedFrom, DateTimeOffset.Now); }
            set { Set(() => SelectedFrom, value); }
        }


        public bool ShowSelectFrom
        {
            get { return Get(() => ShowSelectFrom, true); }
            set { Set(() => ShowSelectFrom, value); }
        }


        public DateTimeOffset SelectedTo
        {
            get { return Get(() => SelectedTo, DateTimeOffset.Now); }
            set { Set(() => SelectedTo, value); }
        }


        public bool ShowSelectTo
        {
            get { return Get(() => ShowSelectTo, true); }
            set { Set(() => ShowSelectTo, value); }
        }


        public List<string> PackageTypes
        {
            get { return Get(() => PackageTypes, GetPackageTypes); }
            set { Set(() => PackageTypes, value); }
        }

        private List<string> GetPackageTypes()
        {
            List<string> pts = new List<string>
            {
                "Return",
                "Satchel",
                "All"
            };

            return pts;
        }

        public string SelectedPackageType
        {
            get { return Get(() => SelectedPackageType, ReportSettings.All); }
            set { Set(() => SelectedPackageType, value); }
        }


        public bool ShowSelectPackageType
        {
            get { return Get(() => ShowSelectPackageType, true); }
            set { Set(() => ShowSelectPackageType, value); }
        }


        public List<string> Operations
        {
            get { return Get(() => Operations, new List<string>()); }
            set { Set(() => Operations, value); }
        }



        public string SelectedOperation
        {
            get { return Get(() => SelectedOperation); }
            set { Set(() => SelectedOperation, value); }
        }


        public bool ShowSelectOperation
        {
            get { return Get(() => ShowSelectOperation, true); }
            set { Set(() => ShowSelectOperation, value); }
        }


        public List<CompanyAddressGroup> Groups
        {
            get { return Get(() => Groups, LoadGroups); }
            set { Set(() => Groups, value); }
        }


        public List<string> GroupNames
        {
            get { return Get(() => GroupNames, new List<string>()); }
            set { Set(() => GroupNames, value); }
        }


        /// <summary>
        /// Holds a map of group names and their companies.
        /// </summary>
        private readonly SortedDictionary<string, List<string>> GroupsAndMembers = new SortedDictionary<string, List<string>>();


        public string SelectedGroupName
        {
            get { return Get(() => SelectedGroupName, ""); }
            set { Set(() => SelectedGroupName, value); }
        }


        public bool ShowSelectGroup
        {
            get { return Get(() => ShowSelectGroup, true); }
            set { Set(() => ShowSelectGroup, value); }
        }


        public bool EnableGroupList
        {
            get { return Get(() => EnableGroupList, false); }
            set { Set(() => EnableGroupList, value); }
        }


        public bool IsAllGroupsSelected
        {
            get { return Get(() => IsAllGroupsSelected, true); }
            set { Set(() => IsAllGroupsSelected, value); }
        }

        [DependsUpon(nameof(IsAllGroupsSelected))]
        public void WhenAllGroupsSelected()
        {
            if (IsAllGroupsSelected)
            {
                SelectedGroupName = ReportSettings.All;
                EnableGroupList = false;
            }
            else
            {
                EnableGroupList = true;
            }
        }

        public List<string> SelectedGroupNames = new List<string>();

        public List<string> FromStatus
        {
            get { return Get(() => FromStatus, GetPublicStatusStrings()); }
            set { Set(() => FromStatus, value); }
        }

        private List<string> GetPublicStatusStrings()
        {
            var strings = new List<string>
                          {
                              //STATUS.UNSET.AsString(),
                              STATUS.NEW.AsString(),
                              STATUS.ACTIVE.AsString(),
                              STATUS.DISPATCHED.AsString(),
                              //STATUS.PICKED_UP.AsString(),
                              //STATUS.DELIVERED.AsString(),
                              STATUS.VERIFIED.AsString(),
                              //STATUS.POSTED.AsString(),
                              STATUS.INVOICED.AsString(),
                              //STATUS.SCHEDULED.AsString(),
                              //STATUS.PAID.AsString(),
                              STATUS.DELETED.AsString()
                          };

            return strings;
        }


        public string SelectedFromStatus
        {
            get { return Get(() => SelectedFromStatus, FromStatus[0]); }
            set { Set(() => SelectedFromStatus, value); }
        }


        public bool ShowFromStatus
        {
            get { return Get(() => ShowFromStatus, true); }
            set { Set(() => ShowFromStatus, value); }
        }


        public List<string> ToStatus
        {
            get { return Get(() => ToStatus, GetPublicStatusStrings()); }
            set { Set(() => ToStatus, value); }
        }


        public string SelectedToStatus
        {
            get { return Get(() => SelectedToStatus, ToStatus[3]); }
            set { Set(() => SelectedToStatus, value); }
        }


        public bool ShowToStatus
        {
            get { return Get(() => ShowToStatus, true); }
            set { Set(() => ShowToStatus, value); }
        }


        public int SelectedExceedDays
        {
            get { return Get(() => SelectedExceedDays, 7); }
            set { Set(() => SelectedExceedDays, value); }
        }


        public bool ShowSelectExceedDays
        {
            get { return Get(() => ShowSelectExceedDays, true); }
            set { Set(() => ShowSelectExceedDays, value); }
        }



        public Zones Zones
        {
            get { return Get(() => Zones); }
            set { Set(() => Zones, value); }
        }


        public List<string> ZoneNames
        {
            get { return Get(() => ZoneNames, new List<string>()); }
            set { Set(() => ZoneNames, value); }
        }


        public Zone SelectedZone
        {
            get { return Get(() => SelectedZone); }
            set { Set(() => SelectedZone, value); }
        }


        public bool ShowSelectZone
        {
            get { return Get(() => ShowSelectZone, true); }
            set { Set(() => ShowSelectZone, value); }
        }


        #endregion
        #region DataMethods

        /// <summary>
        /// Starts the process of loading all group lists.
        /// </summary>
        /// <returns></returns>
        private List<CompanyAddressGroup> LoadGroups()
        {
            Logging.WriteLogLine("Loading groups");
            List<CompanyAddressGroup> ags = new List<CompanyAddressGroup>();

            Task.Run(async () =>
            {
                CompanyAddressGroups cags = null;

                try
                {
                    cags = await Azure.Client.RequestGetCompanyAddressGroups();
                }
                catch (Exception e)
                {
                    Logging.WriteLogLine("Exception thrown: " + e.ToString());
                }

                if (cags != null)
                {
                    foreach (CompanyAddressGroup cag in cags)
                    {
                        ags.Add(cag);
                    }

                    Logging.WriteLogLine("Found " + ags.Count + " groups");

                    Groups = ags;
                    LoadGroupNames();
                }
            });

            return ags;
        }


        private void LoadGroupNames()
        {
            Task.Run(async () =>
            {
                SortedDictionary<string, string> sd = new SortedDictionary<string, string>();
                SortedDictionary<string, int> counts = new SortedDictionary<string, int>();
                GroupsAndMembers.Clear();

                foreach (CompanyAddressGroup cag in Groups)
                {
                    if (!sd.ContainsKey(cag.Description.ToLower()))
                    {
                        sd.Add(cag.Description.ToLower(), cag.Description);

                        // Now get the number of companies in this group
                        try
                        {
                            CompaniesWithinAddressGroups companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup(cag.GroupNumber);
                            if (companies.Count > 0 && companies[0].Companies != null)
                            {
                                int count = companies[0].Companies.Count;
                                counts.Add(cag.Description.ToLower(), count);

                                SortedDictionary<string, string> sd1 = new SortedDictionary<string, string>();

                                foreach (string company in companies[0].Companies)
                                {
                                    Logging.WriteLogLine("Adding: " + company.ToLower());
                                    if (!sd1.ContainsKey(company.ToLower()))
                                    {
                                        sd1.Add(company.ToLower(), company);
                                    }
                                    else
                                    {
                                        Logging.WriteLogLine("Duplicate found: " + company);
                                    }
                                }

                                List<string> sorted = new List<string>();

                                foreach (string key in sd1.Keys)
                                {
                                    sorted.Add(sd1[key]);
                                }

                                GroupsAndMembers.Add(cag.Description, sorted);
                            }
                        }
                        catch (Exception e)
                        {
                            Logging.WriteLogLine("Exception thrown: " + e.ToString());
                        }
                    }
                }

                string members = (string)Application.Current.TryFindResource("AddressBookGroupsMembers");
                string labelSingle = (string)Application.Current.TryFindResource("AddressBookGroupsMembersCountSingle");
                string labelNotSingle = (string)Application.Current.TryFindResource("AddressBookGroupsMembersCountNotSingle");
                List<string> list = new List<string>();

                foreach (string key in sd.Keys)
                {
                    string line = sd[key] + " \t(" + counts[key] + " ";

                    if (counts[key] != 1)
                    {
                        //line = sd[key] + " \t(" + counts[key] + " members)"; 
                        line += labelNotSingle + ")";
                    }
                    else
                    {
                        line += labelSingle + ")";
                    }

                    list.Add(line);
                }

                GroupNames = list;

                //if (SelectedAddress == null)
                //{
                //    LoadAddressesGroupNames();
                //    Loaded = true;
                //}
                //else
                //{
                //    Loaded = true;
                //}

            });
        }


        /// <summary>
        /// Needed for Operations.
        /// </summary>
        private void LoadZones()
        {
            Task.Run(async () =>
            {
                List<Zone> newZones = new List<Zone>();
                List<string> newZoneNames = new List<string>();
                //ZoneNames.Clear();
                //newZoneNames.Add("All");
                Zones = await Azure.Client.RequestZones();
                if (Zones != null && Zones.Count > 0)
                {
                    foreach (Zone zone in Zones)
                    {
                        //newZones.Add(zone);
                        newZoneNames.Add(zone.Name);
                    }
                    ZoneNames = newZoneNames;
                    //Zones.AddRange(newZones);
                }
                else
                {
                    // Testing
                    Zone zone = new Zone
                    {
                        Name = "VWDerrimut-Metro",
                        SortIndex = 0
                    };
                    newZones.Add(zone);
                    newZoneNames.Add(zone.Name);
                    zone = new Zone
                    {
                        Name = "VWDerrimut-Regional",
                        SortIndex = 1
                    };
                    newZones.Add(zone);
                    newZoneNames.Add(zone.Name);
                    zone = new Zone
                    {
                        Name = "SA-Metro",
                        SortIndex = 2
                    };
                    newZones.Add(zone);
                    newZoneNames.Add(zone.Name);
                    zone = new Zone
                    {
                        Name = "SA-Regional",
                        SortIndex = 3
                    };
                    newZones.Add(zone);
                    newZoneNames.Add(zone.Name);
                    zone = new Zone
                    {
                        Name = "TAS-TAS",
                        SortIndex = 4
                    };
                    newZones.Add(zone);
                    newZoneNames.Add(zone.Name);

                    ZoneNames = newZoneNames;
                    Zones.AddRange(newZones);
                }

                List<string> newOperations = new List<string>();
                foreach (string zone in ZoneNames)
                {
                    string operation = ExtractOperation(zone);
                    if (operation.IsNotNullOrWhiteSpace() && !newOperations.Contains(operation))
                    {
                        newOperations.Add(operation);
                    }
                }
                newOperations.Insert(0, "All");
                Operations = newOperations;
            });
        }

        private string ExtractOperation(string zone)
        {
            string operation = string.Empty;
            if (zone.Contains("-"))
            {
                string[] pieces = zone.Split('-');
                operation = pieces[0];
            }

            return operation;
        }


        /// <summary>
        /// TODO Move into db.
        /// </summary>
        private void LoadReports()
        {
            List<ReportSettings> listReports = new List<ReportSettings>();
            ReportSettings report = CreateLinfoxReport();

            listReports.Add(report);
            //ReportNames.Add(SelectedReportSettings.Description);

            Reports = listReports;
        }

        private ReportSettings CreateLinfoxReport()
        {
            Logging.WriteLogLine("Creating Shipment Report");
            ReportSettings report = new ReportSettings()
            {
                Description = (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox"),
                Zones = this.Zones
            };

            return report;
        }

        #endregion

        #region Actions

        public void Execute_Clear()
        {
            Logging.WriteLogLine("Clearing the tab");

            SelectedReportSettings = null;

            SelectedExceedDays = 7;
            SelectedFrom = DateTimeOffset.Now;
            SelectedFromStatus = FromStatus[0];
            SelectedGroupName = string.Empty;
            SelectedOperation = Operations[0];
            SelectedPackageType = PackageTypes[0];
            SelectedTo = DateTimeOffset.Now;
            SelectedToStatus = ToStatus[0];
        }

        public void Execute_Run()
        {
            Logging.WriteLogLine("Running the report");

            if (SelectedReportSettings != null)
            {
                SelectedReportSettings.Zones = new List<Zone>();
                SelectedReportSettings.Zones.AddRange(Zones);

                SelectedReportSettings.SelectedFrom = SelectedFrom;
                SelectedReportSettings.SelectedTo = SelectedTo.AddDays(1); // Set it to the beginning of the next day (12:00 am)
                SelectedReportSettings.SelectedPackageType = SelectedPackageType;
                SelectedReportSettings.SelectedOperations.Clear();
                if (SelectedOperation == ReportSettings.All)
                {
                    SelectedReportSettings.SelectedOperations.AddRange(Operations);
                    SelectedReportSettings.SelectedOperations.Remove(ReportSettings.All);
                }
                else
                {
                    SelectedReportSettings.SelectedOperations.Add(SelectedOperation);
                }
                SelectedReportSettings.SelectedGroups.Clear();
                if (IsAllGroupsSelected)
                {
                    SelectedReportSettings.SelectedGroups.Add(ReportSettings.All);
                }
                else
                {
                    SelectedReportSettings.SelectedGroups.AddRange(SelectedGroupNames);
                }
                SelectedReportSettings.SelectedFromStatus = SelectedFromStatus;
                SelectedReportSettings.SelectedToStatus = SelectedToStatus;

                Logging.WriteLogLine("Report: " + SelectedReportSettings.ToString());

                //Dispatcher.Invoke(() =>
                //{
                //    ReportDialog rd = new ReportDialog(SelectedReportSettings);
                //    rd.Show();
                //});

                if (SelectedReportSettings.Description == (string)Application.Current.TryFindResource("ReportBrowserReportsDescriptionsLinfox"))
                {
                    RunLinfoxShipmentReport(SelectedReportSettings);
                }

            }
        }
        
        /// <summary>
        /// This doesn't load a report in ReportDialog - it creates an Excel
        /// spreadsheet on disk.
        /// </summary>
        /// <param name="rs"></param>
        private void RunLinfoxShipmentReport(ReportSettings rs)
        {
            Logging.WriteLogLine("Running Linfox shipment report");

            CreateLinfoxShipmentReport rpt = new CreateLinfoxShipmentReport();

            (bool success, List<string> errors, string fileName) = rpt.Run(rs);
            string title = string.Empty;
            string message = string.Empty;
            MessageBoxImage mbi = MessageBoxImage.Information;
            if (success)
            {
                Logging.WriteLogLine("Created report in " + fileName);

                // TODO inform user
                title = (string)Application.Current.TryFindResource("ReportBrowserReportsReportCreatedTitle");
                message = (string)Application.Current.TryFindResource("ReportBrowserReportsReportCreated");
                message += "\n" + fileName;
            }
            else
            {
                string tmp = string.Join(", ", errors);
                Logging.WriteLogLine("Errors: " + tmp);

                // TODO inform user
                title = (string)Application.Current.TryFindResource("ReportBrowserReportsErrorsTitle");
                message = (string)Application.Current.TryFindResource("ReportBrowserReportsErrorsFound");
                foreach (string line in errors)
                {
                    message += "\n\t" + line;
                }
                mbi = MessageBoxImage.Error;
            }
            Dispatcher.Invoke(() =>
            {
                MessageBox.Show(message, title, MessageBoxButton.OK, mbi);
            });

        }

        /// <summary>
        /// TODO - need a URI for ReportBrowser.
        /// </summary>        
        public void Execute_ShowHelp()
        {

        }

        #endregion

    }
    
    /// <summary>
    /// Holds the settings for the report fields visibility.
    /// TODO - put into db.
    /// </summary>
    public class ReportSettings
        {
            public static readonly string All = "All";

            public string Description { get; set; }

            public string CompanyCode { get; set; }

            // They will select Return, Up or All.
            public static readonly List<string> PackageType = new List<string> { "Return", "Up", All };

            // They will select a “From” and a “To” date from a Calendar Dropdown
            public bool ShowSelectFrom { get; set; }
            public bool ShowSelectTo { get; set; }
            public DateTimeOffset SelectedFrom { get; set; }
            public DateTimeOffset SelectedTo { get; set; }

            // They will select Return, Upliftcomplete or All
            public bool ShowSelectPackageType { get; set; }
            public string SelectedPackageType { get; set; }

            // They will select 1 Operation or All.
            public bool ShowSelectOperation { get; set; }
            //public string SelectedOperation { get; set; }
            public List<string> SelectedOperations { get; set; }

            // They will select a Group from a dropdown.
            public bool ShowSelectGroup { get; set; }
            public List<string> SelectedGroups { get; set; }

            // They will select a From Status
            public bool ShowSelectFromStatus { get; set; }
            public string SelectedFromStatus { get; set; }

            // They will select a To Status
            public bool ShowSelectToStatus { get; set; }
            public string SelectedToStatus { get; set; }

            // They will enter a number into a field labelled, 'Exceeds Days'. This will default to the number, 7.
            public bool ShowSelectExceedDays { get; set; }
            public int SelectedExceedDays { get; set; }

            public List<Zone> Zones { get; set; }

            /// <summary>
            /// Sets some default settings.
            /// </summary>
            public ReportSettings()
            {
                ShowSelectFrom = ShowSelectTo = ShowSelectPackageType = ShowSelectOperation = 
                    ShowSelectGroup = ShowSelectFromStatus = ShowSelectToStatus = ShowSelectExceedDays = true;

                SelectedOperations = new List<string>();
                SelectedGroups = new List<string>();
                SelectedExceedDays = 7;                
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Description: " + Description);
                sb.Append(", CompanyCode: " + CompanyCode);
                sb.Append(", SelectedFrom: " + SelectedFrom);
                sb.Append(", SelectedTo: " + SelectedTo);
                sb.Append(", SelectedPackageType: " + SelectedPackageType);
                //sb.Append(", SelectedOperation: " + SelectedOperation);
                foreach (string op in SelectedOperations)
                {
                    sb.Append(", SelectedOperation: " + op);
                }
                foreach (string tmp in SelectedGroups)
                {
                    sb.Append(", SelectedGroup: " + tmp);
                }
                sb.Append(", SelectedFromStatus: " + SelectedFromStatus);
                sb.Append(", SelectedToStatus: " + SelectedToStatus);
                sb.Append(", SelectedExceedDays: " + SelectedExceedDays);

                return sb.ToString();
            }
        }
}
