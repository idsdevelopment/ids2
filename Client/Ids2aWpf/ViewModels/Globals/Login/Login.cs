﻿using System;

namespace ViewModels
{
	public static partial class Globals
	{
		public static class Login
		{
			public static Action _SignOut;

			public static void SignOut()
			{
				_SignOut?.Invoke();
			}
		}
	}
}