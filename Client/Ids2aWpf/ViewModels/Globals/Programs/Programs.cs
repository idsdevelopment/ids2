﻿using System;
using System.Collections.Generic;
using ViewModels._Customers.Pml;
using ViewModels._Customers.Priority.Routes;
using ViewModels._Diagnostics.ServerTime;
using ViewModels._Diagnostics.StressTest;
using ViewModels._Ids;
using ViewModels._Ids.Backup;
using ViewModels.AuditTrails;
using ViewModels.Customers.Accounts;
using ViewModels.Customers.AddressBook;
using ViewModels.Roles;
using ViewModels.Staff;
using ViewModels.Trips.Boards;
using ViewModels.Trips.SearchTrips;
using ViewModels.Trips.TripEntry;
using ViewModels.Uplift.Products;
using ViewModels.Uplift.Shipments;

namespace ViewModels
{
	public static partial class Globals
	{
		// These names are used in the client log file
		public const string LOGIN = "Login",
		                    CARRIER_CREATE_WIZARD = "IDS/Wizard",
		                    MAINTAIN_SETTINGS = "Maintain Settings",
		                    MAINTAIN_STAFF = "Maintain Staff",
		                    SEARCH_TRIPS = "Search Shipments",
		                    DISPATCH_BOARD = "Dispatch Board",
		                    DRIVERS_BOARD = "Drivers Board",
		                    STAFF_AUDIT_TRAIL = "Staff Audit Trail",
		                    TRIP_AUDIT_TRAIL = "Trip Audit Trail",
		                    PRIORITY_ROUTES = "Priority Maintain Routes",
		                    TRIP_ENTRY = "Shipment Entry",
		                    MAINTAIN_ROLES = "Maintain Roles",
		                    RATE_WIZARD = "Rate Wizard",
		                    ADDRESS_BOOK = "Address Book",
		                    ACCOUNTS = "Accounts",
		                    PML_INVENTORY = "Pml Inventory",
		                    UPLIFT_PRODUCTS = "Uplift Products",
		                    UPLIFT_SHIPMENTS = "Uplift Shipments",
		                    REPORT_BROWSER = "Reports",

							// Ids
		                    BACKUP_RESTORE = "Backup/Restore",

							// Diagnostics
							SERVER_TIME = "Server Time",
		                    STRESS_TEST = "Stress Test";

		public class Program
		{
			public bool AllowClose = true;

			public bool CanView,
			            CanEdit,
			            CanDelete,
			            CanCreate;

			public Type PageType;
			public string[] RelatedPrograms = new string[ 0 ];
		}

		public static Dictionary<string, Program> Programs = new Dictionary<string, Program>
		                                                     {
			                                                     {
				                                                     LOGIN,
				                                                     new Program
				                                                     {
					                                                     PageType   = typeof( StartPage.StartPage ),
					                                                     AllowClose = false
				                                                     }
			                                                     },
			                                                     {
				                                                     CARRIER_CREATE_WIZARD,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( IdsCreateWizard )
				                                                     }
			                                                     },

			                                                     {
				                                                     MAINTAIN_SETTINGS,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Settings.Settings )
				                                                     }
			                                                     },
			                                                     {
				                                                     MAINTAIN_STAFF,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( MaintainStaff )
				                                                     }
			                                                     },
			                                                     {
				                                                     SEARCH_TRIPS,
				                                                     new Program
				                                                     {
					                                                     PageType        = typeof( SearchTrips ),
					                                                     RelatedPrograms = new[] { TRIP_ENTRY }
				                                                     }
			                                                     },
			                                                     {
				                                                     DISPATCH_BOARD,

				                                                     new Program
				                                                     {
					                                                     PageType = typeof( DispatchBoard ),
																		 RelatedPrograms = new[] { TRIP_ENTRY }
																	 }
			                                                     },
			                                                     {
				                                                     DRIVERS_BOARD,

				                                                     new Program
				                                                     {
					                                                     PageType = typeof( DriversBoard ),
																		 RelatedPrograms = new[] { TRIP_ENTRY }
																	 }
			                                                     },
			                                                     {
				                                                     STAFF_AUDIT_TRAIL,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( AuditTrails.Staff )
				                                                     }
			                                                     },
			                                                     {
				                                                     TRIP_AUDIT_TRAIL,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Trip )
				                                                     }
			                                                     },
			                                                     {
				                                                     TRIP_ENTRY,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof(  TripEntry )
				                                                     }
			                                                     },
			                                                     {
				                                                     PRIORITY_ROUTES,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Routes )
				                                                     }
			                                                     },
			                                                     {
				                                                     MAINTAIN_ROLES,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( MaintainRoles )
				                                                     }
			                                                     },
			                                                     {
				                                                     RATE_WIZARD,
				                                                     new Program
				                                                     {
					                                                     // TODO only allow 1 instance
					                                                     PageType = typeof( RateWizard.RateWizard )
				                                                     }
			                                                     },
			                                                     {
				                                                     ADDRESS_BOOK,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( AddressBook )
				                                                     }
			                                                     },
			                                                     {
				                                                     ACCOUNTS,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Accounts )
				                                                     }
			                                                     },
			                                                     {
				                                                     PML_INVENTORY,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Inventory )
				                                                     }
			                                                     },
			                                                     {
				                                                     UPLIFT_PRODUCTS,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Products )
				                                                     }
			                                                     },
			                                                     {
				                                                     UPLIFT_SHIPMENTS,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Shipments )
				                                                     }
			                                                     },
			                                                     {
				                                                     REPORT_BROWSER,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( ReportBrowser.ReportBrowser )
				                                                     }
			                                                     },

																 // Ids
																 {
																	 BACKUP_RESTORE,
																	 new Program
																	 {
																		 PageType = typeof( BackupRestore )
																	 }
																 },

			                                                     // Diagnostics
			                                                     {
																	 SERVER_TIME,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Time )
				                                                     }
			                                                     },
			                                                     {
				                                                     STRESS_TEST,
				                                                     new Program
				                                                     {
					                                                     PageType = typeof( Test )
				                                                     }
			                                                     }
		                                                     };

		public static void RunProgram( string programName, object arg = null )
		{
			DataContext.MainDataContext.RunProgram( programName, arg );
		}
	}
}