﻿using System.Windows;
using ViewModels.MainWindow;

namespace ViewModels
{
	public static partial class Globals
	{
		public static class DataContext
		{
			private static MainWindowModel _MainDataContext;

			public static void ResetDataContext()
			{
				_MainDataContext = null;
			}

			public static MainWindowModel MainDataContext
			{
				get
				{
					var RetVal = _MainDataContext;
					if( RetVal == null )
					{
						var Current = Application.Current;
						Current?.Dispatcher.Invoke( () =>
						                            {
							                            RetVal = Current.MainWindow?.DataContext as MainWindowModel;
						                            } );
						_MainDataContext = RetVal;
					}

					return RetVal;
				}
			}
		}
	}
}