﻿namespace ViewModels
{
	public static partial class Globals
	{
		public class Menu
		{
			public static bool IsVisible
			{
				get => DataContext.MainDataContext.ShowMenu;
				set => DataContext.MainDataContext.ShowMenu = value;
			}
		}
	}
}