﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using IdsControlLibraryV2.ViewModel;

namespace ViewModels
{
	public static partial class Extensions
	{
		public static void CloseModel( this ViewModelBase model )
		{
			Globals.DataContext.MainDataContext?.CloseProgram( model );
		}

		public static void ClosePage( this Page page )
		{
			Globals.DataContext.MainDataContext?.CloseProgram( page );
		}
	}
}