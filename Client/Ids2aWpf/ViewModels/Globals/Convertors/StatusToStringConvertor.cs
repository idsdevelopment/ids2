﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Protocol.Data;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Convertors
{
	public class StatusToStringConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return value switch
			       {
				       int IntStatus              => ( (DisplayTrip.STATUS)IntStatus ).AsString(),
				       STATUS Status              => Status.AsString(),
				       DisplayTrip.STATUS DStatus => DStatus.AsString(),
				       _                          => STATUS.UNSET.AsString()
			       };
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
	}


	public class IntStatusToStringConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			STATUS Stat;

			switch( value )
			{
			case int IntStatus:
				Stat = (STATUS)IntStatus;

				break;
			case STATUS Status:
				Stat = Status;

				break;
			default:
				return "UNKNOWN";
			}

			return Stat.AsString();
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
	}

	public class StatusToSolidBrushConvertor : IValueConverter
	{
		private static SolidColorBrush _Delivered,
		                               _PickedUp,
		                               _Default,
		                               _Undeliverable;

		public static SolidColorBrush Delivered     => _Delivered ??= Globals.FindSolidColorBrush( "TripDeliveredRowBrush" );
		public static SolidColorBrush PickedUp      => _PickedUp ??= Globals.FindSolidColorBrush( "TripPickedUpRowBrush" );
		public static SolidColorBrush Undeliverable => _Undeliverable ??= Globals.FindSolidColorBrush( "TripUndeliverableRowBrush" );
		public static SolidColorBrush Default       => _Default ??= new SolidColorBrush( SystemColors.WindowColor );


		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			STATUS Status;

			SolidColorBrush Convert()
			{
				return Status switch
				       {
					       STATUS.DELIVERED => Delivered,
					       STATUS.PICKED_UP => PickedUp,
					       _                => Default
				       };
			}

			switch( value )
			{
			case STATUS Status1:
				{
					Status = Status1;

					return Convert();
				}
			case DisplayTrip.STATUS DStatus:
				switch( DStatus )
				{
				case DisplayTrip.STATUS.UNDELIVERABLE:
				case DisplayTrip.STATUS.UNPICKUPABLE:
				case DisplayTrip.STATUS.UNDELIVERABLE_NEW:
					return Undeliverable;
				default:
					Status = (STATUS)DStatus;

					return Convert();
				}
			default:
				return new SolidColorBrush( Colors.Red );
			}
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
	}
}