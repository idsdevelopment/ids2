﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ViewModels.Convertors
{
	public class BoolToGreenRedConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return value is bool Green && Green ? Brushes.Green : Brushes.OrangeRed;
		}


		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}
}