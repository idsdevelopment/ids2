﻿using System;

namespace ViewModels
{
	public static partial class Globals
	{
		public static class Company
		{
			private static string _CompanyName;

			public static string CompanyName
			{
				get => _CompanyName;
				set => _CompanyName = value.Trim();
			}
			public static string CarrierId
			{
				get => _CompanyName;
				set => _CompanyName = value.Trim();
			}
		}

		public partial class Modifications
		{
			private static bool IsCompany( string companyName, string compareName )
			{
				return string.Compare( companyName, compareName, StringComparison.OrdinalIgnoreCase ) == 0;
			}

			private static bool IsCompany( string companyName )
			{
				return IsCompany( companyName, Company.CompanyName );
			}
		}
	}
}