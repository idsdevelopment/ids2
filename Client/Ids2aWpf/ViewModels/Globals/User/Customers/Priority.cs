﻿namespace ViewModels
{
	public static partial class Globals
	{
		public partial class Modifications
		{
			public const string PRIORITY = "Priority";

			public static bool IsPriority => IsCompany( PRIORITY );
		}
	}
}
