﻿namespace ViewModels
{
	public static partial class Globals
	{
		public partial class Modifications
		{
			public const string PML_TEST = "PmlTest";
			public const string PML = "Pml";

			public static bool IsPml => IsCompany( PML ) || IsCompany( PML_TEST );
		}
	}
}