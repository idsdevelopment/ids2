﻿using Protocol.Data._Customers.Pml;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Protocol.Data;
using Utils;

namespace ViewModels.Trips.TripEntry
{
    /// <summary>
    /// Interaction logic for EditPackagesInventory.xaml
    /// </summary>
    public partial class EditPackagesInventory : Window
    {
        public EditPackagesInventory(Window owner, List<ExtendedInventory> items)
        {
            this.Owner = owner;
            InitializeComponent();

            if (DataContext is EditPackagesInventoryModel Model)
            {
                Model.SelectedInventoryItems = new List<ExtendedInventory>(items);

            }
        }

        public (bool cancelled, List<ExtendedInventory>) ShowEditPackagesInventory(List<ExtendedInventory> items)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                Model.SelectedInventoryItems = new List<ExtendedInventory>(items);

                this.ShowDialog();

                return (Model.IsCancelled, Model.SelectedInventoryItems);
            }
            else
            {
                return (true,null); // Don't update anything
            }
        }

        /// <summary>
        /// Add to SelectedInventoryItems
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BtnSelect_Click(null, null);
        }

        private void TbFilterProducts_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                Model.AdhocFilter = tbFilterProducts.Text.Trim().ToLower();
            }
        }

        /// <summary>
        /// Adds the selected items from the left to the list on the right.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            if (lvProducts.SelectedItems.Count > 0)
            {
                if (DataContext is EditPackagesInventoryModel Model)
                {
                    List<ExtendedInventory> list = new List<ExtendedInventory>();
                    foreach (Inventory pi in lvProducts.SelectedItems)
                    {
                        ExtendedInventory ei = new ExtendedInventory(pi.Description, 1, Model.ServiceLevels[0], pi);

                        list.Add(ei);
                    }
                    Model.AddSelectedInventoryItems(list);
                }
            }
        }

        private void BtnRemoveSelected_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                List<ExtendedInventory> list = new List<ExtendedInventory>();

                foreach (ExtendedInventory ei in lvSelectedProducts.Items)
                {
                    if (ei.IsSelected)
                    {
                        list.Add(ei);
                    }
                }
                Model.RemoveSelectedInventoryItems(list);
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                List<Inventory> selected = Model.GetInventoryItemsForSelected();
                Logging.WriteLogLine("Selected " + selected.Count + " Inventory items");
                Model.IsCancelled = false;
            }

            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                Logging.WriteLogLine("Cancelled");
                Model.SelectedInventoryItems.Clear();
                Model.IsCancelled = true;
            }
            this.Close();
        }

        private void IntegerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;
        }

        private void LvSelectedProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvSelectedProducts.SelectedIndex > -1)
            {
                List<ExtendedInventory> list = new List<ExtendedInventory>();
                ExtendedInventory ei = (ExtendedInventory)lvSelectedProducts.SelectedItem;
                list.Add(ei);
                if (DataContext is EditPackagesInventoryModel Model)
                {
                    Model.RemoveSelectedInventoryItems(list);
                }
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                Model.CheckForCheckedItems();
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditPackagesInventoryModel Model)
            {
                bool anyChecked = false;
                foreach (var child in lvSelectedProducts.GetChildren())
                {
                    if (child is CheckBox cb)
                    {
                        if (cb.IsChecked == true)
                        {
                            anyChecked = true;
                            break;
                        }
                    }
                }
                Model.IsChangeServiceLevelEnabled = anyChecked;
            }
        }
    }
}
