﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using ViewModels.Trips.Boards.Common;
using Xceed.Wpf.Toolkit;
using static ViewModels.Globals;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Trips.TripEntry
{
    /// <summary>
    ///     Interaction logic for TripEntry.xaml
    /// </summary>
    public partial class TripEntry : Page
	{        
        private readonly Dictionary<string, Control> dictControls = new Dictionary<string, Control>();

        public TripEntry()
        {
            InitializeComponent();

            if (DataContext is TripEntryModel Model)
            {
                // KLUDGE                
                Model.view = this;

                //this.BuildNewPackageTypeRow(Model);

                //this.Clear();
            }
        }

        private void BtnClear_Click( object sender, RoutedEventArgs e )
		{
            Clear();
		}

        public void Clear()
        {
            // KLUDGE
            //DateTime now = DateTime.Now;
            //dtpCallDate.Value = now;
            //tpCallTime.Value = now;
            //dtpReadyTimeDate.Value = now;
            //dtpReadyTimeTime.Value = now;
            //dtpDueTimeDate.Value = now;
            //dtpDueTimeTime.Value = now;

            if (DataContext is TripEntryModel Model)
            {
                Model.Execute_ClearTripEntry();
                ClearErrorConditions();

                cbAccount.SelectedIndex = -1;
                cbPickupAddresses.SelectedIndex = -1;
                //cbPickupAddressProv.SelectedIndex = -1;
                //cbPickupAddressCountry.SelectedIndex = -1;
                cbDeliveryAddresses.SelectedIndex = -1;
                //cbDeliveryAddressProv.SelectedIndex = -1;
                //cbDeliveryAddressCountry.SelectedIndex = -1;

                //cbPackageTypes.SelectedIndex = 0;
                //row_0_PackageType.SelectedIndex = 0;
                //cbServiceLevel.SelectedIndex = -1;
                cbServiceLevel.SelectedIndex = 0;

                cbDrivers.SelectedIndex = -1;
                //List<TripPackage> list = new List<TripPackage>() { 
                //List< TripPackage> list = new List<TripPackage>() { 
                //    new TripPackage() 
                //    {
                //        Pieces = 1,
                //        Weight = 1
                //    }
                //};
                ObservableCollection<DisplayPackage> list = new ObservableCollection<DisplayPackage>();
                list.Add(new DisplayPackage(new TripPackage
                {
                    Pieces = 1,
                    Weight = 1
                }));
                Model.TripPackages = list;
                BuildPackageRows(Model);
                //BuildNewPackageTotalRow(Model);
            }

            // KLUDGE
            DateTime now = DateTime.Now;
            if ( DataContext is TripEntryModel Model1 )
            {
                Model1.CallDate = now;
                //Model1.CallTime = now;
            }

            dtpCallDate.Value = now;
            tpCallTime.Value = now;
            dtpReadyTimeDate.Value = now;
            dtpReadyTimeTime.Value = now;
            dtpDueTimeDate.Value = now;
            dtpDueTimeTime.Value = now;
            //EnsureDateTimesArentBeginningOfTime();
        }
		private void ClearErrorConditions()
		{
			foreach( var ctrl in this.GetChildren() )
			{
				if( ctrl is TextBox tb )
				{
					if( tb.Background == Brushes.Yellow )
						tb.Background = Brushes.White;
				}
				else if( ctrl is ComboBox cb )
				{
					if( cb.Background == Brushes.Yellow )
						cb.Background = Brushes.White;
				}
				else if( ctrl is DateTimePicker dtp )
				{
					if( dtp.Background == Brushes.Yellow )
						dtp.Background = Brushes.White;
				}
				else if( ctrl is TimePicker tp )
				{
					if( tp.Background == Brushes.Yellow )
						tp.Background = Brushes.White;
				}
			}
		}

        private void BtnNew_Click( object sender, RoutedEventArgs e )
        {
            if( DataContext is TripEntryModel Model )
            {
                var onlyTripId = false;
                bool cancelled = false;
                bool errorsFound = false;
                if (Model.IsNewTrip)
                {
                    // 
                    Dispatcher.Invoke(() =>
                                      {
                                          var message = (string)Application.Current.TryFindResource("TripEntrySaveTripFirst")
                                                        + Environment.NewLine
                                                        + (string)Application.Current.TryFindResource("TripEntrySaveTripFirst2");
                                          var title = (string)Application.Current.TryFindResource("TripEntrySaveTripFirstTitle");
                                          var choice = MessageBox.Show(message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                                          if (choice == MessageBoxResult.Yes)
                                          {
                                              //Model.StartNewTrip = true;

                                              //errorsFound = BtnSave_Click( sender, e );
                                              errorsFound = Save();
                                              if (!errorsFound)
                                              {
                                                  BtnClear_Click(sender, e);
                                              }
                                          }
                                          else if (choice == MessageBoxResult.Cancel)
                                          {
                                              cancelled = true;
                                          }
                                      });
                }

                if (!cancelled && !errorsFound)
                {
                    // KLUDGE

                    DateTime now = DateTime.Now;
                    //DumpDateTime("BtnNew_Click");

                    //if (dtpCallDate.Text.Contains("0001"))
                    //{
                    //    Logging.WriteLogLine("ERROR - CallDate: " + dtpCallDate.Text);
                    //}
                    //Logging.WriteLogLine("ERROR - now: " + now.ToString());
                    Dispatcher.Invoke(() =>
                    {

                        Model.CallDate = now;
                        //Model.CallTime = now;

                        dtpCallDate.Value = now;
                        tpCallTime.Value = now;
                        dtpReadyTimeDate.Value = now;
                        dtpReadyTimeTime.Value = now;
                        dtpDueTimeDate.Value = now;
                        dtpDueTimeTime.Value = now;

                    });

                    BtnClear_Click(sender, e);

                    Model.Execute_NewTrip(onlyTripId);
                    // TODO TripItemsInventory still contains the Inventory for the previous trip
                    // TODO This is because Model.TripPackages isn't empty
                    //Model.TripItemsInventory = new SortedDictionary<int, List<ExtendedInventory>>();
                    //BuildPackageRows(Model);

                    //DumpDateTime("BtnNew_Click-after Execute_NewTrip");

                    //Dispatcher.Invoke(() =>
                    //{
                    //    dtpCallDate.Value = now;
                    //    dtpCallDate.Text = now.ToString("dd/MM/yyyy");
                    //    tpCallTime.Value = now;
                    //    tpCallTime.Text = now.ToString("HH:mm z");                    
                    //    dtpReadyTimeDate.Value = now;
                    //    dtpReadyTimeDate.Text = now.ToString("dd/MM/yyyy");
                    //    dtpReadyTimeTime.Value = now;
                    //    dtpReadyTimeTime.Text = now.ToString("HH:mm z");
                    //    dtpDueTimeDate.Value = now;
                    //    dtpDueTimeDate.Text = now.ToString("dd/MM/yyyy");
                    //    dtpDueTimeTime.Value = now;
                    //    dtpDueTimeTime.Text = now.ToString("HH:mm z");
                    //});
                    //EnsureDateTimesArentBeginningOfTime();
                }
            }
        }

        private void BtnNew_Click_orig( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var onlyTripId = false;
                bool cancelled = false;
                bool errorsFound = false;
                // This is the case if viewing a trip
                if (!Model.IsTripIdEditable)
                {
                    // Was
                    //BtnClear_Click( sender, e );
                    if (Model.SelectedAccount.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty())
                    {
                        Dispatcher.Invoke(() =>
                                          {
                                              var message = (string)Application.Current.TryFindResource("TripEntryAlreadyFilledGetTripId")
                                                            + Environment.NewLine
                                                            + (string)Application.Current.TryFindResource("TripEntryAlreadyFilledGetTripId2");
                                              var title = (string)Application.Current.TryFindResource("TripEntryAlreadyFilledGetTripIdTitle");
                                              var choice = MessageBox.Show(message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                                              if (choice == MessageBoxResult.No)
                                              {
                                                  //Model.Execute_ClearTripEntry();
                                                  BtnClear_Click(sender, e);
                                              }
                                              else if (choice == MessageBoxResult.Cancel)
                                              {
                                                  cancelled = true;
                                              }
                                              else
                                              {
                                                  Logging.WriteLogLine("NOT clearing the screen");
                                                  onlyTripId = true;
                                              }
                                          });
                    }
                    else
                    {
                        BtnClear_Click( sender, e );
                    }
                }
                else
                {
				    if( Model.IsNewTrip )
				    {
					    // 
					    Dispatcher.Invoke( () =>
					                       {
						                       var message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
						                                     + Environment.NewLine
						                                     + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
						                       var title = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
						                       var choice = MessageBox.Show( message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );
						                       if( choice == MessageBoxResult.Yes )
						                       {
                                                   //Model.StartNewTrip = true;

                                                   //errorsFound = BtnSave_Click( sender, e );
                                                   errorsFound = Save();
                                                   if (!errorsFound)
                                                   {
							                           BtnClear_Click( sender, e );
                                                   }
						                       }
                                               else if (choice == MessageBoxResult.Cancel)
                                               {
                                                   cancelled = true;
                                               }
                                           } );
				    }

				    //if( !cancelled && Model.Account.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty() )
				    if( !cancelled && Model.SelectedAccount.IsNotNullOrEmpty() && Model.TripId.IsNullOrEmpty() )
				    {
					    Dispatcher.Invoke( () =>
					                       {
						                       var message = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId" )
						                                     + Environment.NewLine
						                                     + (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripId2" );
						                       var title = (string)Application.Current.TryFindResource( "TripEntryAlreadyFilledGetTripIdTitle" );
						                       var choice = MessageBox.Show( message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question );
						                       if( choice == MessageBoxResult.No )
						                       {
							                       //Model.Execute_ClearTripEntry();
							                       BtnClear_Click( sender, e );
						                       }
                                               else if (choice == MessageBoxResult.Cancel)
                                               {
                                                   cancelled = true;
                                               }
						                       else
						                       {
							                       Logging.WriteLogLine( "NOT clearing the screen" );
							                       onlyTripId = true;
						                       }
					                       } );
				    }
                    //else
                    //{
                    //    //Model.Execute_ClearTripEntry();
                    //    //BtnClear_Click(sender, e);
                    //    // TODO does this work?
                    //    Model.TripPackages = new ObservableCollection<DisplayPackage>();
                    //    BuildPackageRows(Model);
                    //}
                }

                if (!cancelled && !errorsFound)
                {
                    // KLUDGE

                    DateTime now = DateTime.Now;
                    //DumpDateTime("BtnNew_Click");

                    //if (dtpCallDate.Text.Contains("0001"))
                    //{
                    //    Logging.WriteLogLine("ERROR - CallDate: " + dtpCallDate.Text);
                    //}
                    //Logging.WriteLogLine("ERROR - now: " + now.ToString());
                    Dispatcher.Invoke(() =>
                    {

                        Model.CallDate = now;
                        //Model.CallTime = now;

                        dtpCallDate.Value = now;
                        tpCallTime.Value = now;
                        dtpReadyTimeDate.Value = now;
                        dtpReadyTimeTime.Value = now;
                        dtpDueTimeDate.Value = now;
                        dtpDueTimeTime.Value = now;

                    });

                    Model.Execute_NewTrip( onlyTripId );
                    // TODO TripItemsInventory still contains the Inventory for the previous trip
                    // TODO This is because Model.TripPackages isn't empty
                    //Model.TripItemsInventory = new SortedDictionary<int, List<ExtendedInventory>>();
                    //BuildPackageRows(Model);

                    //DumpDateTime("BtnNew_Click-after Execute_NewTrip");

                    //Dispatcher.Invoke(() =>
                    //{
                    //    dtpCallDate.Value = now;
                    //    dtpCallDate.Text = now.ToString("dd/MM/yyyy");
                    //    tpCallTime.Value = now;
                    //    tpCallTime.Text = now.ToString("HH:mm z");                    
                    //    dtpReadyTimeDate.Value = now;
                    //    dtpReadyTimeDate.Text = now.ToString("dd/MM/yyyy");
                    //    dtpReadyTimeTime.Value = now;
                    //    dtpReadyTimeTime.Text = now.ToString("HH:mm z");
                    //    dtpDueTimeDate.Value = now;
                    //    dtpDueTimeDate.Text = now.ToString("dd/MM/yyyy");
                    //    dtpDueTimeTime.Value = now;
                    //    dtpDueTimeTime.Text = now.ToString("HH:mm z");
                    //});
                    //EnsureDateTimesArentBeginningOfTime();
                }
            }
        }

        private void DumpDateTime(string method)
        {
            if( DataContext is TripEntryModel Model )
            {
                Logging.WriteLogLine("ERROR - " + method + " Model.CallDate: " + Model.CallDate.ToString());
                Logging.WriteLogLine("ERROR - " + method + " Model.CallTime: " + Model.CallDate.ToString());
                Logging.WriteLogLine("ERROR - " + method + " Model.ReadyDate: " + Model.ReadyDate.ToString());
                Logging.WriteLogLine("ERROR - " + method + " Model.ReadyTime: " + Model.ReadyDate.ToString());
                Logging.WriteLogLine("ERROR - " + method + " Model.DueDate: " + Model.DueDate.ToString());
                Logging.WriteLogLine("ERROR - " + method + " Model.DueTime: " + Model.DueDate.ToString());
            }
            Logging.WriteLogLine("ERROR - " + method + " CallDate: " + dtpCallDate.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " CallDate.Text: " + dtpCallDate.Text);
            Logging.WriteLogLine("ERROR - " + method + " CallTime: " + tpCallTime.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " CallTime.Text: " + tpCallTime.Text);
            Logging.WriteLogLine("ERROR - " + method + " ReadyDate: " + dtpReadyTimeDate.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " ReadyDate.Text: " + dtpReadyTimeDate.Text);
            Logging.WriteLogLine("ERROR - " + method + " ReadyTime: " + dtpReadyTimeTime.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " ReadyTime.Text: " + dtpReadyTimeTime.Text);
            Logging.WriteLogLine("ERROR - " + method + " DueTimeDate: " + dtpDueTimeDate.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " DueTimeDate.Text: " + dtpDueTimeDate.Text);
            Logging.WriteLogLine("ERROR - " + method + " DueTimeTime: " + dtpDueTimeTime.Value.ToString());
            Logging.WriteLogLine("ERROR - " + method + " DueTimeTime.Text: " + dtpDueTimeTime.Text);
        }

		private void BtnSave_Click( object sender, RoutedEventArgs e )
		{
            Save();            
		}

        private bool Save()
        {
            bool errorsFound = false;

            if (DataContext is TripEntryModel Model)
            {
                if (dictControls.Count == 0)
                    LoadMappingDictionary();

                // Load each time to catch the extra TripItem rows
                //LoadMappingDictionary(); // TODO This doesn't include the extra Package type rows

                var errors = Model.Execute_SaveTrip(dictControls);

                if ((errors != null) && (errors.Count > 0))
                {
                    errorsFound = true;
                    var message = (string)Application.Current.TryFindResource("TripEntryValidationErrors")
                                  + Environment.NewLine;
                    foreach (var line in errors)
                        message += "\t" + line + Environment.NewLine;

                    message += (string)Application.Current.TryFindResource("TripEntryValidationErrors1");
                    var title = (string)Application.Current.TryFindResource("TripEntryValidationErrorsTitle");

                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return errorsFound;
        }

		/// <summary>
		///     Add widgets and their associated fields.
		///     Uses the GetChildren extension method defined in Widgets.cs.
		/// </summary>
		/// <returns></returns>
		private void LoadMappingDictionary()
		{
			var children = this.GetChildren();
			foreach( var ctrl in children )
			{
				Binding binding = null;
				if( ctrl is TextBox tb )
					binding = BindingOperations.GetBinding( tb, TextBox.TextProperty );
				else if( ctrl is ComboBox cb )
					binding = BindingOperations.GetBinding( cb, ItemsControl.ItemsSourceProperty );
				else if( ctrl is DateTimePicker dtp )
					binding = BindingOperations.GetBinding( dtp, DateTimePicker.ValueProperty );
				else if( ctrl is TimePicker tp )
					binding = BindingOperations.GetBinding( tp, DateTimePicker.ValueProperty );

				if( binding != null )
				{
					var name = binding.Path.Path;
					if( !dictControls.ContainsKey( name ) )
						dictControls.Add( name, (Control)ctrl );
					else
						Logging.WriteLogLine( "ERROR " + name + " already exists" );
				}
			}
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>

		//private Trip CreateTripFromFields()
		//{
		//    Trip trip = new Trip(new Trip());

		//    return trip;
		//}
		private void CbPickupAddresses_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var currentAddress = Model.SelectedPickupAddress;
                if (currentAddress.IsNotNullOrWhiteSpace())
                {
				    foreach( var tmp in Model.Addresses )
				    {
					    if( string.Compare( tmp.CompanyName, currentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
					    {
                            Dispatcher.Invoke(() =>
                            {
						        Logging.WriteLogLine( "Loading address: " + currentAddress );
						        Model.PickupSuite = tmp.Suite;
						        Model.PickupStreet = tmp.AddressLine1;
						        Model.PickupCity = tmp.City;
						        Model.PickupLocation = tmp.LocationBarcode;

						        //if( !string.IsNullOrEmpty( tmp.CountryCode ) )
							       // Model.PickupCountry = tmp.CountryCode;
						        //else
						        //{
                                    Task.WaitAll(Task.Run( async () =>
                                    {

                                        //PrimaryCompany pc = Azure.Client.RequestGetPrimaryCompanyByCustomerCode(Model.SelectedAccount).Result;
                                        PrimaryCompany pc = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(Model.SelectedAccount);

                                        if (pc != null && pc.BillingCompany != null)
                                        {
                                            if (pc.BillingCompany.Country.IsNotNullOrWhiteSpace() && Model.PickupCountry != pc.BillingCompany.Country)
                                            {
                                                Logging.WriteLogLine("Setting Model.PickupCountry to: " + pc.BillingCompany.Country);
                                                Model.PickupCountry = pc.BillingCompany.Country;
                                            }
                                            //Model.WhenPickupCountryChanges();

                                            // Need to ensure that the region list is loaded
                                            Model.PickupProvState = tmp.Region;
                                            Model.PickupPostalZip = tmp.PostalCode;
                                        }
                                    }));
                                //try
                                //{
                                //                         // TODO CompanyAddress is null
                                //	Model.PickupCountry = Model.CompanyAddress.Country;
                                //}
                                //catch( Exception )
                                //{
                                //	Logging.WriteLogLine( "Model.CompanyAddress is null" );
                                //}
                                //}
                                // Need to ensure that the region list is loaded
                                // TODO Removed to try and stop the freezes that happen _after_ the log message
                                Model.PickupProvState = tmp.Region;
                                Model.PickupPostalZip = tmp.PostalCode;

                            });
						    break;
					    }
				    }
                }
			}
		}

		private void CbDeliveryAddresses_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var currentAddress = Model.SelectedDeliveryAddress;
				foreach( var tmp in Model.Addresses )
				{
					if( string.Compare( tmp.CompanyName, currentAddress, StringComparison.CurrentCultureIgnoreCase ) == 0 )
					{
                        Dispatcher.Invoke(() =>
                        {
						    Logging.WriteLogLine( "Loading address: " + currentAddress );

						    Model.DeliverySuite = tmp.Suite;
						    Model.DeliveryStreet = tmp.AddressLine1;
						    Model.DeliveryCity = tmp.City;
						    Model.DeliveryLocation = tmp.LocationBarcode;

						    if( !string.IsNullOrEmpty( tmp.CountryCode ) )
							    Model.DeliveryCountry = tmp.CountryCode;
						    else
						    {
                                if (Model.CompanyAddress != null && !string.IsNullOrEmpty(Model.CompanyAddress.Country))
                                {
								    Model.DeliveryCountry = Model.CompanyAddress.Country;
                                }							
                                else
                                {
                                    Task.WaitAll(Task.Run( async () =>
                                    {

                                        //PrimaryCompany pc = Azure.Client.RequestGetPrimaryCompanyByCustomerCode(Model.SelectedAccount).Result;
                                        PrimaryCompany pc = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(Model.SelectedAccount);

                                        if (pc != null && pc.BillingCompany != null)
                                        {                           
                                            Model.DeliveryCountry = pc.BillingCompany.Country;
                                            //Model.WhenDeliveryCountryChanges();

                                            // Need to ensure that the region list is loaded
                                            Model.DeliveryProvState = tmp.Region;
                                            Model.DeliveryPostalZip = tmp.PostalCode;                                                                             
                                        }
                                    }));
                                }
						    }
                            // Need to ensure that the region list is loaded
						    Model.DeliveryProvState = tmp.Region;
                            Model.DeliveryPostalZip = tmp.PostalCode;
                        });

						break;
					}
				}
			}
		}

		private void Label_MouseDown( object sender, MouseButtonEventArgs e )
		{
			Logging.WriteLogLine( "Clearing " + cbDrivers.Text );
			cbDrivers.SelectedIndex = -1;
		}

		/// <summary>
		///     Toggles the visibility of the toolbar and menu.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuItem_Click( object sender, RoutedEventArgs e )
		{
			menuMain.Visibility = Visibility.Collapsed;
			toolbar.Visibility = Visibility.Visible;
		}

		/// <summary>
		///     Toggles the visibility of the toolbar and menu.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuItem_Click_1( object sender, RoutedEventArgs e )
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void Toolbar_MouseDoubleClick( object sender, MouseButtonEventArgs e )
		{
			menuMain.Visibility = Visibility.Visible;
			toolbar.Visibility = Visibility.Collapsed;
		}

		private void BtnProduceWaybill_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var printerName = WeighBillPrinter.CurrentPrinterName;
				if( printerName == "No printer selected" )
				{
					Logging.WriteLogLine( "No printer selected" );
					MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryToolBarSelectPrinterFirst" ),
					                 (string)Application.Current.TryFindResource( "TripEntryToolBarSelectPrinterFirstTitle" ),
					                 MessageBoxButton.OK, MessageBoxImage.Warning );
				}
				else
				{
					Logging.WriteLogLine( "Printer " + printerName + " selected" );

					var parentWindow = Application.Current.MainWindow;

					Model.Execute_ProduceWaybill( printerName, parentWindow );
				}
			}
		}

		private void BtnProduceTrack_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var parentWindow = Application.Current.MainWindow;
				Model.Execute_ProduceTracking( parentWindow );
			}
		}

		private void BtnProduceAudit_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				var parentWindow = Application.Current.MainWindow;
				Model.Execute_ProduceAudit( parentWindow );
			}
		}

		private void BtnProduceDocs_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				DisplayNotImplementedMessage();

				//Window parentWindow = Application.Current.MainWindow;
				// Model.Execute_ProduceAudit(parentWindow);
			}
		}

		private void DisplayNotImplementedMessage()
		{
			MessageBox.Show( "Not implemented yet", "Not Implemented", MessageBoxButton.OK, MessageBoxImage.Information );
		}

		private void BtnAddressSwap_Click( object sender, RoutedEventArgs e )
		{
			DisplayNotImplementedMessage();
		}

		private void BtnAddressBook_Click( object sender, RoutedEventArgs e )
		{
			//DisplayNotImplementedMessage();
			RunProgram( ADDRESS_BOOK );
		}

		private void BtnFindCustomer_Click( object sender, RoutedEventArgs e )
		{
			DisplayNotImplementedMessage();
		}

		private void BtnFindTrip_Click( object sender, RoutedEventArgs e )
		{
			// DisplayNotImplementedMessage();
			if( DataContext is TripEntryModel Model )
			{
				if( !string.IsNullOrEmpty( Model.TripId ) )
				{
				}
			}
		}

		private void BtnClone_Click( object sender, RoutedEventArgs e )
		{
			//DisplayNotImplementedMessage();
			if( DataContext is TripEntryModel Model )
			{
				if( Model.IsNewTrip )
				{
					// 
					Dispatcher.Invoke( () =>
					                   {
						                   var message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
						                                 + Environment.NewLine
						                                 + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
						                   var title = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
						                   var choice = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );
						                   if( choice == MessageBoxResult.Yes )
						                   {
							                   //Model.StartNewTrip = true;

							                   BtnSave_Click( sender, e );

							                   BtnClear_Click( sender, e );
						                   }
					                   } );
				}


                Model.Execute_CloneTrip();

                // KLUDGE
                var now = DateTime.Now;
                dtpCallDate.Value = now;
                tpCallTime.Value = now;
                dtpReadyTimeDate.Value = now;
                dtpReadyTimeTime.Value = now;
                dtpDueTimeDate.Value = now;
                dtpDueTimeTime.Value = now;
                //EnsureDateTimesArentBeginningOfTime();
			}
		}

		private void BtnClonePickup_Click( object sender, RoutedEventArgs e )
		{
			//DisplayNotImplementedMessage();
			if( DataContext is TripEntryModel Model )
			{
				if( Model.IsNewTrip )
				{
					// 
					Dispatcher.Invoke( () =>
					                   {
						                   var message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
						                                 + Environment.NewLine
						                                 + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
						                   var title = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
						                   var choice = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );
						                   if( choice == MessageBoxResult.Yes )
						                   {
							                   //Model.StartNewTrip = true;

							                   BtnSave_Click( sender, e );

							                   BtnClear_Click( sender, e );
						                   }
					                   } );
				}

                cbDeliveryAddresses.SelectedIndex = -1;
				Model.Execute_CloneTripForPickup();

                // KLUDGE
                var now = DateTime.Now;
                dtpCallDate.Value = now;
                tpCallTime.Value = now;
                dtpReadyTimeDate.Value = now;
                dtpReadyTimeTime.Value = now;
                dtpDueTimeDate.Value = now;
                dtpDueTimeTime.Value = now;

                //EnsureDateTimesArentBeginningOfTime();
			}
		}

		private void BtnCloneReturn_Click( object sender, RoutedEventArgs e )
		{
			//DisplayNotImplementedMessage();
			if( DataContext is TripEntryModel Model )
			{
				if( Model.IsNewTrip )
				{
					// 
					Dispatcher.Invoke( () =>
					                   {
						                   var message = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst" )
						                                 + Environment.NewLine
						                                 + (string)Application.Current.TryFindResource( "TripEntrySaveTripFirst2" );
						                   var title = (string)Application.Current.TryFindResource( "TripEntrySaveTripFirstTitle" );
						                   var choice = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );
						                   if( choice == MessageBoxResult.Yes )
						                   {
							                   //Model.StartNewTrip = true;

							                   BtnSave_Click( sender, e );

							                   BtnClear_Click( sender, e );
						                   }
					                   } );
				}

                var pickupIndex = cbPickupAddresses.SelectedIndex;
				var deliveryIndex = cbDeliveryAddresses.SelectedIndex;
				Model.Execute_CloneTripForReturn();
				cbPickupAddresses.SelectedIndex = deliveryIndex;
				cbDeliveryAddresses.SelectedIndex = pickupIndex;

                // KLUDGE
                var now = DateTime.Now;
                dtpCallDate.Value = now;
                tpCallTime.Value = now;
                dtpReadyTimeDate.Value = now;
                dtpReadyTimeTime.Value = now;
                dtpDueTimeDate.Value = now;
                dtpDueTimeTime.Value = now;

                //EnsureDateTimesArentBeginningOfTime();                
            }
		}

        //private void EnsureDateTimesArentBeginningOfTime()
        //{
        //    if (dtpCallDate.Value == DateTime.MinValue)
        //    {
        //        dtpCallDate.Value = DateTime.Now;
        //    }
        //    if (tpCallTime.Value == DateTime.MinValue)
        //    {
        //        tpCallTime.Value = DateTime.Now;
        //    }
        //    if (dtpReadyTimeDate.Value == DateTime.MinValue)
        //    {
        //        dtpReadyTimeDate.Value = DateTime.Now;
        //    }
        //    if (dtpReadyTimeTime.Value == DateTime.MinValue)
        //    {
        //        dtpReadyTimeTime.Value = DateTime.Now;
        //    }
        //    if (dtpDueTimeDate.Value == DateTime.MinValue)
        //    {
        //        dtpDueTimeDate.Value = DateTime.Now;
        //    }
        //    if (dtpDueTimeTime.Value == DateTime.MinValue)
        //    {
        //        dtpDueTimeTime.Value = DateTime.MinValue;
        //    }
        //}

		private void BtnSchedule_Click( object sender, RoutedEventArgs e )
		{
			DisplayNotImplementedMessage();
		}

		private void BtnPost_Click( object sender, RoutedEventArgs e )
		{
			DisplayNotImplementedMessage();
		}

		private void BtnCalculate_Click( object sender, RoutedEventArgs e )
		{
			DisplayNotImplementedMessage();
		}

		private void Image_MouseUp( object sender, MouseButtonEventArgs e )
		{
			if( DataContext is TripEntryModel Model && sender is Image image )
			{
				Logging.WriteLogLine( "Remove TripItem clicked: " + image.Name );

				var rowToRemove = GetRowNumberFromName( image.Name );

				//// Can't delete the first row
				//if( row > 1 )
				//{
				//}
				Logging.WriteLogLine( "Deleting row: " + rowToRemove );

                var tis = Model.TripPackages;
                //tis.RemoveAt(row - 2);
                tis.RemoveAt(rowToRemove - 1);
                Model.TripPackages = tis;

                //if (Model.TripItemsInventory.ContainsKey(row)) {
                //    Model.TripItemsInventory.Remove(row);
                //    Model.SelectedInventoryForTripItem = new List<ExtendedInventory>();
                //}

                Model.RemoveRowFromTripItemsInventory(rowToRemove);

                BuildPackageRows( Model );
			}
		}

        
		/// <summary>
		///     Creates rows based upon Model.TripItems.
		/// </summary>
		public void BuildPackageRows( TripEntryModel Model )
		{
			// First, empty the collection
			var removeControls = new List<Control>();
			var removeImages = new List<Image>();

			foreach( var tmp in gridTripItems.Children )
			{
				if( tmp is Control control )
				{
					if( control.Name.IndexOf( "Row_" ) > -1 )
					{
						removeControls.Add( control );
					}
				}
				else if( tmp is Image image )
				{
					if( ( image.Name.IndexOf( "Row_" ) > -1 ) && ( image.Name.IndexOf( "Row_0" ) == -1 ) )
						removeImages.Add( image );
				}
			}

			foreach( var c in removeControls )
				gridTripItems.Children.Remove( c );

			foreach( var i in removeImages )
				gridTripItems.Children.Remove( i );

            if (gridTripItems.RowDefinitions.Count > 1)
            {
                gridTripItems.RowDefinitions.RemoveRange(1, gridTripItems.RowDefinitions.Count - 1);
                //gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 3 );
            }
            //if( gridTripItems.RowDefinitions.Count > 2 )
            //         {
            //	gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 2 );
            //	//gridTripItems.RowDefinitions.RemoveRange( 2, gridTripItems.RowDefinitions.Count - 3 );
            //         }

            //if (gridTripItems.RowDefinitions.Count > 2)
            //if (Model.TripPackages.Count > 0)
            //if (Model.TripPackages.Count > 1)
            //{
            //    Row_0_AddRow.Visibility = Visibility.Hidden;
            //}
            //else
            //{
            //    Row_0_AddRow.Visibility = Visibility.Visible;
            //}


            //foreach( TripPackage tp in Model.TripPackages )
            //for (int i = 1; i < Model.TripPackages.Count; i++) // Skip over the first line - attached to the first row
            for (int i = 0; i < Model.TripPackages.Count; i++)
            {
                TripPackage tp = Model.TripPackages[i];
				BuildNewPackageTypeRow( Model, tp );
            }

            // Turn off all Add buttons except for the last TripItem 
            //for (int i = 1; i < gridTripItems.RowDefinitions.Count - 1; i++)
            for (int i = 1; i < gridTripItems.RowDefinitions.Count; i++)
            {
                // Turn off the first one if there are no other rows
                if (i == 1 && gridTripItems.RowDefinitions.Count == 2)
                {
                    string name = "Row_" + i + "_DeleteRow";
                    foreach (var tmp in gridTripItems.Children)
                    {
                        if (tmp is Image image)
                        {
                            if (image.Name == name)
                            {
                                image.Visibility = Visibility.Hidden;
                                break;
                            }
                        }
                    }
                }
                // Turn off all but the last one
                else if (i < gridTripItems.RowDefinitions.Count - 1)
                {
                    string name = "Row_" + i + "_AddRow";
                    foreach (var tmp in gridTripItems.Children)
                    {
                        if (tmp is Image image)
                        {
                            if (image.Name == name)
                            {
                                image.Visibility = Visibility.Hidden;
                                break;
                            }
                        }
                    }
                }
            }
            

            // Finally
            BuildNewPackageTotalRow();

		}

		private int GetRowNumberFromName( string name )
		{
			var row = 0;

			var pieces = name.ToLower().Split( '_' );
			if( pieces.Length > 2 )
                if (!pieces[1].ToLower().Contains("total"))
                {
				    row = int.Parse( pieces[ 1 ] );
                }

			return row;
		}

		private void Image_MouseUp_1( object sender, MouseButtonEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				Logging.WriteLogLine( "Add TripPackage clicked" );

                //BuildNewPackageTypeRow(Model);
                //BuildNewPackageTotalRow(Model);
                //TripPackage tp = new TripPackage();
                Boards.Common.DisplayPackage tp = new Boards.Common.DisplayPackage(new TripPackage());
                tp.Weight = 1;
                tp.Pieces = 1;
                var tis = Model.TripPackages;
                tis.Add(tp);
                Model.TripPackages = tis;

                BuildPackageRows(Model);
            }
        }

		private void CbPickupAddressCountry_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				Model.PickupCountry = (string)cbPickupAddressCountry.SelectedItem;
				cbPickupAddressProv.SelectedIndex = 0;
			}
		}

		private void CbDeliveryAddressCountry_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				Model.DeliveryCountry = (string)cbDeliveryAddressCountry.SelectedItem;
				cbDeliveryAddressProv.SelectedIndex = 0;
			}
		}

		private void BtnHelp_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is TripEntryModel Model )
			{
				Logging.WriteLogLine( "Opening " + Model.HelpUri );
				var uri = new Uri( Model.HelpUri );
				Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
			}
		}

		private void SelectAllText( object sender, RoutedEventArgs e )
		{
			var textBox = e.OriginalSource as TextBox;
			if( textBox != null )
			{
				Keyboard.Focus( textBox );
				textBox.SelectAll();

                int row = GetRowNumberFromName(textBox.Name);
                
                if (DataContext is TripEntryModel Model)
                {
                    Model.LoadInventoryForTripItem(row);
                }
			}            



		}

		/// <summary>
		/// </summary>
		/// <param name="Model"></param>
		/// <param name="tp"></param>
		public void BuildNewPackageTypeRow( TripEntryModel Model, TripPackage tp = null )
		{
            //var newRowNumber = gridTripItems.RowDefinitions.Count - 1;
            var newRowNumber = gridTripItems.RowDefinitions.Count;
            var rd = new RowDefinition
			         {
				         Height = gridTripItems.RowDefinitions[ 0 ].Height
			         };
			gridTripItems.RowDefinitions.Add( rd );

			if( tp == null )
			{
                //tp = new TripPackage
                //{
                //    Weight = 1,
                //    Pieces = 1,
                //};
                tp = new DisplayPackage(new TripPackage())
                {
                    Weight = 1,
                    Pieces = 1
                };

                var tis = Model.TripPackages;
                tis.Add(new DisplayPackage(tp));
                Model.TripPackages = tis;
            }
            else
            {
                if (tp.Pieces < 1)
                {
                    tp.Pieces = 1;
                }
                if (tp.Weight < 1)
                {
                    tp.Weight = 1;
                }
            }

			//
			// Build columns
			//

			var column = 0;

			// Package Type
			var cb = new ComboBox
			         {
				         Name = "Row_" + newRowNumber + "_PackageType",
				         ItemsSource = Model.PackageTypes,
				         Background = Brushes.White,
				         BorderBrush = Brushes.White
			         };
			var bind = new Binding
			           {
				           Path = new PropertyPath("PackageType"),
                           //Source = Model.TripPackages[newRowNumber - 2]
                           Source = Model.TripPackages[newRowNumber - 1]
                       };
			cb.SetBinding( Selector.SelectedItemProperty, bind );

			gridTripItems.Children.Add( cb );
			Grid.SetRow( cb, newRowNumber );
			Grid.SetColumn( cb, column++ );
            if (Model.TripPackages[newRowNumber - 1].PackageType.IsNotNullOrWhiteSpace())
            {
                cb.SelectedItem = Model.TripPackages[newRowNumber - 1].PackageType;
            }
            else
            {
			    cb.SelectedIndex = 0; // TODO doesn't work
            }

            // Weight
            //var tb = new TextBox
            //         {
            //	         Name = "Row_" + newRowNumber + "_Weight"
            //         };
            var decimalSpinner = new DecimalUpDown
            {
                Name = "Row_" + newRowNumber + "_Weight",
                AllowTextInput = true,
                //Minimum = decimal.Parse("0.1")
                Minimum = (decimal)0.1
            };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Weight" ),
                       Mode=BindingMode.TwoWay,
                       //Source = Model.TripPackages[newRowNumber - 2]
                       Source = Model.TripPackages[newRowNumber - 1]
            };
			//tb.SetBinding( TextBox.TextProperty, bind );
			//decimalSpinner.SetBinding( TextBox.TextProperty, bind );
			decimalSpinner.SetBinding( DecimalUpDown.ValueProperty, bind );

            //tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
            //tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );
            //tb.TextChanged += (s, e) => RecalculateTripItemTotals(s,e);
            //tb.PreviewTextInput += (s, e) => FloatTextBox_PreviewTextInput(s, e);
            decimalSpinner.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
            decimalSpinner.GotMouseCapture += (s, e) => SelectAllText(s, e);
            decimalSpinner.ValueChanged += (s, e) => RecalculateTripItemTotals(s, null);
            decimalSpinner.PreviewTextInput += (s, e) => FloatTextBox_PreviewTextInput(s, e);

            //gridTripItems.Children.Add( tb );
            //Grid.SetRow( tb, newRowNumber );
            //Grid.SetColumn( tb, column++ );
            gridTripItems.Children.Add(decimalSpinner);
            Grid.SetRow(decimalSpinner, newRowNumber);
            Grid.SetColumn(decimalSpinner, column++);

            // Pieces
            // <xctk:IntegerUpDown AllowTextInput="True" Text="{Binding Quantity}" Minimum="1"/>
            var integerSpinner = new IntegerUpDown
                          {
                              Name = "Row_" + newRowNumber + "_Pieces",
                              AllowTextInput = true,
                              Minimum = 1
                          };
			//tb = new TextBox
			//     {
			//	     Name = "Row_" + newRowNumber + "_Pieces"
			//     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Pieces" ),
                       StringFormat="{0:#}",
                       Mode=BindingMode.TwoWay,
                       //Source = Model.TripPackages[ newRowNumber - 2 ]
                       Source = Model.TripPackages[newRowNumber - 1]
                   };
            //spinner.SetBinding( TextBox.TextProperty, bind );
            integerSpinner.SetBinding(IntegerUpDown.ValueProperty, bind);

			integerSpinner.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			integerSpinner.GotMouseCapture += ( s, e ) => SelectAllText( s, e );
            //tb.TextChanged += (s, e) => RecalculateTripItemTotals(s,e);
            integerSpinner.ValueChanged += (s, e) => RecalculateTripItemTotals(s,null);
            // tb.PreviewTextInput += (s, e) => IntegerTextBox_PreviewTextInput(s, e);

			gridTripItems.Children.Add( integerSpinner );
			Grid.SetRow( integerSpinner, newRowNumber );
			Grid.SetColumn( integerSpinner, column++ );

            if (Model.TripItemsInventory.ContainsKey(newRowNumber) && Model.TripItemsInventory[newRowNumber].Count > 0) 
            {
                integerSpinner.IsEnabled = false;
            }

            //if (tp.Items != null && tp.Items.Count > 0)
            //{
            //    spinner.IsEnabled = false;
            //}

			// Length
			TextBox tb = new TextBox
			     {
				     Name = "Row_" + newRowNumber + "_Length"
			     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Length" ),
                       Mode=BindingMode.TwoWay,
                       //Source = Model.TripPackages[ newRowNumber - 2 ]
                       Source = Model.TripPackages[newRowNumber - 1]
            };
			tb.SetBinding( TextBox.TextProperty, bind );

			bind = new Binding
			       {
				       Path = new PropertyPath( "IsDIM" ),
				       Source = Model.IsDIM
			       };
			tb.SetBinding( IsEnabledProperty, bind );

			tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );

			tb.IsEnabled = false; // TODO

			gridTripItems.Children.Add( tb );
			Grid.SetRow( tb, newRowNumber );
			Grid.SetColumn( tb, column++ );

			// Width
			tb = new TextBox
			     {
				     Name = "Row_" + newRowNumber + "_Width"
			     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Width" ),
                       Mode=BindingMode.TwoWay,
				       // Source = Model.TripPackages[ newRowNumber - 2 ]
				       Source = Model.TripPackages[ newRowNumber - 1 ]
			       };
			tb.SetBinding( TextBox.TextProperty, bind );
			bind = new Binding
			       {
				       Path = new PropertyPath( "IsDIM" ),
				       Source = Model.IsDIM
			       };
			tb.SetBinding( IsEnabledProperty, bind );

			tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );

			tb.IsEnabled = false; // TODO

			gridTripItems.Children.Add( tb );
			Grid.SetRow( tb, newRowNumber );
			Grid.SetColumn( tb, column++ );

			// Height
			tb = new TextBox
			     {
				     Name = "Row_" + newRowNumber + "_Height"
			     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Height" ),
                       Mode=BindingMode.TwoWay,
				       // Source = Model.TripPackages[ newRowNumber - 2 ]
				       Source = Model.TripPackages[ newRowNumber - 1 ]
			       };
			tb.SetBinding( TextBox.TextProperty, bind );
			bind = new Binding
			       {
				       Path = new PropertyPath( "IsDIM" ),
				       Source = Model.IsDIM
			       };
			tb.SetBinding( IsEnabledProperty, bind );

			tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );

			tb.IsEnabled = false; // TODO

			gridTripItems.Children.Add( tb );
			Grid.SetRow( tb, newRowNumber );
			Grid.SetColumn( tb, column++ );

			// Original
			tb = new TextBox
			     {
				     Name = "Row_" + newRowNumber + "_Original"
			     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "Original" ),
                       Mode=BindingMode.TwoWay,
				       // Source = Model.TripPackages[ newRowNumber - 2 ]
				       Source = Model.TripPackages[ newRowNumber - 1 ]
			       };
			tb.SetBinding( TextBox.TextProperty, bind );
			bind = new Binding
			       {
				       Path = new PropertyPath( "IsDIM" ),
				       Source = Model.IsDIM
			       };
			tb.SetBinding( IsEnabledProperty, bind );

			tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );
            tb.TextChanged += (s, e) => RecalculateTripItemTotals(s,e);

			tb.IsEnabled = false; // TODO

			gridTripItems.Children.Add( tb );
			Grid.SetRow( tb, newRowNumber );
			Grid.SetColumn( tb, column++ );

			// Q/H
			tb = new TextBox
			     {
				     Name = "Row_" + newRowNumber + "_QH"
			     };
			bind = new Binding
			       {
				       Path = new PropertyPath( "QH" ),
                       Mode=BindingMode.TwoWay,
				       // Source = Model.TripPackages[ newRowNumber - 2 ]
				       Source = Model.TripPackages[ newRowNumber - 1 ]
			       };
			tb.SetBinding( TextBox.TextProperty, bind );
			bind = new Binding
			       {
				       Path = new PropertyPath( "IsDIM" ),
				       Source = Model.IsDIM
			       };
			tb.SetBinding( IsEnabledProperty, bind );

			tb.GotKeyboardFocus += ( s, e ) => SelectAllText( s, e );
			tb.GotMouseCapture += ( s, e ) => SelectAllText( s, e );

			tb.IsEnabled = false; // TODO

			gridTripItems.Children.Add( tb );
			Grid.SetRow( tb, newRowNumber );
			Grid.SetColumn( tb, column++ );

            // Add row
            var addImage = new Image
            {
                Name = "Row_" + newRowNumber + "_AddRow",

                //finalImage.Source = new BitmapImage(new Uri("pack://application:,,,/AssemblyName;component/Resources/logo.png"));
                Source = new BitmapImage(new Uri("pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Table-Add_16x16.scale-100.png")),
                ToolTip = (string)Application.Current.TryFindResource("TripEntryPackageAddRowTooltip"),
                Margin = new Thickness(3, 0, 0, 0)
            };

            addImage.MouseUp += Image_MouseUp_1;

            gridTripItems.Children.Add(addImage);
            Grid.SetRow(addImage, newRowNumber);
            Grid.SetColumn(addImage, column++);


            // Delete row
            var delImage = new Image
			        {
				        Name = "Row_" + newRowNumber + "_DeleteRow",
				        Source = new BitmapImage( new Uri( "pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Table-Delete_16x16.scale-100.png" ) ),
				        ToolTip = (string)Application.Current.TryFindResource( "TripEntryPackageRemoveRowTooltip" ),
				        Margin = new Thickness( 3, 0, 0, 0 )
			        };

			delImage.MouseUp += Image_MouseUp;

			gridTripItems.Children.Add( delImage );
			Grid.SetRow( delImage, newRowNumber );
			Grid.SetColumn( delImage, column++ );

            // Edit Inventory
            var inventoryImage = new Image
            {
                Name = "Row_" + newRowNumber + "_EditInventory",
                Source = new BitmapImage(new Uri("pack://application:,,,/ViewModels;component/Resources/Images/Buttons/Box_16x16.scale-150.png")),
                ToolTip = (string)Application.Current.TryFindResource("TripEntryPackageEditContentsTooltip"),
                Margin = new Thickness(3, 0, 0, 0)
            };

            inventoryImage.MouseUp += EditContentsOfPackage;

            gridTripItems.Children.Add(inventoryImage);
            Grid.SetRow(inventoryImage, newRowNumber);
            Grid.SetColumn(inventoryImage, column++);
        }

        /// <summary>
        /// Appends a Totals row to the package types grid.
        /// </summary>
        /// <param name="Model"></param>
        public void BuildNewPackageTotalRow()
        {
            var newRowNumber = gridTripItems.RowDefinitions.Count;
            //var newRowNumber = gridTripItems.RowDefinitions.Count + 1;
            var rd = new RowDefinition
            {
                Height = gridTripItems.RowDefinitions[0].Height
            };
            gridTripItems.RowDefinitions.Add(rd);

            // <Label Grid.Column="0" Grid.Row="2" Content="{DynamicResource TripEntryPackageTotals}"/>
            var column = 0;
            Label label = new Label
            {
                //Name = "Row_" + newRowNumber + "_Label",
                Name = "Row_Totals_Label",
                Content = (string)Application.Current.TryFindResource( "TripEntryPackageTotals" )
            };
            gridTripItems.Children.Add(label);
            Grid.SetRow(label, newRowNumber);
            Grid.SetColumn(label, column);

            // <TextBox Grid.Column="1" Grid.Row="2" x:Name="tbTotalWeight" Text="{Binding TotalWeight}" IsReadOnly="True"/>
            column = 1;
            TextBox tb = new TextBox
            {
                //Name = "Row_" + newRowNumber + "_TotalWeight",
                Name = "Row_Totals_TotalWeight",
                IsReadOnly = true
            };
            //tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
            //tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
            //Binding bind = new Binding
            //{
            //    Path = new PropertyPath("TotalWeight"),
            //    Mode = BindingMode.TwoWay,
            //    Source = Model.TotalWeight
            //};
            //tb.SetBinding(TextBox.TextProperty, bind);
            gridTripItems.Children.Add(tb);
            Grid.SetRow(tb, newRowNumber);
            Grid.SetColumn(tb, column);

            // <TextBox Grid.Column="2" Grid.Row="2" x:Name="tbTotalPieces" Text="{Binding TotalPieces}" IsReadOnly="True"/>
            column = 2;
            tb = new TextBox
            {
                //Name = "Row_" + newRowNumber + "_TotalPieces",
                Name = "Row_Totals_TotalPieces",
                IsReadOnly = true
            };
            //tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
            //tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
            //bind = new Binding
            //{
            //    Path = new PropertyPath("TotalPieces"),
            //    Mode = BindingMode.TwoWay,
            //    Source = Model.TotalPieces
            //};
            //tb.SetBinding(TextBox.TextProperty, bind);
            gridTripItems.Children.Add(tb);
            Grid.SetRow(tb, newRowNumber);
            Grid.SetColumn(tb, column);

            //<TextBox Grid.Column="6" Grid.Row="2" x:Name="tbTotalOriginal" Text="{Binding TotalOriginal}" IsReadOnly="True"/>
            column = 6;
            tb = new TextBox
            {
                //Name = "Row_" + newRowNumber + "_TotalOriginal",
                Name = "Row_Totals_TotalOriginal",
                IsReadOnly = true
            };
            //tb.GotKeyboardFocus += (s, e) => SelectAllText(s, e);
            //tb.GotMouseCapture += (s, e) => SelectAllText(s, e);
            //bind = new Binding
            //{
            //    Path = new PropertyPath("TotalOriginal"),
            //    Mode = BindingMode.TwoWay,
            //    Source = Model.TotalOriginal
            //};
            //tb.SetBinding(TextBox.TextProperty, bind);
            gridTripItems.Children.Add(tb);
            Grid.SetRow(tb, newRowNumber);
            Grid.SetColumn(tb, column);

            RecalculateTripItemTotals(null, null);
        }

                               
        private void RecalculateTripItemTotals(object sender, TextChangedEventArgs e)
        {
            Logging.WriteLogLine("Recalculating totals");

            decimal weight = 0;
            int pieces = 0;
            decimal original = 0;

            foreach (var control in gridTripItems.Children)
            {
                if (control is TextBox tb)
                {
                    if (!tb.Name.Contains("Row_Totals"))
                    {
                        // Weight, Pieces, and Original
                        if (tb.Name.ToLower().Contains("weight"))
                        {
                            //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            if (tb.Text.IsNotNullOrWhiteSpace())
                            {
                                decimal tmp = decimal.Parse(tb.Text.Trim());
                                weight += tmp;
                            }
                        }
                        else if (tb.Name.ToLower().Contains("pieces"))
                        {
                            //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            if (tb.Text.IsNotNullOrWhiteSpace())
                            {
                                int tmp = Decimal.ToInt32(decimal.Parse(tb.Text.Trim()));
                                pieces += tmp;
                            }
                        }
                        else if (tb.Name.ToLower().Contains("original"))
                        {
                            //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            if (tb.Text.IsNotNullOrWhiteSpace())
                            {
                                decimal tmp = decimal.Parse(tb.Text.Trim());
                                original += tmp;
                            }
                        }
                    }
                }
                else if (control is IntegerUpDown iud)
                {
                    if (iud.Name.ToLower().Contains("pieces"))
                    {
                        if (iud.Value != null)
                        {
                            pieces += (int)iud.Value;
                        }
                    }
                }
                else if (control is DecimalUpDown dud)
                {
                    if (dud.Name.ToLower().Contains("weight"))
                    {
                        if (dud.Value != null)
                        {
                            weight += (decimal)dud.Value;
                        }
                    }
                }
            }

            if (DataContext is TripEntryModel Model)
            {
                Model.TotalWeight = weight;
                Model.TotalPieces = pieces;
                Model.TotalOriginal = original;

                Dispatcher.Invoke(() =>
                {
                    // Kludge the bound fields aren't showing up
                    foreach (var control in gridTripItems.Children)
                    {
                        if (control is TextBox tb)
                        {
                            if (tb.Name == "Row_Totals_TotalWeight")
                            {
                                tb.Text = weight + "";
                                //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            }
                            else if (tb.Name == "Row_Totals_TotalPieces")
                            {
                                tb.Text = pieces + "";
                                //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            }
                            else if (tb.Name == "Row_Totals_TotalOriginal")
                            {
                                tb.Text = original + "";
                                //Logging.WriteLogLine(tb.Name + ", Text: " + tb.Text);
                            }
                        }
                    }
                });
            }
        }

        /// <summary>
		///     Takes an integer.
		///     /// Either attach this to the TextBox in XAML:
		///     PreviewTextInput="IntegerTextBox_PreviewTextInput"
		///     or in code as:
		///     this.pieces[row].AddHandler(TextBox.PreviewTextInputEvent, new
		///     TextCompositionEventHandler(this.IntegerTextBox_PreviewTextInput));
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">e</param>
		private void IntegerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;
        }

        /// <summary>
        ///     Takes a float. Numbers and '.' only. Only allows 1 decimal.
        ///     Either attach this to the TextBox in XAML:
        ///     PreviewTextInput="FloatTextBox_PreviewTextInput"
        ///     or in code as:
        ///     this.pieces[row].AddHandler(TextBox.PreviewTextInputEvent, new
        ///     TextCompositionEventHandler(this.FloatTextBox_PreviewTextInput));
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void FloatTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var ToCheck = e.Text.Substring(e.Text.Length - 1);
            const string DECIMAL_CHAR = ".";
            var decimalFound = false;

            if (sender is TextBox)
            {
                if (((TextBox)sender).Text.IndexOf(DECIMAL_CHAR) > -1)
                    decimalFound = true;
            }

            if (ToCheck.Equals(DECIMAL_CHAR) && decimalFound)
                e.Handled = true;
            else if (!char.IsDigit(e.Text, e.Text.Length - 1) && !ToCheck.Equals(DECIMAL_CHAR))
                e.Handled = true;
        }

        /// <summary>
        /// Opens a dialog where the Inventory items can be selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditContentsOfPackage(object sender, MouseButtonEventArgs e)
        {
            if (DataContext is TripEntryModel Model && sender is Image image)
            {
                Logging.WriteLogLine("Editing Contents of row with " + image.Name);

                var row = GetRowNumberFromName(image.Name);

                List<ExtendedInventory> inventory;
                // Try to load
                if (Model.TripItemsInventory.ContainsKey(row))
                {
                    inventory = new List<ExtendedInventory>(Model.TripItemsInventory[row]);
                }
                else
                {
                    inventory = new List<ExtendedInventory>();
                }

                (bool isCancelled, List<ExtendedInventory> selected) = new EditPackagesInventory(Application.Current.MainWindow, inventory).ShowEditPackagesInventory(inventory);

                if (!isCancelled)
                {
                    Logging.WriteLogLine("Finished Editing Contents: selected " + selected.Count + " items in row " + row);

                    Model.AddUpdateInventoryToPackageRow(row, selected);
                    Model.LoadInventoryForTripItem(row);

                    string name = "Row_" + row + "_Pieces";
                    if (selected.Count > 0 && row > 0)
                    {
                        int newPieceCount = GetTotalPiecesForInventoryItems(selected);
                        // Lock the Pieces TextBox
                        SetPiecesReadonlyAndEnabled(true, name, newPieceCount);
                        // And set the pieces count to the total of the selected items' quantities
                        //Model.TripPackages[row].Pieces = Model.Pieces = GetTotalPiecesForInventoryItems(selected);
                        //Model.TripPackages[row].Pieces = GetTotalPiecesForInventoryItems(selected);
                        Model.TripPackages[row - 1].Pieces = newPieceCount;

                        RecalculateTripItemTotals(null, null);
                    }
                    else
                    {
                        // Unlock the Pieces TextBox
                        SetPiecesReadonlyAndEnabled(false, name, 1);
                        // Remove any value
                        //Model.TripPackages[row].Pieces = 0;
                        Model.TripPackages[row - 1].Pieces = 1;
                    }
                }
            }
        }

        private void SetPiecesReadonlyAndEnabled(bool isReadonly, string name, int count)
        {
            foreach (var tmp in gridTripItems.Children)
            {
                if (tmp is TextBox tb)
                {
                    if (tb.Name == name)
                    {
                        tb.IsReadOnly = isReadonly;
                        tb.IsEnabled = !isReadonly;
                        tb.Text = count.ToString();
                        break;
                    }
                }
                else if (tmp is IntegerUpDown iud)
                {
                    if (iud.Name == name)
                    {
                        iud.IsReadOnly = isReadonly;
                        iud.IsEnabled = !isReadonly;
                        iud.Value = count;
                    }
                }
            }
        }

        private int GetTotalPiecesForInventoryItems(List<ExtendedInventory> eis)
        {
            int total = 0;

            if (eis != null && eis.Count > 0)
            {
                foreach (ExtendedInventory ei in eis)
                {
                    total += ei.Quantity;
                }
            }

            return total;
        }

        private void gridTripItems_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var point = Mouse.GetPosition(gridTripItems);
            int row = 0;
            double accumulatedHeight = 0.0;

            foreach (RowDefinition rd in gridTripItems.RowDefinitions)
            {
                accumulatedHeight += rd.ActualHeight;
                if (accumulatedHeight >= point.Y)
                {
                    break;
                }
                row++;
            }

            if (DataContext is TripEntryModel Model)
            {
                Model.LoadInventoryForTripItem(row);
            }
        }

        private void tbCustomerPhone_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is TripEntryModel Model)
            {
                Model.CustomerPhone = tbCustomerPhone.Text.Trim();
            }
        }
    }
}