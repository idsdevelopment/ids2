﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Protocol.Data._Customers.Pml;
using Utils;
using ViewModels.Dialogues;
using ViewModels.Trips.Boards.Common;
using Xceed.Wpf.Toolkit;
using static ViewModels.Reporting.DataSets;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Trips.TripEntry
{
	public class TripEntryModel : ViewModelBase
	{
		public static readonly string PROGRAM = "Shipment Entry (" + Globals.CurrentVersion.APP_VERSION + ")";


		public bool Loaded
		{
			get { return Get( () => Loaded, false ); }
			set { Set( () => Loaded, value ); }
		}

		public TripEntry view = null;

		public override void OnArgumentChange( object selectedTrip )
		{
			if( selectedTrip is DisplayTrip Trip )
			{
				CurrentTrip = Trip;
				WhenCurrentTripChanges();
			}
		}


		protected override void OnInitialised()
		{
			base.OnInitialised();
			Loaded = false;

			Task.Run( () =>
			          {
				          Task.WaitAll( Task.Run( GetServiceLevels ),
				                        Task.Run( GetPackageTypes ),
				                        Task.Run( GetInventory ),
				                        Task.Run( GetCompanyNames ),
				                        Task.Run( GetDrivers )
				                      );

				          Dispatcher.Invoke( () =>
				                             {
					                             Loaded = true;

					                             view.Clear();
				                             } );
			          } );
		}

	#region Data

		public IList Countries
		{
			get
			{
				var countries = CountriesRegions.Countries;

				return countries;
			}
			set { Set( () => Countries, value ); }
		}


		public bool StartNewTrip
		{
			get { return Get( () => StartNewTrip, false ); }
			set { Set( () => StartNewTrip, value ); }
		}


		[DependsUpon( nameof( CurrentTrip ) )]
		public bool IsTripIdEditable
		{
			//get { return Get( () => IsTripIdEditable, CurrentTrip != null ); }
			get { return Get( () => IsTripIdEditable, false ); } // There are no situations where it should be editable
			set { Set( () => IsTripIdEditable, value ); }
		}

		public bool TextBoxesEditable
		{
			//get { return Get(() => TextBoxesEditable, false); }
			get { return Get( () => TextBoxesEditable, true ); }
			set { Set( () => TextBoxesEditable, value ); }
		}


		public bool TextBoxesReadonly
		{
			get { return Get( () => TextBoxesReadonly, false ); }
			set { Set( () => TextBoxesReadonly, value ); }
		}


		public bool WidgetEnabled
		{
			get { return Get( () => WidgetEnabled, false ); }
			set { Set( () => WidgetEnabled, value ); }
		}


		public DisplayTrip CurrentTrip
		{
			get { return Get( () => CurrentTrip ); }
			set { Set( () => CurrentTrip, value ); }
		}

		// ViewModels.Trips.Boards.Common.DisplayTrip
		public ObservableCollection<DisplayPackage> TripPackages
		{
			get { return Get( () => TripPackages, new ObservableCollection<DisplayPackage>() ); }
			set { Set( () => TripPackages, value ); }
		}

		//public List<TripPackage> TripPackages
		//{
		//	get { return Get( () => TripPackages, new List<TripPackage>() ); }
		//	set { Set( () => TripPackages, value ); }
		//}

		/// <summary>
		///     Holds the inventory for each line item.
		///     The key is the row number.
		///     Note - they will be off by 1 as the first is for the
		///     first line in the package list.
		/// </summary>
		public SortedDictionary<int, List<ExtendedInventory>> TripItemsInventory = new SortedDictionary<int, List<ExtendedInventory>>();

		public void AddUpdateInventoryToPackageRow( int row, List<ExtendedInventory> eis )
		{
			if( ( row > -1 ) && ( eis != null ) )
			{
				foreach( var ei in eis )
					ei.IsSelected = false; // Clear the flags

				if( TripItemsInventory.ContainsKey( row ) )
					TripItemsInventory[ row ] = eis;
				else
					TripItemsInventory.Add( row, eis );

				// TODO Need to add these items to the TripPackages
				//if (TripPackages.Count > 0)
				//{

				//}
			}
		}


		public List<ExtendedInventory> SelectedInventoryForTripItem
		{
			get { return Get( () => SelectedInventoryForTripItem, new List<ExtendedInventory>() ); }
			set { Set( () => SelectedInventoryForTripItem, value ); }
		}

		public void LoadInventoryForTripItem( int row )
		{
			Logging.WriteLogLine( "Loading inventory for row: " + row );

			//Dispatcher.Invoke(() =>
			//{
			//    if (TripItemsInventory.ContainsKey(row))
			//    {
			//        SelectedInventoryForTripItem = TripItemsInventory[row];
			//    }
			//    else
			//    {
			//        SelectedInventoryForTripItem.Clear();
			//    }
			//});
			if( TripItemsInventory.ContainsKey( row ) )
				SelectedInventoryForTripItem = TripItemsInventory[ row ];
			else
			{
				var list = new List<ExtendedInventory>();
				SelectedInventoryForTripItem = list;
			}
		}

		public void RemoveRowFromTripItemsInventory( int rowToRemove )
		{
			var newDict = new SortedDictionary<int, List<ExtendedInventory>>();

			for( var i = 1; i <= TripItemsInventory.Count; i++ )
			{
				var payload = TripItemsInventory[ i ];

				if( i < rowToRemove )
					newDict.Add( i, payload );
				else if( i == rowToRemove )
				{
					// Do nothing
				}
				else if( i > rowToRemove )
					newDict.Add( i - 1, payload );
			}

			TripItemsInventory = newDict;

			SelectedInventoryForTripItem = new List<ExtendedInventory>();
		}


		public decimal TotalWeight
		{
			get { return Get( () => TotalWeight, 0 ); }
			set { Set( () => TotalWeight, value ); }
		}


		public int TotalPieces
		{
			get { return Get( () => TotalPieces, 0 ); }
			set { Set( () => TotalPieces, value ); }
		}


		public decimal TotalOriginal
		{
			get { return Get( () => TotalOriginal, 0 ); }
			set { Set( () => TotalOriginal, value ); }
		}


		public string TripId
		{
			get { return Get( () => TripId, "" ); }
			set { Set( () => TripId, value ); }
		}


		public int Status1
		{
			get { return Get( () => Status1, 0 ); }
			set { Set( () => Status1, value ); }
		}


		public string DriverCode
		{
			get { return Get( () => DriverCode, "" ); }
			set { Set( () => DriverCode, value ); }
		}


		public string TPU
		{
			get { return Get( () => TPU ); }
			set { Set( () => TPU, value ); }
		}


		public string TDEL
		{
			get { return Get( () => TDEL ); }
			set { Set( () => TDEL, value ); }
		}


		public string CallName
		{
			get { return Get( () => CallName, "" ); }
			set { Set( () => CallName, value ); }
		}


		public string CallPhone
		{
			get { return Get( () => CallPhone, "" ); }
			set { Set( () => CallPhone, value ); }
		}


		public string CallEmail
		{
			get { return Get( () => CallEmail, "" ); }
			set { Set( () => CallEmail, value ); }
		}


		//public DateTimeOffset CallDate
		//{
		//	get { return Get( () => CallDate, DateTime.Now ); }
		//	set { Set( () => CallDate, value ); }
		//}


		//public DateTimeOffset CallTime
		//{
		//	get { return Get( () => CallTime, DateTimeOffset.Now ); }
		//	set { Set( () => CallTime, value ); }
		//}

		public DateTime CallDate
		{
			get { return Get( () => CallDate ); } // , DateTime.Now
			set { Set( () => CallDate, value ); }
		}


		//public DateTime CallTime
		//{
		//    get { return Get(() => CallTime, DateTime.Now); }
		//    set { Set(() => CallTime, value); }
		//}


		public bool IsQuote
		{
			get { return Get( () => IsQuote ); } //, true 
			set { Set( () => IsQuote, value ); }
		}


		public string UndeliverableNotes
		{
			get { return Get( () => UndeliverableNotes, "" ); }
			set { Set( () => UndeliverableNotes, value ); }
		}


		public string Account
		{
			get { return Get( () => Account, "" ); }
			set { Set( () => Account, value ); }
		}


		public string Customer
		{
			get { return Get( () => Customer, "" ); }
			set { Set( () => Customer, value ); }
		}


		public CompanyAddress CompanyAddress;


		public string Reference
		{
			get { return Get( () => Reference, "" ); }
			set { Set( () => Reference, value ); }
		}


		public string PickupCompany
		{
			get { return Get( () => PickupCompany, "" ); }
			set { Set( () => PickupCompany, value ); }
		}


		public string PickupName
		{
			get { return Get( () => PickupName, "" ); }
			set { Set( () => PickupName, value ); }
		}


		public string PickupPhone
		{
			get { return Get( () => PickupPhone, "" ); }
			set { Set( () => PickupPhone, value ); }
		}


		public bool PickupDefault
		{
			get { return Get( () => PickupDefault, false ); }
			set { Set( () => PickupDefault, value ); }
		}


		public string PickupSuite
		{
			get { return Get( () => PickupSuite, "" ); }
			set { Set( () => PickupSuite, value ); }
		}


		public string PickupStreet
		{
			get { return Get( () => PickupStreet, "" ); }
			set { Set( () => PickupStreet, value ); }
		}


		public string PickupCity
		{
			get { return Get( () => PickupCity, "" ); }
			set { Set( () => PickupCity, value ); }
		}

		public string PickupLocation
		{
			get { return Get( () => PickupLocation, "" ); }
			set { Set( () => PickupLocation, value ); }
		}


		public string PickupProvState
		{
			get { return Get( () => PickupProvState, "" ); }
			set { Set( () => PickupProvState, value ); }
		}

		[DependsUpon( nameof( PickupProvState ) )]
		public void WhenPickupProvStateChanges()
		{
			var newProvState = PickupProvState;

			if( ( PickupRegions != null ) && !PickupRegions.Contains( PickupProvState ) )
			{
				// Problem - abbreviation rather than name
				var name = CountriesRegions.FindRegionForAbbreviationAndCountry( PickupProvState, PickupCountry );

				if( name.IsNotNullOrWhiteSpace() )
				{
					Logging.WriteLogLine( "PickupProvState: was abbreviation: " + newProvState + ", now: " + name );
					PickupProvState = name;
				}
			}
		}

		public string PickupCountry
		{
			get { return Get( () => PickupCountry, "" ); }
			set { Set( () => PickupCountry, value ); }
		}


		public IList PickupRegions
		{
			get { return Get( () => PickupRegions ); }
			set { Set( () => PickupRegions, value ); }
		}

		//[DependsUpon( nameof( PickupCountry ) )]
		public void WhenPickupCountryChanges()
		{
			Logging.WriteLogLine( "PickupCountry: " + PickupCountry );
			var regions = new List<string>();

			switch( PickupCountry )
			{
			case "Australia":
				regions = CountriesRegions.AustraliaRegions;

				break;

			case "Canada":
				regions = CountriesRegions.CanadaRegions;

				break;

			case "United States":
				regions = CountriesRegions.USRegions;

				break;
			}

			PickupRegions = regions;

			//     Dispatcher.Invoke(() =>
			//     {
			//List<string> regions = new List<string>();

			//switch( PickupCountry )
			//{
			//case "Australia":
			// regions = CountriesRegions.AustraliaRegions;

			// break;

			//case "Canada":
			// regions = CountriesRegions.CanadaRegions;

			// break;

			//case "United States":
			// regions = CountriesRegions.USRegions;

			// break;
			//}

			//PickupRegions = regions;
			//     });
		}


		public string PickupPostalZip
		{
			get { return Get( () => PickupPostalZip, "" ); }
			set { Set( () => PickupPostalZip, value ); }
		}


		public string PickupZone
		{
			get { return Get( () => PickupZone, "" ); }
			set { Set( () => PickupZone, value ); }
		}


		public string PickupAddressNotes
		{
			get { return Get( () => PickupAddressNotes, "" ); }
			set { Set( () => PickupAddressNotes, value ); }
		}


		public string PickupNotes
		{
			get { return Get( () => PickupNotes, "" ); }
			set { Set( () => PickupNotes, value ); }
		}

		//
		//
		//


		public string DeliveryCompany
		{
			get { return Get( () => DeliveryCompany, "" ); }
			set { Set( () => DeliveryCompany, value ); }
		}


		public string DeliveryName
		{
			get { return Get( () => DeliveryName, "" ); }
			set { Set( () => DeliveryName, value ); }
		}


		public string DeliveryPhone
		{
			get { return Get( () => DeliveryPhone, "" ); }
			set { Set( () => DeliveryPhone, value ); }
		}


		public bool DeliveryDefault
		{
			get { return Get( () => DeliveryDefault, false ); }
			set { Set( () => DeliveryDefault, value ); }
		}


		public string DeliverySuite
		{
			get { return Get( () => DeliverySuite, "" ); }
			set { Set( () => DeliverySuite, value ); }
		}


		public string DeliveryStreet
		{
			get { return Get( () => DeliveryStreet, "" ); }
			set { Set( () => DeliveryStreet, value ); }
		}


		public string DeliveryCity
		{
			get { return Get( () => DeliveryCity, "" ); }
			set { Set( () => DeliveryCity, value ); }
		}

		public string DeliveryLocation
		{
			get { return Get( () => DeliveryLocation, "" ); }
			set { Set( () => DeliveryLocation, value ); }
		}


		public string DeliveryProvState
		{
			get { return Get( () => DeliveryProvState, "" ); }
			set { Set( () => DeliveryProvState, value ); }
		}

		[DependsUpon( nameof( DeliveryProvState ) )]
		public void WhenDeliveryProvStateChanges()
		{
			var newProvState = DeliveryProvState;

			if( ( DeliveryRegions != null ) && !DeliveryRegions.Contains( DeliveryProvState ) )
			{
				// Problem - abbreviation rather than name
				var name = CountriesRegions.FindRegionForAbbreviationAndCountry( DeliveryProvState, DeliveryCountry );

				if( name.IsNotNullOrWhiteSpace() )
				{
					Logging.WriteLogLine( "DeliveryProvState: was abbreviation: " + newProvState + ", now: " + name );
					DeliveryProvState = name;
				}
			}
		}

		public string DeliveryCountry
		{
			get { return Get( () => DeliveryCountry, "" ); }
			set { Set( () => DeliveryCountry, value ); }
		}


		public IList DeliveryRegions
		{
			get { return Get( () => DeliveryRegions ); }
			set { Set( () => DeliveryRegions, value ); }
		}

		//[DependsUpon( nameof( DeliveryCountry ) )]
		public void WhenDeliveryCountryChanges()
		{
			Logging.WriteLogLine( "DeliveryCountry: " + DeliveryCountry );
			List<string> regions = null;

			switch( DeliveryCountry )
			{
			case "Australia":
				regions = CountriesRegions.AustraliaRegions;

				break;

			case "Canada":
				regions = CountriesRegions.CanadaRegions;

				break;

			case "United States":
				regions = CountriesRegions.USRegions;

				break;
			}

			DeliveryRegions = regions;
		}


		public string DeliveryPostalZip
		{
			get { return Get( () => DeliveryPostalZip, "" ); }
			set { Set( () => DeliveryPostalZip, value ); }
		}


		public string DeliveryZone
		{
			get { return Get( () => DeliveryZone, "" ); }
			set { Set( () => DeliveryZone, value ); }
		}


		public string DeliveryAddressNotes
		{
			get { return Get( () => DeliveryAddressNotes, "" ); }
			set { Set( () => DeliveryAddressNotes, value ); }
		}


		public string DeliveryNotes
		{
			get { return Get( () => DeliveryNotes, "" ); }
			set { Set( () => DeliveryNotes, value ); }
		}


		public string PackageType
		{
			get { return Get( () => PackageType, "" ); }
			set { Set( () => PackageType, value ); }
		}


		//public decimal Weight
		//{
		//	get { return Get( () => Weight, 1 ); }
		//	set { Set( () => Weight, value ); }
		//}


		//public int Pieces
		//{
		//	get { return Get( () => Pieces, 1 ); }
		//	set { Set( () => Pieces, value ); }
		//}


		public bool IsDIM
		{
			get { return Get( () => IsDIM, false ); }
			set { Set( () => IsDIM, value ); }
		}


		//public decimal Length
		//{
		//	get { return Get( () => Length, 0 ); }
		//	set { Set( () => Length, value ); }
		//}


		//public decimal Width
		//{
		//	get { return Get( () => Width, 0 ); }
		//	set { Set( () => Width, value ); }
		//}


		//public decimal Height
		//{
		//	get { return Get( () => Height, 0 ); }
		//	set { Set( () => Height, value ); }
		//}


		//public decimal Original
		//{
		//	get { return Get( () => Original, 0 ); }
		//	set { Set( () => Original, value ); }
		//}


		//public decimal QH
		//{
		//	get { return Get( () => QH, 0 ); }
		//	set { Set( () => QH, value ); }
		//}


		public string ServiceLevel
		{
			get { return Get( () => ServiceLevel, "" ); }
			set { Set( () => ServiceLevel, value ); }
		}


		public List<string> ServiceLevels
		{
			get => Get( () => ServiceLevels, new List<string>() );
			set { Set( () => ServiceLevels, value ); }
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public void GetServiceLevels()
		{
			// From Terry: Need to do this to avoid bad requests in the server log
			if( !IsInDesignMode )
			{
				try
				{
					var sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;

					if( sls != null )
					{
						var serviceLevels = ( from S in sls
						                      orderby S.SortOrder, S.OldName
						                      select S.OldName ).ToList();

						Dispatcher.Invoke( () => { ServiceLevels = serviceLevels; } );
					}
				}
				catch( Exception e )
				{
					//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
					Logging.WriteLogLine( "Exception thrown: " + e );
					MessageBox.Show( "Error fetching Service Levels\n" + e, "Error", MessageBoxButton.OK );
				}
			}
			//// TODO Not in db yet
			//List<string> sls = new List<string>()
			//{
			//    "Regular",
			//    "Rush",
			//    "Truck"
			//};

			//return sls;
		}

		//public DateTimeOffset ReadyDate
		//{
		//	get { return Get( () => ReadyDate, DateTimeOffset.Now ); }
		//	set { Set( () => ReadyDate, value ); }
		//}


		//public DateTimeOffset ReadyTime
		//{
		//	get { return Get( () => ReadyTime, DateTimeOffset.Now ); }
		//	set { Set( () => ReadyTime, value ); }
		//}


		//public DateTimeOffset DueDate
		//{
		//	get { return Get( () => DueDate, DateTimeOffset.Now ); }
		//	set { Set( () => DueDate, value ); }
		//}


		//public DateTimeOffset DueTime
		//{
		//	get { return Get( () => DueTime, DateTimeOffset.Now ); }
		//	set { Set( () => DueTime, value ); }
		//}

		public DateTime ReadyDate
		{
			get { return Get( () => ReadyDate, DateTime.Now ); }
			set { Set( () => ReadyDate, value ); }
		}


		//public DateTime ReadyTime
		//{
		//    get { return Get(() => ReadyTime, DateTime.Now); }
		//    set { Set(() => ReadyTime, value); }
		//}


		public DateTime DueDate
		{
			get { return Get( () => DueDate, DateTime.Now ); }
			set { Set( () => DueDate, value ); }
		}


		//public DateTime DueTime
		//{
		//    get { return Get(() => DueTime, DateTime.Now); }
		//    set { Set(() => DueTime, value); }
		//}


		public string POD
		{
			get { return Get( () => POD, "" ); }
			set { Set( () => POD, value ); }
		}


		public string POP
		{
			get { return Get( () => POP, "" ); }
			set { Set( () => POP, value ); }
		}

		//private string HiddenCustomerPhone;

		public string CustomerPhone
		{
			get { return Get( () => CustomerPhone, "" ); }
			set { Set( () => CustomerPhone, value ); }

			// TODO
			//get { return Get(() => HiddenCustomerPhone); }
			//set
			//{
			//    Logging.WriteLogLine("Setting CustomerPhone to: " + value);
			//    HiddenCustomerPhone = value;
			//    //if (value != HiddenCustomerPhone)
			//    //{
			//    //}
			//}
		}

		//public string NewCustomerPhone{ get; set; }

		public string CustomerNotes
		{
			get { return Get( () => CustomerNotes, "" ); }
			set { Set( () => CustomerNotes, value ); }
		}


		public string FirstName
		{
			get { return Get( () => FirstName, "" ); }
			set { Set( () => FirstName, value ); }
		}


		public string LastName
		{
			get { return Get( () => LastName, "" ); }
			set { Set( () => LastName, value ); }
		}


		public string MiddleNames
		{
			get { return Get( () => MiddleNames, "" ); }
			set { Set( () => MiddleNames, value ); }
		}

		public IList CompanyNames
		{
			get { return Get( () => CompanyNames, new List<string>() ); }
			set { Set( () => CompanyNames, value ); }
		}


		public List<CompanyAddress> Companies
		{
			get { return Get( () => Companies, new List<CompanyAddress>() ); }
			set { Set( () => Companies, value ); }
		}


		public void ReloadPrimaryCompanies()
		{
			Logging.WriteLogLine( "Reloading Primary Companies" );

			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          var Names = await Azure.Client.RequestGetCustomerCodeList();

					          if( Names != null )
					          {
						          var CoNames = ( from N in Names
						                          let Ln = N.ToLower()
						                          orderby Ln
						                          select Ln ).ToList();
						          var companies = new List<CompanyAddress>();

						          new Tasks.Task<string>().Run( CoNames, coName =>
						                                                 {
							                                                 try
							                                                 {
								                                                 var company = Azure.Client.RequestGetResellerCustomerCompany( coName ).Result;

								                                                 lock( companies )
									                                                 companies.Add( company );
							                                                 }
							                                                 catch( Exception Exception )
							                                                 {
								                                                 Logging.WriteLogLine( "Exception thrown: " + Exception );
							                                                 }
						                                                 }, 5 );

						          Dispatcher.Invoke( () =>
						                             {
							                             CompanyNames = CoNames;
							                             Companies    = companies;
						                             } );
					          }
				          } );
			}
		}

		// [DependsUpon(nameof(ReloadAcounts))]
		public void GetCompanyNames()
		{
			// From Terry: Need to do this to avoid bad requests in the server log
			if( !IsInDesignMode )
			{
				try
				{
					Logging.WriteLogLine( "Loading Company Names" );
					var Names = Azure.Client.RequestGetCustomerCodeList().Result;

					if( Names != null )
					{
						//var CoNames = ( from N in Names
						//                let Ln = N.ToLower()
						//                orderby Ln
						//                select Ln ).ToList();

						var CoNames = ( from N in Names
						                orderby N.ToLower()
						                select N ).ToList();

						var companies = new List<CompanyAddress>();

						new Tasks.Task<string>().Run( CoNames, coName =>
						                                       {
							                                       try
							                                       {
								                                       var company = Azure.Client.RequestGetResellerCustomerCompany( coName ).Result;

								                                       lock( companies )
									                                       companies.Add( company );
							                                       }
							                                       catch( Exception Exception )
							                                       {
								                                       Logging.WriteLogLine( "Exception thrown: " + Exception );
							                                       }
						                                       }, 5 );

						Dispatcher.Invoke( () =>
						                   {
							                   CompanyNames = CoNames;
							                   Companies    = companies;
						                   } );
					}
				}
				catch( Exception e )
				{
					//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
					Logging.WriteLogLine( "Exception thrown: " + e );
					MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
				}
			}
		}


		public string SelectedAccount
		{
			get { return Get( () => SelectedAccount, "" ); }
			set { Set( () => SelectedAccount, value ); }
		}

		[DependsUpon( nameof( SelectedAccount ) )]
		public void WhenSelectedAccountChanges()
		{
			Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount );

			if( TripId.IsNullOrWhiteSpace() && SelectedAccount.IsNotNullOrWhiteSpace() )
			{
				Task.Run( async () =>
				          {
					          try
					          {
						          var newTripId = await Azure.Client.RequestGetNextTripId();

						          Dispatcher.Invoke( () =>
						                             {
							                             Logging.WriteLogLine( "Fetched new TripId: " + newTripId );
							                             TripId = newTripId;

							                             foreach( var ca in Companies )
							                             {
								                             if( ca.CompanyNumber == SelectedAccount )
								                             {
									                             CustomerPhone = ca.Phone;

									                             DeliveryCountry = ca.Country;
									                             PickupCountry   = ca.Country;
									                             WhenDeliveryCountryChanges();
									                             WhenPickupCountryChanges();

									                             break;
								                             }
							                             }
						                             } );
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "Exception thrown fetching new TripId: " + e );
					          }
				          } );
			}
		}

		public IList PackageTypes
		{
			get { return Get( () => PackageTypes, new List<string>() ); }
			set { Set( () => PackageTypes, value ); }
		}


		private void GetPackageTypes()
		{
			// From Terry: Need to do this to avoid bad requests in the server log
			if( !IsInDesignMode )
			{
				try
				{
					var pts = Azure.Client.RequestGetPackageTypes().Result;

					if( pts != null )
					{
						var PTypes = ( from Pt in pts
						               orderby Pt.SortOrder, Pt.Description
						               select Pt.Description ).ToList();

						Dispatcher.Invoke( () => { PackageTypes = PTypes; } );
					}
				}
				catch( Exception e )
				{
					//Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
					Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );
					MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
				}
			}
		}


		//public string SelectedPackageType
		//{
		//	get { return Get( () => SelectedPackageType, "" ); }
		//	set { Set( () => SelectedPackageType, value ); }
		//}


		public IList Drivers
		{
			get { return Get( () => Drivers, new List<string>() ); }
			set { Set( () => Drivers, value ); }
		}

		private void GetDrivers()
		{
			if( !IsInDesignMode )
			{
				try
				{
					var Drvs = Azure.Client.RequestGetStaff().Result;

					if( !( Drvs is null ) )
					{
						Drivers = ( from D in Drvs
						            select D.StaffId.ToLower() ).ToList();
					}
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown: " + e );
					MessageBox.Show( "Error fetching Drivers\n" + e, "Error", MessageBoxButton.OK );
				}
			}
		}


		public string SelectedDriver
		{
			get { return Get( () => SelectedDriver, "" ); }

			set { Set( () => SelectedDriver, value ); }
		}


		public List<CompanyByAccountSummary> Addresses
		{
			get { return Get( () => Addresses, new List<CompanyByAccountSummary>() ); }
			set { Set( () => Addresses, value ); }
		}


		public List<string> AddressNames
		{
			get { return Get( () => AddressNames, new List<string>() ); }
			set { Set( () => AddressNames, value ); }
		}

		[DependsUpon( nameof( SelectedAccount ) )]
		public async void WhenAccountChanges()
		{
			if( !IsInDesignMode )
			{
				Account = SelectedAccount;

				WhenSelectedAccountChanges();

				var company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode( SelectedAccount );

				if( ( company != null ) && ( company.BillingCompany != null ) )
				{
					Dispatcher.Invoke( () =>
					                   {
						                   Customer = company.BillingCompany.CompanyName;

						                   if( CurrentTrip != null )
						                   {
							                   // TODO
							                   //if (CurrentTrip.BillingAddressPhone.IsNullOrWhiteSpace() 
							                   //         && company.BillingCompany.Phone.IsNotNullOrWhiteSpace())
							                   //{
							                   //    CustomerPhone = company.BillingCompany.Phone;
							                   //}
							                   //if (CurrentTrip.BillingAddressNotes.IsNullOrWhiteSpace())
							                   //{
							                   //    CustomerNotes = company.Company.Notes;
							                   //}

							                   PickupCountry     = company.BillingCompany.Country;
							                   PickupProvState   = company.BillingCompany.Region;
							                   DeliveryCountry   = company.BillingCompany.Country;
							                   DeliveryProvState = company.BillingCompany.Region;
						                   }
					                   } );
				}

				try
				{
					if( !string.IsNullOrEmpty( SelectedAccount ) )
					{
						var csl = await Azure.Client.RequestGetCustomerCompaniesSummary( Account );

						if( csl != null )
						{
							//var Addrs = ( from A in csl
							//              let Cn = A.CompanyName.ToLower()
							//              orderby Cn
							//              group A by Cn
							//              into G
							//              select G.First() ).ToList();

							//var Names = ( from A in Addrs
							//              let Cn = A.CompanyName.ToLower()
							//              orderby Cn
							//              group Cn by Cn
							//              into G
							//              select G.First() ).ToList();

							var Addrs = new List<CompanyByAccountSummary>();
							var Names = new List<string>();
							var sd    = new SortedDictionary<string, CompanyByAccountSummary>();

							foreach( var cs in csl )
							{
								if( cs.CompanyName.IsNotNullOrWhiteSpace() && !sd.ContainsKey( cs.CompanyName.ToLower() ) )
									sd.Add( cs.CompanyName.ToLower(), cs );
								else
									Logging.WriteLogLine( "Skipping - cs: " + cs.CompanyName );
							}

							foreach( var key in sd.Keys )
							{
								Addrs.Add( sd[ key ] );
								Names.Add( sd[ key ].CompanyName );
							}

							Dispatcher.Invoke( () =>
							                   {
								                   Addresses    = Addrs;
								                   AddressNames = Names;

								                   // KLUDGE Try to load desired pu & del addresses 
								                   // if (CurrentTrip != null)
								                   //{
								                   //    if (CurrentTrip.PickupCompanyName.IsNotNullOrEmpty())
								                   //    {
								                   //        SelectedPickupAddress = CurrentTrip.PickupCompanyName;
								                   //    }
								                   //    if (CurrentTrip.DeliveryCompanyName.IsNotNullOrEmpty())
								                   //    {
								                   //        SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
								                   //    }
								                   //}
							                   } );
						}
					}
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown: " + e );
					MessageBox.Show( "Error fetching Addresses\n" + e, "Error", MessageBoxButton.OK );
				}
			}
		}

		public string SelectedPickupAddress
		{
			get { return Get( () => SelectedPickupAddress, "" ); }

			set { Set( () => SelectedPickupAddress, value ); }
		}


		public string SelectedDeliveryAddress
		{
			get { return Get( () => SelectedDeliveryAddress, "" ); }

			set { Set( () => SelectedDeliveryAddress, value ); }
		}

		public LOG Log
		{
			get { return Get( () => Log, LOG.TRIP ); }

			set { Set( () => Log, value ); }
		}

		public string User
		{
			get { return Get( () => User, "" ); }

			set { Set( () => User, value ); }
		}


		public bool IsNewTrip
		{
			get { return Get( () => IsNewTrip, false ); }

			set { Set( () => IsNewTrip, value ); }
		}


		public string HelpUri
		{
			get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638124048/Shipment+Entry" ); }
		}

	#endregion

	#region Actions

		/// <summary>
		///     Performs a deep clone of the source trip.
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
		private static DisplayTrip Clone( DisplayTrip src )
		{
			var dt = new DisplayTrip();

			if( src != null )
			{
				Logging.WriteLogLine( "Cloning" );
				dt.AccountId = src.AccountId;

				//dt.Background = src.Background;
				dt.Barcode                     = src.Barcode;
				dt.BillingAccountId            = src.BillingAccountId;
				dt.BillingAddressAddressLine1  = src.BillingAddressAddressLine1;
				dt.BillingAddressAddressLine2  = src.BillingAddressAddressLine2;
				dt.BillingAddressBarcode       = src.BillingAddressBarcode;
				dt.BillingAddressCity          = src.BillingAddressCity;
				dt.BillingAddressCountry       = src.BillingAddressCountry;
				dt.BillingAddressCountryCode   = src.BillingAddressCountryCode;
				dt.BillingAddressEmailAddress  = src.BillingAddressEmailAddress;
				dt.BillingAddressEmailAddress1 = src.BillingAddressEmailAddress1;
				dt.BillingAddressEmailAddress2 = src.BillingAddressEmailAddress2;
				dt.BillingAddressFax           = src.BillingAddressFax;
				dt.BillingAddressLatitude      = src.BillingAddressLatitude;
				dt.BillingAddressLongitude     = src.BillingAddressLongitude;
				dt.BillingAddressMobile        = src.BillingAddressMobile;
				dt.BillingAddressMobile1       = src.BillingAddressMobile1;
				dt.BillingAddressNotes         = src.BillingAddressNotes;
				dt.BillingAddressPhone         = src.BillingAddressPhone;
				dt.BillingAddressPhone1        = src.BillingAddressPhone1;
				dt.BillingAddressPostalBarcode = src.BillingAddressPostalBarcode;
				dt.BillingAddressPostalCode    = src.BillingAddressPostalCode;
				dt.BillingAddressRegion        = src.BillingAddressRegion;
				dt.BillingAddressSuite         = src.BillingAddressSuite;
				dt.BillingAddressVicinity      = src.BillingAddressVicinity;
				dt.BillingCompanyName          = src.BillingCompanyName;
				dt.BillingContact              = src.BillingContact;
				dt.BillingNotes                = src.BillingNotes;
				dt.Board                       = src.Board;
				dt.BroadcastToDispatchBoard    = src.BroadcastToDispatchBoard;
				dt.BroadcastToDriver           = src.BroadcastToDriver;
				dt.BroadcastToDriverBoard      = src.BroadcastToDriverBoard;
				dt.CallerEmail                 = src.CallerEmail;
				dt.CallerName                  = src.CallerName;
				dt.CallerPhone                 = src.CallerPhone;
				dt.CallTime                    = src.CallTime;
				dt.ClaimLatitude               = src.ClaimLatitude;
				dt.ClaimLongitude              = src.ClaimLongitude;
				dt.ClaimTime                   = src.ClaimTime;
				dt.ContainerId                 = src.ContainerId;
				dt.CurrentZone                 = src.CurrentZone;
				dt.DangerousGoods              = src.DangerousGoods;

				//dt.Delivered = src.Delivered;
				dt.DeliveryAccountId            = src.DeliveryAccountId;
				dt.DeliveryAddressAddressLine1  = src.DeliveryAddressAddressLine1;
				dt.DeliveryAddressAddressLine2  = src.DeliveryAddressAddressLine2;
				dt.DeliveryAddressBarcode       = src.DeliveryAddressBarcode;
				dt.DeliveryAddressCity          = src.DeliveryAddressCity;
				dt.DeliveryAddressCountry       = src.DeliveryAddressCountry;
				dt.DeliveryAddressCountryCode   = src.DeliveryAddressCountryCode;
				dt.DeliveryAddressEmailAddress  = src.DeliveryAddressEmailAddress;
				dt.DeliveryAddressEmailAddress1 = src.DeliveryAddressEmailAddress1;
				dt.DeliveryAddressEmailAddress2 = src.DeliveryAddressEmailAddress2;
				dt.DeliveryAddressFax           = src.DeliveryAddressFax;
				dt.DeliveryAddressLatitude      = src.DeliveryAddressLatitude;
				dt.DeliveryAddressLongitude     = src.DeliveryAddressLongitude;
				dt.DeliveryAddressMobile        = src.DeliveryAddressMobile;
				dt.DeliveryAddressMobile1       = src.DeliveryAddressMobile1;
				dt.DeliveryAddressNotes         = src.DeliveryAddressNotes;
				dt.DeliveryAddressPhone         = src.DeliveryAddressPhone;
				dt.DeliveryAddressPhone1        = src.DeliveryAddressPhone1;
				dt.DeliveryAddressPostalBarcode = src.DeliveryAddressPostalBarcode;
				dt.DeliveryAddressPostalCode    = src.DeliveryAddressPostalCode;
				dt.DeliveryAddressRegion        = src.DeliveryAddressRegion;
				dt.DeliveryAddressSuite         = src.DeliveryAddressSuite;
				dt.DeliveryAddressVicinity      = src.DeliveryAddressVicinity;
				dt.DeliveryCompanyName          = src.DeliveryCompanyName;
				dt.DeliveryContact              = src.DeliveryContact;
				dt.DeliveryLatitude             = src.DeliveryLatitude;
				dt.DeliveryLongitude            = src.DeliveryLongitude;
				dt.DeliveryNotes                = src.DeliveryNotes;
				dt.DeliveryTime                 = src.DeliveryTime;
				dt.DeliveryZone                 = src.DeliveryZone;
				dt.DetailsVisible               = src.DetailsVisible;
				dt.Driver                       = src.Driver;
				dt.DueTime                      = src.DueTime;
				dt.EnableAccept                 = src.EnableAccept;
				dt.ExtensionAsJson              = src.ExtensionAsJson;
				dt.Filter                       = src.Filter;
				dt.HasDocuments                 = src.HasDocuments;

				//dt.HasPackages = src.HasPackages;
				dt.IsQuote       = src.IsQuote;
				dt.IsVisible     = src.IsVisible;
				dt.LastModified  = src.LastModified;
				dt.Location      = src.Location;
				dt.Measurement   = src.Measurement;
				dt.MissingPieces = src.MissingPieces;

				//dt.NotReceivedByDevice = src.NotReceivedByDevice;
				dt.Ok                 = src.Ok;
				dt.OriginalPieceCount = src.OriginalPieceCount;

				// dt.Packages = src.Packages;
				if( ( src.Packages != null ) && ( src.Packages.Count > 0 ) )
				{
					foreach( var srcDp in src.Packages )
					{
						var dp = new DisplayPackage( new TripPackage() );
						dp.DetailsVisible = srcDp.DetailsVisible;
						dp.HasDocuments   = srcDp.HasDocuments;
						dp.Height         = srcDp.Height;

						//dp.Items = srcDp.Items;
						if( ( srcDp.Items != null ) && ( srcDp.Items.Count > 0 ) )
						{
							foreach( var srcTi in srcDp.Items )
							{
								var ti = new TripItem();
								ti.Barcode      = srcTi.Barcode;
								ti.Barcode1     = srcTi.Barcode1;
								ti.Description  = srcTi.Description;
								ti.HasDocuments = srcTi.HasDocuments;
								ti.Height       = srcTi.Height;
								ti.ItemCode     = srcTi.ItemCode;
								ti.ItemCode1    = srcTi.ItemCode1;
								ti.Length       = srcTi.Length;
								ti.Original     = srcTi.Original;
								ti.Pieces       = srcTi.Pieces;
								ti.Qh           = srcTi.Qh;
								ti.Reference    = srcTi.Reference;
								ti.Tax1         = srcTi.Tax1;
								ti.Tax2         = srcTi.Tax2;
								ti.Value        = srcTi.Value;
								ti.Volume       = srcTi.Volume;
								ti.Weight       = srcTi.Weight;
								ti.Width        = srcTi.Weight;
								ti.Width        = srcTi.Width;

								dp.Items.Add( ti );
							}
						}

						dp.Length      = srcDp.Length;
						dp.Original    = srcDp.Original;
						dp.PackageType = srcDp.PackageType;
						dp.Pieces      = srcDp.Pieces;
						dp.Qh          = srcDp.Qh;
						dp.Tax1        = srcDp.Tax1;
						dp.Tax2        = srcDp.Tax2;
						dp.Value       = srcDp.Value;
						dp.Volume      = srcDp.Volume;
						dp.Weight      = srcDp.Weight;
						dp.Width       = srcDp.Width;

						dt.Packages.Add( dp );
					}
				}

				dt.PackageType                = src.PackageType;
				dt.PickupAccountId            = src.PickupAccountId;
				dt.PickupAddressAddressLine1  = src.PickupAddressAddressLine1;
				dt.PickupAddressAddressLine2  = src.PickupAddressAddressLine2;
				dt.PickupAddressBarcode       = src.PickupAddressBarcode;
				dt.PickupAddressCity          = src.PickupAddressCity;
				dt.PickupAddressCountry       = src.PickupAddressCountry;
				dt.PickupAddressCountryCode   = src.PickupAddressCountryCode;
				dt.PickupAddressEmailAddress  = src.PickupAddressEmailAddress;
				dt.PickupAddressEmailAddress1 = src.PickupAddressEmailAddress1;
				dt.PickupAddressEmailAddress2 = src.PickupAddressEmailAddress2;
				dt.PickupAddressFax           = src.PickupAddressFax;
				dt.PickupAddressLatitude      = src.PickupAddressLatitude;
				dt.PickupAddressLongitude     = src.PickupAddressLongitude;
				dt.PickupAddressMobile        = src.PickupAddressMobile;
				dt.PickupAddressMobile1       = src.PickupAddressMobile1;
				dt.PickupAddressNotes         = src.PickupAddressNotes;
				dt.PickupAddressPhone         = src.PickupAddressPhone;
				dt.PickupAddressPhone1        = src.PickupAddressPhone1;
				dt.PickupAddressPostalBarcode = src.PickupAddressPostalBarcode;
				dt.PickupAddressPostalCode    = src.PickupAddressPostalCode;
				dt.PickupAddressRegion        = src.PickupAddressRegion;
				dt.PickupAddressSuite         = src.PickupAddressSuite;
				dt.PickupAddressVicinity      = src.PickupAddressVicinity;
				dt.PickupCompanyName          = src.PickupCompanyName;
				dt.PickupContact              = src.PickupContact;
				dt.PickupLatitude             = src.PickupLatitude;
				dt.PickupLongitude            = src.PickupLongitude;
				dt.PickupNotes                = src.PickupNotes;
				dt.PickupTime                 = src.PickupTime;
				dt.PickupZone                 = src.PickupZone;
				dt.Pieces                     = src.Pieces;
				dt.POD                        = src.POD;
				dt.POP                        = src.POP;
				dt.Program                    = src.Program;
				dt.ReadByDriver               = src.ReadByDriver;
				dt.ReadyTime                  = src.ReadyTime;
				dt.ReceivedByDevice           = src.ReceivedByDevice;
				dt.Reference                  = src.Reference;
				dt.Selected                   = src.Selected;
				dt.ServiceLevel               = src.ServiceLevel;

				//dt.Signatures = src.Signatures;
				if( ( src.Signatures != null ) && ( src.Signatures.Count > 0 ) )
				{
					foreach( var srcSig in src.Signatures )
					{
						var sig = new Signature();
						sig.Date    = srcSig.Date;
						sig.Height  = srcSig.Height;
						sig.Points  = srcSig.Points;
						sig.Status  = srcSig.Status;
						sig.Status1 = srcSig.Status1;
						sig.Status2 = srcSig.Status2;
						sig.Width   = srcSig.Width;

						dt.Signatures.Add( sig );
					}
				}

				dt.Status  = src.Status;
				dt.Status1 = src.Status1;
				dt.Status2 = src.Status2;

				//dt.TripCharges = src.TripCharges;
				if( ( src.TripCharges != null ) && ( src.TripCharges.Count > 0 ) )
				{
					foreach( var srcTc in src.TripCharges )
					{
						var tc = new TripCharge();
						tc.ChargeId = srcTc.ChargeId;
						tc.Quantity = srcTc.Quantity;
						tc.Text     = srcTc.Text;
						tc.Value    = srcTc.Value;

						dt.TripCharges.Add( tc );
					}
				}

				dt.TripId             = src.TripId;
				dt.UNClass            = src.UNClass;
				dt.UndeliverableNotes = src.UndeliverableNotes;
				dt.VerifiedLatitude   = src.VerifiedLatitude;
				dt.VerifiedLongitude  = src.VerifiedLongitude;
				dt.VerifiedTime       = src.VerifiedTime;
				dt.Volume             = src.Volume;
				dt.Weight             = src.Weight;
			}

			return dt;
		}

		//[DependsUpon(nameof(CurrentTrip))]
		public void WhenCurrentTripChanges()
		{
			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "CurrentTrip has changed: " + CurrentTrip.TripId );

				var dtCopy = Clone( CurrentTrip );
				CurrentTrip = dtCopy;

				//Task.WaitAll(Task.Run(() =>
				//{
				Dispatcher.Invoke( () =>
				                   {
					                   //Execute_ClearTripEntry(true);

					                   // Need to force the loading in order that the address combos are populated
					                   Task.WaitAll( Task.Run( () =>
					                                           {
						                                           //WhenAccountChanges(dtCopy.AccountId);
						                                           var csl = Azure.Client.RequestGetCustomerCompaniesSummary( dtCopy.AccountId ).Result;

						                                           if( csl != null )
						                                           {
							                                           //var Addrs = (from A in csl
							                                           //             let Cn = A.CompanyName.ToLower()
							                                           //             orderby Cn
							                                           //             group A by Cn
							                                           //                into G
							                                           //             select G.First()).ToList();

							                                           //var Names = (from A in Addrs
							                                           //             let Cn = A.CompanyName.ToLower()
							                                           //             orderby Cn
							                                           //             group Cn by Cn
							                                           //                into G
							                                           //             select G.First()).ToList();
							                                           var Addrs = new List<CompanyByAccountSummary>();
							                                           var Names = new List<string>();
							                                           var sd    = new SortedDictionary<string, CompanyByAccountSummary>();

							                                           foreach( var cs in csl )
							                                           {
								                                           if( cs.CompanyName.IsNotNullOrWhiteSpace() && !sd.ContainsKey( cs.CompanyName.ToLower() ) )
									                                           sd.Add( cs.CompanyName.ToLower(), cs );
							                                           }

							                                           foreach( var key in sd.Keys )
							                                           {
								                                           Addrs.Add( sd[ key ] );
								                                           Names.Add( sd[ key ].CompanyName );
							                                           }

							                                           Addresses    = Addrs;
							                                           AddressNames = Names;
						                                           }
					                                           } ) );

					                   //Task.WaitAll(Task.Run(() =>
					                   //{
					                   Logging.WriteLogLine( "Loading the rest of the trip" );
					                   Status1        = (int)dtCopy.Status1;
					                   DriverCode     = dtCopy.Driver;
					                   SelectedDriver = DriverCode.ToLower();
					                   TPU            = dtCopy.PickupTime.ToString();
					                   TDEL           = dtCopy.DeliveryTime.ToString();

					                   TripId = dtCopy.TripId;

					                   //SelectedAccount = Account = dtCopy.AccountId; // TODO Freezes here
					                   SelectedAccount = dtCopy.AccountId;
					                   Account         = dtCopy.AccountId;
					                   Customer        = dtCopy.AccountId;
					                   CustomerPhone   = dtCopy.BillingAddressPhone;
					                   CustomerNotes   = dtCopy.BillingAddressNotes;

					                   GetCompanyAddress( dtCopy.AccountId ); // Force the population of country and region

					                   Reference = dtCopy.Reference;

					                   CallName  = dtCopy.CallerName;
					                   CallPhone = dtCopy.CallerPhone;
					                   CallEmail = dtCopy.CallerEmail;

					                   if( !string.IsNullOrEmpty( dtCopy.CallTime.ToString() ) )
					                   {
						                   CallDate = dtCopy.Ct.DateTime;

						                   //CallTime = dtCopy.Ct.DateTime;
					                   }

					                   IsQuote            = dtCopy.IsQuote;
					                   UndeliverableNotes = dtCopy.UndeliverableNotes;

					                   // Pickup
					                   PickupCompany = dtCopy.PickupCompanyName;

					                   //GetCompanyAddress(dtCopy.AccountId); // Force the population of country and region

					                   // This is no longer finding the item in the combo
					                   //SelectedPickupAddress = dtCopy.PickupCompanyName;
					                   //SelectedPickupAddress = string.Empty;
					                   var tmpName = string.Empty;

					                   var lcName = dtCopy.PickupCompanyName.Trim().ToLower();

					                   //foreach( string name in AddressNames )
					                   //{
					                   //	//if( name.Equals(lcName, StringComparison.OrdinalIgnoreCase ))
					                   //                   if (name.Trim().ToLower() == lcName)
					                   //	{
					                   //		//SelectedPickupAddress = name;
					                   //		tmpName = name;

					                   //		break;
					                   //	}
					                   //}

					                   var match = from name in AddressNames
					                               where name.ToLower() == lcName
					                               select name;

					                   if( match.Any() )
						                   tmpName = match.First();

					                   //if (string.IsNullOrEmpty(SelectedPickupAddress))
					                   if( !string.IsNullOrEmpty( tmpName ) )
					                   {
						                   //SelectedPickupAddress = dtCopy.PickupCompanyName;
						                   SelectedPickupAddress = tmpName;
					                   }

					                   PickupName  = dtCopy.PickupContact;
					                   PickupPhone = dtCopy.PickupAddressPhone;
					                   PickupSuite = dtCopy.PickupAddressSuite;

					                   PickupStreet = ( dtCopy.PickupAddressAddressLine1 + " " + dtCopy.PickupAddressAddressLine2 ).Trim();

					                   //PickupStreet = dtCopy.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
					                   PickupCity = dtCopy.PickupAddressCity;

					                   // PickupCountry = dtCopy.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
					                   //if (dtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace())
					                   if( dtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() && ( dtCopy.PickupAddressCountry != PickupCountry ) )
					                   {
						                   // TODO Freezes here
						                   PickupCountry = dtCopy.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
					                   }
					                   else if( dtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() )
					                   {
						                   // Empty - load the one from the pickup address
						                   PickupCountry = dtCopy.DeliveryAddressCountry;
					                   }

					                   WhenPickupCountryChanges();

					                   // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
					                   PickupProvState    = FindItemInRegionsList( PickupRegions, dtCopy.PickupAddressRegion );
					                   PickupPostalZip    = dtCopy.PickupAddressPostalCode;
					                   PickupZone         = dtCopy.PickupZone;
					                   PickupAddressNotes = dtCopy.PickupAddressNotes;
					                   PickupNotes        = dtCopy.PickupNotes;

					                   // Delivery
					                   DeliveryCompany = dtCopy.DeliveryCompanyName;

					                   // This is no longer finding the item in the combo
					                   //SelectedDeliveryAddress = dtCopy.DeliveryCompanyName;
					                   //SelectedDeliveryAddress = string.Empty;
					                   tmpName = string.Empty;

					                   lcName = dtCopy.DeliveryCompanyName.Trim().ToLower();

					                   //foreach( string name in AddressNames )
					                   //{
					                   //	if( name == dtCopy.DeliveryCompanyName.ToLower() )
					                   //	{
					                   //		//SelectedDeliveryAddress = name;
					                   //		tmpName = name;

					                   //		break;
					                   //	}
					                   //}

					                   match = from name in AddressNames
					                           where name.ToLower() == lcName
					                           select name;

					                   if( match.Any() )
						                   tmpName = match.First();

					                   //if (string.IsNullOrEmpty(SelectedDeliveryAddress))
					                   if( !string.IsNullOrEmpty( tmpName ) )
					                   {
						                   //SelectedDeliveryAddress = dtCopy.DeliveryCompanyName;
						                   SelectedDeliveryAddress = tmpName;
					                   }

					                   DeliveryName  = dtCopy.DeliveryContact;
					                   DeliveryPhone = dtCopy.DeliveryAddressPhone;
					                   DeliverySuite = dtCopy.DeliveryAddressSuite;

					                   DeliveryStreet = ( dtCopy.DeliveryAddressAddressLine1 + " " + dtCopy.DeliveryAddressAddressLine2 ).Trim();

					                   //DeliveryStreet = dtCopy.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
					                   DeliveryCity = dtCopy.DeliveryAddressCity;

					                   if( dtCopy.DeliveryAddressCountry.IsNotNullOrWhiteSpace() && ( dtCopy.DeliveryAddressCountry != DeliveryCountry ) )
						                   DeliveryCountry = dtCopy.DeliveryAddressCountry; // Had to move before prov so that the DeliverRegions was loaded
					                   else if( dtCopy.PickupAddressCountry.IsNotNullOrWhiteSpace() )
					                   {
						                   // Empty - load the one from the pickup address
						                   DeliveryCountry = dtCopy.PickupAddressCountry;
					                   }

					                   WhenDeliveryCountryChanges();

					                   //else
					                   //{
					                   //    DeliveryCountry = Selected
					                   //}

					                   // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
					                   DeliveryProvState    = FindItemInRegionsList( DeliveryRegions, dtCopy.DeliveryAddressRegion );
					                   DeliveryPostalZip    = dtCopy.DeliveryAddressPostalCode;
					                   DeliveryZone         = dtCopy.DeliveryZone;
					                   DeliveryAddressNotes = dtCopy.DeliveryAddressNotes;
					                   DeliveryNotes        = dtCopy.DeliveryNotes;

					                   // Package
					                   //PackageType = dtCopy.PackageType;
					                   //Weight = dtCopy.Weight;
					                   //TotalWeight = Weight;
					                   //Pieces = (int)dtCopy.Pieces;
					                   //TotalPieces = Pieces;
					                   //Length = 0;   // TODO Fix this
					                   //Width = 0;    // TODO Fix this
					                   //Height = 0;   // TODO Fix this
					                   //Original = 0; // TODO Fix this
					                   //TotalOriginal = Original;
					                   //QH = 0; // TODO Fix this

					                   // Service
					                   ServiceLevel = dtCopy.ServiceLevel;
					                   PackageType  = dtCopy.PackageType;
					                   ReadyDate    = dtCopy.ReadyTime.DateTime;

					                   //ReadyTime = dtCopy.ReadyTime.DateTime;
					                   DueDate = dtCopy.DueTime.DateTime;

					                   //DueTime = dtCopy.DueTime.DateTime;

					                   // POP, POD & Docs
					                   POP = dtCopy.POP;
					                   POD = dtCopy.POD;

					                   // TODO Signatures

					                   //SelectedPackageType = dtCopy.PackageType;
					                   SelectedDriver = dtCopy.Driver;

					                   TripPackages.Clear();

					                   // Need at least 1 for the row to be built
					                   view.BuildPackageRows( this );

					                   //TripItems = dtCopy.TripItems;
					                   TripPackages = dtCopy.Packages;
					                   Logging.WriteLogLine( "Packages.Count: " + dtCopy.Packages.Count );

					                   if( TripPackages.Count == 0 )
					                   {
						                   var oc = new ObservableCollection<DisplayPackage>
						                            {
							                            new DisplayPackage( new TripPackage() )
						                            };
						                   TripPackages = oc;
					                   }

					                   // TripPackages = new ObservableCollection<Boards.Common.DisplayTrip.DisplayPackage>( dtCopy.Packages );

					                   // Update TripItemsInventory
					                   TripItemsInventory.Clear();
					                   var sdict = new SortedDictionary<int, List<ExtendedInventory>>();

					                   for( var i = 0; i < TripPackages.Count; i++ )
					                   {
						                   TripPackage tp = TripPackages[ i ];

						                   if( tp.Items.IsNotNull() && ( tp.Items.Count > 0 ) )
						                   {
							                   Logging.WriteLogLine( "Package " + tp.PackageType + " contains " + tp.Items.Count + " items" );
							                   var eis = new List<ExtendedInventory>();

							                   for( var j = 0; j < tp.Items.Count; j++ )
							                   {
								                   var ti = tp.Items[ j ];

								                   if( ti != null )
								                   {
									                   PmlInventory pi;

									                   foreach( var tmp in Inventory )
									                   {
										                   if( tmp.Barcode == ti.Barcode )
										                   {
											                   pi = tmp;
											                   var ei = new ExtendedInventory( ti.Description, (int)ti.Pieces, ti.ItemCode, pi );
											                   eis.Add( ei );

											                   break;
										                   }
									                   }
								                   }
							                   }

							                   //TripItemsInventory.Add(i + 1, eis);
							                   sdict.Add( i + 1, eis );
						                   }
					                   }

					                   TripItemsInventory = sdict;

					                   if( ( TripItemsInventory.Count > 0 ) && TripItemsInventory.ContainsKey( 1 ) )
						                   SelectedInventoryForTripItem = TripItemsInventory[ 1 ];

					                   view.BuildPackageRows( this );

					                   //if ((TripItems != null) && (TripItems.Count > 0))
					                   //{
					                   //    Logging.WriteLogLine("TripItem.Count: " + TripItems.Count);
					                   //    foreach (var ti in TripItems)
					                   //    {
					                   //        Logging.WriteLogLine(TripItemToString(ti));

					                   //        // KLUDGE
					                   //        view.BuildNewPackageTypeRow(this, ti);

					                   //        TotalOriginal += ti.Original;
					                   //        TotalPieces += (int)ti.Pieces;
					                   //        TotalWeight += ti.Weight;
					                   //    }
					                   //    view.BuildNewPackageTotalRow(this);
					                   //}

					                   // Charges

					                   // Some of the fields aren't being updated, and this helps
					                   //if (!string.IsNullOrEmpty(dtCopy.CallTime))
					                   if( dtCopy.Ct != null )
					                   {
						                   CallDate = dtCopy.Ct.DateTime;

						                   //CallTime = dtCopy.Ct.DateTime;
					                   }

					                   if( dtCopy.ReadyTime != null )
					                   {
						                   ReadyDate = dtCopy.ReadyTime.DateTime;

						                   //ReadyTime = dtCopy.ReadyTime.DateTime;
					                   }

					                   if( dtCopy.DueTime != null )
					                   {
						                   DueDate = dtCopy.DueTime.DateTime;

						                   //DueTime = dtCopy.DueTime.DateTime;
					                   }

					                   tmpName = string.Empty;

					                   foreach( var name in AddressNames )
					                   {
						                   if( name == dtCopy.PickupCompanyName.ToLower() )
						                   {
							                   tmpName = name;

							                   break;
						                   }
					                   }

					                   if( !string.IsNullOrEmpty( tmpName ) )
						                   SelectedPickupAddress = tmpName;

					                   tmpName = string.Empty;

					                   foreach( var name in AddressNames )
					                   {
						                   if( name == dtCopy.DeliveryCompanyName.ToLower() )
						                   {
							                   //SelectedDeliveryAddress = name;
							                   tmpName = name;

							                   break;
						                   }
					                   }

					                   if( !string.IsNullOrEmpty( tmpName ) )
						                   SelectedDeliveryAddress = tmpName;

					                   CustomerPhone = dtCopy.BillingAddressPhone;
					                   CustomerNotes = dtCopy.BillingAddressNotes;

					                   Task.WaitAll( Task.Run( async () =>
					                                           {
						                                           try
						                                           {
							                                           // From Terry: Need to do this to avoid bad requests in the server log
							                                           if( !IsInDesignMode )
							                                           {
								                                           var ca = await Azure.Client.RequestGetResellerCustomerCompany( dtCopy.AccountId );
								                                           Customer = ca.CompanyName;
							                                           }
						                                           }
						                                           catch( Exception e )
						                                           {
							                                           Logging.WriteLogLine( "Exception thrown looking up Customer for AccountId: " + dtCopy.AccountId + " e->" + e );
						                                           }
					                                           } ) );

					                   //}));
				                   } );

				//}));

				Dispatcher.Invoke( () =>
				                   {
					                   Logging.WriteLogLine( "Setting tbCustomerPhone.Text directly to " + dtCopy.BillingAddressPhone );
					                   view.tbCustomerPhone.Text = dtCopy.BillingAddressPhone;

					                   view.BuildPackageRows( this );
				                   } );
			}
		}

		//[DependsUpon(nameof(CurrentTrip))]
		//public void WhenCurrentTripChanges()
		//{
		//    if (CurrentTrip != null)
		//    {
		//        Logging.WriteLogLine("CurrentTrip has changed: " + CurrentTrip.TripId);

		//        //Task.WaitAll(Task.Run(() =>
		//        //{
		//        Dispatcher.Invoke(() =>
		//        {
		//            //Execute_ClearTripEntry(true);

		//            // Need to force the loading in order that the address combos are populated
		//            Task.WaitAll(Task.Run(() =>
		//                                {
		//                                //WhenAccountChanges(CurrentTrip.AccountId);
		//                                var csl = Azure.Client.RequestGetCustomerCompaniesSummary(CurrentTrip.AccountId).Result;

		//                                    if (csl != null)
		//                                    {
		//                                        var Addrs = (from A in csl
		//                                                     let Cn = A.CompanyName.ToLower()
		//                                                     orderby Cn
		//                                                     group A by Cn
		//                                                        into G
		//                                                     select G.First()).ToList();

		//                                        var Names = (from A in Addrs
		//                                                     let Cn = A.CompanyName.ToLower()
		//                                                     orderby Cn
		//                                                     group Cn by Cn
		//                                                        into G
		//                                                     select G.First()).ToList();

		//                                        Addresses = Addrs;
		//                                        AddressNames = Names;
		//                                    }
		//                                }));

		//            //Task.WaitAll(Task.Run(() =>
		//            //{
		//            Logging.WriteLogLine("Loading the rest of the trip");
		//            Status1 = (int)CurrentTrip.Status1;
		//            DriverCode = CurrentTrip.Driver;
		//            SelectedDriver = DriverCode.ToLower();
		//            TPU = CurrentTrip.PickupTime.ToString();
		//            TDEL = CurrentTrip.DeliveryTime.ToString();

		//            TripId = CurrentTrip.TripId;

		//            //SelectedAccount = Account = CurrentTrip.AccountId; // TODO Freezes here
		//            SelectedAccount = CurrentTrip.AccountId;
		//            Account = CurrentTrip.AccountId;
		//            Customer = CurrentTrip.AccountId;
		//            CustomerPhone = CurrentTrip.BillingAddressPhone;
		//            CustomerNotes = CurrentTrip.BillingAddressNotes;

		//            GetCompanyAddress(CurrentTrip.AccountId); // Force the population of country and region

		//            Reference = CurrentTrip.Reference;

		//            CallName = CurrentTrip.CallerName;
		//            CallPhone = CurrentTrip.CallerPhone;
		//            CallEmail = CurrentTrip.CallerEmail;

		//            if (!string.IsNullOrEmpty(CurrentTrip.CallTime.ToString()))
		//            {
		//                CallDate = CurrentTrip.Ct.DateTime;

		//                //CallTime = CurrentTrip.Ct.DateTime;
		//            }

		//            IsQuote = CurrentTrip.IsQuote;
		//            UndeliverableNotes = CurrentTrip.UndeliverableNotes;

		//            // Pickup
		//            PickupCompany = CurrentTrip.PickupCompanyName;

		//            //GetCompanyAddress(CurrentTrip.AccountId); // Force the population of country and region

		//            // This is no longer finding the item in the combo
		//            //SelectedPickupAddress = CurrentTrip.PickupCompanyName;
		//            //SelectedPickupAddress = string.Empty;
		//            string tmpName = string.Empty;

		//            string lcName = CurrentTrip.PickupCompanyName.Trim().ToLower();
		//            //foreach( string name in AddressNames )
		//            //{
		//            //	//if( name.Equals(lcName, StringComparison.OrdinalIgnoreCase ))
		//            //                   if (name.Trim().ToLower() == lcName)
		//            //	{
		//            //		//SelectedPickupAddress = name;
		//            //		tmpName = name;

		//            //		break;
		//            //	}
		//            //}

		//            IEnumerable<string> match = from name in AddressNames where name.ToLower() == lcName select name;
		//            if (match.Any())
		//            {
		//                tmpName = match.First();
		//            }

		//            //if (string.IsNullOrEmpty(SelectedPickupAddress))
		//            if (!string.IsNullOrEmpty(tmpName))
		//            {
		//                //SelectedPickupAddress = CurrentTrip.PickupCompanyName;
		//                SelectedPickupAddress = tmpName;
		//            }

		//            PickupName = CurrentTrip.PickupContact;
		//            PickupPhone = CurrentTrip.PickupAddressPhone;
		//            PickupSuite = CurrentTrip.PickupAddressSuite;

		//            PickupStreet = (CurrentTrip.PickupAddressAddressLine1 + " " + CurrentTrip.PickupAddressAddressLine2).Trim();

		//            //PickupStreet = CurrentTrip.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
		//            PickupCity = CurrentTrip.PickupAddressCity;

		//            // PickupCountry = CurrentTrip.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
		//            //if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
		//            if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace() && CurrentTrip.PickupAddressCountry != PickupCountry)
		//            {
		//                // TODO Freezes here
		//                PickupCountry = CurrentTrip.PickupAddressCountry; // Had to move before prov so that the PickupRegions was loaded
		//            }
		//            else if (CurrentTrip.DeliveryAddressCountry.IsNotNullOrWhiteSpace())
		//            {
		//                // Empty - load the one from the pickup address
		//                PickupCountry = CurrentTrip.DeliveryAddressCountry;
		//            }
		//            WhenPickupCountryChanges();

		//            // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
		//            PickupProvState = FindItemInRegionsList(PickupRegions, CurrentTrip.PickupAddressRegion);
		//            PickupPostalZip = CurrentTrip.PickupAddressPostalCode;
		//            PickupZone = CurrentTrip.PickupZone;
		//            PickupAddressNotes = CurrentTrip.PickupAddressNotes;
		//            PickupNotes = CurrentTrip.PickupNotes;

		//            // Delivery
		//            DeliveryCompany = CurrentTrip.DeliveryCompanyName;

		//            // This is no longer finding the item in the combo
		//            //SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
		//            //SelectedDeliveryAddress = string.Empty;
		//            tmpName = string.Empty;

		//            lcName = CurrentTrip.DeliveryCompanyName.Trim().ToLower();
		//            //foreach( string name in AddressNames )
		//            //{
		//            //	if( name == CurrentTrip.DeliveryCompanyName.ToLower() )
		//            //	{
		//            //		//SelectedDeliveryAddress = name;
		//            //		tmpName = name;

		//            //		break;
		//            //	}
		//            //}

		//            match = from name in AddressNames where name.ToLower() == lcName select name;
		//            if (match.Any())
		//            {
		//                tmpName = match.First();
		//            }

		//            //if (string.IsNullOrEmpty(SelectedDeliveryAddress))
		//            if (!string.IsNullOrEmpty(tmpName))
		//            {
		//                //SelectedDeliveryAddress = CurrentTrip.DeliveryCompanyName;
		//                SelectedDeliveryAddress = tmpName;
		//            }

		//            DeliveryName = CurrentTrip.DeliveryContact;
		//            DeliveryPhone = CurrentTrip.DeliveryAddressPhone;
		//            DeliverySuite = CurrentTrip.DeliveryAddressSuite;

		//            DeliveryStreet = (CurrentTrip.DeliveryAddressAddressLine1 + " " + CurrentTrip.DeliveryAddressAddressLine2).Trim();

		//            //DeliveryStreet = CurrentTrip.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
		//            DeliveryCity = CurrentTrip.DeliveryAddressCity;

		//            if (CurrentTrip.DeliveryAddressCountry.IsNotNullOrWhiteSpace() && CurrentTrip.DeliveryAddressCountry != DeliveryCountry)
		//            {
		//                DeliveryCountry = CurrentTrip.DeliveryAddressCountry; // Had to move before prov so that the DeliverRegions was loaded
		//            }
		//            else if (CurrentTrip.PickupAddressCountry.IsNotNullOrWhiteSpace())
		//            {
		//                // Empty - load the one from the pickup address
		//                DeliveryCountry = CurrentTrip.PickupAddressCountry;
		//            }
		//            WhenDeliveryCountryChanges();

		//            //else
		//            //{
		//            //    DeliveryCountry = Selected
		//            //}

		//            // Problem incoming value is "OHIO", while the item in PickupRegions is "Ohio"                
		//            DeliveryProvState = FindItemInRegionsList(DeliveryRegions, CurrentTrip.DeliveryAddressRegion);
		//            DeliveryPostalZip = CurrentTrip.DeliveryAddressPostalCode;
		//            DeliveryZone = CurrentTrip.DeliveryZone;
		//            DeliveryAddressNotes = CurrentTrip.DeliveryAddressNotes;
		//            DeliveryNotes = CurrentTrip.DeliveryNotes;

		//            // Package
		//            //PackageType = CurrentTrip.PackageType;
		//            //Weight = CurrentTrip.Weight;
		//            //TotalWeight = Weight;
		//            //Pieces = (int)CurrentTrip.Pieces;
		//            //TotalPieces = Pieces;
		//            //Length = 0;   // TODO Fix this
		//            //Width = 0;    // TODO Fix this
		//            //Height = 0;   // TODO Fix this
		//            //Original = 0; // TODO Fix this
		//            //TotalOriginal = Original;
		//            //QH = 0; // TODO Fix this

		//            // Service
		//            ServiceLevel = CurrentTrip.ServiceLevel;
		//            PackageType = CurrentTrip.PackageType;
		//            ReadyDate = CurrentTrip.ReadyTime.DateTime;

		//            //ReadyTime = CurrentTrip.ReadyTime.DateTime;
		//            DueDate = CurrentTrip.DueTime.DateTime;

		//            //DueTime = CurrentTrip.DueTime.DateTime;

		//            // POP, POD & Docs
		//            POP = CurrentTrip.POP;
		//            POD = CurrentTrip.POD;

		//            // TODO Signatures

		//            //SelectedPackageType = CurrentTrip.PackageType;
		//            SelectedDriver = CurrentTrip.Driver;

		//            TripPackages.Clear();

		//            // Need at least 1 for the row to be built
		//            view.BuildPackageRows(this);

		//            //TripItems = CurrentTrip.TripItems;
		//            TripPackages = CurrentTrip.Packages;
		//            Logging.WriteLogLine("Packages.Count: " + CurrentTrip.Packages.Count);
		//            if (TripPackages.Count == 0)
		//            {
		//                ObservableCollection<DisplayPackage> oc = new ObservableCollection<DisplayPackage>
		//                    {
		//                        new DisplayPackage(new TripPackage())
		//                    };
		//                TripPackages = oc;
		//            }

		//            // TripPackages = new ObservableCollection<Boards.Common.DisplayTrip.DisplayPackage>( CurrentTrip.Packages );

		//            // Update TripItemsInventory
		//            TripItemsInventory.Clear();
		//            for (int i = 0; i < TripPackages.Count; i++)
		//            {
		//                TripPackage tp = TripPackages[i];
		//                if (tp.Items.IsNotNull() && tp.Items.Count > 0)
		//                {
		//                    Logging.WriteLogLine("Package " + tp.PackageType + " contains " + tp.Items.Count + " items");
		//                    List <ExtendedInventory> eis = new List<ExtendedInventory>();
		//                    for (int j = 0; j < tp.Items.Count; j++)
		//                    {
		//                        TripItem ti = tp.Items[j];
		//                        if (ti != null)
		//                        {
		//                            PmlInventory pi;
		//                            foreach (PmlInventory tmp in Inventory)
		//                            {
		//                                if (tmp.Barcode == ti.Barcode)
		//                                {
		//                                    pi = tmp;
		//                                    ExtendedInventory ei = new ExtendedInventory(ti.Description, (int)ti.Pieces, ti.ItemCode, pi);
		//                                    eis.Add(ei);
		//                                    break;
		//                                }
		//                            }

		//                        }
		//                    }
		//                    TripItemsInventory.Add(i + 1, eis);
		//                }
		//            }

		//            if (TripItemsInventory.Count > 0 && TripItemsInventory.ContainsKey(1))
		//            {
		//                SelectedInventoryForTripItem = TripItemsInventory[1];
		//            }

		//            view.BuildPackageRows(this);

		//            //if ((TripItems != null) && (TripItems.Count > 0))
		//            //{
		//            //    Logging.WriteLogLine("TripItem.Count: " + TripItems.Count);
		//            //    foreach (var ti in TripItems)
		//            //    {
		//            //        Logging.WriteLogLine(TripItemToString(ti));

		//            //        // KLUDGE
		//            //        view.BuildNewPackageTypeRow(this, ti);

		//            //        TotalOriginal += ti.Original;
		//            //        TotalPieces += (int)ti.Pieces;
		//            //        TotalWeight += ti.Weight;
		//            //    }
		//            //    view.BuildNewPackageTotalRow(this);
		//            //}

		//            // Charges

		//            // Some of the fields aren't being updated, and this helps
		//            //if (!string.IsNullOrEmpty(CurrentTrip.CallTime))
		//            if (CurrentTrip.Ct != null)
		//            {
		//                CallDate = CurrentTrip.Ct.DateTime;

		//                //CallTime = CurrentTrip.Ct.DateTime;
		//            }

		//            if (CurrentTrip.ReadyTime != null)
		//            {
		//                ReadyDate = CurrentTrip.ReadyTime.DateTime;

		//                //ReadyTime = CurrentTrip.ReadyTime.DateTime;
		//            }

		//            if (CurrentTrip.DueTime != null)
		//            {
		//                DueDate = CurrentTrip.DueTime.DateTime;

		//                //DueTime = CurrentTrip.DueTime.DateTime;
		//            }

		//            tmpName = string.Empty;

		//            foreach (string name in AddressNames)
		//            {
		//                if (name == CurrentTrip.PickupCompanyName.ToLower())
		//                {
		//                    tmpName = name;

		//                    break;
		//                }
		//            }

		//            if (!string.IsNullOrEmpty(tmpName))
		//            {
		//                SelectedPickupAddress = tmpName;
		//            }

		//            tmpName = string.Empty;

		//            foreach (string name in AddressNames)
		//            {
		//                if (name == CurrentTrip.DeliveryCompanyName.ToLower())
		//                {
		//                    //SelectedDeliveryAddress = name;
		//                    tmpName = name;

		//                    break;
		//                }
		//            }

		//            if (!string.IsNullOrEmpty(tmpName))
		//            {
		//                SelectedDeliveryAddress = tmpName;
		//            }

		//            CustomerPhone = CurrentTrip.BillingAddressPhone;
		//            CustomerNotes = CurrentTrip.BillingAddressNotes;

		//            Task.WaitAll(Task.Run(async () =>
		//                            {
		//                                try
		//                                {
		//                                        // From Terry: Need to do this to avoid bad requests in the server log
		//                                        if (!IsInDesignMode)
		//                                    {
		//                                        var ca = await Azure.Client.RequestGetResellerCustomerCompany(CurrentTrip.AccountId);
		//                                        Customer = ca.CompanyName;
		//                                    }
		//                                }
		//                                catch (Exception e)
		//                                {
		//                                    Logging.WriteLogLine("Exception thrown looking up Customer for AccountId: " + CurrentTrip.AccountId + " e->" + e);
		//                                }
		//                            }));


		//            //}));

		//        });

		//        //}));

		//        Dispatcher.Invoke(() =>
		//        {
		//            Logging.WriteLogLine("Setting tbCustomerPhone.Text directly to " + CurrentTrip.BillingAddressPhone);
		//            view.tbCustomerPhone.Text = CurrentTrip.BillingAddressPhone;

		//            view.BuildPackageRows(this);
		//        });
		//    }
		//}

		private string FindItemInRegionsList( IList regions, string region )
		{
			var newRegion = string.Empty;

			if( regions != null )
			{
				foreach( string tmp in regions )
				{
					if( tmp.ToLower() == region.ToLower() )
					{
						newRegion = tmp;

						break;
					}
				}
			}

			return newRegion;
		}

		private string FindItemInDeliveryRegionsList( string region )
		{
			var newRegion = string.Empty;

			if( DeliveryRegions != null )
			{
				foreach( string tmp in DeliveryRegions )
				{
					if( tmp.ToLower() == region.ToLower() )
					{
						newRegion = tmp;

						break;
					}
				}
			}

			return newRegion;
		}

		private string TripItemToString( TripItem ti )
		{
			var sb = new StringBuilder();
			sb.Append( "TripItem: " ).Append( ti.Description ).Append( ", ItemCode: " ).Append( ti.ItemCode ).Append( ", ItemCode1: " ).Append( ti.ItemCode1 );
			sb.Append( ", Pieces: " ).Append( ti.Pieces ).Append( ", Weight" ).Append( ti.Weight );

			return sb.ToString();
		}

		/// <summary>
		/// </summary>
		/// <param name="onlyTripId"></param>
		public void Execute_NewTrip( bool onlyTripId = false )
		{
			Logging.WriteLogLine( "New Trip" );
			IsNewTrip = true;

			Task.Run( async () =>
			          {
				          try
				          {
					          Execute_ClearTripEntry();

					          var newTripId = await Azure.Client.RequestGetNextTripId();
					          Logging.WriteLogLine( "Got new tripid: " + newTripId );

					          Dispatcher.Invoke( () =>
					                             {
						                             TripId = newTripId;
						                             var now          = DateTime.Now;
						                             var savedAccount = SelectedAccount;

						                             // Using this to lock the TripId field
						                             CurrentTrip = new DisplayTrip
						                                           {
							                                           TripId    = newTripId,
							                                           Packages  = new ObservableCollection<DisplayPackage>(),
							                                           CallTime  = now,
							                                           ReadyTime = now,
							                                           DueTime   = now
						                                           };

						                             var dp = new DisplayPackage( new TripPackage
						                                                          {
							                                                          Pieces = 1,
							                                                          Weight = 1
						                                                          } );
						                             CurrentTrip.Packages.Add( dp );
						                             var dict = new SortedDictionary<int, List<ExtendedInventory>>();
						                             TripItemsInventory           = dict;
						                             SelectedInventoryForTripItem = new List<ExtendedInventory>();

						                             //TripItemsInventory.Add()

						                             SelectedAccount = Account = savedAccount;

						                             SelectedPickupAddress   = "";
						                             SelectedDeliveryAddress = "";

						                             TotalWeight   = 1;
						                             TotalPieces   = 1;
						                             TotalOriginal = 1;

						                             //CurrentTrip = new DisplayTrip(new Trip())
						                             //{
						                             //    TripId = newTripId,
						                             //    Packages = new List<TripPackage>() { new TripPackage 
						                             //                                               {
						                             //                                                   Weight = 1,
						                             //                                                   Pieces = 1
						                             //                                               }
						                             //                                       }

						                             //};
					                             } );

					          if( !onlyTripId )
					          {
						          Dispatcher.Invoke( () =>
						                             {
							                             Logging.WriteLogLine( "SelectedAccount: " + SelectedAccount + ", Account: " + Account );

							                             WhenAccountChanges(); // Reload the addresses

							                             //var now = DateTimeOffset.Now;
							                             var now = DateTime.Now;
							                             CallDate = now;

							                             //CallTime = now;

							                             ReadyDate = now;

							                             //ReadyTime = now;

							                             DueDate = now;

							                             //DueTime = now;

							                             //Pieces = 1;
							                             //Weight = 1;
						                             } );
					          }

					          if( ServiceLevel.IsNullOrWhiteSpace() )
						          ServiceLevel = ServiceLevels[ 0 ];
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + e );
				          }
			          } );
		}

		/// <summary>
		/// </summary>
		/// <param name="keepCurrentTrip"></param>
		public void Execute_ClearTripEntry( bool keepCurrentTrip = false )
		{
			Logging.WriteLogLine( "Clearing TripEntry" );
			IsNewTrip = false;

			//if (PackageTypes.Count == 0)
			//{
			//    Task.Run(() =>
			//         {
			//             Task.WaitAll( Task.Run(GetPackageTypes) );
			//         });
			//}

			Dispatcher.Invoke( () =>
			                   {
				                   var now = DateTime.Now;

				                   Account = string.Empty;

				                   //CallDate = DateTimeOffset.Now;
				                   CallDate  = now;
				                   CallEmail = string.Empty;
				                   CallName  = string.Empty;
				                   CallPhone = string.Empty;

				                   //CallTime = DateTimeOffset.Now;
				                   //CallTime = now;
				                   if( !keepCurrentTrip )
					                   CurrentTrip = null;

				                   Customer             = string.Empty;
				                   CustomerNotes        = string.Empty;
				                   CustomerPhone        = string.Empty;
				                   DeliveryAddressNotes = string.Empty;
				                   DeliveryCity         = string.Empty;
				                   DeliveryCompany      = string.Empty;
				                   DeliveryCountry      = string.Empty;
				                   DeliveryDefault      = false;
				                   DeliveryName         = string.Empty;
				                   DeliveryNotes        = string.Empty;
				                   DeliveryPhone        = string.Empty;
				                   DeliveryPostalZip    = string.Empty;
				                   DeliveryProvState    = string.Empty;
				                   DeliveryStreet       = string.Empty;
				                   DeliverySuite        = string.Empty;
				                   DeliveryZone         = string.Empty;
				                   DriverCode           = string.Empty;

				                   //DueDate = DateTimeOffset.Now;
				                   //DueTime = DateTimeOffset.Now;
				                   DueDate = now;

				                   //DueTime = now;
				                   FirstName = string.Empty;

				                   //Height = 0;
				                   IsDIM    = false;
				                   IsQuote  = false;
				                   LastName = string.Empty;

				                   //Length = 0;
				                   MiddleNames = string.Empty;

				                   //Original = 0;
				                   PackageType        = string.Empty;
				                   PickupAddressNotes = string.Empty;
				                   PickupCity         = string.Empty;
				                   PickupCompany      = string.Empty;
				                   PickupCountry      = string.Empty;
				                   PickupDefault      = false;
				                   PickupName         = string.Empty;
				                   PickupNotes        = string.Empty;
				                   PickupPhone        = string.Empty;
				                   PickupPostalZip    = string.Empty;
				                   PickupProvState    = string.Empty;
				                   PickupStreet       = string.Empty;
				                   PickupSuite        = string.Empty;
				                   PickupZone         = string.Empty;

				                   //Pieces = 1;
				                   POD = string.Empty;

				                   //QH = 0;

				                   //ReadyDate = DateTimeOffset.Now;
				                   //ReadyTime = DateTimeOffset.Now;
				                   ReadyDate = now;

				                   //ReadyTime = now;
				                   Reference         = string.Empty;
				                   ServiceLevel      = string.Empty;
				                   Status1           = -1;
				                   TDEL              = string.Empty;
				                   TextBoxesEditable = true;
				                   TPU               = string.Empty;
				                   TotalOriginal     = 0;
				                   TotalPieces       = 0;
				                   TotalWeight       = 0;
				                   TripId            = string.Empty;
				                   TripPackages.Clear();

				                   //TripPackages.Add(new TripPackage()); // Need 1 for the first row
				                   TripPackages.Add( new DisplayPackage( new TripPackage
				                                                         {
					                                                         Pieces = 1,
					                                                         Weight = 1
				                                                         } ) );
				                   UndeliverableNotes = string.Empty;

				                   //Weight = 1;

				                   // Inventory
				                   SelectedInventory            = null;
				                   SelectedInventoryBarcode     = string.Empty;
				                   SelectedInventoryName        = string.Empty;
				                   SelectedInventoryForTripItem = new List<ExtendedInventory>();
				                   TripItemsInventory           = new SortedDictionary<int, List<ExtendedInventory>>();
			                   } );
		}

		/// <summary>
		/// </summary>
		/// <param name="controls">Contains pairs of Controls and the property name (eg. tbTripId, TripId)</param>
		/// <returns>List of errors</returns>
		public List<string> Execute_SaveTrip( Dictionary<string, Control> controls )
		{
			var errors = ValidateTrip( controls );

			if( errors.Count == 0 )
			{
				Logging.WriteLogLine( "Trip is valid - saving" );
				IsNewTrip = false;
				var trip = BuildTrip();

				AddCustomerCompany pickup = null;

				//bool isPickupNew = false;
				AddCustomerCompany delivery = null;

				//bool isDeliveryNew = false;

				// Problem - every trip will result in a new Address
				// Solution - compare new address to old and only save if new

				// Check to see if the addresses exist - add if not
				// Note: this will mean that if an existing address is selected and then modified, 
				// the modified address will only exist in the trip; the addressbook is _not_ updated.
				if( !DoesAddressNameExist( SelectedPickupAddress ) )
				{
					var id = GenerateIdFromAddressName( SelectedPickupAddress );
					Logging.WriteLogLine( "Generated pickup id: " + id );

					var puCompany = new Company
					                {
						                AddressLine1  = PickupStreet.Trim(),
						                City          = PickupCity.Trim(),
						                CompanyName   = SelectedPickupAddress.Trim(),
						                CompanyNumber = id,
						                Country       = PickupCountry.Trim(),
						                Phone         = PickupPhone.Trim(),
						                PostalCode    = PickupPostalZip.Trim(),
						                Region        = PickupProvState.Trim(),
						                Suite         = PickupSuite.Trim()
					                };

					pickup = new AddCustomerCompany( "TripEntryModel" )
					         {
						         Company      = puCompany,
						         CustomerCode = Account
					         };
				}

				if( !DoesAddressNameExist( SelectedDeliveryAddress ) )
				{
					var id = GenerateIdFromAddressName( SelectedDeliveryAddress );
					Logging.WriteLogLine( "Generated delivery id: " + id );

					var delCompany = new Company
					                 {
						                 AddressLine1  = DeliveryStreet.Trim(),
						                 City          = DeliveryCity.Trim(),
						                 CompanyName   = SelectedDeliveryAddress.Trim(),
						                 CompanyNumber = id,
						                 Country       = DeliveryCountry.Trim(),
						                 Phone         = DeliveryPhone.Trim(),
						                 PostalCode    = DeliveryPostalZip.Trim(),
						                 Region        = DeliveryProvState.Trim(),
						                 Suite         = DeliverySuite.Trim()
					                 };

					delivery = new AddCustomerCompany( "TripEntryModel" )
					           {
						           Company      = delCompany,
						           CustomerCode = Account
					           };
				}

				Task.Run( async () =>
				          {
					          if( pickup != null )
						          await Azure.Client.RequestAddCustomerCompany( pickup );

					          if( delivery != null )
						          await Azure.Client.RequestAddCustomerCompany( delivery );

					          await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( trip ) );

					          Dispatcher.Invoke( () =>
					                             {
						                             // Update the top bar
						                             Status1 = (int)trip.Status1;

						                             MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
						                                              (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
						                                              MessageBoxButton.OK, MessageBoxImage.Asterisk );
					                             } );
				          } );
			}

			return errors;
		}

		public List<string> Execute_SaveTrip_orig2( Dictionary<string, Control> controls )
		{
			var errors = ValidateTrip( controls );

			if( errors.Count == 0 )
			{
				Logging.WriteLogLine( "Trip is valid - saving" );
				IsNewTrip = false;
				var trip = BuildTrip();

				AddCustomerCompany pickup = null;

				//bool isPickupNew = false;
				AddCustomerCompany delivery = null;

				//bool isDeliveryNew = false;

				// Problem - every trip will result in a new Address
				// Solution - compare new address to old and only save if new

				var id = GenerateIdFromAddressName( SelectedPickupAddress );
				Logging.WriteLogLine( "Generated pickup id: " + id );

				var puCompany = new Company
				                {
					                AddressLine1  = PickupStreet.Trim(),
					                City          = PickupCity.Trim(),
					                CompanyName   = SelectedPickupAddress.Trim(),
					                CompanyNumber = id,
					                Country       = PickupCountry.Trim(),
					                Phone         = PickupPhone.Trim(),
					                PostalCode    = PickupPostalZip.Trim(),
					                Region        = PickupProvState.Trim(),
					                Suite         = PickupSuite.Trim()
				                };

				pickup = new AddCustomerCompany( "TripEntryModel" )
				         {
					         Company      = puCompany,
					         CustomerCode = Account
				         };

				if( SelectedPickupAddress != SelectedDeliveryAddress )
				{
					id = GenerateIdFromAddressName( SelectedDeliveryAddress );
					Logging.WriteLogLine( "Generated delivery id: " + id );

					var delCompany = new Company
					                 {
						                 AddressLine1  = DeliveryStreet.Trim(),
						                 City          = DeliveryCity.Trim(),
						                 CompanyName   = SelectedDeliveryAddress.Trim(),
						                 CompanyNumber = id,
						                 Country       = DeliveryCountry.Trim(),
						                 Phone         = DeliveryPhone.Trim(),
						                 PostalCode    = DeliveryPostalZip.Trim(),
						                 Region        = DeliveryProvState.Trim(),
						                 Suite         = DeliverySuite.Trim()
					                 };

					delivery = new AddCustomerCompany( "TripEntryModel" )
					           {
						           Company      = delCompany,
						           CustomerCode = Account
					           };
				}
				else
					Logging.WriteLogLine( "PickupAddress and DeliveryAddress are the same" );

				// Check to see if the addresses have been modified
				if( DoesAddressNameExist( SelectedPickupAddress ) )
				{
					foreach( var ca in Companies )
					{
						if( ca.CompanyName == SelectedPickupAddress )
						{
							if( CompareCompanies( pickup.Company, ca ) )
								pickup = null;

							break;
						}
					}
				}

				if( DoesAddressNameExist( SelectedDeliveryAddress ) )
				{
					foreach( var ca in Companies )
					{
						if( ca.CompanyName == SelectedDeliveryAddress )
						{
							if( CompareCompanies( delivery.Company, ca ) )
								delivery = null;

							break;
						}
					}
				}

				Task.Run( async () =>
				          {
					          if( pickup != null )
						          await Azure.Client.RequestAddCustomerCompany( pickup );

					          if( delivery != null )
						          await Azure.Client.RequestAddCustomerCompany( delivery );

					          await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( trip ) );

					          Dispatcher.Invoke( () =>
					                             {
						                             // Update the top bar
						                             Status1 = (int)trip.Status1;

						                             MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
						                                              (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
						                                              MessageBoxButton.OK, MessageBoxImage.Asterisk );
					                             } );
				          } );
			}

			return errors;
		}


		private static bool CompareCompanies( Company newCompany, CompanyAddress origCompany )
		{
			var same = true;

			if( newCompany.AddressLine1 != origCompany.AddressLine1 )
				same = false;
			else if( same && ( newCompany.AddressLine2 != origCompany.AddressLine2 ) )
				same = false;
			else if( same && ( newCompany.City != origCompany.City ) )
				same = false;
			else if( same && ( newCompany.CompanyNumber != origCompany.CompanyNumber ) )
				same = false;
			else if( same && ( newCompany.Country != origCompany.Country ) )
				same = false;
			else if( same && ( newCompany.EmailAddress != origCompany.EmailAddress ) )
				same = false;
			else if( same && ( newCompany.Notes != origCompany.Notes ) )
				same = false;
			else if( same && ( newCompany.Phone != origCompany.Phone ) )
				same = false;
			else if( same && ( newCompany.PostalCode != origCompany.PostalCode ) )
				same = false;
			else if( same && ( newCompany.Region != origCompany.Region ) )
				same = false;
			else if( same && ( newCompany.Suite != origCompany.Suite ) )
				same = false;

			//else if (same && newCompany.UserName != origCompany.UserName)
			//{
			//    same = false;
			//}
			if( !same )
				Logging.WriteLogLine( "Entered company " + newCompany.CompanyName + " is different to the original" );

			return same;
		}

		public List<string> Execute_SaveTrip_orig( Dictionary<string, Control> controls )
		{
			var errors = ValidateTrip( controls );

			if( errors.Count == 0 )
			{
				Logging.WriteLogLine( "Trip is valid - saving" );
				IsNewTrip = false;
				var trip = BuildTrip();

				AddCustomerCompany pickup   = null;
				AddCustomerCompany delivery = null;

				if( !DoesAddressNameExist( SelectedPickupAddress ) )
				{
					var id = GenerateIdFromAddressName( SelectedPickupAddress );
					Logging.WriteLogLine( "Generated id: " + id );

					var company = new Company
					              {
						              AddressLine1  = PickupStreet,
						              City          = PickupCity,
						              CompanyName   = SelectedPickupAddress,
						              CompanyNumber = id,
						              Country       = PickupCountry,
						              Phone         = PickupPhone,
						              PostalCode    = PickupPostalZip,
						              Region        = PickupProvState,
						              Suite         = PickupSuite
					              };

					pickup = new AddCustomerCompany( "TripEntryModel" )
					         {
						         Company      = company,
						         CustomerCode = Account
					         };
				}

				if( !DoesAddressNameExist( SelectedDeliveryAddress ) )
				{
					if( SelectedPickupAddress != SelectedDeliveryAddress )
					{
						var id = GenerateIdFromAddressName( SelectedDeliveryAddress );

						var company = new Company
						              {
							              AddressLine1  = DeliveryStreet,
							              City          = DeliveryCity,
							              CompanyName   = SelectedDeliveryAddress,
							              CompanyNumber = id,
							              Country       = DeliveryCountry,
							              Phone         = DeliveryPhone,
							              PostalCode    = DeliveryPostalZip,
							              Region        = DeliveryProvState,
							              Suite         = DeliverySuite
						              };

						delivery = new AddCustomerCompany( "TripEntryModel" )
						           {
							           Company      = company,
							           CustomerCode = Account
						           };
					}
					else
						Logging.WriteLogLine( "PickupAddress and DeliveryAddress are the same" );
				}

				Task.Run( async () =>
				          {
					          if( pickup != null )
						          await Azure.Client.RequestAddCustomerCompany( pickup );

					          if( delivery != null )
						          await Azure.Client.RequestAddCustomerCompany( delivery );

					          await Azure.Client.RequestAddUpdateTrip( UpdateBroadcast( trip ) );

					          Dispatcher.Invoke( () =>
					                             {
						                             MessageBox.Show( (string)Application.Current.TryFindResource( "TripEntryTripSaved" ),
						                                              (string)Application.Current.TryFindResource( "TripEntryTripSavedTitle" ),
						                                              MessageBoxButton.OK, MessageBoxImage.Asterisk );
					                             } );
				          } );
			}

			return errors;
		}

		private Trip UpdateBroadcast( Trip t )
		{
			switch( t.Status1 )
			{
			case STATUS.ACTIVE:
				t.BroadcastToDispatchBoard = true;

				break;

			case STATUS.DISPATCHED:
				t.BroadcastToDriverBoard = true;
				t.BroadcastToDriver      = true;

				break;
			}

			return t;
		}


		private bool DoesAddressNameExist( string address )
		{
			var exists = false;

			foreach( var name in AddressNames )
			{
				if( name.ToLower() == address.Trim().ToLower() )
				{
					exists = true;

					break;
				}
			}

			return exists;
		}

		private bool DoesAddressIdExist( string id )
		{
			var exists = false;

			foreach( var comp in Companies )
			{
				if( comp.CompanyNumber == id )
				{
					exists = true;

					break;
				}
			}

			return exists;
		}

		/// <summary>
		///     Tries to create an id from the letters and digits in the name.
		///     If it exists, it tries to find a unique one by appending a digit.
		///     If, after 100000 attempts, one hasn't been found, it appends
		///     the current time (in ticks) to the name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		private string GenerateIdFromAddressName( string name )
		{
			var id = name;

			var attempt = 1;

			while( true )
			{
				id = new string( id.ToCharArray()
				                   .Where( c => char.IsLetterOrDigit( c ) )
				                   .ToArray() );

				if( !DoesAddressIdExist( id ) )
					break;

				if( attempt < 100000 )
				{
					id = name + attempt; // Append a digit
					++attempt;
				}
				else
				{
					// Give up - append the timestamp
					id = new string( name.ToCharArray()
					                     .Where( c => char.IsLetterOrDigit( c ) )
					                     .ToArray() );
					id += "_" + DateTime.Now.Ticks;

					break;
				}
			}

			return id;
		}

		public void Execute_ProduceWaybill( string printerName, Window parent )
		{
			Logging.WriteLogLine( "Produce waybill" );

			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "TripId: " + CurrentTrip.TripId );
				Trip trip = CurrentTrip;

// TODO The parameters need to be set properly
				var resellerId = Globals.DataContext.MainDataContext.Account;
				var phone      = "123-456-7890"; // TODO Where is this stored?

				Dispatcher.Invoke( () =>
				                   {
					                   var weighbillDialog = new WeighbillDialog( printerName, trip, false, string.Empty, false, resellerId, phone )
					                                         {
						                                         Owner = parent
					                                         };
					                   weighbillDialog.ShowDialog();
				                   } );
			}
			else
				Logging.WriteLogLine( "No trip selected" );
		}

		public void Execute_ProduceTracking( Window parent )
		{
			Logging.WriteLogLine( "Produce tracking" );

			if( CurrentTrip != null )
			{
				Trip trip = CurrentTrip;

				var trackingDialog = new TrackingDialog( trip )
				                     {
					                     Owner = parent
				                     };
				trackingDialog.ShowDialog();
			}
			else
				Logging.WriteLogLine( "No trip selected" );
		}

		public void Execute_ProduceAudit( Window parent )
		{
			Logging.WriteLogLine( "Produce audit" );

			if( CurrentTrip != null )
			{
				//Trip trip = this.CurrentTrip;
				////var lines = GetAuditTripLines();
				//PrintAuditDialogModel model = new PrintAuditDialogModel(trip);
				//var lines = model.Lines;

				Dispatcher.Invoke( () =>
				                   {
					                   GetAuditLines();

					                   var dialog = new PrintAuditDialog( CurrentTrip, AuditLines )
					                                {
						                                Owner = parent
					                                };
					                   dialog.ShowDialog();
				                   } );
			}
			else
				Logging.WriteLogLine( "No trip selected" );
		}

//private ObservableCollection<string> GetAuditTripLines()
//{
//    var NewItems = new ObservableCollection<string>();
//    if (CurrentTrip != null)
//    {
//        Utils.Logging.WriteLogLine("Loading log for " + CurrentTrip.TripId);
//        if (!IsInDesignMode)
//        {
//            //MainWindow.MainWindowModel.
//            var Lookup = new LogLookup
//            {
//                Log = Log,
//                FromDate = DateTimeOffset.MinValue,
//                ToDate = DateTimeOffset.Now,
//                User = User
//            };

//            Task.Run(async () =>
//                     {
//                         try
//                         {
//                             var Lg = await Azure.Client.RequestGetLog(RequestOverride(Lookup));
//                             foreach (var Entry in Lg)
//                             {
//                                 //Entry.Data = FormatData(Entry.Data);
//                                 //NewItems.Add(new DisplayEntry(Entry));
//                             }

//                             Dispatcher.Invoke(() =>
//                                                {
//                                                    Lines = NewItems;
//                                                });
//                         }
//                         catch (Exception Exception)
//                         {
//                             Console.WriteLine(Exception);
//                         }
//                     });
//        }
//    }

//    return NewItems;
//}
		public List<AuditLine> AuditLines
		{
			get { return Get( () => AuditLines, GetAuditLines() ); }
			set { Set( () => AuditLines, value ); }
		}


		public Func<string, string> FormatData = data => data;

		private List<AuditLine> GetAuditLines()
		{
			var NewItems = new List<AuditLine>();

			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "Loading log for " + CurrentTrip.TripId );

				if( !IsInDesignMode )
				{
					// From LogViewerModel
					var Lookup = new LogLookup
					             {
						             Log      = Log,
						             FromDate = DateTime.Now,
						             ToDate   = DateTime.Now,
						             Key      = CurrentTrip.TripId
					             };

					Task.Run( async () =>
					          {
						          try
						          {
							          var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );

							          //var LogItems = new ObservableCollection<String>();

							          foreach( var Entry in Lg )
							          {
								          Entry.Data = FormatData( Entry.Data );
								          var L = Log;

								          //LogItems.Add(new DisplayEntry(L, Entry));
								          var partitionKey = Entry.PartitionKey;
								          var operation    = Entry.Operation;
								          var data         = Entry.Data;

								          var line = new AuditLine( Entry.Timestamp, partitionKey, operation, data );

								          // LogItems.Add(L.ToString());
								          NewItems.Add( line );
							          }

							          Dispatcher.Invoke( () => { AuditLines = NewItems; } );

							          //Dispatcher.Invoke(() =>
							          //               {
							          //                   //NothingFound = LogItems.Count == 0;

							          //                   //Items = LogItems;
							          //                   //SearchButtonEnabled = true;
							          //                   //NewItems = LogItems;
							          //               });
						          }
						          catch( Exception Exception )
						          {
							          Console.WriteLine( Exception );
						          }
					          } );

					// Original
					//  var Lookup = new LogLookup
					//{
					// Log = Log,

					// // FromDate = DateTimeOffset.Now.AddDays( -1 ), //DateTimeOffset.MinValue,
					// FromDate = DateTimeOffset.Parse( CurrentTrip.CallTime ),
					// ToDate = DateTimeOffset.Now,
					// Key = User
					//};

					//Task.Run( async () =>
					//          {
					//	          try
					//	          {
					//		          var Lg = await Azure.Client.RequestGetLog( RequestOverride( Lookup ) );
					//		          Logging.WriteLogLine( "Found " + Lg.Count + " Entries" );

					//		          if( Lg.Count > 0 )
					//		          {
					//			          foreach( var Entry in Lg )
					//			          {
					//				          if( !string.IsNullOrEmpty( Entry.Data ) )
					//				          {
					//					          // Problem - Nav is getting this message
					//					          // 2019-07-11 14:17:41.1753; Found 9 Entries; Method: GetAuditLines; File: TripEntryModel.cs; Line: 1891
					//					          // Unhandled exception : Not enough quota is available to process this command
					//					          //var line = new AuditLine( Entry.Timestamp, Entry.PartitionKey, Entry.Operation, Entry.Data );

					//					          string partitionKey,
					//					                 operation,
					//					                 data = string.Empty;

					//					          if( Entry.PartitionKey.Length > 1024 )
					//					          {
					//						          partitionKey = Entry.PartitionKey.Substring( 0, 1024 );
					//						          Logging.WriteLogLine( "Entry.PartitionKey.Length: " + Entry.PartitionKey.Length + " - truncating to 1024" );
					//					          }
					//					          else
					//					          {
					//						          partitionKey = Entry.PartitionKey;
					//					          }

					//					          if( Entry.Operation.Length > 1024 )
					//					          {
					//						          operation = Entry.Operation.Substring( 0, 1024 );
					//						          Logging.WriteLogLine( "Entry.Operation.Length: " + Entry.Operation.Length + " - truncating to 1024" );
					//					          }
					//					          else
					//					          {
					//						          operation = Entry.Operation;
					//					          }

					//					          if( Entry.Data.Length > 1024 )
					//					          {
					//						          data = Entry.Data.Substring( 0, 1024 );
					//						          Logging.WriteLogLine( "Entry.Data.Length: " + Entry.Data.Length + " - truncating to 1024" );
					//					          }
					//					          else
					//					          {
					//						          data = Entry.Data;
					//					          }

					//					          var line = new AuditLine( Entry.Timestamp, partitionKey, operation, data );
					//					          NewItems.Add( line );
					//				          }
					//			          }

					//			          Dispatcher.Invoke( () =>
					//			                             {
					//				                             AuditLines = NewItems;
					//			                             } );
					//		          }
					//	          }
					//	          catch( Exception Exception )
					//	          {
					//		          Logging.WriteLogLine( "Exception: " + Exception );
					//	          }
					//          } );
				}
			}

			return NewItems;
		}

//public ObservableCollection<string> Lines
//{
//    get { return Get(() => Lines, GetAuditTripLines()); }
//    set { Set(() => Lines, value); }
//}
		public Func<LogLookup, LogLookup> RequestOverride = lookup => lookup;

		/// <summary>
		///     Design:
		///     TripEntry.xaml.cs builds a dictionary of binding names and controls.
		///     Only controls with active bindings (TextBoxes) or the Tag set (ComboBoxes) are included.
		///     This method is responsible for deciding which controls are checked.
		///     Widgets.SetErrorState handles the marking or clearing of the error in the widget.
		/// </summary>
		/// <param name="controls">
		///     Contains pairs of property names (from the control's binding) and Controls (eg. TripId,
		///     tbTripId)
		/// </param>
		/// <returns></returns>
		private List<string> ValidateTrip( Dictionary<string, Control> controls )
		{
			var errors = new List<string>();

			if( controls.Count > 0 )
			{
				foreach( var fieldName in controls.Keys )
				{
					var error = string.Empty;

					switch( fieldName )
					{
					case "TripId":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsTripId" ) );

						break;

					case "CompanyNames":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCompanyNames" ) );

						break;

					case "Customer":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCustomer" ) );

						break;

					case "CustomerPhone":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCustomerPhone" ) );

						break;

					case "CallName":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallerName" ) );

						break;

					case "CallPhone":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallerPhone" ) );

						break;

					case "CallDate":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallDate" ) );

						break;

					case "CallTime":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsCallTime" ) );

						break;

					case "PickupCompany":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCompany" ) );

						break;

					case "PickupStreet":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupStreet" ) );

						break;

					case "PickupCity":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCity" ) );

						break;

					case "PickupProvState":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupProvState" ) );

						break;

					case "PickupCountry":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupCountry" ) );

						break;

					case "PickupPostalZip":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPickupPostalZip" ) );

						break;

					case "DeliveryCompany":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCompany" ) );

						break;

					case "DeliveryStreet":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryStreet" ) );

						break;

					case "DeliveryCity":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCity" ) );

						break;

					case "DeliveryProvState":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryProvState" ) );

						break;

					case "DeliveryCountry":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryCountry" ) );

						break;

					case "DeliveryPostalZip":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDeliveryPostalZip" ) );

						break;

					case "PackageTypes":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackagePackageType" ) );

						break;

					case "Weight":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackageWeight" ) );

						break;

					case "Pieces":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsPackagePieces" ) );

						break;

					case "ServiceLevels":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsServiceLevel" ) );

						break;

					case "ReadyDate":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsReadyDate" ) );

						break;

					case "ReadyTime":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsReadyTime" ) );

						break;

					case "DueDate":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsDueDate" ) );

						break;

					case "DueTime":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "TripEntryValidationErrorsDetailsDueTime" ) );

						break;
					}

					if( !string.IsNullOrEmpty( error ) )
						errors.Add( error );
				}
			}

			return errors;
		}

		/// <summary>
		///     Creates the trip object.
		/// </summary>
		/// <returns></returns>
		private Trip BuildTrip()
		{
			var trip = new Trip();

			if( CurrentTrip != null )
				trip = CurrentTrip;

			trip.Program                  = PROGRAM;
			trip.BroadcastToDispatchBoard = true;
			trip.TripId                   = TripId.Trim();
			trip.AccountId                = Account.Trim();

			trip.BillingCompanyName  = Customer.Trim();
			trip.BillingAddressPhone = CustomerPhone;
			trip.BillingAddressNotes = CustomerNotes;

			trip.CallerName  = CallName.Trim();
			trip.CallerPhone = CallPhone.Trim();
			trip.CallerEmail = CallEmail.Trim();

			//TimeSpan ts = new TimeSpan(this.CallTime.Hour, this.CallTime.Minute, this.CallTime.Second);            
			//trip.CallTime = this.CallDate + ts;
			//trip.CallTime = this.CallDate.Add(new TimeSpan(this.CallTime.Hour, this.CallTime.Minute, this.CallTime.Second));
			trip.CallTime = new DateTime( CallDate.Year, CallDate.Month, CallDate.Day, CallDate.Hour, CallDate.Minute, CallDate.Second );

			//Logging.WriteLogLine( "trip.CallTime: " + trip.CallTime );

			trip.IsQuote            = IsQuote;
			trip.UndeliverableNotes = UndeliverableNotes.Trim();

			if( !string.IsNullOrEmpty( SelectedPickupAddress ) )
				trip.PickupCompanyName = SelectedPickupAddress.Trim();
			else
				trip.PickupCompanyName = PickupName.Trim();

			trip.PickupAddressBarcode = PickupLocation.Trim();

			trip.PickupContact             = PickupName.Trim();
			trip.PickupAddressSuite        = PickupSuite.Trim();
			trip.PickupAddressAddressLine1 = PickupStreet.Trim();
			trip.PickupAddressCity         = PickupCity.Trim();
			trip.PickupAddressRegion       = PickupProvState.Trim();
			trip.PickupAddressCountry      = PickupCountry.Trim();
			trip.PickupAddressPostalCode   = PickupPostalZip.Trim();
			trip.PickupZone                = PickupZone.Trim();

			trip.PickupAddressNotes = PickupAddressNotes.Trim();

			trip.PickupNotes = PickupNotes.Trim();

			if( !string.IsNullOrEmpty( SelectedDeliveryAddress ) )
				trip.DeliveryCompanyName = SelectedDeliveryAddress.Trim();
			else
				trip.DeliveryCompanyName = DeliveryName.Trim();

			trip.DeliveryAddressBarcode = DeliveryAddressNotes.Trim();

			trip.DeliveryContact             = DeliveryName.Trim();
			trip.DeliveryAddressSuite        = DeliverySuite.Trim();
			trip.DeliveryAddressAddressLine1 = DeliveryStreet.Trim();
			trip.DeliveryAddressCity         = DeliveryCity.Trim();
			trip.DeliveryAddressRegion       = DeliveryProvState.Trim();
			trip.DeliveryAddressCountry      = DeliveryCountry.Trim();
			trip.DeliveryAddressPostalCode   = DeliveryPostalZip.Trim();
			trip.DeliveryZone                = DeliveryZone.Trim();

			trip.DeliveryAddressNotes = DeliveryAddressNotes.Trim();

			trip.DeliveryNotes = DeliveryNotes.Trim();

			//trip.PackageType = SelectedPackageType;
			if( TripPackages.IsNotNull() && ( TripPackages.Count() > 0 ) )
				trip.PackageType = TripPackages[ 0 ].PackageType;

			//trip.Weight = Weight;
			//trip.Pieces = Pieces;
			trip.ServiceLevel = ServiceLevel.Trim();
			trip.POP          = POP.Trim();
			trip.POD          = POD.Trim();

			//ts = new TimeSpan(this.ReadyTime.Hour, this.ReadyTime.Minute, this.ReadyTime.Second);
			//trip.ReadyTime = this.ReadyDate + ts;
			//trip.ReadyTime = this.ReadyDate.Add(new TimeSpan(this.ReadyTime.Hour, this.ReadyTime.Minute, this.ReadyTime.Second));
			trip.ReadyTime = new DateTime( ReadyDate.Year, ReadyDate.Month, ReadyDate.Day, ReadyDate.Hour, ReadyDate.Minute, ReadyDate.Second );

			//Logging.WriteLogLine( "trip.ReadyTime: " + trip.ReadyTime );

			//ts = new TimeSpan(this.DueTime.Hour, this.DueTime.Minute, this.DueTime.Second);
			//trip.DueTime = this.ReadyTime + ts;
			//trip.DueTime = this.DueDate.Add(new TimeSpan(this.DueTime.Hour, this.DueTime.Minute, this.DueTime.Second));
			trip.DueTime = new DateTime( DueDate.Year, DueDate.Month, DueDate.Day, DueDate.Hour, DueDate.Minute, DueDate.Second );

			//Logging.WriteLogLine( "trip.DueTime: " + trip.DueTime );

			if( !string.IsNullOrEmpty( SelectedDriver ) )
			{
				trip.Driver = SelectedDriver;

				if( CurrentTrip == null )
					trip.Status1 = STATUS.DISPATCHED;
				else
				{
					if( CurrentTrip.Status1 > STATUS.ACTIVE )
						trip.Status1 = CurrentTrip.Status1;
					else
					{
						Logging.WriteLogLine( "Trip has driver assigned but status is " + trip.Status1 + "  - changing to Dispatched" );
						trip.Status1 = STATUS.DISPATCHED;
					}
				}
			}
			else
			{
				if( ( CurrentTrip == null ) || ( CurrentTrip.Status1 < STATUS.ACTIVE ) )
					trip.Status1 = STATUS.ACTIVE;
			}

			if( TripPackages.Count > 0 )
			{
				// Only include TripItems with pieces and weight
				var valid = new List<TripPackage>();

				/*
				 * From Terry:
				 * Pieces, weight fields in trip hold the totals of the pieces
				    in the Packages.
				    There is a Package for each row.
				    The row's Pieces is the total number of inventory items, or the entered
				    value if there are no items.
				    Therefore the row's pieces can't be edited if there are items attached.
				 */

				//foreach( var tp in TripPackages )
				//for (int i = 1; i < TripPackages.Count; i++)
				for( var i = 0; i < TripPackages.Count; i++ )
				{
					var tp = TripPackages[ i ];

					if( ( tp.PackageType != string.Empty ) && ( tp.Pieces > 0 ) && ( tp.Weight > 0 ) )
					{
						//if( ( ti.ItemCode != string.Empty ) && ( ti.Pieces > 0 ) && ( ti.Weight > 0 ) )
						//	valid.Add( ti );

						if( TripItemsInventory.ContainsKey( i + 1 ) )
						{
							tp.Items.Clear();
							var items = TripItemsInventory[ i + 1 ];

							foreach( var ei in items )
							{
								var ti = new TripItem
								         {
									         Description = ei.Inventory.Description,
									         Barcode     = ei.Inventory.Barcode,
									         Pieces      = ei.Quantity,
									         ItemCode    = ei.ServiceLevel
								         };

								tp.Items.Add( ti );
							}
						}

						if( ( tp.Items != null ) && ( tp.Items.Count > 0 ) )
							valid.Add( tp );
					}
				}

				if( valid.Count > 0 )
					trip.Packages = valid;

				// These fields are totals, according to Terry
				trip.Weight             = TotalWeight;
				trip.Pieces             = TotalPieces;
				trip.OriginalPieceCount = TotalOriginal;
			}

			return trip;
		}

		/// <summary>
		/// </summary>
		/// <param name="ctrl"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		private string SetErrorState( Control ctrl, string message )
		{
			var error = string.Empty;

			if( IsControlEmpty( ctrl ) )
			{
				ctrl.Background = Brushes.Yellow;
				error           = message;
			}
			else
				ctrl.Background = Brushes.White;

			return error;
		}

		/// <summary>
		/// </summary>
		/// <param name="ctrl"></param>
		/// <returns></returns>
		private bool IsControlEmpty( Control ctrl )
		{
			var isEmpty = true;

			if( ctrl is TextBox tb )
				isEmpty = IsTextBoxEmpty( tb );
			else if( ctrl is ComboBox cb )
				isEmpty = IsComboBoxSelected( cb );
			else if( ctrl is DateTimePicker dtp )
				isEmpty = IsDateTimePickerEmpty( dtp );
			else if( ctrl is TimePicker tp )
				isEmpty = IsTimePickerEmpty( tp );

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="tb"></param>
		/// <returns></returns>
		private bool IsTextBoxEmpty( TextBox tb )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( tb.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="cb"></param>
		/// <returns></returns>
		private bool IsComboBoxSelected( ComboBox cb )
		{
			var isEmpty = true;

			if( cb.SelectedIndex > -1 )
				isEmpty = false;

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="dtp"></param>
		/// <returns></returns>
		private bool IsDateTimePickerEmpty( DateTimePicker dtp )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( dtp.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		private bool IsTimePickerEmpty( TimePicker tp )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( tp.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		public CompanyAddress GetCompanyAddress( string accountId )
		{
			CompanyAddress ca = null;

			//Task.WaitAll(Task.Run(async () =>
			Task.Run( async () =>
			          {
				          try
				          {
					          ca = await Azure.Client.RequestGetResellerCustomerCompany( accountId );

					          Dispatcher.Invoke( () =>
					                             {
						                             Customer = ca.CompanyName;

						                             //               if (ca.Phone.IsNotNullOrWhiteSpace())
						                             //               {
						                             //CustomerPhone = ca.Phone;
						                             //               }
						                             //PickupCountry = ca.Country;
						                             //DeliveryCountry = ca.Country;
						                             CompanyAddress = ca;
					                             } );
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown looking up Customer for AccountId: " + accountId + " e->" + e );
				          }
			          } );

			//}));

			return ca;
		}

		//public DisplayTrip Execute_FindTrip()
		//{
		//	DisplayTrip trip = null;
		//	Logging.WriteLogLine( "Finding trip: " + TripId );
		//	if( !string.IsNullOrEmpty( TripId ) )
		//	{
		//		Task.Run( () =>
		//		          {
		//			          try
		//			          {
		//				          var Trps = Azure.Client.RequestSearchTrips( new Protocol.Data.SearchTrips
		//				                                                      {
		//					                                                      TripId = TripId,
		//					                                                      ByTripId = true
		//				                                                      } ).Result;
		//				          Dispatcher.Invoke( () =>
		//				                             {
		//					                             if( Trps.Count > 0 )
		//					                             {
		//						                             trip = new DisplayTrip( Trps[ 0 ] );
		//						                             CurrentTrip = trip;
		//					                             }
		//				                             } );
		//			          }
		//			          catch
		//			          {
		//				          MessageBox.Show( "Error fetching trip", "Error", MessageBoxButton.OK );
		//			          }
		//		          } );
		//	}

		//	return trip;
		//}

		public void Execute_SwapAddresses()
		{
			Logging.WriteLogLine( "Swapping addresses for trip: " + TripId );

			if( !string.IsNullOrEmpty( TripId ) )
			{
				Dispatcher.Invoke( () =>
				                   {
					                   var tmp = string.Empty;

// Putting this at the top, so that the subsequent field copies aren't overwritten
					                   tmp                     = SelectedDeliveryAddress;
					                   SelectedDeliveryAddress = SelectedPickupAddress;
					                   SelectedPickupAddress   = tmp;
					                   tmp                     = DeliveryCompany;
					                   DeliveryCompany         = PickupCompany;
					                   PickupCompany           = tmp;
					                   tmp                     = DeliveryName;
					                   DeliveryName            = PickupName;
					                   PickupName              = tmp;
					                   tmp                     = DeliveryPhone;
					                   DeliveryPhone           = PickupPhone;
					                   PickupPhone             = tmp;
					                   tmp                     = DeliverySuite;
					                   DeliverySuite           = PickupSuite;
					                   PickupSuite             = tmp;
					                   tmp                     = DeliveryStreet;
					                   DeliveryStreet          = PickupStreet;
					                   PickupStreet            = tmp;
					                   tmp                     = DeliveryCity;
					                   DeliveryCity            = PickupCity;
					                   PickupCity              = tmp;
					                   tmp                     = DeliveryProvState;
					                   DeliveryProvState       = PickupProvState;
					                   PickupProvState         = tmp;
					                   tmp                     = DeliveryPostalZip;
					                   DeliveryPostalZip       = PickupPostalZip;
					                   PickupPostalZip         = tmp;
					                   tmp                     = DeliveryZone;
					                   DeliveryZone            = PickupZone;
					                   PickupZone              = tmp;
					                   tmp                     = DeliveryCountry;
					                   DeliveryCountry         = PickupCountry;
					                   PickupCountry           = tmp;
					                   tmp                     = DeliveryAddressNotes;
					                   DeliveryAddressNotes    = PickupAddressNotes;
					                   PickupAddressNotes      = tmp;
					                   tmp                     = DeliveryAddressNotes;
					                   DeliveryAddressNotes    = PickupAddressNotes;
					                   PickupAddressNotes      = tmp;
					                   tmp                     = DeliveryNotes;
					                   DeliveryNotes           = PickupNotes;
					                   PickupNotes             = tmp;
				                   } );
			}
		}

		public void Execute_CloneTrip()
		{
			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "Cloning trip: " + CurrentTrip.TripId );

				Task.Run( async () =>
				          {
					          try
					          {
						          var newTripId = await Azure.Client.RequestGetNextTripId();
						          Logging.WriteLogLine( "Got new tripid: " + newTripId );
						          TripId = newTripId;
						          var newTrip = CopyTripIntoAnother( CurrentTrip );

//Execute_NewTrip(true);
						          newTrip.TripId = TripId;
						          CurrentTrip    = newTrip;

//WhenCurrentTripChanges();
						          //var now = DateTimeOffset.Now;
						          var now = DateTime.Now;
						          CallDate = now;

						          //CallTime = now;

						          Status1            = (int)STATUS.NEW;
						          CurrentTrip.Status = DisplayTrip.STATUS.NEW;

//if (!onlyTripId)
//{
//    DateTimeOffset now = DateTimeOffset.Now;
//    this.CallDate = now;
//    this.CallTime = now;

//    this.ReadyDate = now;
//    this.ReadyTime = now;

//    this.DueDate = now;
//    this.DueTime = now;
//}
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + e );
					          }
				          } );
			}
			else
				Logging.WriteLogLine( "No trip loaded" );
		}

		public void Execute_CloneTripForPickup()
		{
			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "Cloning trip for pickup: " + CurrentTrip.TripId );

				Task.Run( async () =>
				          {
					          try
					          {
						          var newTripId = await Azure.Client.RequestGetNextTripId();
						          Logging.WriteLogLine( "Got new tripid: " + newTripId );
						          TripId = newTripId;
						          var newTrip = CopyTripIntoAnother( CurrentTrip );
						          newTrip.TripId = TripId;

						          newTrip.DeliveryAddressAddressLine1 = string.Empty;
						          newTrip.DeliveryAddressAddressLine2 = string.Empty;
						          newTrip.DeliveryAddressCity         = string.Empty;
						          newTrip.DeliveryAddressCountry      = string.Empty;
						          newTrip.DeliveryAddressCountryCode  = string.Empty;
						          newTrip.DeliveryAddressNotes        = string.Empty;
						          newTrip.DeliveryAddressPostalCode   = string.Empty;
						          newTrip.DeliveryAddressRegion       = string.Empty;
						          newTrip.DeliveryAddressSuite        = string.Empty;
						          newTrip.DeliveryCompanyName         = string.Empty;
						          newTrip.DeliveryZone                = string.Empty;
						          SelectedDeliveryAddress             = string.Empty;

//Execute_NewTrip(true);
						          CurrentTrip = newTrip;

						          //var now = DateTimeOffset.Now;
						          var now = DateTime.Now;
						          CallDate = now;

						          //CallTime = now;
						          Status1            = (int)STATUS.NEW;
						          CurrentTrip.Status = DisplayTrip.STATUS.NEW;
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + e );
					          }
				          } );
			}
			else
				Logging.WriteLogLine( "No trip loaded" );
		}

		public void Execute_CloneTripForReturn()
		{
			if( CurrentTrip != null )
			{
				Logging.WriteLogLine( "Cloning trip for return: " + CurrentTrip.TripId );

				Task.Run( async () =>
				          {
					          try
					          {
						          var newTripId = await Azure.Client.RequestGetNextTripId();
						          Logging.WriteLogLine( "Got new tripid: " + newTripId );
						          TripId = newTripId;
						          var newTrip = CopyTripIntoAnother( CurrentTrip );
						          newTrip.TripId = TripId;

//Execute_NewTrip(true);
						          CurrentTrip = newTrip;

						          //var now = DateTimeOffset.Now;
						          var now = DateTime.Now;
						          CallDate = now;

						          //CallTime = now;
						          Execute_SwapAddresses();

						          Status1            = (int)STATUS.NEW;
						          CurrentTrip.Status = DisplayTrip.STATUS.NEW;
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "ERROR - exception thrown fetching new tripid: " + e );
					          }
				          } );
			}
			else
				Logging.WriteLogLine( "No trip loaded" );
		}

		/// <summary>
		///     Note: it drops the TripId and Driver.
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
		private DisplayTrip CopyTripIntoAnother( DisplayTrip src )
		{
			DisplayTrip result = null;

			if( src != null )
			{
				//result = new DisplayTrip( new Trip() );
				result         = new DisplayTrip();
				result.Status1 = src.Status1;

				result.Driver = src.Driver;

				//result.TPU = src.PickupTime;
				//result.TDEL = src.DeliveryTime;

				//result.TripId = src.TripId;
				result.AccountId = src.AccountId;
				result.AccountId = Account;
				result.AccountId = src.AccountId;
				result.Reference = src.Reference;

				result.CallerName  = src.CallerName;
				result.CallerPhone = src.CallerPhone;
				result.CallerEmail = src.CallerEmail;

				result.IsQuote            = src.IsQuote;
				result.UndeliverableNotes = src.UndeliverableNotes;

				// Pickup
				result.PickupCompanyName = src.PickupCompanyName;

//GetAddresses();  // Only need to do once
//SelectedPickupAddress = src.PickupCompanyName;

//PickupName = string.Empty; // TODO Fix this
				result.PickupAddressPhone = src.PickupAddressPhone;
				result.PickupAddressSuite = src.PickupAddressSuite;

//PickupStreet = (src.PickupAddressAddressLine1 + " " + src.PickupAddressAddressLine2).Trim();
				result.PickupAddressAddressLine1 = src.PickupAddressAddressLine1; // TODO Fix PickupAddressAddressLine2 returns PickupAddressAddressLine1
				result.PickupAddressCity         = src.PickupAddressCity;
				result.PickupAddressRegion       = src.PickupAddressRegion;
				result.PickupAddressCountry      = src.PickupAddressCountry;
				result.PickupAddressPostalCode   = src.PickupAddressPostalCode;

//PickupZone = string.Empty; // TODO Fix this
				result.PickupAddressNotes = src.PickupAddressNotes;

//PickupNotes = string.Empty; // TODO Fix this

// Delivery
				result.DeliveryCompanyName = src.DeliveryCompanyName;

//SelectedDeliveryAddress = src.DeliveryCompanyName;
//DeliveryName = string.Empty; // TODO Fix this
				result.DeliveryAddressPhone = src.DeliveryAddressPhone;
				result.DeliveryAddressSuite = src.DeliveryAddressSuite;

//DeliveryStreet = (src.DeliveryAddressAddressLine1 + " " + src.DeliveryAddressAddressLine2).Trim();
				result.DeliveryAddressAddressLine1 = src.DeliveryAddressAddressLine1; // TODO Fix DeliveryAddressAddressLine2 returns DeliveryAddressAddressLine1
				result.DeliveryAddressCity         = src.DeliveryAddressCity;
				result.DeliveryAddressRegion       = src.DeliveryAddressRegion;
				result.DeliveryAddressCountry      = src.DeliveryAddressCountry;
				result.DeliveryAddressPostalCode   = src.DeliveryAddressPostalCode;

				DeliveryZone                = src.DeliveryZone;
				result.DeliveryAddressNotes = src.DeliveryAddressNotes;

				DeliveryNotes = src.DeliveryNotes;

				// Package
				result.PackageType = src.PackageType;
				result.Weight      = src.Weight;
				result.Pieces      = (int)src.Pieces;

//Length = 0;   // TODO Fix this
//Width = 0;    // TODO Fix this
//Height = 0;   // TODO Fix this
				result.OriginalPieceCount = (int)src.OriginalPieceCount;

//QH = 0;       // TODO Fix this

// Service
				result.ServiceLevel = src.ServiceLevel;

//result.ReadyDate = src.ReadyTime;
				result.ReadyTime = src.ReadyTime;

//result.DueDate = src.DueTime;
				result.DueTime = src.DueTime;

				//result.POP = src.POP;
				//result.POD = src.POD;

				//SelectedPackageType = src.PackageType;

				//SelectedDriver = src.Driver;

				result.Packages = src.Packages;
			}

			return result;
		}

	#endregion

	#region Inventory

		public ObservableCollection<PmlInventory> Inventory
		{
			get { return Get( () => Inventory, new ObservableCollection<PmlInventory>() ); }
			set { Set( () => Inventory, value ); }
		}


		public ObservableCollection<string> InventoryNames
		{
			get { return Get( () => InventoryNames, new ObservableCollection<string>() ); }
			set { Set( () => InventoryNames, value ); }
		}

		private List<string> FullListOfInventoryNames = new List<string>();


		public ICollection<PmlInventory> SelectedInventory
		{
			get { return Get( () => SelectedInventory, new Collection<PmlInventory>() ); }
			set { Set( () => SelectedInventory, value ); }
		}


		public string SelectedInventoryName
		{
			get { return Get( () => SelectedInventoryName, "" ); }
			set { Set( () => SelectedInventoryName, value ); }
		}


		public string SelectedInventoryBarcode
		{
			get { return Get( () => SelectedInventoryBarcode, "" ); }
			set { Set( () => SelectedInventoryBarcode, value ); }
		}


		[DependsUpon( nameof( SelectedInventoryName ) )]
		public void WhenSelectedInventoryNameChanges()
		{
			Logging.WriteLogLine( "New Inventory name: " + SelectedInventoryName );

			if( SelectedInventoryName.IsNotNullOrWhiteSpace() )
			{
				foreach( var pi in Inventory )
				{
					if( pi.Description == SelectedInventoryName )
					{
						SelectedInventoryBarcode = pi.Barcode;

						break;
					}
				}
			}
			else
				SelectedInventoryBarcode = string.Empty;
		}

		public void GetInventory()
		{
			Task.Run( async () =>
			          {
				          Logging.WriteLogLine( "Loading Inventory" );

				          var Inv = await Azure.Client.RequestPML_GetInventory();

				          if( !( Inv is null ) )
				          {
					          var names = new List<string>();
					          var IList = new ObservableCollection<PmlInventory>();

					          foreach( var I in Inv )
					          {
						          //Logging.WriteLogLine( "Adding item: " + I.Description );
						          IList.Add( I );
						          names.Add( I.Description );
					          }

					          if( names.Count > 0 )
					          {
						          names.Insert( 0, "" );

						          Dispatcher.Invoke( () =>
						                             {
							                             Inventory                = IList;
							                             InventoryNames           = new ObservableCollection<string>( names );
							                             FullListOfInventoryNames = names;

							                             //SelectedInventoryName = InventoryNames[0];
						                             } );
					          }
				          }
			          } );
		}

	#endregion
	}
}