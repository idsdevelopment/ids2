﻿using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data._Customers.Pml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Protocol.Data;
using Utils;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Trips.TripEntry
{
	internal class EditPackagesInventoryModel : ViewModelBase
    {
        public bool Loaded
        {
            get { return Get(() => Loaded, false); }
            set { Set(() => Loaded, value); }
        }

        protected override void OnInitialised()
        {
            base.OnInitialised();
            Loaded = false;

            Task.WaitAll(
                Task.Run(GetServiceLevels),
                Task.Run( GetInventory )
            );
        }

        // TODO this needs to be updated to the correct page
        public string HelpUri
        {
            get { return Get(() => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/overview"); }
        }


        public bool IsCancelled
        {
            get { return Get(() => IsCancelled, true); }
            set { Set(() => IsCancelled, value); }
        }


        public ObservableCollection<Inventory> Inventory
        {
            get { return Get(() => Inventory, new ObservableCollection<Inventory>()); }
            set { Set(() => Inventory, value); }
        }

        private readonly List<Inventory> OriginalInventory = new List<Inventory>();

        public ObservableCollection<string> InventoryNames
        {
            get { return Get(() => InventoryNames, new ObservableCollection<string>()); }
            set { Set(() => InventoryNames, value); }
        }

        public Inventory SelectedInventory
        {
            get { return Get(() => SelectedInventory); }
            set { Set(() => SelectedInventory, value); }
        }

        public string SelectedInventoryName
        {
            get { return Get(() => SelectedInventoryName, ""); }
            set { Set(() => SelectedInventoryName, value); }
        }

        public string AdhocFilter
        {
            get { return Get(() => AdhocFilter, ""); }
            set { Set(() => AdhocFilter, value); }
        }

        [DependsUpon(nameof(AdhocFilter))]
        public void WhenAdhocFilterChanges()
        {
            Logging.WriteLogLine("Filter: " + AdhocFilter);

            if (AdhocFilter.IsNotNullOrWhiteSpace())
            {
                List<Inventory> matches = new List<Inventory>();
                string[] words = AdhocFilter.Split(' ');
                if (words.Length > 0)
                {
                    foreach (Inventory pi in OriginalInventory)
                    {
                        string lowerName = pi.Description.ToLower();
                        string lowerBarcode = pi.Barcode.ToLower();
                        bool match = true;
                        foreach (string word in words)
                        {
                            if (!lowerName.Contains(word) && !lowerBarcode.Contains(word))
                            {
                                match = false;
                                break;
                            }
                        }
                        if (match)
                        {
                            matches.Add(pi);
                        }
                    }

                    Dispatcher.Invoke(() =>
                    {
                        Inventory = new ObservableCollection<Inventory>(matches);
                    });
                }
            }
            else
            {
                // Restore all 
                Dispatcher.Invoke(() =>
                {
                    Inventory = new ObservableCollection<Inventory>(OriginalInventory);
                });
            }
        }

        private void GetInventory()
        {
            Task.Run(async () =>
                     {
                         Logging.WriteLogLine("Loading Inventory");


                         var Inv = await Azure.Client.RequestPML_GetInventory();
                         if (!(Inv is null))
                         {
                             OriginalInventory.Clear();
                             List<string> names = new List<string>();
                             var IList = new ObservableCollection<Inventory>();
                             foreach (var I in Inv)
                             {
                                 Logging.WriteLogLine("Adding item: " + I.Description);
                                 IList.Add(I);
                                 OriginalInventory.Add(I);
                                 names.Add(I.Description);
                             }
                             // Testing
                             //for (int i = 0; i < 4; i++)
                             //{
                             //    foreach (var I in Inv)
                             //    {
                             //        Logging.WriteLogLine("Adding item: " + I.Description);
                             //        IList.Add(I);
                             //        OriginalInventory.Add(I);
                             //        names.Add(I.Description);
                             //    }
                             //}

                             if (names.Count > 0)
                             {
                                 Dispatcher.Invoke(() =>
                                                    {
                                                        Inventory = IList;
                                                        InventoryNames = new ObservableCollection<string>(names);
                                                        SelectedInventoryName = InventoryNames[0];
                                                    });
                             }
                         }
                     });
        }

        public List<string> ServiceLevels
        {
            get => Get(() => ServiceLevels, new List<string>());
            //get => Get(() => ServiceLevels, GetServiceLevels);
            set { Set(() => ServiceLevels, value); }
        }


        public string SelectedServiceLevel
        {
            get { return Get(() => SelectedServiceLevel, ""); }
            set { Set(() => SelectedServiceLevel, value); }
        }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<string> GetServiceLevels()
        {
            // From Terry: Need to do this to avoid bad requests in the server log
            if (!IsInDesignMode)
            {
                Task.Run(async () =>
                {
                    try
                    {
                        var sls = await Azure.Client.RequestGetServiceLevelsDetailed();

                        if (sls != null)
                        {
                            var serviceLevels = (from S in sls
                                                 orderby S.SortOrder, S.OldName
                                                 select S.OldName).ToList();

                            Logging.WriteLogLine("Loaded " + serviceLevels.Count + " service levels");

                            Dispatcher.Invoke(() =>
                                              {
                                                  ServiceLevels = serviceLevels;
                                              });
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                        Logging.WriteLogLine("Exception thrown: " + e);
                        MessageBox.Show("Error fetching Service Levels\n" + e, "Error", MessageBoxButton.OK);
                    }
                });
            }
            //// TODO Not in db yet
            //List<string> sls = new List<string>()
            //{
            //    "Regular",
            //    "Rush",
            //    "Truck"
            //};

            //return sls;

            return ServiceLevels;
        }

        public List<ExtendedInventory> SelectedInventoryItems
        {
            get { return Get(() => SelectedInventoryItems, new List<ExtendedInventory>()); }
            set { Set(() => SelectedInventoryItems, value); }
        }

        [DependsUpon(nameof(SelectedInventoryItems))]
        public bool IsChangeServiceLevelEnabled
        {
            // get { return Get(() => IsChangeServiceLevelEnabled, false); }
            get
            {
                bool enable = false;
                if (SelectedInventoryItems.Count > 0)
                {
                    foreach (ExtendedInventory ei in SelectedInventoryItems)
                    {
                        if (ei.IsSelected)
                        {
                            enable = true;
                            break;
                        }
                    }
                }

                return enable;
            }
            set { Set(() => IsChangeServiceLevelEnabled, value); }
        }

        public void CheckForCheckedItems()
        {
            foreach (ExtendedInventory ei in SelectedInventoryItems)
            {
                if (ei.IsSelected)
                {
                    IsChangeServiceLevelEnabled = true;
                    break;
                }
            }
        }


        public void AddSelectedInventoryItems(List<ExtendedInventory> list)
        {
            List<ExtendedInventory> newList = new List<ExtendedInventory>(SelectedInventoryItems);
            foreach (ExtendedInventory ei in list)
            {
                if (!AlreadyAdded(ei))
                {
                    ei.IsSelected = true; // Always set the check mark
                    newList.Add(ei);
                }
            }

            Dispatcher.Invoke(() =>
            {
                SelectedInventoryItems = new List<ExtendedInventory>(newList);
            });
        }

        private bool AlreadyAdded(ExtendedInventory ei)
        {
            bool yes = false;
            foreach (ExtendedInventory tmp in SelectedInventoryItems)
            {
                if (tmp.Name == ei.Name)
                {
                    yes = true;
                    break;
                }
            }

            return yes;
        }

        public void RemoveSelectedInventoryItems(List<ExtendedInventory> list)
        {
            List<ExtendedInventory> newList = new List<ExtendedInventory>();
            foreach (ExtendedInventory sii in SelectedInventoryItems)
            {
                if (!list.Contains(sii)) {
                    newList.Add(sii);
                }
            }

            Dispatcher.Invoke(() =>
            {
                SelectedInventoryItems = new List<ExtendedInventory>(newList);
            });
        }

        public void Execute_SelectAll()
        {
            Select(true);
        }

        public void Execute_SelectNone()
        {
            Select(false);
        }

        private void Select(bool isSelected)
        {
            if (SelectedInventoryItems.Count > 0)
            {
                List<ExtendedInventory> list = new List<ExtendedInventory>(SelectedInventoryItems);

                foreach (ExtendedInventory ei in SelectedInventoryItems)
                {
                    ei.IsSelected = isSelected;
                }

                Dispatcher.Invoke(() =>
                {
                    SelectedInventoryItems = new List<ExtendedInventory>(list);
                });
            }
        }

        public List<Inventory> GetInventoryItemsForSelected()
        {
            List<Inventory> list = new List<Inventory>();

            if (SelectedInventoryItems.Count > 0)
            {
                foreach (ExtendedInventory ei in SelectedInventoryItems)
                {
                    list.Add(ei.Inventory);
                }
            }

            return list;
        }

        
        public void Execute_SetSelectedToServiceLevel()
        {
            if (SelectedServiceLevel.IsNotNullOrWhiteSpace() && SelectedInventoryItems.Count > 0)
            {
                Logging.WriteLogLine("Setting " + SelectedInventoryItems.Count + " items to service level: " + SelectedServiceLevel);

                List<ExtendedInventory> eis = new List<ExtendedInventory>();
                foreach (ExtendedInventory ei in SelectedInventoryItems)
                {
                    if (ei.IsSelected)
                    {
                        ei.ServiceLevel = SelectedServiceLevel;
                    }
                    eis.Add(ei);
                }

                SelectedInventoryItems = eis;
            }
        }

    }

    ////////////////////////////////////////////////////////////////////
    ///
    /// <summary>
    /// Utility wrapper.
    /// </summary>
    public class ExtendedInventory
    {
        public bool IsSelected { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string ServiceLevel { get; set; }
        public Inventory Inventory { get; set; }

        private readonly int DefaultQuantity = 1;


        public ExtendedInventory(string name, int quantity, string serviceLevel, Inventory pi)
        {
            Name = name;
            Quantity = quantity;
            ServiceLevel = serviceLevel;
            IsSelected = false;
            Inventory = pi;
        }

        public ExtendedInventory(string name, string quantity, string serviceLevel, Inventory pi)
        {
            Name = name;

            try
            {
                Quantity = int.Parse(quantity);
            }
            catch
            {
                Quantity = DefaultQuantity;
            }
            ServiceLevel = serviceLevel;
            Inventory = pi;
            IsSelected = false;
        }
    }
}
