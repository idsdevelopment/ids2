﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using ViewModels.BaseViewModels;
using ViewModels.Trips.Boards.Common;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

namespace ViewModels.Trips.Boards
{
	public class DispatchViewModel : AMessagingViewModel
	{
		public static readonly string PROGRAM = "Dispatch Board (" + Globals.CurrentVersion.APP_VERSION + ")";

		public bool DataGridVisible
		{
			get { return Get( () => DataGridVisible, IsInDesignMode ); }
			set { Set( () => DataGridVisible, value ); }
		}

		public DisplayTrip SelectedTrip
		{
			get { return Get( () => SelectedTrip, new DisplayTrip() ); }
			set { Set( () => SelectedTrip, value ); }
		}


		public List<DisplayTrip> SelectedTrips
		{
			get { return Get( () => SelectedTrips, new List<DisplayTrip>() ); }
			set { Set( () => SelectedTrips, value ); }
		}


		public ObservableCollection<Driver> Drivers
		{
			get { return Get( () => Drivers, new ObservableCollection<Driver>() ); }
			set { Set( () => Drivers, value ); }
		}


		public bool EnableAssignDriver
		{
			get { return Get( () => EnableAssignDriver, false ); }
			set { Set( () => EnableAssignDriver, value ); }
		}


		public bool EnableDelete
		{
			get { return Get( () => EnableDelete, false ); }
			set { Set( () => EnableDelete, value ); }
		}


		private DisplayTrip PreviousSelectedTrip;

		private readonly Dictionary<string, ServiceLevel> ServiceLevels = new Dictionary<string, ServiceLevel>();

		private bool IgnoreTripReceive = true;


		private void UpdateStatusAndRemove( STATUS status )
		{
			if( !( SelectedTrips is null ) )
			{
				SelectedTrip = new DisplayTrip();

				var TripsToProcess = new List<TripUpdate>( SelectedTrips );

				for( var I = Trips.Count; --I >= 0; )
				{
					var TripId = Trips[ I ].TripId;

					foreach( var TripUpdate in TripsToProcess )
					{
						if( TripId == TripUpdate.TripId )
						{
							Trips.RemoveAt( I );

							break;
						}
					}
				}

				for( var I = TripsToProcess.Count; --I >= 0; )
				{
					var T = TripsToProcess[ I ];

					T.Program = PROGRAM;

					T.Status1          = status;
					T.ReceivedByDevice = false;
					T.ReadByDriver     = false;

					T.BroadcastToDriverBoard   = true;
					T.BroadcastToDispatchBoard = true;
					T.BroadcastToDriver        = true;
				}

				TripsToProcess.ForEach( async update => await Azure.Client.RequestAddUpdateTrip( update ) );

				SelectedTrips = null;
			}
		}

		private ServiceLevel GetServiceLevel( string s )
		{
			if( !ServiceLevels.TryGetValue( s, out var Sl ) )
				Sl = new ServiceLevel();

			return Sl;
		}

		[DependsUpon( nameof( SelectedTrips ) )]
		public void WhenSelectedTripsChange()
		{
			EnableDelete = !( SelectedTrips is null ) && ( SelectedTrips.Count > 0 );
		}

		public void Execute_DeleteTrips()
		{
			UpdateStatusAndRemove( STATUS.DELETED );
		}

		[DependsUpon( nameof( SelectedTrip ) )]
		public void WhenSelectedTripChanges()
		{
			if( !( PreviousSelectedTrip is null ) )
				PreviousSelectedTrip.Selected = false;
			PreviousSelectedTrip = SelectedTrip;

			if( !( SelectedTrip is null ) ) // Happens on tab change
			{
				SelectedTrip.Selected = true;
				var Drvr = SelectedTrip.Driver;

				var Ok = Drvr.IsNotNullOrWhiteSpace();

				if( Ok )
					Ok = Drivers.Any( driver => driver.StaffId == Drvr );

				var Trip = SelectedTrip;
				Trip.EnableAccept = Ok;
			}

			EnableAssignDriver = Trips.Any( displayTrip => displayTrip.EnableAccept );
		}

		public void Execute_DispatchTrips()
		{
			Dispatcher.InvokeAsync( async () =>
			                        {
				                        var Tps = Trips;

				                        var Update = new TripUpdateList
				                                     {
					                                     Program = PROGRAM
				                                     };

				                        for( var I = Tps.Count; --I >= 0; )
				                        {
					                        var T = Tps[ I ];

					                        if( T.EnableAccept )
					                        {
						                        T.Status1 =  ( T.Status3 & ( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) ) != 0 ? STATUS.PICKED_UP : STATUS.DISPATCHED;
						                        T.Status2 =  STATUS1.UNSET;
						                        T.Status3 &= ~STATUS2.WAS_PICKED_UP; // Remove Was Picked-up bit

						                        T.BroadcastToDriverBoard   = true;
						                        T.BroadcastToDispatchBoard = true; // Remove from other dispatch boards
						                        T.BroadcastToDriver        = true;

						                        Update.Trips.Add( T );
						                        Tps.RemoveAt( I );
					                        }
				                        }

				                        if( Update.Trips.Count > 0 )
				                        {
					                        foreach( var T in Update.Trips )
						                        Logging.WriteLogLine( $"Trip: {T.TripId} dispatched to driver: {T.Driver}" );

					                        await Azure.Client.RequestAddUpdateTrips( Update );
				                        }

				                        TripCount = Tps.Count;
			                        } );
		}

		protected override async void OnInitialised()
		{
			DisplayTrip.SelectedBackground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedBackgroundColourBrush" );
			DisplayTrip.SelectedForeground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedForegroundColourBrush" );

			base.OnInitialised();

			try
			{
				Trips.Clear();

				if( !IsInDesignMode )
				{
					var STemp = await Azure.Client.RequestGetServiceLevelsDetailed();
					var Temp  = await Azure.Client.RequestGetTripsByStatus( new StatusRequest { STATUS.ACTIVE } );

					Drivers = new ObservableCollection<Driver>( from D in await Azure.Client.RequestGetDrivers()
					                                            select new Driver
					                                                   {
						                                                   StaffId     = D.StaffId,
						                                                   DisplayName = $"{D.FirstName} {D.LastName}".Trim().Capitalise()
					                                                   } );

					foreach( var ServiceLevel in STemp )
						ServiceLevels.Add( ServiceLevel.NewName, new ServiceLevel( ServiceLevel ) );

					Trips = new ObservableCollection<DisplayTrip>( from T in Temp
					                                               orderby T.Status3 descending, T.CallTime
					                                               select new DisplayTrip( T, GetServiceLevel( T.ServiceLevel ) ) );

					DataGridVisible   = true;
					IgnoreTripReceive = false;
				}
			}
			catch( Exception E )
			{
				Logging.WriteLogLine( E );
			}
		}


		public DispatchViewModel() : this( "" )
		{
		}


		// ReSharper disable once UnusedParameter.Local
		public DispatchViewModel( string userName = "" ) : base( Messaging.DISPATCH_BOARD )
		{
		}

	#region Abstracts

		protected override void OnUpdateTrip( Trip trip )
		{
			if( !IgnoreTripReceive )
			{
				var TripId = trip.TripId;

				if( trip.Status1 == STATUS.ACTIVE )
				{
					Dispatcher.InvokeAsync( () =>
					{
						var Dt = new DisplayTrip( trip, GetServiceLevel( trip.ServiceLevel ) );

						var Ndx = 0;
						var Found = false;

						foreach( var Trip in Trips )
						{
							if( Trip.TripId == TripId )
							{
								Found = true;

								break;
							}

							++Ndx;
						}

						if( Found )
						{
							Trips.RemoveAt( Ndx );
							Trips.Insert( ( Dt.Status3 & ( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) ) != 0 ? 0 : Ndx, Dt ); // Put Undeliverable First (Shouldn't Happen)
						}
						else if( ( Dt.Status3 & ( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) ) != 0 )
							Trips.Insert( 0, Dt ); // Put Undeliverable First
						else
							Trips.Add( Dt );

						TripCount = Trips.Count;
					} );
				}

				else // May have been changed somewhere else
				{
					Dispatcher.Invoke( () =>
					{
						var Trps = Trips;

						for( var I = Trips.Count; --I >= 0; )
						{
							if( Trps[ I ].TripId == TripId )
							{
								Trips.RemoveAt( I );

								break;
							}
						}
					} );
				}
			}
		}

		protected override void OnRemoveTrip( string tripId )
		{
			throw new NotImplementedException();
		}

		protected override void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, bool receivedByDevice, bool readByDriver )
		{
			throw new NotImplementedException();
		}

	#endregion

	#region Filter

		public string Filter
		{
			get { return Get( () => Filter, "" ); }
			set { Set( () => Filter, value ); }
		}

		[DependsUpon350( nameof( Filter ) )]
		public void WhenFilterChanges()
		{
			var F = Filter.Trim().ToLower();

			var Count = 0;

			foreach( var Trip in Trips )
			{
				Trip.Filter = F;

				if( Trip.IsVisible )
					Count++;
			}

			TotalShipmentCount = Count;
		}

	#endregion

	#region Trips

		public ObservableCollection<DisplayTrip> Trips
		{
			get { return Get( () => Trips, new ObservableCollection<DisplayTrip>() ); }
			set { Set( () => Trips, value ); }
		}


		public int TotalShipmentCount
		{
			get { return Get( () => TotalShipmentCount, 0 ); }
			set { Set( () => TotalShipmentCount, value ); }
		}


		public int TripCount
		{
			get { return Get( () => TripCount, 0 ); }
			set { Set( () => TripCount, value ); }
		}

		[DependsUpon( nameof( Trips ) )]
		public void WhenTripsChange()
		{
			var T = Trips;
			TripCount = T.Count;

			var Count = T.Count( trip => trip.IsVisible );

			TotalShipmentCount = Count;
		}

	#endregion

	#region Menu

		public void Execute_ClearSelection()
		{
			foreach( var DisplayTrip in Trips )
				DisplayTrip.EnableAccept = false;
			SelectedTrip = null;
		}

		public void Execute_AuditTrip()
		{
			Globals.RunProgram( Globals.TRIP_AUDIT_TRAIL, SelectedTrip.TripId );
		}

	#endregion
	}
}