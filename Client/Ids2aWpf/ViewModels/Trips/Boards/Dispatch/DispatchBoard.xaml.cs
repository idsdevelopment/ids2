﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using Utils;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.Boards
{
	/// <summary>
	///     Interaction logic for Dispatch.xaml
	/// </summary>
	public partial class DispatchBoard : Page
	{
		private DispatchViewModel Model;
		private DisplayTrip CurrentTrip;
		private bool WasKeyPress;

		private void DataGrid_BeginningEdit( object sender, DataGridBeginningEditEventArgs e )
		{
			var Ndx = e.Column.DisplayIndex;

			if( Ndx == 0 )
			{
				var Cell = DataGrid.GetCell( e.Row.GetIndex(), Ndx );
				DriversPopup.PlacementTarget = Cell;
				DriversPopup.IsOpen = true;
				DriverCombo.RemoveParent();
			}
		}

		private void DataGrid_CellEditEnding( object sender, DataGridCellEditEndingEventArgs e )
		{
			DriversPopup.IsOpen = false;
		}


		private void DataGrid_PreparingCellForEdit( object sender, DataGridPreparingCellForEditEventArgs e )
		{
			if( e.Column.DisplayIndex == 0 )
			{
				var StackPanel = e.EditingElement.FindChild<StackPanel>();

				if( !( StackPanel is null ) )
				{
					DriverCombo.RemoveParent();
					StackPanel.Children.Add( DriverCombo );
					DriverCombo.DataContext = Model;
					DriverCombo.Visibility = Visibility.Visible;
					DriverCombo.UpdateLayout();
					var TexBox = DriverCombo.FindChild<TextBox>( "PART_EditableTextBox" );

					if( !( CurrentTrip is null ) )
						TexBox.Text = CurrentTrip.Driver;
					TexBox?.Focus();
				}
			}
		}

		private void MenuItem_Click( object sender, RoutedEventArgs e )
		{
			if( !( Model is null ) )
			{
				var SelectedTrips = Model.SelectedTrips;

				if( !( SelectedTrips is null ) )
				{
					var Count = SelectedTrips.Count;

					if( Count > 0 )
					{
						var Trip = SelectedTrips[ Count - 1 ];

						var Ndx = 0;

						foreach( var ModelTrip in Model.Trips )
						{
							if( Trip == ModelTrip )
							{
								var Cell = DataGrid.GetCell( Ndx, 0 );
								Cell.Focus();
								DataGrid.BeginEdit();
							}

							++Ndx;
						}
					}
				}
			}
		}

		private void DataGrid_SelectedCellsChanged( object sender, SelectedCellsChangedEventArgs e )
		{
			if( ( e.AddedCells.Count > 0 ) && e.AddedCells[ 0 ].Item is DisplayTrip Trip )
				CurrentTrip = Trip;
		}

		private void DataGrid_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( sender is DataGrid Grid )
			{
				var Trips = Model.SelectedTrips;

				if( Trips is null )
					Trips = new List<DisplayTrip>();
				else
					Trips.Clear();

				Model.SelectedTrips = null;
				Trips.AddRange( Grid.SelectedItems.Cast<DisplayTrip>() );
				Model.SelectedTrips = Trips;
			}
		}

		private void ToggleButton_Checked( object sender, RoutedEventArgs e )
		{
			// Binding sometimes doesn't work
			if( sender is ToggleButton Tb )
			{
				var DGrid = Tb.FindParent<DataGrid>();

				if( !( DGrid is null ) )
				{
					var Ndx = DGrid.SelectedIndex;

					if( Ndx >= 0 )
					{
						var Itm = (DisplayTrip)DGrid.SelectedItem;
						Itm.DetailsVisible = true;

						var Row = DGrid.GetRow( Ndx );

						if( !( Row is null ) )
							DGrid.ScrollIntoView( Row );
					}
				}
			}
		}

		private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
		{
			// Binding sometimes doesn't work
			if( sender is ToggleButton Tb )
			{
				var Grid = Tb.FindParent<DataGrid>();

				if( Grid?.SelectedItem is DisplayTrip Itm )
					Itm.DetailsVisible = false;
			}
		}

		public DispatchBoard()
		{
			Initialized += ( sender, args ) =>
			               {
				               if( DataContext is DispatchViewModel M )
					               Model = M;

				               void DoSelectionChanged()
				               {
					               WasKeyPress = false;

					               if( DriverCombo.Visibility == Visibility.Visible )
					               {
						               if( DriverCombo.SelectedItem is Driver D )
						               {
							               CurrentTrip.Driver = D.StaffId.Trim();
							               DataGrid.CancelEdit();
							               Model.SelectedTrip = CurrentTrip;
							               Model.WhenSelectedTripChanges();
						               }
					               }
				               }

				               DriverCombo.PreviewKeyDown += ( o, eventArgs ) =>
				                                             {
					                                             var Key = eventArgs.Key;

					                                             switch( Key )
					                                             {
					                                             case Key.Return:
					                                             case Key.Tab:
						                                             {
							                                             WasKeyPress = false;
							                                             var Txt = DriverCombo.Text;

							                                             if( DriverCombo.ItemsSource is ObservableCollection<Driver> Drivers )
							                                             {
								                                             foreach( var Driver in Drivers )
								                                             {
									                                             if( Driver.StaffId == Txt )
									                                             {
										                                             DriverCombo.SelectedItem = Driver;
										                                             DoSelectionChanged();

										                                             break;
									                                             }
								                                             }
							                                             }
						                                             }

						                                             break;

					                                             default:
						                                             WasKeyPress = true;

						                                             break;
					                                             }
				                                             };

				               DriverCombo.SelectionChanged += ( o, eventArgs ) =>
				                                               {
					                                               if( !WasKeyPress )
						                                               DoSelectionChanged();
					                                               else
						                                               WasKeyPress = false;
				                                               };

				               DriverCombo.LostFocus += ( o, eventArgs ) =>
				                                        {
					                                        DoSelectionChanged();
				                                        };

				               DriverCombo.DropDownClosed += ( o, eventArgs ) =>
				                                             {
					                                             DoSelectionChanged();
				                                             };

				               DriversPopup.OnSelectionChanged += ( o, s ) =>
				                                                  {
					                                                  try
					                                                  {
						                                                  var Driver = s.Trim();

						                                                  foreach( var Trip in Model.SelectedTrips )
						                                                  {
							                                                  Trip.Driver = Driver;
							                                                  Trip.EnableAccept = true;
						                                                  }

						                                                  DriversPopup.IsOpen = false;
						                                                  DataGrid.CancelEdit();
						                                                  Model.SelectedTrip = CurrentTrip;
					                                                  }
					                                                  catch( Exception Exception )
					                                                  {
						                                                  Console.WriteLine( Exception );
					                                                  }
				                                                  };
			               };
			InitializeComponent();
		}

		private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			DisplayTrip selectedTrip = (DisplayTrip)this.DataGrid.SelectedItem;
			if (selectedTrip != null)
			{
				Logging.WriteLogLine("Selected trip: " + selectedTrip.TripId);

				//DisplayTrip latestVersion = GetLatestVersion(selectedTrip.TripId);
				//// TODO Checking for TripId in case the trip is in Storage
				//if (latestVersion.TripId.IsNotNullOrWhiteSpace() && latestVersion != null && selectedTrip.LastModified.ToLongTimeString() != latestVersion.LastModified.ToLongTimeString())
				//{
				//	Logging.WriteLogLine("Latest version - was : " + selectedTrip.LastModified.ToLongTimeString() + ", now: " + latestVersion.LastModified.ToLongTimeString());
				//	selectedTrip = latestVersion;
				//	if (DataContext is SearchTripsModel Model)
				//	{
				//		Dispatcher.Invoke(() =>
				//		{
				//			Model.UpdateTripsWithLatestVersion(selectedTrip);
				//		});
				//	}
				//}

				var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
				if (tis != null && tis.Count > 0)
				{
					PageTabItem pti = null;
					for (int i = 0; i < tis.Count; i++)
					{
						var ti = tis[i];
						if (ti.Content is TripEntry.TripEntry)
						{
							pti = ti;
							// Load the trip into the first TripEntry tab
							Logging.WriteLogLine("Displaying trip in extant " + Globals.TRIP_ENTRY + " tab");

							//( (TripEntry.TripEntryModel)pti.Content.DataContext ).Execute_ClearTripEntry();
							((TripEntry.TripEntryModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
							((TripEntry.TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
							Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

							break;
						}
					}

					if (pti == null)
					{
						// Need to create a new Trip Entry tab
						Logging.WriteLogLine("Opening new " + Globals.TRIP_ENTRY + " tab");
						Globals.RunProgram(Globals.TRIP_ENTRY, selectedTrip);
					}
				}
			}
		}
	}
}