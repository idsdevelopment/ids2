﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using Utils;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.Boards
{
	/// <summary>
	///     Interaction logic for DriversBoard.xaml
	/// </summary>
	public partial class DriversBoard : Page
	{
		private DriversBoardViewModel Model;
		private ScrollViewer ScrollViewer;

		private void DataGrid_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( sender is DataGrid Grid )
			{
				var Trips = Model.SelectedTrips;

				if( Trips is null )
					Trips = new List<DisplayTrip>();
				else
					Trips.Clear();

				Model.SelectedTrips = null;
				Trips.AddRange( Grid.SelectedItems.Cast<DisplayTrip>() );
				Model.SelectedTrips = Trips;
			}
		}


		private void DataGrid_PreviewMouseWheel( object sender, MouseWheelEventArgs e )
		{
			e.Handled = true;
			ScrollViewer.ScrollToVerticalOffset( ScrollViewer.VerticalOffset - e.Delta );
		}

		private void ToggleButton_Checked( object sender, RoutedEventArgs e )
		{
			// Binding sometimes doesn't work
			if( sender is ToggleButton Tb )
			{
				var Grid = Tb.FindParent<DataGrid>();

				if( !( Grid is null ) )
				{
					var Ndx = Grid.SelectedIndex;

					if( Ndx >= 0 )
					{
						var Itm = (DisplayTrip)Grid.SelectedItem;
						Itm.DetailsVisible = true;

						var Row = Grid.GetRow( Ndx );

						if( !( Row is null ) )
							Grid.ScrollIntoView( Row );
					}
				}
			}
		}

		private void ToggleButton_Unchecked( object sender, RoutedEventArgs e )
		{
			// Binding sometimes doesn't work
			if( sender is ToggleButton Tb )
			{
				var Grid = Tb.FindParent<DataGrid>();

				if( Grid?.SelectedItem is DisplayTrip Itm )
					Itm.DetailsVisible = false;
			}
		}

		public DriversBoard()
		{
			Initialized += ( sender, args ) =>
			               {
				               Model = (DriversBoardViewModel)DataContext;

				               DataGrid.Initialized += ( o, eventArgs ) =>
				                                       {
				                                       };

				               DataGrid.Loaded += ( o, eventArgs ) =>
				                                  {
					                                  var Temp = DataGrid.FindChildren<ScrollViewer>();
					                                  ScrollViewer = Temp[ 0 ];
				                                  };

				               //	               ScrollViewer = Temp[ 0 ];
			               };
			InitializeComponent();
		}

		private void DataGrid_LostFocus( object sender, RoutedEventArgs e )
		{
			if( sender is DataGrid Grid )
				Grid.UnselectAll();
		}

		private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (DataContext is DriversBoardViewModel Model)
			{
				//DisplayTrip selectedTrip = (DisplayTrip)this.DataGrid.SelectedItem;
				if (Model.SelectedTrip != null)
				{
					Logging.WriteLogLine("Selected trip: " + Model.SelectedTrip.TripId);					

					//DisplayTrip latestVersion = GetLatestVersion(selectedTrip.TripId);
					//// TODO Checking for TripId in case the trip is in Storage
					//if (latestVersion.TripId.IsNotNullOrWhiteSpace() && latestVersion != null && selectedTrip.LastModified.ToLongTimeString() != latestVersion.LastModified.ToLongTimeString())
					//{
					//	Logging.WriteLogLine("Latest version - was : " + selectedTrip.LastModified.ToLongTimeString() + ", now: " + latestVersion.LastModified.ToLongTimeString());
					//	selectedTrip = latestVersion;
					//	if (DataContext is SearchTripsModel Model)
					//	{
					//		Dispatcher.Invoke(() =>
					//		{
					//			Model.UpdateTripsWithLatestVersion(selectedTrip);
					//		});
					//	}
					//}

					var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
					if (tis != null && tis.Count > 0)
					{
						PageTabItem pti = null;
						for (int i = 0; i < tis.Count; i++)
						{
							var ti = tis[i];
							if (ti.Content is TripEntry.TripEntry)
							{
								pti = ti;
								// Load the trip into the first TripEntry tab
								Logging.WriteLogLine("Displaying trip in extant " + Globals.TRIP_ENTRY + " tab");

								//( (TripEntry.TripEntryModel)pti.Content.DataContext ).Execute_ClearTripEntry();
								((TripEntry.TripEntryModel)pti.Content.DataContext).CurrentTrip = Model.SelectedTrip;
								((TripEntry.TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
								Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

								break;
							}
						}

						if (pti == null)
						{
							// Need to create a new Trip Entry tab
							Logging.WriteLogLine("Opening new " + Globals.TRIP_ENTRY + " tab");
							Globals.RunProgram(Globals.TRIP_ENTRY, Model.SelectedTrip);
						}
					}
				}
			}
		}
	}
}