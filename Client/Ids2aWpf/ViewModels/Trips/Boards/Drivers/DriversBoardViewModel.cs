﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Protocol.Data;
using Utils;
using ViewModels.BaseViewModels;
using ViewModels.Trips.Boards.Common;
using ServiceLevel = ViewModels.Trips.Boards.Common.ServiceLevel;

namespace ViewModels.Trips.Boards
{
	public class TripsByDriver : INotifyPropertyChanged
	{
		public string Driver { get; set; }

		public string DriverAndName { get; set; }

		public bool Expanded
		{
			get => _Expanded;
			set
			{
				if( _Expanded != value )
				{
					_Expanded = value;
					OnPropertyChanged();
				}
			}
		}

		public ObservableCollection<DisplayTrip> Trips { get; set; } = new ObservableCollection<DisplayTrip>();

		private bool _Expanded = true;

		[NotifyPropertyChangedInvocator]
		public virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class DriversBoardViewModel : AMessagingViewModel
	{
		public static readonly string PROGRAM = "Drivers Board (" + Globals.CurrentVersion.APP_VERSION + ")";

		public ObservableCollection<DisplayTrip> Trips
		{
			get { return Get( () => Trips, new ObservableCollection<DisplayTrip>() ); }
			set { Set( () => Trips, value ); }
		}


		public ObservableCollection<TripsByDriver> TripsByDrivers
		{
			get { return Get( () => TripsByDrivers, new ObservableCollection<TripsByDriver>() ); }
			set { Set( () => TripsByDrivers, value ); }
		}


		public int TripCount
		{
			get { return Get( () => TripCount, 0 ); }
			set { Set( () => TripCount, value ); }
		}

		public DisplayTrip SelectedTrip
		{
			get { return Get( () => SelectedTrip, new DisplayTrip() ); }
			set { Set( () => SelectedTrip, value ); }
		}


		public List<DisplayTrip> SelectedTrips
		{
			get { return Get( () => SelectedTrips, new List<DisplayTrip>() ); }
			set { Set( () => SelectedTrips, value ); }
		}


		public bool TripDetailEnabled
		{
			get { return Get( () => TripDetailEnabled, true ); }
			set { Set( () => TripDetailEnabled, value ); }
		}


		public ObservableCollection<Driver> Drivers
		{
			get { return Get( () => Drivers, new ObservableCollection<Driver>() ); }
			set { Set( () => Drivers, value ); }
		}


		public Driver SelectedDriver
		{
			get { return Get( () => SelectedDriver, new Driver() ); }
			set { Set( () => SelectedDriver, value ); }
		}


		public TripsByDriver SelectedTripsByDriver
		{
			get { return Get( () => SelectedTripsByDriver, new TripsByDriver() ); }
			set { Set( () => SelectedTripsByDriver, value ); }
		}


		public bool DataGridVisible
		{
			get { return Get( () => DataGridVisible, IsInDesignMode ); }
			set { Set( () => DataGridVisible, value ); }
		}

		[Setting]
		public bool DriverSearchVisible
		{
			get { return Get( () => DriverSearchVisible, IsInDesignMode ); }
			set { Set( () => DriverSearchVisible, value ); }
		}


		public string DriversFilter
		{
			get { return Get( () => DriversFilter, "" ); }
			set { Set( () => DriversFilter, value ); }
		}


		public bool EnableBounce
		{
			get { return Get( () => EnableBounce, false ); }
			set { Set( () => EnableBounce, value ); }
		}


		public Trip SelectedTripDetails
		{
			get { return Get( () => SelectedTripDetails ); }
			set { Set( () => SelectedTripDetails, value ); }
		}

		private bool IgnoreTripReceive;

		private readonly Dictionary<string, ServiceLevel> ServiceLevels = new Dictionary<string, ServiceLevel>();

		private Dictionary<string, string> DriverDict;

		private readonly object UpdateLock = new object();

		private void UpdateStatusAndRemove( STATUS status, bool resetOtherStatus = false )
		{
			if( !( SelectedTrips is null ) )
			{
				SelectedTrip = new DisplayTrip();
				var TripsToProcess = new List<TripUpdate>( SelectedTrips );

				for( var I = TripsToProcess.Count; --I >= 0; )
				{
					var T = TripsToProcess[ I ];
					RemoveTrip( T.TripId );

					T.Program = PROGRAM;

					if( resetOtherStatus )
					{
						T.Status2 = STATUS1.UNSET;
						T.Status3 = STATUS2.UNSET;
					}
					else
					{
						if( ( T.Status1 == STATUS.DELIVERED ) || ( T.Status1 == STATUS.PICKED_UP ) )
							T.Status3 |= STATUS2.WAS_PICKED_UP;

						if( T.Status2 == STATUS1.UNDELIVERED )
							T.Status3 |= STATUS2.WAS_UNDELIVERABLE;
					}

					T.Status1          = status;
					T.ReceivedByDevice = false;
					T.ReadByDriver     = false;

					T.BroadcastToDriverBoard   = true;
					T.BroadcastToDispatchBoard = true;
					T.BroadcastToDriver        = true;
				}

				TripsToProcess.ForEach( async update => await Azure.Client.RequestAddUpdateTrip( update ) );

				SelectedTrips = null;
			}
		}

		private void UpdateDriverList()
		{
			var Hash = new HashSet<string>();

			foreach( var TripsByDriver in TripsByDrivers )
				Hash.Add( TripsByDriver.Driver );

			foreach( var Driver in Drivers )
				Driver.IsVisible = Hash.Contains( Driver.StaffId );
		}

		private void RemoveTrip( string tripId )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   try
				                   {
					                   var Trps   = Trips;
					                   var Driver = "";

					                   for( var I = Trips.Count; --I >= 0; )
					                   {
						                   var T = Trps[ I ];

						                   if( T.TripId == tripId )
						                   {
							                   Driver = T.Driver;
							                   Trips.RemoveAt( I );

							                   break;
						                   }
					                   }

					                   if( Driver.IsNotNullOrWhiteSpace() )
					                   {
						                   var TTemp = TripsByDrivers;

						                   for( var J = TTemp.Count; --J >= 0; )
						                   {
							                   var DTrips = TTemp[ J ];

							                   if( DTrips.Driver == Driver )
							                   {
								                   var Tps = DTrips.Trips;

								                   for( var I = Tps.Count; --I >= 0; )
								                   {
									                   var Trip = Tps[ I ];

									                   if( Trip.TripId == tripId )
									                   {
										                   Tps.Remove( Trip );

										                   break;
									                   }
								                   }

								                   if( DTrips.Trips.Count == 0 )
									                   TTemp.RemoveAt( J );

								                   break;
							                   }
						                   }
					                   }

					                   UpdateDriverList();
				                   }
				                   catch( Exception Exception )
				                   {
					                   Logging.WriteLogLine( Exception );
				                   }
			                   }
			                 );
		}

		private ServiceLevel GetServiceLevel( string s )
		{
			if( !ServiceLevels.TryGetValue( s, out var Sl ) )
				Sl = new ServiceLevel();

			return Sl;
		}

		[DependsUpon( nameof( SelectedTrips ) )]
		public void WhenSelectedTripsChanges()
		{
			var Trps = SelectedTrips;

			if( !( Trps is null ) )
			{
				TripDetailEnabled = true;
				var Count = Trps.Count;

				if( Count == 1 )
				{
					SelectedTrip = Trps[ 0 ];

					return;
				}

				TripDetailEnabled = false;
				SelectedTrip      = null;
			}
		}


		[DependsUpon350( nameof( SelectedTrip ) )]
		public void WhenSelectedTripChangesDelayed()
		{
			if( !IsInDesignMode )
			{
				var S = SelectedTrip;

				if( !( S is null ) )
				{
					SelectedTripDetails = new Trip();

					var TripId = S.TripId;

					Task.Run( async () =>
					          {
						          var Trip = await Azure.Client.RequestGetTrip( new GetTrip
						                                                        {
							                                                        Signatures = true,
							                                                        TripId     = TripId
						                                                        } );

						          Dispatcher.Invoke( () => { SelectedTripDetails = Trip; } );
					          } );
				}
			}
		}

		[DependsUpon150( nameof( SelectedTrip ) )]
		public void WhenSelectedTripChanges()
		{
			EnableBounce = !( SelectedTrips is null ) && ( SelectedTrips.Count > 0 );
		}

		[DependsUpon( nameof( SelectedDriver ) )]
		public void WhenSelectedDriverChanges()
		{
			var Driver = SelectedDriver.StaffId;

			foreach( var TripsByDriver in TripsByDrivers )
			{
				if( TripsByDriver.Driver == Driver )
				{
					SelectedTripsByDriver  = TripsByDriver;
					TripsByDriver.Expanded = true;
				}
				else
					TripsByDriver.Expanded = false;
			}
		}

		[DependsUpon( nameof( DriverSearchVisible ) )]
		public void WhenDriverSearchVisibleChanges()
		{
			SaveSettings();
		}

		[DependsUpon350( nameof( DriversFilter ) )]
		public void WhenDriversFilterChanges()
		{
			var Filter  = DriversFilter;
			var IsBlank = Filter.IsNullOrWhiteSpace();

			foreach( var Driver in Drivers )
			{
				Driver.IsVisible = IsBlank || ( Driver.StaffId.IndexOf( Filter, StringComparison.CurrentCultureIgnoreCase ) >= 0 )
				                           || ( Driver.DisplayName.IndexOf( Filter, StringComparison.InvariantCultureIgnoreCase ) >= 0 );
			}
		}


		public void Execute_CollapseAll()
		{
			foreach( var TripsByDriver in TripsByDrivers )
				TripsByDriver.Expanded = false;
		}

		public void Execute_ExpandAll()
		{
			foreach( var TripsByDriver in TripsByDrivers )
				TripsByDriver.Expanded = true;
		}

		public void Execute_BounceTrip()
		{
			UpdateStatusAndRemove( STATUS.ACTIVE );
		}


		public void Execute_DeleteTrip()
		{
			UpdateStatusAndRemove( STATUS.DELETED );
		}


		public void Execute_AuditTrip()
		{
			Globals.RunProgram( Globals.TRIP_AUDIT_TRAIL, SelectedTrip.TripId );
		}

		protected override async void OnInitialised()
		{
			DisplayTrip.SelectedBackground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedBackgroundColourBrush" );
			DisplayTrip.SelectedForeground = Globals.Dictionary.AsSolidColorBrush( "DefaultSelectedForegroundColourBrush" );

			base.OnInitialised();

			try
			{
				Trips.Clear();

				if( !IsInDesignMode )
				{
					var STemp = await Azure.Client.RequestGetServiceLevelsDetailed();
					var Temp  = await Azure.Client.RequestGetTripsByStatus( new StatusRequest { STATUS.DISPATCHED, STATUS.PICKED_UP, STATUS.DELIVERED } );

					Drivers = new ObservableCollection<Driver>( from D in await Azure.Client.RequestGetDrivers()
					                                            select new Driver
					                                                   {
						                                                   StaffId     = D.StaffId,
						                                                   DisplayName = $"{D.FirstName} {D.LastName}".Trim().Capitalise()
					                                                   } );

					foreach( var ServiceLevel in STemp )
						ServiceLevels.Add( ServiceLevel.NewName, new ServiceLevel( ServiceLevel ) );

					var Trps = new ObservableCollection<DisplayTrip>( from T in Temp
					                                                  select new DisplayTrip( T, GetServiceLevel( T.ServiceLevel ) ) );
					TripCount = Trps.Count;
					Trips     = Trps;

					TripsByDrivers = new ObservableCollection<TripsByDriver>( from T in Trps
					                                                          orderby T.Driver
					                                                          group T by T.Driver
					                                                          into G
					                                                          select new TripsByDriver { Driver = G.Key, Trips = new ObservableCollection<DisplayTrip>( G ) } );

					DriverDict = Drivers.ToDictionary( driver => driver.StaffId, driver => driver.DisplayName );

					foreach( var TripsByDriver in TripsByDrivers )
					{
						if( DriverDict.TryGetValue( TripsByDriver.Driver, out var DriverName ) )
							DriverName = $" -- {DriverName}";

						TripsByDriver.DriverAndName = $"{TripsByDriver.Driver}{DriverName}";
					}

					UpdateDriverList();

					DataGridVisible   = true;
					IgnoreTripReceive = false;
				}
			}
			catch( Exception Exception )
			{
				Logging.WriteLogLine( Exception );
			}
		}

		public DriversBoardViewModel( string userName = "" ) : base( Messaging.DRIVERS_BOARD )
		{
			UserName = userName;
		}

		public DriversBoardViewModel() : this( "" )
		{
		}

	#region Abstracts

		protected override void OnUpdateTrip( Trip trip )
		{
			if( !IgnoreTripReceive )
			{
				var TripId = trip.TripId;

				switch( trip.Status1 )
				{
				default:
					lock( UpdateLock )
						RemoveTrip( trip.TripId );

					break;

				case STATUS.DISPATCHED:
				case STATUS.PICKED_UP:
				case STATUS.DELIVERED:
					Dispatcher.Invoke( () =>
					                   {
						                   lock( UpdateLock )
						                   {
							                   try
							                   {
								                   var Dt = new DisplayTrip( trip, GetServiceLevel( trip.ServiceLevel ) );

								                   var Ndx   = 0;
								                   var Found = false;

								                   foreach( var Trip in Trips )
								                   {
									                   if( Trip.TripId == TripId )
									                   {
										                   Found = true;

										                   break;
									                   }

									                   ++Ndx;
								                   }

								                   if( Found )
								                   {
									                   Trips.RemoveAt( Ndx );
									                   Trips.Insert( Ndx, Dt );
								                   }
								                   else
									                   Trips.Add( Dt );

								                   TripCount = Trips.Count;

								                   var Driver = trip.Driver;
								                   Found = false;

								                   foreach( var TripsByDriver in TripsByDrivers )
								                   {
									                   if( TripsByDriver.Driver == Driver )
									                   {
										                   Found = true;
										                   var TFound = false;
										                   var Trps   = TripsByDriver.Trips;

										                   for( var I = Trps.Count; --I >= 0; )
										                   {
											                   if( Trps[ I ].TripId == TripId )
											                   {
												                   TFound    = true;
												                   Trps[ I ] = Dt;

												                   break;
											                   }
										                   }

										                   if( !TFound )
											                   Trps.Add( Dt );

										                   break;
									                   }
								                   }

								                   if( !Found )
								                   {
									                   if( DriverDict.TryGetValue( Driver, out var DriverName ) )
										                   DriverName = $" -- {DriverName}";

									                   var NewDriver = new TripsByDriver
									                                   {
										                                   Driver        = Driver,
										                                   DriverAndName = $"{Driver}{DriverName}"
									                                   };
									                   NewDriver.Trips.Add( Dt );
									                   TripsByDrivers.Add( NewDriver );
								                   }

								                   UpdateDriverList();
							                   }
							                   catch( Exception Exception )
							                   {
								                   Logging.WriteLogLine( Exception );
							                   }
						                   }
					                   } );

					break;
				}
			}
		}

		protected override void OnRemoveTrip( string tripId )
		{
			for( var I = Trips.Count; --I >= 0; )
			{
				if( Trips[ I ].TripId == tripId )
				{
					Trips.RemoveAt( I );

					break;
				}
			}

			if( !( SelectedTrip is null ) && ( SelectedTrip.TripId == tripId ) )
			{
				SelectedTrip = null;
				WhenSelectedTripChangesDelayed();
			}
		}

		protected override void OnUpdateStatus( string tripId, STATUS status, STATUS1 status1, bool receivedByDevice, bool readByDriver )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   if( ( status != STATUS.ACTIVE ) || ( ( status == STATUS.ACTIVE ) && ( status1 == STATUS1.UNDELIVERED ) ) )
				                   {
					                   lock( UpdateLock )
					                   {
						                   var TripId = tripId;
						                   var Driver = "";
						                   var Delete = ( status < STATUS.DISPATCHED ) || ( ( status >= STATUS.DELIVERED ) && ( status1 != STATUS1.UNDELIVERED ) );
						                   var Trps   = Trips;

						                   for( var I = Trps.Count; --I >= 0; )
						                   {
							                   var T = Trps[ I ];

							                   if( T.TripId == TripId )
							                   {
								                   Driver = T.Driver;

								                   if( Delete )
									                   Trps.RemoveAt( I );

								                   break;
							                   }
						                   }

						                   if( Driver.IsNotNullOrWhiteSpace() )
						                   {
							                   var ByDrivers = TripsByDrivers;

							                   for( var I = ByDrivers.Count; --I >= 0; )
							                   {
								                   var TripsByDriver = ByDrivers[ I ];

								                   if( TripsByDriver.Driver == Driver )
								                   {
									                   var Tps = TripsByDriver.Trips;

									                   for( var J = Tps.Count; --J >= 0; )
									                   {
										                   var T = Tps[ J ];

										                   if( T.TripId == TripId )
										                   {
											                   if( Delete )
												                   Tps.RemoveAt( J );
											                   else
											                   {
												                   T.ReceivedByDevice = receivedByDevice;
												                   T.ReadByDriver     = readByDriver;
												                   T.Status           = DisplayTrip.ToDisplayTripStatus( status, status1 );

												                   if( !( SelectedTrip is null ) && ( SelectedTrip.TripId == TripId ) )
													                   WhenSelectedTripChangesDelayed();
											                   }

											                   break;
										                   }
									                   }
								                   }
							                   }
						                   }

						                   UpdateDriverList();
					                   }
				                   }
			                   } );
		}

	#endregion
	}
}