﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Media;
using IdsControlLibraryV2.Utils.Colours;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Newtonsoft.Json;
using Protocol.Data;
using Utils;
using ViewModels.Convertors;

namespace ViewModels.Trips.Boards.Common
{
	public static class DisplayTripExtensions
	{
		public static string AsString( this DisplayTrip.STATUS status )
		{
			return status switch
			       {
				       DisplayTrip.STATUS.UNPICKUPABLE      => "Cannot pick-up",
				       DisplayTrip.STATUS.UNDELIVERABLE     => "Undeliverable",
				       DisplayTrip.STATUS.UNDELIVERABLE_NEW => "Undeliverable",
				       _                                    => ( (STATUS)status ).AsString()
			       };
		}
	}

	public class Driver : INotifyPropertyChanged
	{
		public string StaffId     { get; set; }
		public string DisplayName { get; set; }

		public bool IsVisible
		{
			get => _IsVisible && HasTrips;
			set
			{
				if( _IsVisible != value )
				{
					_IsVisible = value;
					OnPropertyChanged();
				}
			}
		}

		private bool _IsVisible = true;
		public  bool HasTrips   = true;

		public override string ToString() => StaffId;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}


	public class ServiceLevel
	{
		public SolidColorBrush Foreground,
		                       Background;

		public string Name;

		public ServiceLevel()
		{
			Foreground = new SolidColorBrush( Colors.Black );
			Background = new SolidColorBrush( Colors.White );
		}

		public ServiceLevel( Protocol.Data.ServiceLevel s )
		{
			Foreground = s.ForegroundARGB.ArgbToSolidColorBrush();
			Background = s.BackgroundARGB.ArgbToSolidColorBrush();
		}
	}

	public class DisplayPackage : TripPackage, INotifyPropertyChanged
	{
		public bool DetailsVisible
		{
			get => _DetailsVisible;
			set
			{
				_DetailsVisible = value;
				OnPropertyChanged();
			}
		}

		private bool _DetailsVisible;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public DisplayPackage( TripPackage p ) : base( p )
		{
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class DisplayTrip : Trip, INotifyPropertyChanged
	{
		// Must match Protocol Data STATUS
		public enum STATUS : sbyte
		{
			UNSET,
			NEW,
			ACTIVE,
			DISPATCHED,
			PICKED_UP,
			DELIVERED,
			VERIFIED,
			POSTED,
			INVOICED,
			SCHEDULED,
			PAID,
			DELETED,

			UNKNOWN,

			UNPICKUPABLE,
			UNDELIVERABLE,
			UNDELIVERABLE_NEW
		}

		internal static SolidColorBrush SelectedBackground,
		                                SelectedForeground;

		public new ObservableCollection<DisplayPackage> Packages
		{
			get
			{
				if( _Packages is null )
				{
					var Temp = new ObservableCollection<DisplayPackage>();

					foreach( var Package in base.Packages )
						Temp.Add( new DisplayPackage( Package ) );

					Packages = Temp;
				}

				return _Packages;
			}

			set
			{
				_Packages = value;
				OnPropertyChanged();
			}
		}

		public bool IsVisible
		{
			get => _IsVisible;
			set
			{
				_IsVisible = value;
				OnPropertyChanged();
			}
		}

		[JsonIgnore]
		public string Filter
		{
			get => _Filter;
			set
			{
				_Filter = value;

				bool StringContainsFilter( string v )
				{
					return v.Contains( value, StringComparison.OrdinalIgnoreCase );
				}

				bool DateContainsFilter( DateTimeOffset v )
				{
					var Str = v.ToString( "MMM d h:mm tt" );

					return StringContainsFilter( Str );
				}

				bool DecimalContainsFilter( decimal v )
				{
					var Str = v.ToString( CultureInfo.InvariantCulture );

					return StringContainsFilter( Str );
				}

				IsVisible = value.IsNullOrEmpty()
				            || StringContainsFilter( Driver )
				            || StringContainsFilter( TripId )
				            || StringContainsFilter( PickupZone )
				            || StringContainsFilter( PickupCompanyName )
				            || StringContainsFilter( DeliveryZone )
				            || StringContainsFilter( DeliveryCompanyName )
				            || StringContainsFilter( ServiceLevel )
				            || StringContainsFilter( PackageType )
				            || StringContainsFilter( Board )
				            || DateContainsFilter( ReadyTime )
				            || DateContainsFilter( DueTime )
				            || DecimalContainsFilter( Pieces )
				            || DecimalContainsFilter( Weight )
					;
			}
		}

		[JsonIgnore]
		public bool Selected
		{
			get => _Selected;
			set
			{
				if( value != _Selected )
				{
					_Selected = value;

					OnPropertyChanged( nameof( Foreground ) );
					OnPropertyChanged( nameof( Background ) );
				}
			}
		}

		public new string Driver
		{
			get => base.Driver;
			set
			{
				if( base.Driver != value )
				{
					base.Driver = value;
					OnPropertyChanged();
				}
			}
		}

		[JsonIgnore]
		public bool EnableAccept
		{
			get => _EnableAccept;
			set
			{
				if( value != _EnableAccept )
				{
					_EnableAccept = value;
					OnPropertyChanged();
				}
			}
		}

		[JsonIgnore]
		public STATUS Status
		{
			get => ToDisplayTripStatus( Status1, Status2 );

			set
			{
				Protocol.Data.STATUS NewStatus;
				var                  NewStatus1 = STATUS1.UNSET;

				switch( value )
				{
				case STATUS.UNDELIVERABLE:
					NewStatus  = Protocol.Data.STATUS.DELIVERED;
					NewStatus1 = STATUS1.UNDELIVERED;

					break;
				case STATUS.UNPICKUPABLE:
					NewStatus  = Protocol.Data.STATUS.PICKED_UP;
					NewStatus1 = STATUS1.UNDELIVERED;

					break;
				case STATUS.UNDELIVERABLE_NEW:
					NewStatus  = Protocol.Data.STATUS.NEW;
					NewStatus1 = STATUS1.UNDELIVERED;

					break;
				default:
					NewStatus = (Protocol.Data.STATUS)value;

					break;
				}

				if( ( Status1 != NewStatus ) || ( Status2 != NewStatus1 ) )
				{
					Status1 = NewStatus;
					Status2 = NewStatus1;

					OnPropertyChanged( nameof( ReceivedByDevice ) );
					OnPropertyChanged( nameof( NotReceivedByDevice ) );
					OnPropertyChanged( nameof( ReadByDriver ) );
					OnPropertyChanged( nameof( PickedUp ) );
					OnPropertyChanged( nameof( Delivered ) );
					OnPropertyChanged( nameof( Status ) );
				}
			}
		}

		[JsonIgnore]
		public bool PickedUp => Status1 == Protocol.Data.STATUS.PICKED_UP;

		[JsonIgnore]
		public bool Delivered => Status1 == Protocol.Data.STATUS.DELIVERED;

		[JsonIgnore]
		public bool PickedUpOrDelivered => PickedUp || Delivered;

		[JsonIgnore]
		public bool HasPackages => !( Packages is null ) && ( Packages.Count > 0 );

		[JsonIgnore]
		public bool Undeliverable => Status2 == STATUS1.UNDELIVERED;


		[JsonIgnore]
		public bool DetailsVisible
		{
			get => _DetailsVisible;
			set
			{
				_DetailsVisible = value;
				OnPropertyChanged();
			}
		}


		[JsonIgnore]
		public new bool ReceivedByDevice
		{
			get => base.ReceivedByDevice && !base.ReadByDriver && !PickedUpOrDelivered;
			set
			{
				if( base.ReceivedByDevice != value )
				{
					base.ReceivedByDevice = value;
					OnPropertyChanged();
					OnPropertyChanged( nameof( NotReceivedByDevice ) );
				}
			}
		}

		[JsonIgnore]
		public bool NotReceivedByDevice
		{
			get => !base.ReceivedByDevice && !base.ReadByDriver && !PickedUpOrDelivered;
			set => throw new NotImplementedException();
		}

		public new bool ReadByDriver
		{
			get => base.ReadByDriver && !PickedUpOrDelivered;
			set
			{
				if( base.ReadByDriver != value )
				{
					base.ReadByDriver = value;
					OnPropertyChanged();
					OnPropertyChanged( nameof( ReceivedByDevice ) );
					OnPropertyChanged( nameof( NotReceivedByDevice ) );
				}
			}
		}


		[JsonIgnore]
		public SolidColorBrush Foreground => _Selected ? SelectedForeground : UnSelectedForeground;

		[JsonIgnore]
		public SolidColorBrush Background => _Selected ? SelectedBackground : UnSelectedBackground;

		private string _FormattedPickupAddress { get; set; }

		[JsonIgnore]
		public string FormattedPickupAddress
		{
			get
			{
				if( _FormattedPickupAddress.IsNullOrEmpty() )
				{
					_FormattedPickupAddress = FormatAddress( PickupCompanyName, PickupAddressSuite, PickupAddressAddressLine1, PickupAddressAddressLine2, PickupAddressVicinity,
					                                         PickupAddressCity, PickupAddressRegion, BillingAddressPostalCode );
				}

				return _FormattedPickupAddress;
			}
		}


		private string _FormattedDeliveryAddress { get; set; }

		[JsonIgnore]
		public string FormattedDeliveryAddress
		{
			get
			{
				if( _FormattedDeliveryAddress.IsNullOrEmpty() )
				{
					_FormattedDeliveryAddress = FormatAddress( DeliveryCompanyName, DeliveryAddressSuite, DeliveryAddressAddressLine1, DeliveryAddressAddressLine2, DeliveryAddressVicinity,
					                                           DeliveryAddressCity, DeliveryAddressRegion, BillingAddressPostalCode );
				}

				return _FormattedDeliveryAddress;
			}
		}


		[JsonIgnore]
		public SolidColorBrush UnSelectedBackground { get; set; }

		[JsonIgnore]
		public SolidColorBrush UnSelectedForeground { get; set; }

		private ObservableCollection<DisplayPackage> _Packages;

		private bool _IsVisible = true;

		private string _Filter;

		private bool _DetailsVisible;


		private bool _EnableAccept;


		private bool _Selected;


		private static string FormatAddress( string companyName, string suite, string addr1, string addr2, string vicinity, string city, string state, string postCode )
		{
			var Address = new StringBuilder();

			void AddLine( string text )
			{
				text = text.Trim();

				if( text.IsNotNullOrWhiteSpace() )
					Address.Append( text ).Append( "\r\n" );
			}

			void Add( string text, string extra = "" )
			{
				if( text.IsNotNullOrWhiteSpace() )
					Address.Append( text.Trim() ).Append( extra );
			}

			AddLine( companyName );

			Add( suite, "/" );
			AddLine( addr1 );
			AddLine( addr2 );
			AddLine( vicinity );
			Add( city, " " );
			Add( state, " " );
			Add( postCode, " " );

			return Address.ToString().Trim();
		}

		public static STATUS ToDisplayTripStatus( Protocol.Data.STATUS status, STATUS1 status1 )
		{
			return status switch
			       {
				       Protocol.Data.STATUS.PICKED_UP when status1 == STATUS1.UNDELIVERED => STATUS.UNPICKUPABLE,
				       Protocol.Data.STATUS.PICKED_UP                                     => STATUS.PICKED_UP,

				       Protocol.Data.STATUS.DELIVERED when status1 == STATUS1.UNDELIVERED => STATUS.UNDELIVERABLE,
				       Protocol.Data.STATUS.DELIVERED                                     => STATUS.DELIVERED,

				       Protocol.Data.STATUS.NEW when status1 == STATUS1.UNDELIVERED => STATUS.UNDELIVERABLE_NEW,
				       Protocol.Data.STATUS.NEW                                     => STATUS.NEW,

				       _ => (STATUS)status
			       };
		}

		[NotifyPropertyChangedInvocator]
		public virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		public DisplayTrip()
		{
			UnSelectedBackground = new SolidColorBrush( Colors.White );
			UnSelectedForeground = new SolidColorBrush( Colors.Black );
		}


		public DisplayTrip( Trip t, ServiceLevel s ) : base( t )
		{
			Status = ToDisplayTripStatus( Status1, Status2 );

			if( ( t.Status3 & ( STATUS2.WAS_PICKED_UP | STATUS2.WAS_UNDELIVERABLE ) ) != 0 )
			{
				UnSelectedBackground = StatusToSolidBrushConvertor.Undeliverable;
				UnSelectedForeground = Brushes.Black;
			}
			else
			{
				UnSelectedForeground = s.Foreground;
				UnSelectedBackground = s.Background;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}