﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Utils;

using DisplayTrip = ViewModels.Trips.Boards.Common.DisplayTrip;

// ReSharper disable InconsistentNaming

namespace ViewModels.Trips.SearchTrips
{
    //public class DisplayTrip : Trip
    public class DisplayTrip : Boards.Common.DisplayTrip
    {
        public new string CallTime => base.CallTime.ShowAsLocalTime(ShowAsLocalTime);
        public new string PickupTime => base.PickupTime.ShowAsLocalTime(ShowAsLocalTime);
        public new string DeliveryTime => base.DeliveryTime.ShowAsLocalTime(ShowAsLocalTime);

        public bool ShowAsLocalTime = true;

        public DisplayTrip(Trip t) : base(t, new Boards.Common.ServiceLevel())
        {
            // Kludge 
            POD = t.POD;
            POP = t.POP;
            BillingAddressPhone = t.BillingAddressPhone;
            BillingAddressNotes = t.BillingAddressNotes;
        }
    }

    public class SearchTripsList : ObservableCollection<DisplayTrip>
	{
		public bool ShowAsLocalTime
		{
			get => _ShowAsLocalTime;
			set
			{
				_ShowAsLocalTime = value;

                foreach (var Trip in this)
                    Trip.ShowAsLocalTime = value;

                CollectionViewSource.GetDefaultView( this ).Refresh();
			}
		}

		private bool _ShowAsLocalTime;

		public new void Add( DisplayTrip t )
		{
            t.ShowAsLocalTime = ShowAsLocalTime;
            base.Add( t );
		}
	}

	/// <summary>
	///     From http://www.albahari.com/nutshell/predicatebuilder.aspx
	/// </summary>

	//public static class PredicateBuilder
	//{
	//    public static Expression<Func<T, bool>> True<T>() { return f => true; }
	//    public static Expression<Func<T, bool>> False<T>() { return f => false; }

	//    public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
	//                                                        Expression<Func<T, bool>> expr2)
	//    {
	//        var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
	//        return Expression.Lambda<Func<T, bool>>
	//              (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
	//    }

	//    public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
	//                                                         Expression<Func<T, bool>> expr2)
	//    {
	//        var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
	//        return Expression.Lambda<Func<T, bool>>
	//              (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
	//    }
	//}
	public class SearchTripsModel : ViewModelBase
	{
		// Used for the filter
		private const string SORT_CALLTIME = "CallTime";
		private const string SORT_ACCOUNT = "AccountId";
		private const string SORT_DRIVER = "Driver";
		private const string SORT_DELIVERY_CITY = "DeliveryCity";
		private const string SORT_DELIVERY_NAME = "DeliveryName";
		private const string SORT_DELIVERY_STATE = "DeliveryState";
		private const string SORT_DELIVERY_STREET = "DeliveryStreet";
		private const string SORT_DELIVERY_TIME = "DeliveryTime";
		private const string SORT_PACKAGE_TYPE = "PackageType";
		private const string SORT_PICKUP_CITY = "PickupCity";
		private const string SORT_PICKUP_COMPANY = "PickupCompany";
		private const string SORT_PICKUP_STATE = "PickupState";
		private const string SORT_PICKUP_STREET = "PickupStreet";
		private const string SORT_PICKUP_TIME = "PickupTime";
		private const string SORT_PIECES = "Pieces";
		private const string SORT_REFERENCE = "Reference";
		private const string SORT_STATUS = "Status";
		private const string SORT_TRIP_ID = "TripId";
		private const string SORT_WEIGHT = "Weight";
		private const string SORT_READYTIME = "ReadyTime";
		private const string SORT_DUETIME = "DueTime";

		private static Dictionary<string, string> dictionaryColumns = new Dictionary<string, string>();

		public bool Loaded
		{
			get { return Get( () => Loaded, false ); }
			set { Set( () => Loaded, value ); }
		}

		[Setting]
		public bool AsLocalTime
		{
			get { return Get( () => AsLocalTime, true ); }
			set { Set( () => AsLocalTime, value ); }
		}

		public SearchTripsList Trips
		{
			get { return Get( () => Trips, new SearchTripsList() ); }
			set { Set( () => Trips, value ); }
		}


		public DisplayTrip SelectedTrip
		{
			get { return Get( () => SelectedTrip, (DisplayTrip)null ); }
			set { Set( () => SelectedTrip, value ); }
		}


		public int TotalTrips
		{
			get { return Get( () => TotalTrips ); }
			set { Set( () => TotalTrips, value ); }
		}

		public string TotalSearchTime
		{
			get { return Get( () => TotalSearchTime, "" ); }
			set { Set( () => TotalSearchTime, value ); }
		}

		public DateTimeOffset FromDate
		{
			get { return Get( () => FromDate, DateTimeOffset.Now ); }
			set { Set( () => FromDate, value ); }
		}

		public DateTimeOffset ToDate
		{
			get { return Get( () => ToDate, DateTimeOffset.Now ); }
			set { Set( () => ToDate, value ); }
		}


		public bool SearchStatusAll
		{
			get { return Get( () => SearchStatusAll, true ); }
			set { Set( () => SearchStatusAll, value ); }
		}

		public bool SearchStatusVerified
		{
			get { return Get( () => SearchStatusVerified, false ); }
			set { Set( () => SearchStatusVerified, value ); }
		}

		public bool SearchStatusPickedUp
		{
			get { return Get( () => SearchStatusPickedUp, false ); }
			set { Set( () => SearchStatusPickedUp, value ); }
		}

		public bool SearchStatusDeleted
		{
			get { return Get( () => SearchStatusDeleted, false ); }
			set { Set( () => SearchStatusDeleted, value ); }
		}

		public bool SearchStatusScheduled
		{
			get { return Get( () => SearchStatusScheduled, false ); }
			set { Set( () => SearchStatusScheduled, value ); }
		}

		public bool SearchByClientReference
		{
			get { return Get( () => SearchByClientReference, false ); }
			set { Set( () => SearchByClientReference, value ); }
		}

		public string ClientReference
		{
			get { return Get( () => ClientReference, "" ); }
			set { Set( () => ClientReference, value ); }
		}

		public bool SearchByTripId
		{
			get { return Get( () => SearchByTripId, false ); }
			set { Set( () => SearchByTripId, value ); }
		}

		public string TripId
		{
			get { return Get( () => TripId, "" ); }
			set { Set( () => TripId, value ); }
		}

		public bool SearchByPackageType
		{
			get { return Get( () => SearchByPackageType, false ); }
			set { Set( () => SearchByPackageType, value ); }
		}


		public List<string> PackageTypes
		{
			get { return Get( () => PackageTypes, new List<string>() ); }
			set { Set( () => PackageTypes, value ); }
		}


		public List<string> SearchPackageTypes

		{
			get { return Get( () => SearchPackageTypes, new List<string>() ); }
			set { Set( () => SearchPackageTypes, value ); }
		}


		public string SelectedPackageType
		{
			get { return Get( () => SelectedPackageType, "" ); }
			set { Set( () => SelectedPackageType, value ); }
		}


		public bool SearchByServiceLevel
		{
			get { return Get( () => SearchByServiceLevel, false ); }
			set { Set( () => SearchByServiceLevel, value ); }
		}


		public string SelectedServiceLevel
		{
			get { return Get( () => SelectedServiceLevel, "" ); }
			set { Set( () => SelectedServiceLevel, value ); }
		}


		public List<string> ServiceLevels
		{
			get { return Get( () => ServiceLevels, new List<string>() ); }
			set { Set( () => ServiceLevels, value ); }
		}


		public List<string> SearchServiceLevels
		{
			get { return Get( () => SearchServiceLevels, new List<string>() ); }
			set { Set( () => SearchServiceLevels, value ); }
		}


		public bool SearchButtonEnable
		{
			get { return Get( () => SearchButtonEnable, true ); }
			set { Set( () => SearchButtonEnable, value ); }
		}

		// Green == true
		public bool GreenRed
		{
			get { return Get( () => GreenRed, true ); }
			set { Set( () => GreenRed, value ); }
		}

		public bool DateEnable
		{
			get { return Get( () => DateEnable, true ); }
			set { Set( () => DateEnable, value ); }
		}


		public IList SelectedItems
		{
			get { return Get( () => SelectedItems ); }
			set { Set( () => SelectedItems, value ); }
		}

        public ObservableCollection<DisplayTrip> SelectedTrips
        {
            get { return Get(() => SelectedTrips, new ObservableCollection<DisplayTrip>()); }
            set { Set(() => SelectedTrips, value); }
        }

        public IList StartStatus
		{
			get { return Get( () => StartStatus, GetPublicStatusStrings() ); }
			set { Set( () => StartStatus, value ); }
		}


		public STATUS SelectedStartStatus
		{
			get { return Get( () => SelectedStartStatus, STATUS.UNSET ); }
			set { Set( () => SelectedStartStatus, value ); }

			//set { Set(() => SelectedStartStatus, value); }
			//set
			//{
			//    SelectedStartStatus = value;
			//    Console.WriteLine("SelectedStartStatus: set to " + SelectedStartStatus);
			//}
		}

		public IList EndStatus
		{
			get { return Get( () => EndStatus, GetPublicStatusStrings() ); }
			set { Set( () => EndStatus, value ); }
		}

		public STATUS SelectedEndStatus
		{
			get { return Get( () => SelectedEndStatus, STATUS.UNSET ); }
			set { Set( () => SelectedEndStatus, value ); }

			//set
			//{
			//    SelectedEndStatus = value;
			//    Console.WriteLine("SelectedEndStatus: set to " + SelectedEndStatus);
			//}
		}

		public IList CompanyNames
		{
			get { return Get( () => CompanyNames, GetCompanyNames() ); }
			set { Set( () => CompanyNames, value ); }
		}


		public bool FilterByCompanyName
		{
			get { return Get( () => FilterByCompanyName, false ); }
			set { Set( () => FilterByCompanyName, value ); }

			//set
			//{
			//    //FilterByCompanyName = value;
			//    FilterByAddress = value;
			//}
		}

		public string SelectedCompanyName
		{
			get { return Get( () => SelectedCompanyName, "" ); }
			set { Set( () => SelectedCompanyName, value ); }
		}


		public bool EnableCsvCopy
		{
			get { return Get( () => EnableCsvCopy ); }
			set { Set( () => EnableCsvCopy, value ); }
		}

		public string TripAdhocFilter
		{
			get { return Get( () => TripAdhocFilter, "" ); }
			set { Set( () => TripAdhocFilter, value ); }
		}


		public bool FilterByAddressEnabled
		{
			get { return Get( () => FilterByAddressEnabled, false ); }
			set { Set( () => FilterByAddressEnabled, value ); }
		}


		public bool FilterByPUAddress
		{
			get { return Get( () => FilterByPUAddress, false ); }
			set { Set( () => FilterByPUAddress, value ); }
		}

		public bool FilterByDeliveryAddress
		{
			get { return Get( () => FilterByDeliveryAddress, false ); }
			set { Set( () => FilterByDeliveryAddress, value ); }
		}

		public IList CompanyAddressNames
		{
			//get { return Get(() => CompanyAddressNames, GetCompanyAddressDescriptions()); }
			get { return Get( () => CompanyAddressNames ); }
			set { Set( () => CompanyAddressNames, value ); }
		}


		public string SelectedCompanyPUAddressName
		{
			get { return Get( () => SelectedCompanyPUAddressName, "" ); }
			set { Set( () => SelectedCompanyPUAddressName, value ); }
		}

		public string SelectedCompanyDelAddressName
		{
			get { return Get( () => SelectedCompanyDelAddressName, "" ); }
			set { Set( () => SelectedCompanyDelAddressName, value ); }
		}

		public string HelpUri
		{
			//get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638222337/search" ); }
			get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/638124054/How+to+Search+Trip"); }
		}

		private readonly List<string> ALL_COLUMNS = new List<string>
		                                            {
			                                            "TripId", "AccountId", "DriverCode", "Reference", "CallTime", "PickupCompanyName", "PickupAddressCity", "PickupAddressAddressLine1",
			                                            "DeliveryCompanyName", "DeliveryAddressCity", "DeliveryAddressAddressLine1", "ServiceLevel", "PackageType", "Pieces",
			                                            "Weight", "ReadyTime", "DueTime", "Status1", "PickupTime", "DeliveryTime", "DeliveryAddressRegion", "DeliveryAddressPostalCode"
		                                            };

		private string CurrentSortName = SORT_CALLTIME;

		private bool CallTimeAscending = true;

		private bool AccountAscending = true;

		private bool DriverAscending = true;

		private bool DeliveryCityAscending = true;

		private bool DeliveryNameAscending = true;

		private bool DeliveryStateAscending = true;


		private bool DeliveryStreetAscending = true;

		private bool DeliveryTimeAscending = true;


		private bool PackageTypeAscending = true;


		private bool PickupCityAscending = true;

		private bool PickupCompanyAscending = true;

		private bool PickupStateAscending = true;

		private bool PickupStreetAscending = true;

		private bool PickupTimeAscending = true;

		private bool PiecesAscending = true;

		private bool ReferenceAscending = true;

		private bool StatusAscending = true;

		private bool TripIdAscending = true;

		private bool WeightAscending = true;

		private bool ReadyTimeAscending = true;

		private bool DueTimeAscending = true;

		//public SearchTripsList UnFilteredTrips
		//{
		//    get { return Get(() => UnFilteredTrips, new SearchTripsList()); }
		//    set { Set(() => UnFilteredTrips, value); }
		//}
		private readonly List<DisplayTrip> UnFilteredTrips = new List<DisplayTrip>();

		//private List<string> ActiveColumns = new List<string>();

		public Dictionary<string, bool> dictionaryActiveColumns = new Dictionary<string, bool>();

		private IList GetPublicStatusStrings()
		{
			var strings = new List<string>
			              {
				              STATUS.UNSET.AsString(),
				              STATUS.NEW.AsString(),
				              STATUS.ACTIVE.AsString(),
				              STATUS.DISPATCHED.AsString(),
				              STATUS.PICKED_UP.AsString(),
				              STATUS.DELIVERED.AsString(),
				              STATUS.VERIFIED.AsString(),
				              STATUS.POSTED.AsString(),
				              STATUS.INVOICED.AsString(),
				              STATUS.SCHEDULED.AsString(),
				              STATUS.PAID.AsString(),
				              STATUS.DELETED.AsString()
			              };

			//var values = Enum.GetValues(typeof(STATUS));
			//for (int i = 0; i < values.Length; i++)
			//{
			// //   strings.Add(TripExtensions.AsString(values[i]));
			//}

			return strings;
		}


		private IList GetCompanyNames()
		{
			//List<string> CompanyNames = new List<string>()
			//{
			//    "co 1",
			//    "co 2",
			//    "co 3",
			//    "co 4",
			//    "co 5"
			//};
			var CompanyNames = new List<string>();

			// CompanyNames = new List<string>();
			Task.Run( () =>
			          {
				          try
				          {
					          // NOTE If this is uncommented, no names appear
					          //Dispatcher.Invoke(() =>
					          //{
					          //    CompanyNames = new List<string>();
					          //});

					          //var Names = Client.RequestGetCustomerCodeList().Result;
					          // From Terry: Need to do this to avoid bad requests in the server log
					          CustomerCodeList Names = null;

					          if( !IsInDesignMode )
						          Names = Azure.Client.RequestGetCustomerCodeList().Result;

					          if( Names != null )
					          {
						          Dispatcher.Invoke( () =>
						                             {
							                             var Sd = new SortedDictionary<string, string>();

							                             foreach( var Name in Names )

								                             //Console.WriteLine("GetCompanyNames: " + Name);
								                             Sd.Add( Name.ToLower(), Name );

							                             foreach( var Key in Sd.Keys )
								                             CompanyNames.Add( Sd[ Key ] );
						                             } );
					          }
				          }
				          catch( Exception e )
				          {
					          //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
					          Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );
					          MessageBox.Show( "Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK );
				          }
			          } );

			return CompanyNames;
		}

		/// <summary>
		///     Looks up company name.
		/// </summary>
		/// <param name="CompanyName"></param>
		/// <returns>Company or null</returns>
		private Company GetCompany( string CompanyName )
		{
			Company company = null;

			Task.Run( () =>
			          {
				          try
				          {
					          var Companies = Azure.Client.RequestGetResellerCustomerCompany( CompanyName ).Result;
				          }
				          catch( Exception e )
				          {
					          // Console.WriteLine("GetCompany: Exception thrown: " + e.ToString());
					          Logging.WriteLogLine( "Exception thrown: " + e, "GetCompany", "SearchTripsModel" );
					          MessageBox.Show( "Error fetching Companies\n" + e, "Error", MessageBoxButton.OK );
				          }
			          } );

			return company;
		}


		private void UpdateTrips( IEnumerable<DisplayTrip> trips )
		{
			Dispatcher.Invoke( () =>
			                   {
				                   Trips.Clear();

				                   foreach( var Trip in trips )
					                   Trips.Add( Trip );
			                   } );
		}

		private void UpdateTrips()
		{
			UpdateTrips( new List<DisplayTrip>( Trips ) );
		}

		/// <summary>
		///     Applies filter to the trips in this.UnFilterdTrips and populates
		///     this.Trips.
		/// </summary>
		/// <param name="filter"></param>
		private void FilterTrips( string filter )
		{
			if( filter == null )
				filter = string.Empty;

			filter = filter.ToLower();
			Logging.WriteLogLine( "Filter: " + filter );

			if( ( filter != null ) && ( filter != string.Empty ) )
			{
				//List<DisplayTrip> filtered = (from T in UnFilteredTrips
				//                              where (T.CallTime.ToLower().Contains(filter)
				//                                  || T.DeliveryAddressCity.ToLower().Contains(filter)
				//                                  || T.DeliveryCompanyName.ToLower().Contains(filter)
				//                                  || T.DeliveryAddressRegion.ToLower().Contains(filter)
				//                                  || T.DeliveryAddressAddressLine1.ToLower().Contains(filter)
				//                                  || T.DeliveryTime.ToLower().Contains(filter)
				//                                  || T.PackageType.ToLower().Contains(filter)
				//                                  || T.PickupAddressCity.ToLower().Contains(filter)
				//                                  || T.PickupCompanyName.ToLower().Contains(filter)
				//                                  || T.PickupAddressRegion.ToLower().Contains(filter)
				//                                  || T.PickupAddressAddressLine1.ToLower().Contains(filter)
				//                                  || T.PickupTime.ToLower().Contains(filter)
				//                                  || T.Pieces.ToString().Contains(filter)
				//                                  || T.Reference.ToLower().Contains(filter)
				//                                  // NOTE the user will be searching for verified, not 6
				//                                  || T.Status1.ToString().ToLower().Contains(filter)
				//                                  || T.TripId.ToLower().Contains(filter)
				//                                  || T.Weight.ToString().Contains(filter)
				//                              )
				//                              orderby T.TripId
				//                              select T).ToList();

				var fieldsToSearch = GetActiveColumns();

				var filtered = new Dictionary<string, DisplayTrip>();

				foreach( var dt in UnFilteredTrips )
				{
					foreach( var field in fieldsToSearch )
					{
						//bool next = false;
						var tmp = string.Empty;

						switch( field )
						{
						case "TripId":
							tmp = dt.TripId.ToLower();

							break;

						case "AccountId":
							tmp = dt.AccountId.ToLower();

							break;

						case "DriverCode":
							tmp = dt.Driver.ToLower();

							break;

						case "Reference":
							tmp = dt.Reference.ToLower();

							break;

						case "CallTime":
							//tmp = dt.CallTime.ToLower();
							tmp = dt.CallTime.ToString().ToLower();

							break;

						case "PickupCompanyName":
							tmp = dt.PickupCompanyName.ToLower();

							break;

						case "PickupAddressCity":
							tmp = dt.PickupAddressCity.ToLower();

							break;

						case "PickupAddressAddressLine1":
							tmp = dt.PickupAddressAddressLine1.ToLower();

							break;

						case "DeliveryCompanyName":
							tmp = dt.DeliveryCompanyName.ToLower();

							break;

						case "DeliveryAddressCity":
							tmp = dt.DeliveryAddressCity.ToLower();

							break;

						case "DeliveryAddressAddressLine1":
							tmp = dt.DeliveryAddressAddressLine1.ToLower();

							break;

						case "ServiceLevel":
							tmp = dt.ServiceLevel.ToLower();

							break;

						case "PackageType":
							tmp = dt.PackageType.ToLower();

							break;

						case "Pieces":
							tmp = dt.Pieces.ToString();

							break;

						case "Weight":
							tmp = dt.Weight.ToString();

							break;

						case "ReadyTime":
							tmp = dt.ReadyTime.ToString();

							break;

						case "DueTime":
							tmp = dt.DueTime.ToString();

							break;

						case "Status1":
							tmp = dt.Status1.ToString().ToLower();

							break;

						case "PickupTime":
							//tmp = dt.PickupTime;
							tmp = dt.PickupTime.ToString();

							break;

						case "DeliveryTime":
							//tmp = dt.DeliveryTime;
							tmp = dt.DeliveryTime.ToString();

							break;

						case "DeliveryAddressRegion":
							tmp = dt.DeliveryAddressRegion.ToLower();

							break;

						case "DeliveryAddressPostalCode":
							tmp = dt.DeliveryAddressPostalCode.ToLower();

							break;
						}

						if( tmp != string.Empty )
						{
							if( tmp.Contains( filter ) )
							{
								var tripId = dt.TripId.ToLower();

								if( !filtered.ContainsKey( tripId ) )
									filtered.Add( tripId, dt );

								break;
							}
						}

						//this.ALL_COLUMNS;
					}
				}

				Trips.Clear();

				foreach( var trip in filtered.Values )
					Trips.Add( trip );
			}
			else
			{
				// Was a filter active?
				if( Trips.Count != UnFilteredTrips.Count )
				{
					//Console.WriteLine("SearchTripsModel.FilterTrips: Clearing filter");
					//Logging.WriteLogLine("Clearing filter", "FilterTrips", "SearchTripsModel");
					Logging.WriteLogLine( "Clearing filter" );

					// Filter is cleared
					Trips.Clear();

					foreach( var dt in UnFilteredTrips )
						Trips.Add( dt );
				}
			}
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();
			Loaded = false;

			Task.Run( () =>
			          {
				          Task.WaitAll( Task.Run( GetServiceLevels ),
				                        Task.Run( GetPackageTypes ),
				                        Task.Run( () =>
				                                  {
					                                  GetCompanyNames();
				                                  } )
				                      );

				          Dispatcher.Invoke( () =>
				                             {
					                             Loaded = true;

					                             //view.Clear();
				                             } );
			          } );
		}


		public void GetPackageTypes()
		{
			var pts = new List<string>();

			if( !IsInDesignMode )
			{
				try
				{
					var Raw = Azure.Client.RequestGetPackageTypes().Result;

					if( Raw != null )
					{
						var PTypes = ( from Pt in Raw
						               orderby Pt.SortOrder, Pt.Description
						               select Pt.Description ).ToList();

						Dispatcher.Invoke( () =>
						                   {
							                   PackageTypes = PTypes;

							                   SearchPackageTypes = PTypes;
							                   SearchPackageTypes.Insert( 0, "" ); // Make the first empty
						                   } );
					}
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown: " + e, "GetCompanyNames", "SearchTripsModel" );

					//MessageBox.Show("Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK);
				}
			}

			//return pts;
		}


		public void GetServiceLevels()
		{
			if( !IsInDesignMode )
			{
				try
				{
					var sls = Azure.Client.RequestGetServiceLevelsDetailed().Result;

					if( sls != null )
					{
						var serviceLevels = ( from S in sls
						                      orderby S.SortOrder, S.OldName
						                      select S.OldName ).ToList();

						Dispatcher.Invoke( () =>
						                   {
							                   ServiceLevels = serviceLevels;

							                   SearchServiceLevels = serviceLevels;
							                   SearchServiceLevels.Insert( 0, "" ); // Make the first empty
						                   } );
					}
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown: " + e );

					//MessageBox.Show("Error fetching Company Names\n" + e, "Error", MessageBoxButton.OK);
				}
			}
		}

		[DependsUpon( nameof( AsLocalTime ) )]
		public void WhenAsLocalTimeChanges()
		{
			Trips.ShowAsLocalTime = AsLocalTime;
		}

		[DependsUpon( nameof( Trips ) )]
		public void WhenTripsChange()
		{
			Trips.ShowAsLocalTime = AsLocalTime;
		}


		[DependsUpon( nameof( StartStatus ) )]
		public void WhenStartStatusChanges()
		{
			Console.WriteLine( "WhenStartStatusChanges" );
		}

		public STATUS GetStatusForString( string label )
		{
			var found = STATUS.UNSET;

			found = (STATUS)Enum.Parse( typeof( STATUS ), label );

			return found;
		}

		[DependsUpon( nameof( SelectedItems ), Delay = 100 )]
		public void WhenSelectedItemsChange()
		{
			EnableCsvCopy = SelectedItems.Count > 0;
		}

        [DependsUpon(nameof(SelectedTrips), Delay = 100)]
        public void WhenSelectedTripsChange()
        {
            EnableCsvCopy = SelectedTrips.Count > 0;
        }

		public IList GetCompanyAddressDescriptions()
		{
			var cads = new List<string>();

			var CompanyName = SelectedCompanyName;

			if( CompanyName != string.Empty )
			{
				// i've turned this off and put the Task.Run into the caller - NOW it works
				//Task.Run(async () =>
				//{
				try
				{
					var result = Azure.Client.RequestGetCustomerCompaniesSummary( CompanyName ).Result;

					//var result = await Client.RequestGetCustomerCompaniesSummary(CompanyName);
					if( ( result != null ) && ( result.Count > 0 ) )
					{
						Logging.WriteLogLine( "Found " + result.Count + " companies for " + CompanyName );

						Dispatcher.Invoke( () =>
						                   {
							                   var sd = new SortedDictionary<string, string>();

							                   // Needs check - throws exception
							                   foreach( var cbas in result )
							                   {
								                   if( !sd.ContainsKey( cbas.CompanyName.ToLower() ) )
									                   sd.Add( cbas.CompanyName.ToLower(), cbas.CompanyName );
							                   }

							                   foreach( var key in sd.Keys )
								                   cads.Add( sd[ key ] );

							                   //this.CompanyAddressDescriptions = cads;
							                   //Utils.Logging.WriteLogLine("this.CompanyAddressNames.Count: " + this.CompanyAddressNames.Count);
						                   } );
					}

					//CompanyByAccountAndCompanyNameList list = new CompanyByAccountAndCompanyNameList();
					//CompanyByAccountAndCompanyName tmp = new CompanyByAccountAndCompanyName
					//{
					//    CompanyName = CompanyName,
					//    CustomerCode = CompanyName
					//};
					//list.Add(tmp);

					//var result1 = Client.RequestGetCustomerCompanyAddressList(list).Result;
					//if (result1 != null)
					//{

					//}
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown fetching Company Addresses: " + e );
				}

				//});
			}

			return cads;
		}

		[DependsUpon( nameof( FromDate ) )]
		public void WhenFromDateChanges()
		{
			if( FromDate > ToDate )
				ToDate = FromDate;
		}

		[DependsUpon( nameof( ToDate ) )]
		public void WhenFromToChanges()
		{
			if( ToDate < FromDate )
				FromDate = ToDate;
		}


		[DependsUpon( nameof( SearchByClientReference ) )]
		[DependsUpon( nameof( SearchByTripId ) )]
		[DependsUpon( nameof( ClientReference ), Delay = 250 )]
		[DependsUpon( nameof( TripId ), Delay = 250 )]
		public void EnableSearchButton()
		{
			if( SearchByClientReference || SearchByTripId )
				SearchButtonEnable = !string.IsNullOrEmpty( ClientReference.Trim() ) || !string.IsNullOrEmpty( TripId.Trim() );
			else
				SearchButtonEnable = true;
		}

		[DependsUpon( nameof( SearchByClientReference ) )]
		public void WhenSearchByClientReferenceChanges()
		{
			if( SearchByClientReference )
				SearchByTripId = false;
		}


		[DependsUpon( nameof( SearchByTripId ) )]
		public void WhenSearchByTripIdChanges()
		{
			if( SearchByTripId )
				SearchByClientReference = false;
		}


		[DependsUpon( nameof( SearchByClientReference ) )]
		[DependsUpon( nameof( SearchByTripId ) )]
		public void NonDateSearch()
		{
			DateEnable = !SearchByClientReference && !SearchByTripId;
		}


		/// <summary>
		///     Replaces changed trips in Trips.
		///     Saves the changed trips to the db.
		/// </summary>
		/// <param name="changed"></param>
		public void UpdateChangedTripsAsync(List<DisplayTrip> changed)
		//public Task UpdateChangedTripsAsync(ObservableCollection<DisplayTrip> changed)
		//public async Task UpdateChangedTripsAsync(ObservableCollection<DisplayTrip> changed)
		{
			// Update the UI
			//foreach( var trip in changed )
			//{
			//	var toDo = new Dictionary<int, DisplayTrip>();

			//	for( var i = 0; i < Trips.Count; i++ )
			//	{
			//		var dt = Trips[ i ];

			//		if( dt.TripId == trip.TripId )
			//		{
			//			Logging.WriteLogLine("trip.Status: " + trip.Status);
			//			toDo.Add( i, trip );

			//			break;
			//		}
			//	}

			//	foreach( int pos in toDo.Keys )
			//	{
			//		//Dispatcher.Invoke(() =>
			//		//{
			//		//	try
			//		//	{
			//		//		int at = pos;
			//		//		Logging.WriteLogLine("at: " + at);
			//		//		Trips.RemoveAt( at );
			//		//		Trips.Insert( at, trip );
			//		//		Logging.WriteLogLine("trip.Status: " + Trips[at].Status);
			//		//	}
			//		//	catch (Exception e)
			//		//	{
			//		//		Logging.WriteLogLine("Exception: " + e.ToString());
			//		//	}

			//		//});
			//		try
			//		{
			//			int at = pos;
			//			Logging.WriteLogLine("at: " + at);
			//			Trips.RemoveAt(at);
			//			Trips.Insert(at, trip);
			//			Logging.WriteLogLine("trip.Status: " + Trips[at].Status);
			//		}
			//		catch (Exception e)
			//		{
			//			Logging.WriteLogLine("Exception: " + e.ToString());
			//		}
			//	}
			//}

			// Save the trips

			try
			{
				Logging.WriteLogLine( "Updating database..." );
				Task.Run(async () =>
				{
					var tul = new TripUpdateList();
					tul.Trips.AddRange(changed);
					await Azure.Client.RequestAddUpdateTrips(tul);
				});
				//Dispatcher.Invoke(() =>
				//{
				//	Task.Run(async () =>
				//	{
				//		var tul = new TripUpdateList();
				//		tul.Trips.AddRange(changed);
				//		await Azure.Client.RequestAddUpdateTrips(tul);
				//	});
				//	//_ = Azure.Client.RequestAddUpdateTrips(tul);
				//});
				//await Azure.Client.RequestAddUpdateTrips(tul);
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown while updating trips: " + e );
			}
			Logging.WriteLogLine("Updated database");
		}

		/// <summary>
		///     If ActiveColumns is empty, loads ALL_COLUMNS
		/// </summary>
		/// <returns></returns>
		public List<string> GetActiveColumns()
		{
			//if (this.ActiveColumns.Count == 0) {
			//    this.ActiveColumns = this.ALL_COLUMNS;

			//    // TESTING remove 1
			//    //this.ActiveColumns.RemoveAt(this.ActiveColumns.Count - 1);
			//}            
			//return this.ActiveColumns;
			var activeCols = new List<string>();

			if( dictionaryActiveColumns.Count == 0 )
			{
				foreach( var tmp in ALL_COLUMNS )
					dictionaryActiveColumns.Add( tmp, true );
			}

			foreach( var key in dictionaryActiveColumns.Keys )
			{
				if( dictionaryActiveColumns[ key ] )
					activeCols.Add( key );
			}

			return activeCols;
		}

		public void UpdateActiveColumns( string column )
		{
			if( !string.IsNullOrEmpty( column ) && dictionaryActiveColumns.ContainsKey( column ) )
			{
				Logging.WriteLogLine( "Toggling " + column, "UpdateActiveColumns", "SearchTripsModel" );
				dictionaryActiveColumns[ column ] = !dictionaryActiveColumns[ column ];
			}
		}

		/// <summary>
		///     Matches the column header to the field.
		///     TODO These will have to be updated when the missing fields
		///     are added to the table.
		/// </summary>
		/// <param name="column"></param>
		/// <returns></returns>
		public string MapColumnToTripField( string column )
		{
			var fieldName = string.Empty;

			if( ( dictionaryColumns == null ) || ( dictionaryColumns.Count == 0 ) )
			{
				dictionaryColumns = new Dictionary<string, string>
				                    {
					                    { (string)Application.Current.TryFindResource( "SearchTripsListTripId" ), "TripId" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListAccount" ), "AccountId" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDriver" ), "DriverCode" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListClientReference" ), "Reference" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListCallTime" ), "CallTime" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPickupName" ), "PickupCompanyName" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPickupCity" ), "PickupAddressCity" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPickupStreet" ), "PickupAddressAddressLine1" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryName" ), "DeliveryCompanyName" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryCity" ), "DeliveryAddressCity" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryStreet" ), "DeliveryAddressAddressLine1" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListServiceLevel" ), "ServiceLevel" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPackageType" ), "PackageType" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPieces" ), "Pieces" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListWeight" ), "Weight" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListReadyTime" ), "ReadyTime" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDueTime" ), "DueTime" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListStatus" ), "Status1" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListPickupTime" ), "PickupTime" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryTime" ), "DeliveryTime" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryState" ), "DeliveryAddressRegion" },
					                    { (string)Application.Current.TryFindResource( "SearchTripsListDeliveryZip" ), "DeliveryAddressPostalCode" }
				                    };
			}

			if( !string.IsNullOrEmpty( column ) )
				fieldName = dictionaryColumns[ column ];

			return fieldName;
		}

	#region CSV / Clipboard

		public string CsvHeader
        {
            get { return Get( () => CsvHeader, header ); }            
            set { Set(() => CsvHeader, value); }
        }

        private readonly string header = "ShipmentId, CallTime, PickupCompanyName, PickupAddressStreet, PickupAddressCity, PickupAddressRegion, "
                        + "DeliveryCompanyName, DeliveryAddressStreet, DeliveryAddressCity, DeliveryAddressRegion, "
                        + "Pieces, Weight, PackageType, Reference, PickupTime, DeliveryTime, Status";

        private void CopyCsvToClipboard( string header )
		{
			var Text = new StringBuilder( header );

			//foreach( Trip T in SelectedItems )
			foreach( Trip T in SelectedTrips )
			{
				Text.Append( $"{T.TripId},{T.CallTime.ToTzDateTime()},{T.PickupCompanyName},{T.PickupAddressAddressLine1},{T.PickupAddressCity},{T.PickupAddressRegion},"
				             + $"{T.DeliveryCompanyName},{T.DeliveryAddressAddressLine1},{T.DeliveryAddressCity},{T.DeliveryAddressRegion},"
				             + $"{T.Pieces},{T.Weight},{T.PackageType},{T.Reference},{T.PickupTime.ToTzDateTime()},{T.DeliveryTime.ToTzDateTime()},{T.Status1.AsString()}\r\n"
				           );
			}

			Clipboard.SetText( Text.ToString().TrimEnd() );
		}

		public void Execute_CopyCsvToClipboard()
		{
			CopyCsvToClipboard( "" );
		}

		public void Execute_CopyCsvToClipboardWithHeader()
		{
			CopyCsvToClipboard( $"{CsvHeader}\r\n" );
		}

		public void Execute_CopyTripIdToClipboard()
		{
			//var Items = SelectedItems;
			var Items = SelectedTrips;

			if( Items.Count > 0 )
			{
				var Id = ( (Trip)Items[ 0 ] ).TripId.Trim();
				Clipboard.SetText( Id );
			}
		}

	#endregion

	#region Execution

        public void Execute_SearchTrips()
		{
			SearchButtonEnable = false;
			GreenRed = false;
			TotalSearchTime = "";
			var Start = DateTime.Now;

			Task.Run( () =>
			          {
				          try
				          {
					          var Result = new SearchTripsList();

					          Dispatcher.Invoke( () =>
					                             {
						                             Trips = Result;
						                             TotalTrips = 0;
					                             } );

					          //var Fd = FromDate;
					          //var Td = ToDate;
					          var Fd = Util.GetStartOfDay( FromDate );
					          var Td = Util.GetEndOfDay( ToDate );

					          var FromStatus = SelectedStartStatus;
					          var ToStatus = SelectedEndStatus;

					          var PackageType = SelectedPackageType;
					          var ServiceLevel = SelectedServiceLevel;

					          var SelectedCompany = string.Empty;
					          var SelectedPUCompanyAddress = string.Empty;
					          var SelectedDelCompanyAddress = string.Empty;

					          var message = "Searching for trips from " + Fd + " to " + Td
					                        + ", FromStatus: " + FromStatus + ", ToStatus: " + ToStatus;

							  if (SearchByTripId)
							  {
								  message += ", TripId: " + TripId;
							  }
							  if (SearchByClientReference)
							  {
								  message += ", Reference: " + ClientReference;
							  }

					          if( FilterByCompanyName )
					          {
						          SelectedCompany = SelectedCompanyName;
						          message += ", SelectedCompany: " + SelectedCompany;

						          if( FilterByPUAddress && SelectedPUCompanyAddress.IsNotNullOrEmpty() )
						          {
							          SelectedPUCompanyAddress = SelectedCompanyPUAddressName;
							          message += ", SelectedCompanyPUAddressName: " + SelectedPUCompanyAddress;
						          }

						          if( FilterByDeliveryAddress && SelectedDelCompanyAddress.IsNotNullOrEmpty() )
						          {
							          SelectedDelCompanyAddress = SelectedCompanyDelAddressName;
							          message += ", SelectedCompanyDelAddressName: " + SelectedDelCompanyAddress;
						          }
					          }

					          Logging.WriteLogLine( message );

					          //	var DiffDays = ( Td - Fd ).TotalDays;
					          //	for( var I = 0; I < DiffDays; I++ )
					          //	{
					          //	Td = Fd.AddDays( 1 );

					          var Trps = Azure.Client.RequestSearchTrips( new Protocol.Data.SearchTrips
					                                                      {
						                                                      FromDate = Fd,
						                                                      ToDate = Td,
						                                                      TripId = TripId,
						                                                      ByReference = SearchByClientReference,
						                                                      ByTripId = SearchByTripId,
						                                                      Reference = ClientReference,
						                                                      StartStatus = FromStatus,
						                                                      EndStatus = ToStatus,
						                                                      CompanyCode = SelectedCompany,
						                                                      SelectedCompanyPUAddressName = SelectedPUCompanyAddress,
						                                                      SelectedCompanyDelAddressName = SelectedDelCompanyAddress,
						                                                      PackageType = PackageType,
						                                                      ServiceLevel = ServiceLevel,
																			  IncludeCharges = true																			  
					                                                      } ).Result;

					          Dispatcher.Invoke( () =>
					                             {
						                             UnFilteredTrips.Clear();

						                             foreach( var Trip in Trps )
						                             {
                                                         var dt = new DisplayTrip(Trip);
                                                         //var dt = new DisplayTrip( Trip, new Boards.Common.ServiceLevel() );
                                                         Result.Add( dt );
							                             UnFilteredTrips.Add( dt );
						                             }

						                             TotalTrips = Result.Count;

						                             var Diff = DateTime.Now - Start;
						                             const string FMT = @"hh\:mm\:ss";

						                             TotalSearchTime = $" ({Diff.ToString( FMT )})";

						                             Logging.WriteLogLine( "Found " + TotalTrips + " trips in " + TotalSearchTime );
					                             } );

					          //	Fd = Fd.AddDays( 1 );
					          //  }
				          }
				          catch
				          {
					          MessageBox.Show( "Error fetching trips\r\rTry a smaller date range", "Error", MessageBoxButton.OK );
				          }
				          finally
				          {
					          Dispatcher.Invoke( () =>
					                             {
						                             SearchButtonEnable = true;
						                             GreenRed = true;
					                             } );
				          }
			          } );
		}


		public void Execute_AuditTrip()
		{
			var Trip = SelectedTrip;

			if( !( Trip is null ) )
				Globals.RunProgram( Globals.TRIP_AUDIT_TRAIL, Trip.TripId );
		}

		public void Execute_SortByTripId( string filter = "" )
		{
			CurrentSortName = SORT_TRIP_ID;
			TripIdAscending = !TripIdAscending;
			FilterTrips( filter );

			if( TripIdAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.TripId
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.TripId descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByAccount( string filter = "" )
		{
			CurrentSortName = SORT_ACCOUNT;
			AccountAscending = !AccountAscending;
			FilterTrips( filter );

			if( AccountAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.AccountId
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.AccountId descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDriver( string filter = "" )
		{
			CurrentSortName = SORT_DRIVER;
			DriverAscending = !DriverAscending;
			FilterTrips( filter );

			if( DriverAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Driver
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Driver descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByCallTime( string filter = "" )
		{
			CurrentSortName = SORT_CALLTIME;
			CallTimeAscending = !CallTimeAscending;
			FilterTrips( filter );

			if( CallTimeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.CallTime
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.CallTime descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPickupCompany( string filter = "" )
		{
			CurrentSortName = SORT_PICKUP_COMPANY;
			PickupCompanyAscending = !PickupCompanyAscending;
			FilterTrips( filter );

			if( PickupCompanyAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupCompanyName
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupCompanyName descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPickupStreet( string filter = "" )
		{
			CurrentSortName = SORT_PICKUP_STREET;
			PickupStreetAscending = !PickupStreetAscending;
			FilterTrips( filter );

			if( PickupStreetAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressAddressLine1
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressAddressLine1 descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPickupCity( string filter = "" )
		{
			CurrentSortName = SORT_PICKUP_CITY;
			PickupCityAscending = !PickupCityAscending;
			FilterTrips( filter );

			if( PickupCityAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressCity
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressCity descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPickupState( string filter = "" )
		{
			CurrentSortName = SORT_PICKUP_STATE;
			PickupStateAscending = !PickupStateAscending;
			FilterTrips( filter );

			if( PickupStateAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressRegion
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupAddressRegion descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDeliveryName( string filter = "" )
		{
			CurrentSortName = SORT_DELIVERY_NAME;
			DeliveryNameAscending = !DeliveryNameAscending;
			FilterTrips( filter );

			if( DeliveryNameAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryCompanyName
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryCompanyName descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDeliveryStreet( string filter = "" )
		{
			CurrentSortName = SORT_DELIVERY_STREET;
			DeliveryStreetAscending = !DeliveryStreetAscending;
			FilterTrips( filter );

			if( DeliveryStreetAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressAddressLine1
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressAddressLine1 descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDeliveryCity( string filter = "" )
		{
			CurrentSortName = SORT_DELIVERY_CITY;
			DeliveryCityAscending = !DeliveryCityAscending;
			FilterTrips( filter );

			if( DeliveryCityAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressCity
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressCity descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDeliveryState( string filter = "" )
		{
			CurrentSortName = SORT_DELIVERY_STATE;
			DeliveryStateAscending = !DeliveryStateAscending;
			FilterTrips( filter );

			if( DeliveryStateAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressRegion
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryAddressRegion descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPieces( string filter = "" )
		{
			CurrentSortName = SORT_PIECES;
			PiecesAscending = !PiecesAscending;
			FilterTrips( filter );

			if( PiecesAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Pieces
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Pieces descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByWeight( string filter = "" )
		{
			CurrentSortName = SORT_WEIGHT;
			WeightAscending = !WeightAscending;
			FilterTrips( filter );

			if( WeightAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Weight
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Weight descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPackageType( string filter = "" )
		{
			CurrentSortName = SORT_PACKAGE_TYPE;
			PackageTypeAscending = !PackageTypeAscending;
			FilterTrips( filter );

			if( PackageTypeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PackageType
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PackageType descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByReference( string filter = "" )
		{
			CurrentSortName = SORT_REFERENCE;
			ReferenceAscending = !ReferenceAscending;
			FilterTrips( filter );

			if( ReferenceAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Reference
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Reference descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByPickupTime( string filter = "" )
		{
			CurrentSortName = SORT_PICKUP_TIME;
			PickupTimeAscending = !PickupTimeAscending;
			FilterTrips( filter );

			if( PickupTimeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupTime
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.PickupTime descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDeliveryTime( string filter = "" )
		{
			CurrentSortName = SORT_DELIVERY_TIME;
			DeliveryTimeAscending = !DeliveryTimeAscending;
			FilterTrips( filter );

			if( DeliveryTimeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryTime
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DeliveryTime descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByStatus( string filter = "" )
		{
			CurrentSortName = SORT_STATUS;
			StatusAscending = !StatusAscending;
			FilterTrips( filter );

			if( StatusAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Status1
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.Status1 descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByReadyTime( string filter = "" )
		{
			CurrentSortName = SORT_STATUS;
			ReadyTimeAscending = !ReadyTimeAscending;
			FilterTrips( filter );

			if( ReadyTimeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.ReadyTime
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.ReadyTime descending
				               select T ).ToList() );
			}
		}

		public void Execute_SortByDueTime( string filter = "" )
		{
			CurrentSortName = SORT_DUETIME;
			DueTimeAscending = !DueTimeAscending;
			FilterTrips( filter );

			if( DueTimeAscending )
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DueTime
				               select T ).ToList() );
			}
			else
			{
				UpdateTrips( ( from T in Trips
				               orderby T.DueTime descending
				               select T ).ToList() );
			}
		}

		/// <summary>
		///     Problem - need to reapply the sorting.
		/// </summary>
		/// <param name="filter"></param>
		public void Execute_Filter( string filter )
		{
			TripAdhocFilter = filter;

			if( filter != string.Empty )
				filter = filter.ToLower();

			switch( CurrentSortName )
			{
			// Keep the current sort direction by toggling the flag before calling the method
			case SORT_CALLTIME:
				CallTimeAscending = !CallTimeAscending;
				Execute_SortByCallTime( filter );

				break;

			case SORT_DELIVERY_CITY:
				DeliveryCityAscending = !DeliveryCityAscending;
				Execute_SortByDeliveryCity( filter );

				break;

			case SORT_DELIVERY_NAME:
				DeliveryNameAscending = !DeliveryNameAscending;
				Execute_SortByDeliveryName( filter );

				break;

			case SORT_DELIVERY_STATE:
				DeliveryStateAscending = !DeliveryStateAscending;
				Execute_SortByDeliveryState( filter );

				break;

			case SORT_DELIVERY_STREET:
				DeliveryStreetAscending = !DeliveryStreetAscending;
				Execute_SortByDeliveryStreet( filter );

				break;

			case SORT_DELIVERY_TIME:
				DeliveryTimeAscending = !DeliveryTimeAscending;
				Execute_SortByDeliveryTime( filter );

				break;

			case SORT_PACKAGE_TYPE:
				PackageTypeAscending = !PackageTypeAscending;
				Execute_SortByPackageType( filter );

				break;

			case SORT_PICKUP_CITY:
				PickupCityAscending = !PickupCityAscending;
				Execute_SortByPickupCity( filter );

				break;

			case SORT_PICKUP_COMPANY:
				PickupCompanyAscending = !PickupCompanyAscending;
				Execute_SortByPickupCompany( filter );

				break;

			case SORT_PICKUP_STATE:
				PickupStateAscending = !PickupStateAscending;
				Execute_SortByPickupState( filter );

				break;

			case SORT_PICKUP_STREET:
				PickupStreetAscending = !PickupStreetAscending;
				Execute_SortByPickupStreet( filter );

				break;

			case SORT_PICKUP_TIME:
				PickupTimeAscending = !PickupTimeAscending;
				Execute_SortByPickupTime( filter );

				break;

			case SORT_PIECES:
				PiecesAscending = !PiecesAscending;
				Execute_SortByPieces( filter );

				break;

			case SORT_REFERENCE:
				ReferenceAscending = !ReferenceAscending;
				Execute_SortByReference( filter );

				break;

			case SORT_STATUS:
				StatusAscending = !StatusAscending;
				Execute_SortByStatus( filter );

				break;

			case SORT_TRIP_ID:
				TripIdAscending = !TripIdAscending;
				Execute_SortByTripId( filter );

				break;

			case SORT_WEIGHT:
				WeightAscending = !WeightAscending;
				Execute_SortByWeight( filter );

				break;
			}
		}

		public void Execute_ClearSearchTrips()
		{
			Logging.WriteLogLine( "Clearing Search Trips" );

			Trips.Clear();
			UnFilteredTrips.Clear();
			SearchByClientReference = false;
			ClientReference = string.Empty;
			SearchByTripId = false;
			TripId = string.Empty;

			SelectedPackageType = string.Empty;
			SelectedServiceLevel = string.Empty;

			FilterByCompanyName = false;
			FilterByPUAddress = false;
			FilterByDeliveryAddress = false;
			FilterByAddressEnabled = false;

			//SelectedCompanyName = string.Empty;
			TotalSearchTime = string.Empty;
			TotalTrips = 0;

			FromDate = DateTime.Now;
			ToDate = DateTime.Now;

			TripAdhocFilter = string.Empty;

			//FilterByAddress = false;
		}

		//public Dictionary<string, bool> Execute_SelectVisibleColumns()
		//{
		//    Dictionary<string, bool> mis = new Dictionary<string, bool>();
		//    Logging.WriteLogLine("Opening Select Visible Columns");

		//    foreach (string column in this.ALL_COLUMNS)
		//    {
		//        mis.Add(column, true);
		//        if (!this.ActiveColumns.Contains(column))
		//        {
		//            mis[column] = false;
		//        }
		//    }

		//    //foreach (string column in this.ALL_COLUMNS)
		//    //{
		//    //    System.Windows.Controls.MenuItem newMi = new System.Windows.Controls.MenuItem
		//    //    {
		//    //        Header = column,
		//    //        IsCheckable = true
		//    //    };
		//    //    newMi.IsChecked = this.ActiveColumns.Contains(column);
		//    //}


		//    return mis;
		//}

		public void Execute_EditSelectedTrips()
		{
		}

		public void ToggleActiveColumn( string column )
		{
			foreach( var tmp in ALL_COLUMNS )
			{
			}
		}

		public void UpdateTripsWithLatestVersion( DisplayTrip newVersion )
		{
			if( newVersion != null )
			{
				var pos = -1;

				for( var i = 0; i < UnFilteredTrips.Count; i++ )
				{
					if( UnFilteredTrips[ i ].TripId == newVersion.TripId )
					{
						pos = i;

						break;
					}
				}

				if( pos > -1 )
				{
					UnFilteredTrips.RemoveAt( pos );
					UnFilteredTrips.Insert( pos, newVersion );
				}
			}
		}

	#endregion
	}
}