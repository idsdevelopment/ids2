﻿using AzureRemoteService;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.Utils;
using IdsRemoteServiceControlLibraryV2.Drivers;
using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Utils;

using DisplayTrip = ViewModels.Trips.Boards.Common.DisplayTrip;

namespace ViewModels.Trips.SearchTrips
{
	/// <summary>
	///     Interaction logic for SearchTrips.xaml
	/// </summary>
	public partial class SearchTrips : Page
	{
        private readonly SearchTripsModel Model;
        //private Boards.Common.DisplayTrip CurrentTrip;
        //private bool WasKeyPress;

        public SearchTrips()
		{
			InitializeComponent();

            if( DataContext is SearchTripsModel )
            {
                Model = (SearchTripsModel)DataContext;
            }

			this.ApplyActiveColumns( true );
		}

		/// <summary>
		/// Hides any columns that aren't in the list.
		/// </summary>
		private void ApplyActiveColumns( bool updateContextMenu = false )
		{
			if( DataContext is SearchTripsModel Model )
			{
                // TODO
				//if( this.TripListView.View is GridView gridView )
				//{
				//	GridViewColumnCollection cols = gridView.Columns;
				//	List<string> activeColumns = Model.GetActiveColumns();

				//	foreach( GridViewColumn col in cols )
				//	{
				//		//if (col.Header is Button header)
				//		if( col.Header is GridViewColumnHeader header )
				//		{
				//			string tmp = (string)header.Content;
				//			string column = Model.MapColumnToTripField( tmp );
				//			if( column != string.Empty )
				//			{
				//				if( !activeColumns.Contains( column ) )
				//				{
				//					col.Width = 0;
				//				}
				//				else
				//				{
				//					// Tells the program to treat the width as AUTO
				//					col.Width = double.NaN;
				//				}
				//			}
				//		}
				//	}
				//}

				//if( updateContextMenu )
				//{
				//	// Build the context menu
				//	if( this.TripListView.ContextMenu is ContextMenu menu )
				//	{
				//		// Application.Current.TryFindResource("SearchTripsSelectVisibleColumns")
				//		//int index = menu.Items.IndexOf(System.Windows.Application.Current.TryFindResource("SearchTripsSelectVisibleColumns"));
				//		int index = -1;
				//		for( int i = 0; i < menu.Items.Count; i++ )
				//		{
				//			if( menu.Items[ i ] is MenuItem mi )
				//			{
				//				if( mi.Header == System.Windows.Application.Current.TryFindResource( "SearchTripsSelectVisibleColumns" ) )
				//				{
				//					index = i;
				//					break;
				//				}
				//			}
				//		}

				//		if( index > -1 )
				//		{
				//			MenuItem parent = (MenuItem)menu.Items[ index ];
				//			parent.Items.Clear();
				//			// Attach all Columns with active ones checked
				//			//Dictionary<string, bool> columns = Model.Execute_SelectVisibleColumns();
				//			Dictionary<string, bool> columns = Model.dictionaryActiveColumns;
				//			if( columns != null && columns.Count > 0 )
				//			{
				//				foreach( string key in columns.Keys )
				//				{
				//					MenuItem newMi = new MenuItem
				//					                 {
				//						                 Header = key,
				//						                 IsCheckable = true,
				//						                 IsChecked = columns[ key ]
				//						                 //Command = Model.ToggleActiveColumn(key)
				//					                 };
				//					newMi.Click += new RoutedEventHandler( MenuItem_Click );
				//					parent.Items.Add( newMi );
				//				}
				//			}
				//		}
				//	}
				//}
			}
		}

        // TODO
		//public void MenuItem_Click( object sender, RoutedEventArgs e )
		//{
		//	if( sender is MenuItem mi && DataContext is SearchTripsModel Model )
		//	{
		//		string column = (string)mi.Header;
		//		Model.UpdateActiveColumns( column );

		//		// Update the columns in the listview
		//		this.ApplyActiveColumns();
		//	}
		//}

		private void ListView_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( sender is ListView ListView && DataContext is SearchTripsModel Model )
			{
				Model.SelectedItems = ListView.SelectedItems;
				if( ListView.View is GridView GridView )
				{
					var Text = new StringBuilder();
					var Cols = GridView.Columns;
					var C = Cols.Count;
					var First = true;

					for( var I = 0; I < C; I++ )
					{
						if( !First )
							Text.Append( ',' );
						else
							First = false;

						var Header = Cols[ I ].Header;
						if( Header is Button Button )
							Text.Append( Button.Content );
						else
							Text.Append( Header );
					}

					Model.CsvHeader = Text.ToString();
				}
			}
		}

		/// <summary>
		/// Start Status
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ComboBox_SelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			if( sender is ComboBox cb && DataContext is SearchTripsModel Model )
			{
				if( cb.SelectedIndex > -1 )
				{
					string label = (string)cb.SelectedItem;
					Protocol.Data.STATUS status = (Protocol.Data.STATUS)System.Enum.Parse( typeof( Protocol.Data.STATUS ), label );
					Model.SelectedStartStatus = status;
				}
			}
		}

		/// <summary>
		/// End Status.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ComboBox_SelectionChanged_1( object sender, SelectionChangedEventArgs e )
		{
			if( sender is ComboBox cb && DataContext is SearchTripsModel Model )
			{
				if( cb.SelectedIndex > -1 )
				{
					string label = (string)cb.SelectedItem;
					Protocol.Data.STATUS status = (Protocol.Data.STATUS)System.Enum.Parse( typeof( Protocol.Data.STATUS ), label );
					Model.SelectedEndStatus = status;
				}
			}
		}

		/// <summary>
		/// Selected Company name.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public async void ComboBox_SelectionChanged_2Async( object sender, SelectionChangedEventArgs e )
		{
			if( sender is ComboBox cb && DataContext is SearchTripsModel Model )
			{
				if( cb.SelectedIndex > -1 )
				{
					string companyName = (string)cb.SelectedItem;
					Model.SelectedCompanyName = companyName;
					//Model.CompanyAddressNames.Clear();

					// Testing
					//var tmp = Model.GetCompanyAddressDescriptions();

					// This finally works
					var tmp = await Task.Run( () => Model.GetCompanyAddressDescriptions() );

					Model.CompanyAddressNames = tmp;

					if( tmp != null && tmp.Count > 0 )
					{
						this.comboPUAddress.SelectedIndex = 0;
						this.comboDelAddress.SelectedIndex = 0;
					}
				}
			}
		}

		/// <summary>
		/// Filter box.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TextBox_TextChanged( object sender, TextChangedEventArgs e )
		{
			if( sender is TextBox tb && DataContext is SearchTripsModel Model )
			{
				Model.Execute_Filter( tb.Text.Trim() );
			}
		}

		private void MenuItem_ContextMenuOpening( object sender, ContextMenuEventArgs e )
		{
			if( sender is MenuItem mi && DataContext is SearchTripsModel Model )
			{
				// Attach all Columns with active ones checked
				//Dictionary<string, bool> columns = Model.Execute_SelectVisibleColumns();
				//if (columns != null && columns.Count > 0)
				//{
				//    foreach (string key in columns.Keys)
				//    {
				//        MenuItem newMi = new MenuItem
				//        {
				//            Header = key,
				//            IsCheckable = true,
				//            IsChecked = columns[key]
				//        };
				//        mi.Items.Add(newMi);
				//    }
				//}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static List<string> GetActiveColumnsInOrder()
		{
			List<string> acs = new List<string>();

            // TODO
			//if( this.TripListView.View is GridView gridView )
			//{
			//	GridViewColumnCollection cols = gridView.Columns;
			//	foreach( GridViewColumn gvc in cols )
			//	{
			//		acs.Add( (string)gvc.Header );
			//	}
			//}

			return acs;
		}

		//private void TextBox_MouseDoubleClick( object sender, System.Windows.Input.MouseButtonEventArgs e )
		//{
		//	TextBox tb = (TextBox)sender;
		//	if( tb.IsReadOnly )
		//	{
		//		tb.IsReadOnly = false;
		//	}
		//	else
		//	{
		//		tb.IsReadOnly = true;
		//	}
		//}

		private void TripListView_MouseDoubleClick( object sender, System.Windows.Input.MouseButtonEventArgs e )
		{
            DisplayTrip selectedTrip = (DisplayTrip)this.DataGrid.SelectedItem;
			if( selectedTrip != null )
			{
				Logging.WriteLogLine( "Selected trip: " + selectedTrip.TripId );

                DisplayTrip latestVersion = GetLatestVersion(selectedTrip.TripId);
                // TODO Checking for TripId in case the trip is in Storage
                if (latestVersion.TripId.IsNotNullOrWhiteSpace() && latestVersion != null && selectedTrip.LastModified.ToLongTimeString() != latestVersion.LastModified.ToLongTimeString())
                {
                    Logging.WriteLogLine("Latest version - was : " + selectedTrip.LastModified.ToLongTimeString() + ", now: " + latestVersion.LastModified.ToLongTimeString());
                    selectedTrip = latestVersion;
                    if (DataContext is SearchTripsModel Model)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            Model.UpdateTripsWithLatestVersion(selectedTrip);
                        });
                    }
                }

                var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
				if( tis != null && tis.Count > 0 )
				{
					PageTabItem pti = null;
					for( int i = 0; i < tis.Count; i++ )
					{
						var ti = tis[ i ];
						if( ti.Content is TripEntry.TripEntry )
						{
							pti = ti;
							// Load the trip into the first TripEntry tab
							Logging.WriteLogLine( "Displaying trip in extant " + Globals.TRIP_ENTRY + " tab" );

							//( (TripEntry.TripEntryModel)pti.Content.DataContext ).Execute_ClearTripEntry();
							( (TripEntry.TripEntryModel)pti.Content.DataContext ).CurrentTrip = selectedTrip;
                            ((TripEntry.TripEntryModel)pti.Content.DataContext).WhenCurrentTripChanges();
                            Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

                            break;
						}
					}

					if( pti == null )
					{
						// Need to create a new Trip Entry tab
						Logging.WriteLogLine( "Opening new " + Globals.TRIP_ENTRY + " tab" );
                        Globals.RunProgram(Globals.TRIP_ENTRY, selectedTrip);                        
                    }
				}
			}
		}


        private DisplayTrip GetLatestVersion(string tripId)
        {
            DisplayTrip dt = null;
            Logging.WriteLogLine("Getting latest version of trip: " + tripId);

            Task.WaitAll(Task.Run(() =>
            {
                GetTrip getTrip = new GetTrip
                {
                    Signatures = true,
                    TripId = tripId
                };
                var trip = Azure.Client.RequestGetTrip(getTrip).Result;

                if (trip != null)
                {
                    dt = new DisplayTrip(trip);
                }
            }));

            return dt;
        }

        /// <summary>
        /// Edit Selected Trips.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private async void MenuItem_Click_1( object sender, RoutedEventArgs e )
        private void MenuItem_Click_1( object sender, RoutedEventArgs e )
		{
			if (this.DataGrid.SelectedItems.Count > 0)
			{
                StringBuilder sb = new StringBuilder();
                List<DisplayTrip> selected = new List<DisplayTrip>();
                foreach (DisplayTrip tmp in this.DataGrid.SelectedItems)
                {
                    DisplayTrip dt = GetLatestVersion(tmp.TripId);

                    //selected.Add( (DisplayTrip)tmp );
                    //sb.Append( ( (DisplayTrip)tmp ).TripId ).Append( "," );
                    if (dt != null)
                    {
                        selected.Add(dt);
                        sb.Append((dt).TripId).Append(",");
                    }
                }

                Logging.WriteLogLine("Edit Selected Trips: TripIds: " + sb.ToString());
                List<DisplayTrip> changed = new EditSelectedTrips(Application.Current.MainWindow, selected).ShowEditSelectedTrips(selected);
                if (changed != null && changed.Count > 0)
                {
                    if (DataContext is SearchTripsModel Model)
                    {
                        Dispatcher.Invoke(() =>
						{
							Model.UpdateChangedTripsAsync(changed);
							Logging.WriteLogLine("Finished updating");							

							// KLUDGE need the delay to give the server time to update
							Thread.Sleep(500);
							Model.Execute_SearchTrips();

						});
					}
					Logging.WriteLogLine("Updated " + changed.Count + " trips");
				}
            }
        }

		private void ComboBox_SelectionChanged_3( object sender, SelectionChangedEventArgs e )
		{
			if( sender is ComboBox cb && DataContext is SearchTripsModel Model )
			{
				if( cb.SelectedIndex > -1 )
				{
					string address = (string)cb.SelectedItem;
					Model.SelectedCompanyPUAddressName = address;
				}
			}
		}

        private void ComboBox_SelectionChanged_4(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox cb && DataContext is SearchTripsModel Model)
            {
                if (cb.SelectedIndex > -1)
                {
                    string address = (string)cb.SelectedItem;
                    Model.SelectedCompanyDelAddressName = address;
                }
            }
        }

        private void CbCompanyName_Checked( object sender, RoutedEventArgs e )
		{
			if( DataContext is SearchTripsModel Model )
			{
				if( this.cbCompanyName.IsChecked == true )
				{
					this.comboCompanyName.IsEnabled = true;
					this.cbPUAddress.IsEnabled = true;
                    //this.comboPUAddress.IsEnabled = true;
                    this.cbDelAddress.IsEnabled = true;
                    //this.comboDelAddress.IsEnabled = true;
                    
					Model.FilterByCompanyName = true;
				}
				else
				{
					this.comboCompanyName.IsEnabled = false;
					this.cbPUAddress.IsEnabled = false;
					this.cbPUAddress.IsChecked = false;
                    this.comboPUAddress.IsEnabled = false;
					this.cbDelAddress.IsEnabled = false;
					this.cbDelAddress.IsChecked = false;
                    this.comboDelAddress.IsEnabled = false
                        ;
					Model.FilterByCompanyName = false;
				}
			}
		}

		private void CbPUAddress_Checked( object sender, RoutedEventArgs e )
		{
			if( DataContext is SearchTripsModel Model )
			{
				if( this.cbPUAddress.IsChecked == true )
				{
					this.comboPUAddress.IsEnabled = true;
					Model.FilterByPUAddress = true;
				}
				else
				{
					this.comboPUAddress.IsEnabled = false;
					Model.FilterByPUAddress = false;
				}
			}
		}

        private void CbDelAddress_Checked(object sender, RoutedEventArgs e)
        {
            if (DataContext is SearchTripsModel Model)
            {
                if (this.cbDelAddress.IsChecked == true)
                {
                    this.comboDelAddress.IsEnabled = true;
                    Model.FilterByDeliveryAddress = true;
                }
                else
                {
                    this.comboDelAddress.IsEnabled = false;
                    Model.FilterByDeliveryAddress = false;
                }
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is SearchTripsModel Model && sender is DatePicker dp)
            {
                if (dp.SelectedDate != null)
                {
                    Model.FromDate = (DateTimeOffset)dp.SelectedDate;
                }
            }
        }

        private void DatePicker_SelectedDateChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is SearchTripsModel Model && sender is DatePicker dp)
            {
                if (dp.SelectedDate != null)
                {
                    Model.ToDate = (DateTimeOffset)dp.SelectedDate;
                }
                else
                {
                    Model.ToDate = DateTimeOffset.Now;
                }
            }
        }

        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is SearchTripsModel Model)
            {
                Logging.WriteLogLine("Opening " + Model.HelpUri);
                Uri uri = new Uri(Model.HelpUri);
                Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dpFrom.SelectedDate = DateTime.Now;
            dpTo.SelectedDate = DateTime.Now;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            tbClientReference.IsEnabled = false;
            tbTripId.IsEnabled = true;
            tbTripId.Focus();

            cbPUAddress.IsEnabled = false;
            cbDelAddress.IsEnabled = false;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            tbTripId.IsEnabled = false;

            if (cbCompanyName.IsChecked == true)
            {
                cbPUAddress.IsEnabled = true;
                cbDelAddress.IsEnabled = true;
            }
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            tbTripId.IsEnabled = false;
            tbClientReference.IsEnabled = true;
            tbClientReference.Focus();

            cbPUAddress.IsEnabled = false;
            cbDelAddress.IsEnabled = false;
        }

        private void CheckBox_Unchecked_1(object sender, RoutedEventArgs e)
        {
            tbClientReference.IsEnabled = false;

            if (cbCompanyName.IsChecked == true)
            {
                cbPUAddress.IsEnabled = true;
                cbDelAddress.IsEnabled = true;
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            //MenuItem_Click_1(null, null);            
            MiOpenInShipmentEntry_Click(null, null);
        }

        //
        // Copied from DispatchBoard.xaml.cs

        private void DataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            var Ndx = e.Column.DisplayIndex;

            if (Ndx == 0)
            {
                var Cell = DataGrid.GetCell(e.Row.GetIndex(), Ndx);
                //DriversPopup.PlacementTarget = Cell;
                //DriversPopup.IsOpen = true;
                //DriverCombo.RemoveParent();
            }
        }

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            //DriversPopup.IsOpen = false;
        }


        private void DataGrid_PreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            if (e.Column.DisplayIndex == 0)
            {
                var StackPanel = e.EditingElement.FindChild<StackPanel>();

                if (!(StackPanel is null))
                {
                    //DriverCombo.RemoveParent();
                    //StackPanel.Children.Add(DriverCombo);
                    //DriverCombo.DataContext = Model;
                    //DriverCombo.Visibility = Visibility.Visible;
                    //DriverCombo.UpdateLayout();
                    //var TexBox = DriverCombo.FindChild<TextBox>("PART_EditableTextBox");

                    //if (!(CurrentTrip is null))
                    //    TexBox.Text = CurrentTrip.Driver;
                    //TexBox?.Focus();
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!(Model is null))
            {
                // TODO
                List<ViewModels.Trips.Boards.Common.DisplayTrip> SelectedTrips = null; // Model.SelectedTrips;

                if (!(SelectedTrips is null))
                {
                    var Count = SelectedTrips.Count;

                    if (Count > 0)
                    {
                        var Trip = SelectedTrips[Count - 1];

                        var Ndx = 0;

                        foreach (ViewModels.Trips.Boards.Common.DisplayTrip ModelTrip in Model.Trips)
                        {
                            if (Trip == ModelTrip)
                            {
                                var Cell = DataGrid.GetCell(Ndx, 0);
                                Cell.Focus();
                                DataGrid.BeginEdit();
                            }

                            ++Ndx;
                        }
                    }
                }
            }
        }

        private void DataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            //if ((e.AddedCells.Count > 0) && e.AddedCells[0].Item is ViewModels.Trips.Boards.Common.DisplayTrip Trip)
            //    CurrentTrip = Trip;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is DataGrid Grid)
            {
                var Trips = Model.SelectedTrips;

                if (Trips is null)
                    //Trips = new List<DisplayTrip>();
                    Trips = new ObservableCollection<DisplayTrip>();
                else
                    Trips.Clear();

                Model.SelectedTrips = null;
                //Trips.AddRange(Grid.SelectedItems.Cast<DisplayTrip>()); 

                foreach (DisplayTrip dt in Grid.SelectedItems)
                {
                    Trips.Add(dt); 
                }
                Model.SelectedTrips = Trips;
            }
        }

		//private void DataGrid_CleanUpVirtualizedItem(object sender, CleanUpVirtualizedItemEventArgs e)
		//{
		//	var row = e.UIElement as DataGridRow;
		//	if (row != null && row.Background == Brushes.LightGreen) //don't recycle light green rows...
		//	{
		//		e.Cancel = true;
		//	}
		//}

		private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            // Binding sometimes doesn't work
            if (sender is ToggleButton Tb)
            {
                var DGrid = Tb.FindParent<DataGrid>();

                if (!(DGrid is null))
                {
                    var Ndx = DGrid.SelectedIndex;

                    if (Ndx >= 0)
                    {
                        var Itm = (DisplayTrip)DGrid.SelectedItem;
                        Itm.DetailsVisible = true;

                        var Row = DGrid.GetRow(Ndx);

                        if (!(Row is null))
                            DGrid.ScrollIntoView(Row);
                    }
                }
            }
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            // Binding sometimes doesn't work
            if (sender is ToggleButton Tb)
            {
                var Grid = Tb.FindParent<DataGrid>();

                if (Grid?.SelectedItem is DisplayTrip Itm)
                    Itm.DetailsVisible = false;
            }
        }

		private void MiOpenInShipmentEntry_Click(object sender, RoutedEventArgs e)
		{
			// Same as double-click
			Logging.WriteLogLine("loading into Shipment Entry tab");
			TripListView_MouseDoubleClick(null, null);
		}
	}
}