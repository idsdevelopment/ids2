﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using Utils;

namespace ViewModels.Trips.SearchTrips
{
	public class EditSelectedTripsModel : ViewModelBase
	{
		public List<DisplayTrip> Trips
		{
			get { return Get( () => Trips, new List<DisplayTrip>() ); }
			set { Set( () => Trips, value ); }

			//set
			//{
			//    Trips = new ObservableCollection<DisplayTrip>(value);
			//}
		}


		public List<string> StatusStrings
		{
			get { return Get( () => StatusStrings, GetStatusStrings ); }
			set { Set( () => StatusStrings, value ); }
		}


		public string Description
		{
			get { return Get( () => Description, FindStringResource( "EditSelectedTripsDescription" ) ); }
		}


		public static List<string> Drivers
		{
			//get { return Get(() => Drivers, GetDriversList); }
			get
			{
				var Drivers = GetDriversList();

				return Drivers;
			}
		}


		public string SelectedDriver
		{
			get { return Get( () => SelectedDriver, "" ); }
			set { Set( () => SelectedDriver, value ); }
		}


		public bool IsDriverSelected
		{
			get { return Get( () => IsDriverSelected, false ); }
			set { Set( () => IsDriverSelected, value ); }
		}


		public DateTime SelectedCallTime
		{
			get { return Get( () => SelectedCallTime, DateTime.MinValue ); }
			set { Set( () => SelectedCallTime, value ); }
		}


		public bool IsCallTimeSelected
		{
			get { return Get( () => IsCallTimeSelected, false ); }
			set { Set( () => IsCallTimeSelected, value ); }
		}


		public string SelectedPOD
		{
			get { return Get( () => SelectedPOD, "" ); }
			set { Set( () => SelectedPOD, value ); }
		}


		public bool IsPODSelected
		{
			get { return Get( () => IsPODSelected, false ); }
			set { Set( () => IsPODSelected, value ); }
		}


		public string SelectedPOP
		{
			get { return Get( () => SelectedPOP, "" ); }
			set { Set( () => SelectedPOP, value ); }
		}


		public bool IsPOPSelected
		{
			get { return Get( () => IsPOPSelected, false ); }
			set { Set( () => IsPOPSelected, value ); }
		}


		public DateTime SelectedTPu
		{
			get { return Get( () => SelectedTPu, DateTime.MinValue ); }
			set { Set( () => SelectedTPu, value ); }
		}


		public bool IsTPuSelected
		{
			get { return Get( () => IsTPuSelected, false ); }
			set { Set( () => IsTPuSelected, value ); }
		}


		public DateTime SelectedTDel
		{
			get { return Get( () => SelectedTDel, DateTime.MinValue ); }
			set { Set( () => SelectedTDel, value ); }
		}


		public bool IsTDelSelected
		{
			get { return Get( () => IsTDelSelected, false ); }
			set { Set( () => IsTDelSelected, value ); }
		}


		public string SelectedStatus
		{
			get { return Get( () => SelectedStatus, "" ); }
			set { Set( () => SelectedStatus, value ); }
		}


		public bool IsStatusSelected
		{
			get { return Get( () => IsStatusSelected, false ); }
			set { Set( () => IsStatusSelected, value ); }
		}


		public bool AnyTripsChanged
		{
			get { return Get( () => AnyTripsChanged, false ); }
			set { Set( () => AnyTripsChanged, value ); }
		}

		[DependsUpon( nameof( IsCallTimeSelected ) )]
		[DependsUpon( nameof( IsDriverSelected ) )]
		[DependsUpon( nameof( IsPODSelected ) )]
		[DependsUpon( nameof( IsPOPSelected ) )]
		[DependsUpon( nameof( IsStatusSelected ) )]
		[DependsUpon( nameof( IsTDelSelected ) )]
		[DependsUpon( nameof( IsTPuSelected ) )]
		public bool IsApplyButtonEnabled
		{
			//get { return Get(() => IsApplyButtonEnabled, false); }
			get
			{
				var enabled = false;

				if( IsCallTimeSelected || IsDriverSelected || IsPODSelected || IsPOPSelected || IsStatusSelected || IsTDelSelected || IsTPuSelected )
					enabled = true;

				return enabled;
			}
			set { Set( () => IsApplyButtonEnabled, value ); }
		}

		private List<string> GetStatusStrings()
		{
			var strings = new List<string>
			              {
				              string.Empty,

				              //STATUS.UNSET.AsString(),
				              STATUS.NEW.AsString(),
				              STATUS.ACTIVE.AsString(),
				              STATUS.DISPATCHED.AsString(),
				              STATUS.PICKED_UP.AsString(),
				              STATUS.DELIVERED.AsString(),
				              STATUS.VERIFIED.AsString(),

				              //STATUS.POSTED.AsString(),
				              //STATUS.INVOICED.AsString(),
				              //STATUS.SCHEDULED.AsString(),
				              //STATUS.PAID.AsString(),
				              STATUS.DELETED.AsString()
			              };

			return strings;
		}


		private static List<string> GetDriversList()
		{
			var drivers = new List<string>();

			_ = Task.Run( () =>
			              {
				              try
				              {
					              // From Terry - stop asking the server if not logged in
					              if( !IsInDesignMode )
					              {
						              var staff = Azure.Client.RequestGetStaff().Result;

						              if( staff != null )
						              {
							              // Ensure that they are sorted
							              var sd = new SortedDictionary<string, string>();

							              foreach( var member in staff )
							              {
								              var roles = Azure.Client.RequestStaffMemberRoles( member.StaffId ).Result;

								              foreach( var role in roles )
								              {
									              if( role.Name == "Driver" )
									              {
										              sd.Add( member.StaffId, string.Empty );

										              break;
									              }
								              }
							              }

							              foreach( var key in sd.Keys )
								              drivers.Add( key );

							              drivers.Insert( 0, "" ); // Adding a blank line so that the driver can be removed
						              }
					              }
				              }
				              catch( Exception e )
				              {
					              Logging.WriteLogLine( "Exception while getting staff list: " + e );
				              }
			              } );

			return drivers;
		}

		/// <summary>
		///     Clears out some of the fields.
		/// </summary>
		private void ChangeStatusOfTrips()
		{
			var NewStatus = ConvertStatusStringToSTATUS( SelectedStatus );

			foreach( var trip in Trips )
			{
				Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing status " + trip.Status + " to " + SelectedStatus );
				trip.Status = NewStatus;

				switch( NewStatus )
				{
				case Boards.Common.DisplayTrip.STATUS.UNSET:
				case Boards.Common.DisplayTrip.STATUS.NEW:
				case Boards.Common.DisplayTrip.STATUS.ACTIVE:
					// Clear times, pop, pod, and driver
					trip.ClaimTime = DateTimeOffset.MinValue;

					//trip.DeliveryTime = DateTimeOffset.MinValue;
					//trip.PickupTime = DateTimeOffset.MinValue;
					trip.VerifiedTime  = DateTimeOffset.MinValue;
					trip.Driver        = string.Empty;
					trip.PickupNotes   = string.Empty;
					trip.DeliveryNotes = string.Empty;
					trip.POD           = string.Empty;
					trip.POP           = string.Empty;

					break;
				case Boards.Common.DisplayTrip.STATUS.DISPATCHED:
					trip.VerifiedTime  = DateTimeOffset.MinValue;
					trip.PickupNotes   = string.Empty;
					trip.DeliveryNotes = string.Empty;
					trip.POD           = string.Empty;
					trip.POP           = string.Empty;

					break;
				case Boards.Common.DisplayTrip.STATUS.PICKED_UP:
					trip.DeliveryNotes = string.Empty;
					trip.POD           = string.Empty;

					break;
				}
			}
		}

		private Boards.Common.DisplayTrip.STATUS ConvertStatusStringToSTATUS( string status )
		{
			return status switch
			       {
				       "UNSET"      => Boards.Common.DisplayTrip.STATUS.UNSET,
				       "NEW"        => Boards.Common.DisplayTrip.STATUS.NEW,
				       "ACTIVE"     => Boards.Common.DisplayTrip.STATUS.ACTIVE,
				       "DISPATCHED" => Boards.Common.DisplayTrip.STATUS.DISPATCHED,
				       "PICKED_UP"  => Boards.Common.DisplayTrip.STATUS.PICKED_UP,
				       "DELIVERED"  => Boards.Common.DisplayTrip.STATUS.DELIVERED,
				       "VERIFIED"   => Boards.Common.DisplayTrip.STATUS.VERIFIED,
				       "POSTED"     => Boards.Common.DisplayTrip.STATUS.POSTED,
				       "INVOICED"   => Boards.Common.DisplayTrip.STATUS.INVOICED,
				       "SCHEDULED"  => Boards.Common.DisplayTrip.STATUS.SCHEDULED,
				       "PAID"       => Boards.Common.DisplayTrip.STATUS.PAID,
				       "DELETED"    => Boards.Common.DisplayTrip.STATUS.DELETED,
				       _            => Boards.Common.DisplayTrip.STATUS.UNSET
			       };
		}

		/// <summary>
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		private static string FindStringResource( string name ) => (string)Application.Current.TryFindResource( name );

		/// <summary>
		///     Updates the trips with the new values.
		///     Note: it doesn't update the db - that is handled by SearchTripsModel.
		/// </summary>
		public void UpdateTrips()
		{
			if( Trips.Count > 0 )
			{
				// Decide what needs to be changed
				// Putting this first, so that the times, pod, pop, and driver won't be overwritten
				if( IsStatusSelected )
				{
					AnyTripsChanged = true;
					Logging.WriteLogLine( "Updating trips with status: " + SelectedStatus );
					ChangeStatusOfTrips();
				}

				if( IsDriverSelected )
				{
					AnyTripsChanged = true;
					var driver = SelectedDriver;
					Logging.WriteLogLine( "Updating trips with driver: " + driver );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing driver " + trip.Driver + " to " + driver );
						trip.Driver = driver;

						if( trip.Status1 < STATUS.DISPATCHED )
							trip.Status1 = STATUS.DISPATCHED;
					}
				}

				if( IsCallTimeSelected )
				{
					AnyTripsChanged = true;
					var tCallTime = SelectedCallTime;
					Logging.WriteLogLine( "Updating trips with CallTime: " + tCallTime );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing CallTime " + trip.CallTime + " to " + tCallTime );
						trip.Ct = tCallTime;
					}
				}

				if( IsPOPSelected )
				{
					AnyTripsChanged = true;
					var pop = SelectedPOP;
					Logging.WriteLogLine( "Updating trips with POP: " + pop );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing POP " + trip.POP + " to " + pop );
						trip.POP = pop;
					}
				}

				if( IsPODSelected )
				{
					AnyTripsChanged = true;
					var pod = SelectedPOD;
					Logging.WriteLogLine( "Updating trips with POD: " + pod );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing POD " + trip.POD + " to " + pod );
						trip.POD = pod;
					}
				}

				if( IsTPuSelected )
				{
					AnyTripsChanged = true;
					var tPu = SelectedTPu;
					Logging.WriteLogLine( "Updating trips with T-PU: " + tPu );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing T-PU " + trip.Pt + " to " + tPu );
						trip.Pt = tPu;
					}
				}

				if( IsTDelSelected )
				{
					AnyTripsChanged = true;
					var tDel = SelectedTDel;
					Logging.WriteLogLine( "Updating trips with T-Del: " + tDel );

					foreach( var trip in Trips )
					{
						Logging.WriteLogLine( "TripId: " + trip.TripId + " - Changing T-Del " + trip.Dt + " to " + tDel );
						trip.Dt = tDel;
					}
				}
			}
		}
	}
}