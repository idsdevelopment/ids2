﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using ViewModels.Trips.Boards.Common;

namespace ViewModels.Trips.SearchTrips
{

    /// <summary>
    /// Interaction logic for EditSelectedTrips.xaml
    /// </summary>
    public partial class EditSelectedTrips : Window
    {
        public EditSelectedTrips(Window owner,  List<DisplayTrip> trips)
        {

            this.Owner = owner;
            InitializeComponent();

            if (DataContext is EditSelectedTripsModel Model)
            {
                Model.Trips = new List<DisplayTrip>(trips);
            }
        }

        public List<DisplayTrip> ShowEditSelectedTrips(List<DisplayTrip> trips)
        {
            if (DataContext is EditSelectedTripsModel Model)
            {
                Model.Trips = new List<DisplayTrip>(trips);
                this.ShowDialog();
                if (Model.AnyTripsChanged)
                {
                    return Model.Trips;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditSelectedTripsModel Model)
            {
                Model.UpdateTrips();
            }

            this.Close();
        }

        /// <summary>
        /// Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (DataContext is EditSelectedTripsModel Model)
            {
                Model.Trips.Clear();
            }
            this.Close();
        }

        /// <summary>
        /// Selects Driver.        
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.comboDrivers.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.comboDrivers.IsEnabled = false;
                    Model.SelectedDriver = (string)this.comboDrivers.SelectedItem;
                    Model.IsDriverSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.comboDrivers.IsEnabled = true;
                    Model.SelectedDriver = null;
                    Model.IsDriverSelected = false;
                }
            }
        }

        /// <summary>
        /// Selects POD.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.tbPOD.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.tbPOD.IsEnabled = false;
                    Model.SelectedPOD = this.tbPOD.Text.Trim();
                    Model.IsPODSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.tbPOD.IsEnabled = true;
                    Model.SelectedPOD = null;
                    Model.IsPODSelected = false;
                }
            }
        }

        /// <summary>
        /// Selects T-PU
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.dtpPU.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.dtpPU.IsEnabled = false;
                    Model.SelectedTPu = (DateTime)this.dtpPU.Value;
                    Model.IsTPuSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.dtpPU.IsEnabled = true;
                    Model.SelectedTPu = DateTime.MinValue;
                    Model.IsTPuSelected = false;
                }
            }
        }

        /// <summary>
        /// Selects T-DEL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.dtpDel.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.dtpDel.IsEnabled = false;
                    Model.SelectedTDel = (DateTime)this.dtpDel.Value;
                    Model.IsTDelSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.dtpDel.IsEnabled = true;
                    Model.SelectedTDel = DateTime.MinValue;
                    Model.IsTDelSelected = false;
                }
            }
        }

        /// <summary>
        /// Selects CallTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.dtpCallTime.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.dtpCallTime.IsEnabled = false;
                    Model.SelectedCallTime = (DateTime)this.dtpCallTime.Value;
                    Model.IsCallTimeSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.dtpCallTime.IsEnabled = true;
                    Model.SelectedCallTime = DateTime.MinValue;
                    Model.IsCallTimeSelected = false;
                }
            }
        }

        /// <summary>
        /// POP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (this.tbPOP.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    this.tbPOP.IsEnabled = false;
                    Model.SelectedPOP = this.tbPOP.Text.Trim();
                    Model.IsPOPSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    this.tbPOP.IsEnabled = true;
                    Model.SelectedPOP = null;
                    Model.IsPOPSelected = false;
                }
            }
        }
        /// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		private string FindStringResource(string name)
        {
            return (string)Application.Current.TryFindResource(name);
        }

        /// <summary>
        /// Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && DataContext is EditSelectedTripsModel Model)
            {
                if (cbStatus.IsEnabled)
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelected");
                    cbStatus.IsEnabled = false;
                    Model.SelectedStatus = (string)cbStatus.SelectedItem;
                    Model.IsStatusSelected = true;
                }
                else
                {
                    b.Content = this.FindStringResource("EditSelectedTripsSelect");
                    cbStatus.IsEnabled = true;
                    cbStatus.SelectedIndex = 0;
                    Model.SelectedStatus = null;
                    Model.IsStatusSelected = false;
                }
            }
        }
    }
}
