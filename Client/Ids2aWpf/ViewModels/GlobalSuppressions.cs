﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.SearchTrips.SearchTripsModel.Execute_ClearSearchTrips")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.TripEntry.TripEntry.GetRowNumberFromName(System.String)~System.Int32")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.TripEntry.TripEntry.BuildPackageRows(ViewModels.Trips.TripEntry.TripEntryModel)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.TripEntry.TripEntryModel.WhenCurrentTripChanges")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.TripEntry.TripEntryModel.Execute_CloneTripForReturn")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.SearchTrips.SearchTrips.MenuItem_Click_1(System.Object,System.Windows.RoutedEventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.SearchTrips.SearchTrips.TripListView_MouseDoubleClick(System.Object,System.Windows.Input.MouseButtonEventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "<Pending>", Scope = "member", Target = "~M:ViewModels.Trips.SearchTrips.SearchTrips.ComboBox_SelectionChanged_2Async(System.Object,System.Windows.Controls.SelectionChangedEventArgs)")]
