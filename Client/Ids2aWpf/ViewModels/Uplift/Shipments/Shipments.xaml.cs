﻿using Protocol.Data;
using Protocol.Data._Customers.Pml;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Utils;
using Xceed.Wpf.Toolkit;

namespace ViewModels.Uplift.Shipments
{
    /// <summary>
    /// Interaction logic for Shipments.xaml
    /// </summary>
    public partial class Shipments : Page
    {
        public Shipments()
        {
            InitializeComponent();

            this.tabSelectedAddress.Visibility = Visibility.Collapsed;
            this.tabSelectedGroup.Visibility = Visibility.Collapsed;
        }

        private void cbShipmentPickupAddressGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnShipmentSelectGroup_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lbShipmentAddresses_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lbShipmentAddresses.SelectedItem is CompanyAddress ca)
            {
                Logging.WriteLogLine("Loading Address" + ca.CompanyName);
                if (DataContext is ShipmentsModel Model)
                {
                    Model.SelectedAddress = ca;
                    Model.Execute_SelectAddress();
                }
            }
        }

        private void tbShipmentSearchCompanyName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                Model.FilterCompanyName = tbShipmentSearchCompanyName.Text;
            }
        }

        private void tbShipmentSearchSSID_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                Model.FilterLocationBarcode = tbShipmentSearchSSID.Text;
            }
        }

        private void cbShipmentSearchCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbShipmentSearchState_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnShipmentSearchSelectAddress_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnShipmentSearchClear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cbShipmentDetailsServiceLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                Model.WhenSelectedServiceLevelChanges();
            }
        }

        private void IntegerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void btnShipmentDetailsAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnShipmentDetailsRemove_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                if (lvPackageList.SelectedIndex > -1)
                {
                    Logging.WriteLogLine("Removing item at " + lvPackageList.SelectedIndex);
                    List<PmlInventory> list = new List<PmlInventory>();
                    foreach (PmlInventory pi in Model.PackageList)
                    {
                        if (pi.Description != Model.SelectedInventoryInPackageList.Description)
                        {
                            list.Add(pi);
                        }
                    }

                    Model.PackageList = list;

                    // lvPackageList.ItemsSource = null;
                    // lvPackageList.Items.RemoveAt(lvPackageList.SelectedIndex);

                    //Model.PackageList.RemoveAt(lvPackageList.SelectedIndex);
                    //lvPackageList.ItemsSource = Model.PackageList;
                }
            }
        }

        private void btnShipmentTabSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnShipmentTabCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Handle_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        private void TextBox_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            menuMain.Visibility = Visibility.Collapsed;
            toolbar.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            menuMain.Visibility = Visibility.Visible;
            toolbar.Visibility = Visibility.Collapsed;
        }

        private void Toolbar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            menuMain.Visibility = Visibility.Visible;
            toolbar.Visibility = Visibility.Collapsed;
        }

        private void BtnAddUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        //private void TbShipmentDetailsQuantity_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    if (DataContext is ShipmentsModel Model)
        //    {
        //        Model.ShipmentDetailsQuantity = tbShipmentDetailsQuantity.Text;
        //    }
        //}

        private void LvPackageList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                PmlInventory selection = (PmlInventory)lvPackageList.SelectedItem;
                Model.SelectedInventoryInPackageList = selection;
            }
        }

        private void TbPackageTypeFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is ShipmentsModel Model)
            {
                Model.FilterPackageTypes = tbPackageTypeFilter.Text.Trim().ToLower();
            }
        }

        private void IntegerUpDown_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
            else if (sender is IntegerUpDown iud)
            {
                if (iud.Value < 1)
                {
                    iud.Value = 1;
                    e.Handled = true;
                }
            }
        }
    }
}
