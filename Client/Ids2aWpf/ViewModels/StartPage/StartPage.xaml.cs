﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ViewModels.StartPage
{
	/// <summary>
	///     Interaction logic for StartPage.xaml
	/// </summary>
	public partial class StartPage : Page
	{
		private StartPageModel Model;

		private void Button_Click_1( object sender, RoutedEventArgs e )
		{
			Application.Current.Shutdown();
		}

		private void Password_KeyUp( object sender, KeyEventArgs e )
		{
			Model.Password = Password.Password;
		}

		private void Page_Loaded( object sender, RoutedEventArgs e )
		{
			if( DataContext is StartPageModel M )
			{
				M.StartPage = this;
				Model       = M;
			}
		}

		private void imageRevealPassword_MouseDown( object sender, MouseButtonEventArgs e )
		{
			//Utils.Logging.WriteLogLine("Showing password");
			imageRevealPassword.Source = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-open-24.png", UriKind.RelativeOrAbsolute ) );
			tbPassword.Text            = Password.Password;
			Password.Visibility        = Visibility.Hidden;
			tbPassword.Visibility      = Visibility.Visible;
		}

		private void imageRevealPassword_MouseLeave( object sender, MouseEventArgs e )
		{
			//Utils.Logging.WriteLogLine("Hiding password");
			imageRevealPassword.Source = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute ) );
			tbPassword.Text            = string.Empty;
			Password.Visibility        = Visibility.Visible;
			tbPassword.Visibility      = Visibility.Hidden;
		}

		private void imageRevealPassword_MouseUp( object sender, MouseButtonEventArgs e )
		{
			//Utils.Logging.WriteLogLine("Hiding password");
			imageRevealPassword.Source = new BitmapImage( new Uri( "/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute ) );
			tbPassword.Text            = string.Empty;
			Password.Visibility        = Visibility.Visible;
			tbPassword.Visibility      = Visibility.Hidden;
		}

		private void TextBox_GotFocus( object sender, RoutedEventArgs e )
		{
			( (TextBox)sender ).SelectAll();
		}

		private void TextBox_PreviewMouseDown( object sender, MouseButtonEventArgs e )
		{
			var TextBox = (TextBox)sender;

			if( !TextBox.IsKeyboardFocusWithin )
			{
				TextBox.Focus();
				e.Handled = true;
			}
		}

		public StartPage()
		{
			InitializeComponent();

			EventManager.RegisterClassHandler( typeof( TextBox ), GotFocusEvent, new RoutedEventHandler( TextBox_GotFocus ) );
			EventManager.RegisterClassHandler( typeof( TextBox ), PreviewMouseDownEvent, new MouseButtonEventHandler( TextBox_PreviewMouseDown ) );
		}
	}
}