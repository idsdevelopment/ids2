﻿using Protocol.Data;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Utils;
using ViewModels.Trips.TripEntry;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Customers.AddressBook
{
    /// <summary>
    /// Interaction logic for AddressBook.xaml
    /// </summary>
    public partial class AddressBook : Page
    {
        private readonly Dictionary<string, Control> dictControls = new Dictionary<string, Control>();

        public AddressBook()
        {
            InitializeComponent();

            foreach (TabItem ti in this.tcGroups.Items)
            {
                ti.Visibility = Visibility.Collapsed;
            }

            // TODO This should probably be only on for PML as they have 1 account
            //cbSelectCompany.SelectedIndex = -1; // Don't load an account
        }

        private void BtnAddUpdate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                if (dictControls.Count == 0)
                {
                    LoadMappingDictionary();
                }
                // Kludge - this is sometimes empty
                Model.AddressLocationBarcode = tbLocationBarcode.Text;

                List<string> errors = Model.AddUpdateAddress(dictControls);

                if ((errors != null) && (errors.Count > 0))
                {
                    var message = (string)Application.Current.TryFindResource("AddressBookValidationErrors")
                                  + Environment.NewLine;
                    foreach (var line in errors)
                        //message += "\t" + line + Environment.NewLine;
                        message += "    " + line + Environment.NewLine;

                    message += (string)Application.Current.TryFindResource("AddressBookValidationErrors1");
                    var title = (string)Application.Current.TryFindResource("AddressBookValidationErrorsTitle");

                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);

                    UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
                }
            }
        }

        private void UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses()
        {
            var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
            if (tis != null && tis.Count > 0)
            {
                for (int i = 0; i < tis.Count; i++)
                {
                    var ti = tis[i];
                    if (ti.Content is TripEntry)
                    {
                        Logging.WriteLogLine("Reloading addresses in extant " + Globals.UPLIFT_SHIPMENTS + " tab");

                        ((TripEntryModel)ti.Content.DataContext).WhenAccountChanges();
                    }
                    else if (ti.Content is Trips.SearchTrips.SearchTrips)
                    {
                        Logging.WriteLogLine("Reloading addresses in extant " + Globals.SEARCH_TRIPS + " tab");

                        //((Trips.SearchTrips.SearchTripsModel)ti.Content.DataContext).ComboBox_SelectionChanged_2Async(null, null);
                        ((Trips.SearchTrips.SearchTrips)ti.Content).ComboBox_SelectionChanged_2Async(null, null);

                    }
                }
            }
        }

        private void DisplayNotImplementedMessage()
        {
            MessageBox.Show("Not implemented yet", "Not Implemented", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        //private void BtnExport_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    if (DataContext is AddressBookModel Model)
        //    {

        //    }
        //}

        private void TbFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                //Model.AddressFilter = this.tbFilter.Text.ToLower().Trim();
                //Model.WhenAddressFilterChanges(this.tbFilter.Text.ToLower().Trim());
            }
        }

        private void LvAddresses_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                if (this.lvAddresses.SelectedIndex > -1 && this.lvAddresses.SelectedItem != null)
                {                    
                    Model.Execute_LoadAddress();
                }
            }
        }

        //private void BtnEdit_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    if (DataContext is AddressBookModel Model)
        //    {
        //        if (this.lvAddresses.SelectedIndex > -1 && this.lvAddresses.SelectedItem != null)
        //        {
        //            Model.Execute_LoadAddress();
        //        }
        //    }
        //}

        private void BtnDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                //DisplayNotImplementedMessage();
                if (this.lvAddresses.SelectedIndex > -1 && this.lvAddresses.SelectedItems.Count > 0)
                {
                    List<ExtendedCompanyAddress> addresses = new List<ExtendedCompanyAddress>();
                    foreach (ExtendedCompanyAddress eca in lvAddresses.SelectedItems)
                    {
                        addresses.Add(eca);
                    }


                    var title = (string)Application.Current.TryFindResource("AddressBookDeleteQuestionTitle");
                    var message = (string)Application.Current.TryFindResource("AddressBookDeleteQuestion") + "\n";
                    message += (string)Application.Current.TryFindResource("AddressBookDeleteQuestion2");
                    MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (mbr == MessageBoxResult.Yes)
                    {
                        Model.Execute_DeleteAddress(addresses);
                    }

                }
            }
        }

        /// <summary>
		///     Add widgets and their associated fields.
		///     Uses the GetChildren extension method defined in Widgets.cs.
		/// </summary>
		/// <returns></returns>
		private void LoadMappingDictionary()
        {
            var children = this.GetChildren();
            foreach (var ctrl in children)
            {
                Binding binding = null;
                if (ctrl is TextBox tb)
                    binding = BindingOperations.GetBinding(tb, TextBox.TextProperty);
                else if (ctrl is ComboBox cb)
                    binding = BindingOperations.GetBinding(cb, ItemsControl.ItemsSourceProperty);
                else if (ctrl is DateTimePicker dtp)
                    binding = BindingOperations.GetBinding(dtp, DateTimePicker.ValueProperty);
                else if (ctrl is TimePicker tp)
                    binding = BindingOperations.GetBinding(tp, DateTimePicker.ValueProperty);

                if (binding != null)
                {
                    var name = binding.Path.Path;
                    if (!dictControls.ContainsKey(name))
                        dictControls.Add(name, (Control)ctrl);
                    else
                        Logging.WriteLogLine("ERROR " + name + " already exists");
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            menuMain.Visibility = Visibility.Collapsed;
            toolbar.Visibility = Visibility.Visible;
        }

        private void Toolbar_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            menuMain.Visibility = Visibility.Visible;
            toolbar.Visibility = Visibility.Collapsed;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Toolbar_MouseDoubleClick(null, null);
        }

        /// <summary>
        /// Kludge - the field isn't being updated through the binding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbNotes_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressNotes = tbNotes.Text.Trim();
            }
        }

        /// <summary>
        /// Kludge - the field isn't being updated through the binding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbContactName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressContactName = tbContactName.Text.Trim();
            }
        }

        /// <summary>
        /// Kludge - the field isn't being updated through the binding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbContactEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressContactEmail = tbContactEmail.Text.Trim();
            }
        }

        private void TbFilterCompanyName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Logging.WriteLogLine("FilterCompanyName: " + tbFilterCompanyName.Text + ", Model.FilterCompanyName: " + Model.FilterCompanyName);

                Model.WhenFilterCompanyNameChanges(tbFilterCompanyName.Text.Trim());
            }
        }

        private void TbFilterLocationBarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Logging.WriteLogLine("FilterLocationBarcode: " + tbFilterLocationBarcode.Text + ", Model.FilterLocationBarcode: " + Model.FilterLocationBarcode);
                Model.WhenFilterLocationBarcodeChanges(tbFilterLocationBarcode.Text.Trim());
            }
        }

        private void tbAddressBookNewGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.NewGroupDescription = tbAddressBookNewGroupName.Text.Trim();
            }
        }

        private void lbAddressBookGroups_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.Execute_ModifyGroup();
            }
        }

        private void btnAddressBookCreateGroup_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddressBookModifyGroup_Click(object sender, RoutedEventArgs e)
        {
            if (lbAddressBookGroups.SelectedItem != null)
            {

            }
        }

        private void btnAddressBookDeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                if (Model.SelectedGroup.IsNotNullOrWhiteSpace())
                {
                    string message = (string)Application.Current.TryFindResource("AddressBookGroupsDeleteGroup") + Environment.NewLine;
                    message += (string)Application.Current.TryFindResource("AddressBookGroupsDeleteGroup1") + Environment.NewLine;
                    message = message.Replace("@1", Model.SelectedGroup);
                    message = message.Replace("\t", " ");
                    string title = (string)Application.Current.TryFindResource("AddressBookGroupsDeleteGroupTitle");
                    MessageBoxResult mbr = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (mbr == MessageBoxResult.Yes)
                    {
                        Model.Execute_DeleteGroup();
                    }
                }
            }
        }

        private void btnModifyGroupAdd_Click(object sender, RoutedEventArgs e)
        {

            if (DataContext is AddressBookModel Model)
            {
                // Model.SelectedAddresses = (List<ExtendedCompanyAddress>)lvAddresses.SelectedItems;
                // Model.SelectedAddresses.Clear();
                List<ExtendedCompanyAddress> list = new List<ExtendedCompanyAddress>();
                foreach (var tmp in lvAddresses.SelectedItems)
                {
                    //Model.SelectedAddresses.Add((ExtendedCompanyAddress) tmp);
                    list.Add((ExtendedCompanyAddress) tmp);
                }
                Model.SelectedAddresses = list;

                Model.Execute_AddSelectedToGroup();
            }
        }

        private void btnModifyGroupRemove_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                if (lbGroupUpdate.SelectedItems != null && lbGroupUpdate.SelectedItems.Count > 0)
                {
                    List<string> list = new List<string>();
                    foreach (string tmp in lbGroupUpdate.SelectedItems)
                    {
                        list.Add(tmp);
                    }
                    Model.SelectedGroupMembers = list;
                    Model.Execute_RemoveSelectedFromGroup();
                }
            }
        }

        private void btnModifyGroupSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnModifyGroupCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var tmp in this.lvAddresses.Items)
            {
                this.lvAddresses.SelectedItems.Add(tmp);
            }
        }

        private void BtnSelectNone_Click(object sender, RoutedEventArgs e)
        {
            this.lvAddresses.SelectedItems.Clear();
        }

        private void btnAddressBookRemoveMemberOfGroups_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddressBookAddCurrentAddressToGroup_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LbAddressBookAddCurrentAddressToGroup_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.Execute_AddToMemberOfGroups();
            }
        }

        private void LbAddressBookMemberOfGroups_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.Execute_RemoveFromMemberOfGroups();
            }
        }

        private void BtnToolbarClear_Click(object sender, RoutedEventArgs e)
        {
            tbDescription.Background = Brushes.White;
            tbLocationBarcode.Background = Brushes.White;
            tbStreet.Background = Brushes.White;
            tbCity.Background = Brushes.White;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            BtnToolbarClear_Click(null, null);
        }

        private void TbContactPhone_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressContactPhone = tbContactPhone.Text.Trim();
            }
        }

        private void TbDescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressDescription = tbDescription.Text.Trim();
            }
        }

        private void TbLocationBarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressLocationBarcode = tbLocationBarcode.Text.Trim();
            }
        }

        private void TbSuite_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressSuite = tbSuite.Text.Trim();
            }
        }

        private void TbStreet_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressStreet = tbStreet.Text.Trim();
            }
        }

        private void TbCity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressCity = tbCity.Text.Trim();
            }
        }

        private void TbZipPostal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DataContext is AddressBookModel Model)
            {
                Model.AddressZipPostal = tbZipPostal.Text.Trim();
            }
        }

        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            string filter = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*txt";
            var ofd = new Microsoft.Win32.OpenFileDialog() { 
                Filter = filter
            };
            ofd.Multiselect = false;
            bool? result = ofd.ShowDialog();
            if (result == true)
            {
                Logging.WriteLogLine("Selected " + ofd.FileName);
                string file = ofd.FileName;

                if (DataContext is AddressBookModel Model)
                {
                    Model.SelectedImportFile = file;
                    (bool isCorrect, List<string> errors) = Model.IsImportAddressFileValid();
                    if (!isCorrect)
                    {
                        Model.SelectedImportFile = string.Empty;
                        string title = (string)Application.Current.TryFindResource("AddressBookImportAddressesFormatErrorsInFileHeaderTitle");
                        string message = (string)Application.Current.TryFindResource("AddressBookImportAddressesFormatErrorsInFileHeader");
                        message = message.Replace("@1", file);
                        foreach (string error in errors)
                        {
                            message += "\n\t" + error;
                        }
                        message += "\n" + (string)Application.Current.TryFindResource("AddressBookImportAddressesFormatErrorsInFileHeaderEnd");

                        MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
    }
}
