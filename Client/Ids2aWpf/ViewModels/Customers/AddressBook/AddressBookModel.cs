﻿using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utils;
using ViewModels.Trips.TripEntry;
using ViewModels.Trips.SearchTrips;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;
using System.IO;
using File = System.IO.File;
using CsvHelper;

namespace ViewModels.Customers.AddressBook
{
	public class AddressBookModel : ViewModelBase
	{
		//private List<CompanyAddress> customers = null;
		private readonly Dictionary<string, string> dictCustomerNamesIds = new Dictionary<string, string>();
		private readonly List<ExtendedCompanyAddress> currentCompanyAddresses = new List<ExtendedCompanyAddress>();

		public bool Loaded
		{
			get { return Get( () => Loaded, false ); }
			set { Set( () => Loaded, value ); }
		}

		protected override void OnInitialised()
		{
			base.OnInitialised();
			Loaded = false;

			Task.Run( () =>
			          {
				          //Task.WaitAll(Task.Run(() =>
				          //                       {
				          //                           GetAccountNamesNotAsync();
				          //                       })

				          //            );
				          Task.WaitAll( Task.Run( () =>
				                                  {
					                                  GetAccountNames();

					                                  //GetCities();
					                                  //Dispatcher.Invoke(() =>
					                                  //                   {
					                                  //                       Loaded = true;
					                                  //                   });
				                                  } ),
				                        Task.Run( () =>
				                                  {
					                                  //Execute_ClearAddress();
					                                  Dispatcher.Invoke( () =>
					                                                     {
						                                                     LoadGroups();

						                                                     //LoadAddressesGroupNames();
					                                                     } );
				                                  } )
				                      );
			          } );
		}

		public override void OnArgumentChange( object selectedAccountName )
		{
			if( SelectedAccountName is string san )
				SelectedAccountName = san;
		}

		public string HelpUri
		{
			get { return Get( () => HelpUri, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/649756883/Address+Book" ); }
		}

	#region Filter Addresses

		public string FilterCompanyName
		{
			get { return Get( () => FilterCompanyName, "" ); }
			set { Set( () => FilterCompanyName, value ); }
		}

		//[DependsUpon(nameof(FilterCompanyName))]
		public void WhenFilterCompanyNameChanges( string text )
		{
			Logging.WriteLogLine( "Filtering by Company Name: " + FilterCompanyName + ", text: " + text );
			FilterCompanyName = text.ToLower();

			if( FilterCompanyName.IsNotNullOrWhiteSpace() )
			{
				List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

				foreach( ExtendedCompanyAddress eca in currentCompanyAddresses )
				{
					//if (eca.Ca.CompanyName.ToLower().Contains(FilterCompanyName) || eca.AddressString.ToLower().Contains(FilterCompanyName))
					if( eca.Ca.CompanyName.ToLower().Contains( FilterCompanyName ) )
					{
						ecas.Add( eca );
					}
				}

				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( ecas );
				                   } );
			}
			else
			{
				// Reload unfiltered list
				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
				                   } );
			}
		}


		public string FilterLocationBarcode
		{
			get { return Get( () => FilterLocationBarcode, "" ); }
			set { Set( () => FilterLocationBarcode, value ); }
		}

		//[DependsUpon(nameof(FilterLocationBarcode))]
		public void WhenFilterLocationBarcodeChanges( string text )
		{
			Logging.WriteLogLine( "Filtering by Location Barcode: " + FilterLocationBarcode + ", text: " + text );
			FilterLocationBarcode = text.ToLower();

			if( FilterLocationBarcode.IsNotNullOrWhiteSpace() )
			{
				List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

				foreach( ExtendedCompanyAddress eca in currentCompanyAddresses )
				{
					//string rawName = eca.Ca.CompanyName.ToLower();
					//if (rawName.Contains("(") && rawName.Contains(")"))
					//{
					//    int start = rawName.IndexOf('(') + 1;
					//    int end = rawName.IndexOf(')');
					//    if (start < end)
					//    {
					//        string ssid = rawName.Substring(start, (end - start)).ToLower();
					//        if (ssid.Contains(text))
					//        {
					//            ecas.Add(eca);
					//        }
					//    }
					//}

					if( eca.Ca.LocationBarcode.ToLower().Contains( FilterLocationBarcode ) )
					{
						ecas.Add( eca );
					}
				}

				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( ecas );
				                   } );
			}
			else
			{
				// Reload unfiltered list
				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
				                   } );
			}
		}


		public List<string> FilterCities
		{
			get { return Get( () => FilterCities, GetCities ); }
			set { Set( () => FilterCities, value ); }
		}


		/// <summary>
		/// Walks the currently used cities and builds a sorted list.
		/// </summary>
		/// <returns></returns>
		[DependsUpon( nameof( FilterCities ) )]
		private List<string> GetCities()
		{
			List<string> cities = new List<string>();

			Logging.WriteLogLine( "Loading currently used cities in the current account's addresses" );

			if( ExtendedAddresses != null && ExtendedAddresses.Count > 0 )
			{
				SortedDictionary<string, string> sd = new SortedDictionary<string, string>();

				foreach( ExtendedCompanyAddress eca in ExtendedAddresses )
				{
					if( !sd.ContainsKey( eca.Ca.City.ToLower() ) )
					{
						sd.Add( eca.Ca.City.ToLower(), eca.Ca.City );
					}
				}

				foreach( string key in sd.Keys )
				{
					cities.Add( sd[ key ] );
				}

				if( cities[ 0 ].IsNotNullOrWhiteSpace() )
				{
					cities.Insert( 0, "" ); // Make sure that there is an empty line
				}

				Dispatcher.Invoke( () =>
				                   {
					                   FilterCities = cities;
				                   } );
			}

			return cities;
		}


		public string FilterSelectedCity
		{
			get { return Get( () => FilterSelectedCity, "" ); }
			set { Set( () => FilterSelectedCity, value ); }
		}

		[DependsUpon( nameof( FilterSelectedCity ) )]
		public void WhenFilterSelectedCityChanges()
		{
			Logging.WriteLogLine( "FilterSelectedCity: " + FilterSelectedCity );

			if( FilterSelectedCity.IsNotNullOrWhiteSpace() )
			{
				if( ExtendedAddresses != null && ExtendedAddresses.Count > 0 )
				{
					SortedDictionary<string, ExtendedCompanyAddress> sd = new SortedDictionary<string, ExtendedCompanyAddress>();

					foreach( ExtendedCompanyAddress eca in ExtendedAddresses )
					{
						if( eca.Ca.City == FilterSelectedCity )
						{
							if( !sd.ContainsKey( eca.Ca.CompanyName.ToLower() ) )
							{
								sd.Add( eca.Ca.CompanyName.ToLower(), eca );
							}
						}
					}

					List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

					foreach( string key in sd.Keys )
					{
						ecas.Add( sd[ key ] );
					}

					Dispatcher.Invoke( () =>
					                   {
						                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( ecas );
					                   } );
				}
			}
			else
			{
				// Reload unfiltered list
				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
				                   } );
			}
		}


		public List<string> FilterRegions
		{
			get { return Get( () => FilterRegions ); }
			set { Set( () => FilterRegions, value ); }
		}


		public string FilterSelectedRegion
		{
			get { return Get( () => FilterSelectedRegion, "" ); }
			set { Set( () => FilterSelectedRegion, value ); }
		}

		[DependsUpon( nameof( FilterSelectedRegion ) )]
		public void WhenFilterSelectedRegionChanges()
		{
			Logging.WriteLogLine( "FilterSelectedRegion: " + FilterSelectedRegion );

			if( FilterSelectedRegion.IsNotNullOrWhiteSpace() )
			{
				if( ExtendedAddresses != null && ExtendedAddresses.Count > 0 )
				{
					SortedDictionary<string, ExtendedCompanyAddress> sd = new SortedDictionary<string, ExtendedCompanyAddress>();

					foreach( ExtendedCompanyAddress eca in ExtendedAddresses )
					{
						if( eca.Ca.Region == FilterSelectedRegion )
						{
							if( !sd.ContainsKey( eca.Ca.CompanyName.ToLower() ) )
							{
								sd.Add( eca.Ca.CompanyName.ToLower(), eca );
							}
						}
					}

					List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

					foreach( string key in sd.Keys )
					{
						ecas.Add( sd[ key ] );
					}

					Dispatcher.Invoke( () =>
					                   {
						                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( ecas );
					                   } );
				}
			}
			else
			{
				// Reload unfiltered list
				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
				                   } );
			}
		}

		public void Execute_ClearFilter()
		{
			Logging.WriteLogLine( "Clearing Filter" );
			FilterCompanyName     = string.Empty;
			FilterLocationBarcode = string.Empty;
			FilterSelectedCity    = string.Empty;
			FilterSelectedRegion  = string.Empty;

			// Reload unfiltered list
			Dispatcher.Invoke( () =>
			                   {
				                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
			                   } );
		}

	#endregion


	#region Groups

		public int NextGroupNumber
		{
			get { return Get( () => NextGroupNumber, -1 ); }
			set { Set( () => NextGroupNumber, value ); }
		}


		public List<CompanyAddressGroup> Groups
		{
			get { return Get( () => Groups, LoadGroups ); }
			set { Set( () => Groups, value ); }
		}


		public List<string> GroupNames
		{
			get { return Get( () => GroupNames, new List<string>() ); }
			set { Set( () => GroupNames, value ); }
		}

		[DependsUpon(nameof(GroupNames))]
		public void WhenGroupNamesChanges()
		{
			string gns = string.Empty;
			foreach (string gn in GroupNames)
			{
				gns += (gn + ", ");
			}
			Logging.WriteLogLine("GroupNames has changed: " + gns);
		}

		/// <summary>
		/// Holds a map of group names and their companies.
		/// </summary>
		private readonly SortedDictionary<string, List<string>> GroupsAndMembers = new SortedDictionary<string, List<string>>();

		/// <summary>
		/// Starts the process of loading all group lists.
		/// </summary>
		/// <returns></returns>
		private List<CompanyAddressGroup> LoadGroups()
		{
			Logging.WriteLogLine( "Loading groups" );
			List<CompanyAddressGroup> ags = new List<CompanyAddressGroup>();

			Task.Run( async () =>
			          {
				          CompanyAddressGroups cags = null;

				          try
				          {
					          cags = await Azure.Client.RequestGetCompanyAddressGroups();
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown: " + e.ToString() );
				          }

				          if( cags != null )
				          {
					          foreach( CompanyAddressGroup cag in cags )
					          {
						          ags.Add( cag );
					          }

					          //
					          // TODO Deletions are going to leave this out of sync
					          //
					          NextGroupNumber = cags.Count;
					          Logging.WriteLogLine( "Found " + ags.Count + " groups ; next GroupNumber: " + NextGroupNumber );

					          Groups = ags;
					          LoadGroupNames();
				          }
			          } );

			return ags;
		}


		private void LoadGroupNames()
		{
			Task.Run( async () =>
			          {
				          SortedDictionary<string, string> sd     = new SortedDictionary<string, string>();
				          SortedDictionary<string, int>    counts = new SortedDictionary<string, int>();
				          GroupsAndMembers.Clear();

				          foreach( CompanyAddressGroup cag in Groups )
				          {
					          //if( !sd.ContainsKey( cag.Description.ToLower() ) )
					          if( !sd.ContainsKey( cag.Description ) )
					          {
						          //sd.Add( cag.Description.ToLower(), cag.Description );
						          sd.Add( cag.Description, cag.Description );

						          // Now get the number of companies in this group
						          try
						          {
							          CompaniesWithinAddressGroups companies = await Azure.Client.RequestGetCompaniesWithinAddressGroup( cag.GroupNumber );

							          if( companies.Count > 0 && companies[ 0 ].Companies != null )
							          {
								          int count = companies[ 0 ].Companies.Count;
								          counts.Add( cag.Description.ToLower(), count );

								          SortedDictionary<string, string> sd1 = new SortedDictionary<string, string>();

								          foreach( string company in companies[ 0 ].Companies )
								          {
									          //sd1.Add( company.ToLower(), company );
									          if( !sd1.ContainsKey( company ) )
									          {
										          sd1.Add( company, company );
									          }
								          }

								          List<string> sorted = new List<string>();

								          foreach( string key in sd1.Keys )
								          {
									          sorted.Add( sd1[ key ] );
								          }

								          GroupsAndMembers.Add( cag.Description, sorted );
							          }
						          }
						          catch( Exception e )
						          {
							          Logging.WriteLogLine( "Exception thrown: " + e.ToString() );
						          }
					          }
				          }

				          string       members        = (string)Application.Current.TryFindResource( "AddressBookGroupsMembers" );
				          string       labelSingle    = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountSingle" );
				          string       labelNotSingle = (string)Application.Current.TryFindResource( "AddressBookGroupsMembersCountNotSingle" );
				          List<string> list           = new List<string>();

				          foreach( string key in sd.Keys )
				          {
							  if (counts.ContainsKey(key))
							  {
								  string line = sd[ key ] + " \t(" + counts[ key ] + " ";

								  if( counts[ key ] != 1 )
								  {
									  //line = sd[key] + " \t(" + counts[key] + " members)"; 
									  line += labelNotSingle + ")";
								  }
								  else
								  {
									  line += labelSingle + ")";
								  }

								  Logging.WriteLogLine("line: " + line);
								  list.Add( line );
							  }
							  else
							  {
								  Logging.WriteLogLine("counts doesn't contain the key: " + key);
								  foreach (string tmp in counts.Keys)
								  {
									  Logging.WriteLogLine("counts keys: " + tmp);
								  }
							  }
				          }

				          GroupNames = list;

				          if( SelectedAddress == null )
				          {
					          LoadAddressesGroupNames();
				          }
					      Loaded = true;
			          } );
		}


		public string NewGroupDescription
		{
			get { return Get( () => NewGroupDescription, "" ); }
			set { Set( () => NewGroupDescription, value ); }
		}

		[DependsUpon( nameof( NewGroupDescription ) )]
		public bool IsCreateGroupEnabled
		{
			// get { return Get(() => IsCreateEnabled, false); }
			get => NewGroupDescription.IsNotNullOrWhiteSpace();
			set { Set( () => IsCreateGroupEnabled, value ); }
		}


		public void Execute_CreateGroup()
		{
			Logging.WriteLogLine( "Creating group: " + NewGroupDescription );

			bool exists = CreateGroup( NewGroupDescription );

			if( exists )
			{
				NewGroupDescription = string.Empty;
			}
		}

		private bool CreateGroup( string groupName )
		{
			bool exists = false;

			foreach( CompanyAddressGroup ag in Groups )
			{
				if( ag.Description.ToLower() == groupName.ToLower() )
				{
					exists = true;

					break;
				}
			}

			if( !exists )
			{
				//Task.WaitAll(Task.Run(() =>
				Task.Run( async () =>
				          {
					          try
					          {
						          NextGroupNumber = GetNextGroupNumber();

						          CompanyAddressGroup cag = new CompanyAddressGroup
						                                    {
							                                    Description = groupName,
							                                    GroupNumber = NextGroupNumber
						                                    };
						          await Azure.Client.RequestAddUpdateCompanyAddressGroup( cag );

						          string title   = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupCreatedTitle" );
						          string message = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupCreated" );
						          message = message.Replace( "@1", groupName );

						          Dispatcher.Invoke( () =>
						                             {
							                             MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );
							                             groupName = string.Empty;
							                             LoadGroups();
						                             } );
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "Exception thrown: " + e.ToString() );
					          }
				          } );
			}
			else
			{
				string title   = (string)Application.Current.TryFindResource( "AddressBookGroupsErrorGroupAlreadyExistsTitle" );
				string message = (string)Application.Current.TryFindResource( "AddressBookGroupsErrorGroupAlreadyExists" );
				message = message.Replace( "@1", groupName );

				Dispatcher.Invoke( () =>
				                   {
					                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
				                   } );
			}

			return exists;
		}

		private int GetNextGroupNumber()
		{
			int NextGroupNumber = -1;

			HashSet<int> hsLocal = new HashSet<int>();

			foreach( CompanyAddressGroup cag in Groups )
			{
				hsLocal.Add( cag.GroupNumber );
			}

			CompanyAddressGroups cags = null;

			try
			{
				cags = Azure.Client.RequestGetCompanyAddressGroups().Result;
			}
			catch( Exception e )
			{
				Logging.WriteLogLine( "Exception thrown: " + e.ToString() );
			}

			if( cags != null )
			{
				HashSet<int> hsRemote = new HashSet<int>();

				foreach( CompanyAddressGroup cag in cags )
				{
					hsRemote.Add( cag.GroupNumber );
				}

				for( int i = 0; i < int.MaxValue; i++ )
				{
					// Make sure that it isn't used locally
					if( !hsRemote.Contains( i ) && !hsLocal.Contains( i ) )
					{
						NextGroupNumber = i;

						break;
					}
				}
			}

			return NextGroupNumber;
		}

		public List<ExtendedCompanyAddress> SelectedAddresses
		{
			get { return Get( () => SelectedAddresses, new List<ExtendedCompanyAddress>() ); }
			set { Set( () => SelectedAddresses, value ); }
		}


		public void Execute_AddSelectedToGroup()
		{
			if( SelectedAddresses.Count > 0 )
			{
				List<string> names = new List<string>();
				names.AddRange( GroupAddressNames );

				foreach( var tmp in SelectedAddresses )
				{
					if( !names.Contains( tmp.Ca.CompanyName ) )
					{
						names.Add( tmp.Ca.CompanyName );
					}
					else
					{
						Logging.WriteLogLine( "Skipping " + tmp.Ca.CompanyName + " - already in list" );
					}
				}

				GroupAddressNames = names;
			}
		}


		public void Execute_RemoveSelectedFromGroup()
		{
			if( SelectedGroupMembers.Count > 0 )
			{
				List<string> keep = new List<string>();

				foreach( string gan in GroupAddressNames )
				{
					if( !SelectedGroupMembers.Contains( gan ) )
					{
						keep.Add( gan );
					}
				}

				GroupAddressNames = keep;
			}
		}


		public List<string> SelectedGroupMembers
		{
			get { return Get( () => SelectedGroupMembers, new List<string>() ); }
			set { Set( () => SelectedGroupMembers, value ); }
		}


		public List<string> GroupAddressNames
		{
			get { return Get( () => GroupAddressNames, new List<string>() ); }
			set { Set( () => GroupAddressNames, value ); }
		}


		public string SelectedGroup
		{
			get { return Get( () => SelectedGroup, "" ); }
			set { Set( () => SelectedGroup, value ); }
		}


		[DependsUpon(nameof(SelectedGroup))]
		public bool IsSelectedGroupPopulated()
		{
			bool yes = SelectedGroup.IsNotNullOrWhiteSpace();
			return yes;
		}


		public int SelectedGroupTab
		{
			get { return Get( () => SelectedGroupTab, 0 ); }
			set { Set( () => SelectedGroupTab, value ); }
		}


		public void Execute_ModifyGroup()
		{
			if( SelectedGroup.IsNotNullOrWhiteSpace() )
			{
				Logging.WriteLogLine( "Modifying " + SelectedGroup );

				SelectedGroupTab = 1;

				// Load the addresses for the group
				Task.Run( async () =>
				          {
					          CompanyAddressGroup cag = GetGroupForName( SelectedGroup );

					          if( cag != null )
					          {
						          GroupAddressNames.Clear();
						          var addresses = await Azure.Client.RequestGetCompaniesWithinAddressGroup( cag.GroupNumber );

						          if( addresses != null && addresses.Count > 0 && addresses[ 0 ].Companies.Count > 0 )
						          {
							          SortedDictionary<string, CompanyAddress> sd = new SortedDictionary<string, CompanyAddress>();

							          foreach( var tmp in addresses[ 0 ].Companies )
							          {
								          //ResellerCustomerCompanyLookup rccl = await Azure.Client.RequestGetCustomerCompanyAddress(SelectedAccountId, tmp);
								          //if (rccl != null)
								          //{
								          //    CompanyAddress ca = ResellerCustomerCompanyLookupToCompanyAddress(rccl);
								          //    sd.Add(tmp, ca);
								          //}
								          sd.Add( tmp, null );
							          }

							          List<string> sorted = new List<string>();

							          foreach( string key in sd.Keys )
							          {
								          sorted.Add( key );
							          }

							          GroupAddressNames = sorted;
						          }
					          }
				          } );
			}
		}

		public void Execute_SaveGroup()
		{
			bool success = SaveGroup( SelectedGroup, GroupAddressNames );
			SelectedGroup = string.Empty;
			GroupAddressNames.Clear();
			NewGroupDescription = string.Empty;
			SelectedGroupTab    = 0;
		}

		private bool SaveGroup( string group, List<string> names )
		{
			bool success = true;

			List<string> copyOfNames = new List<string>();
			copyOfNames.AddRange(names);

			Logging.WriteLogLine( "Saving Group " + group + " with " + copyOfNames.Count + " Addresses" );

			CompanyAddressGroup cag = GetGroupForName( group );

			Task.Run( async () =>
			          {
				          if( cag == null )
				          {
					          NextGroupNumber = GetNextGroupNumber();

					          cag = new CompanyAddressGroup
					                {
						                Description = group,
						                GroupNumber = NextGroupNumber
					                };
					          await Azure.Client.RequestAddUpdateCompanyAddressGroup( cag );
				          }
				          else
				          {
					          // First, empty the group by deleting and adding it
					          await Azure.Client.RequestDeleteCompanyAddressGroup( cag.GroupNumber );
				          }

				          // First, empty the group by deleting and adding it
				          //await Azure.Client.RequestDeleteCompanyAddressGroup(cag.GroupNumber);
				          await Azure.Client.RequestAddUpdateCompanyAddressGroup( cag );

				          if(copyOfNames.Count > 0 )
				          {
					          foreach( string gan in copyOfNames)
					          {
						          Logging.WriteLogLine( "Adding " + gan + " to group " + group );

						          CompanyAddressGroupEntry cage = new CompanyAddressGroupEntry
						                                          {
							                                          CompanyName = gan,
							                                          GroupNumber = cag.GroupNumber
						                                          };
						          await Azure.Client.RequestAddCompanyAddressGroupEntry( cage );
					          }
				          }

				          string title   = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupSavedTitle" );
				          string message = (string)Application.Current.TryFindResource( "AddressBookGroupsGroupSaved" );

				          //message = message.Replace("@1", group);
				          string tmp = group;

				          if( tmp.Contains( "\t" ) )
				          {
					          tmp = tmp.Substring( 0, tmp.IndexOf( "\t" ) );
				          }

				          message = message.Replace( "@1", tmp );

				          Dispatcher.Invoke( () =>
				                             {
					                             MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );
					                             NewGroupDescription = string.Empty;

												 //group = string.Empty;
												 copyOfNames.Clear();

					                             LoadGroups();
				                             } );
			          } );

			return success;
		}

		public IList GroupAddresses
		{
			//get { return Get(() => GroupAddresses, new List<CompaniesWithinAddressGroup>()); }
			//get { return Get(() => GroupAddresses, new List<CompanyAddress>()); }
			get { return Get( () => GroupAddresses, new List<string>() ); }
			set { Set( () => GroupAddresses, value ); }
		}


		private CompanyAddressGroup GetGroupForName( string name )
		{
			CompanyAddressGroup cag = null;

			// "test group 0 \t(0 members)"
			if( name.IndexOf( "\t" ) > -1 )
			{
				name = name.Substring( 0, name.IndexOf( "\t" ) ).TrimEnd();
			}

			foreach( CompanyAddressGroup tmp in Groups )
			{
				if( tmp.Description == name )
				{
					cag = tmp;

					break;
				}
			}

			return cag;
		}

		public void Execute_DeleteGroup()
		{
			Logging.WriteLogLine( "Deleting " + SelectedGroup );

			Task.Run( async () =>
			          {
				          string groupName = SelectedGroup.Substring( 0, SelectedGroup.IndexOf( "\t" ) ).TrimEnd();

				          foreach( CompanyAddressGroup cag in Groups )
				          {
					          if( cag.Description == groupName )
					          {
						          await Azure.Client.RequestDeleteCompanyAddressGroup( cag.GroupNumber );

						          Dispatcher.Invoke( () =>
						                             {
							                             LoadGroups();
						                             } );

						          break;
					          }
				          }
			          } );
		}


		//
		// Modify
		//

		public void Execute_CancelModifyGroup()
		{
			Logging.WriteLogLine( "Modify Group cancelled: " + SelectedGroup );

			//SelectedGroup = string.Empty;
			SelectedGroupTab = 0;
		}


		public List<string> MemberOfGroups
		{
			get { return Get( () => MemberOfGroups, new List<string>() ); }
			set { Set( () => MemberOfGroups, value ); }
		}


		public List<string> AvailableGroups
		{
			get { return Get( () => AvailableGroups, new List<string>() ); } // LoadAddressesGroupNames()
			set { Set( () => AvailableGroups, value ); }
		}


		private void LoadAddressesGroupNames()
		{
			List<string> mogList = new List<string>();

			MemberOfGroups.Clear();
			AvailableGroups.Clear();

			SortedDictionary<string, string> sdMog = new SortedDictionary<string, string>();
			SortedDictionary<string, string> sdAg  = new SortedDictionary<string, string>();

			foreach( string key in GroupsAndMembers.Keys )
			{
				if( GroupsAndMembers[ key ].Contains( AddressDescription ) )
				{
					sdMog.Add( key.ToLower(), key );
				}
				else
				{
					sdAg.Add( key.ToLower(), key );
				}
			}

			if( sdMog.Count > 0 )
			{
				foreach( string mog in sdMog.Keys )
				{
					mogList.Add( sdMog[ mog ] );
				}
			}

			MemberOfGroups = mogList;

			List<string> agList = new List<string>();

			if( sdAg.Count > 0 )
			{
				foreach( string ag in sdAg.Keys )
				{
					agList.Add( sdAg[ ag ] );
				}
			}

			AvailableGroups = agList;

			//foreach (string name in GroupNames)
			//{
			//    // "test group 0 \t(0 members)"
			//    string item = name.Substring(0, name.IndexOf("\t")).TrimEnd();

			//    if (MemberOfGroups.Count > 0)
			//    {
			//        if (!MemberOfGroups.Contains(item))
			//        {
			//            list.Add(item);
			//        }
			//    }
			//    else
			//    {
			//        list.Add(item);
			//    }
			//}

			//return list;
		}


		public string MemberOfGroupsSelected
		{
			get { return Get( () => MemberOfGroupsSelected, "" ); }
			set { Set( () => MemberOfGroupsSelected, value ); }
		}


		public string AvailableGroupsSelected
		{
			get { return Get( () => AvailableGroupsSelected, "" ); }
			set { Set( () => AvailableGroupsSelected, value ); }
		}


		public void Execute_RemoveFromMemberOfGroups()
		{
			if( MemberOfGroupsSelected.IsNotNullOrWhiteSpace() )
			{
				SortedDictionary<string, string> sd = new SortedDictionary<string, string>
				                                      {
					                                      { MemberOfGroupsSelected.ToLower(), MemberOfGroupsSelected }
				                                      };

				foreach( string group in AvailableGroups )
				{
					sd.Add( group.ToLower(), group );
				}

				List<string> list = new List<string>();

				foreach( string key in sd.Keys )
				{
					list.Add( sd[ key ] );
				}

				List<string> mogs = new List<string>();

				foreach( string key in MemberOfGroups )
				{
					if( key != MemberOfGroupsSelected )
					{
						mogs.Add( key );
					}
				}

				Dispatcher.Invoke( () =>
				                   {
					                   AvailableGroups = list;

					                   //MemberOfGroups.Remove(MemberOfGroupsSelected);
					                   MemberOfGroups = mogs;
				                   } );
			}
		}


		public void Execute_AddToMemberOfGroups()
		{
			if( AvailableGroupsSelected.IsNotNullOrWhiteSpace() )
			{
				SortedDictionary<string, string> sd = new SortedDictionary<string, string>
				                                      {
					                                      { AvailableGroupsSelected.ToLower(), AvailableGroupsSelected }
				                                      };

				foreach( string group in MemberOfGroups )
				{
					sd.Add( group.ToLower(), group );
				}

				List<string> list = new List<string>();

				foreach( string key in sd.Keys )
				{
					list.Add( sd[ key ] );
				}

				List<string> ags = new List<string>();

				foreach( string key in AvailableGroups )
				{
					if( key != AvailableGroupsSelected )
					{
						ags.Add( key );
					}
				}

				Dispatcher.Invoke( () =>
				                   {
					                   MemberOfGroups = list;

					                   // AvailableGroups.Remove(AvailableGroupsSelected);
					                   AvailableGroups = ags;
				                   } );
			}
		}

	#endregion

	#region Data

		public bool IsDescriptionReadOnly
		{
			get { return Get( () => IsDescriptionReadOnly, false ); }
			set { Set( () => IsDescriptionReadOnly, value ); }
		}

		[DependsUpon( nameof( IsDescriptionReadOnly ) )]
		public bool IsDescriptionEnabled
		{
			get { return !IsDescriptionReadOnly; }

			//get { return Get(() => !IsDescriptionReadOnly, true); }
			//set { Set(() => IsDescriptionReadOnly, value); }
		}


		public IList Countries
		{
			get
			{
				List<string> countries = Utils.CountriesRegions.Countries;

				return countries;
			}
			set { Set( () => Countries, value ); }
		}


		public IList Regions
		{
			get { return Get( () => Regions ); }
			set { Set( () => Regions, value ); }
		}

		public List<string> AccountNames
		{
			get { return Get( () => AccountNames, new List<string>() ); }

			//get { return Get(() => AccountNames); }
			set { Set( () => AccountNames, value ); }
		}


		public ObservableCollection<ExtendedCompanyAddress> ExtendedAddresses
		{
			get { return Get( () => ExtendedAddresses ); }
			set { Set( () => ExtendedAddresses, value ); }
		}


		public string SelectedAccountName
		{
			get { return Get( () => SelectedAccountName, "" ); }
			set { Set( () => SelectedAccountName, value ); }
		}


		public string SelectedAccountId
		{
			get { return Get( () => SelectedAccountId, "" ); }

			//get
			//{
			//    // Load the Account

			//    return SelectedAccountId;
			//}
			set { Set( () => SelectedAccountId, value ); }
		}


		public string SelectedAccountOutstandingFormatted
		{
			get { return Get( () => SelectedAccountOutstandingFormatted, GetOutstandingForSelectedAccountFormatted ); }
			set { Set( () => SelectedAccountOutstandingFormatted, value ); }
		}


		public ObservableCollection<CompanyAddress> Addresses
		{
			get { return Get( () => Addresses ); }
			set { Set( () => Addresses, value ); }
		}


		public bool IsGlobalAddressBook
		{
			get { return Get( () => IsGlobalAddressBook, false ); }
			set { Set( () => IsGlobalAddressBook, value ); }
		}


		public bool IsAccountAddressBook
		{
			get { return Get( () => IsAccountAddressBook, true ); }
			set { Set( () => IsAccountAddressBook, value ); }
		}


		public string AddressFilter
		{
			get { return Get( () => AddressFilter, "" ); }
			set { Set( () => AddressFilter, value ); }
		}


		public ExtendedCompanyAddress SelectedAddress
		{
			get { return Get( () => SelectedAddress ); }
			set { Set( () => SelectedAddress, value ); }
		}


		// Address Details section


		public string AddressDescription
		{
			get { return Get( () => AddressDescription, "" ); }
			set { Set( () => AddressDescription, value ); }
		}

		//[DependsUpon(nameof(AddressDescription))]
		//private void WhenAddressDescriptionChanges()
		//{
		//    // Check that the description is unique
		//}


		public string AddressLocationBarcode
		{
			get { return Get( () => AddressLocationBarcode, "" ); }
			set { Set( () => AddressLocationBarcode, value ); }
		}


		public string AddressSuite
		{
			get { return Get( () => AddressSuite, "" ); }
			set { Set( () => AddressSuite, value ); }
		}


		public string AddressStreet
		{
			get { return Get( () => AddressStreet, "" ); }
			set { Set( () => AddressStreet, value ); }
		}


		public string AddressCity
		{
			get { return Get( () => AddressCity, "" ); }
			set { Set( () => AddressCity, value ); }
		}


		public string AddressZipPostal
		{
			get { return Get( () => AddressZipPostal, "" ); }
			set { Set( () => AddressZipPostal, value ); }
		}


		public string AddressProvState
		{
			get { return Get( () => AddressProvState, "" ); }
			set { Set( () => AddressProvState, value ); }
		}


		public string AddressCountry
		{
			get { return Get( () => AddressCountry, "" ); }
			set { Set( () => AddressCountry, value ); }
		}

		[DependsUpon( nameof( AddressCountry ) )]
		public void WhenAddressCountryChanges()
		{
			List<string> regions = null;

			switch( AddressCountry )
			{
			case "Australia":
				regions = Utils.CountriesRegions.AustraliaRegions;

				break;

			case "Canada":
				regions = Utils.CountriesRegions.CanadaRegions;

				break;

			case "United States":
				regions = Utils.CountriesRegions.USRegions;

				break;
			}

			Regions = regions;
		}

		public string AddressContactName
		{
			get { return Get( () => AddressContactName, "" ); }
			set { Set( () => AddressContactName, value ); }
		}


		public string AddressContactPhone
		{
			get { return Get( () => AddressContactPhone, "" ); }
			set { Set( () => AddressContactPhone, value ); }
		}


		public string AddressContactEmail
		{
			get { return Get( () => AddressContactEmail, "" ); }
			set { Set( () => AddressContactEmail, value ); }
		}


		public string AddressNotes
		{
			get { return Get( () => AddressNotes, "" ); }
			set { Set( () => AddressNotes, value ); }

			//set
			//{
			//    Logging.WriteLogLine("Setting AddressNotes: " + value);
			//    AddressNotes = value;
			//}
		}


		// Export Addresses section


		public string ExportDescription
		{
			get { return Get( () => ExportDescription, "" ); }
			set { Set( () => ExportDescription, value ); }
		}


		public bool ExportIsExclude
		{
			get { return Get( () => ExportIsExclude, false ); }
			set { Set( () => ExportIsExclude, value ); }
		}


		//
		// Data Methods
		//


		[DependsUpon( nameof( SelectedAccountName ) )]
		public void WhenSelectedAccountNameChanges()
		{
			if( !string.IsNullOrEmpty( SelectedAccountName ) )
			{
				if( dictCustomerNamesIds.ContainsKey( SelectedAccountName ) )
				{
					SelectedAccountId = dictCustomerNamesIds[ SelectedAccountName ];

					LoadAddresses();

					//GetCities();
				}
			}
		}

		private string GetOutstandingForSelectedAccountFormatted()
		{
			decimal raw = 0.0M; // TODO 

			string outstanding = string.Format( "{0:c}", raw );

			return outstanding;
		}

		private List<string> GetAccountNames()
		{
			List<string> names = new List<string>();
			Loaded = false;

			Task.Run( async () =>
			          {
				          try
				          {
					          if( Addresses == null || Addresses.Count == 0 )
					          {
						          if( !IsInDesignMode )
						          {
							          if( AccountNames != null )
							          {
								          AccountNames.Clear();
							          }
							          else
							          {
								          AccountNames = new List<string>();
							          }

							          //if (Addresses != null)
							          //{
							          //    Addresses.Clear();
							          //}
							          //else
							          //{
							          //    Addresses = new ObservableCollection<CompanyAddress>();
							          //}
							          if( ExtendedAddresses != null )
							          {
								          ExtendedAddresses.Clear();
							          }
							          else
							          {
								          ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>();
							          }

							          CustomerCodeList ccl = await Azure.Client.RequestGetCustomerCodeList();

							          Logging.WriteLogLine( "Found " + ccl.Count + " Customers" );

							          if( ccl.Count > 0 )
							          {
								          SortedDictionary<string, string> Sd = new SortedDictionary<string, string>();

								          //SortedDictionary<string, CompanyAddress> sdCas = new SortedDictionary<string, CompanyAddress>();
								          SortedDictionary<string, ExtendedCompanyAddress> sdExtendedCas = new SortedDictionary<string, ExtendedCompanyAddress>();

								          //List<CompanyAddress> cas = new List<CompanyAddress>();
								          List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

								          foreach( string id in ccl )
								          {
									          CompanyAddress ca = await Azure.Client.RequestGetResellerCustomerCompany( id );

									          //if( !Sd.ContainsKey( ca.CompanyName.ToLower() ) )
									          if( !Sd.ContainsKey( ca.CompanyName ) )
									          {
										          //Sd.Add( ca.CompanyName.ToLower(), ca.CompanyName );
										          Sd.Add( ca.CompanyName, ca.CompanyName );
									          }

									          //if( !sdExtendedCas.ContainsKey( ca.CompanyName.ToLower() ) )
									          if( !sdExtendedCas.ContainsKey( ca.CompanyName ) )
									          {
										          //sdCas.Add(ca.CompanyName.ToLower(), ca);
										          //sdExtendedCas.Add( ca.CompanyName.ToLower(), new ExtendedCompanyAddress( ca ) );
										          sdExtendedCas.Add( ca.CompanyName, new ExtendedCompanyAddress( ca ) );
									          }

									          if( !dictCustomerNamesIds.ContainsKey( ca.CompanyName ) )
									          {
										          dictCustomerNamesIds.Add( ca.CompanyName, id );
									          }
								          }

								          foreach( string Key in Sd.Keys )
								          {
									          names.Add( Sd[ Key ] );

									          //cas.Add(sdCas[Key]);

									          ecas.Add( sdExtendedCas[ Key ] );
								          }

								          Dispatcher.Invoke( () =>
								                             {
									                             AccountNames = names;

									                             //SelectedAccountName = names[0];
									                             //Addresses = new ObservableCollection<CompanyAddress>(cas);
									                             //ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>(ecas);
									                             //Loaded = true;
								                             } );
							          }
						          }
					          }
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown fetching customers:\n" + e.ToString() );
				          }
			          } );

			return names;
		}

		private List<string> GetAccountNamesNotAsync()
		{
			List<string> names = new List<string>();

			Task.Run( () =>
			          {
				          try
				          {
					          if( !IsInDesignMode )
					          {
						          if( AccountNames != null )
						          {
							          AccountNames.Clear();
						          }
						          else
						          {
							          AccountNames = new List<string>();
						          }

						          if( Addresses != null )
						          {
							          Addresses.Clear();
						          }
						          else
						          {
							          Addresses = new ObservableCollection<CompanyAddress>();
						          }

						          CustomerCodeList ccl = Azure.Client.RequestGetCustomerCodeList().Result;
						          Logging.WriteLogLine( "Found " + ccl.Count + " Customers" );

						          if( ccl != null && ccl.Count > 0 )
						          {
							          SortedDictionary<string, string>         Sd    = new SortedDictionary<string, string>();
							          SortedDictionary<string, CompanyAddress> sdCas = new SortedDictionary<string, CompanyAddress>();
							          List<CompanyAddress>                     cas   = new List<CompanyAddress>();

							          foreach( string id in ccl )
							          {
								          CompanyAddress ca = Azure.Client.RequestGetResellerCustomerCompany( id ).Result;

								          if( !Sd.ContainsKey( ca.CompanyName.ToLower() ) )
								          {
									          Sd.Add( ca.CompanyName.ToLower(), ca.CompanyName );
								          }

								          if( !sdCas.ContainsKey( ca.CompanyName.ToLower() ) )
								          {
									          sdCas.Add( ca.CompanyName.ToLower(), ca );
								          }

								          if( !dictCustomerNamesIds.ContainsKey( ca.CompanyName ) )
								          {
									          dictCustomerNamesIds.Add( ca.CompanyName, id );
								          }
							          }

							          foreach( string Key in Sd.Keys )
							          {
								          names.Add( Sd[ Key ] );
								          cas.Add( sdCas[ Key ] );
							          }

							          Dispatcher.Invoke( () =>
							                             {
								                             AccountNames        = names;
								                             SelectedAccountName = names[ 0 ];
								                             Addresses           = new ObservableCollection<CompanyAddress>( cas );
							                             } );
						          }
					          }
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown fetching customers:\n" + e.ToString() );
				          }
			          } );

			return names;
		}

		private void LoadAddresses()
		{
			if( ExtendedAddresses != null )
			{
				ExtendedAddresses.Clear();
			}
			else
			{
				ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>();
			}

			currentCompanyAddresses.Clear();

			Task.Run( async () =>
			          {
				          try
				          {
					          Loaded = false;

					          if( !IsInDesignMode )
					          {
						          Logging.WriteLogLine( "Loading addresses for " + SelectedAccountName + " (" + SelectedAccountId + ")..." );

						          var Details = await Azure.Client.RequestGetCustomerCompaniesDetailed( SelectedAccountId );

						          SortedDictionary<string, ExtendedCompanyAddress> sdExtended = new SortedDictionary<string, ExtendedCompanyAddress>();

						          foreach( var Detail in Details )
						          {
							          var Company = Detail.Company;

							          if( Company.CompanyName.IsNotNullOrWhiteSpace() )
							          {
								          var Key = Company.CompanyName.ToLower();

								          if( !sdExtended.ContainsKey( Key ) )
									          sdExtended.Add( Key, new ExtendedCompanyAddress( Detail.Address ) );
								          else
									          Logging.WriteLogLine( "Duplicate: " + Company.CompanyName.ToLower() );
							          }
						          }

/*
						          var csl = await Azure.Client.RequestGetCustomerCompaniesSummary( SelectedAccountId );
						          SortedDictionary<string, ExtendedCompanyAddress> sdExtended = new SortedDictionary<string, ExtendedCompanyAddress>();

						          foreach( CompanyByAccountSummary cbas in csl )
						          {
							          Logging.WriteLogLine( "Loading address: " + cbas.CompanyName + ", LocationBarcode: " + cbas.LocationBarcode );

							          ResellerCustomerCompanyLookup tmp = await Azure.Client.RequestGetCustomerCompanyAddress( cbas.CustomerCode, cbas.CompanyName );

							          if( tmp != null && tmp.CompanyName.IsNotNullOrWhiteSpace() )
							          {
								          //Logging.WriteLogLine("Found: " + tmp.AddressLine1 + " for " + tmp.CompanyName);

								          CompanyAddress ca = ResellerCustomerCompanyLookupToCompanyAddress( tmp );

								          if( ca.LocationBarcode.IsNullOrWhiteSpace() && cbas.LocationBarcode.IsNotNullOrWhiteSpace() )
								          {
									          //Logging.WriteLogLine("Updating ca with LocationBarcode: " + cbas.LocationBarcode);
									          ca.LocationBarcode = cbas.LocationBarcode;
								          }

								          string key = ca.CompanyName.ToLower() + ca.CompanyNumber;

								          if( !sdExtended.ContainsKey( key ) )
								          {
									          sdExtended.Add( key, new ExtendedCompanyAddress( ca ) );
								          }
								          else
								          {
									          Logging.WriteLogLine( "Duplicate: " + ca.CompanyName.ToLower() + " in " + ca.CompanyNumber );
								          }
							          }

						          }
*/
						          Dispatcher.Invoke( () =>
						                             {
							                             ExtendedAddresses.Clear();
							                             currentCompanyAddresses.Clear();

							                             foreach( string key1 in sdExtended.Keys )
							                             {
								                             Logging.WriteLogLine( "eca: " + sdExtended[ key1 ].Ca.CompanyName + ", LocationBarcode: " + sdExtended[ key1 ].Ca.LocationBarcode );
								                             currentCompanyAddresses.Add( sdExtended[ key1 ] );
								                             ExtendedAddresses.Add( sdExtended[ key1 ] );
							                             }

							                             if( ExtendedAddresses.Count > 0 )
							                             {
								                             foreach( ExtendedCompanyAddress eca in ExtendedAddresses )
								                             {
									                             string country = eca.Ca.Country;

									                             if( country.IsNotNullOrWhiteSpace() )
									                             {
										                             AddressCountry = country;
										                             Regions        = CountriesRegions.GetRegionsForCountry( country );
										                             FilterRegions  = CountriesRegions.GetRegionsForCountry( country );

										                             if( FilterRegions.Count > 0 && FilterRegions[ 0 ].IsNotNullOrWhiteSpace() )
										                             {
											                             FilterRegions.Insert( 0, "" );
										                             }

										                             break;
									                             }
								                             }

								                             GetCities();
							                             }

							                             Loaded = true;
						                             } );
					          }
				          }
				          catch( Exception e )
				          {
					          Logging.WriteLogLine( "Exception thrown fetching addresses:\n" + e.ToString() );
				          }
			          } );
		}

		private CompanyAddress ResellerCustomerCompanyLookupToCompanyAddress( ResellerCustomerCompanyLookup src )
		{
			CompanyAddress ca = new CompanyAddress();

			ca.AddressLine1    = src.AddressLine1;
			ca.AddressLine2    = src.AddressLine2;
			ca.Barcode         = src.Barcode;
			ca.City            = src.City;
			ca.CompanyName     = src.CompanyName;
			ca.CompanyNumber   = src.CustomerCode;
			ca.Country         = src.Country;
			ca.CountryCode     = src.CountryCode;
			ca.EmailAddress    = src.EmailAddress;
			ca.EmailAddress1   = src.EmailAddress1;
			ca.EmailAddress2   = src.EmailAddress2;
			ca.Fax             = src.Fax;
			ca.Latitude        = src.Latitude;
			ca.LocationBarcode = src.LocationBarcode;
			ca.Longitude       = src.Longitude;
			ca.Mobile          = src.Mobile;
			ca.Mobile1         = src.Mobile1;
			ca.Notes           = src.Notes;
			ca.Phone           = src.Phone;
			ca.Phone1          = src.Phone1;
			ca.PostalBarcode   = src.PostalBarcode;
			ca.PostalCode      = src.PostalCode;
			ca.Region          = src.Region;
			ca.SecondaryId     = src.SecondaryId;
			ca.Suite           = src.Suite;
			ca.Vicinity        = src.Vicinity;

			return ca;
		}

		// [DependsUpon(nameof(AddressFilter))]
		public void WhenAddressFilterChanges( string filter = "" )
		{
			if( !string.IsNullOrEmpty( filter ) )
			{
				AddressFilter = filter;
			}

			Logging.WriteLogLine( "Filter changed: " + AddressFilter );

			if( !string.IsNullOrEmpty( AddressFilter ) )
			{
				List<ExtendedCompanyAddress> ecas = new List<ExtendedCompanyAddress>();

				//foreach (ExtendedCompanyAddress eca in ExtendedAddresses)
				foreach( ExtendedCompanyAddress eca in currentCompanyAddresses )
				{
					if( eca.Ca.CompanyName.ToLower().Contains( AddressFilter ) || eca.AddressString.ToLower().Contains( AddressFilter ) )
					{
						ecas.Add( eca );
					}
				}

				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( ecas );
				                   } );
			}
			else
			{
				// Reload unfiltered list
				//LoadAddresses();
				Dispatcher.Invoke( () =>
				                   {
					                   ExtendedAddresses = new ObservableCollection<ExtendedCompanyAddress>( currentCompanyAddresses );
				                   } );
			}
		}


		public bool IsSelectAccountFromListSelected
		{
			get { return Get( () => IsSelectAccountFromListSelected, true ); }
			set { Set( () => IsSelectAccountFromListSelected, value ); }
		}


		public bool IsGlobalSelected
		{
			get { return Get( () => IsGlobalSelected, false ); }
			set { Set( () => IsGlobalSelected, value ); }
		}


		public string SelectedImportFile
		{
			get { return Get( () => SelectedImportFile, "" ); }
			set { Set( () => SelectedImportFile, value ); }
		}


		public bool IsSelectGroupFromListSelected
		{
			get { return Get( () => IsSelectGroupFromListSelected, true ); }
			set { Set( () => IsSelectGroupFromListSelected, value ); }
		}


		public bool IsNewGroupSelected
		{
			get { return Get( () => IsNewGroupSelected, false ); }
			set { Set( () => IsNewGroupSelected, value ); }
		}


		public string SelectedGroupName
		{
			get { return Get( () => SelectedGroupName, "" ); }
			set { Set( () => SelectedGroupName, value ); }
		}


		public string NewImportGroupName
		{
			get { return Get( () => NewImportGroupName, "" ); }
			set { Set( () => NewImportGroupName, value ); }
		}


		[DependsUpon( nameof( SelectedImportFile ) )]
		[DependsUpon( nameof( IsSelectGroupFromListSelected ) )]
		[DependsUpon( nameof( SelectedGroupName ) )]
		[DependsUpon( nameof( NewImportGroupName ) )]
		[DependsUpon( nameof( IsNewGroupSelected ) )]
		public bool IsImportButtonEnabled
		{
			get
			{
				bool isEnabled = false;

				if( SelectedImportFile.IsNotNullOrWhiteSpace()
				    && ( ( IsSelectGroupFromListSelected && SelectedGroupName.IsNotNullOrWhiteSpace() )
				         || ( IsNewGroupSelected && NewImportGroupName.IsNotNullOrWhiteSpace() && IsNewImportGroupNameUnique() ) ) )
				{
					isEnabled = true;
				}

				return isEnabled;
			}
			set { Set( () => IsImportButtonEnabled, value ); }
		}

		private bool IsNewImportGroupNameUnique()
		{
			bool   unique = true;
			string name   = NewImportGroupName.ToLower();

			foreach( string gn in GroupNames )
			{
				string   testName = gn;
				string[] pieces;

				if( gn.Contains( '(' ) )
				{
					pieces   = gn.Split( '(' );
					testName = pieces[ 0 ].Trim();
				}

				if( testName.ToLower() == name )
				{
					unique = false;

					break;
				}
			}

			return unique;
		}

	#endregion

	#region Actions

		public List<string> AddUpdateAddress( Dictionary<string, Control> controls )
		{
			Logging.WriteLogLine( "Add Update Address clicked" );

			List<string> errors = ValidateAddress( controls );

			if( errors.Count == 0 )
			{
				Logging.WriteLogLine( "Address is valid - saving" );

				// If the AddressDescription is in the list of ExtendedAddresses, 
				// we are updating an existing Address
				ExtendedCompanyAddress address = null;

				//foreach (ExtendedCompanyAddress eca in ExtendedAddresses)
				//{
				//    if (eca.Ca.CompanyName == AddressDescription.Trim())
				//    {
				//        address = eca;
				//        break;
				//    }
				//}

				var matches = from eca in ExtendedAddresses
				              where eca.Ca.CompanyName == AddressDescription.Trim()
				              select eca;

				if( matches.Any() )
				{
					address = matches.FirstOrDefault();
				}

				if( !IsInDesignMode )
				{
					Task.Run( async () =>
					          {
						          try
						          {
							          string  title   = string.Empty;
							          string  message = string.Empty;
							          Company company = BuildCompany();

							          if( address == null )
							          {
								          Logging.WriteLogLine( "Adding new address: " + AddressDescription );

								          AddCustomerCompany acc = new AddCustomerCompany( "AddressBookModel" )
								                                   {
									                                   Company      = company,
									                                   CustomerCode = SelectedAccountId
								                                   };
								          await Azure.Client.RequestAddCustomerCompany( acc );

								          // Now update the address's groups
								          foreach( string mogName in MemberOfGroups )
								          {
									          CompanyAddressGroup cag = GetGroupForName( mogName );

									          // New address - just add the groups
									          Logging.WriteLogLine( "Adding " + company.CompanyName + " to group " + mogName );

									          CompanyAddressGroupEntry cage = new CompanyAddressGroupEntry
									                                          {
										                                          CompanyName = company.CompanyName,
										                                          GroupNumber = cag.GroupNumber
									                                          };
									          await Azure.Client.RequestAddCompanyAddressGroupEntry( cage );
								          }

								          title   = (string)Application.Current.TryFindResource( "AddressBookAddTitle" );
								          message = (string)Application.Current.TryFindResource( "AddressBookAdd" );
							          }
							          else
							          {
								          Logging.WriteLogLine( "Updating address: " + AddressDescription );

								          UpdateCustomerCompany ucc = new UpdateCustomerCompany( "AddressBookModel" )
								                                      {
									                                      Company      = company,
									                                      CustomerCode = SelectedAccountId
								                                      };
								          await Azure.Client.RequestUpdateCustomerCompany( ucc );

								          // Now update the address's groups
								          foreach( string group in GroupsAndMembers.Keys )
								          {
									          if( GroupsAndMembers[ group ].Contains( company.CompanyName ) )
									          {
										          if( !MemberOfGroups.Contains( group ) )
										          {
											          Logging.WriteLogLine( "Removing " + company.CompanyName + " from group " + group );
											          CompanyAddressGroup cag = GetGroupForName( group );

											          CompanyAddressGroupEntry cage = new CompanyAddressGroupEntry
											                                          {
												                                          CompanyName = company.CompanyName,
												                                          GroupNumber = cag.GroupNumber
											                                          };
											          await Azure.Client.RequestDeleteCompanyAddressGroupEntry( cage );
										          }
									          }
									          else if( MemberOfGroups.Contains( group ) )
									          {
										          Logging.WriteLogLine( "Adding " + company.CompanyName + " to group " + group );
										          CompanyAddressGroup cag = GetGroupForName( group );

										          CompanyAddressGroupEntry cage = new CompanyAddressGroupEntry
										                                          {
											                                          CompanyName = company.CompanyName,
											                                          GroupNumber = cag.GroupNumber
										                                          };
										          await Azure.Client.RequestAddCompanyAddressGroupEntry( cage );
									          }
								          }

								          title   = (string)Application.Current.TryFindResource( "AddressBookUpdateTitle" );
								          message = (string)Application.Current.TryFindResource( "AddressBookUpdate" );
							          }

							          LoadGroups();

							          Dispatcher.Invoke( () =>
							                             {
								                             message = message.Replace( "@1", company.CompanyName );
								                             MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );

								                             LoadAddresses();

								                             Execute_ClearAddress();

															 UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
														 } );
						          }
						          catch( Exception e )
						          {
							          Logging.WriteLogLine( "Exception thrown when adding a company: " + e.ToString() );
						          }
					          } );
				}
			}

			return errors;
		}

		public void Execute_DeleteAddress( List<ExtendedCompanyAddress> ecas )
		{
			//if( SelectedAddress != null )
			if( ecas != null && ecas.Count > 0 )
			{
				Logging.WriteLogLine( "Deleting Address: " + SelectedAddress.Ca.CompanyName );

				Task.Run( async () =>
				          {
					          try
					          {
						          if( !IsInDesignMode )
						          {
							          string names = string.Empty;

							          foreach( ExtendedCompanyAddress eca in ecas )
							          {
								          //ResellerCustomerCompanyLookup tmp = await Azure.Client.RequestGetCustomerCompanyAddress( SelectedAccountId, SelectedAddress.Ca.CompanyName );
								          ResellerCustomerCompanyLookup tmp = await Azure.Client.RequestGetCustomerCompanyAddress( SelectedAccountId, eca.Ca.CompanyName );

								          //Company company = BuildCompany( SelectedAddress );
								          Company company = BuildCompany( eca );

								          DeleteCustomerCompany dcc = new DeleteCustomerCompany( "AddressBookModel" )
								                                      {
									                                      Company      = company,
									                                      CustomerCode = SelectedAccountId
								                                      };

								          await Azure.Client.RequestDeleteCustomerCompany( dcc );

								          foreach( string group in GroupsAndMembers.Keys )
								          {
									          if( GroupsAndMembers[ group ].Contains( company.CompanyName ) )
									          {
										          Logging.WriteLogLine( "Removing " + company.CompanyName + " from group " + group );
										          CompanyAddressGroup cag = GetGroupForName( group );

										          CompanyAddressGroupEntry cage = new CompanyAddressGroupEntry
										                                          {
											                                          CompanyName = company.CompanyName,
											                                          GroupNumber = cag.GroupNumber
										                                          };
										          await Azure.Client.RequestDeleteCompanyAddressGroupEntry( cage );
									          }
								          }

								          names += eca.Ca.CompanyName + ", ";
							          }

							          Dispatcher.Invoke( () =>
							                             {
								                             string title   = (string)Application.Current.TryFindResource( "AddressBookDeleteTitle" );
								                             string message = (string)Application.Current.TryFindResource( "AddressBookDelete" );

								                             Logging.WriteLogLine( "Deleted " + names );
								                             names = "\n" + names.Substring( 0, names.Length - 2 ).Trim() + "\n";

								                             //message = message.Replace( "@1", company.CompanyName );
								                             message = message.Replace( "@1", names );

								                             MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Information );

								                             this.Execute_ClearAddress();
								                             this.LoadAddresses();
								                             this.LoadGroups();

															 UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses();
														 } );
						          }
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "Exception thrown when deleting a company: " + e.ToString() );
					          }
				          } );
			}
		}

		private Company BuildCompany( ExtendedCompanyAddress eca = null )
		{
			Company company = null;

			if( eca == null )
			{
				company = new Company
				          {
					          LocationBarcode = AddressLocationBarcode,
					          Suite           = AddressSuite,
					          AddressLine1    = AddressStreet,
					          City            = AddressCity,
					          Country         = AddressCountry,
					          CompanyName     = AddressDescription,
					          PostalCode      = AddressZipPostal,
					          Region          = AddressProvState,
					          UserName        = AddressContactName,
					          Phone           = AddressContactPhone,
					          EmailAddress    = AddressContactEmail,
					          Notes           = AddressNotes
				          };
			}
			else
			{
				// Used by Execute_Delete
				company = new Company
				          {
					          LocationBarcode = eca.Ca.LocationBarcode,
					          Suite           = eca.Ca.Suite,
					          AddressLine1    = eca.Ca.AddressLine1,
					          City            = eca.Ca.City,
					          Country         = eca.Ca.Country,
					          CompanyName     = eca.Ca.CompanyName,
					          PostalCode      = eca.Ca.PostalCode,
					          Region          = eca.Ca.Region,

					          //UserName = eca.,
					          Phone        = eca.Ca.Phone,
					          EmailAddress = eca.Ca.EmailAddress,
					          Notes        = eca.Ca.Notes
				          };
			}

			//if (rccl == null)
			//{
			//    company = new Company
			//    {
			//        Suite = AddressSuite,
			//        AddressLine1 = AddressStreet,
			//        City = AddressCity,
			//        Country = AddressCountry,
			//        EmailAddress = AddressContactEmail,
			//        Phone = AddressContactPhone,
			//        CompanyName = AddressDescription,
			//        PostalCode = AddressZipPostal,
			//        Region = AddressProvState,
			//        Notes = AddressNotes
			//    };
			//}
			//else
			//{
			//    company = new Company
			//    {
			//        Suite = AddressSuite,
			//        AddressLine1 = rccl.AddressLine1,
			//        City = rccl.City,
			//        Country = rccl.Country,
			//        EmailAddress = rccl.EmailAddress,
			//        Phone = rccl.Phone,
			//        CompanyName = rccl.CompanyName,
			//        PostalCode = rccl.PostalCode,
			//        Region = rccl.Region,
			//        Notes = AddressNotes
			//    };
			//}

			return company;
		}

		private List<string> ValidateAddress( Dictionary<string, Control> controls )
		{
			List<string> errors = new List<string>();

			if( controls.Count > 0 )
			{
				foreach( var fieldName in controls.Keys )
				{
					var error = string.Empty;

					switch( fieldName )
					{
					case "AddressDescription":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsDescription" ) );

						if( !IsControlEmpty( controls[ fieldName ] ) && SelectedAddress == null )
						{
							// Check to see if the description is unique
							string description = ( (TextBox)controls[ fieldName ] ).Text.Trim();

							foreach( ExtendedCompanyAddress eca in currentCompanyAddresses )
							{
								if( eca.Ca.CompanyName.ToLower() == description.ToLower() )
								{
									error                            = (string)Application.Current.TryFindResource( "AddressBookValidationErrorsDescriptionDuplicate" );
									error                            = error.Replace( "@1", description );
									error                            = error.Replace( "@2", eca.Ca.CompanyName );
									controls[ fieldName ].Background = Brushes.Yellow;
								}
							}
						}

						break;

					case "AddressStreet":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsStreet" ) );

						break;

					case "AddressCity":
						error = SetErrorState( controls[ fieldName ], (string)Application.Current.TryFindResource( "AddressBookValidationErrorsCity" ) );

						break;

					case "AddressLocationBarcode":
						if( !IsControlEmpty( controls[ fieldName ] ) && SelectedAddress == null )
						{
							// Check to see if the barcode is unique
							string barcode = ( (TextBox)controls[ fieldName ] ).Text.Trim();

							//foreach (ExtendedCompanyAddress eca in ExtendedAddresses)
							foreach( ExtendedCompanyAddress eca in currentCompanyAddresses ) // This is the unfiltered list
							{
								// TODO the eca.Ca.LocationBarcode is always empty
								if( eca.Ca.LocationBarcode.ToLower() == barcode.ToLower() )
								{
									error                            = (string)Application.Current.TryFindResource( "AddressBookValidationErrorsLocationBarcode" );
									error                            = error.Replace( "@1", barcode );
									error                            = error.Replace( "@2", eca.Ca.CompanyName );
									controls[ fieldName ].Background = Brushes.Yellow;
								}
							}
						}

						break;
					}

					if( !string.IsNullOrEmpty( error ) )
					{
						errors.Add( error );
					}
				}
			}

			return errors;
		}

		private string SetErrorState( Control ctrl, string message )
		{
			var error = string.Empty;

			if( IsControlEmpty( ctrl ) )
			{
				ctrl.Background = Brushes.Yellow;
				error           = message;
			}
			else
				ctrl.Background = Brushes.White;

			return error;
		}

		/// <summary>
		/// </summary>
		/// <param name="ctrl"></param>
		/// <returns></returns>
		private bool IsControlEmpty( Control ctrl )
		{
			var isEmpty = true;

			if( ctrl is TextBox tb )
				isEmpty = IsTextBoxEmpty( tb );
			else if( ctrl is ComboBox cb )
				isEmpty = IsComboBoxSelected( cb );
			else if( ctrl is DateTimePicker dtp )
				isEmpty = IsDateTimePickerEmpty( dtp );
			else if( ctrl is TimePicker tp )
				isEmpty = IsTimePickerEmpty( tp );

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="tb"></param>
		/// <returns></returns>
		private bool IsTextBoxEmpty( TextBox tb )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( tb.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="cb"></param>
		/// <returns></returns>
		private bool IsComboBoxSelected( ComboBox cb )
		{
			var isEmpty = true;

			if( cb.SelectedIndex > -1 )
				isEmpty = false;

			return isEmpty;
		}

		/// <summary>
		/// </summary>
		/// <param name="dtp"></param>
		/// <returns></returns>
		private bool IsDateTimePickerEmpty( DateTimePicker dtp )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( dtp.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		private bool IsTimePickerEmpty( TimePicker tp )
		{
			var isEmpty = true;

			if( !string.IsNullOrEmpty( tp.Text ) )
				isEmpty = false;

			return isEmpty;
		}

		public void Execute_ClearAddress()
		{
			Logging.WriteLogLine( "Clear Address clicked" );

			Dispatcher.Invoke( () =>
			                   {
				                   IsDescriptionReadOnly = false;

				                   SelectedAddress = null;

				                   AddressDescription     = string.Empty;
				                   AddressLocationBarcode = string.Empty;
				                   AddressSuite           = string.Empty;
				                   AddressStreet          = string.Empty;
				                   AddressCity            = string.Empty;
				                   AddressZipPostal       = string.Empty;

				                   AddressProvState = string.Empty;

				                   AddressContactName  = string.Empty;
				                   AddressContactPhone = string.Empty;
				                   AddressContactEmail = string.Empty;
				                   AddressNotes        = string.Empty;

				                   LoadAddressesGroupNames();

				                   //AvailableGroups = AvailableGroups; // LoadAddressesGroupNames();

				                   Execute_ClearImport();
			                   } );
		}

		public void Execute_ClearImport()
		{
			SelectedImportFile = "";

			if( GroupNames != null && GroupNames.Count > 0 )
			{
				SelectedGroupName = GroupNames[ 0 ];
			}
		}

		public void Execute_LoadAddress()
		{
			if( SelectedAddress != null )
			{
				Logging.WriteLogLine( "Load Address clicked - SelectedAddress: " + SelectedAddress.Ca.CompanyName );

				Task.Run( async () =>
				          {
					          try
					          {
						          IsDescriptionReadOnly = true;

						          var ca = SelectedAddress.Ca;

						          //await Azure.Client.requestget

						          //var tmp = await Azure.Client.RequestGetCustomerCompanyAddress("", ca.CompanyName);
						          CompanyByAccountAndCompanyNameList ro = new CompanyByAccountAndCompanyNameList();

						          CompanyByAccountAndCompanyName company = new CompanyByAccountAndCompanyName
						                                                   {
							                                                   CompanyName  = ca.CompanyName,
							                                                   CustomerCode = SelectedAccountId // ca.CompanyNumber
						                                                   };
						          ro.Add( company );

						          var list = await Azure.Client.RequestGetCustomerCompanyAddressList( ro );

						          Dispatcher.Invoke( () =>
						                             {
							                             // Do this first, so that the Regions are loaded
							                             //AddressCountry = ca.Country; // TODO dirty data - case is wrong 'United states'
							                             AddressCountry = FindCountryMatch( ca.Country );

							                             AddressDescription     = ca.CompanyName;
							                             AddressLocationBarcode = ca.LocationBarcode;
							                             AddressSuite           = ca.Suite;
							                             AddressStreet          = ca.AddressLine1;
							                             AddressCity            = ca.City;
							                             AddressZipPostal       = ca.PostalCode;
							                             AddressNotes           = ca.Notes;

							                             //AddressProvState = ca.Region; // TODO dirty data - case is wrong 'MICHEGAN'
							                             bool found;
							                             ( AddressProvState, found ) = Utils.CountriesRegions.FindRegionForCountry( ca.Region, AddressCountry );

							                             if( !found )
							                             {
								                             // Try treating it as an abbreviation
								                             AddressProvState = Utils.CountriesRegions.FindRegionForAbbreviationAndCountry( ca.Region, AddressCountry );
							                             }

							                             if( list != null && list.Count > 0 && list[ 0 ].Companies != null )
							                             {
								                             foreach( var co in list )
								                             {
									                             if( co.Companies != null && co.Companies.Count > 0 )
									                             {
										                             if( co.Companies[ 0 ].CompanyName == ca.CompanyName )
										                             {
											                             AddressContactName     = co.Companies[ 0 ].UserName;
											                             AddressLocationBarcode = co.Companies[ 0 ].LocationBarcode;
										                             }
									                             }
								                             }
							                             }

							                             AddressContactPhone = ca.Phone;
							                             AddressContactEmail = ca.EmailAddress;

							                             // Update Member Of Groups and Available Groups lists
							                             LoadAddressesGroupNames();
						                             } );
					          }
					          catch( Exception e )
					          {
						          Logging.WriteLogLine( "Exception thrown fetching the address for " + SelectedAddress.Ca.CompanyName + ": " + e.ToString() );
					          }
				          } );
			}
		}

		public void Execute_ShowHelp()
		{
			Logging.WriteLogLine( "Opening " + HelpUri );
			Uri uri = new Uri( HelpUri );
			Process.Start( new ProcessStartInfo( uri.AbsoluteUri ) );
		}

		private string FindCountryMatch( string orig )
		{
			string country = orig.ToLower();

			foreach( string tmp in Utils.CountriesRegions.Countries )
			{
				if( tmp.ToLower() == country )
				{
					country = tmp;

					break;
				}
			}

			return country;
		}

		/// <summary>
		/// Note: This only looks at the description field.
		/// From ca_mod.jsp:
		/// boolean matches = p.matcher(ca.description).matches();
		/// </summary>        
		public void Execute_ExportAddresses()
		{
			Logging.WriteLogLine( "Export Addresses clicked" );

			//var cas = this.lvAddressBook.Items;

			//var filter = this.tbAddresssesExportFilter.Text.Trim();
			//bool reverse = (bool)this.cbAddressesExportExclude.IsChecked;

			string filter  = ExportDescription;
			bool   reverse = ExportIsExclude;

			if( ExtendedAddresses != null && ExtendedAddresses.Count > 0 )
			{
				Regex r = null;

				if( filter.Length > 0 )
				{
					r = new Regex( filter );
				}

				//var csvList = "#accountId,description,suite,street,city,province,country,contactName,contactPhone,contactEmail,routeId,gpsLong,gpsLat,zip/postal\r\n";
				var csvList = "#accountId,description,suite,street,city,province,country,contactPhone,contactEmail,gpsLong,gpsLat,zip/postal\r\n";

				foreach( ExtendedCompanyAddress eca in ExtendedAddresses )
				{
					var line = '"' + this.SanitizeForCsv( eca.Ca.CompanyNumber ) + '"' + "," + '"' + this.SanitizeForCsv( eca.Ca.CompanyName ) + '"' + ","
					           + '"' + this.SanitizeForCsv( eca.Ca.Suite ) + '"' + "," + '"' + this.SanitizeForCsv( eca.Ca.AddressLine1 ) + '"' + ","
					           + '"' + this.SanitizeForCsv( eca.Ca.City ) + '"' + "," + '"' + this.SanitizeForCsv( eca.Ca.Region ) + '"' + ","
					           + '"' + this.SanitizeForCsv( eca.Ca.Country ) + '"' + "," // TODO Missing field  + '"' + this.SanitizeForCsv(eca.Ca.contactName) + '"' + ","
					           + '"' + this.SanitizeForCsv( eca.Ca.Phone ) + '"' + "," + '"' + this.SanitizeForCsv( eca.Ca.EmailAddress ) + '"' + ","
					           + '"' + eca.Ca.Longitude + '"' + "," + '"' + eca.Ca.Latitude + '"' + "," + '"'
					           + this.SanitizeForCsv( eca.Ca.PostalCode ) + '"' + "\r\n";

					if( r != null )
					{
						var isMatch = r.IsMatch( eca.Ca.CompanyName );

						if( isMatch && !reverse )
						{
							csvList += line;
						}
						else if( !isMatch && reverse )
						{
							csvList += line;
						}
					}
					else
					{
						csvList += line;
					}
				}

				Utils.NotepadHelper.ShowMessage( csvList, "Exported Address Book" );
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private string SanitizeForCsv( string item )
		{
			//if (item != null && item.Length > 0 && item.Contains('"'))
			if( item != null && item.Length > 0 && item.Contains( "\"" ) )
			{
				item = item.Replace( '"', '\'' );
			}

			return item;
		}

		private void UpdateSearchAndShipmentEntryTabsWithNewCompanyAddresses()
		{
			var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
			if (tis != null && tis.Count > 0)
			{
				for (int i = 0; i < tis.Count; i++)
				{
					var ti = tis[i];
					if (ti.Content is TripEntry)
					{
						// Reload the Inventory
						Logging.WriteLogLine("Reloading inventory in extant " + Globals.TRIP_ENTRY + " tab");
						((TripEntryModel)ti.Content.DataContext).WhenAccountChanges();
					}
					else if (ti.Content is Trips.SearchTrips.SearchTrips)
					{
						Logging.WriteLogLine("Reloading inventory in extant " + Globals.SEARCH_TRIPS + " tab");
						((Trips.SearchTrips.SearchTrips)ti.Content).ComboBox_SelectionChanged_2Async(null, null);
					}
				}
			}
		}

		/// <summary>
		/// The CSV file to be imported will have the following format:
		/// 1. Address Name – this is the Company Name aka Pick Up Name for the address. (required)
		/// 2. Location Number – this is the Location Barcode for the address. (required)
		/// 3. Street Address – this is the Street address for the address. (required).
		/// 4. Unit/Suite – this is the Unit or Suite number for the address. (not required).
		/// 5. City – this is the City for the address(required).
		/// 6. State/Prov – this is the State for the address(required).
		/// 7. Country – this is the Country for the address(required).
		/// 8. Postal Code/Zip – this is the Postal Code for the address(required).
		/// 9. Email Address – this is the Email Address for the address(not required).
		/// </summary>
		/// <returns></returns>
		public void Execute_ImportAddresses()
		{
			if( IsSelectGroupFromListSelected )
			{
				Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the group: " + SelectedGroupName );
			}
			else
			{
				Logging.WriteLogLine( "Importing Addresses from file: " + SelectedImportFile + " into the new group: " + NewImportGroupName );
			}

			// (bool exists, List<string> lines) = ReadLocalFile(SelectedImportFile);
			if( File.Exists( SelectedImportFile ) )
			{
				bool         errors        = false;
				List<string> errorMessages = new List<string>();

				List<CsvAddressLine> lines = new List<CsvAddressLine>();

				using( CsvReader csv = new CsvReader( new StreamReader( SelectedImportFile ), CultureInfo.CurrentCulture, true ) )
				{
					int numberOfLines = 0;

					csv.Read();
					csv.ReadHeader();

					while( csv.Read() )
					{
						++numberOfLines;

						CsvAddressLine line = new CsvAddressLine
						                      {
							                      Name            = csv.GetField<string>( "Address Name" ),
							                      LocationBarcode = csv.GetField<string>( "Location Number" ),
							                      Street          = csv.GetField<string>( "Street Address" ),
							                      UnitSuite       = csv.GetField<string>( "Unit/Suite" ),
							                      City            = csv.GetField<string>( "City" ),
							                      Region          = csv.GetField<string>( "State/Prov" ),
							                      Country         = csv.GetField<string>( "Country" ),
							                      PostalCode      = csv.GetField<string>( "Postal Code/Zip" ),
							                      Email           = csv.GetField<string>( "Email Address" )
						                      };

						( errors, errorMessages ) = ValidateLine( line );

						if( errors )
						{
							Logging.WriteLogLine( "Errors in line: " + numberOfLines + ", line: " + line.ToString() );

							break;
						}

						lines.Add( line );
					}

					if( numberOfLines == 0 )
					{
						errors = true;
						errorMessages.Add( "File is empty" );
					}
				}

				if( !errors )
				{
					// Import the addresses
					( bool success, List<string> problems ) = AddUpdateAddressBook( lines );

					if( success )
					{
						string group;

						if( IsNewGroupSelected )
						{
							// Make the group
							//success = CreateGroup(NewImportGroupName);
							group = NewImportGroupName;
						}
						else
						{
							group = SelectedGroupName;

							// test1 \t(Members
							group = group.Substring( 0, group.IndexOf( '\t' ) ).Trim();
						}

						// Group now exists - time to add the addresses to the group

						List<string> names = new List<string>();

						foreach( CsvAddressLine cal in lines )
						{
							names.Add( cal.Name );
						}

						success = SaveGroup( group, names );
					}
					else
					{
						Dispatcher.Invoke( () =>
						                   {
							                   string title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
							                   string message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrors" ) + "\n";

							                   foreach( string error in errorMessages )
							                   {
								                   message += "\t" + error + "\n";
							                   }

							                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
						                   } );
					}
				}
				else
				{
					Dispatcher.Invoke( () =>
					                   {
						                   string title   = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileTitle" );
						                   string message = (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFile" ) + "\n";

						                   foreach( string error in errorMessages )
						                   {
							                   message += "\t" + error + "\n";
						                   }

						                   message += (string)Application.Current.TryFindResource( "AddressBookImportAddressesErrorsInFileEnd" ) + "\n";
						                   MessageBox.Show( message, title, MessageBoxButton.OK, MessageBoxImage.Error );
					                   } );
				}
			}
		}

		private (bool success, List<string> errors) AddUpdateAddressBook( List<CsvAddressLine> cals )
		{
			bool         success = true;
			List<string> errors  = new List<string>();

			Logging.WriteLogLine( "Adding/Updating " + cals.Count + " addresses" );

			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          foreach( CsvAddressLine cal in cals )
					          {
						          Logging.WriteLogLine( "Line: " + cal.ToString() );

						          ExtendedCompanyAddress address = null;

						          var matches = from eca in ExtendedAddresses
						                        where eca.Ca.LocationBarcode == cal.LocationBarcode
						                        select eca;

						          if( matches.Any() )
						          {
							          address = matches.FirstOrDefault();
						          }

						          Company company = new Company
						                            {
							                            LocationBarcode = cal.LocationBarcode,
							                            Suite           = cal.UnitSuite,
							                            AddressLine1    = cal.Street,
							                            City            = cal.City,
							                            Country         = cal.Country,
							                            CompanyName     = cal.Name,
							                            PostalCode      = cal.PostalCode,
							                            Region          = cal.Region,

							                            //UserName = AddressContactName,
							                            //Phone = AddressContactPhone,
							                            EmailAddress = cal.Email

							                            //Notes = AddressNotes
						                            };

						          if( address == null )
						          {
							          Logging.WriteLogLine( "Adding new address: " + cal.Name );

							          AddCustomerCompany acc = new AddCustomerCompany( "AddressBookModel" )
							                                   {
								                                   Company      = company,
								                                   CustomerCode = SelectedAccountId
							                                   };
							          await Azure.Client.RequestAddCustomerCompany( acc );
						          }
						          else
						          {
							          Logging.WriteLogLine( "Updating address: " + cal.Name );

							          UpdateCustomerCompany ucc = new UpdateCustomerCompany( "AddressBookModel" )
							                                      {
								                                      Company      = company,
								                                      CustomerCode = SelectedAccountId
							                                      };
							          await Azure.Client.RequestUpdateCustomerCompany( ucc );
						          }
					          }

					          Dispatcher.Invoke( () =>
					                             {
						                             LoadAddresses();
					                             } );
				          } );
			}

			return ( success, errors );
		}

		private (bool errors, List<string> messages) ValidateLine( CsvAddressLine line )
		{
			bool         errors   = false;
			List<string> messages = new List<string>();

			if( line.Name.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "Address Name is empty" );
				messages.Add( "Address Name is empty" );
			}

			if( line.LocationBarcode.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "Location Number is empty" );
				messages.Add( "Location Number is empty" );
			}

			if( line.Street.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "Street Address is empty" );
				messages.Add( "Street Address is empty" );
			}

			if( line.City.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "City is empty" );
				messages.Add( "City is empty" );
			}

			if( line.Region.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "State/Prov is empty" );
				messages.Add( "State/Prov is empty" );
			}

			if( line.Name.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "Country is empty" );
				messages.Add( "Country is empty" );
			}

			if( line.PostalCode.IsNullOrWhiteSpace() )
			{
				errors = true;
				Logging.WriteLogLine( "Postal Code/Zip is empty" );
				messages.Add( "Postal Code/Zip is empty" );
			}

			return ( errors, messages );
		}

		//private (bool exists, List<string> lines) ReadLocalFile(string fileName)
		//{
		//    bool exists = true;
		//    List<string> lines = new List<string>();

		//    if (File.Exists(SelectedImportFile))
		//    {
		//        string line;
		//        int counter = 0;
		//        using (StreamReader sr = new StreamReader(SelectedImportFile))
		//        {
		//            // NOTE the first line will be the header
		//            while ((line = sr.ReadLine()) != null)
		//            {
		//                ++counter;
		//                Logging.WriteLogLine("line: " + line);
		//                lines.Add(line);
		//            }

		//            sr.Close();
		//        }

		//        Logging.WriteLogLine("Read " + counter + " lines");
		//    }

		//    return (exists, lines);
		//}

		/// <summary>
		/// The CSV file to be imported will have the following format:
		/// 1. Address Name – this is the Company Name aka Pick Up Name for the address. (required)
		/// 2. Location Number – this is the Location Barcode for the address. (required)
		/// 3. Street Address – this is the Street address for the address. (required).
		/// 4. Unit/Suite – this is the Unit or Suite number for the address. (not required).
		/// 5. City – this is the City for the address(required).
		/// 6. State/Prov – this is the State for the address(required).
		/// 7. Country – this is the Country for the address(required).
		/// 8. Postal Code/Zip – this is the Postal Code for the address(required).
		/// 9. Email Address – this is the Email Address for the address(not required).
		/// </summary>
		/// <returns></returns>
		public (bool correct, List<string> errors) IsImportAddressFileValid()
		{
			bool         isCorrect = true;
			List<string> errors    = new List<string>();

			// NOTE the first line will be the header
			using( CsvReader csv = new CsvReader( new StreamReader( SelectedImportFile ), CultureInfo.CurrentCulture, true ) )
			{
				try
				{
					//csv.ReadHeader();
					csv.Read();

					// Should be header
					CsvAddressLine line = new CsvAddressLine
					                      {
						                      Name            = csv.GetField<string>( 0 ),
						                      LocationBarcode = csv.GetField<string>( 1 ),
						                      Street          = csv.GetField<string>( 2 ),
						                      UnitSuite       = csv.GetField<string>( 3 ),
						                      City            = csv.GetField<string>( 4 ),
						                      Region          = csv.GetField<string>( 5 ),
						                      Country         = csv.GetField<string>( 6 ),
						                      PostalCode      = csv.GetField<string>( 7 ),
						                      Email           = csv.GetField<string>( 8 )
					                      };

					if( line.Name != "Address Name" )
					{
						Logging.WriteLogLine( "Address Name is wrong: " + line.Name );
						errors.Add( "Found " + line.Name + " - should be 'Address Name'" );
						isCorrect = false;
					}

					if( isCorrect && line.LocationBarcode != "Location Number" )
					{
						Logging.WriteLogLine( "LocationBarcode is wrong: " + line.LocationBarcode );
						errors.Add( "Found " + line.LocationBarcode + " - should be 'Location Number'" );
						isCorrect = false;
					}

					if( isCorrect && line.Street != "Street Address" )
					{
						Logging.WriteLogLine( "Street is wrong: " + line.Street );
						errors.Add( "Found " + line.Street + " - should be 'Street Address'" );
						isCorrect = false;
					}

					if( isCorrect && line.UnitSuite != "Unit/Suite" )
					{
						Logging.WriteLogLine( "UnitSuite is wrong: " + line.UnitSuite );
						errors.Add( "Found " + line.UnitSuite + " - should be 'Unit/Suite'" );
						isCorrect = false;
					}

					if( isCorrect && line.City != "City" )
					{
						Logging.WriteLogLine( "City is wrong: " + line.City );
						errors.Add( "Found " + line.City + " - should be 'City'" );
						isCorrect = false;
					}

					if( isCorrect && line.Region != "State/Prov" )
					{
						Logging.WriteLogLine( "Region is wrong: " + line.Region );
						errors.Add( "Found " + line.Region + " - should be 'State/Prov'" );
						isCorrect = false;
					}

					if( isCorrect && line.Country != "Country" )
					{
						Logging.WriteLogLine( "Country is wrong: " + line.Country );
						errors.Add( "Found " + line.Country + " - should be 'Country'" );
						isCorrect = false;
					}

					if( isCorrect && line.PostalCode != "Postal Code/Zip" )
					{
						Logging.WriteLogLine( "PostalCode is wrong: " + line.PostalCode );
						errors.Add( "Found " + line.PostalCode + " - should be 'Postal Code/Zip'" );
						isCorrect = false;
					}

					if( isCorrect && line.Email != "Email Address" )
					{
						Logging.WriteLogLine( "Email is wrong: " + line.Email );
						errors.Add( "Found " + line.Email + " - should be 'Email Address'" );
						isCorrect = false;
					}

					//CsvAddressLine line = new CsvAddressLine
					//{
					//    Name = parser.GetField<string>("Address Name"),
					//    LocationBarcode = parser.GetField<string>("Location Number"),
					//    Street = parser.GetField<string>("Street Address"),
					//    UnitSuite = parser.GetField<string>("Unit/Suite"),
					//    City = parser.GetField<string>("City"),
					//    Region = parser.GetField<string>("State/Prov"),
					//    Country = parser.GetField<string>("Country"),
					//    PostalCode = parser.GetField<string>("Postal Code/Zip"),
					//    Email = parser.GetField<string>("Email Address")
					//};
				}
				catch( Exception e )
				{
					Logging.WriteLogLine( "Exception thrown while reading file: " + SelectedImportFile + ": " + e.ToString() );
					errors.Add( "Unable to read the file" );
					isCorrect = false;
				}
			}

			return ( isCorrect, errors );
		}

	#endregion
	}

	/// <summary>
	/// Wrapper for CompanyAddress to provide an address string for list.
	/// </summary>
	public class ExtendedCompanyAddress
	{
		public CompanyAddress Ca { get; set; }

		public string AddressString
		{
			get
			{
				string value = string.Empty;

				if( Ca.Suite.Trim().Length > 0 )
				{
					value += Ca.Suite + "-";
				}

				string street = Ca.AddressLine1;

				if( !string.IsNullOrWhiteSpace( Ca.AddressLine2 ) )
				{
					street += ", " + Ca.AddressLine2;
				}

				value += street + ", " + Ca.City + ", " + Ca.Region;

				return value;
			}
		}

		public ExtendedCompanyAddress( CompanyAddress ca )
		{
			this.Ca = ca;
		}
	}

	public class CsvAddressLine
	{
		public string Name;
		public string LocationBarcode;
		public string Street;
		public string UnitSuite;
		public string City;
		public string Region;
		public string Country;
		public string PostalCode;
		public string Email;

		public override string ToString()
		{
			return ( "Name: " + Name + ", LocationBarcode: " + LocationBarcode + ", Street: " + Street + ", UnitSuite: " + UnitSuite
			         + ", City: " + City + ", Region: " + Region + ", Country: " + Country + ", PostalCode: " + PostalCode + ", Email: " + Email );
		}
	}
}