﻿using AzureRemoteService;
using IdsControlLibraryV2.TabControl;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utils;
using ViewModels.Trips.TripEntry;
using Xceed.Wpf.Toolkit;

namespace ViewModels.Customers.Accounts
{
    public class AccountsModel : ViewModelBase
    {
        private List<object> customers = null;
        private readonly Dictionary<string, string> dictCustomerNamesIds = new Dictionary<string, string>();

        #region Data

        public bool Loaded
        {
            get { return Get(() => Loaded, false); }
            set { Set(() => Loaded, value); }
        }

        protected override void OnInitialised()
        {
            base.OnInitialised();
            Loaded = false;

            Task.Run(() =>
                     {
                         Task.WaitAll(Task.Run(() =>
                                                {
                                                     GetAccountNamesNotAsync();
                                                 })
                                       
                                     );

                         Dispatcher.Invoke(() =>
                                            {
                                                Loaded = true;
                                            });
                     });
        }

        // public ObservableCollection<string> AccountNames = new ObservableCollection<string>();

        public ObservableCollection<string> AccountNames
        {
            //get { return Get(() => AccountNames, GetAccountNames); }
            get { return Get(() => AccountNames); }
            set { Set(() => AccountNames, value); }
        }

        //public IList AccountNames
        //{
        //    get { return Get(() => AccountNames, GetAccountNames); }
        //    set { Set(() => AccountNames, value); }
        //}



        public string SelectedAccountName
        {
            get { return Get(() => SelectedAccountName, ""); }
            set { Set(() => SelectedAccountName, value); }
        }


        public Company SelectedAccountCompany
        {
            get { return Get(() => SelectedAccountCompany); }
            set { Set(() => SelectedAccountCompany, value); }
        }


        public string SelectedAccountId
        {
            get { return Get(() => SelectedAccountId, ""); }
            set { Set(() => SelectedAccountId, value); }
        }

        [DependsUpon(nameof(SelectedAccountCompany))]
        public bool IsAccountIdLocked
        {
            get
            {
                bool isLocked = true;
                if (SelectedAccountCompany == null)
                {
                    isLocked = false;
                }
                return isLocked;
            }
            set { Set(() => IsAccountIdLocked, value); }
        }


        public string SelectedAccountPhone
        {
            get { return Get(() => SelectedAccountPhone, ""); }
            set { Set(() => SelectedAccountPhone, value); }
        }


        public string SelectedAccountSalesRep
        {
            get { return Get(() => SelectedAccountSalesRep, ""); }
            set { Set(() => SelectedAccountSalesRep, value); }
        }


        public string SelectedAccountNotes
        {
            get { return Get(() => SelectedAccountNotes, ""); }
            set { Set(() => SelectedAccountNotes, value); }
        }


        public string SelectedAccountBillingPeriod
        {
            get { return Get(() => SelectedAccountBillingPeriod, ""); }
            set { Set(() => SelectedAccountBillingPeriod, value); }
        }


        public string SelectedAccountBillingMethod
        {
            get { return Get(() => SelectedAccountBillingMethod, ""); }
            set { Set(() => SelectedAccountBillingMethod, value); }
        }


        public string SelectedAccountInvoiceOverride
        {
            get { return Get(() => SelectedAccountInvoiceOverride, ""); }
            set { Set(() => SelectedAccountInvoiceOverride, value); }
        }


        public string SelectedAccountInvoiceEmail
        {
            get { return Get(() => SelectedAccountInvoiceEmail, ""); }
            set { Set(() => SelectedAccountInvoiceEmail, value); }
        }


        public string SelectedAccountOutstandingFormatted
        {
            get { return Get(() => SelectedAccountOutstandingFormatted, GetOutstandingForSelectedAccountFormatted); }
            set { Set(() => SelectedAccountOutstandingFormatted, value); }
        }


        public bool SelectedAccountIsEnabled
        {
            get { return Get(() => SelectedAccountIsEnabled, true); }
            set { Set(() => SelectedAccountIsEnabled, value); }
        }

        public bool SelectedAccountRequireReferenceField
        {
            get { return Get(() => SelectedAccountRequireReferenceField, false); }
            set { Set(() => SelectedAccountRequireReferenceField, value); }
        }


        public string SelectedAccountCreditLimit
        {
            get { return Get(() => SelectedAccountCreditLimit, ""); }
            set { Set(() => SelectedAccountCreditLimit, value); }
        }


        public string SelectedAccountAdminName
        {
            get { return Get(() => SelectedAccountAdminName, ""); }
            set { Set(() => SelectedAccountAdminName, value); }
        }

        

        public string SelectedAccountAdminPhone
        {
            get { return Get(() => SelectedAccountAdminPhone, ""); }
            set { Set(() => SelectedAccountAdminPhone, value); }
        }



        public string SelectedAccountBillingAdminName
        {
            get { return Get(() => SelectedAccountBillingAdminName, ""); }
            set { Set(() => SelectedAccountBillingAdminName, value); }
        }


        public string SelectedAccountBillingAdminPhone
        {
            get { return Get(() => SelectedAccountBillingAdminPhone, ""); }
            set { Set(() => SelectedAccountBillingAdminPhone, value); }
        }


        public string SelectedAccountBillingSuite
        {
            get { return Get(() => SelectedAccountBillingSuite, ""); }
            set { Set(() => SelectedAccountBillingSuite, value); }
        }


        public string SelectedAccountBillingStreet
        {
            get { return Get(() => SelectedAccountBillingStreet, ""); }
            set { Set(() => SelectedAccountBillingStreet, value); }
        }


        public string SelectedAccountBillingCity
        {
            get { return Get(() => SelectedAccountBillingCity, ""); }
            set { Set(() => SelectedAccountBillingCity, value); }
        }


        public string SelectedAccountBillingProvState
        {
            get { return Get(() => SelectedAccountBillingProvState, ""); }
            set { Set(() => SelectedAccountBillingProvState, value); }
        }


        public string SelectedAccountBillingCountry
        {
            get { return Get(() => SelectedAccountBillingCountry, ""); }
            set { Set(() => SelectedAccountBillingCountry, value); }
        }


        public string SelectedAccountBillingPostalZip
        {
            get { return Get(() => SelectedAccountBillingPostalZip, ""); }
            set { Set(() => SelectedAccountBillingPostalZip, value); }
        }


        public string SelectedAccountBillingZone
        {
            get { return Get(() => SelectedAccountBillingZone, ""); }
            set { Set(() => SelectedAccountBillingZone, value); }
        }

        public string SelectedAccountShippingAdminName
        {
            get { return Get(() => SelectedAccountShippingAdminName, ""); }
            set { Set(() => SelectedAccountShippingAdminName, value); }
        }


        public string SelectedAccountShippingAdminPhone
        {
            get { return Get(() => SelectedAccountShippingAdminPhone, ""); }
            set { Set(() => SelectedAccountShippingAdminPhone, value); }
        }


        public string SelectedAccountShippingSuite
        {
            get { return Get(() => SelectedAccountShippingSuite, ""); }
            set { Set(() => SelectedAccountShippingSuite, value); }
        }


        public string SelectedAccountShippingStreet
        {
            get { return Get(() => SelectedAccountShippingStreet, ""); }
            set { Set(() => SelectedAccountShippingStreet, value); }
        }


        public string SelectedAccountShippingCity
        {
            get { return Get(() => SelectedAccountShippingCity, ""); }
            set { Set(() => SelectedAccountShippingCity, value); }
        }


        public string SelectedAccountShippingProvState
        {
            get { return Get(() => SelectedAccountShippingProvState, ""); }
            set { Set(() => SelectedAccountShippingProvState, value); }
        }


        public string SelectedAccountShippingCountry
        {
            get { return Get(() => SelectedAccountShippingCountry, ""); }
            set { Set(() => SelectedAccountShippingCountry, value); }
        }


        public string SelectedAccountShippingPostalZip
        {
            get { return Get(() => SelectedAccountShippingPostalZip, ""); }
            set { Set(() => SelectedAccountShippingPostalZip, value); }
        }


        public string SelectedAccountShippingZone
        {
            get { return Get(() => SelectedAccountShippingZone, ""); }
            set { Set(() => SelectedAccountShippingZone, value); }
        }
        

        public string SelectedAccountShippingAddressNotes
        {
            get { return Get(() => SelectedAccountShippingAddressNotes, ""); }
            set { Set(() => SelectedAccountShippingAddressNotes, value); }
        }



        public string SelectedAccountAdminUserId
        {
            get { return Get(() => SelectedAccountAdminUserId, ""); }
            set { Set(() => SelectedAccountAdminUserId, value); }
        }


        public string SelectedAccountAdminUserPassword
        {
            get { return Get(() => SelectedAccountAdminUserPassword, ""); }
            set { Set(() => SelectedAccountAdminUserPassword, value); }
        }


        public PasswordBox Password = null;
        public PasswordBox ConfirmPassword = null;

        //public Control Password
        //{
        //    get { return Get(() => Password); }
        //    set { Set(() => Password, value); }
        //}


        //public Control ConfirmPassword
        //{
        //    get { return Get(() => ConfirmPassword); }
        //    set { Set(() => ConfirmPassword, value); }
        //}


        public bool SelectedAccountAdminUserEnabled
        {
            get { return Get(() => SelectedAccountAdminUserEnabled, true); }
            set { Set(() => SelectedAccountAdminUserEnabled, value); }
        }


        public string SelectedAccountAdminUserFirstName
        {
            get { return Get(() => SelectedAccountAdminUserFirstName, ""); }
            set { Set(() => SelectedAccountAdminUserFirstName, value); }
        }



        public string SelectedAccountAdminUserLastName
        {
            get { return Get(() => SelectedAccountAdminUserLastName, ""); }
            set { Set(() => SelectedAccountAdminUserLastName, value); }
        }


        public string SelectedAccountAdminUserEmail
        {
            get { return Get(() => SelectedAccountAdminUserEmail, ""); }
            set { Set(() => SelectedAccountAdminUserEmail, value); }
        }


        public string SelectedAccountAdminUserPhone
        {
            get { return Get(() => SelectedAccountAdminUserPhone, ""); }
            set { Set(() => SelectedAccountAdminUserPhone, value); }
        }


        public string SelectedAccountAdminUserCell
        {
            get { return Get(() => SelectedAccountAdminUserCell, ""); }
            set { Set(() => SelectedAccountAdminUserCell, value); }
        }

        public IList Countries
        {
            get
            {
                List<string> countries = Utils.CountriesRegions.Countries;
                return countries;
            }
            set { Set(() => Countries, value); }
        }

        public string SelectedBillingCountry
        {
            get { return Get(() => SelectedBillingCountry, ""); }
            set { Set(() => SelectedBillingCountry, value); }
        }


        public string SelectedShippingCountry
        {
            get { return Get(() => SelectedShippingCountry, ""); }
            set { Set(() => SelectedShippingCountry, value); }
        }

        [DependsUpon(nameof(SelectedBillingCountry))]
        public IList BillingRegions
        {
            get { return Get(() => BillingRegions); }
            set { Set(() => BillingRegions, value); }
        }


        [DependsUpon(nameof(SelectedShippingCountry))]
        public IList ShippingRegions
        {
            get { return Get(() => ShippingRegions); }
            set { Set(() => ShippingRegions, value); }
        }


        public string AdhocFilter
        {
            get { return Get(() => AdhocFilter, ""); }
            set { Set(() => AdhocFilter, value); }
        }


        //
        // Private methods for Data
        //

        private string GetOutstandingForSelectedAccountFormatted()
        {
            decimal raw = 0.0M; // TODO 

            string outstanding = string.Format("{0:c}", raw);

            return outstanding;
        }

        //private List<string> GetAccountNames()
        //{
        //    List<string> names = new List<string>();

        //    Task.WaitAll(Task.Run(() =>
        //    {
        //        try
        //        {
        //            if (customers == null || customers.Count == 0)
        //            {
        //                if (!IsInDesignMode)
        //                {
        //                    CustomerCodeList ccl = Azure.Client.RequestGetCustomerCodeList().Result;
        //                    Logging.WriteLogLine("Found " + ccl.Count + " Customers");
        //                    if (ccl != null && ccl.Count > 0)
        //                    {
        //                        //names = (from c in ccl
        //                        //             let cn = c.ToLower()
        //                        //             orderby cn
        //                        //             group cn by cn into g
        //                        //             select g.First()).ToList();


        //                        SortedDictionary<string, string> Sd = new SortedDictionary<string, string>();
        //                        foreach (string id in ccl)
        //                        {
        //                            CompanyAddress customer = Azure.Client.RequestGetResellerCustomerCompany(id).Result;
        //                            Sd.Add(customer.CompanyName.ToLower(), customer.CompanyName);

        //                            dictCustomerNamesIds.Add(customer.CompanyName, id);
        //                        }

        //                        foreach (string Key in Sd.Keys)
        //                        {
        //                            names.Add(Sd[Key]);
        //                        }

        //                        Dispatcher.Invoke(() =>
        //                        {
        //                            AccountNames = names;
        //                            SelectedAccountName = names[0];
        //                        });
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Logging.WriteLogLine("Exception thrown fetching customers:\n" + e.ToString());
        //        }
        //    }));


        //    return names;
        //}


        public ObservableCollection<string> GetAccountNamesNotAsync()
        {
            List<string> names = new List<string>();

            try
            {
                if (customers == null || customers.Count == 0)
                {
                    if (!IsInDesignMode)
                    {
                        Logging.WriteLogLine("Fetching Customers");

                        CustomerCodeList ccl = Azure.Client.RequestGetCustomerCodeList().Result;
                        Logging.WriteLogLine("Found " + ccl.Count + " Customers");
                        if (ccl != null && ccl.Count > 0)
                        {
                            //names = (from c in ccl
                            //             let cn = c.ToLower()
                            //             orderby cn
                            //             group cn by cn into g
                            //             select g.First()).ToList();

                            dictCustomerNamesIds.Clear();
                            SortedDictionary<string, string> Sd = new SortedDictionary<string, string>();
                            foreach (string id in ccl)
                            {
                                CompanyAddress customer = Azure.Client.RequestGetResellerCustomerCompany(id).Result;
                                if (!Sd.ContainsKey(customer.CompanyName.ToLower()))
                                {
                                    Sd.Add(customer.CompanyName.ToLower(), customer.CompanyName);
                                }
                                else
                                {
                                    Logging.WriteLogLine("Sd already contains " + customer.CompanyName.ToLower());
                                }

                                if (!dictCustomerNamesIds.ContainsKey(customer.CompanyName))
                                {
                                    dictCustomerNamesIds.Add(customer.CompanyName, id);
                                }
                                else
                                {
                                    Logging.WriteLogLine("dictCustomerNamesIds already contains " + customer.CompanyName);
                                }
                            }

                            foreach (string Key in Sd.Keys)
                            {
                                names.Add(Sd[Key]);
                            }

                            Dispatcher.Invoke(() =>
                            {
                                if (AccountNames == null)
                                {
                                    AccountNames = new ObservableCollection<string>();
                                }
                                AccountNames.Clear();

                                foreach (string name in names)
                                {
                                    AccountNames.Add(name);
                                }
                                    //AccountNames = names;
                                    //SelectedAccountName = names[0];
                                });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteLogLine("Exception thrown fetching customers:\n" + e.ToString());
            }


            return new ObservableCollection<string>(names);
        }

        public ObservableCollection<string> GetAccountNames()
        {
            //ObservableCollection<string> names = new ObservableCollection<string>();
            List<string> names = new List<string>();

            Task.Run(async () =>
            {
                try
                {
                    if (customers == null || customers.Count == 0)
                    {
                        if (!IsInDesignMode)
                        {
                            Logging.WriteLogLine("Fetching Customers");

                            CustomerCodeList ccl = await Azure.Client.RequestGetCustomerCodeList();
                            Logging.WriteLogLine("Found " + ccl.Count + " Customers");
                            if (ccl != null && ccl.Count > 0)
                            {
                                //names = (from c in ccl
                                //             let cn = c.ToLower()
                                //             orderby cn
                                //             group cn by cn into g
                                //             select g.First()).ToList();

                                dictCustomerNamesIds.Clear();
                                SortedDictionary<string, string> Sd = new SortedDictionary<string, string>();
                                foreach (string id in ccl)
                                {
                                    CompanyAddress customer = await Azure.Client.RequestGetResellerCustomerCompany(id);
                                    if (!Sd.ContainsKey(customer.CompanyName.ToLower()))
                                    {
                                        Sd.Add(customer.CompanyName.ToLower(), customer.CompanyName);
                                    }
                                    else
                                    {
                                        Logging.WriteLogLine("Sd already contains " + customer.CompanyName.ToLower());
                                    }

                                    if (!dictCustomerNamesIds.ContainsKey(customer.CompanyName))
                                    {
                                        dictCustomerNamesIds.Add(customer.CompanyName, id);
                                    }
                                    else
                                    {
                                        Logging.WriteLogLine("dictCustomerNamesIds already contains " + customer.CompanyName);
                                    }
                                }

                                foreach (string Key in Sd.Keys)
                                {
                                    names.Add(Sd[Key]);
                                }

                                Dispatcher.Invoke(() =>
                                {
                                    AccountNames.Clear();

                                    foreach (string name in names)
                                    {
                                        AccountNames.Add(name);
                                    }
                                    //AccountNames = names;
                                    //SelectedAccountName = names[0];
                                });
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logging.WriteLogLine("Exception thrown fetching customers:\n" + e.ToString());
                }
            });


            // return names;
            return new ObservableCollection<string>(names);
        }


        #endregion

        #region Actions

        [DependsUpon (nameof(SelectedBillingCountry))]
        public void WhenSelectedBillingCountryChanges()
        {
            List<string> regions = null;

            switch (SelectedBillingCountry)
            {
                case "Australia":
                    regions = Utils.CountriesRegions.AustraliaRegions;
                    break;
                case "Canada":
                    regions = Utils.CountriesRegions.CanadaRegions;
                    break;
                case "United States":
                    regions = Utils.CountriesRegions.USRegions;
                    break;
            }
            BillingRegions = regions;
        }

        [DependsUpon(nameof(SelectedShippingCountry))]
        public void WhenSelectedShippingCountryChanges()
        {
            List<string> regions = null;

            switch (SelectedShippingCountry)
            {
                case "Australia":
                    regions = Utils.CountriesRegions.AustraliaRegions;
                    break;
                case "Canada":
                    regions = Utils.CountriesRegions.CanadaRegions;
                    break;
                case "United States":
                    regions = Utils.CountriesRegions.USRegions;
                    break;
            }
            ShippingRegions = regions;
        }
        [DependsUpon( nameof( SelectedAccountName ) )]
        public void WhenSelectedAccountChanges()
        {
            Logging.WriteLogLine("Selected Account Name has changed: " + SelectedAccountName);
            if (!string.IsNullOrEmpty(SelectedAccountName))
            {
                if (dictCustomerNamesIds.ContainsKey(SelectedAccountName))
                {
                    SelectedAccountId = dictCustomerNamesIds[SelectedAccountName];

                    Task.Run(async () =>
                    {
                        try
                        {
                            //CustomerCodeList ccl = await Azure.Client.RequestGetPrimaryCompanyList();
                            //if (ccl != null)
                            //{
                            //    // Returns 21 companies
                            //    foreach (string name in ccl)
                            //    {
                            //        PrimaryCompany company = await Azure.Client.RequestGetPrimaryCompanyByCompanyName(name);
                            //        if (company != null)
                            //        {
                            //            Logging.WriteLogLine("Found Primary Company: " + company.BillingCompany);
                            //        }
                            //    }
                            //}

                            //PrimaryCompany testCompany = await Azure.Client.RequestGetPrimaryCompanyByCompanyName("test201906140800 Company");
                            // PrimaryCompany company = await Azure.Client.RequestGetPrimaryCompanyByCompanyName(SelectedAccountName);
                            PrimaryCompany company = await Azure.Client.RequestGetPrimaryCompanyByCustomerCode(SelectedAccountId);
                            if (company != null)
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    // Get these loading
                                    SelectedAccountBillingCountry = company.BillingCompany.Country;
                                    SelectedAccountShippingCountry = company.Company.Country;

                                    SelectedAccountBillingCity = company.BillingCompany.City;

                                    //
                                    // Hard code "Print" & "Email" - TODO future allow arbitary definition
                                    SelectedAccountBillingMethod = "Missing Field"; // company.BillingCompany.Method; // TODO

                                    //
                                    // Hard code "Individual" (creates 1 invoice for each trip) and the other IDS1 periods
                                    // TODO future, allow user to define specific periods other than Individual and Single
                                    SelectedAccountBillingPeriod = "Missing Field"; // company.BillingCompany.Period; // TODO

                                    SelectedAccountPhone = company.Company.Phone;

                                    SelectedAccountBillingAdminPhone = company.BillingCompany.Phone;
                                    SelectedAccountBillingPostalZip = company.BillingCompany.PostalCode;
                                    SelectedAccountBillingStreet = company.BillingCompany.AddressLine1 + " " + company.BillingCompany.AddressLine2;
                                    SelectedAccountBillingSuite = company.BillingCompany.Suite;
                                    SelectedAccountBillingZone = "Missing Field"; // company.BillingCompany.Zone; // TODO

                                    SelectedAccountShippingAdminPhone = company.Company.Phone;
                                    SelectedAccountShippingCity = company.Company.City;
                                    SelectedAccountShippingPostalZip = company.Company.PostalCode;
                                    SelectedAccountShippingStreet = company.Company.AddressLine1 + " " + company.Company.AddressLine2;
                                    SelectedAccountShippingSuite = company.Company.Suite;
                                    SelectedAccountShippingZone = "Missing Field"; // company.ShippingCompany.Zone; // TODO

                                    SelectedAccountShippingAddressNotes = company.Company.Notes;

                                    SelectedAccountAdminUserId = company.BillingCompany.UserName; // "Missing Field"; // company.LoginCode;
                                    SelectedAccountAdminName = company.UserName;
                                    //SelectedAccountAdminUserId = company.BillingCompany.UserName;


                                    SelectedAccountAdminUserPassword = company.Password;
                                    //SelectedAccountAdminUserPassword = company.BillingCompany.Password;

                                    Password.Password = SelectedAccountAdminUserPassword;
                                    ConfirmPassword.Password = SelectedAccountAdminUserPassword;

                                    // The new region lists should be loaded by now
                                    SelectedAccountBillingProvState = company.BillingCompany.Region;
                                    SelectedAccountShippingProvState = company.Company.Region;
                                });

                                SelectedAccountCompany =  company.BillingCompany;
                            }
                        }
                        catch (Exception e)
                        {
                            Logging.WriteLogLine("Exception thrown fetching Account: " + e.ToString());
                        }
                    });
                }
            }
        }

        public void ClearAccountDetails()
        {
            Logging.WriteLogLine("Clearing Account Details");

            SelectedAccountAdminName = string.Empty;
            SelectedAccountAdminPhone = string.Empty;
            SelectedAccountAdminUserCell = string.Empty;
            SelectedAccountAdminUserEmail = string.Empty;
            SelectedAccountAdminUserEnabled = true;
            SelectedAccountAdminUserFirstName = string.Empty;
            SelectedAccountAdminUserId = string.Empty;
            SelectedAccountAdminUserLastName = string.Empty;
            SelectedAccountAdminUserPassword = string.Empty;
            SelectedAccountAdminUserPhone = string.Empty;
            SelectedAccountBillingCity = string.Empty;
            SelectedAccountBillingCountry = string.Empty;
            SelectedAccountBillingMethod = string.Empty;
            SelectedAccountBillingPeriod = string.Empty;
            SelectedAccountBillingPostalZip = string.Empty;
            SelectedAccountBillingProvState = string.Empty;
            SelectedAccountBillingStreet = string.Empty;
            SelectedAccountBillingSuite = string.Empty;
            SelectedAccountBillingZone = string.Empty;
            SelectedAccountId = string.Empty;
            SelectedAccountInvoiceEmail = string.Empty;
            SelectedAccountInvoiceOverride = string.Empty;
            SelectedAccountIsEnabled = true;
            SelectedAccountName = string.Empty;
            SelectedAccountNotes = string.Empty;
            SelectedAccountOutstandingFormatted = string.Empty;
            SelectedAccountPhone = string.Empty;
            SelectedAccountRequireReferenceField = false;
            SelectedAccountSalesRep = string.Empty;
            SelectedAccountShippingAddressNotes = string.Empty;
            SelectedAccountShippingAdminName = string.Empty;
            SelectedAccountShippingAdminPhone = string.Empty;
            SelectedAccountShippingCity = string.Empty;
            SelectedAccountShippingCountry = string.Empty;
            SelectedAccountShippingPostalZip = string.Empty;
            SelectedAccountShippingProvState = string.Empty;
            SelectedAccountShippingStreet = string.Empty;
            SelectedAccountShippingSuite = string.Empty;
            SelectedAccountShippingZone = string.Empty;

            SelectedAccountCompany = null;

        }

        public List<string> SaveAccountDetails(Dictionary<string, Control> controls)
        {
            List<string> errors = Validate(controls);
            if (errors.Count == 0)
            {
                Logging.WriteLogLine("Saving Account: " + SelectedAccountName + " (" + SelectedAccountId + ")");

                Task.Run(async () =>
                {
                    try
                    {
                        PrimaryCompany primaryCompany = null;
                        Company company = new Company();
                        Company billingCompany = new Company();
                        
                        if (SelectedAccountCompany != null)
                        {
                            // Updating
                            company = SelectedAccountCompany;
                            primaryCompany = await Azure.Client.RequestGetPrimaryCompanyByCompanyName(SelectedAccountName);
                            billingCompany = primaryCompany.Company;
                        }
                        else
                        {
                            // Only update if NOT editing - new Account
                            company.CompanyNumber = SelectedAccountId;
                            primaryCompany = new PrimaryCompany();
                        }                        

                        company.CompanyName = SelectedAccountName;
                        company.CompanyNumber = SelectedAccountId;

                        company.UserName = SelectedAccountShippingAdminName;
                        company.Phone = SelectedAccountShippingAdminPhone;
                        company.Password = SelectedAccountAdminUserPassword;

                        company.Suite = SelectedAccountShippingSuite;
                        company.AddressLine1 = SelectedAccountShippingStreet;
                        company.City = SelectedAccountShippingCity;
                        company.Region = SelectedAccountShippingProvState;
                        company.PostalCode = SelectedAccountShippingPostalZip;
                        company.Country = SelectedAccountShippingCountry;

                        company.UserName = SelectedAccountAdminName;
                        //company.Password = SelectedAccountAdminUserPassword;
                        company.Password = Password.Password;


                        billingCompany.CompanyName = SelectedAccountName;
                        billingCompany.CompanyNumber = SelectedAccountId;

                        billingCompany.UserName = SelectedAccountBillingAdminName;
                        billingCompany.Phone = SelectedAccountBillingAdminPhone;
                        //billingCompany.Password = SelectedAccountAdminUserPassword;
                        billingCompany.Password = Password.Password;

                        billingCompany.Suite = SelectedAccountBillingSuite;
                        billingCompany.AddressLine1 = SelectedAccountBillingStreet;
                        billingCompany.City = SelectedAccountBillingCity;
                        billingCompany.Region = SelectedAccountBillingProvState;
                        billingCompany.PostalCode = SelectedAccountBillingPostalZip;
                        billingCompany.Country = SelectedAccountBillingCountry;

                        //billingCompany.UserName = SelectedAccountAdminName;
                        //billingCompany.Password = SelectedAccountAdminUserPassword;


                        primaryCompany.Company = company;
                        primaryCompany.BillingCompany = billingCompany;
                        primaryCompany.CustomerCode = SelectedAccountId;
                        primaryCompany.LoginCode = SelectedAccountId;

                        primaryCompany.UserName = SelectedAccountAdminName;
                        primaryCompany.Password = SelectedAccountAdminUserPassword;

                        // string loginCode = await Azure.Client.RequestGenerateLoginCode(billingCompany.UserName, billingCompany.CompanyNumber, billingCompany.AddressLine1);
                        string loginCode = "";
                        // The parameter is used for logging in the server
                        AddUpdatePrimaryCompany aupc = new AddUpdatePrimaryCompany("AccountsModel")
                        {
                            // Password = Utils.Encryption.ToTimeLimitedToken(""), // From Terry
                            Password = Utils.Encryption.ToTimeLimitedToken(SelectedAccountAdminUserPassword),
                            Company = company,
                            BillingCompany = billingCompany,
                            UserName = billingCompany.UserName,
                            CustomerCode = billingCompany.CompanyNumber,
                            SuggestedLoginCode = loginCode
                        };
                        string tmp = await Azure.Client.RequestAddUpdatePrimaryCompany(aupc);
                        Logging.WriteLogLine("Response: " + tmp);
                        
                        // Reload the account list
                        Task.WaitAll(Task.Run(() =>
                        {
                            Dispatcher.Invoke(() =>
                            {
                                var discard = GetAccountNames();

                                UpdateTripEntryPagesWithNewAccounts();
                            });
                        }));
                        //GetAccountNames();

                    }
                    catch (Exception e)
                    {
                        Logging.WriteLogLine("Exception thrown when saving Account Details: " + e.ToString());
                    }
                });                
            }

            return errors;
        }

        private void UpdateTripEntryPagesWithNewAccounts()
        {
            var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
            if (tis != null && tis.Count > 0)
            {
                for (int i = 0; i < tis.Count; i++)
                {
                    var ti = tis[i];
                    if (ti.Content is TripEntry)
                    {
                        Logging.WriteLogLine("Updating Accounts in extant " + Globals.TRIP_ENTRY + " tabs");

                        // Reload
                        Dispatcher.Invoke(() =>
                        {
                            ((TripEntryModel)ti.Content.DataContext).ReloadPrimaryCompanies();
                        });
                    }
                }                
            }
        }

        private List<string> Validate(Dictionary<string, Control> controls)
        {
            List<string> errors = new List<string>();

            foreach (string fieldName in controls.Keys)
            {
                string error = string.Empty;
                switch (fieldName)
                {
                    case "SelectedAccountId":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationAccountId"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountName":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationAccountName"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountPhone":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationAccountPhone"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    
                    case "SelectedAccountBillingStreet":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationBillingStreet"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountBillingCity":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationBillingCity"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountBillingProvState":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationBillingProvState"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountBillingCountry":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationBillingCountry"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountBillingPostalZip":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationBillingPostalZip"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;

                    case "SelectedAccountShippingStreet":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationShippingStreet"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountShippingCity":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationShippingCity"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountShippingProvState":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationShippingProvState"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountShippingCountry":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationShippingCountry"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                    case "SelectedAccountShippingPostalZip":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationShippingPostalZip"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;

                    case "SelectedAccountAdminUserId":
                        error = SetErrorState(controls[fieldName], (string)Application.Current.TryFindResource("AccountsValidationAdminUserId"));
                        if (!string.IsNullOrEmpty(error))
                        {
                            errors.Add(error);
                        }
                        break;
                }
            }

            return errors;
        }

        private string SetErrorState(Control ctrl, string message)
        {
            string error = string.Empty;
            if (IsControlEmpty(ctrl))
            {
                ctrl.Background = Brushes.Yellow;
                error = message;
            }
            else
            {
                ctrl.Background = Brushes.White;
            }

            return error;
        }

        private bool IsControlEmpty(Control ctrl)
        {
            bool isEmpty = true;
            if (ctrl is TextBox tb)
            {
                isEmpty = IsTextBoxEmpty(tb);
            }
            else if (ctrl is ComboBox cb)
            {
                isEmpty = IsComboBoxSelected(cb);
            }
            else if (ctrl is DateTimePicker dtp)
            {
                isEmpty = IsDateTimePickerEmpty(dtp);
            }

            return isEmpty;
        }

        private bool IsTextBoxEmpty(TextBox tb)
        {
            bool isEmpty = true;

            if (!string.IsNullOrEmpty(tb.Text))
            {
                isEmpty = false;
            }

            return isEmpty;
        }

        private bool IsComboBoxSelected(ComboBox cb)
        {
            bool isEmpty = true;

            if (cb.SelectedIndex > -1)
            {
                isEmpty = false;
            }

            return isEmpty;
        }

        private bool IsDateTimePickerEmpty(DateTimePicker dtp)
        {
            bool isEmpty = true;

            if (!string.IsNullOrEmpty(dtp.Text))
            {
                isEmpty = false;
            }

            return isEmpty;
        }

        // [DependsUpon (nameof(AdhocFilter))]
        public void Execute_Filter(string filter)
        {
            AdhocFilter = filter;

            ObservableCollection<string> names = new ObservableCollection<string>();
            if (string.IsNullOrEmpty(filter))
            {
                //GetAccountNames();
                foreach (string name in dictCustomerNamesIds.Keys)
                {
                    names.Add(name);
                }
            }
            else
            {
                foreach (string name in AccountNames)
                {
                    if (name.ToLower().Contains(filter.ToLower()))
                    {
                        names.Add(name);
                    }
                }

            }

            AccountNames = names;
        }

        #endregion
    }
}
