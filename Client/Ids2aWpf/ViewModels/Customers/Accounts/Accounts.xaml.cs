﻿using IdsControlLibraryV2.TabControl;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Utils;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.Customers.Accounts
{
    /// <summary>
    /// Interaction logic for Accounts.xaml
    /// </summary>
    public partial class Accounts : Page
    {
        private readonly Dictionary<string, Control> dictControls = new Dictionary<string, Control>();

        public Accounts()
        {
            InitializeComponent();

            if (DataContext is AccountsModel Model)
            {
                // Trying to force loading into sidebar
                // var tmp = Model.AccountNames;
                //Model.AccountNames = Model.GetAccountNames();
                
                //this.lvAccounts.SelectedIndex = 0;
                //this.LvAccounts_MouseDoubleClick(this.lvAccounts, null);

                // this.BtnCollapse_Click(null, null);


                Model.Password = this.pbAccountAdminPassword;
                Model.ConfirmPassword = this.pbAccountAdminConfirmPassword;
            }
        }

        private void LoadMappingDictionary()
        {
            foreach (var ctrl in this.GetChildren())
            {
                Binding binding = null;
                if (ctrl is TextBox tb)
                {
                    binding = BindingOperations.GetBinding(tb, TextBox.TextProperty);
                }
                else if (ctrl is ComboBox cb)
                {
                    binding = BindingOperations.GetBinding(cb, ComboBox.ItemsSourceProperty);
                }
                else if (ctrl is DateTimePicker dtp)
                {
                    binding = BindingOperations.GetBinding(dtp, DateTimePicker.ValueProperty);
                }
                if (binding != null)
                {
                    string name = binding.Path.Path;
                    if (!dictControls.ContainsKey(name))
                    {
                        dictControls.Add(name, (Control)ctrl);
                    }
                    else
                    {
                        Utils.Logging.WriteLogLine("ERROR " + name + " already exists");
                    }
                }
            }
        }

        private void DisplayNotImplementedMessage()
        {
            MessageBox.Show("Not implemented yet", "Not Implemented", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.menuMain.Visibility = Visibility.Collapsed;
            this.toolbar.Visibility = Visibility.Visible;
        }
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.menuMain.Visibility = Visibility.Visible;
            this.toolbar.Visibility = Visibility.Collapsed;
        }

        private void Toolbar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.menuMain.Visibility = Visibility.Visible;
            this.toolbar.Visibility = Visibility.Collapsed;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            //this.DisplayNotImplementedMessage();
            if (DataContext is AccountsModel Model)
            {
                if (this.dictControls.Count == 0)
                {
                    this.LoadMappingDictionary();
                }
                List<string> errors = Model.SaveAccountDetails(dictControls);
                if (errors.Count > 0)
                {
                    // TODO MessageBox
                    string title = (string)Application.Current.TryFindResource("AccountsValidationTitle");
                    StringBuilder sb = new StringBuilder();
                    sb.Append((string)Application.Current.TryFindResource("AccountsValidationErrors")).Append("\n");
                    foreach (string error in errors)
                    {
                        sb.Append("\t").Append(error).Append("\n");
                    }
                    sb.Append((string)Application.Current.TryFindResource("AccountsValidationErrors1"));

                    MessageBox.Show(sb.ToString(), title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    string title = (string)Application.Current.TryFindResource("AccountsAccountSavedTitle");
                    string message = (string)Application.Current.TryFindResource("AccountsAccountSaved");
                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Asterisk);

                    // Reload the account to lock the accountid field
                    string selectedAccountName = Model.SelectedAccountName;
                    this.BtnClear_Click(null, null);
                    Model.SelectedAccountName = selectedAccountName;
                }

            }
        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            this.DisplayNotImplementedMessage();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            //this.DisplayNotImplementedMessage();
            if (DataContext is AccountsModel Model)
            {
                Model.ClearAccountDetails();
            }
        }

        private void BtnAudit_Click(object sender, RoutedEventArgs e)
        {
            this.DisplayNotImplementedMessage();
        }

        private void BtnPricing_Click(object sender, RoutedEventArgs e)
        {
            this.DisplayNotImplementedMessage();
        }

        private void BtnAddressBook_Click(object sender, RoutedEventArgs e)
        {
            //this.DisplayNotImplementedMessage();
            // This opens the main address book without an account selected
            var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
            if (tis != null && tis.Count > 0)
            {
                PageTabItem pti = null;
                for (int i = 0; i < tis.Count; i++)
                {
                    var ti = tis[i];
                    if (ti.Content is AddressBook.AddressBook)
                    {
                        pti = ti;
                        // Load the trip into the first TripEntry tab
                        Logging.WriteLogLine("Switching to extant " + Globals.ADDRESS_BOOK + " tab");

                        //((AddressBook.AddressBookModel)pti.Content.DataContext).CurrentTrip = selectedTrip;
                        Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

                        break;
                    }
                }

                if (pti == null)
                {
                    // Need to create a new Trip Entry tab
                    Utils.Logging.WriteLogLine("Opening new " + Globals.ADDRESS_BOOK + " tab");
                    Globals.RunProgram(Globals.ADDRESS_BOOK);
                }
            }
        }

        private void BtnUsers_Click(object sender, RoutedEventArgs e)
        {
            this.DisplayNotImplementedMessage();
        }

        private void BtnCollapse_Click(object sender, RoutedEventArgs e)
        {
            this.spSidePanel.Visibility = Visibility.Collapsed;
            this.btnExpand.Visibility = Visibility.Visible;
        }

        private void BtnExpand_Click(object sender, RoutedEventArgs e)
        {
            this.spSidePanel.Visibility = Visibility.Visible;
            this.btnExpand.Visibility = Visibility.Collapsed;
        }

        private void BtnAddressBookForAccount_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Utils.Logging.WriteLogLine("Show Address Book for Account clicked");
            if (DataContext is AccountsModel Model)
            {
                var tis = Globals.DataContext.MainDataContext.ProgramTabItems;
                if (tis != null && tis.Count > 0)
                {
                    PageTabItem pti = null;
                    for (int i = 0; i < tis.Count; i++)
                    {
                        var ti = tis[i];
                        if (ti.Content is AddressBook.AddressBook)
                        {
                            pti = ti;
                            // Load the trip into the first TripEntry tab
                            Logging.WriteLogLine("Switching to extant " + Globals.ADDRESS_BOOK + " tab");

                            ((AddressBook.AddressBookModel)pti.Content.DataContext).SelectedAccountName = Model.SelectedAccountName;
                            Globals.DataContext.MainDataContext.TabControl.SelectedIndex = i;

                            break;
                        }
                    }

                    if (pti == null)
                    {
                        // Need to create a new Trip Entry tab
                        Utils.Logging.WriteLogLine("Opening new " + Globals.ADDRESS_BOOK + " tab");
                        // TODO this does open a new tab but it doesn't load the desired account's addresses
                        Globals.RunProgram(Globals.ADDRESS_BOOK, Model.SelectedAccountName);
                    }
                }
            }
        }

        private void CbDrivers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.DisplayNotImplementedMessage();
        }

        private void BtnSendWelcomeEmail_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Utils.Logging.WriteLogLine("Send Welcome Email clicked");
        }

        private void BtnUserCallersList_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Utils.Logging.WriteLogLine("Show User Callers List clicked");
        }

        private void imageRevealPassword_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Utils.Logging.WriteLogLine("Showing password");
            this.imageRevealPassword.Source = new BitmapImage(new Uri("/ViewModels;component/Resources/Images/eye-open-24.png", UriKind.RelativeOrAbsolute));
            this.ViewPassword.Text = this.pbAccountAdminPassword.Password;
            this.pbAccountAdminPassword.Visibility = Visibility.Hidden;
            this.ViewPassword.Visibility = Visibility.Visible;

            this.ViewConfirmPassword.Text = this.pbAccountAdminConfirmPassword.Password;
            this.pbAccountAdminConfirmPassword.Visibility = Visibility.Hidden;
            this.ViewConfirmPassword.Visibility = Visibility.Visible;
        }

        private void imageRevealPassword_MouseLeave(object sender, MouseEventArgs e)
        {
            //Utils.Logging.WriteLogLine("Hiding password");
            this.imageRevealPassword.Source = new BitmapImage(new Uri("/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute));
            this.ViewPassword.Text = string.Empty;
            this.pbAccountAdminPassword.Visibility = Visibility.Visible;
            this.ViewPassword.Visibility = Visibility.Hidden;

            this.ViewConfirmPassword.Text = string.Empty;
            this.pbAccountAdminConfirmPassword.Visibility = Visibility.Visible;
            this.ViewConfirmPassword.Visibility = Visibility.Hidden;
        }

        private void imageRevealPassword_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //Utils.Logging.WriteLogLine("Hiding password");
            this.imageRevealPassword.Source = new BitmapImage(new Uri("/ViewModels;component/Resources/Images/eye-closed-24.png", UriKind.RelativeOrAbsolute));
            this.ViewPassword.Text = string.Empty;
            this.pbAccountAdminPassword.Visibility = Visibility.Visible;
            this.ViewPassword.Visibility = Visibility.Hidden;

            this.ViewConfirmPassword.Text = string.Empty;
            this.pbAccountAdminConfirmPassword.Visibility = Visibility.Visible;
            this.ViewConfirmPassword.Visibility = Visibility.Hidden;
        }

        private void LvAccounts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string selectedAccountName = (string)this.lvAccounts.SelectedItem;
            Utils.Logging.WriteLogLine("Double-clicked Account in list: " + selectedAccountName);

            if (DataContext is AccountsModel Model)
            {
                Model.SelectedAccountName = selectedAccountName;
            }
        }

        private void CbAccountBillingCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is AccountsModel Model)
            {
                Model.SelectedBillingCountry = (string)cbAccountBillingCountry.SelectedItem;
                cbAccountBillingProvState.SelectedIndex = 0;
            }
        }

        private void CbAccountShippingCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is AccountsModel Model)
            {
                Model.SelectedShippingCountry = (string)cbAccountShippingCountry.SelectedItem;
                cbAccountShippingProvState.SelectedIndex = 0;
            }
        }

        private void TbFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox tb && DataContext is AccountsModel Model)
            {
                Model.Execute_Filter(tb.Text.Trim());
            }
        }
    }
}
