﻿using AzureRemoteService;
using IdsControlLibraryV2.Utils.Colours;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Utils;
using Xceed.Wpf.Toolkit;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace ViewModels.RateWizard
{
    class RateWizardModel : ViewModelBase
    {
        public List<ServiceLevel> ServiceLevelObjects = new List<ServiceLevel>();
        public List<DisplayServiceLevel> DisplayServiceLevelObjects = new List<DisplayServiceLevel>();

        public List<ServiceLevel> ServiceLevelObjectsToDelete = new List<ServiceLevel>();

        public List<PackageType> PackageTypeObjects = new List<PackageType>();

        //public List<PackageType> PackageTypeObjects
        //{
        //    get { return Get(() => PackageTypeObjects, new List<PackageType>()); }
        //    set { Set(() => PackageTypeObjects, value); }
        //}


        public List<PackageType> PackageTypeObjectsToDelete = new List<PackageType>();


        //public List<ServiceLevel> ServiceLevelObjects
        //{
        //    get { return Get(() => ServiceLevelObjects); }
        //    set { Set(() => ServiceLevelObjects, value); }
        //}

        #region Common


        public bool ArePendingServiceLevels
        {
            get { return Get(() => ArePendingServiceLevels, false); }
            set { Set(() => ArePendingServiceLevels, value); }
        }

        public bool ArePendingPackageTypes
        {
            get { return Get(() => ArePendingPackageTypes, false); }
            set { Set(() => ArePendingPackageTypes, value); }
        }

        public bool ArePendingZones
        {
            get { return Get(() => ArePendingZones, false); }
            set { Set(() => ArePendingZones, value); }
        }

        [DependsUpon (nameof(ArePendingServiceLevels))]
        [DependsUpon (nameof(ArePendingPackageTypes))]
        [DependsUpon (nameof(ArePendingZones))]
        public string ArePendingChanges
        {
            get
            {
                bool pending = (ArePendingServiceLevels || ArePendingPackageTypes || ArePendingZones);
                // return pending;
                string visibility = "Collapsed";
                if (pending)
                {
                    visibility = "Visible";
                }
                return visibility;
            }
        }
        public void SaveAll()
        {
            Logging.WriteLogLine("Saving all tabs");

            this.SaveServiceLevels();
            this.SavePackageTypes();            
            this.Execute_SaveZones();            
        }

        #endregion

        #region ServiceLevels Data

        public ObservableCollection<string> ServiceLevels
        {
            get { return Get(() => ServiceLevels, GetServiceLevels); }
            set { Set(() => ServiceLevels, value); }
        }


        //public ObservableCollection<DisplayServiceLevel> DisplayServiceLevels
        //{
        //    get { return Get(() => DisplayServiceLevels, GetServiceLevels); }
        //    set { Set(() => DisplayServiceLevels, value); }
        //}

        private ObservableCollection<DisplayServiceLevel> GetServiceLevels_testing()
        {
            ObservableCollection<DisplayServiceLevel> serviceLevels = new ObservableCollection<DisplayServiceLevel>();

            Task.Run(async () =>
            {
                try
                {
                    Logging.WriteLogLine("Getting service levels...");
                    // From Terry: Need to do this to avoid bad requests in the server log
                    ServiceLevelList sls = null;
                    if (!IsInDesignMode)
                    {
                        sls = await Azure.Client.RequestGetServiceLevels();
                    }

                    if (sls != null)
                    {
                        this.ServiceLevelObjects.Clear();
                        //List<string> strings = new List<string>();

                        SortedDictionary<int, ServiceLevel> sd = new SortedDictionary<int, ServiceLevel>();
                        //ushort sortOrder = 0;
                        foreach (string name in sls)
                        {
                            var result = await Azure.Client.RequestGetServiceLevel(name);
                            if (result != null)
                            {
                                ServiceLevel sl = result.ServiceLevel;
                                Logging.WriteLogLine("Loading ServiceLevel: " + name + ", sortOrder: " + sl.SortOrder);
                                //sl.SortOrder = sortOrder++;
                                // Duplicate - put at end
                                if (sd.ContainsKey(sl.SortOrder))
                                {
                                    sl.SortOrder = (ushort)sd.Count;

                                    await Azure.Client.RequestAddUpdateServiceLevel(sl);
                                }
                                sd.Add(sl.SortOrder, sl);
                                //this.ServiceLevelObjects.Add(sl);
                            }
                        }

                        List<string> slNames = new List<string>();
                        foreach (int key in sd.Keys)
                        {
                            // slNames.Add(sd[key].N);
                            string line = this.FormatServiceLevelDetailed(sd[key]);
                            Utils.Logging.WriteLogLine("Adding " + line);
                            slNames.Add(line);
                            // Make sure that they are in matching order
                            this.ServiceLevelObjects.Add(sd[key]);
                            DisplayServiceLevel dsl = new DisplayServiceLevel(sd[key])
                            {
                                Name = line
                            };
                            this.DisplayServiceLevelObjects.Add(dsl);
                        }

                        //Dispatcher.Invoke(() =>
                        //{
                        //    //serviceLevels.AddRange(slNames);
                        //    foreach (string name in slNames)
                        //    {
                        //        serviceLevels.Add(name);
                        //    }
                        //});

                        // serviceLevels.AddRange(slNames);

                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                    Logging.WriteLogLine("Exception thrown: " + e.ToString());
                    MessageBox.Show("Error fetching Company Names\n" + e.ToString(), "Error", MessageBoxButton.OK);
                }
            });

            return serviceLevels;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<string> GetServiceLevels()
        {
            ObservableCollection<string> serviceLevels = new ObservableCollection<string>();

            Task.Run(async () =>
                     {
                         try
                         {
                             Logging.WriteLogLine("Getting service levels...");
                             // From Terry: Need to do this to avoid bad requests in the server log
                             ServiceLevelList sls = null;
                             if (!IsInDesignMode)
                             {
                                 sls = await Azure.Client.RequestGetServiceLevels();
                             }

                             if (sls != null)
                             {
                                 this.ServiceLevelObjects.Clear();
                                 //List<string> strings = new List<string>();

                                 SortedDictionary<int, ServiceLevel> sd = new SortedDictionary<int, ServiceLevel>();
                                 //ushort sortOrder = 0;
                                 foreach (string name in sls)
                                 {
                                     var result = await Azure.Client.RequestGetServiceLevel(name);
                                     if (result != null)
                                     {
                                         ServiceLevel sl = result.ServiceLevel;
                                         Logging.WriteLogLine("Loading ServiceLevel: " + name + ", sortOrder: " + sl.SortOrder);
                                         //sl.SortOrder = sortOrder++;
                                         // Duplicate - put at end
                                         if (sd.ContainsKey(sl.SortOrder)) 
                                         {
                                             Logging.WriteLogLine("SortOrder already exists - changing it to " + (sd.Count + 1));
                                             sl.SortOrder = (ushort)(sd.Count + 1);

                                             await Azure.Client.RequestAddUpdateServiceLevel(sl);
                                         }
                                         sd.Add(sl.SortOrder, sl);
                                         //this.ServiceLevelObjects.Add(sl);
                                     }
                                 }

                                 List<string> slNames = new List<string>();
                                 foreach (int key in sd.Keys)
                                 {
                                     // slNames.Add(sd[key].N);
                                     string line = this.FormatServiceLevelDetailed(sd[key]);
                                     Utils.Logging.WriteLogLine("Adding " + line);
                                     slNames.Add(line);
                                     // Make sure that they are in matching order
                                     this.ServiceLevelObjects.Add(sd[key]);
                                     DisplayServiceLevel dsl = new DisplayServiceLevel(sd[key])
                                     {
                                         Name = line
                                     };
                                     this.DisplayServiceLevelObjects.Add(dsl);
                                 }

                                 Dispatcher.Invoke(() =>
                                                    {
                                                        //serviceLevels.AddRange(slNames);
                                                        foreach (string name in slNames)
                                                        {
                                                            serviceLevels.Add(name);
                                                        }
                                                    });

                                 // serviceLevels.AddRange(slNames);
                                 
                             }
                         }
                         catch (Exception e)
                         {
                             //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                             Logging.WriteLogLine("Exception thrown: " + e.ToString());
                             MessageBox.Show("Error fetching Company Names\n" + e.ToString(), "Error", MessageBoxButton.OK);
                         }
                     });

            return serviceLevels;
        }

        public string SelectedServiceLevel
        {
            get { return Get(() => SelectedServiceLevel, ""); }
            set { Set(() => SelectedServiceLevel, value); }
        }


        public string SelectedServiceLevelName
        {
            get { return Get(() => SelectedServiceLevelName, ""); }
            set { Set(() => SelectedServiceLevelName, value); }
        }


        public string SelectedServiceLevelStartTime
        {
            get { return Get(() => SelectedServiceLevelStartTime, ""); }
            set { Set(() => SelectedServiceLevelStartTime, value); }
        }

        public string SelectedServiceLevelEndTime
        {
            get { return Get(() => SelectedServiceLevelEndTime, ""); }
            set { Set(() => SelectedServiceLevelEndTime, value); }
        }
        
        public bool SelectedServiceLevelSun
        {
            get { return Get(() => SelectedServiceLevelSun, false); }
            set { Set(() => SelectedServiceLevelSun, value); }
        }

        public bool SelectedServiceLevelMon
        {
            get { return Get(() => SelectedServiceLevelMon, false); }
            set { Set(() => SelectedServiceLevelMon, value); }
        }

        public bool SelectedServiceLevelTue
        {
            get { return Get(() => SelectedServiceLevelTue, false); }
            set { Set(() => SelectedServiceLevelTue, value); }
        }

        public bool SelectedServiceLevelWed
        {
            get { return Get(() => SelectedServiceLevelWed, false); }
            set { Set(() => SelectedServiceLevelWed, value); }
        }

        public bool SelectedServiceLevelThu
        {
            get { return Get(() => SelectedServiceLevelThu, false); }
            set { Set(() => SelectedServiceLevelThu, value); }
        }

        public bool SelectedServiceLevelFri
        {
            get { return Get(() => SelectedServiceLevelFri, false); }
            set { Set(() => SelectedServiceLevelFri, value); }
        }

        public bool SelectedServiceLevelSat
        {
            get { return Get(() => SelectedServiceLevelSat, false); }
            set { Set(() => SelectedServiceLevelSat, value); }
        }


        public string SelectedServiceLevelDeliveryHours
        {
            get { return Get(() => SelectedServiceLevelDeliveryHours, ""); }
            set { Set(() => SelectedServiceLevelDeliveryHours, value); }
        }


        public string SelectedServiceLevelWarn
        {
            get { return Get(() => SelectedServiceLevelWarn, ""); }
            set { Set(() => SelectedServiceLevelWarn, value); }
        }


        public string SelectedServiceLevelCritical
        {
            get { return Get(() => SelectedServiceLevelCritical, ""); }
            set { Set(() => SelectedServiceLevelCritical, value); }
        }


        public string SelectedServiceLevelBeyond
        {
            get { return Get(() => SelectedServiceLevelBeyond, ""); }
            set { Set(() => SelectedServiceLevelBeyond, value); }
        }


        public bool IsEditingServiceLevel
        {
            get { return Get(() => IsEditingServiceLevel, false); }
            set { Set(() => IsEditingServiceLevel, value); }
        }

        [DependsUpon (nameof(IsEditingServiceLevel))]
        public bool IsServiceLevelTextBoxReadOnly
        {
            get { return Get(() => IsServiceLevelTextBoxReadOnly, !IsEditingServiceLevel); }
            set { Set(() => IsServiceLevelTextBoxReadOnly, value); }
        }


        public ServiceLevel CurrentlyEditedServiceLevel
        {
            get { return Get(() => CurrentlyEditedServiceLevel); }
            set { Set(() => CurrentlyEditedServiceLevel, value); }
        }





        public bool DoesServiceLevelNameExist(string name)
        {
            bool exists = false;
            
            if (this.ServiceLevelObjects.Count > 0)
            {
                foreach (ServiceLevel sl in this.ServiceLevelObjects)
                {
                    if (sl.N.Trim() == name)
                    {
                        exists = true;
                        break;
                    }
                }
            }
            return exists;
        }

        public void ReloadServiceLevels()
        {
            ServiceLevels = GetServiceLevels();
        }



        public string HelpUriServiceLevel
        {
            get { return Get(() => HelpUriServiceLevel, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
        }
        
        #endregion

        #region ServiceLevel Commands

        //public void EditServiceLevel()
        //{
        //    string line = this.SelectedServiceLevel;
        //    // sl is the detailed string - need to extract the service level
        //    string slName = this.GetServiceLevelFromDetailed(line);
        //    //int selectedIndex = lbServiceLevels.SelectedIndex;

        //    List<ServiceLevel> sls = this.ServiceLevelObjects;

        //    //if (selectedIndex > -1)
        //    //{
        //    //    ServiceLevel sl = sls[selectedIndex];
        //    //    if (sl != null)
        //    //    {
        //    //        this.SelectedServiceLevelName = sl.N;
        //    //        this.SelectedServiceLevelStartTime = sl.StartTime.ToString();
        //    //        this.SelectedServiceLevelEndTime = sl.EndTime.ToString();
        //    //    }
        //    //}
        //}

        //private string GetServiceLevelFromDetailed(string line)
        //{
        //    string sl = string.Empty;

        //    // Service[Next Day,DEL hrs: 00:00:00,END time: 00:00:00]
        //    //int start = "Service[".Length;
        //    int start = "[".Length;
        //    string endTag = ",DEL";
        //    int end = line.IndexOf(endTag) - endTag.Length;
        //    sl = line.Substring(start, end);

        //    return sl;
        //}

        /// <summary>
        /// Note: sortOrder hasn't been modified yet.
        /// </summary>
        /// <param name="sl"></param>
        /// <param name="newSortOrder"></param>
        public void MoveServiceLevelLocally(ServiceLevel sl, int newSortOrder)
        {
            Logging.WriteLogLine("Moving ServiceLevel from " + sl.SortOrder + " to " + newSortOrder);


            this.ServiceLevelObjects[newSortOrder].SortOrder = sl.SortOrder;
            this.ServiceLevelObjects[sl.SortOrder].SortOrder = (ushort)newSortOrder;
            

            SortedDictionary<int, ServiceLevel> sd = new SortedDictionary<int, ServiceLevel>();

            foreach (ServiceLevel tmp in this.ServiceLevelObjects)
            {
                if (!sd.ContainsKey(tmp.SortOrder))
                {
                    sd.Add(tmp.SortOrder, tmp);
                } 
                else
                {
                    Logging.WriteLogLine("ERROR sd already contains sortOrder: " + tmp.SortOrder);
                }
            }

            List<ServiceLevel> newList = new List<ServiceLevel>();
            foreach (int key in sd.Keys)
            {
                newList.Add(sd[key]);
            }

            this.ServiceLevelObjects.Clear();
            this.ServiceLevelObjects.AddRange(newList);

            List<string> slNames = new List<string>();
            foreach (ServiceLevel tmp in this.ServiceLevelObjects)
            {
                string line = this.FormatServiceLevelDetailed(tmp);
                slNames.Add(line);
            }
            ObservableCollection<string> serviceLevels = new ObservableCollection<string>();
            Dispatcher.Invoke(() =>
                                {
                                    //serviceLevels.AddRange(slNames);
                                    foreach (string name in slNames)
                                    {
                                        serviceLevels.Add(name);
                                    }
                                });

            this.ServiceLevels = serviceLevels;

            this.ArePendingServiceLevels = true;
        }

        public void DeleteServiceLevelLocally(ServiceLevel sl)
        {
            if (sl != null)
            {
                string slName = sl.NewName;
                Logging.WriteLogLine("Deleting Service Level: " + slName);


                List<ServiceLevel> newList = new List<ServiceLevel>();
                foreach (ServiceLevel tmp in ServiceLevelObjects)
                {
                    if (tmp.N == slName)
                    {
                        // OldName == "Value", NewName == "" Is a Delete
                        sl.OldName = slName;
                        sl.NewName = string.Empty;
                        ServiceLevelObjectsToDelete.Add(sl);
                    }
                    else
                    {
                        newList.Add(tmp);
                    }
                }
                this.ServiceLevelObjects.Clear();
                this.ServiceLevelObjects.AddRange(newList);

                List<string> slNames = new List<string>();
                foreach (ServiceLevel tmp in this.ServiceLevelObjects)
                {
                    string line = this.FormatServiceLevelDetailed(tmp);
                    slNames.Add(line);
                }
                ObservableCollection<string> serviceLevels = new ObservableCollection<string>();
                Dispatcher.Invoke(() =>
                                    {
                                    //serviceLevels.AddRange(slNames);
                                    foreach (string name in slNames)
                                        {
                                            serviceLevels.Add(name);
                                        }
                                    });

                this.ServiceLevels = serviceLevels;

                this.ArePendingServiceLevels = true;
            }
        }

        public void SaveServiceLevels()
        {
            Logging.WriteLogLine("Saving Service Levels");

            Task.Run(async () =>
                            {
                                try
                                {
                                    foreach (ServiceLevel sl in ServiceLevelObjects)
                                    {
                                        Logging.WriteLogLine("Saving NewName: " + sl.NewName + ", OldName: " + sl.OldName);
                                        await Azure.Client.RequestAddUpdateServiceLevel(sl);
                                    }

                                    foreach (ServiceLevel sl in ServiceLevelObjectsToDelete)
                                    {
                                        Logging.WriteLogLine("Deleting NewName: " + sl.NewName + ", OldName: " + sl.OldName);
                                        await Azure.Client.RequestAddUpdateServiceLevel(sl);
                                    }

                                    ObservableCollection<string> sls = GetServiceLevels();
                                    Dispatcher.Invoke(() =>
                                    {
                                        ServiceLevels = sls;
                                    });

                                    this.ArePendingServiceLevels = false;
                                }
                                catch (Exception e)
                                {
                                    Logging.WriteLogLine("Exception thrown: " + e.ToString());
                                    MessageBox.Show("Error Saving Service Levels:\n" + e.ToString(), "Error", MessageBoxButton.OK);
                                }
                            });

        }



        //public IList ServiceLevelDetailedStrings
        //{
        //    get { return Get(() => ServiceLevelDetailedStrings, GetServiceLevelDetailedStrings); }
        //    set { Set(() => ServiceLevelDetailedStrings, value); }
        //}

        //private IList GetServiceLevelDetailedStrings()
        //{
        //    List<string> strings = new List<string>();

        //    GetServiceLevels();

        //    if (this.serviceLevelObjects.Count > 0)
        //    {
        //        foreach (ServiceLevel sl in this.serviceLevelObjects)
        //        {
        //            string line = this.FormatServiceLevelDetailed(sl);
        //            strings.Add(line);
        //        }
        //    }

        //    return strings;
        //}

        private string FormatServiceLevelDetailed(ServiceLevel sl)
        {
            string service = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionService");
            //string delHrs = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionDelHrs") + " " + sl.DeliveryHours;
            //string endTime = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionEndTime") + " " + sl.EndTime;
            string delHrs = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionDelHrs") + " " + DateTimeOffset.MinValue;
            string endTime = (string)Application.Current.TryFindResource("RateWizardTabServiceLevelDescriptionEndTime") + " " + DateTimeOffset.MinValue;

            string line = service + " [" + sl.N.Trim() + "," + delHrs + "," + endTime + "]";

            return line;
        }

        private void UpdateLocalServiceLevels(ServiceLevel sl)
        {
            string slName = sl.NewName;
            
            Logging.WriteLogLine("Updating " + slName);

            bool exists = false;
            for (int i = 0; i < this.ServiceLevelObjects.Count; i++)
            {
                ServiceLevel level = this.ServiceLevelObjects[i];
                if (level.N == slName)
                {
                    exists = true;
                    this.ServiceLevelObjects[i] = sl;
                    break;
                }
            }

            if (!exists)
            {
                sl.OldName = string.Empty;
                sl.NewName = sl.N;
                this.ServiceLevelObjects.Add(sl);
            }


            List<string> slNames = new List<string>();
            foreach (ServiceLevel tmp in this.ServiceLevelObjects)
            {
                string line = this.FormatServiceLevelDetailed(tmp);
                slNames.Add(line);
            }
            ObservableCollection<string> serviceLevels = new ObservableCollection<string>();
            Dispatcher.Invoke(() =>
                                {
                                    //serviceLevels.AddRange(slNames);
                                    foreach (string name in slNames)
                                    {
                                        serviceLevels.Add(name);
                                    }
                                });

            this.ServiceLevels = serviceLevels;

            this.ArePendingServiceLevels = true;
        }

        /// <summary>
        /// Handles the update process.
        /// </summary>
        /// <param name="sl"></param>
        /// <returns></returns>
        public bool AddUpdateServiceLevel(ServiceLevel sl)
        {
            bool success = true;

            Logging.WriteLogLine("Adding/Updating Service Level: " + sl.N);            
            this.UpdateLocalServiceLevels(sl);

            this.ArePendingServiceLevels = true;
            
            return success;
        }


        public void Execute_ShowHelpServiceLevel()
        {
            Logging.WriteLogLine("Opening " + HelpUriServiceLevel);
            var uri = new Uri(HelpUriServiceLevel);
            Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
        }


        #endregion

        #region PackageTypes Data
        public IList PackageTypes
        {
            get { return Get(() => PackageTypes, GetPackageTypes()); }
            set { Set(() => PackageTypes, value); }
        }

        private List<string> GetPackageTypes()
        {
            List<string> packageTypes = new List<string>();

            //Task.Run(() =>
            Task.Run(async () =>
                     {
                         try
                         {
                             Logging.WriteLogLine("Getting package types...");
                             // From Terry: Need to do this to avoid bad requests in the server log
                             PackageTypeList pts = null;
                             if (!IsInDesignMode)
                             {
                                 pts = await Azure.Client.RequestGetPackageTypes();
                                 //pts = Azure.Client.RequestGetPackageTypes().Result;
                             }

                             if (pts != null)
                             {
                                 SortedDictionary<int, PackageType> sd = new SortedDictionary<int, PackageType>();

                                 // KLUDGE this task seems to run multiple time
                                 if (this.PackageTypeObjects.Count == 0)
                                 {
                                     this.PackageTypeObjects.Clear();

                                     foreach (PackageType pt in pts)
                                     {
                                         // If there is a duplicate sortOrder, put at bottom
                                         if (sd.ContainsKey(pt.SortOrder))
                                         {
                                             pt.SortOrder = (ushort)sd.Count;

                                             await Azure.Client.RequestAddUpdatePackageType(pt);
                                             //_ = Azure.Client.RequestAddUpdatePackageType(pt);
                                         }
                                         sd.Add(pt.SortOrder, pt);
                                         this.PackageTypeObjects.Add(pt);
                                     }
                                 }
                                 else
                                 {
                                     foreach (PackageType pt in this.PackageTypeObjects)
                                     {
                                         sd.Add(pt.SortOrder, pt);
                                     }
                                 }
                                 Dispatcher.Invoke(() =>
                                                 {
                                                     foreach (int Key in sd.Keys)
                                                     {
                                                         //packageTypes.Add(sd[Key].Description);
                                                         string line = this.FormatPackageTypeDetailed(sd[Key]);
                                                         //Logging.WriteLogLine("Adding " + line);
                                                         packageTypes.Add(line);
                                                     }
                                                 });
                             }
                         }
                         catch (Exception e)
                         {
                             //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                             Logging.WriteLogLine("Exception thrown: " + e.ToString());
                             MessageBox.Show("Error fetching Package Types\n" + e.ToString(), "Error", MessageBoxButton.OK);
                         }
                     });

            return packageTypes;
        }

        private List<string> GetPackageTypes_ootw()
        {
            List<string> packageTypes = new List<string>();

            try
            {
                // From Terry: Need to do this to avoid bad requests in the server log
                PackageTypeList pts = null;
                if (!IsInDesignMode)
                {
                    //pts = await Azure.Client.RequestGetPackageTypes();
                    Task.Run(() =>
                    {
                        pts = Azure.Client.RequestGetPackageTypes().Result;
                    });
                }

                if (pts != null)
                {
                    this.PackageTypeObjects.Clear();
                    SortedDictionary<int, PackageType> sd = new SortedDictionary<int, PackageType>();
                    // TODO need to sort by packageType sort order


                    foreach (PackageType pt in pts)
                    {
                        // If there is a duplicate sortOrder, put at bottom
                        if (sd.ContainsKey(pt.SortOrder))
                        {
                            pt.SortOrder = (ushort)sd.Count;

                            //await Azure.Client.RequestAddUpdatePackageType(pt);
                            _ = Azure.Client.RequestAddUpdatePackageType(pt);
                        }
                        sd.Add(pt.SortOrder, pt);
                        this.PackageTypeObjects.Add(pt);
                    }
                    Dispatcher.Invoke(() =>
                                    {
                                        foreach (int Key in sd.Keys)
                                        {
                                            //packageTypes.Add(sd[Key].Description);
                                            string line = this.FormatPackageTypeDetailed(sd[Key]);
                                            Logging.WriteLogLine("Adding " + line);
                                            packageTypes.Add(line);
                                        }
                                    });
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("GetCompanyNames: Exception thrown: " + e.ToString());
                Logging.WriteLogLine("Exception thrown: " + e.ToString());
                MessageBox.Show("Error fetching Package Types\n" + e.ToString(), "Error", MessageBoxButton.OK);
            }

            return packageTypes;
        }


        public string SelectedPackageTypeName
        {
            get { return Get(() => SelectedPackageTypeName, ""); }
            set { Set(() => SelectedPackageTypeName, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        private string FormatPackageTypeDetailed(PackageType pt)
        {
            string byWeight = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionByWeight") + ": " + pt.WeightXRate;            
            string hourly = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionHourly") + ": " + pt.HoursXRate;
            string formula = (string)Application.Current.TryFindResource("RateWizardTabPackageTypeDescriptionFormula") + ": " + pt.Formula;
            
            string line = pt.Description.Trim() + " [" + byWeight + "," + hourly + "," + formula + "]";

            return line;
        }


        public PackageType CurrentlyEditedPackageType
        {
            get { return Get(() => CurrentlyEditedPackageType); }
            set { Set(() => CurrentlyEditedPackageType, value); }
        }

        public bool IsEditingPackageType
        {
            get { return Get(() => IsEditingPackageType, false); }
            set { Set(() => IsEditingPackageType, value); }
        }

        [DependsUpon (nameof(IsEditingPackageType))]
        public bool IsPackageTypeTextBoxReadOnly
        {
            get { return Get(() => IsPackageTypeTextBoxReadOnly, !IsEditingPackageType); }
            set { Set(() => IsPackageTypeTextBoxReadOnly, value); }
        }

        public bool DoesPackageTypeNameExist(string name)
        {
            bool exists = false;

            if (this.PackageTypeObjects.Count > 0)
            {
                foreach (PackageType pt in this.PackageTypeObjects)
                {
                    if (pt.Description.Trim() == name)
                    {
                        exists = true;
                        break;
                    }
                }
            }
            return exists;
        }

        public string HelpUriPackageType
        {
            //get { return Get(() => HelpUriPackageType, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/639074305/Rate+Wizard+-+Package+Type"); }
            get { return Get(() => HelpUriPackageType, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
        }

        #endregion

        #region PackageTypes Commands

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="newSortOrder"></param>
        public void MovePackageTypeLocally(PackageType pt, int newSortOrder)
        {
            Logging.WriteLogLine("Moving PackageType from " + pt.SortOrder + " to " + newSortOrder);


            this.PackageTypeObjects[newSortOrder].SortOrder = pt.SortOrder;
            this.PackageTypeObjects[pt.SortOrder].SortOrder = (ushort)newSortOrder;


            SortedDictionary<int, PackageType> sd = new SortedDictionary<int, PackageType>();

            foreach (PackageType tmp in this.PackageTypeObjects)
            {
                if (!sd.ContainsKey(tmp.SortOrder))
                {
                    sd.Add(tmp.SortOrder, tmp);
                }
                else
                {
                    Logging.WriteLogLine("ERROR sd already contains sortOrder: " + tmp.SortOrder);
                }
            }

            List<PackageType> newList = new List<PackageType>();
            foreach (int key in sd.Keys)
            {
                newList.Add(sd[key]);
            }

            this.PackageTypeObjects.Clear();
            this.PackageTypeObjects.AddRange(newList);

            List<string> ptNames = new List<string>();
            foreach (PackageType tmp in this.PackageTypeObjects)
            {
                string line = this.FormatPackageTypeDetailed(tmp);
                ptNames.Add(line);
            }
            ObservableCollection<string> packageTypes = new ObservableCollection<string>();
            Dispatcher.Invoke(() =>
                                {
                                    //serviceLevels.AddRange(slNames);
                                    foreach (string name in ptNames)
                                    {
                                        packageTypes.Add(name);
                                    }
                                });

            this.PackageTypes = packageTypes;

            this.ArePendingPackageTypes = true;
        }

        public void DeletePackageTypeLocally(PackageType pt)
        {
            if (pt != null)
            {
                string ptName = pt.Description;
                Logging.WriteLogLine("Deleting PackageType: " + ptName);


                List<PackageType> newList = new List<PackageType>();
                foreach (PackageType tmp in PackageTypeObjects)
                {
                    if (tmp.Description == ptName)
                    {
                        pt.Deleted = true;
                        PackageTypeObjectsToDelete.Add(pt);
                    }
                    else
                    {
                        newList.Add(tmp);
                    }
                }
                this.PackageTypeObjects.Clear();
                this.PackageTypeObjects.AddRange(newList);

                List<string> ptNames = new List<string>();
                foreach (PackageType tmp in this.PackageTypeObjects)
                {
                    string line = this.FormatPackageTypeDetailed(tmp);
                    ptNames.Add(line);
                }
                ObservableCollection<string> packageTypes = new ObservableCollection<string>();
                Dispatcher.Invoke(() =>
                                    {
                                        foreach (string name in ptNames)
                                        {
                                            packageTypes.Add(name);
                                        }
                                    });

                this.PackageTypes = packageTypes;

                this.ArePendingPackageTypes = true;
            }
        }

        public void SavePackageTypes()
        {
            Logging.WriteLogLine("Saving PackageTypes");

            Task.Run(async () =>
                            {
                                try
                                {
                                    foreach (PackageType pt in PackageTypeObjects)
                                    {
                                        Logging.WriteLogLine("Saving PackageType: " + pt.Description);
                                        await Azure.Client.RequestAddUpdatePackageType(pt);
                                    }

                                    foreach (PackageType pt in PackageTypeObjectsToDelete)
                                    {
                                        Logging.WriteLogLine("Deleting PackageType: " + pt.Description);
                                        await Azure.Client.RequestAddUpdatePackageType(pt);
                                    }

                                    List<string> pts = GetPackageTypes();
                                    Dispatcher.Invoke(() =>
                                    {
                                        PackageTypes = pts;
                                    });


                                    this.ArePendingPackageTypes = false;
                                }
                                catch (Exception e)
                                {
                                    Logging.WriteLogLine("Exception thrown: " + e.ToString());
                                    MessageBox.Show("Error Saving Package Types:\n" + e.ToString(), "Error", MessageBoxButton.OK);
                                }
                            });

        }

        private void UpdateLocalPackageType(PackageType pt)
        {
            string ptName = pt.Description;

            Logging.WriteLogLine("Updating " + ptName);

            bool exists = false;
            for (int i = 0; i < this.PackageTypeObjects.Count; i++)
            {
                PackageType level = this.PackageTypeObjects[i];
                if (level.Description == ptName)
                {
                    exists = true;
                    this.PackageTypeObjects[i] = pt;
                    break;
                }
            }

            if (!exists)
            {
                this.PackageTypeObjects.Add(pt);
            }

            List<string> ptNames = new List<string>();
            foreach (PackageType tmp in this.PackageTypeObjects)
            {
                string line = this.FormatPackageTypeDetailed(tmp);
                ptNames.Add(line);
            }
            ObservableCollection<string> packageTypes = new ObservableCollection<string>();
            Dispatcher.Invoke(() =>
                                {
                                    //serviceLevels.AddRange(slNames);
                                    foreach (string name in ptNames)
                                    {
                                        packageTypes.Add(name);
                                    }
                                });

            this.PackageTypes = packageTypes;

            this.ArePendingPackageTypes = true;
        }
        public bool AddUpdatePackageType(PackageType pt)
        {
            bool success = true;
            Logging.WriteLogLine("Adding/Updating Package Type: " + pt.Description);


            this.UpdateLocalPackageType(pt);

            return success;
        }

        public void Execute_ShowHelpPackageType()
        {
            Logging.WriteLogLine("Opening " + HelpUriPackageType);
            var uri = new Uri(HelpUriPackageType);
            Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
        }

        #endregion

        #region Zones Data


        public List<Zone> Zones
        {
            get { return Get(() => Zones, GetZones); }
            set { Set(() => Zones, value); }
        }


        public bool IsEditingZone
        {
            get { return Get(() => IsEditingZone, false); }
            set { Set(() => IsEditingZone, value); }
        }


        public bool IsNewZone
        {
            get { return Get(() => IsNewZone, false); }
            set { Set(() => IsNewZone, value); }
        }



        public bool IsZoneTextBoxReadOnly
        {
            get { return Get(() => IsZoneTextBoxReadOnly, !IsEditingZone); }
            set { Set(() => IsZoneTextBoxReadOnly, value); }
        }


        public Zone CurrentlyEditedZone
        {
            get { return Get(() => CurrentlyEditedZone); }
            set { Set(() => CurrentlyEditedZone, value); }
        }


        public string SelectedZoneName
        {
            get { return Get(() => SelectedZoneName, ""); }
            set { Set(() => SelectedZoneName, value); }
        }


        public string SelectedZoneAbbreviation
        {
            get { return Get(() => SelectedZoneAbbreviation, ""); }
            set { Set(() => SelectedZoneAbbreviation, value); }
        }


        public int SelectedZoneSortIndex
        {
            get { return Get(() => SelectedZoneSortIndex, 0); }
            set { Set(() => SelectedZoneSortIndex, value); }
        }


        public string SelectedSortOrder
        {
            get { return Get(() => SelectedSortOrder, ""); }
            set { Set(() => SelectedSortOrder, value); }
        }

        public string HelpUriZones
        {
            get { return Get(() => HelpUriZones, "https://idsservices.atlassian.net/wiki/spaces/I2P/pages/644186113/How+to+use+Rate+Wizard"); }
        }

        #endregion

        #region Zones Commands

        public List<Zone> GetZones()
        {
            Logging.WriteLogLine("Loading Zones");
            List<Zone> zones = new List<Zone>();

            Task.Run(async () =>
            {
                List<Zone> newZones = new List<Zone>();
                Zones = await Azure.Client.RequestZones();

                Logging.WriteLogLine("Found " + Zones.Count + " Zones");

                if (Zones.IsNotNull() && Zones.Count > 0)
                {
                    zones = Zones.OrderBy(x => x.SortIndex).ToList();
                    Zones = zones;
                }
            });

            return zones;
        }

        public void Execute_SaveZones()
        {
            Logging.WriteLogLine("Saving Zones");
            Task.Run(async () =>
            {
                Zones z = new Zones();
                z.AddRange(this.Zones);
                await Azure.Client.RequestAddUpdateZones(z);

                ArePendingZones = false;
            });
        }

        public void Execute_ShowHelpZones()
        {
            Logging.WriteLogLine("Opening " + HelpUriServiceLevel);
            var uri = new Uri(HelpUriServiceLevel);
            Process.Start(new ProcessStartInfo(uri.AbsoluteUri));
        }

        public void MoveZoneLocally(Zone zone, int newSortOrder)
        {
            this.Zones[newSortOrder].SortIndex = zone.SortIndex;
            this.Zones[zone.SortIndex].SortIndex = (ushort)newSortOrder;

            Dispatcher.Invoke(() =>
            {
                List<Zone> zones = Zones.OrderBy(x => x.SortIndex).ToList();
                Zones = zones;
                string tmp = string.Empty;
                foreach (Zone z in Zones)
                {
                    tmp += z.Name + " (SortIndex: " + z.SortIndex + "), ";
                }
                Logging.WriteLogLine("Sorted zones: " + tmp);
    
                this.ArePendingZones = true;
            });
        }

        /// <summary>
        /// Make a list of the zones.
        /// If !new zone, remove the old one from the list.
        /// Then check that the new name doesn't exist.
        /// </summary>
        /// <returns></returns>
        public string AddUpdateZoneLocally()
        {
            string error = string.Empty;

            List<Zone> checkAgainst = new List<Zone>();
            checkAgainst.AddRange(Zones);

            if (CurrentlyEditedZone != null && !IsNewZone)
            {
                // Editing - remove the old zone
                checkAgainst.Remove(CurrentlyEditedZone);
            }

            foreach (Zone z in checkAgainst)
            {
                if (z.Name == SelectedZoneName.Trim())
                {
                    error = (string)Application.Current.TryFindResource("RateWizardTabZoneErrorDuplicateName");
                    break;
                }
                else if (z.Abbreviation == SelectedZoneAbbreviation)
                {
                    error = (string)Application.Current.TryFindResource("RateWizardTabZoneErrorDuplicateAbbreviation");
                    break;
                }
            }

            if (error.IsNullOrWhiteSpace())
            {
                // If new, create a zone, otherwise update the Current
                Zone zone = new Zone
                {
                    Name = SelectedZoneName.Trim(),
                    Abbreviation = SelectedZoneAbbreviation.Trim(),
                    SortIndex = SelectedZoneSortIndex
                };

                if (CurrentlyEditedZone == null)                
                {
                    zone.SortIndex = checkAgainst.Count; // Put at end
                }

                checkAgainst.Add(zone);
                checkAgainst = checkAgainst.OrderBy(x => x.SortIndex).ToList();
                Dispatcher.Invoke(() =>
                {
                    Zones = checkAgainst;
                });
                ArePendingZones = true;
            }

            return error;
        }

        /// <summary>
        /// Note - this reorders the SortIndex, removing the hole.
        /// </summary>
        /// <param name="zone"></param>
        public void DeleteZoneLocally(Zone zone)
        {
            Logging.WriteLogLine("Deleting from local list: " + zone.Name + " (" + zone.Abbreviation + "), SortIndex: " + zone.SortIndex );

            List<Zone> copy = new List<Zone>();
            int sortIndex = 0;
            foreach (Zone z in Zones)
            {
                if (z.Name != zone.Name)
                {
                    z.SortIndex = sortIndex++;
                    copy.Add(z);
                }                
            }

            Dispatcher.Invoke(() =>
            {
                Zones = copy;
            });

            ArePendingZones = true;
        }

        #endregion

    }

    /// <summary>
    /// Utility class to allow the listbox in the ServiceLevels to be coloured.
    /// </summary>
    public class DisplayServiceLevel
    {
        public SolidColorBrush Foreground,
                               Background;

        public string Name;

        public DisplayServiceLevel()
        {
            Foreground = new SolidColorBrush(Colors.Black);
            Background = new SolidColorBrush(Colors.White);
        }

        public DisplayServiceLevel(Protocol.Data.ServiceLevel s)
        {
            Foreground = s.ForegroundARGB.ArgbToSolidColorBrush();
            Background = s.BackgroundARGB.ArgbToSolidColorBrush();
        }
    }
}
