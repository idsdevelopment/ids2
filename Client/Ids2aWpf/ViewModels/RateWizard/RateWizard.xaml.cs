﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Protocol.Data;
using Utils;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace ViewModels.RateWizard
{
	/// <summary>
	///     Interaction logic for RateWizard.xaml
	/// </summary>
	public partial class RateWizard : Page
	{
		private void SaveAll( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
				Model.SaveAll();
		}

		private void cpForeground_SelectedColorChanged( object sender, RoutedPropertyChangedEventArgs<Color?> e )
		{
			Logging.WriteLogLine( "Setting foreground to " + cpForeground.SelectedColor );
		}

		private void cpBackground_SelectedColorChanged( object sender, RoutedPropertyChangedEventArgs<Color?> e )
		{
			Logging.WriteLogLine( "Setting background to " + cpBackground.SelectedColor );
		}

		public RateWizard()
		{
			InitializeComponent();

			//ToggleIsEditing();

			//this.lbServiceLevels.Items.Refresh();
		}

		//private string GetServiceLevelFromDetailed(string line)
		//{
		//    string sl = string.Empty;

		//    // Service[Next Day,DEL hrs: 00:00:00,END time: 00:00:00]
		//    //int start = "Service[".Length;
		//    int start = "[".Length;
		//    string endTag = ",DEL";
		//    int end = line.IndexOf(endTag) - endTag.Length;
		//    sl = line.Substring(start, end);

		//    return sl;
		//}

		/// <summary>
		///     Handles the updating of the UI
		/// </summary>

		//private void ToggleIsEditing()
		//{
		//    if (DataContext is RateWizardModel Model)
		//    {
		//        //this.btnServiceLevelEdit.IsEnabled = !Model.IsEditingServiceLevel;
		//        //this.btnServiceLevelAdd.IsEnabled = !Model.IsEditingServiceLevel;
		//        //this.btnServiceLevelDel.IsEnabled = !Model.IsEditingServiceLevel;
		//        //this.btnServiceLevelUp.IsEnabled = !Model.IsEditingServiceLevel;
		//        //this.btnServiceLevelDown.IsEnabled = !Model.IsEditingServiceLevel;

		//        //this.btnUpdate.IsEnabled = Model.IsEditingServiceLevel;
		//        //this.btnCancel.IsEnabled = Model.IsEditingServiceLevel;
		//        //this.btnClear.IsEnabled = Model.IsEditingServiceLevel;

		//        Model.IsTextBoxReadOnly = !Model.IsEditingServiceLevel;
		//    }
		//}

	#region Service Level Handlers

		private (ServiceLevel sl, string oldName) PopulateServiceLevel( ServiceLevel sl )
		{
			ServiceLevel newSl   = null;
			var          oldName = string.Empty;

			if( DataContext is RateWizardModel Model )
			{
				if( sl != null )
				{
					newSl   = sl;
					oldName = sl.N;
				}
				else
				{
					newSl = new ServiceLevel
					        {
						        // Put new ones at the bottom of the list
						        SortOrder = (ushort)lbServiceLevels.Items.Count
					        };
				}

				newSl.N       = Model.SelectedServiceLevelName;
				newSl.NewName = Model.SelectedServiceLevelName;

				static uint ToUint( ColorPicker p, Color defaultColor )
				{
					var C = p?.SelectedColor ?? defaultColor;

					return ( (uint)0xff << 24 ) | ( (uint)C.R << 16 ) | ( (uint)C.G << 8 ) | C.B; // Make sure we have the alpha channel
				}

				// ServiceLevel colours
				newSl.BackgroundARGB = ToUint( cpBackground, Colors.Transparent );
				newSl.ForegroundARGB = ToUint( cpForeground, Colors.Black );

				//newSl.StartTime = new TimeSpan(Model.SelectedServiceLevelStartTime);
				//newSl.EndTime = new TimeSpan(Model.SelectedServiceLevelEndTime);
			}

			return ( newSl, oldName );
		}

		private void BtnServiceLevelEdit_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				//string line = Model.SelectedServiceLevel;
				// sl is the detailed string - need to extract the service level
				// string slName = this.GetServiceLevelFromDetailed(line);
				var selectedIndex = lbServiceLevels.SelectedIndex;

				if( selectedIndex > -1 )
				{
					BtnServiceLevelClear_Click( null, null );
					Model.IsEditingServiceLevel = true;

					//this.ToggleIsEditing();

					var sls = Model.ServiceLevelObjects;

					var sl = sls[ selectedIndex ];

					if( sl != null )
					{
						Logging.WriteLogLine( "Editing Service Level: " + sl.N );
						Model.CurrentlyEditedServiceLevel = sl;

						Model.SelectedServiceLevelName = sl.N;

						// Model.SelectedServiceLevelStartTime = sl.StartTime.ToString();
						// Model.SelectedServiceLevelEndTime = sl.EndTime.ToString();

						// Colours
						var bytes = BitConverter.GetBytes( sl.BackgroundARGB );
						var bg    = new SolidColorBrush( Color.FromRgb( bytes[ 2 ], bytes[ 1 ], bytes[ 0 ] ) );
						cpBackground.SelectedColor = bg.Color;
						bytes                      = BitConverter.GetBytes( sl.ForegroundARGB );
						var fg = new SolidColorBrush( Color.FromRgb( bytes[ 2 ], bytes[ 1 ], bytes[ 0 ] ) );
						cpForeground.SelectedColor = fg.Color;
					}
				}
			}
		}

		private void BtnServiceLevelNew_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				Logging.WriteLogLine( "Adding Service Level" );
				Model.IsEditingServiceLevel = true;

				//this.ToggleIsEditing();                
			}

			// Set default colours
			cpBackground.SelectedColor = new SolidColorBrush( Colors.White ).Color;
			cpForeground.SelectedColor = new SolidColorBrush( Colors.Black ).Color;
		}

		private void BtnServiceLevelDel_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbServiceLevels.SelectedIndex;

				if( selectedIndex > -1 )
				{
					var sl = Model.ServiceLevelObjects[ selectedIndex ];

					if( sl != null )
					{
						var title = (string)Application.Current.TryFindResource( "RateWizardTabServiceLevelDeleteTitle" );

						var message = (string)Application.Current.TryFindResource( "RateWizardTabServiceLevelDelete" );
						message = message.Replace( "@1", sl.N );
						var response = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );

						if( response == MessageBoxResult.Yes )
							Model.DeleteServiceLevelLocally( sl );
					}
				}
			}
		}

		private void BtnServiceLevelUp_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbServiceLevels.SelectedIndex;

				if( selectedIndex > 0 )
				{
					var sl = Model.ServiceLevelObjects[ selectedIndex ];

					if( sl != null )
						Model.MoveServiceLevelLocally( sl, selectedIndex - 1 );
				}
			}
		}

		private void BtnServiceLevelDown_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbServiceLevels.SelectedIndex;

				if( selectedIndex < ( lbServiceLevels.Items.Count - 1 ) )
				{
					var sl = Model.ServiceLevelObjects[ selectedIndex ];

					if( sl != null )
						Model.MoveServiceLevelLocally( sl, selectedIndex + 1 );
				}
			}
		}

		private void BtnServiceLevelUpdate_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var errors = string.Empty;
				var exists = false;

				if( Model.SelectedServiceLevelName != string.Empty )
				{
					exists = Model.DoesServiceLevelNameExist( Model.SelectedServiceLevelName );

					// Make sure not editing
					if( exists && ( Model.CurrentlyEditedServiceLevel == null ) )
					{
						// Name can't be a duplicate
						errors = (string)Application.Current.TryFindResource( "RateWizardTabServiceLevelErrorDuplicateName" );
					}
				}
				else
				{
					// Name can't be empty
					errors = (string)Application.Current.TryFindResource( "RateWizardTabServiceLevelErrorEmptyName" );
				}

				if( errors.Length == 0 )
				{
					/// <summary>
					/// Adds, updates, deletes service levels.
					/// OldName == "", NewName == "Value"		    Is an Add
					/// OldName == "Value", NewName == "NewValue"	Is a Rename
					/// OldName == NewName				            Is an Update
					/// OldName == "Value", NewName == ""		    Is a Delete

					/// </summary>
					var (sl, oldName) = PopulateServiceLevel( Model.CurrentlyEditedServiceLevel );

					if( !exists && ( Model.CurrentlyEditedServiceLevel == null ) )
					{
						// Adding
						sl.OldName = string.Empty;
					}
					else if( !exists && ( Model.CurrentlyEditedServiceLevel != null ) )
					{
						// Rename
						sl.OldName = oldName;
					}
					else if( exists )
					{
						// Update
						sl.OldName = sl.NewName;
					}

					Model.AddUpdateServiceLevel( sl );

					//Model.ReloadServiceLevels();

					Model.CurrentlyEditedServiceLevel = null;
					Model.IsEditingServiceLevel       = false;

					//this.ToggleIsEditing();
					BtnServiceLevelClear_Click( null, null );
				}
				else
				{
					Logging.WriteLogLine( "Error found: " + errors );
					var title = (string)Application.Current.TryFindResource( "RateWizardTabServiceLevelErrorTitle" );

					MessageBox.Show( errors, title, MessageBoxButton.OK, MessageBoxImage.Error );
				}
			}
		}

		private void BtnServiceLevelCancel_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Cancelling Service Level change" );

			if( DataContext is RateWizardModel Model )
			{
				Model.IsEditingServiceLevel       = false;
				Model.CurrentlyEditedServiceLevel = null;

				//this.ToggleIsEditing();
				BtnServiceLevelClear_Click( null, null );
			}
		}

		private void BtnServiceLevelClear_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Clearing Service Level" );

			if( DataContext is RateWizardModel Model )
			{
				Model.SelectedServiceLevelName      = string.Empty;
				Model.SelectedServiceLevelStartTime = string.Empty;
				Model.SelectedServiceLevelEndTime   = string.Empty;
				Model.SelectedServiceLevelSun       = false;
				Model.SelectedServiceLevelMon       = false;
				Model.SelectedServiceLevelTue       = false;
				Model.SelectedServiceLevelWed       = false;
				Model.SelectedServiceLevelThu       = false;
				Model.SelectedServiceLevelFri       = false;
				Model.SelectedServiceLevelSat       = false;

				Model.SelectedServiceLevelDeliveryHours = string.Empty;
				Model.SelectedServiceLevelWarn          = string.Empty;
				Model.SelectedServiceLevelCritical      = string.Empty;
				Model.SelectedServiceLevelBeyond        = string.Empty;
			}

			cpBackground.SelectedColor = null;
			cpForeground.SelectedColor = null;
		}

		private void BtnServiceLevelSave_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Saving Service Levels" );

			if( DataContext is RateWizardModel Model )
				Model.SaveServiceLevels();
		}

		private void LbServiceLevels_MouseDoubleClick( object sender, MouseButtonEventArgs e )
		{
			BtnServiceLevelEdit_Click( null, null );
		}

	#endregion

	#region Package Type Handlers

		private PackageType PopulatePackageType( PackageType pt )
		{
			PackageType newPt = null;

			if( DataContext is RateWizardModel Model )
			{
				if( pt != null )
					newPt = pt;
				else
				{
					newPt = new PackageType
					        {
						        // Put new ones at the bottom of the list
						        SortOrder = (ushort)lbPackageTypes.Items.Count
					        };
				}

				newPt.Description = Model.SelectedPackageTypeName;
			}

			return newPt;
		}

		private void BtnPackageTypeUpdate_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var errors = string.Empty;
				var exists = false;

				if( Model.SelectedPackageTypeName != string.Empty )
				{
					exists = Model.DoesPackageTypeNameExist( Model.SelectedPackageTypeName );

					// Make sure not editing
					if( exists && ( Model.CurrentlyEditedPackageType == null ) )
					{
						// Name can't be a duplicate
						errors = (string)Application.Current.TryFindResource( "RateWizardTabPackageTypeErrorDuplicateName" );
					}
				}
				else
				{
					// Name can't be empty
					errors = (string)Application.Current.TryFindResource( "RateWizardTabPackageTypeErrorEmptyName" );
				}

				if( errors.Length == 0 )
				{
					/// </summary>
					var pt = PopulatePackageType( Model.CurrentlyEditedPackageType );

					Model.AddUpdatePackageType( pt );

					//Model.ReloadServiceLevels();

					Model.CurrentlyEditedPackageType = null;
					Model.IsEditingPackageType       = false;

					//this.ToggleIsEditing();
					BtnPackageTypeClear_Click( null, null );
				}
				else
				{
					Logging.WriteLogLine( "Error found: " + errors );
					var title = (string)Application.Current.TryFindResource( "RateWizardTabPackageTypeErrorTitle" );

					MessageBox.Show( errors, title, MessageBoxButton.OK, MessageBoxImage.Error );
				}
			}
		}

		private void BtnPackageTypeCancel_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Cancelling Package Type change" );

			if( DataContext is RateWizardModel Model )
			{
				Model.IsEditingPackageType       = false;
				Model.CurrentlyEditedPackageType = null;

				//this.ToggleIsEditing();
				BtnPackageTypeClear_Click( null, null );
			}
		}

		private void BtnPackageTypeClear_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Clearing Package Type" );

			if( DataContext is RateWizardModel Model )
				Model.SelectedPackageTypeName = string.Empty;
		}

		private void BtnPackageTypeEdit_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbPackageTypes.SelectedIndex;

				if( selectedIndex > -1 )
				{
					BtnPackageTypeClear_Click( null, null );
					Model.IsEditingPackageType = true;

					var pts = Model.PackageTypeObjects;

					var pt = pts[ selectedIndex ];

					if( pt != null )
					{
						Logging.WriteLogLine( "Editing Package Type: " + pt.Description );
						Model.CurrentlyEditedPackageType = pt;

						Model.SelectedPackageTypeName = pt.Description;

						// Model.SelectedServiceLevelStartTime = sl.StartTime.ToString();
						// Model.SelectedServiceLevelEndTime = sl.EndTime.ToString();
					}
				}
			}
		}

		private void BtnPackageTypeNew_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				Logging.WriteLogLine( "Adding PackageType" );
				Model.IsEditingPackageType = true;
			}
		}

		private void BtnPackageTypeDel_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbPackageTypes.SelectedIndex;

				if( selectedIndex > -1 )
				{
					var pt = Model.PackageTypeObjects[ selectedIndex ];

					if( pt != null )
					{
						var title = (string)Application.Current.TryFindResource( "RateWizardTabPackageTypeDeleteTitle" );

						var message = (string)Application.Current.TryFindResource( "RateWizardTabPackageTypeDelete" );
						message = message.Replace( "@1", pt.Description );
						var response = MessageBox.Show( message, title, MessageBoxButton.YesNo, MessageBoxImage.Question );

						if( response == MessageBoxResult.Yes )
							Model.DeletePackageTypeLocally( pt );
					}
				}
			}
		}

		private void BtnPackageTypeUp_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbPackageTypes.SelectedIndex;

				if( selectedIndex > 0 )
				{
					var pt = Model.PackageTypeObjects[ selectedIndex ];

					if( pt != null )
						Model.MovePackageTypeLocally( pt, selectedIndex - 1 );
				}
			}
		}

		private void BtnPackageTypeDown_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbPackageTypes.SelectedIndex;

				if( selectedIndex < ( lbPackageTypes.Items.Count - 1 ) )
				{
					var pt = Model.PackageTypeObjects[ selectedIndex ];

					if( pt != null )
						Model.MovePackageTypeLocally( pt, selectedIndex + 1 );
				}
			}
		}

		private void LbPackageTypes_MouseDoubleClick( object sender, MouseButtonEventArgs e )
		{
			BtnPackageTypeEdit_Click( null, null );
		}

		private void BtnPackageTypeSave_Click( object sender, RoutedEventArgs e )
		{
			Logging.WriteLogLine( "Saving Package Types" );

			if( DataContext is RateWizardModel Model )
				Model.SavePackageTypes();
		}

	#endregion

	#region Zone Handlers

		private void BtnZoneEdit_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbZones.SelectedIndex;

				if( selectedIndex > -1 )
				{
					//this.BtnPackageTypeClear_Click(null, null);
					Model.SelectedZoneName         = string.Empty;
					Model.SelectedZoneAbbreviation = string.Empty;
					Model.IsEditingZone            = true;

					var zones = Model.Zones;

					var zone = zones[ selectedIndex ];

					if( zone != null )
					{
						Logging.WriteLogLine( "Editing Zone: " + zone.Name );
						Model.CurrentlyEditedZone = zone;

						Model.SelectedZoneName         = zone.Name;
						Model.SelectedZoneAbbreviation = zone.Abbreviation;
						Model.SelectedZoneSortIndex    = zone.SortIndex;

						Model.IsZoneTextBoxReadOnly = false;

						tbZoneName.Focus();
						ToggleZoneListButtons( false );
					}
				}
			}
		}

		private void BtnZoneNew_Click( object sender, RoutedEventArgs e )
		{
			BtnZoneClear_Click( null, null );
			ToggleZoneListButtons( false );
			tbZoneName.Focus();

			if( DataContext is RateWizardModel Model )
			{
				Model.IsNewZone             = true;
				Model.IsZoneTextBoxReadOnly = false;
				Model.IsEditingZone         = true;
			}
		}

		private void BtnZoneDel_Click( object sender, RoutedEventArgs e )
		{
			ToggleZoneListButtons( true );

			if( ( lbZones.SelectedItem != null ) && DataContext is RateWizardModel Model )
				Model.DeleteZoneLocally( (Zone)lbZones.SelectedItem );
		}

		private void BtnZoneUp_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbZones.SelectedIndex;

				if( selectedIndex > 0 )
				{
					var zone = Model.Zones[ selectedIndex ];

					if( zone != null )
						Model.MoveZoneLocally( zone, selectedIndex - 1 );
				}
			}
		}

		private void BtnZoneDown_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var selectedIndex = lbZones.SelectedIndex;

				if( selectedIndex < ( lbZones.Items.Count - 1 ) )
				{
					var zone = Model.Zones[ selectedIndex ];

					if( zone != null )
						Model.MoveZoneLocally( zone, selectedIndex + 1 );
				}
			}
		}

		private void LbZones_MouseDoubleClick( object sender, MouseButtonEventArgs e )
		{
			BtnZoneEdit_Click( null, null );
		}

		private void BtnZoneUpdate_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				var error = Model.AddUpdateZoneLocally();

				if( error.IsNullOrWhiteSpace() )
				{
					//ToggleZoneListButtons(true);
					BtnZoneCancel_Click( null, null );
				}
				else
				{
					tbZoneName.Focus();
					var title = (string)Application.Current.TryFindResource( "RateWizardTabZoneErrorTitle" );
					MessageBox.Show( error, title, MessageBoxButton.OK, MessageBoxImage.Error );
				}
			}
		}

		private void BtnZoneCancel_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				Model.CurrentlyEditedZone = null;

				Model.SelectedZoneName         = string.Empty;
				Model.SelectedZoneAbbreviation = string.Empty;
				Model.IsEditingZone            = false;
				Model.IsNewZone                = false;

				Model.IsZoneTextBoxReadOnly = true;
				ToggleZoneListButtons( true );
			}
		}

		private void BtnZoneClear_Click( object sender, RoutedEventArgs e )
		{
			if( DataContext is RateWizardModel Model )
			{
				Model.CurrentlyEditedZone = null;

				Model.SelectedZoneName         = string.Empty;
				Model.SelectedZoneAbbreviation = string.Empty;

				//Model.IsEditingZone = false;
			}
		}

		//private void BtnZonesSave_Click(object sender, RoutedEventArgs e)
		//{

		//}

		private void ToggleZoneListButtons( bool isEnabled )
		{
			btnZoneEdit.IsEnabled = isEnabled;
			btnZoneNew.IsEnabled  = isEnabled;
			btnZoneDel.IsEnabled  = isEnabled;
			btnZoneUp.IsEnabled   = isEnabled;
			btnZoneDown.IsEnabled = isEnabled;
		}

	#endregion
	}
}