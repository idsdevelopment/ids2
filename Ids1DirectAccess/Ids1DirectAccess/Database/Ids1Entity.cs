﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;

namespace Ids1DirectAccess.Database
{
	public partial class Ids1Entities : DbContext
	{
		private readonly EFCommandInterceptor Interceptor;

		public EFCommandInterceptor.OPERATION Modify
		{
			get => Interceptor.Modify;
			set => Interceptor.Modify = value;
		}

		public Ids1Entities( DbConnection connection ) : base( connection, false )
		{
			DbInterception.Add( Interceptor = new EFCommandInterceptor() );
		}
	}
}