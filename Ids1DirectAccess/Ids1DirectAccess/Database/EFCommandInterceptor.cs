﻿using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;

namespace Ids1DirectAccess.Database
{
	public class EFCommandInterceptor : IDbCommandInterceptor
	{
		public enum OPERATION
		{
			NONE,
			USE_INDEX_CS13
		}


		public OPERATION Modify;


		public void NonQueryExecuting( DbCommand command, DbCommandInterceptionContext<int> interceptionContext )
		{
		}

		public void NonQueryExecuted( DbCommand command, DbCommandInterceptionContext<int> interceptionContext )
		{
		}

		public void ReaderExecuting( DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext )
		{
			switch( Modify )
			{
                case OPERATION.USE_INDEX_CS13:
	                var Text = command.CommandText;
	                var P = Text.IndexOf( "FROM `trips` AS", StringComparison.Ordinal );
	                if( P >= 0 )
	                {
		                P = Text.IndexOf( "WHERE", P, StringComparison.Ordinal );
		               command.CommandText = Text.Insert( P, "USE INDEX (CS13) " );
	                }
	                break;
			}
		}

		public void ReaderExecuted( DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext )
		{
		}

		public void ScalarExecuting( DbCommand command, DbCommandInterceptionContext<object> interceptionContext )
		{
		}

		public void ScalarExecuted( DbCommand command, DbCommandInterceptionContext<object> interceptionContext )
		{
		}
	}
}