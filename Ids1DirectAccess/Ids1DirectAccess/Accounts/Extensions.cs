﻿using System.Collections.Generic;
using System.Linq;
using Ids1DirectAccess.Database;

namespace Ids1DirectAccess.Accounts
{
	public static class Extensions
	{
		public class AccountDetailed
		{
			public account Account;

			public address Address,
			               BillingAddress;
		}


		public static List<AccountDetailed> GetAccountsDetailedByLastUpdated( this Ids1.Connection connection, string resellerId, long lastUpdated, int take )
		{
			using( var E = connection.Entity )
			{
				var Accounts = ( from A in E.accounts
				                 where ( A.mainId == resellerId ) && ( A.lastUpdated > lastUpdated ) && !(bool)A.isArchived && A.isEnabled
				                 let Addr = ( from Adr in E.addresses
				                              where Adr.internalId == A.address_internalId
				                              select Adr ).FirstOrDefault()
				                 let BAddr = ( from Adr in E.addresses
				                               where Adr.internalId == A.billingAddress_internalId
				                               select Adr ).FirstOrDefault()
				                 orderby A.lastUpdated
				                 select new { A, Addr, BAddr } ).Take( take ).ToList();

				return ( from A in Accounts
				         orderby A.A.lastUpdated
				         select new AccountDetailed
				                {
					                Account = A.A,
					                Address = A.Addr,
					                BillingAddress = A.BAddr
				                } ).ToList();
			}
		}
	}
}