﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Ids1DirectAccess.Database;

// ReSharper disable InconsistentNaming

namespace Ids1DirectAccess.Trips
{
	public static class Extensions
	{
		public class TripDetailed
		{
			public List<trip_charge> Charges;
			public address PickupAddress, DeliveryAddress;
			public trip Trip;
		}

		public const string PRIORITY = "priority",
		                    PRIORITY_STATUS = "TE_PRIORITY_STATUS";

		// Service Levels
		private const string ROUTE = "ROUTE",
		                     LINE_HAUL = "LINE-HAUL",
		                     STAT = "STAT",
		                     MULTI_STAT = "MULTI STAT",
		                     SWEEP = "SWEEP",
		                     PICKUP = "PICKUP";

		public static List<trip> GetTripsByPickupDateAndStatus( this Ids1.Connection connection, string accountId, DateTime startDateTime, DateTime endDateTime, Ids1.Trips.TRIP_STATUS fromStatus, Ids1.Trips.TRIP_STATUS toStatus )
		{
			using( var E = connection.Entity )
			{
				return ( from T in E.trips
				         where ( T.mainId == accountId ) && ( T.pickupTime >= startDateTime ) && ( T.pickupTime <= endDateTime ) &&
				               ( T.status >= (int)fromStatus ) && ( T.status <= (int)toStatus )
				         select T ).ToList();
			}
		}

		public static List<trip> GetTripsByDeliveryDateAndStatus( this Ids1.Connection connection, string accountId, DateTime startDateTime, DateTime endDateTime, Ids1.Trips.TRIP_STATUS fromStatus, Ids1.Trips.TRIP_STATUS toStatus )
		{
			using( var E = connection.Entity )
			{
				return ( from T in E.trips
				         where ( T.mainId == accountId ) && ( T.deliveredTime >= startDateTime ) &&
				               ( T.deliveredTime <= endDateTime ) && ( T.status >= (int)fromStatus ) &&
				               ( T.status <= (int)toStatus )
				         select T ).ToList();
			}
		}

		[MethodImpl( MethodImplOptions.NoOptimization )]
		public static List<trip> GetPriorityTripsByPickupDateAndStatus( this Ids1.Connection connection, DateTime startDateTime, DateTime endDateTime )
		{
			// ReSharper disable once ConvertToConstant.Local
			var Priority = PRIORITY; // !!!!!! Don't make it a constant. Query will run 10 times slower. ?????  !!!!!!!
			using( var E = connection.Entity )
			{
				var Trips = ( from T in E.trips
				              where ( T.mainId == Priority ) && ( T.pickupTime >= startDateTime ) && ( T.pickupTime <= endDateTime ) &&
				                    ( T.status >= (int)Ids1.Trips.TRIP_STATUS.VERIFIED ) && ( T.status <= (int)Ids1.Trips.TRIP_STATUS.POSTED )
				                    && ( ( T.serviceLevel == ROUTE ) || ( T.serviceLevel == LINE_HAUL ) || ( T.serviceLevel == STAT ) || ( T.serviceLevel == MULTI_STAT ) || ( T.serviceLevel == SWEEP ) || ( T.serviceLevel == PICKUP ) )
/*				              join Ex in E.trip_extensions
					              on new { resellerId = T.mainId, T.accountId, tripId = T.subId, valueId = PRIORITY_STATUS } equals
					              new { Ex.resellerId, Ex.accountId, Ex.tripId, Ex.valueId } into PriorityStatus
				              from S in PriorityStatus
				              where S.valueLong == (int)Ids1.Trips.TRIP_STATUS.VERIFIED
*/
				              select T
				            ).ToList();

				return Trips;
			}
		}


		public static List<TripDetailed> GetTripsDetailedByLastUpdated( this Ids1.Connection connection, string resellerId, long lastUpdated, int take )
		{
			var Result = new List<TripDetailed>();
			using( var E = connection.Entity )
			{
				//E.Database.Log = Console.Write;

				E.Modify = EFCommandInterceptor.OPERATION.USE_INDEX_CS13;

				var Trips = ( from T in E.trips
				              where T.lastUpdated >= lastUpdated
				              select T ).Take( take ).ToList();

				E.Modify = EFCommandInterceptor.OPERATION.NONE;

				foreach( var Trip in Trips )
				{
					var T = new TripDetailed
					        {
						        Trip = Trip,
						        Charges = ( from C in E.trip_charge
						                    where ( C.accountId == Trip.accountId ) && ( C.resellerId == resellerId ) && ( C.tripId == Trip.subId )
						                    select C ).ToList(),
						        DeliveryAddress = ( from A in E.addresses
						                            where A.internalId == Trip.deliveryAddress_internalId
						                            select A ).FirstOrDefault(),
						        PickupAddress = ( from A in E.addresses
						                          where A.internalId == Trip.pickupAddress_internalId
						                          select A ).FirstOrDefault()
					        };
					Result.Add( T );
				}
			}

			return Result;
		}


		public static List<TripDetailed> GetTripsDetailedByDateCreatedAndLastUpdated( this Ids1.Connection connection, string resellerId, DateTime fromDate, long lastUpdated, int take )
		{
			var Result = new List<TripDetailed>();
			using( var E = connection.Entity )
			{
				//E.Database.Log = Console.Write;
				var Trips = ( from T in E.trips
				              where ( T.mainId == resellerId ) && ( T.lastUpdated > lastUpdated ) && ( T.callTime >= fromDate )
				              orderby T.lastUpdated
				              select T ).Take( take ).ToList();

				foreach( var Trip in Trips )
				{
					var T = new TripDetailed
					        {
						        Trip = Trip,
						        Charges = ( from C in E.trip_charge
						                    where ( C.accountId == Trip.accountId ) && ( C.resellerId == resellerId ) && ( C.tripId == Trip.subId )
						                    select C ).ToList(),
						        DeliveryAddress = ( from A in E.addresses
						                            where A.internalId == Trip.deliveryAddress_internalId
						                            select A ).FirstOrDefault(),
						        PickupAddress = ( from A in E.addresses
						                          where A.internalId == Trip.pickupAddress_internalId
						                          select A ).FirstOrDefault()
					        };
					Result.Add( T );
				}
			}

			return Result;
		}
	}
}