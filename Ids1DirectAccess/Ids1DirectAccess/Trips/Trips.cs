﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Ids1DirectAccess.Database;

namespace Ids1DirectAccess
{
	public partial class Ids1
	{
		public class Trips
		{
			public enum TRIP_STATUS
			{
				UNSET,
				NEW,
				ACTIVE,
				DISPATCHED,
				PICKED_UP,
				DELIVERED,
				VERIFIED,
				POSTED,
				DELETED,
				SCHEDULED,
				UNDELIVERABLE = -3
			}
		}
	}
}
