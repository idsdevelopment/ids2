﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Metadata.Edm;
using System.Diagnostics;
using System.Reflection;
using Ids1DirectAccess.Ssh;
using MySql.Data.MySqlClient;

namespace Ids1DirectAccess.MySql
{
	public class MySqlSsh : IDisposable
	{
		private readonly SshConnection Connection;
		public readonly MySqlConnection MySqlConnection;

		private EntityConnection _EntityConnection;

		public MySqlSsh( string host, int sshPort, string remoteSqlHost, int remoteMySqlPort, int localMySqlPort, string userName, string sshPassword, string database, string databasePassword = "", int connectionTimeoutInSeconds = 30 )
		{
			try
			{
				Connection = new SshConnection( host, sshPort, remoteSqlHost, remoteMySqlPort, localMySqlPort, userName, sshPassword, connectionTimeoutInSeconds );
				if( Connection.Connected )
				{
					var ConnectionString = $"SERVER={Connection.BoundHost};PORT={Connection.BoundPort};DATABASE={database};UID={userName};SslMode=none";
					if( !string.IsNullOrWhiteSpace( databasePassword ) )
						ConnectionString = $"{ConnectionString};PASSWORD={databasePassword};";

					MySqlConnection = new MySqlConnection( ConnectionString );
					MySqlConnection.Open();
					Connected = true;
				}
			}
			catch( Exception Exception )
			{
				Debug.WriteLine( Exception );
			}
		}

		public EntityConnection EntityConnection
		{
			get
			{
				if( _EntityConnection == null )
				{
					var Workspace = new MetadataWorkspace( new[] { "res://*/" }, new[] { Assembly.GetExecutingAssembly() } );

					// check and be sure the metadata loaded
					Debug.Assert( Workspace.GetItemCollection( DataSpace.SSpace ).GetItems<EntityType>().Count != 0 );

					_EntityConnection = new EntityConnection( Workspace, MySqlConnection );
				}

				return _EntityConnection;
			}
		}

		public bool Connected { get; }

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		// ReSharper disable once MemberCanBeMadeStatic.Local
		private void ReleaseUnmanagedResources()
		{
			// TODO release unmanaged resources here
		}

		protected virtual void Dispose( bool disposing )
		{
			ReleaseUnmanagedResources();
			if( disposing )
			{
				_EntityConnection?.Dispose();
				MySqlConnection?.Dispose();
				Connection?.Dispose();
			}
		}

		~MySqlSsh()
		{
			Dispose( false );
		}
	}
}