﻿using System.Collections.Generic;
using System.Linq;
using Ids1DirectAccess.Database;

namespace Ids1DirectAccess.Charges
{
	public static class Extensions
	{
		public static List<charge> GetCharges( this Ids1.Connection connection, string resellerId )
		{
			using( var E = connection.Entity )
			{
				var Result = ( from C in E.charges
				               where C.resellerId == resellerId
				               select C ).ToList();
				return Result;
			}
		}
	}
}