﻿using System.Collections.Generic;
using System.Linq;
using Ids1DirectAccess.Database;

namespace Ids1DirectAccess.Addresses
{
	public static class Extensions
	{
		public static List<common_addresses> GetAddresses( this Ids1.Connection connection, string resellerId, string accountId, long lastUpdated )
		{
			using( var E = connection.Entity )
			{
				var Result = ( from A in E.common_addresses
				               where ( A.resellerId == resellerId ) && ( A.accountId == accountId ) && ( A.lastUpdated > lastUpdated ) && ( A.description != "" )
				               orderby A.lastUpdated
				               select A ).ToList();
				return Result;
			}
		}

		public static List<common_addresses> GetAddresses( this Ids1.Connection connection, string resellerId, long lastUpdated, int take )
		{
			using( var E = connection.Entity )
			{
				var Result = ( from A in E.common_addresses
				               where ( A.resellerId == resellerId ) && ( A.lastUpdated > lastUpdated ) && ( A.description != "" )
				               orderby A.lastUpdated
				               select A ).Take( take ).ToList();
				return Result;
			}
		}
	}
}