﻿using System;
using System.Net;
using Ids1DirectAccess.Database;
using Ids1DirectAccess.MySql;

namespace Ids1DirectAccess
{
	public partial class Ids1
	{
		public enum SERVERS
		{
			FREE_BSD
		}

		public class EndPoint
		{
			public const int DEFAULT_COMMAND_TIMEOUT = 30 * 60,
			                 SSH_PORT = 22,
			                 REMOTE_MYSQL_PORT = 3306,
			                 LOCAL_MYSQL_PORT = 14324;

			public const string PASSWORD = "11dsadm!N",
			                    DEFAULT_USER_NAME = "root",
			                    DEFAULT_DATABASE = "ids";

			public int CommandTimeoutInSeconds = DEFAULT_COMMAND_TIMEOUT;
			public string Database = DEFAULT_DATABASE;

			public int LocalMySqlPort = LOCAL_MYSQL_PORT;
			public string Password = PASSWORD;
			public string RemoteHost;
			public int RemoteMySqlPort = REMOTE_MYSQL_PORT;

			public string SshHost;
			public int SshPort = SSH_PORT;
			public string UserName = DEFAULT_USER_NAME;
		}

		public class Connection : IDisposable
		{
			internal EndPoint EndPoint;
			internal MySqlSsh MySqlConnection;

			public void Dispose()
			{
				Dispose( true );
				GC.SuppressFinalize( this );
			}

			public bool Connected => ( MySqlConnection != null ) && MySqlConnection.Connected;

			public Ids1Entities Entity
			{
				get
				{
					var Temp = new Ids1Entities( MySqlConnection.EntityConnection );
					Temp.Database.CommandTimeout = EndPoint.CommandTimeoutInSeconds;
					return Temp;
				}
			}

			private void ReleaseUnmanagedResources()
			{
				if( MySqlConnection != null )
				{
					MySqlConnection.Dispose();
					MySqlConnection = null;
				}
			}

			protected virtual void Dispose( bool disposing )
			{
				ReleaseUnmanagedResources();
			}

			~Connection()
			{
				Dispose( false );
			}
		}

		public static EndPoint[] EndPoints =
		{
			new EndPoint
			{
				SshHost = "defaultfreebsd11.internetdispatcher.org",
				RemoteHost = "149.56.245.200"
			}
		};

		public static Connection Connect( SERVERS server = SERVERS.FREE_BSD )
		{
			ServicePointManager.DefaultConnectionLimit = 64;

			var Ep = EndPoints[ (int)server ];

			return new Connection
			       {
				       EndPoint = Ep,
				       MySqlConnection = new MySqlSsh( Ep.SshHost, Ep.SshPort, Ep.RemoteHost, Ep.RemoteMySqlPort, Ep.LocalMySqlPort, Ep.UserName, Ep.Password, Ep.Database )
			       };
		}
	}
}