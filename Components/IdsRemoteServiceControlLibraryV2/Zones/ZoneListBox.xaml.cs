﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using IdsRemoteServiceControlLibraryV2.Zones;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2
{
	public partial class ZoneListBox : UserControl
	{
		public delegate void OnSelectionChangedEvent( object sender, List<Zone> selectedItems, bool @checked );

		public ZoneListBox()
		{
			InitializeComponent();
		}

		public event OnSelectionChangedEvent SelectionChanged;

		public List<Zone> SelectedZones
		{
			get
			{
				if( DataContext is ZonesListBoxModel Ctx )
				{
					return ( from Z in Ctx.Items
					         where Z.Checked
					         select Z as Zone ).ToList();
				}
				return new List<Zone>();
			}
		}

		private void CheckBox_Checked( object sender, RoutedEventArgs e )
		{
			SelectionChanged?.Invoke( this, SelectedZones, true );
		}

		private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
		{
			SelectionChanged?.Invoke( this, SelectedZones, false );
		}
	}
}