﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Zones
{
	public class ZoneEntry : Zone
	{
		public bool Checked { get; set; }
	}


	public class ZonesListBoxModel : ViewModelBase
	{
		private readonly object LockObject = new object();

		public List<ZoneEntry> Items
		{
			get
			{
				lock( LockObject )
					return Get( () => Items, new List<ZoneEntry>() );
			}
			set
			{
				lock( LockObject )
					Set( () => Items, value );
			}
		}


		public ZonesListBoxModel()
		{
			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          var Zones = await Azure.Client.RequestZones();
					          if( Zones != null )
					          {
						          var Rls = ( from Z in Zones
						                      select new ZoneEntry
						                             {
							                             Name = Z.Name
						                             } ).ToList();
						          Items = Rls;
					          }
				          } );
			}
		}
	}
}