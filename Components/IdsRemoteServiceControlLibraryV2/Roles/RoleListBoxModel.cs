﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AzureRemoteService;
using IdsControlLibraryV2.ViewModel;
using IdsRemoteServiceControlLibraryV2.Annotations;
using Utils;

namespace IdsRemoteServiceControlLibraryV2.Roles
{
	public class RoleEntry : Protocol.Data.Role, INotifyPropertyChanged
	{
		private readonly RoleListBoxModel Owner;
		private bool _Checked;

		public bool Checked
		{
			get => _Checked;
			set
			{
				_Checked = value;
				Owner.DoRoleChanged( this );
				OnPropertyChanged();
			}
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
		{
			PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
		}

		internal RoleEntry( RoleListBoxModel owner )
		{
			Owner = owner;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}

	public class RoleListBoxModel : ViewModelBase
	{
		private readonly object LockObject = new object();


		private async void GetRoles( Action callback = null )
		{
			var Roles = await Azure.Client.RequestStaffMemberRoles( StaffId );
			Dispatcher.Invoke( () =>
			                   {
				                   var Itms = Items;
				                   foreach( var RoleEntry in Itms )
				                   {
					                   var Checked = false;

					                   foreach( var Role in Roles )
					                   {
						                   if( Role.Name == RoleEntry.Name )
						                   {
							                   Checked = true;
							                   break;
						                   }
					                   }

					                   RoleEntry.Checked = Checked;
				                   }

				                   callback?.Invoke();
			                   } );
		}

		public delegate void OnRoleChangedEvent( string roleName, bool selected );

		internal void DoRoleChanged( RoleEntry role )
		{
			if( !IgnoreEvents && RoleListBox.IsNotNull() )
			{
				RoleListBox.ChangedRole = new Role
				                          {
					                          Name = role.Name,
					                          Selected = role.Checked
				                          };

				OnRoleChanged?.Invoke( role.Name, role.Checked );
			}
		}

		public ObservableCollection<RoleEntry> Items
		{
			get
			{
				lock( LockObject )
					return Get( () => Items, new ObservableCollection<RoleEntry>() );
			}
			private set
			{
				lock( LockObject )
					Set( () => Items, value );
			}
		}


		public string StaffId
		{
			get { return Get( () => StaffId, "" ); }
			set { Set( () => StaffId, value ); }
		}

		[DependsUpon250( nameof( StaffId ) )]
		public void WhenStaffIdChanges()
		{
			if( !IsInDesignMode )
			{
				Task.Run( () =>
				          {
					          GetRoles();
				          } );
			}
		}

		public void Refresh()
		{
			if( !IsInDesignMode )
			{
				IgnoreEvents = true;
				Task.Run( () =>
				          {
					          GetRoles( () =>
					                    {
						                    IgnoreEvents = false;
					                    } );
				          } );
			}
		}

		internal bool IgnoreEvents;

		public OnRoleChangedEvent OnRoleChanged;

		internal RoleListBox RoleListBox;

		public RoleListBoxModel()
		{
			if( !IsInDesignMode )
			{
				Task.Run( async () =>
				          {
					          IgnoreEvents = true;
					          try
					          {
						          var Roles = await Azure.Client.RequestRoles();

						          if( Roles != null )
						          {
							          var Rls = new ObservableCollection<RoleEntry>();

							          foreach( var Role in ( from R in Roles
							                                 orderby R.Name
							                                 select R ) )
							          {
								          Rls.Add( new RoleEntry( this )
								                   {
									                   Mandatory = Role.Mandatory,
									                   Name = Role.Name
								                   } );
							          }

							          Items = Rls;
						          }
					          }
					          finally
					          {
						          IgnoreEvents = false;
					          }
				          } );
			}
		}
	}
}