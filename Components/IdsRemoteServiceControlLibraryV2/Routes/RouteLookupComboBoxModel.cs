﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using AzureRemoteService;
using IdsControlLibraryV2.Combos;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Routes
{
	public class ObjectToRouteScheduleConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return value is RouteSchedule Summary ? Summary : null;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}


	public class LookupItem : LookupComboBoxModelBase<RouteSchedule, string>.LookupItem
	{
		public LookupItem( RouteSchedule item ) : base( item )
		{
		}
	}

	public class RouteLookupComboBoxModel : LookupComboBoxModelBase<RouteSchedule, string>
	{
		public string CustomerCode
		{
			get { return Get( () => CustomerCode, "" ); }
			set { Set( () => CustomerCode, value ); }
		}

		[DependsUpon( nameof( CustomerCode ) )]
		public void WhenCustomerCodeChanges()
		{
			Items = new ObservableCollection<LookupItem>();

			if( SearchText == "" )
				WhenSearchTextChanges();
			else
				SearchText = "";
		}

		public override IList<RouteSchedule> First( uint count )
		{
			return Azure.Client.RequestRouteLookup( new RouteLookup
			                                        {
				                                        CustomerCode = CustomerCode,
				                                        PreFetchCount = (int)PreFetchCount,
				                                        Fetch = PreFetch.PREFETCH.FIRST
			                                        } ).Result;
		}

		public override IList<RouteSchedule> First( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<RouteSchedule> Last( uint count )
		{
			return Azure.Client.RequestRouteLookup( new RouteLookup
			                                        {
				                                        CustomerCode = CustomerCode,
				                                        PreFetchCount = (int)PreFetchCount,
				                                        Fetch = PreFetch.PREFETCH.LAST
			                                        } ).Result;
		}

		public override IList<RouteSchedule> Last( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<RouteSchedule> FindRange( string key, int count )
		{
			return Azure.Client.RequestRouteLookup( new RouteLookup
			                                        {
				                                        CustomerCode = CustomerCode,
				                                        PreFetchCount = count,
				                                        Fetch = PreFetch.PREFETCH.FIND_RANGE,
				                                        Key = key
			                                        } ).Result;
		}

		public override IList<RouteSchedule> FindMatch( string key, int count )
		{
			return Azure.Client.RequestRouteLookup( new RouteLookup
			                                        {
				                                        CustomerCode = CustomerCode,
				                                        PreFetchCount = count,
				                                        Fetch = PreFetch.PREFETCH.FIND_MATCH,
				                                        Key = key
			                                        } ).Result;
		}

		public override int CompareSearchKey( string searchKey, string k2 )
		{
			return string.Compare( searchKey, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override int Compare( string k1, string k2 )
		{
			return string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool StartsWith( string searchValue, string k2 )
		{
			return k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool Filter( string filter, RouteSchedule value )
		{
			throw new NotImplementedException();
		}

		public override string Key( RouteSchedule item )
		{
			return item.RouteName;
		}

		public override string Key( string searchText )
		{
			return searchText;
		}

		public override bool IsEmpty( string key )
		{
			return string.IsNullOrEmpty( key );
		}

		public override string AsString( string key )
		{
			return key;
		}
	}
}