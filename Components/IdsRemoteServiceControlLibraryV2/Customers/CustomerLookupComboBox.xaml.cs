﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using IdsControlLibraryV2.Combos;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Customers
{
	public class CustomerComboBoolToEvenOddSelectedBackgroundConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( parameter is CustomerLookupComboBox Combo && value is LookupItemBase Item )
			{
				if( Item.Selected )
					return Combo.SelectedItemBackground;
				return Item.IsEvenRow ? Combo.EvenRowBackground : Combo.OddRowBackground;
			}

			return Brushes.Red;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}

	/// <summary>
	///     Interaction logic for CustomerLookupComboBox.xaml
	/// </summary>
	public partial class CustomerLookupComboBox : UserControl
	{
		public bool DesignMode => DesignerProperties.GetIsInDesignMode( this );

		private CustomerLookupComboBoxModel Context;

		public CustomerLookupComboBox()
		{
			InitializeComponent();

			Loaded += ( sender, args ) =>
			          {
				          if( Combo.DataContext is CustomerLookupComboBoxModel Ctx )
				          {
					          Context = Ctx;
					          Ctx.OnCustomerCodeChanges = () => { CustomerCode = Context.CustomerCode; };
				          }
			          };
		}

		private void Combo_SecondaryButtonClick()
		{
			SearchingByAccount = !SearchingByAccount;
		}


		[Category( "Options" )]
		public string CustomerCode
		{
			get => Context is null ? "" : Context.CustomerCode;
			set => SetValue( CustomerCodeProperty, value );
		}

		// Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CustomerCodeProperty =
			DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, CustomerCodePropertyChangedCallback ) );

		private static void CustomerCodePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Lc && e.NewValue is string CustCode )
				Lc.Context.CustomerCode = CustCode;
		}


		[Category( "Brush" )]
		public Brush EvenRowBackground
		{
			get => (Brush)GetValue( EvenRowBackgroundProperty );
			set => SetValue( EvenRowBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty EvenRowBackgroundProperty =
			DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.AliceBlue ) );


		[Category( "Brush" )]
		public Brush OddRowBackground
		{
			get => (Brush)GetValue( OddRowBackgroundProperty );
			set => SetValue( OddRowBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for OddRowBackground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty OddRowBackgroundProperty =
			DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightGoldenrodYellow ) );


		[Category( "Options" )]
		public string SearchByAccountText
		{
			get => (string)GetValue( SearchByAccountTextProperty );
			set => SetValue( SearchByAccountTextProperty, value );
		}

		// Using a DependencyProperty as the backing store for SearchByAccountText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SearchByAccountTextProperty =
			DependencyProperty.Register( nameof( SearchByAccountText ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "A", SearchByAccountTextPropertyChangedCallback ) );


		private static void SearchByAccountTextPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Lc && e.NewValue is string Text )
				Lc.Context.SearchByAccountText = Text;
		}


		[Category( "Options" )]
		public string SearchByCompanyNameText
		{
			get => (string)GetValue( SearchByCompanyNameTextProperty );
			set => SetValue( SearchByCompanyNameTextProperty, value );
		}

		// Using a DependencyProperty as the backing store for SearchByCompanyNameText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SearchByCompanyNameTextProperty =
			DependencyProperty.Register( nameof( SearchByCompanyNameText ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "C", SearchByCompanyNameTextPropertyChangedCallback ) );


		private static void SearchByCompanyNameTextPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Lc && e.NewValue is string Text )
				Lc.Context.SearchByCompanyNameText = Text;
		}


		[Category( "Options" )]
		public bool SearchingByAccount
		{
			get => (bool)GetValue( SearchingByAccountProperty );
			set => SetValue( SearchingByAccountProperty, value );
		}

		// Using a DependencyProperty as the backing store for SearchingByAccount.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SearchingByAccountProperty =
			DependencyProperty.Register( nameof( SearchingByAccount ), typeof( bool ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( true, SearchingByAccountPropertyChangedCallback ) );

		private static void SearchingByAccountPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Lc && e.NewValue is bool ByAccount )
				Lc.Context.SearchingByAccount = ByAccount;
		}


		[Category( "Common" )]
		public CustomerLookupSummary SelectedItem
		{
			get => (CustomerLookupSummary)GetValue( SelectedItemProperty );
			set => SetValue( SelectedItemProperty, value );
		}

		// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedItemProperty =
			DependencyProperty.Register( nameof( SelectedItem ), typeof( CustomerLookupSummary ), typeof( CustomerLookupComboBox ),
			                             new FrameworkPropertyMetadata( default( CustomerLookupSummary ), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, SelectedItemPropertyChangedCallback ) );


		private static void SelectedItemPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Cl && !Cl.DesignMode && e.NewValue is CustomerLookupSummary Summary )
				Cl.Context.SelectItem = Summary;
		}


		[Category( "Brush" )]
		public Brush SelectedItemBackground
		{
			get => (Brush)GetValue( SelectedItemBackgroundProperty );
			set => SetValue( SelectedItemBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for SelectedItemBackground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedItemBackgroundProperty =
			DependencyProperty.Register( nameof( SelectedItemBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightBlue ) );

		private void Combo_ItemSelected( LookupItemBase item )
		{
			if( item.GetItem() is CustomerLookupSummary Item )
			{
				SelectedItem = Item;
				CustomerCode = Item.CustomerCode;
			}
		}
	}
}