﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using AzureRemoteService;
using IdsControlLibraryV2.Combos;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Customers
{
	public class ObjectToCustomerLookupSummaryConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return value is CustomerLookupSummary Summary ? Summary : null;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}


	public class LookupItem : LookupComboBoxModelBase<CustomerLookupSummary, string>.LookupItem
	{
		public LookupItem( CustomerLookupSummary item ) : base( item )
		{
		}
	}

	public class CustomerLookupComboBoxModel : LookupComboBoxModelBase<CustomerLookupSummary, string>
	{
		public string CustomerCode
		{
			get { return Get( () => CustomerCode, "" ); }
			set { Set( () => CustomerCode, value ); }
		}

		[DependsUpon( nameof( CustomerCode ), Delay = 200 )]
		public void WhenCustomerCodeChanges()
		{
			OnCustomerCodeChanges?.Invoke();
		}

		[Setting]
		public bool SearchingByAccount
		{
			get { return Get( () => SearchingByAccount, true ); }
			set { Set( () => SearchingByAccount, value ); }
		}

		public string SearchByCompanyNameText
		{
			get { return Get( () => SearchByCompanyNameText, "C" ); }
			set { Set( () => SearchByCompanyNameText, value ); }
		}

		public string SearchByAccountText
		{
			get { return Get( () => SearchByAccountText, "A" ); }
			set { Set( () => SearchByAccountText, value ); }
		}

		public string SearchButtonText
		{
			get { return Get( () => SearchButtonText ); }
			set { Set( () => SearchButtonText, value ); }
		}

		[DependsUpon( nameof( SearchingByAccount ) )]
		public void WhenSearchingByAccountChanges()
		{
			SearchButtonText = SearchingByAccount ? SearchByAccountText : SearchByCompanyNameText;
			SearchText = "";
		}

		public override IList<CustomerLookupSummary> First( uint count )
		{
			return Azure.Client.RequestCustomerLookup( new CustomerLookup
			                                           {
				                                           PreFetchCount = (int)count,
				                                           Fetch = PreFetch.PREFETCH.FIRST,
				                                           ByAccountNumber = SearchingByAccount
			                                           } ).Result;
		}

		public override IList<CustomerLookupSummary> First( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<CustomerLookupSummary> Last( uint count )
		{
			return Azure.Client.RequestCustomerLookup( new CustomerLookup
			                                           {
				                                           PreFetchCount = (int)count,
				                                           Fetch = PreFetch.PREFETCH.LAST,
				                                           ByAccountNumber = SearchingByAccount
			                                           } ).Result;
		}

		public override IList<CustomerLookupSummary> Last( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<CustomerLookupSummary> FindRange( string key, int count )
		{
			return Azure.Client.RequestCustomerLookup( new CustomerLookup
			                                           {
				                                           PreFetchCount = count,
				                                           Key = key,
				                                           Fetch = PreFetch.PREFETCH.FIND_RANGE,
				                                           ByAccountNumber = SearchingByAccount
			                                           } ).Result;
		}

		public override IList<CustomerLookupSummary> FindMatch( string key, int count )
		{
			return Azure.Client.RequestCustomerLookup( new CustomerLookup
			                                           {
				                                           PreFetchCount = count,
				                                           Key = key,
				                                           Fetch = PreFetch.PREFETCH.FIND_MATCH,
				                                           ByAccountNumber = SearchingByAccount
			                                           } ).Result;
		}

		public override string AsString( string key )
		{
			return key;
		}

		public override int CompareSearchKey( string k1, string k2 )
		{
			return string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override int Compare( string k1, string k2 )
		{
			return string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool StartsWith( string searchValue, string k2 )
		{
			return k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool Filter( string filter, CustomerLookupSummary value )
		{
			throw new NotImplementedException();
		}

		public override string Key( CustomerLookupSummary item )
		{
			return SearchingByAccount ? item.CustomerCode : item.CompanyName;
		}

		public override string Key( string searchText )
		{
			return searchText;
		}

		public override bool IsEmpty( string key )
		{
			return string.IsNullOrEmpty( key );
		}

		public Action OnCustomerCodeChanges;
	}
}