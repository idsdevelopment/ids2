﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AzureRemoteService;
using IdsControlLibraryV2.Combos;
using Protocol.Data;
using Utils;

namespace IdsRemoteServiceControlLibraryV2.Staff
{
	public class LookupItem : LookupComboBoxModelBase<StaffLookupSummary, string>.LookupItem
	{
		public LookupItem( StaffLookupSummary item ) : base( item )
		{
		}
	}

	public class StaffLookupComboBoxViewModel : LookupComboBoxModelBase<StaffLookupSummary, string>
	{
		public string StaffId
		{
			get { return Get( () => StaffId, "" ); }
			set { Set( () => StaffId, value ); }
		}


		[DependsUpon( nameof( StaffId ) )]
		public void WhenStaffIdChanges()
		{
			Items = new ObservableCollection<LookupItem>();

			if( SearchText == "" )
				WhenSearchTextChanges();
			else
				SearchText = "";

			StaffLookupComboBox.StaffId = StaffId;
		}


		public override IList<StaffLookupSummary> First( uint count )
		{
			if( !IsInDesignMode )
			{
				return Azure.Client.RequestStaffLookup( new StaffLookup
				                                        {
					                                        Key = StaffId,
					                                        PreFetchCount = (int)count,
					                                        Fetch = PreFetch.PREFETCH.FIRST
				                                        } ).Result;
			}

			return new List<StaffLookupSummary>();
		}

		public override IList<StaffLookupSummary> First( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<StaffLookupSummary> Last( uint count )
		{
			if( !IsInDesignMode )
			{
				return Azure.Client.RequestStaffLookup( new StaffLookup
				                                        {
					                                        Key = StaffId,
					                                        PreFetchCount = (int)count,
					                                        Fetch = PreFetch.PREFETCH.LAST
				                                        } ).Result;
			}

			return new List<StaffLookupSummary>();
		}

		public override IList<StaffLookupSummary> Last( string key, uint count )
		{
			throw new NotImplementedException();
		}

		public override IList<StaffLookupSummary> FindRange( string key, int count )
		{
			if( !IsInDesignMode )
			{
				return Azure.Client.RequestStaffLookup( new StaffLookup
				                                        {
					                                        Key = key,
					                                        PreFetchCount = count,
					                                        Fetch = PreFetch.PREFETCH.FIND_RANGE
				                                        } ).Result;
			}

			return new List<StaffLookupSummary>();
		}

		public override IList<StaffLookupSummary> FindMatch( string key, int count )
		{
			if( !IsInDesignMode )
			{
				return Azure.Client.RequestStaffLookup( new StaffLookup
				                                        {
					                                        Key = key,
					                                        PreFetchCount = count,
					                                        Fetch = PreFetch.PREFETCH.FIND_MATCH
				                                        } ).Result;
			}

			return new List<StaffLookupSummary>();
		}

		public override int CompareSearchKey( string searchKey, string k2 )
		{
			return string.Compare( searchKey, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override int Compare( string k1, string k2 )
		{
			return string.Compare( k1, k2, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool StartsWith( string searchValue, string k2 )
		{
			return k2.StartsWith( searchValue, StringComparison.CurrentCultureIgnoreCase );
		}

		public override bool Filter( string filter, StaffLookupSummary value )
		{
			throw new NotImplementedException();
		}

		public override string Key( StaffLookupSummary item )
		{
			return item.StaffId;
		}

		public override string Key( string searchText )
		{
			return searchText;
		}

		public override bool IsEmpty( string key )
		{
			return key.IsNullOrEmpty();
		}

		public override string AsString( string key )
		{
			return key;
		}

		public StaffLookupComboBox StaffLookupComboBox;
	}
}