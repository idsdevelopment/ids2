﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using IdsControlLibrary.Utils;

namespace IdsControlLibrary
{
    /// <summary>
    ///     Interaction logic for HamburgerWindow.xaml
    /// </summary>
    internal partial class HamburgerWindow
    {
        private readonly Window MainWindow;
        private readonly IdsContentPresenter Presenter;

        internal bool AutoClosed;

        private void SetSize()
        {
            if( MainWindow != null )
            {
                Top = MainWindow.Top + 100;
                Left = MainWindow.Left + 3;
                Height = MainWindow.ActualHeight - 129;
            }
        }

        public HamburgerWindow( Window mainWindow, IdsContentPresenter presenter )
        {
            MainWindow = mainWindow;
            Presenter = presenter;

            InitializeComponent();
            if( mainWindow != null )
            {
                mainWindow.LayoutUpdated += ( sender, args ) => { SetSize(); };
                mainWindow.LocationChanged += ( sender, args ) => { SetSize(); };
                base.Visibility = Visibility.Hidden;
            }
            else
                SetSize();
        }

        private bool InVisibilityChange;

        public new Visibility Visibility
        {
            get => base.Visibility;
            set
            {
                if( !InVisibilityChange )
                {
                    InVisibilityChange = true;
                    try
                    {
                        var Vis = value == Visibility.Visible;

                        if( Vis )
                        {
                            if( !AutoClosed )
                                SetSize();
                            else
                                return;
                        }


                        if( Vis )
                        {
                            Activate();
                            this.SlideFromLeft().FadeIn( 0.5, 0.3 );
                            base.Visibility = Visibility.Visible;
                        }
                        else
                            this.FadeOut( 0.2, 0.1, () => { base.Visibility = Visibility.Hidden; } );
                    }
                    finally
                    {
                        var T = new Timer( 600 );
                        T.Elapsed += ( o, args ) =>
                        {
                            InVisibilityChange = false;
                            T.Dispose();
                        };
                        T.Start();
                    }
                }
            }
        }

        internal void DoHamburgerClick()
        {
            Visibility = Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
        }

        private void Window_Deactivated( object sender, EventArgs e )
        {
            if( !InVisibilityChange )
            {
                this.FadeOut( 0.2, 0.1, () =>
                {
                    base.Visibility = Visibility.Hidden;
                    AutoClosed = true;
                    var T = new Timer( 300 );
                    T.Elapsed += ( o, args ) =>
                    {
                        AutoClosed = false;
                        T.Dispose();
                    };
                    T.Start();
                } );
            }
        }

        private void ListBox_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            e.Handled = true;
            var Ndx = ListBox.SelectedIndex;
            if( Ndx >= 0 )
            {
                if( ListBox.Items[ Ndx ] is HamburgerItem Item )
                {
                    var Prog = Item.OnClick( Presenter );
                    if( Prog != null )
                        Presenter.LoadProgram( Prog );
                }
                ListBox.SelectedIndex = -1;
                Window_Deactivated( this, null );
            }
        }
    }
}