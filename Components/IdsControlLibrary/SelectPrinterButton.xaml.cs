﻿using System.Windows;
using System.Windows.Media;

namespace IdsControlLibrary
{
    /// <summary>
    ///     Interaction logic for SelectPrinterButton.xaml
    /// </summary>
    public partial class SelectPrinterButton
    {
        public SelectPrinterButton()
        {
            InitializeComponent();
        }

        public delegate void SelectionChangedEvent( object sender, PrinterInfo newPrinter, string name );

        public event SelectionChangedEvent SelcectionChanged;

        public string Caption
        {
            get => (string)GetValue( CaptionProperty );
            set => SetValue( CaptionProperty, value );
        }

        // Using a DependencyProperty as the backing store for Caption.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.Register( "Caption", typeof( string ), typeof( SelectPrinterButton ), new PropertyMetadata( "Select Printer" ) );

        public string CurrentPrinterName => SelectPrinter.CurrentPrinterName;

        public string SettingsFolder
        {
            get => (string)GetValue( SettingsFolderProperty );
            set
            {
                SetValue( SettingsFolderProperty, value );
                SelectPrinter.SettingsFolder = value;
            }
        }

        // Using a DependencyProperty as the backing store for SettingsFolder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SettingsFolderProperty = DependencyProperty.Register( "SettingsFolder",
                                                                                                        typeof( string ),
                                                                                                        typeof( SelectPrinterButton ),
                                                                                                        new FrameworkPropertyMetadata( SelectPrinter.PRINTER_SETTINGS_FOLDER,
                                                                                                                                       FrameworkPropertyMetadataOptions.None,
                                                                                                                                       OnSettingsFolderChanged ) );

        private static void OnSettingsFolderChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
        {
            ( (SelectPrinterButton)obj ).SettingsFolder = (string)e.NewValue;
        }

        public string ApplicationContext
        {
            get => (string)GetValue( ApplicationContextProperty );
            set
            {
                SetValue( ApplicationContextProperty, value );
                SelectPrinter.ApplicationContext = value;
            }
        }

        // Using a DependencyProperty as the backing store for ApplicationContext.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ApplicationContextProperty = DependencyProperty.Register( "ApplicationContext",
                                                                                                            typeof( string ),
                                                                                                            typeof( SelectPrinterButton ),
                                                                                                            new FrameworkPropertyMetadata( SelectPrinter.DEFAULT_CONTEXT,
                                                                                                                                           FrameworkPropertyMetadataOptions.None,
                                                                                                                                           OnApplicationContextChanged ) );

        private static void OnApplicationContextChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
        {
            ( (SelectPrinterButton)obj ).ApplicationContext = (string)e.NewValue;
        }

        public string Title
        {
            get => SelectPrinter.Title;
            set => SelectPrinter.Title = value;
        }

        public Brush TitleForeground
        {
            get => (Brush)GetValue( TitleForegroundProperty );
            set
            {
                SetValue( TitleForegroundProperty, value );
                SelectPrinter.TitleForeground = value;
            }
        }

        // Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleForegroundProperty =
            DependencyProperty.Register( "TitleForeground",
                                         typeof( Brush ),
                                         typeof( SelectPrinterButton ),
                                         new FrameworkPropertyMetadata( Brushes.Black,
                                                                        FrameworkPropertyMetadataOptions.AffectsRender,
                                                                        ( o, args ) => { ( (SelectPrinterButton)o ).TitleForeground = (Brush)args.NewValue; } ) );

        public Brush TitleBackground
        {
            get => (Brush)GetValue( TitleBackgroundProperty );
            set
            {
                SetValue( TitleBackgroundProperty, value );
                SelectPrinter.TitleBackground = value;
            }
        }

        // Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBackgroundProperty =
            DependencyProperty.Register( "TitleBackground",
                                         typeof( Brush ),
                                         typeof( SelectPrinterButton ),
                                         new FrameworkPropertyMetadata( Brushes.Transparent,
                                                                        FrameworkPropertyMetadataOptions.AffectsRender,
                                                                        ( o, args ) => { ( (SelectPrinterButton)o ).TitleBackground = (Brush)args.NewValue; } ) );

        public string OkCaption
        {
            get => SelectPrinter.OkCaption;
            set => SelectPrinter.OkCaption = value;
        }

        public string CancelCaption
        {
            get => SelectPrinter.CancelCaption;
            set => SelectPrinter.CancelCaption = value;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            var RetVal = SelectPrinter.Execute();
            if( RetVal.Ok )
                SelcectionChanged?.Invoke( this, RetVal.PrinterInfo, RetVal.PrinterInfo.Name );
        }
    }
}