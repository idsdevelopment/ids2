﻿using System.Threading.Tasks;
using System.Windows;
using AzureRemoteService;
using IdsControlLibraryV2.Debug;

namespace Test_Application
{
	/// <summary>
	///     Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			Closed += (sender, args) =>
			          {
				          Application.Current.Shutdown();
			          };

            //			DebugTrace.Listen();
            Task.Run( () =>
			          {
				          using( var Client = new AzureClient() )
				          {
					          Client.LogIn( "priority", "admin", "window" );
				          }

				          Dispatcher.Invoke( () =>
				                             {
					                             CustComboBox.CustomerCode = "wadsworth";
				                             } );
			          } );

		}
	}
}