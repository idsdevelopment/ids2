﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IdsControlLibraryV2;
using IdsRemoteServiceControlLibraryV2.DataSources.Routes;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Routes
{
    /// <summary>
    ///     Interaction logic for RouteLookupComboBox.xaml
    /// </summary>
    public partial class RouteLookupComboBox : UserControl
    {
        public RouteLookupComboBox()
        {
            InitializeComponent();
            RouteLookupCombox.DataContext = this;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowBackground
        {
            get => (Brush)GetValue( OddRowBackgroundProperty );
            set => SetValue( OddRowBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowBackgroundProperty =
            DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.White ) );

        [ Category( "Brush" ) ]
        public Brush OddRowForeground
        {
            get => (Brush)GetValue( OddRowForegroundProperty );
            set => SetValue( OddRowForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowForegroundProperty =
            DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

        [ Category( "Brush" ) ]
        public Brush EvenRowBackground
        {
            get => (Brush)GetValue( EvenRowBackgroundProperty );
            set => SetValue( EvenRowBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowBackgroundProperty =
            DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.BlanchedAlmond ) );

        [ Category( "Brush" ) ]
        public Brush EvenRowForeground
        {
            get => (Brush)GetValue( EvenRowForegroundProperty );
            set => SetValue( EvenRowForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowForegroundProperty =
            DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

        [ Category( "Common" ) ]
        public int SelectedIndex
        {
            get => (int)GetValue( SelectedIndexProperty );
            set => SetValue( SelectedIndexProperty, value );
        }

        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( RouteLookupComboBox ), new PropertyMetadata( -1 ) );

        [ Category( "Options" ) ]
        public int PreFetch
        {
            get => (int)GetValue( PreFetchProperty );
            set => SetValue( PreFetchProperty, value );
        }

        // Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreFetchProperty =
            DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( LookupComboBoxBase.DEFAULT_PREFETCH, PreFetchPropertyChangedCallback ) );

        private static void PreFetchPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is int Count )
                Combo.RouteLookupCombox.PreFetch = Count;
        }

        [ Category( "Options" ) ]
        public string CustomerCode
        {
            get => (string)GetValue( CustomerCodeProperty );
            set => SetValue( CustomerCodeProperty, value );
        }

        // Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CustomerCodeProperty =
            DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, CustomerCodePropertyChangedCallback ) );

        private Timer CustomerTimer;

        private static void CustomerCodePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is string CustCode )
            {
                var T = Combo.CustomerTimer;

                if( T != null )
                {
                    T.Stop();
                    T.Dispose();
                }

                Combo.CustomerTimer = T = new Timer( 300 )
                                          {
                                              AutoReset = false
                                          };

                T.Elapsed += ( sender, args ) =>
                {
                    T.Stop();
                    T.Dispose();
                    Combo.CustomerTimer = null;

                    var Rc = Combo.RouteCollection;
                    Rc.CustomerCode = CustCode;
                    Rc.First( CustCode );
                };
                T.Start();
            }
        }

        private bool AllowSetRoute;

        [ Category( "Options" ) ]
        public RouteSchedule Route
        {
            get => (RouteSchedule)GetValue( RouteProperty );
            set
            {
                if( AllowSetRoute )
                    SetValue( RouteProperty, value );
            }
        }

        // Using a DependencyProperty as the backing store for Route.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RouteProperty =
            DependencyProperty.Register( nameof( Route ), typeof( RouteSchedule ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );

        [ Category( "Options" ) ]
        public string RouteName
        {
            get => (string)GetValue( RouteNameProperty );
            set => SetValue( RouteNameProperty, value );
        }

        // Using a DependencyProperty as the backing store for RouteName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RouteNameProperty =
            DependencyProperty.Register( nameof( RouteName ), typeof( string ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, RouteNamePropertyChangedCallback ) );

        private static void RouteNamePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is string RName && !Combo.InSelectionChange )
            {
                Combo.RouteLookupCombox.IsDropDownOpen = false;

                Combo.RouteCollection.FindMatch( RName, Combo.PreFetch, () =>
                {
                    Combo.Dispatcher.InvokeAsync( () =>
                    {
                        Combo.AllowSetRoute = true;
                        var Rc = Combo.RouteCollection;
                        Rc.SelectedKey = RName;

                        var R = Rc.SelectedItem?.Item;
                        if( R != null )
                        {
                            Combo.Route = R;
                            Combo.RouteLookupCombox.Text = RName;
                        }
                        Combo.AllowSetRoute = false;
                    } );
                }, true );
            }
        }

        public event Action<object, SelectionChangedEventArgs> SelectionChanged;

        private RouteLookupVirtualObservableCollection RouteCollection;

        private void UserControl_Initialized( object sender, EventArgs e )
        {
            if( !RouteLookupCombox.IsDesignMode )
                RouteLookupCombox.ItemsSource = RouteCollection = new RouteLookupVirtualObservableCollection();
        }

        protected virtual void OnSelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            SelectionChanged?.Invoke( arg1, arg2 );
        }

        private bool InSelectionChange;

        private void LookupCombo_SelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            InSelectionChange = true;
            var R = RouteCollection.SelectedItem?.Item;
            if( R != null )
            {
                var Key = R.RouteName;
                var Source = RouteLookupCombox.ItemsSource;

                Task.Run( () =>
                {
                    var Item = (RouteComboBoxLookupItem)Source.GetItem( Key );
                    Dispatcher.Invoke( () =>
                    {
                        RouteName = Item.LookupKey;
                        AllowSetRoute = true;
                        Route = Item.Item;
                        AllowSetRoute = false;
                        OnSelectionChanged( this, arg2 );
                        InSelectionChange = false;
                    } );
                } );
            }
        }

        public void Refresh()
        {
            RouteLookupCombox.Refresh( CustomerCode );
        }
    }
}