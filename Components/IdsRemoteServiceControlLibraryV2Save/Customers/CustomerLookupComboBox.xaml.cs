﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IdsControlLibraryV2;
using IdsRemoteServiceControlLibraryV2.DataSources.Customers;

namespace IdsRemoteServiceControlLibraryV2.Customers
{
	/// <summary>
	///     Interaction logic for CustomerLookupComboBox.xaml
	/// </summary>
	public partial class CustomerLookupComboBox
	{
		public CustomerLookupComboBox()
		{
			InitializeComponent();
			CustomerLookupCombo.DataContext = this;
		}

		public new bool Focus()
		{
			return CustomerLookupCombo.Focus();
		}

		[ Category( "Brush" ) ]
		public Brush OddRowBackground
		{
			get => (Brush)GetValue( OddRowBackgroundProperty );
			set => SetValue( OddRowBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty OddRowBackgroundProperty =
			DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.White ) );

		[ Category( "Brush" ) ]
		public Brush OddRowForeground
		{
			get => (Brush)GetValue( OddRowForegroundProperty );
			set => SetValue( OddRowForegroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty OddRowForegroundProperty =
			DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

		[ Category( "Brush" ) ]
		public Brush EvenRowBackground
		{
			get => (Brush)GetValue( EvenRowBackgroundProperty );
			set => SetValue( EvenRowBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty EvenRowBackgroundProperty =
			DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.BlanchedAlmond ) );

		[ Category( "Brush" ) ]
		public Brush EvenRowForeground
		{
			get => (Brush)GetValue( EvenRowForegroundProperty );
			set => SetValue( EvenRowForegroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty EvenRowForegroundProperty =
			DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

		[ Category( "Common" ) ]
		public int SelectedIndex
		{
			get => (int)GetValue( SelectedIndexProperty );
			set => SetValue( SelectedIndexProperty, value );
		}

		[ Category( "Common" ) ]
		public string Text
		{
			get => CustomerLookupCombo.Text;
			set => SetValue( TextProperty, value );
		}

		// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register( nameof( Text ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "", TextPropertyChangedCallback ) );

		private static void TextPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox Combo && e.NewValue is string Text )
				Combo.CustomerLookupCombo.Text = Text;
		}

		// Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedIndexProperty =
			DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( -1 ) );

		[ Category( "Options" ) ]
		public bool EnableNew
		{
			get => (bool)GetValue( EnableNewProperty );
			set => SetValue( EnableNewProperty, value );
		}

		[ Category( "Options" ) ]
		public bool ByCompanyName
		{
			get => (bool)GetValue( ByCompanyNameProperty );
			set => SetValue( ByCompanyNameProperty, value );
		}

		// Using a DependencyProperty as the backing store for ByCompanyName.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ByCompanyNameProperty =
			DependencyProperty.Register( nameof( ByCompanyName ), typeof( bool ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( true ) );

		// Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty EnableNewProperty =
			DependencyProperty.Register( nameof( EnableNew ), typeof( bool ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( false ) );

		[ Category( "Options" ) ]
		public int PreFetch
		{
			get => (int)GetValue( PreFetchProperty );
			set => SetValue( PreFetchProperty, value );
		}

		// Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PreFetchProperty =
			DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( LookupComboBoxBase.DEFAULT_PREFETCH ) );

		[ Category( "Options" ) ]
		public string CustomerCode
		{
			get => (string)GetValue( CustomerCodeProperty );
			set => SetValue( CustomerCodeProperty, value );
		}

		// Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CustomerCodeProperty =
			DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, CustomerCodePropertyChangedCallback ) );

		internal bool AllowSelection;

		private static void CustomerCodePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is CustomerLookupComboBox ComboBox && e.NewValue is string Code )
			{
				ComboBox.AllowSelection = true;
				ComboBox.CustomerLookupCombo.ItemsSource.SelectedKey = Code;
			}
		}

		[ Category( "Options" ) ]
		public bool RequiresExactMatch
		{
			get => (bool)GetValue( RequiresExactMatchProperty );
			set => SetValue( RequiresExactMatchProperty, value );
		}

		// Using a DependencyProperty as the backing store for RequiresExactMatch.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty RequiresExactMatchProperty =
			DependencyProperty.Register( nameof( RequiresExactMatch ), typeof( bool ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( true ) );

		public event Action<object, SelectionChangedEventArgs> SelectionChanged;

		private void LookupCombo_SelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
		{
			OnSelectionChanged( this, arg2 );
		}

		protected virtual void OnSelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
		{
			var Txt = arg2.AddedItems.Count > 0 ? arg2.AddedItems[ 0 ].ToString() : CustomerLookupCombo.Text;

			if( RequiresExactMatch )
			{
				var Is = CustomerLookupCombo.ItemsSource;
				Is.SelectedKey = Txt;
				if( Is.SelectedItem is CustomerComboBoxLookupItem Item )
					CustomerCode = Item.CustomerCode;
				else
					CustomerCode = "";
			}
			else
				CustomerCode = Txt;

			SelectionChanged?.Invoke( arg1, arg2 );
		}

		private void UserControl_Loaded( object sender, RoutedEventArgs e )
		{
			if( !CustomerLookupCombo.IsDesignMode )
				CustomerLookupCombo.ItemsSource = new CustomerLookupVirtualObservableCollection( ByCompanyName );
		}
	}
}