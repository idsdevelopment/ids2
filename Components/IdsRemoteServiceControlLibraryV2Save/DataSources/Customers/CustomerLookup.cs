﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzureRemoteService;
using IdsControlLibraryV2;
using IdsControlLibraryV2.Virtualisation;
using IdsControlLibraryV2.Virtualisation.LookupCombo;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.DataSources.Customers
{
    public class CustomerComboBoxLookupItem : ComboBoxLookupItem
    {
        internal CustomerLookupSummary Item;
        public override string LookupKey => ByCompanyName ? Item.CompanyName : Item.CustomerCode;
        public override string FirstLineText => ByCompanyName ? Item.CompanyName : Item.CustomerCode;
        public override string SecondLineText => ByCompanyName ? Item.CustomerCode : Item.CompanyName;
        public override string ThirdLineText => Item.AddressLine1;

        public string CustomerCode => Item.CustomerCode;

        public bool ByCompanyName = true;
    }

    public class CustomersLookupDataSource : IItemsProvider<CustomerComboBoxLookupItem, string>
    {
        public readonly bool ByCompanyName;

        public CustomersLookupDataSource( bool byCompanyName )
        {
            ByCompanyName = byCompanyName;
        }

        public int CompareKey( string k1, string k2 )
        {
            return string.CompareOrdinal( k1, k2 );
        }

        public bool Filter( string filter, CustomerComboBoxLookupItem value )
        {
            return ( filter == null ) || value.LookupKey.StartsWith( filter, StringComparison.InvariantCultureIgnoreCase );
        }

        public string Key( CustomerComboBoxLookupItem item )
        {
            return item.LookupKey;
        }

        private IList<CustomerComboBoxLookupItem> GetItems( PreFetch.PREFETCH prefetch, string key, int count )
        {
            using( var Client = new AzureClient() )
            {
                var RetVal = new List<CustomerComboBoxLookupItem>();

                var Response = Client.RequestCustomerLookup( new CustomerLookup
                                                             {
                                                                 Key = key,
                                                                 PreFetchCount = count,
                                                                 Fetch = prefetch,
                                                                 ByAccountNumber = !ByCompanyName
                                                             } ).Result;
                if( Response != null )
                {
                    RetVal.AddRange( from C in Response
                                     select new CustomerComboBoxLookupItem
                                            {
                                                Item = C,
                                                ByCompanyName = ByCompanyName
                                            } );
                }
                return RetVal;
            }
        }

        public IList<CustomerComboBoxLookupItem> FindMatch( string key, int count )
        {
            return GetItems( PreFetch.PREFETCH.FIND_MATCH, key, count );
        }

        public IList<CustomerComboBoxLookupItem> Last( string key, uint count )
        {
            throw new NotImplementedException();
        }

        public IList<CustomerComboBoxLookupItem> FindRange( string key, int count )
        {
            return GetItems( PreFetch.PREFETCH.FIND_RANGE, key, count );
        }

        public IList<CustomerComboBoxLookupItem> First( uint count )
        {
            return GetItems( PreFetch.PREFETCH.FIRST, "", (int)count );
        }

        public IList<CustomerComboBoxLookupItem> First( string key, uint count )
        {
            throw new NotImplementedException();
        }

        public IList<CustomerComboBoxLookupItem> Last( uint count )
        {
            return GetItems( PreFetch.PREFETCH.LAST, "", (int)count );
        }
    }

    public class CustomerLookupVirtualObservableCollection : LookupComboBoxVirtualObservableCollection<CustomerComboBoxLookupItem>
    {
        public CustomerLookupVirtualObservableCollection( bool byCompanyName ) : base( new CustomersLookupDataSource( byCompanyName ) )
        {
        }
    }
}