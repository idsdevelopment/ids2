﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IdsControlLibraryV2;

namespace IdsRemoteServiceControlLibraryV2.Staff
{
    /// <summary>
    ///     Interaction logic for StaffLookupComboBox.xaml
    /// </summary>
    public partial class StaffLookupComboBox : UserControl
    {
        public StaffLookupComboBox()
        {
            InitializeComponent();
            LookupCombo.DataContext = this;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowBackground
        {
            get => (Brush)GetValue( OddRowBackgroundProperty );
            set => SetValue( OddRowBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowBackgroundProperty =
            DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.White ) );

        [ Category( "Brush" ) ]
        public Brush OddRowForeground
        {
            get => (Brush)GetValue( OddRowForegroundProperty );
            set => SetValue( OddRowForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowForegroundProperty =
            DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

        [ Category( "Brush" ) ]
        public Brush EvenRowBackground
        {
            get => (Brush)GetValue( EvenRowBackgroundProperty );
            set => SetValue( EvenRowBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowBackgroundProperty =
            DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.BlanchedAlmond ) );

        [ Category( "Brush" ) ]
        public Brush EvenRowForeground
        {
            get => (Brush)GetValue( EvenRowForegroundProperty );
            set => SetValue( EvenRowForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowForegroundProperty =
            DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black ) );

        [ Category( "Common" ) ]
        public int SelectedIndex
        {
            get => (int)GetValue( SelectedIndexProperty );
            set => SetValue( SelectedIndexProperty, value );
        }

        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( -1 ) );

        [ Category( "Options" ) ]
        public bool EnableNew
        {
            get => (bool)GetValue( EnableNewProperty );
            set => SetValue( EnableNewProperty, value );
        }

        // Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableNewProperty =
            DependencyProperty.Register( nameof( EnableNew ), typeof( bool ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( false ) );

        [ Category( "Options" ) ]
        public int PreFetch
        {
            get => (int)GetValue( PreFetchProperty );
            set => SetValue( PreFetchProperty, value );
        }

        // Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreFetchProperty =
            DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( StaffLookupComboBox ), new FrameworkPropertyMetadata( LookupComboBoxBase.DEFAULT_PREFETCH ) );
    }
}