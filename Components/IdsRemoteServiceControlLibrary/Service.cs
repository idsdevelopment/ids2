﻿using RemoteService;

namespace IdsRemoteServiceControlLibrary
{
    internal static class Globals
    {
        internal static IAzRemoteService Service => BackendSupport.Globals.Backend.AzureRemoteService;
    }
}