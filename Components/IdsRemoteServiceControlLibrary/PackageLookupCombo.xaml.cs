﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibrary.Utils;
using IdsRemoteServiceControlLibrary.Annotations;
using Protocol.Data;

namespace IdsRemoteServiceControlLibrary
{
    public class PackageTypeLookup : PackageType
    {
        private readonly int Index;

        public bool Even => ( Index & 1 ) == 0;

        private string[] _SearchText;

        public string[] SearchText => _SearchText ?? ( _SearchText = $"{Description} {Label}".ToUpper().Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) );

        public bool Excluded { get; set; }

        public PackageTypeLookup()
        {
        }

        public PackageTypeLookup( PackageType p, int index ) : base( p )
        {
            Index = index;
        }

        public PackageTypeLookup( PackageTypeLookup p ) : base( p )
        {
            Index = p.Index;
            _SearchText = p._SearchText;
        }
    }

    public class PackageTypeLookupList : ObservableCollection<PackageTypeLookup>
    {
        public PackageTypeLookupList( PackageTypeList pl )
        {
            if( pl != null )
            {
                var Ndx = 0;
                foreach( var Pt in pl )
                    Add( new PackageTypeLookup( Pt, Ndx++ ) );
            }
        }

        public PackageTypeLookupList( PackageTypeLookupList pl )
        {
            if( pl != null )
            {
                foreach( var Pt in pl )
                    Add( new PackageTypeLookup( Pt ) );
            }
        }
    }

    internal class PackageTypeBoolToColourConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is bool Grey )
                return Grey ? PackageLookupCombo.Odd : PackageLookupCombo.Even;

            return Brushes.White;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    internal class PackageTypeLabelConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is string Label )
                return string.IsNullOrEmpty( Label ) ? Visibility.Collapsed : Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    internal class PackageTypeVisibilityConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is bool NotVisible )
                return NotVisible ? Visibility.Collapsed : Visibility.Visible;

            return Visibility.Visible;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    ///     Interaction logic for PackageLookupCombo.xaml
    /// </summary>
    public partial class PackageLookupCombo : INotifyPropertyChanged
    {
        internal static Brush Odd;
        internal static Brush Even;

        private static readonly object LockObject = new object();

        private static PackageTypeLookupList _PackageTypes;

        private TextBox TextBox;

        private readonly bool DesignMode;

        public delegate void PackageTypesLoadedEvent( PackageTypeLookupList packageTypes );

        public event PackageTypesLoadedEvent PackageTypesLoaded;

        public new void Focus()
        {
            LookupCombo.Focus();
        }

        public PackageLookupCombo()
        {
            DesignMode = DesignerProperties.GetIsInDesignMode( this );

            PackageTypeLookupList Pt = null;

            if( !DesignMode )
            {
                lock( LockObject )
                    Pt = _PackageTypes;

                if( Pt == null )
                {
                    Task.Run( () =>
                    {
                        lock( LockObject )
                        {
                            var Brushes = Colours.GetComboBoxBrushes();
                            Odd = Brushes.Odd;
                            Even = Brushes.Even;
                        }
                    } );

                    Task.Run( () =>
                    {
                        var Srv = Globals.Service;

                        var Pl = Srv.GetPackageTypes();
                        lock( LockObject )
                        {
                            _PackageTypes = new PackageTypeLookupList( Pl );
                            PackageTypes = _PackageTypes;
                        }
                    } );
                }
            }

            InitializeComponent();

            if( Pt != null )
                PackageTypes = Pt;

            AddButton.Visibility = EnableNew ? Visibility.Visible : Visibility.Collapsed;
            EnableAdd( ( false, true ) );
        }

        public PackageTypeLookupList PackageTypes
        {
            get
            {
                while( !DesignMode )
                {
                    PackageTypeLookupList Pl;
                    lock( LockObject )
                        Pl = _PackageTypes;
                    if( Pl != null )
                        break;

                    Thread.Sleep( 100 );
                }
                return (PackageTypeLookupList)GetValue( PackageTypesProperty );
            }

            set
            {
                value = new PackageTypeLookupList( value ); // Clone to avoid excluded flags

                if( ExclusionsList != null )
                {
                    foreach( var PType in value )
                    {
                        PType.Excluded = false;

                        var Pt = PType.Description;
                        foreach( var Exclusion in ExclusionsList )
                        {
                            if( Exclusion.Description == Pt )
                            {
                                PType.Excluded = true;
                                break;
                            }
                        }
                    }
                }

                Dispatcher.Invoke( () =>
                {
                    SetValue( PackageTypesProperty, value );
                    LookupCombo.Items?.Refresh();
                } );
            }
        }

        // Using a DependencyProperty as the backing store for PackageTypes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PackageTypesProperty =
            DependencyProperty.Register( nameof( PackageTypes ), typeof( PackageTypeLookupList ), typeof( PackageLookupCombo ) );

        public int MaxDropDownHeight
        {
            get => (int)GetValue( MaxDropDownHeightProperty );
            set
            {
                SetValue( MaxDropDownHeightProperty, value );
                OnPropertyChanged( nameof( MaxDropDownHeight ) );
            }
        }

        // Using a DependencyProperty as the backing store for MaxDropDownHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxDropDownHeightProperty =
            DependencyProperty.Register( nameof( MaxDropDownHeight ), typeof( int ), typeof( PackageLookupCombo ), new PropertyMetadata( 300 ) );

        public PackageType SelectedPackageType
        {
            get => (PackageType)GetValue( SelectedPackageTypeProperty );
            set
            {
                SetValue( SelectedPackageTypeProperty, value );
                OnPropertyChanged( nameof( SelectedPackageType ) );
            }
        }

        // Using a DependencyProperty as the backing store for SelectedPackageType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedPackageTypeProperty =
            DependencyProperty.Register( nameof( SelectedPackageType ), typeof( PackageType ), typeof( PackageLookupCombo ) );

        public bool EnableNew
        {
            get => (bool)GetValue( EnableNewProperty );
            set
            {
                Dispatcher.Invoke( () =>
                {
                    AddButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                    SetValue( EnableNewProperty, value );
                    OnPropertyChanged( nameof( EnableNew ) );
                } );
            }
        }

        // Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableNewProperty =
            DependencyProperty.Register( nameof( EnableNew ), typeof( bool ), typeof( PackageLookupCombo ), new PropertyMetadata( false, EnableNewValueChanged ) );

        private static void EnableNewValueChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            if( d is PackageLookupCombo Combo && e.NewValue is bool Visible )
                Combo.AddButton.Visibility = Visible ? Visibility.Visible : Visibility.Collapsed;
        }

        public ObservableCollection<PackageType> ExclusionsList
        {
            get
            {
                ObservableCollection<PackageType> RetVal = null;
                Dispatcher.Invoke( () => { RetVal = (ObservableCollection<PackageType>)GetValue( ExclusionsListProperty ); } );

                return RetVal;
            }

            set
            {
                Task.Run( () => // Stop deadlock
                {
                    Dispatcher.Invoke( () =>
                    {
                        value.CollectionChanged += ( sender, args ) =>
                        {
                            PackageTypes = PackageTypes; // Force refresh
                            OnPropertyChanged( nameof( ExclusionsList ) );
                        };

                        SetValue( ExclusionsListProperty, value );
                        PackageTypes = PackageTypes; // Force refresh
                    } );
                } );
            }
        }

        // Using a DependencyProperty as the backing store for ExclusionsList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExclusionsListProperty =
            DependencyProperty.Register( nameof( ExclusionsList ), typeof( ObservableCollection<PackageType> ), typeof( PackageLookupCombo ) );

        public bool IsReadOnly
        {
            get => (bool)GetValue( IsReadOnlyProperty );
            set => SetValue( IsReadOnlyProperty, value );
        }

        // Using a DependencyProperty as the backing store for IsReadOnly.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register( nameof( IsReadOnly ), typeof( bool ), typeof( PackageLookupCombo ), new PropertyMetadata( false ) );

        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        public event SelectionChangedEventHandler SelectionChanged;

        private void LookupCombo_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if( LookupCombo.SelectedItem is PackageType Pt )
            {
                SelectedPackageType = Pt;
                Dispatcher.InvokeAsync( () => { LookupCombo.Text = Pt.Description; } );
                SelectionChanged?.Invoke( this, e );
            }
        }

        private void EnableAdd( (bool HasPartialMatch, bool HasCompleteMatch) matches )
        {
            var Enab = !matches.HasCompleteMatch && !string.IsNullOrEmpty( LookupCombo.Text.Trim() );
            AddButton.IsEnabled = Enab;
            AddImage.IsEnabled = Enab;
            if( !matches.HasPartialMatch )
                LookupCombo.IsDropDownOpen = false;
        }

        private void LookupCombo_KeyUp( object sender, KeyEventArgs e )
        {
            if( !IsReadOnly )
            {
                Controls.ComboxSearch( LookupCombo, ref TextBox, i =>
                {
                    var PType = _PackageTypes[ i ];
                    return ( FullText: PType.Description, SearchText: PType.SearchText );
                }, EnableAdd );
            }
        }

        private void LookupCombo_Loaded( object sender, RoutedEventArgs e )
        {
            if( PackageTypesLoaded != null )
            {
                Task.Run( () =>
                {
                    while( true )
                    {
                        lock( LockObject )
                        {
                            if( _PackageTypes != null )
                                break;
                        }
                        Thread.Sleep( 100 );
                    }
                    Dispatcher.InvokeAsync( () => { PackageTypesLoaded( _PackageTypes ); } );
                } );
            }

            LookupCombo.SetReadOnlyColour( IsReadOnly );
        }

        private void LookupCombo_DropDownOpened( object sender, EventArgs e )
        {
            LookupCombo.IsDropDownOpen = true;
        }

        private void LookupCombo_MouseMove( object sender, MouseEventArgs e )
        {
            e.Handled = true;
        }
    }
}