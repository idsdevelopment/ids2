﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibrary.Utils;
using IdsRemoteServiceControlLibrary.Annotations;
using Protocol.Data;

namespace IdsRemoteServiceControlLibrary
{
    /// <summary>
    ///     Interaction logic for StaffComboBox.xaml
    /// </summary>
    public class StaffLookup : Staff
    {
        public StaffLookup()
        {
        }

        public StaffLookup( Staff s ) : base( s )
        {
        }

        internal int MyIndex;

        public bool EvenRow => ( MyIndex & 1 ) == 0;
        public Staff Staff => this;

        private string[] _SearchText;

        public string[] SearchText => _SearchText ?? ( _SearchText = $"{StaffId} {FirstName} {LastName}".ToUpper().Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) );
    }

    internal class StaffLookupList : List<StaffLookup>
    {
        internal StaffLookupList( StaffList sl )
        {
            if( sl != null )
            {
                var C = sl.Count;
                for( var Ndx = 0; Ndx < C; Ndx++ )
                    Add( new StaffLookup( sl[ Ndx ] ) { MyIndex = Ndx } );
            }
        }
    }

    internal class StaffBoolToColourConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is bool Grey )
                return Grey ? StaffComboBox.Odd : StaffComboBox.Even;

            return Brushes.White;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    public partial class StaffComboBox : INotifyPropertyChanged
    {
        internal static Brush Odd;
        internal static Brush Even;

        private readonly object LockObject = new object();

        private StaffLookupList _Staff;
        private TextBox TextBox;

        private readonly bool DesignMode;

        public StaffComboBox()
        {
            DesignMode = DesignerProperties.GetIsInDesignMode( this );

            if( !DesignMode )
            {
                Task.Run( () =>
                {
                    lock( LockObject )
                    {
                        var Brushes = Colours.GetComboBoxBrushes();
                        Odd = Brushes.Odd;
                        Even = Brushes.Even;
                    }
                } );

                Task.Run( () =>
                {
                    var Srv = Globals.Service;

                    var S = Srv.GetStaff();
                    lock( LockObject )
                    {
                        _Staff = new StaffLookupList( S );
                        Staff = _Staff;
                    }
                } );
            }
            InitializeComponent();
            AddButton.Visibility = EnableNew ? Visibility.Visible : Visibility.Collapsed;
            EnableAdd( ( false, true ) );
        }

        internal StaffLookupList Staff
        {
            get
            {
                while( !DesignMode )
                {
                    StaffLookupList S;
                    lock( LockObject )
                        S = _Staff;
                    if( S == null )
                        Thread.Sleep( 100 );
                    else
                        break;
                }
                return (StaffLookupList)GetValue( StaffProperty );
            }
            set
            {
                Dispatcher.Invoke( () =>
                {
                    SetValue( StaffProperty, value );
                    LookupCombo.UpdateLayout();
                } );
            }
        }

        // Using a DependencyProperty as the backing store for Staff.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StaffProperty =
            DependencyProperty.Register( "Staff", typeof( StaffLookupList ), typeof( StaffComboBox ) );

        public int MaxDropDownHeight
        {
            get => (int?)GetValue( MaxDropDownHeightProperty ) ?? 0;
            set
            {
                SetValue( MaxDropDownHeightProperty, value );
                OnPropertyChanged( nameof( MaxDropDownHeight ) );
            }
        }

        // Using a DependencyProperty as the backing store for MaxDropDownHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxDropDownHeightProperty =
            DependencyProperty.Register( "MaxDropDownHeight", typeof( int ), typeof( StaffComboBox ), new PropertyMetadata( 300 ) );

        public Staff SelectedStaff
        {
            get => (Staff)GetValue( SelectedStaffProperty );
            set
            {
                SetValue( SelectedStaffProperty, value );
                OnPropertyChanged( nameof( SelectedStaff ) );
            }
        }

        // Using a DependencyProperty as the backing store for SelectedStaff.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedStaffProperty =
            DependencyProperty.Register( "SelectedStaff", typeof( Staff ), typeof( StaffComboBox ) );

        // Using a DependencyProperty as the backing store for AddVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddVisibilityProperty =
            DependencyProperty.Register( "AddVisibility", typeof( Visibility ), typeof( StaffComboBox ), new PropertyMetadata( Visibility.Collapsed ) );

        public bool EnableNew
        {
            get => (bool)GetValue( EnableNewProperty );
            set
            {
                Dispatcher.Invoke( () =>
                {
                    AddButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                    SetValue( EnableNewProperty, value );
                    OnPropertyChanged( nameof( EnableNew ) );
                } );
            }
        }

        // Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableNewProperty =
            DependencyProperty.Register( "EnableNew", typeof( bool ), typeof( StaffComboBox ), new PropertyMetadata( false, EnableNewValueChanged ) );

        private static void EnableNewValueChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            if( d is StaffComboBox Combo && e.NewValue is bool Visible )
                Combo.AddButton.Visibility = Visible ? Visibility.Visible : Visibility.Collapsed;
        }

        private void LookupCombo_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if( LookupCombo.SelectedItem is Staff S )
            {
                SelectedStaff = S;
                Dispatcher.InvokeAsync( () => { LookupCombo.Text = S.StaffId; } );
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        public bool IsNew { get; private set; }

        public string StaffId
        {
            get
            {
                var Id = "";
                Dispatcher.Invoke( () => { Id = LookupCombo.Text.Trim(); } );

                return Id;
            }
        }

        private void EnableAdd( (bool HasPartialMatch, bool HasCompleteMatch) matches )
        {
            var Enab = !matches.HasCompleteMatch;
            IsNew = Enab;
            AddButton.IsEnabled = Enab;
            AddImage.IsEnabled = Enab;
            if( !matches.HasPartialMatch )
                LookupCombo.IsDropDownOpen = false;
        }

        private void LookupCombo_KeyUp( object sender, KeyEventArgs e )
        {
            Controls.ComboxSearch( LookupCombo, ref TextBox, i =>
            {
                var S = _Staff[ i ];
                return ( FullText: S.StaffId, SearchText: S.SearchText );
            }, EnableAdd );
        }

        public delegate void OnNewStaffEvent( string newStaffId );

        public event OnNewStaffEvent OnNewStaff;

        private void AddButton_Click( object sender, RoutedEventArgs e )
        {
            LookupCombo.IsDropDownOpen = false;
            SelectedStaff = null;
            OnNewStaff?.Invoke( LookupCombo.Text.Trim() );
        }

        private void LookupCombo_DropDownOpened( object sender, EventArgs e )
        {
            LookupCombo.IsDropDownOpen = true;
        }

        private void LookupCombo_MouseMove( object sender, MouseEventArgs e )
        {
            e.Handled = true;
        }
    }
}