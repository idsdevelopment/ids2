﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Utils;
using Newtonsoft.Json;
using Utils;
using File = System.IO.File;

namespace IdsControlLibraryV2.Ribbon
{
	public class Ribbon : DockPanel
	{
		private const string TOOLBARS = "ToolBars",
		                     SETTINGS_FILE_EXTENSION = "Settings";

		public class ToolBarSetting
		{
			public int Band { get; set; }
			public int BandIndex { get; set; }
		}


		public class Settings
		{
			public List<ToolBarSetting> TBarSettings { get; set; } = new List<ToolBarSetting>();
			public bool IsMenuVisible { get; set; }
		}


		private static string _ProductName;
		public Collection<ToolBar> ToolBars => ToolBarTray.ToolBars;

		public List<MenuItem> CollapsedMenuItems { get; set; } = new List<MenuItem>();

		private static string ProductName
		{
			get
			{
				if( string.IsNullOrWhiteSpace( _ProductName ) )
					_ProductName = ( Assembly.GetEntryAssembly().GetCustomAttributes( typeof( AssemblyProductAttribute ) ).SingleOrDefault() as AssemblyProductAttribute )?.Product;

				return _ProductName;
			}
		}

		private readonly ToolBarTray ToolBarTray;
		private readonly ToolBar BaseToolBar;
		private readonly Menu Menu;
		private readonly bool IsInDesignMode;
		private bool MenuLoaded;
		private Page Page;

		private bool ToolBarLayoutUpdated,
		             DidUnload;


		private static void DoMenuVisibility( Ribbon r, bool vis )
		{
			if( vis )
			{
				r.BaseToolBar.Visibility = Visibility.Collapsed;
				r.Menu.Visibility = Visibility.Visible;
			}
			else
			{
				r.BaseToolBar.Visibility = Visibility.Visible;
				r.Menu.Visibility = Visibility.Collapsed;
			}
		}

		private static void MenuVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is Ribbon R && e.NewValue is bool Vis )
				DoMenuVisibility( R, Vis );
		}

		private void SaveSettings()
		{
			if( !IsInDesignMode )
			{
				try
				{
					var Settings = new Settings
					               {
						               IsMenuVisible = MenuVisible
					               };

					foreach( var ToolBar in ToolBarTray.ToolBars )
					{
						Settings.TBarSettings.Add( new ToolBarSetting
						                           {
							                           Band = ToolBar.Band,
							                           BandIndex = ToolBar.BandIndex
						                           } );
					}

					var SerializedObject = JsonConvert.SerializeObject( Settings );
					SerializedObject = Encryption.Encrypt( SerializedObject );
					File.WriteAllText( MakeFileName(), SerializedObject );
				}
				catch
				{
				}
			}
		}

		private void LoadSettings()
		{
			if( !IsInDesignMode )
			{
				try
				{
					var FileName = MakeFileName();
					if( File.Exists( FileName ) )
					{
						var SerializedObject = File.ReadAllText( FileName );
						SerializedObject = Encryption.Decrypt( SerializedObject );
						var Settings = JsonConvert.DeserializeObject<Settings>( SerializedObject );
						if( !( Settings is null ) )
						{
							MenuVisible = Settings.IsMenuVisible;

							var ToolBars = ToolBarTray.ToolBars;
							var Count = Math.Min( ToolBars.Count, Settings.TBarSettings.Count );

							for( var I = 0; I < Count; I++ )
							{
								var Setting = Settings.TBarSettings[ I ];
								var TBar = ToolBars[ I ];
								TBar.Band = Setting.Band;
								TBar.BandIndex = Setting.BandIndex;
							}
						}
					}
				}
				catch
				{
				}
			}
		}

		private static void ShowMenuToolTipPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is Ribbon R )
				R.BaseToolBar.ToolTip = e.NewValue;
		}

		private static void ShowToolBarToolPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is Ribbon R )
				R.Menu.ToolTip = e.NewValue;
		}

		protected string MakeFileName()
		{
			var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
			var BaseFolder = $"{AppData}\\{ProductName}\\{TOOLBARS}".TrimEnd( '\\' );

			if( !Directory.Exists( BaseFolder ) )
				Directory.CreateDirectory( BaseFolder );

			return $"{BaseFolder}\\{LayoutSaveProfile}.{SETTINGS_FILE_EXTENSION}";
		}


		public Ribbon()
		{
			var Prop = DesignerProperties.IsInDesignModeProperty;
			IsInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty( Prop, typeof( FrameworkElement ) ).Metadata.DefaultValue;

			HorizontalAlignment = HorizontalAlignment.Stretch;
			VerticalAlignment = VerticalAlignment.Stretch;

			BaseToolBar = new ToolBar
			              {
				              Background = Brushes.White
			              };

			BaseToolBar.Items.Add( ToolBarTray = new ToolBarTray
			                                     {
				                                     Background = Brushes.White
			                                     } );

			ToolBarTray.LayoutUpdated += ( sender, args ) =>
			                             {
				                             if( !DidUnload )
				                             {
					                             if( Mouse.LeftButton == MouseButtonState.Pressed )
						                             ToolBarLayoutUpdated = true;
					                             else if( ToolBarLayoutUpdated )
					                             {
						                             ToolBarLayoutUpdated = false;
						                             SaveSettings();
					                             }
				                             }
			                             };

			Menu = new Menu
			       {
				       Visibility = IsInDesignMode ? Visibility.Visible : Visibility.Collapsed,
				       HorizontalAlignment = HorizontalAlignment.Stretch,
				       Height = 20,
				       Background = Brushes.Transparent
			       };

			Menu.MouseDoubleClick += ( sender, args ) =>
			                         {
				                         MenuVisible = false;
			                         };

			SetDock( Menu, Dock.Top );
			SetDock( BaseToolBar, Dock.Top );

			Children.Add( Menu );
			Children.Add( BaseToolBar );

			Unloaded += ( sender, args ) =>
			            {
				            if( !DidUnload )
				            {
					            DidUnload = true;
					            SaveSettings();
				            }
			            };

			void DoHide( ToolBar toolbar )
			{
				if( !( toolbar is null ) && ( ToolBar.GetOverflowMode( toolbar ) != OverflowMode.Always ) )
				{
					if( toolbar.Template.FindName( "OverflowGrid", toolbar ) is FrameworkElement OverflowGrid )
						OverflowGrid.Visibility = Visibility.Collapsed;

					if( toolbar.Template.FindName( "MainPanelBorder", toolbar ) is FrameworkElement MainPanelBorder )
						MainPanelBorder.Margin = new Thickness();
				}
			}

			Initialized += ( sender, args ) =>
			               {
				               DoHide( sender as ToolBar );
			               };

			Loaded += ( sender, args ) =>
			          {
				          if( !MenuLoaded )
				          {
					          MenuLoaded = true;

					          DoMenuVisibility( this, false );

					          ToolBarTray.ToolTip = ShowMenuToolTip;
					          Menu.ToolTip = ShowToolBarToolTip;

					          foreach( var ToolBar in ToolBarTray.ToolBars )
					          {
						          ToolBar.Loaded += ( o, eventArgs ) =>
						                            {
							                            DoHide( o as ToolBar );
						                            };

						          DoHide( ToolBar );

						          var Header = ToolBar.Header;
						          if( !( Header is null ) )
						          {
							          ToolBar.Header = null;

							          var Items = new List<FrameworkElement>();

							          foreach( var Item in ToolBar.Items )
							          {
								          if( Item is FrameworkElement E )
									          Items.Add( E );
							          }

							          ToolBar.Items.Clear();
							          var Sp = new StackPanel();
							          foreach( var Item in Items )
							          {
								          Sp.Children.Add( Item );
								          Sp.Children.Add( new TextBlock
								                           {
									                           HorizontalAlignment = HorizontalAlignment.Stretch,
									                           TextAlignment = TextAlignment.Center,
									                           Text = Header.ToString()
								                           } );
							          }

							          ToolBar.Items.Add( Sp );
						          }
					          }

					          var Mi = Menu.Items;
					          foreach( var Item in CollapsedMenuItems )
						          Mi.Add( Item );

					          var MenuText = ShowToolBarText.Split( new[] { "//" }, StringSplitOptions.RemoveEmptyEntries );
					          if( MenuText.Length >= 2 )
					          {
						          var SubItem = new MenuItem
						                        {
							                        Header = MenuText[ 1 ]
						                        };

						          SubItem.Click += ( o, eventArgs ) =>
						                           {
							                           MenuVisible = false;
						                           };

						          var MainItem = new MenuItem
						                         {
							                         Header = MenuText[ 0 ]
						                         };
						          MainItem.Items.Add( SubItem );
						          Mi.Add( MainItem );
					          }

					          var P = Page = this.FindParent<Page>();
					          if( P != null )
					          {
						          P.PreviewKeyDown += ( o, kargs ) =>
						                              {
							                              if( kargs.Key == Key.F1 )
							                              {
								                              args.Handled = true;
								                              Page.IsEnabled = false;
								                              try
								                              {
									                              var Url = HelpUri;
									                              if( !string.IsNullOrEmpty( Url ) )
									                              {
										                              try
										                              {
											                              Process.Start( Url );
										                              }
										                              catch( Exception e )
										                              {
											                              Console.WriteLine( e );
										                              }
									                              }
								                              }
								                              finally
								                              {
									                              Page.IsEnabled = true;
								                              }
							                              }
						                              };
					          }

					          LoadSettings();
				          }
			          };

			BaseToolBar.MouseDoubleClick += ( sender, args ) =>
			                                {
				                                MenuVisible = true;
			                                };
			if( IsInDesignMode )
			{
				foreach( var ToolBar in ToolBarTray.ToolBars )
				{
					DoHide( ToolBar );
					ToolBar.Header = null;
				}
			}

			ShowMenuToolTip = new TextBlock
			                  {
				                  Text = "Double click to hide toolbar"
			                  };

			ShowToolBarToolTip = new TextBlock
			                     {
				                     Text = "Double click to show toolbar"
			                     };
		}

		[Category( "Options" )]
		public string HelpUri
		{
			get => (string)GetValue( HelpUriProperty );
			set => SetValue( HelpUriProperty, value );
		}

		// Using a DependencyProperty as the backing store for HelpUri.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty HelpUriProperty =
			DependencyProperty.Register( nameof( HelpUri ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "https://idsservices.atlassian.net/wiki/display/IKB" ) );


		public string LayoutSaveProfile
		{
			get
			{
				var Temp = (string)GetValue( LayoutSaveProfileProperty );
				if( string.IsNullOrWhiteSpace( Temp ) )
					Temp = Page?.Title ?? "Ribbon";

				return Temp;
			}
			set => SetValue( LayoutSaveProfileProperty, value );
		}

		// Using a DependencyProperty as the backing store for LayoutSaveProfile.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty LayoutSaveProfileProperty =
			DependencyProperty.Register( nameof( LayoutSaveProfile ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "" ) );


		[Category( "Options" )]
		public bool MenuVisible
		{
			get => (bool)GetValue( MenuVisibleProperty );
			set => SetValue( MenuVisibleProperty, value );
		}

		// Using a DependencyProperty as the backing store for MenuVisible.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MenuVisibleProperty =
			DependencyProperty.Register( nameof( MenuVisible ), typeof( bool ), typeof( Ribbon ), new FrameworkPropertyMetadata( false, MenuVisiblePropertyChangedCallback ) );


		public object ShowMenuToolTip
		{
			get => GetValue( ShowMenuToolTipProperty );
			set => SetValue( ShowMenuToolTipProperty, value );
		}

		// Using a DependencyProperty as the backing store for ShowMenuToolTip.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowMenuToolTipProperty =
			DependencyProperty.Register( nameof( ShowMenuToolTip ), typeof( object ), typeof( Ribbon ), new FrameworkPropertyMetadata( null, ShowMenuToolTipPropertyChangedCallback ) );

		[Category( "Options" )]
		public string ShowToolBarText
		{
			get => (string)GetValue( ShowToolBarTextProperty );
			set => SetValue( ShowToolBarTextProperty, value );
		}

		// Using a DependencyProperty as the backing store for ShowToolBarText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowToolBarTextProperty =
			DependencyProperty.Register( nameof( ShowToolBarText ), typeof( string ), typeof( Ribbon ), new FrameworkPropertyMetadata( "View//Show Toolbar" ) );


		public object ShowToolBarToolTip
		{
			get => GetValue( ShowToolBarToolTipProperty );
			set => SetValue( ShowToolBarToolTipProperty, value );
		}

		// Using a DependencyProperty as the backing store for ShowMenuToolTip.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowToolBarToolTipProperty =
			DependencyProperty.Register( nameof( ShowToolBarToolTip ), typeof( object ), typeof( Ribbon ), new FrameworkPropertyMetadata( null, ShowToolBarToolPropertyChangedCallback ) );
	}
}