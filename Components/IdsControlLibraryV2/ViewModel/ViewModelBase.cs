#if XAMARIN
	using Xamarin.Forms;
#else
using System.Windows;
using System.Windows.Threading;
#endif
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utils;
using File = System.IO.File;


namespace IdsControlLibraryV2.ViewModel
{
	public class ViewModelBase : DynamicObject, INotifyPropertyChanged
	{
		public bool CanEdit
		{
			get { return Get( () => CanEdit, false ); }
			private set { Set( () => CanEdit, value ); }
		}


		public bool CanDelete
		{
			get { return Get( () => CanDelete, false ); }
			private set { Set( () => CanDelete, value ); }
		}


		public bool CanCreate
		{
			get { return Get( () => CanCreate, false ); }
			set { Set( () => CanCreate, value ); }
		}


		public void SetSecurity( bool canEdit, bool canDelete, bool canCreate )
		{
			CanEdit   = canEdit;
			CanDelete = canDelete;
			CanCreate = canCreate;
		}

		public virtual void OnArgumentChange( object arg )
		{
		}

		public delegate void PropertyChangedEventBoundEventHandler();

		[AttributeUsage( AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true )]
		protected class DependsUponAttribute : Attribute
		{
			public string DependencyName { get; }
			public int Delay { get; set; }

			public bool VerifyStaticExistence { get; set; }

			public DependsUponAttribute( string propertyName )
			{
				DependencyName = propertyName;
			}
		}

		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon150Attribute : DependsUponAttribute
		{
			public DependsUpon150Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 150;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon250Attribute : DependsUponAttribute
		{
			public DependsUpon250Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 250;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon350Attribute : DependsUponAttribute
		{
			public DependsUpon350Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 350;
			}
		}


		// ReSharper disable once UnusedMember.Global
		protected class DependsUpon500Attribute : DependsUponAttribute
		{
			public DependsUpon500Attribute( string propertyName ) : base( propertyName )
			{
				Delay = 500;
			}
		}

		[AttributeUsage( AttributeTargets.Property )]
		protected class SettingAttribute : Attribute
		{
		}

		private const string EXECUTE_PREFIX = "Execute_";
		private const string CAN_EXECUTE_PREFIX = "CanExecute_";
		protected const string SETTINGS_FILE_EXTENSION = "Settings";

		private readonly Dictionary<string, Timer> ActiveDelayMap;
		private readonly IDictionary<string, List<string>> CommandMap;

		protected readonly Dictionary<string, ICommand> Commands = new Dictionary<string, ICommand>();
		private readonly IDictionary<string, int> DelayMap;
		private readonly IDictionary<string, List<string>> MethodMap;
		private readonly IDictionary<string, List<string>> PropertyMap;
		private readonly IDictionary<string, PropertyInfo> SettingsMap = new Dictionary<string, PropertyInfo>();
		private readonly Dictionary<string, object> Values = new Dictionary<string, object>();

		private IEnumerable<MemberInfo> _Methods;
		private string BaseFolder;

		private JObject Settings;
		protected string SettingsFolder = "";
		private bool SettingsSaved;

		protected virtual void OnInitialised()
		{
		}

		static ViewModelBase()
		{
#if !XAMARIN
			var Prop = DesignerProperties.IsInDesignModeProperty;
			IsInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty( Prop, typeof( FrameworkElement ) ).Metadata.DefaultValue;
#else
			IsInDesignMode = DesignMode.IsDesignModeEnabled;
#endif
		}


		public ViewModelBase() : this( true )
		{
		}

		private ViewModelBase( bool loadDefault )
		{
			PropertyMap = MapDependencies<DependsUponAttribute>( () => GetType().GetProperties() );

			MethodMap = MapDependencies<DependsUponAttribute>( () =>
			                                                   {
				                                                   return Methods.Where( method => !method.Name.StartsWith( CAN_EXECUTE_PREFIX ) );
			                                                   } );

			CommandMap = MapDependencies<DependsUponAttribute>( () =>
			                                                    {
				                                                    return Methods.Where( method => method.Name.StartsWith( CAN_EXECUTE_PREFIX ) );
			                                                    } );

			DelayMap       = MapDelays();
			ActiveDelayMap = new Dictionary<string, Timer>();

			CreateCommands();

			var PropertiesWithSettingsAttribute = from PropertyInfo in GetType().GetProperties()
			                                      let Attr = PropertyInfo.GetCustomAttributes( typeof( SettingAttribute ), true )
			                                      where Attr.Length == 1
			                                      select PropertyInfo;

			foreach( var PropertyInfo in PropertiesWithSettingsAttribute )
				SettingsMap.Add( PropertyInfo.Name, PropertyInfo );

			VerifyDependencies();

			if( loadDefault )
				InitSettings( Globals.Settings.SaveFolder );
		}

		public ViewModelBase( string settingsFolder ) : this( false )
		{
			InitSettings( settingsFolder );
		}

		protected static string ProductName { get; private set; }

#if XAMARIN
		public static void Dispatcher( Action a )
		{
			Device.BeginInvokeOnMainThread( a );
		}

#else
		public static Dispatcher Dispatcher => Application.Current.Dispatcher;
#endif

		public static bool IsInDesignMode { get; }

		private IEnumerable<MemberInfo> Methods => _Methods ??= GetType().GetMethods();

		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				_PropertyChanged += value;
				PropertyChangedEventBound?.Invoke();
			}
			remove => _PropertyChanged -= value;
		}

		private void InitSettings( string settingsFolder )
		{
			SettingsFolder = settingsFolder.Trim().ToLower();
			Settings       = new JObject();

			if( string.IsNullOrEmpty( ProductName ) )
#if !XAMARIN
				ProductName = ( Assembly.GetEntryAssembly().GetCustomAttributes( typeof( AssemblyProductAttribute ) ).SingleOrDefault() as AssemblyProductAttribute )?.Product;
#else
				ProductName = "DroidAz";
#endif
			LoadSettings();
			OnInitialised();
		}


		~ViewModelBase()
		{
			if( !SettingsSaved )
				SaveSettings();
		}


		protected string MakeFileName( Type type )
		{
			var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
			BaseFolder = $"{AppData}\\{ProductName}\\{SettingsFolder}".TrimEnd( '\\' );

			if( !Directory.Exists( BaseFolder ) )
				Directory.CreateDirectory( BaseFolder );

			return $"{BaseFolder}\\{type.Name}.{SETTINGS_FILE_EXTENSION}";
		}

		protected string MakeFileName()
		{
			return MakeFileName( GetType() );
		}


		protected void SaveSettings()
		{
			if( Settings != null )
			{
				try
				{
					Settings = new JObject();

					foreach( var PropertyInfo in SettingsMap )
						Settings.Add( new JProperty( PropertyInfo.Key, PropertyInfo.Value.GetValue( this ) ) );

					var SerializedObject = JsonConvert.SerializeObject( Settings );
					SerializedObject = Encryption.Encrypt( SerializedObject );
					File.WriteAllText( MakeFileName(), SerializedObject );
					SettingsSaved = true;
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
			}
		}

		protected void LoadSettings()
		{
			if( Settings != null )
			{
				try
				{
					var FileName = MakeFileName();

					if( File.Exists( FileName ) )
					{
						var SerializedObject = File.ReadAllText( FileName );
						SerializedObject = Encryption.Decrypt( SerializedObject );
						Settings         = JsonConvert.DeserializeObject<JObject>( SerializedObject );

						foreach( var Setting in Settings )
						{
							if( SettingsMap.TryGetValue( Setting.Key, out var Info ) )
							{
								dynamic Value = Convert.ChangeType( Setting.Value, Info.PropertyType );
								Info.SetValue( this, Value );
							}
						}
					}
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
			}
		}


		protected static string CallerName( [CallerMemberName] string name = null )
		{
			return name ?? "";
		}

		protected T Get<T>( string name )
		{
			return Get( name, default( T ) );
		}

		protected T Get<T>( string name, T defaultValue )
		{
			if( Values.TryGetValue( name, out var Value ) )
				return (T)Value;

			return defaultValue;
		}

		protected T Get<T>( string name, Func<T> initialValue )
		{
			if( Values.TryGetValue( name, out var Value ) )
				return (T)Value;

			Set( name, initialValue() );

			return Get<T>( name );
		}

		protected T Get<T>( Expression<Func<T>> expression )
		{
			return Get<T>( PropertyName( expression ) );
		}

		protected T Get<T>( Expression<Func<T>> expression, T defaultValue )
		{
			return Get( PropertyName( expression ), defaultValue );
		}

		protected T Get<T>( Expression<Func<T>> expression, Func<T> initialValue )
		{
			return Get( PropertyName( expression ), initialValue );
		}

		public void Set<T>( string name, T value )
		{
			if( Values.TryGetValue( name, out var Value ) )
			{
				if( Value is null )
				{
					if( value == null )
						return;
				}
				else
				{
					if( Value.Equals( value ) )
						return;
				}

				Values[ name ] = value;
			}
			else
				Values.Add( name, value );

			RaisePropertyChanged( name );
		}

		protected void RaisePropertyChanged( string name )
		{
			_PropertyChanged.Raise( this, name );

			if( PropertyMap.TryGetValue( name, out var Map ) )
				Map.Each( RaisePropertyChanged );

			ExecuteDependentMethods( name );
			FireChangesOnDependentCommands( name );
		}

		private void ExecuteDependentMethods( string name )
		{
			if( MethodMap.TryGetValue( name, out var Map ) )
				Map.Each( method => ExecuteMethod( method, name ) );
		}

		private void FireChangesOnDependentCommands( string name )
		{
			if( CommandMap.TryGetValue( name, out var Map ) )
				Map.Each( RaiseCanExecuteChangedEvent );
		}

		protected void Set<T>( Expression<Func<T>> expression, T value )
		{
			Set( PropertyName( expression ), value );
		}

		private static string PropertyName<T>( Expression<Func<T>> expression )
		{
			if( !( expression.Body is MemberExpression MemberExpression ) )
				throw new ArgumentException( "expression must be a property expression" );

			return MemberExpression.Member.Name;
		}

		public override bool TryGetMember( GetMemberBinder binder, out object result )
		{
			result = Get<object>( binder.Name );

			return ( result != null ) || base.TryGetMember( binder, out result );
		}

		public override bool TrySetMember( SetMemberBinder binder, object value )
		{
			var Result = base.TrySetMember( binder, value );

			if( Result )
				return true;

			Set( binder.Name, value );

			return true;
		}

		private event PropertyChangedEventHandler _PropertyChanged;

		public event PropertyChangedEventBoundEventHandler PropertyChangedEventBound;

		private void CreateCommands()
		{
			var CNames = from Method in Methods
			             where Method.Name.StartsWith( EXECUTE_PREFIX )
			             select Method.Name.StripLeft( EXECUTE_PREFIX.Length );

			foreach( var Name in CNames )
			{
				var Command = new DelegateCommand<object>( x => ExecuteCommand( Name, x ), x => CanExecuteCommand( Name, x ) );
				Commands.Add( Name, Command );
				Set( Name, Command );
			}
		}

		private void ExecuteCommand( string name, object parameter )
		{
			var MethodInfo = GetType().GetMethod( EXECUTE_PREFIX + name );

			if( MethodInfo == null ) return;

			MethodInfo.Invoke( this, MethodInfo.GetParameters().Length == 1 ? new[] { parameter } : null );
		}

		private bool CanExecuteCommand( string name, object parameter )
		{
			var MethodInfo = GetType().GetMethod( CAN_EXECUTE_PREFIX + name );

			if( MethodInfo == null ) return true;

			return (bool)MethodInfo.Invoke( this, MethodInfo.GetParameters().Length == 1 ? new[] { parameter } : null );
		}

		protected void RaiseCanExecuteChangedEvent( string canExecuteName )
		{
			var CommandName = canExecuteName.StripLeft( CAN_EXECUTE_PREFIX.Length );
			var Command     = Get<DelegateCommand<object>>( CommandName );
			Command?.RaiseCanExecuteChanged();
		}

		private static IDictionary<string, List<string>> MapDependencies<T>( Func<IEnumerable<MemberInfo>> getInfo ) where T : DependsUponAttribute
		{
			var RetVal = new Dictionary<string, List<string>>();

			foreach( var Info in getInfo() )
			{
				var Name    = Info.Name;
				var DepName = Info.GetCustomAttributes( typeof( T ), true ).Cast<T>().Select( a => a.DependencyName );

				foreach( var DName in DepName.Where( dName => !string.IsNullOrEmpty( dName ) ) )
				{
					if( !RetVal.TryGetValue( DName, out var DepList ) )
						RetVal.Add( DName, DepList = new List<string>() );

					DepList.Add( Name );
				}
			}

			return RetVal;
		}

		private IDictionary<string, int> MapDelays()
		{
			var RetVal = new Dictionary<string, int>();

			foreach( var Method in GetType().GetMethods() )
			{
				if( !Method.IsSpecialName && Method.IsPublic )
				{
					var Name = Method.Name;

					var Delay = ( from DependsUponAttribute Attr in Method.GetCustomAttributes( typeof( DependsUponAttribute ), true )
					              select Attr.Delay ).Concat( new[] { 0 } )
					                                 .Max();

					if( Delay > 0 )
						RetVal.Add( Name, Delay );
				}
			}

			return RetVal;
		}

		private readonly HashSet<string> DisableEventMap = new HashSet<string>();
		private readonly HashSet<string> DisableDelayMap = new HashSet<string>();

		protected void DisableEvent( string methodName )
		{
			DisableEventMap.Add( methodName );
		}

		protected void EnableEvent( string methodName )
		{
			DisableEventMap.Remove( methodName );
		}

		protected void DisableDelay( string methodName )
		{
			DisableDelayMap.Add( methodName );
		}

		protected void EnableDelay( string methodName )
		{
			DisableDelayMap.Remove( methodName );
		}

		private readonly Dictionary<string, HashSet<string>> WaitForMap = new Dictionary<string, HashSet<string>>();

		protected struct WaitForArg
		{
			public string MethodName;
			public string[] PropertyNames;
		}

		protected void WaitFor( WaitForArg[] waitForArgs )
		{
			foreach( var WaitForArg in waitForArgs )
			{
				if( !WaitForMap.TryGetValue( WaitForArg.MethodName, out var PropertyNames ) )
				{
					PropertyNames = new HashSet<string>();
					WaitForMap.Add( WaitForArg.MethodName, PropertyNames );
				}

				foreach( var PropertyName in WaitForArg.PropertyNames )
					PropertyNames.Add( PropertyName );
			}
		}

		private void ExecuteMethod( string name, string propertyName )
		{
			if( !DisableEventMap.Contains( name ) )
			{
				if( WaitForMap.TryGetValue( name, out var PropertyNames ) )
				{
					if( PropertyNames.Contains( propertyName ) )
					{
						PropertyNames.Remove( propertyName );

						if( PropertyNames.Count == 0 )
							WaitForMap.Remove( name );
					}

					return;
				}

				var MemberInfo = GetType().GetMethod( name );

				if( !( MemberInfo is null ) )
				{
					lock( ActiveDelayMap )
					{
						// Has Debug args?
						var C    = MemberInfo.GetParameters().Length;
						var Args = new object[ C ];

						if( C > 0 )
							Args[ 0 ] = propertyName;

						if( !DisableDelayMap.Contains( name ) && DelayMap.TryGetValue( name, out var Delay ) && ( Delay > 0 ) )
						{
							// Always recreate Timer for debug info
							if( ActiveDelayMap.TryGetValue( name, out var Timer ) )
							{
								Timer.Stop();

								if( C > 0 )
								{
									Timer.Dispose();
									ActiveDelayMap.Remove( name );
									Timer = null;
								}
							}

							if( Timer is null )
							{
								Timer = new Timer( Delay ) { AutoReset = false };
								ActiveDelayMap.Add( name, Timer );

								Timer.Elapsed += ( sender, args ) =>
								                 {
#if !XAMARIN
									                 Dispatcher.InvokeAsync( () =>
									                                         {
										                                         MemberInfo.Invoke( this, Args );
									                                         } );
#else
									                 Dispatcher( () =>
									                             {
										                             MemberInfo.Invoke( this, Args );
									                             } );
#endif
								                 };
							}

							Timer.Start();
						}
						else
							MemberInfo.Invoke( this, Args );
					}
				}
			}
			else
			{
				lock( ActiveDelayMap )
				{
					if( ActiveDelayMap.TryGetValue( name, out var Timer ) )
						Timer.Stop();
				}
			}
		}

		private void VerifyDependencies()
		{
			var Properties = GetType().GetProperties();

			var PropertyNames = Methods.Union( Properties )
			                           .SelectMany( method => method.GetCustomAttributes( typeof( DependsUponAttribute ), true ).Cast<DependsUponAttribute>() )
			                           .Where( attribute => attribute.VerifyStaticExistence )
			                           .Select( attribute => attribute.DependencyName );

			PropertyNames.Each( VerifyDependencies );
		}

		private void VerifyDependencies( string propertyName )
		{
			var Property = GetType().GetProperty( propertyName );

			if( Property == null )
				throw new ArgumentException( "DependsUpon Property Does Not Exist: " + propertyName );
		}

		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			_PropertyChanged?.Invoke( this, e );
		}
	}
}