﻿using System;
using System.Linq;
using Unity;

namespace IdsControlLibraryV2.ViewModel
{
	public interface IRandomNumberGenerator
	{
		int Get();
	}

	public class ViewModelWithDependancy : ViewModelBase
	{
		public ViewModelWithDependancy( IRandomNumberGenerator generator )
		{
			Value = generator.Get();
		}

		public int Value { get; }
	}

	public class RandomNumberGenerator : IRandomNumberGenerator
	{
		public int Get()
		{
			return new Random().Next();
		}
	}

	public class UnityViewModelResolver : IViewModelResolver
	{
		private readonly UnityContainer Container = new UnityContainer();

		public UnityViewModelResolver()
		{
			Container.RegisterType<IRandomNumberGenerator, RandomNumberGenerator>();
		}

		public object Resolve( string viewModelNameName )
		{
			var FoundType = GetType().Assembly.GetTypes().FirstOrDefault( type => type.Name == viewModelNameName );
			return FoundType == null ? null : Container.Resolve( FoundType );
		}
	}
}