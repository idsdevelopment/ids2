﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Input;
using IdsControlLibraryV2.Utils;
using IdsControlLibraryV2.Window;
using Timer = System.Timers.Timer;

namespace IdsControlLibraryV2.Combos
{
	public class BoolToComboSelectedBackgroundConvertor : IValueConverter
	{
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if( value is bool Selected )
			{
				if( parameter is LookupComboBoxBase Combo )
					return Selected ? Combo.SelectedItemBackground : Combo.Background;
			}

			return Brushes.Red;
		}

		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}

	public class LookupComboBoxBase : Border
	{
		public delegate void ItemSelectedEvent( LookupItemBase item );

		public delegate void SecondaryButtonClickEvent();

		private const int DEFAULT_SEARCH_INTERVAL = 150;
		private const double DEFAULT_TEXT_WIDTH = 100;

		private readonly TextBox Input;

		private readonly ChildWindow ListVieWindow;
		private readonly Timer SearchTimer = new Timer();

		private readonly Button SecondaryButton, DropDownButton;

		// ReSharper disable once InconsistentNaming
		private ListView _CurrentListView;


		private bool DontOpenDropdown;


		private bool JustSetText;
		private System.Threading.Timer Timer;

		private ALookupComboBoxModelBase ViewModel;
		private VirtualizingStackPanel VPanel;

		public LookupComboBoxBase()
		{
			ListVieWindow = new ChildWindow
			                {
				                Top = 0,
				                Left = 0,
				                SizeToContent = SizeToContent.WidthAndHeight,
				                Background = Brushes.Red,
				                Padding = new Thickness(),
				                Owner = this
			                };


			LostFocus += OnLostFocus;
			ListVieWindow.LostKeyboardFocus += OnLostFocus;

			GotFocus += ( sender, args ) =>
			            {
				            ViewModel.SearchText = Input.Text;
			            };

			Loaded += ( sender, args ) =>
			          {
				          SetUpWindow();

				          if( DataContext is ALookupComboBoxModelBase Model )
				          {
					          ViewModel = Model;
					          Model.WhenSelectedItemChanges = item =>
					                                          {
						                                          ListView?.ScrollIntoView( item );
					                                          };

					          ViewModel.CollectionChanged = () =>
					                                        {
						                                        Dispatcher.Invoke( () =>
						                                                           {
							                                                           if( !( CurrentListView is null ) )
								                                                           CurrentListView.ItemsSource = ViewModel.GetItems();

							                                                           if( VPanel == null )
							                                                           {
								                                                           VPanel = ListView.FindChild<VirtualizingStackPanel>();
								                                                           if( VPanel != null )
								                                                           {
									                                                           VPanel.FindParent<ScrollViewer>().ScrollChanged += ( o, eventArgs ) =>
									                                                                                                              {
										                                                                                                              var VChange = eventArgs.VerticalChange;
										                                                                                                              if( VChange != 0 )
										                                                                                                              {
											                                                                                                              ViewModel.ScrollPositionChanged( eventArgs.ViewportHeight, eventArgs.VerticalChange,
											                                                                                                                                               eventArgs.VerticalOffset );
										                                                                                                              }
									                                                                                                              };
								                                                           }
							                                                           }
						                                                           } );
					                                        };

					          ViewModel.ListView = CurrentListView;
					          CurrentListView.SelectionChanged += ( o, eventArgs ) =>
					                                              {
						                                              eventArgs.Handled = true;
						                                              var Items = eventArgs.AddedItems;
						                                              if( ( Items.Count > 0 ) && Items[ 0 ] is LookupItemBase Li )
						                                              {
							                                              SelectedItem = Li.GetItem();
							                                              JustSetText = true;
							                                              try
							                                              {
								                                              Text = ViewModel.ItemKeyAsString( Li );
								                                              ItemSelected?.Invoke( Li );
							                                              }
							                                              finally
							                                              {
								                                              JustSetText = false;
							                                              }
						                                              }
					                                              };
				          }
			          };

			BorderThickness = new Thickness( 1, 1, 1, 1 );
			Padding = new Thickness();

			HorizontalAlignment = HorizontalAlignment.Left;

			SearchTimer.Interval = DEFAULT_SEARCH_INTERVAL;
			SearchTimer.AutoReset = false;
			SearchTimer.Elapsed += ( sender, args ) =>
			                       {
				                       Dispatcher.Invoke( () =>
				                                          {
					                                          var Txt = Input.Text;
					                                          ViewModel.SearchText = Txt;
					                                          if( !DontOpenDropdown )
					                                          {
						                                          if( AutoDropDown && !IsDropDownVisible )
							                                          IsDropDownVisible = true;
					                                          }
					                                          else
						                                          DontOpenDropdown = false;
				                                          } );
			                       };

			var Inp = Input = new TextBox
			                  {
				                  MinWidth = DEFAULT_TEXT_WIDTH,
				                  BorderThickness = new Thickness()
			                  };

			Inp.GotFocus += ( sender, args ) =>
			                {
				                if( sender is TextBox Tb )
				                {
					                Timer = new System.Threading.Timer( state =>
					                                                    {
						                                                    Dispatcher.Invoke( () =>
						                                                                       {
							                                                                       Tb.SelectionStart = 0;
							                                                                       Tb.SelectionLength = Tb.Text.Length;
						                                                                       } );
						                                                    // ReSharper disable once AccessToModifiedClosure
						                                                    Timer?.Dispose();
						                                                    Timer = null;
					                                                    }, null, 10, Timeout.Infinite );
				                }
			                };

			Inp.KeyUp += ( sender, args ) =>
			             {
				             SearchTimer.Stop();
				             SearchTimer.Start();
			             };

			KeyUp += async ( sender, args ) =>
			         {
				         var Key = args.Key;

				         void DoSelection( LookupItemBase item )
				         {
					         Dispatcher.Invoke( () =>
					                            {
						                            ViewModel.SetSelectItem( item );
						                            if( !ListViewHasFocus )
							                            ListView.Focus();
						                            SendKeys.Send( Key );
					                            } );
				         }

				         switch( Key )
				         {
				         case Key.Home:
					         SearchTimer.Stop();
					         ViewModel.First( async () =>
					                          {
						                          DoSelection( await ViewModel.GetLastItem() );
					                          } );
					         break;

				         case Key.End:
					         SearchTimer.Stop();
					         ViewModel.Last( async () =>
					                         {
						                         DoSelection( await ViewModel.GetLastItem() );
					                         } );
					         break;

				         case Key.PageDown:
				         case Key.PageUp:
				         case Key.Down:
				         case Key.Up:
					         break;

				         case Key.Enter:
				         case Key.Tab:
					         if( IsDropDownVisible )
						         IsDropDownVisible = false;
					         var Txt = Input.Text;
					         var Item = await ViewModel.GetItem( Txt );
					         if( !( Item is null ) )
					         {
						         JustSetText = true;
						         try
						         {
							         Text = Txt;
							         ViewModel.SetSelectItem( Item );
							         ItemSelected?.Invoke( Item );
						         }
						         finally
						         {
							         JustSetText = false;
						         }
					         }

					         return;

				         default:
					         if( !IsDropDownVisible )
						         IsDropDownVisible = true;
					         ViewModel.SearchText = Input.Text;
					         return;
				         }

				         args.Handled = true;
			         };

			DropDownButton = new Button
			                 {
				                 Content = "˅",
				                 Background = Brushes.Transparent,
				                 Margin = new Thickness(),
				                 Padding = new Thickness( 3, 0, 3, 0 ),
				                 BorderThickness = new Thickness( 1, 0, 1, 0 ),
				                 HorizontalAlignment = HorizontalAlignment.Right,
				                 Focusable = false
			                 };

			DropDownButton.Click += ( sender, args ) =>
			                        {
				                        args.Handled = true;
				                        var Vis = !IsDropDownVisible;
				                        if( Vis )
					                        ViewModel.SearchText = Inp.Text;

				                        IsDropDownVisible = Vis;
			                        };

			SecondaryButton = new Button
			                  {
				                  Content = "",
				                  Background = Brushes.Transparent,
				                  Margin = new Thickness(),
				                  Padding = new Thickness( 2, 0, 2, 0 ),
				                  BorderThickness = new Thickness(),
				                  HorizontalAlignment = HorizontalAlignment.Right,
				                  Focusable = false,
				                  Visibility = Visibility.Collapsed
			                  };

			SecondaryButton.Click += ( sender, args ) =>
			                         {
				                         args.Handled = true;
				                         SecondaryButtonClick?.Invoke();
			                         };

			var Dp = new DockPanel();

			var C = Dp.Children;
			DockPanel.SetDock( SecondaryButton, Dock.Right );
			DockPanel.SetDock( DropDownButton, Dock.Right );
			DockPanel.SetDock( Inp, Dock.Left );

			C.Add( SecondaryButton );
			C.Add( DropDownButton );
			C.Add( Inp );

			DockPanel.SetDock( Dp, Dock.Top );

			var Dp1 = new DockPanel();
			Dp1.Children.Add( Dp );

			// ReSharper disable once VirtualMemberCallInConstructor
			Child = Dp1;

			BorderBrush = Input.BorderBrush;
		}

		private bool ListViewHasFocus => ( CurrentListView != null ) && ( CurrentListView.IsFocused || CurrentListView.IsKeyboardFocusWithin );

		private ListView CurrentListView
		{
			get => _CurrentListView;
			set
			{
				_CurrentListView = value;
				if( ViewModel != null )
					ViewModel.ListView = value;
			}
		}


		private void SetUpWindow()
		{
			ListVieWindow.Visibility = Visibility.Collapsed;
			ListVieWindow.OffsetTop = ActualHeight - 1; // Border thickness
			ListVieWindow.OffsetLeft = -1;
			ListVieWindow.MinWidth = ActualWidth;
			ListVieWindow.MaxWidth = ActualWidth;
			ListVieWindow.UpdateLayout();
		}

		public event ItemSelectedEvent ItemSelected;

		private void OnLostFocus( object sender, RoutedEventArgs e )
		{
			if( sender is ChildWindow Win )
			{
				if( IsDropDownVisible )
				{
					Win.Topmost = true;
					Win.Topmost = false;
				}
			}

			if( e is KeyboardFocusChangedEventArgs FocusEvent )
			{
				if( FocusEvent.NewFocus is UIElement NewElement )
				{
					if( !NewElement.IsChildOf( this ) )
					{
						IsDropDownVisible = false;
						return;
					}
				}
			}

			if( !IsKeyboardFocusWithin )
				IsDropDownVisible = false;
		}

		public event SecondaryButtonClickEvent SecondaryButtonClick;


		[Category( "Appearance" )]
		public bool AutoDropDown { get => (bool)GetValue( AutoDropDownProperty ); set => SetValue( AutoDropDownProperty, value ); }

		// Using a DependencyProperty as the backing store for AutoDropDown.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AutoDropDownProperty =
			DependencyProperty.Register( nameof( AutoDropDown ), typeof( bool ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( true ) );


		[Category( "Appearance" )]
		public double DropDownHeight { get => (double)GetValue( DropDownHeightProperty ); set => SetValue( DropDownHeightProperty, value ); }

		// Using a DependencyProperty as the backing store for DropDownHeight.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty DropDownHeightProperty =
			DependencyProperty.Register( nameof( DropDownHeight ), typeof( double ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( 200.0 ) );


		[Category( "Brush" )]
		public Brush Foreground { get => (Brush)GetValue( ForegroundProperty ); set => SetValue( ForegroundProperty, value ); }

		// Using a DependencyProperty as the backing store for Foreground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ForegroundProperty =
			DependencyProperty.Register( nameof( Foreground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.Black, ForegroundPropertyChangedCallback ) );

		private static void ForegroundPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is Brush Brush )
			{
				Lc.Input.Foreground = Brush;
				Lc.DropDownButton.Foreground = Brush;
				Lc.SecondaryButton.Foreground = Brush;
			}
		}


		[Category( "Appearance" )]
		public bool IsDropDownVisible { get => (bool)GetValue( IsDropDownVisibleProperty ); set => SetValue( IsDropDownVisibleProperty, value ); }

		// Using a DependencyProperty as the backing store for IsDropDownVisible.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsDropDownVisibleProperty =
			DependencyProperty.Register( nameof( IsDropDownVisible ), typeof( bool ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.Inherits, IsDropDownVisiblePropertyChangedCallback ) );


		private static void IsDropDownVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is bool Visible )
			{
				var Win = Lc.ListVieWindow;

				if( Visible )
				{
					// Stop flashing in wrong position
					Win.Height = 0;
					Win.Width = 0;
					Win.UpdateLayout();

					Lc.SetUpWindow();
					Win.Show();
					Win.Topmost = true;
					Win.Topmost = false;

					var Item = Lc.ViewModel.GetSelectItem();
					Lc.ListView.ScrollIntoView( Item );
					Lc.Input.Focus();
				}
				else
					Win.Visibility = Visibility.Collapsed;
			}
		}


		[Category( "Common" )]
		public IList<LookupItemBase> ItemsSource { get => (IList<LookupItemBase>)GetValue( ItemsSourceProperty ); set => SetValue( ItemsSourceProperty, value ); }

		// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register( nameof( ItemsSource ), typeof( IList<LookupItemBase> ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( null, ItemsSourcePropertyChangedCallback ) );

		private static void ItemsSourcePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is IList<LookupItemBase> Items && !( Lc.CurrentListView is null ) )
				Lc.CurrentListView.ItemsSource = Items;
		}


		[Category( "Common" )]
		public ListView ListView { get => (ListView)GetValue( ListViewProperty ); set => SetValue( ListViewProperty, value ); }

// Using a DependencyProperty as the backing store for ListView.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ListViewProperty =
			DependencyProperty.Register( nameof( ListView ), typeof( ListView ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( null, ListViewPropertyChangedCallback ) );

		private static void ListViewPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is ListView Lv )
			{
				var H = Lc.DropDownHeight;
				Lv.Height = H;
				Lv.MaxHeight = H;
				Lv.Visibility = Visibility.Visible;
				Lv.IsTextSearchEnabled = false;
				Lv.LostFocus += Lc.OnLostFocus;
				Lc.ListVieWindow.Content = Lv;
				Lc.CurrentListView = Lv;
				Lc.IsDropDownVisible = false;
			}
		}

		[Category( "Options" )]
		public int SearchDelayInMs { get => (int)GetValue( SearchDelayInMsProperty ); set => SetValue( SearchDelayInMsProperty, value ); }

// Using a DependencyProperty as the backing store for SearchDelayInMs.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SearchDelayInMsProperty =
			DependencyProperty.Register( nameof( SearchDelayInMs ), typeof( int ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( DEFAULT_SEARCH_INTERVAL, SearchDelayInMsPropertyChangedCallback ) );

		private static void SearchDelayInMsPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is int Delay )
			{
				var Timer = Lc.SearchTimer;
				Timer.Stop();
				Timer.Interval = Delay;
			}
		}

		[Category( "Appearance" )]
		public object SecondaryButtonContent { get => GetValue( SecondaryButtonContentProperty ); set => SetValue( SecondaryButtonContentProperty, value ); }

// Using a DependencyProperty as the backing store for SecondaryButtonContent.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SecondaryButtonContentProperty =
			DependencyProperty.Register( nameof( SecondaryButtonContent ), typeof( object ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( default, SecondaryButtonContentPropertyChangedCallback ) );

		private static void SecondaryButtonContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc )
				Lc.SecondaryButton.Content = e.NewValue;
		}

		[Category( "Appearance" )]
		public bool SecondaryButtonVisible { get => (bool)GetValue( SecondaryButtonVisibleProperty ); set => SetValue( SecondaryButtonVisibleProperty, value ); }

// Using a DependencyProperty as the backing store for SecondaryButtonVisible.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SecondaryButtonVisibleProperty =
			DependencyProperty.Register( nameof( SecondaryButtonVisible ), typeof( bool ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( false, SecondaryButtonVisiblePropertyChangedCallback ) );

		private static void SecondaryButtonVisiblePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is bool Visible )
				Lc.SecondaryButton.Visibility = Visible ? Visibility.Visible : Visibility.Collapsed;
		}

		[Category( "Common" )]
		public object SelectedItem { get => GetValue( SelectedItemProperty ); set => SetValue( SelectedItemProperty, value ); }

// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedItemProperty =
			DependencyProperty.Register( nameof( SelectedItem ), typeof( object ), typeof( LookupComboBoxBase ),
			                             new FrameworkPropertyMetadata( default, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );

		[Category( "Brush" )]
		public Brush SelectedItemBackground { get => (Brush)GetValue( SelectedItemBackgroundProperty ); set => SetValue( SelectedItemBackgroundProperty, value ); }

// Using a DependencyProperty as the backing store for SelectedItemBackground.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedItemBackgroundProperty =
			DependencyProperty.Register( nameof( SelectedItemBackground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.LightBlue ) );

		[Category( "Common" )]
		public string Text { get => (string)GetValue( TextProperty ); set => SetValue( TextProperty, value ); }

// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register( nameof( Text ), typeof( string ), typeof( LookupComboBoxBase ),
			                             new FrameworkPropertyMetadata( "", FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, TextPropertyChangedCallback ) );

		private static void TextPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is string Text )
			{
				Lc.Input.Text = Text;
				if( !Lc.JustSetText )
				{
					Lc.DontOpenDropdown = true;
					var Timer = Lc.SearchTimer;
					Timer.Stop();
					Timer.Start();
				}
			}
		}

		[Category( "Appearance" )]
		public double TextWidth { get => (double)GetValue( TextWidthProperty ); set => SetValue( TextWidthProperty, value ); }

// Using a DependencyProperty as the backing store for TextWidth.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TextWidthProperty =
			DependencyProperty.Register( nameof( TextWidth ), typeof( double ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( DEFAULT_TEXT_WIDTH, FrameworkPropertyMetadataOptions.Inherits, TextWidthPropertyChangedCallback ) );

		private static void TextWidthPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is LookupComboBoxBase Lc && e.NewValue is double Width )
				Lc.Input.MinWidth = Width;
		}
	}
}