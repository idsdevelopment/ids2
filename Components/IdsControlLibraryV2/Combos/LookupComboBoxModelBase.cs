﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using IdsControlLibraryV2.Annotations;
using IdsControlLibraryV2.ViewModel;
using IdsControlLibraryV2.Virtualisation;

namespace IdsControlLibraryV2.Combos
{
	public abstract class ALookupComboBoxModelBase : ViewModelBase
	{
		internal Action CollectionChanged;

		public ListView ListView;
		public Action<LookupItemBase> WhenSelectedItemChanges;
		private const uint PREFETCH_COUNT = 100;

		public uint PreFetchCount
		{
			get { return Get( () => PreFetchCount, PREFETCH_COUNT ); }
			set { Set( () => PreFetchCount, value ); }
		}

		public string SearchText
		{
			get { return Get( () => SearchText ); }
			set { Set( () => SearchText, value ); }
		}

		[DependsUpon( nameof( SearchText ), Delay = 100 )]
		public abstract void WhenSearchTextChanges();

		public abstract void SetSelectItem( LookupItemBase item );
		public abstract object GetSelectItem();

		public abstract Task<LookupItemBase> GetFirstItem();
		public abstract Task<LookupItemBase> GetLastItem();
		public abstract Task<LookupItemBase> GetItem( string key );


		public abstract string ItemKeyAsString( LookupItemBase item );

		public abstract void First( Action completeCallback = null );
		public abstract void Last( Action completeCallback = null );

		public abstract void ScrollPositionChanged( double viewportHeight, double scrollChange, double verticalOffset );
		public abstract IEnumerable<LookupItemBase> GetItems();
	}

	public abstract class LookupItemBase
	{
		public abstract bool Selected { get; set; }

		public uint Index { get; set; }

		public bool IsEvenRow => ( Index & 1 ) == 0;
		public bool IsOddRow => ( Index & 1 ) != 0;
		public abstract object GetItem();
	}

	public abstract class LookupComboBoxModelBase<T, TK> : ALookupComboBoxModelBase, IItemsProvider<T, TK>
	{
		public class LookupItem : LookupItemBase, INotifyPropertyChanged
		{
			private bool _Selected;

			public LookupItem( T item )
			{
				Item = item;
			}

			public event PropertyChangedEventHandler PropertyChanged;

			public override bool Selected
			{
				get => _Selected;
				set
				{
					_Selected = value;
					OnPropertyChanged();
				}
			}


			public T Item { get; }

			public static implicit operator T( LookupItem i )
			{
				return i.Item;
			}

			[NotifyPropertyChangedInvocator]
			protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
			{
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
			}

			public override object GetItem()
			{
				return Item;
			}
		}


		private enum SEARCH_BY
		{
			NONE,
			FIRST,
			RANGE,
			LAST
		}

		private bool InScrollChange;

		private readonly object LockObject = new object();

		private bool ProcessingRequest;


		public abstract IList<T> First( uint count );
		public abstract IList<T> First( TK key, uint count );
		public abstract IList<T> Last( uint count );
		public abstract IList<T> Last( TK key, uint count );
		public abstract IList<T> FindRange( TK key, int count );
		public abstract IList<T> FindMatch( TK key, int count );
		public abstract int Compare( TK k1, TK k2 );
		public abstract bool StartsWith( string searchValue, TK k2 );
		public abstract bool Filter( TK filter, T value );
		public abstract TK Key( T item );
		public abstract TK Key( string searchText );
		public abstract bool IsEmpty( TK key );
		public abstract string AsString( TK key );
		public abstract int CompareSearchKey( string searchKey, TK k2 );


		public ObservableCollection<LookupItem> DisplayItems
		{
			get { return Get( () => DisplayItems, new ObservableCollection<LookupItem>() ); }
			set { Set( () => DisplayItems, value ); }
		}

		public ObservableCollection<LookupItem> Items
		{
			get { return Get( () => Items ); }
			set
			{
				Set( () => Items, value );
				CollectionChanged?.Invoke();
			}
		}


		public T SelectItem
		{
			get { return Get( () => SelectItem ); }
			set
			{
				lock( LockObject )
				{
					Set( () => SelectItem, value );

					var K = Key( value );

					foreach( var Item in Items )
					{
						if( Compare( Key( Item ), K ) == 0 )
						{
							Dispatcher.InvokeAsync( () =>
							                        {
								                        foreach( var LookupItem in Items )
									                        LookupItem.Selected = false;

								                        Item.Selected = true;
								                        WhenSelectedItemChanges?.Invoke( Item );
							                        } );
							return;
						}
					}
				}
			}
		}


		public override IEnumerable<LookupItemBase> GetItems()
		{
			return Items;
		}

		public override void ScrollPositionChanged( double viewportHeight, double scrollChange, double verticalOffset )
		{
			if( !InScrollChange )
			{
				var ViewportHeight = (int)viewportHeight;
				var ScrollChange = (int)scrollChange;
				var VerticalOffset = (int)verticalOffset;

				var Count = Items.Count;

				if( Count <= 0 )
					return;

				if( VerticalOffset > Count )
				{
					VerticalOffset = Count - 1;
					if( VerticalOffset < 0 )
						return;
				}

				InScrollChange = true;
				var ItemKey = Key( Items[ VerticalOffset ] );

				var Backwards = ScrollChange < 0;

				var FetchCount = Backwards ? -PreFetchCount : PreFetchCount;

				// Top or end of list
				if( ( ( VerticalOffset <= ViewportHeight ) && Backwards ) || ( ( VerticalOffset >= ( Count - VerticalOffset ) ) && !Backwards ) )
				{
					Task.Run( () =>
					          {
						          try
						          {
							          UpdateItems( FindRange( ItemKey, (int)FetchCount ), true );
						          }
						          finally
						          {
							          InScrollChange = false;
						          }
					          } );
				}
			}
		}


		public override void First( Action completeCallback = null )
		{
			Task.Run( () =>
			          {
				          UpdateItems( First( PreFetchCount ), false );
				          completeCallback?.Invoke();
			          } );
		}


		public override void Last( Action completeCallback = null )
		{
			Task.Run( () =>
			          {
				          UpdateItems( Last( PreFetchCount ), false );
				          completeCallback?.Invoke();
			          } );
		}

		public override async Task<LookupItemBase> GetFirstItem()
		{
			return await Task.Run( async () =>
			                       {
				                       while( ProcessingRequest )
					                       await Task.Delay( 1 );

				                       return ( Items != null ) && ( Items.Count > 0 ) ? Items[ 0 ] : null;
			                       } );
		}

		public override async Task<LookupItemBase> GetLastItem()
		{
			return await Task.Run( async () =>
			                       {
				                       while( ProcessingRequest )
					                       await Task.Delay( 1 );

				                       if( !( Items is null ) )
				                       {
					                       var C = Items.Count;
					                       if( C > 0 )
						                       return Items[ C - 1 ];
				                       }

				                       return null;
			                       } );
		}

		public override async Task<LookupItemBase> GetItem( string key )
		{
			return await Task.Run( async () =>
			                       {
				                       while( ProcessingRequest )
					                       await Task.Delay( 1 );

				                       if( !( Items is null ) )
				                       {
					                       var SearchKey = Key( key );

					                       foreach( var Item in Items )
					                       {
						                       if( Compare( SearchKey, Key( Item ) ) == 0 )
							                       return Item;
					                       }

					                       var Result = FindMatch( SearchKey, 1 );

					                       if( Result.Count > 0 )
						                       return new LookupItem( Result[ 0 ] );
				                       }

				                       return null;
			                       } );
		}


		public override string ItemKeyAsString( LookupItemBase item )
		{
			return AsString( Key( (LookupItem)item ) );
		}

		public override void SetSelectItem( LookupItemBase item )
		{
			if( item is LookupItem Item )
				SelectItem = Item;
		}

		// Keep in sync with Azure (May have been modified)
		[DependsUpon( nameof( SelectItem ) )]
		public void WhenSelectedItemChanged()
		{
			Task.Run( () =>
			          {
				          var Ndx = 0;
				          var RNdx = -1;
				          var SelKey = Key( SelectItem );
				          var SKey = AsString( SelKey );
				          foreach( var Item in Items )
				          {
					          var Key = AsString( this.Key( Item ) );
					          if( SKey == Key )
					          {
						          RNdx = Ndx;
						          break;
					          }

					          Ndx++;
				          }

				          var MatchItem = FindMatch( SelKey, 1 );

				          if( ( RNdx != -1 ) && ( RNdx < Items.Count ) )
				          {
					          Dispatcher.Invoke( () =>
					                             {
						                             Items.RemoveAt( RNdx );
					                             } );
				          }

				          UpdateItems( MatchItem, true );
			          } );
		}

		public override object GetSelectItem()
		{
			return SelectItem;
		}


		private void UpdateItems( IList<T> items, bool addToListOnly )
		{
			if( items != null )
			{
				Dispatcher.Invoke( () =>
				                   {
					                   lock( LockObject )
					                   {
						                   var Itms = Items;

						                   if( Itms == null )
							                   Items = Itms = new ObservableCollection<LookupItem>();

						                   if( !addToListOnly )
						                   {
							                   var Remove = ( from CurrentItem in Itms
							                                  let I = ( from NewItem in items
							                                            where Compare( Key( NewItem ), Key( CurrentItem ) ) != 0
							                                            select NewItem ).FirstOrDefault()
							                                  where I == null
							                                  select CurrentItem ).ToList();

							                   foreach( var Item in Remove )
								                   Itms.Remove( Item );
						                   }

						                   var Add = ( from NewItem in items
						                               let I = ( from CurrentItem in Itms
						                                         where Compare( Key( NewItem ), Key( CurrentItem ) ) == 0
						                                         select CurrentItem ).FirstOrDefault()
						                               where I == null
						                               select NewItem ).ToList();

						                   var Combined = new List<LookupItem>();
						                   foreach( var Item in Itms )
							                   Combined.Add( Item );

						                   Combined.AddRange( from A in Add
						                                      select new LookupItem( A ) );

						                   uint Index = 0;
						                   var Ordered = from Item in Combined
						                                 orderby AsString( Key( Item.Item ) )
						                                 select new LookupItem( Item ) { Index = Index++ };

						                   Items = new ObservableCollection<LookupItem>( Ordered );
					                   }
				                   } );
			}
		}

		public override async void WhenSearchTextChanges()
		{
			var Search = SearchText;
			try
			{
				SEARCH_BY DoSelected()
				{
					var Result = SEARCH_BY.RANGE;

					lock( LockObject )
					{
						if( string.IsNullOrEmpty( Search ) )
							return SEARCH_BY.FIRST;

						if( Items != null )
						{
							var Selected = new List<LookupItem>();

							foreach( var Item in Items )
							{
								Item.Selected = false;

								if( StartsWith( Search, Key( Item ) ) )
								{
									Selected.Add( Item );
									Result = SEARCH_BY.NONE;
								}
							}

							if( Selected.Count > 0 )
							{
								var Item = Selected[ 0 ];
								Item.Selected = true;
								SelectItem = Item;
							}
						}
					}

					return Result;
				}

				ProcessingRequest = true;

				switch( DoSelected() )
				{
				case SEARCH_BY.FIRST:
					await Task.Run( () =>
					                {
						                UpdateItems( First( PreFetchCount ), false );
					                } );
					break;

				case SEARCH_BY.RANGE:
					await Task.Run( async () =>
					                {
						                UpdateItems( FindRange( Key( SearchText ), (int)PreFetchCount ), false );
						                SetSelectItem( await GetItem( Search ) );
					                } );
					break;
				}

				ProcessingRequest = false;
			}
			catch( Exception E )
			{
				Console.WriteLine( nameof( WhenSearchTextChanges ) );
				Console.WriteLine( E );
			}
		}
	}
}