﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IdsControlLibraryV2.TabControl
{
	public class PageTabItem : FloatableTabItem
	{
		private readonly Label CloseLabel = new Label();
		private readonly Label HeaderContent = new Label { Padding = new Thickness( 5, 0, 5, 0 ) };
		private readonly Frame PageFrame = new Frame();

		public PageTabItem()
		{
			base.Content = PageFrame;
			CloseLabel.Padding = new Thickness( 2 );
			CloseLabel.Content = "❌";
			CloseLabel.FontSize = 8;
			CloseLabel.Visibility = Visibility.Collapsed;

			CloseLabel.HorizontalAlignment = HorizontalAlignment.Right;
			CloseLabel.VerticalAlignment = VerticalAlignment.Center;

			CloseLabel.Background = Brushes.Transparent;

			CloseLabel.MouseEnter += ( sender, args ) =>
			                         {
				                         if( Owner is PageTabControl Pt )
					                         CloseLabel.Background = Pt.CloseHover;
			                         };

			CloseLabel.MouseLeave += ( sender, args ) => { CloseLabel.Background = Brushes.Transparent; };
			CloseLabel.MouseLeftButtonDown += ( sender, args ) => { RemoveTab(); };

			var Dp = new DockPanel { LastChildFill = true };
			base.SetHeader( Dp );
			DockPanel.SetDock( HeaderContent, Dock.Left );
			DockPanel.SetDock( CloseLabel, Dock.Right );
			Dp.Children.Add( HeaderContent );
			Dp.Children.Add( CloseLabel );

			BeginFloat = SizeToContent;
			EndFloat = RemoveSizeToContent;

		}

		private void DoAllow( bool allow )
		{
			base.AllowClose = allow;
			CloseLabel.Visibility = allow ? Visibility.Visible : Visibility.Collapsed;
		}


		protected override void SetHeader( object header )
		{
			HeaderContent.Content = header;
		}

		protected override object GetHeader()
		{
			return HeaderContent.Content;
		}

		[Category( "Options" )]
		public new bool AllowClose
		{
			get => (bool)GetValue( AllowCloseProperty );
			set => SetValue( AllowCloseProperty, value );
		}

		// Using a DependencyProperty as the backing store for AllowClose.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty AllowCloseProperty =
			DependencyProperty.Register( nameof( AllowClose ), typeof( bool ), typeof( PageTabItem ), new FrameworkPropertyMetadata( false, AllowClosePropertyChangedCallback ) );

		private static void AllowClosePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is PageTabItem Pt && e.NewValue is bool Allow )
				Pt.DoAllow( Allow );
		}


		[Category( "Common" )]
		public new Page Content
		{
			get => (Page)PageFrame.Content;
			set => SetValue( ContentProperty, value );
		}

		// Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty ContentProperty =
			DependencyProperty.Register( nameof( Content ), typeof( Page ), typeof( PageTabItem ), new FrameworkPropertyMetadata( null, ContentPropertyChangedCallback ) );


		private static void ContentPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is PageTabItem Pt && e.NewValue is Page Page )
				Pt.PageFrame.Content = Page;
		}


		[Category( "Common" )]
		public new object Header
		{
			get => GetValue( HeaderProperty );
			set => SetValue( HeaderProperty, value );
		}

		// Using a DependencyProperty as the backing store for Header.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty HeaderProperty =
			DependencyProperty.Register( nameof( Header ), typeof( object ), typeof( PageTabItem ), new FrameworkPropertyMetadata( null, HeaderPropertyChangedCallback ) );


		private static void HeaderPropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is PageTabItem Ft )
				Ft.HeaderContent.Content = e.NewValue;
		}
	}

	public class PageTabControl : FloatableTabControl
	{
		private bool First = true;

		public PageTabControl()
		{
			Loaded += ( sender, args ) =>
			          {
				          if( First )
				          {
					          First = false;

					          if( Items.Count > 0 )
						          ItemsSource = Items;
				          }
			          };
		}

		protected override void OnRemoveTab( FloatableTabItem item )
		{
			var Id = item.Id;
			foreach( var Item in ItemsSource )
			{
				if( Item.Id == Id )
				{
					ItemsSource.Remove( Item );
					return;
				}
			}
		}

		private void DoBaseItemsSource( ObservableCollection<PageTabItem> items )
		{
			var NewCollection = new ObservableCollection<FloatableTabItem>( items );
			items.CollectionChanged += ( sender, args ) =>
			                           {
				                           switch( args.Action )
				                           {
				                           case NotifyCollectionChangedAction.Add:
					                           var Ndx = args.NewStartingIndex;
					                           foreach( var NewItem in args.NewItems )
					                           {
						                           if( NewItem is PageTabItem Item )
						                           {
							                           Item.Owner = this;
							                           NewCollection.Insert( Ndx++, Item );
							                           SelectedItem = Item;
						                           }
					                           }

					                           break;

				                           case NotifyCollectionChangedAction.Remove:
					                           foreach( var OldItem in args.OldItems )
					                           {
						                           foreach( var Item in NewCollection )
						                           {
							                           if( Equals( OldItem, Item ) )
							                           {
								                           NewCollection.Remove( Item );
								                           break;
							                           }
						                           }
					                           }

					                           break;
				                           }
			                           };
			base.ItemsSource = NewCollection;
		}

		[Category( "Brush" )]
		public Brush CloseDisableHover
		{
			get => (Brush)GetValue( CloseDisableHoverProperty );
			set => SetValue( CloseDisableHoverProperty, value );
		}

		// Using a DependencyProperty as the backing store for CloseDisableHover.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CloseDisableHoverProperty =
			DependencyProperty.Register( nameof( CloseDisableHover ), typeof( Brush ), typeof( PageTabControl ), new FrameworkPropertyMetadata( Brushes.DimGray ) );


		[Category( "Brush" )]
		public Brush CloseHover
		{
			get => (Brush)GetValue( CloseHoverProperty );
			set => SetValue( CloseHoverProperty, value );
		}

		// Using a DependencyProperty as the backing store for SystemCloseHover.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CloseHoverProperty =
			DependencyProperty.Register( nameof( CloseHover ), typeof( Brush ), typeof( PageTabControl ), new FrameworkPropertyMetadata( Brushes.Red ) );

		[Category( "Common" )]
		public new ObservableCollection<PageTabItem> Items
		{
			get => (ObservableCollection<PageTabItem>)GetValue( ItemsProperty );
			set => SetValue( ItemsProperty, value );
		}

		// Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty ItemsProperty =
			DependencyProperty.Register( nameof( Items ), typeof( ObservableCollection<PageTabItem> ), typeof( PageTabControl ), new FrameworkPropertyMetadata( new ObservableCollection<PageTabItem>() ) );

		[Category( "Common" )]
		public new ObservableCollection<PageTabItem> ItemsSource
		{
			get => (ObservableCollection<PageTabItem>)GetValue( ItemsSourceProperty );
			set => SetValue( ItemsSourceProperty, value );
		}

		// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
		public new static readonly DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register( nameof( ItemsSource ), typeof( ICollection<PageTabItem> ), typeof( PageTabControl ), new FrameworkPropertyMetadata( null, ItemsSourcePropertyChangedCallback ) );

		private static void ItemsSourcePropertyChangedCallback( DependencyObject d, DependencyPropertyChangedEventArgs e )
		{
			if( d is PageTabControl Tc && e.NewValue is ObservableCollection<PageTabItem> Items )
				Tc.DoBaseItemsSource( Items );
		}
	}
}