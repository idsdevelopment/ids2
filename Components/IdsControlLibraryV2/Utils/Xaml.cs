﻿using System.Windows.Controls;
using System.Windows.Markup;

namespace IdsControlLibraryV2.Utils
{
	public static class Xaml
	{
		private static string ToXaml( string objectType, string xaml )
		{
			return $@"<{objectType} xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
									xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
									xmlns:system='clr-namespace:System;assembly=mscorlib'>
							{xaml}
					</{objectType}>";
		}

		public static object XamlToObject( this string xaml )
		{
			return XamlReader.Parse( xaml );
		}

		public static ItemsPanelTemplate XamlToItemsPanelTemplate( this string xaml )
		{
			return XamlToObject( ToXaml( "ItemsPanelTemplate", xaml ) ) as ItemsPanelTemplate;
		}

		public static GridView XamlToGridView( this string xaml )
		{
			return XamlToObject( ToXaml( "GridView", xaml ) ) as GridView;
		}

		public static ListView XamlToListView( this string xaml )
		{
			return XamlToObject( ToXaml( "ListView", xaml ) ) as ListView;
		}

		public static Border XamlToBorder( this string xaml )
		{
			return XamlToObject( ToXaml( "Border", xaml ) ) as Border;
		}

		public static ToolBar XamlToToolBar(this string xaml)
		{
			return XamlToObject(ToXaml("ToolBar", xaml)) as ToolBar;
		}
		public static ToolBarTray XamlToToolBarTray(this string xaml)
		{
			return XamlToObject(ToXaml("ToolBarTray", xaml)) as ToolBarTray;
		}
	}
}