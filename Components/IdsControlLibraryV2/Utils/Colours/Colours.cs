﻿using System.Windows.Media;

namespace IdsControlLibraryV2.Utils.Colours
{
	public static class Colours
	{
		public static Color ArgbToColor( this uint argb )
		{
			return new Color
			       {
				       B = (byte)( argb & 0xff ),
				       G = (byte)( ( argb >> 8 ) & 0xff ),
				       R = (byte)( ( argb >> 16 ) & 0xff ),
				       A = (byte)( ( argb >> 24 ) & 0xff )
			       };
		}

		public static Color ArgbToColor( this int argb )
		{
			return ( (uint)argb ).ArgbToColor();
		}


		public static SolidColorBrush ArgbToSolidColorBrush( this uint argb )
		{
			return new SolidColorBrush( argb.ArgbToColor() );
		}

		public static SolidColorBrush ArgbToSolidColorBrush( this int argb )
		{
			return new SolidColorBrush( argb.ArgbToColor() );
		}


		public static uint ToArgb( this Color c )
		{
			return (uint)( ( c.A << 24 ) | ( c.R << 16 ) | ( c.G << 8 ) | c.B );
		}

		public static uint ToArgb( this SolidColorBrush s )
		{
			return s.Color.ToArgb();
		}
	}
}