﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdsControlLibraryV2.Virtualisation
{
	public interface IItemsProvider<T, K>
	{
		IList<T> First( uint count );
		IList<T> First( K key, uint count );
		IList<T> Last( uint count );
		IList<T> Last( K key, uint count );
		IList<T> FindRange( K key, int count ); // + == next, - == prior
		IList<T> FindMatch( K key, int count );
		int CompareSearchKey( string k1, K k2 );
		int Compare( K k1, K k2 );
		bool StartsWith( string searchValue, K k2 );
		bool Filter( K filter, T value );
		K Key( T item ); // Returns the key for the item
		K Key( string searchText );
		bool IsEmpty( K key );
		string AsString( K key );
	}
}