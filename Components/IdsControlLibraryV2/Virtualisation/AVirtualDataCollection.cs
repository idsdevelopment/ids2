﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace IdsControlLibraryV2.Virtualisation
{
	public abstract class AVirtualDataCollection<TK, TV> : AsyncObservableCollection<TV>, IEnumerator<TV>
	{
		private readonly IEnumerator<TV> BasEnumerator;

		private readonly DataCache<TK, TV> Cache;
		private readonly DataCache<int, TK> OffsetCache;
		protected Action OnDispose;

		protected AVirtualDataCollection( int maxCacheSize )
		{
			BasEnumerator = base.GetEnumerator();
			Cache = new DataCache<TK, TV>( maxCacheSize );
			OffsetCache = new DataCache<int, TK>( maxCacheSize );
		}

		public bool MoveNext()
		{

			return BasEnumerator.MoveNext();
		}

		public void Reset()
		{
			BasEnumerator.Reset();
		}

		public TV Current => BasEnumerator.Current;

		object IEnumerator.Current => Current;

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		public new IEnumerator<TV> GetEnumerator()
		{
			return this;
		}

		protected virtual void Dispose( bool disposing )
		{
			if( disposing )
				OnDispose?.Invoke();
		}

		protected override void ClearItems()
		{
			Cache.Clear();
			base.ClearItems();
		}


		public bool ContainsKey( TK key )
		{
			return Cache.ContainsKey( key );
		}
	}
}