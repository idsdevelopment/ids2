﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitMultOrDiv( FormulasParser.MultOrDivContext context )
		{
			var Rval = VisitPow( context.pow() );
			var MultOrDiv = context.multOrDiv();

			if( MultOrDiv != null )
			{
				var LVal = VisitMultOrDiv( MultOrDiv );

				if( context.MULT() != null )
					return LVal * Rval;

				return Rval != 0 ? LVal / Rval : 0;
			}

			return Rval;
		}
	}
}