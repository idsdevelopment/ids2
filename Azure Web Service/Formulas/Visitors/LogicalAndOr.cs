﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitLogicalAndOr( FormulasParser.LogicalAndOrContext context )
		{
			var AndOr = context.logicalAndOr();

			if( AndOr != null )
			{
				if( VisitLogicalAndOr( AndOr ) != 0 )
					return 1;
			}

			return VisitLogicalAnd( context.logicalAnd() );
		}

		public override Result VisitLogicalAnd( FormulasParser.LogicalAndContext context )
		{
			var And = context.logicalAnd();

			if( And != null )
			{
				if( VisitLogicalAnd( context.logicalAnd() ) == 0 )
					return 0;
			}

			return VisitConditionalExpression( context.conditionalExpression() );
		}
	}
}