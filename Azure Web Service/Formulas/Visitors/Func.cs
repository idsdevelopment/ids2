﻿using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitRounding( FormulasParser.RoundingContext context )
		{
			var Value = VisitLogicalAndOr( context.logicalAndOr() );
			var Places = 2;
			var Digit = context.DIGIT();

			if( Digit != null )
				int.TryParse( Digit.GetText().Trim(), out Places );

			return Math.Round( Value, Places, MidpointRounding.AwayFromZero );
		}

		public override Result VisitMinMax( FormulasParser.MinMaxContext context )
		{
			var Logical = context.logicalAndOr();
			var LVal = VisitLogicalAndOr( Logical[ 0 ] );
			var RVal = VisitLogicalAndOr( Logical[ 1 ] );

			return context.MAX() != null ? Math.Max( LVal, RVal ) : Math.Min( LVal, RVal );
		}

		public override Result VisitContains( FormulasParser.ContainsContext context )
		{
			var Logical = context.logicalAndOr();
			var LVal = VisitLogicalAndOr( Logical[ 0 ] );
			var RVal = VisitLogicalAndOr( Logical[ 1 ] );

			return ( (string)LVal ).Contains( RVal ) ? 1 : 0;
		}


		public override Result VisitFunc( FormulasParser.FuncContext context )
		{
			var Rounding = context.rounding();

			if( Rounding != null )
				return VisitRounding( Rounding );

			var MinMax = context.minMax();

			if( MinMax != null )
				return VisitMinMax( MinMax );

			var Contains = context.contains();

			if( Contains != null )
				return VisitContains( Contains );

			var Value = VisitLogicalAndOr( context.logicalAndOr() );

			var Func = context.funcname().GetText().Trim().ToLower();

			try
			{
				switch( Func )
				{
				case "sin":
					return (decimal)Math.Sin( (double)Value );

				case "asin":
					return (decimal)Math.Asin( (double)Value );

				case "sqrt":
					return (decimal)Math.Sqrt( (double)Value );

				case "acos":
					return (decimal)Math.Acos( (double)Value );

				case "cos":
					return (decimal)Math.Cos( (double)Value );

				case "tan":
					return (decimal)Math.Tan( (double)Value );

				case "atan":
					return (decimal)Math.Atan( (double)Value );

				case "log":
					return (decimal)Math.Log( (double)Value );

				case "log10":
					return (decimal)Math.Log10( (double)Value );

				// String support
				case "upper":
				case "toupper":
					return ( (string)Value ).ToUpper();

				case "lower":
				case "tolower":
					return ( (string)Value ).ToLower();

				case "trim":
					return ( (string)Value ).Trim();

				case "trimright":
				case "trimend":
					return ( (string)Value ).TrimEnd();

				case "trimleft":
				case "trimstart":
					return ( (string)Value ).TrimStart();
				}
			}
			catch
			{
			}

			return 0;
		}
	}
}