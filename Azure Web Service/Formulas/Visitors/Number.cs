﻿using System.Text;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitNumber( FormulasParser.NumberContext context )
		{
			var Digits = context.DIGIT();

			if( Digits != null )
			{
				var Num = new StringBuilder();

				foreach( var Digit in Digits )
					Num.Append( Digit.GetText().Trim() );

				if( decimal.TryParse( Num.ToString(), out var Value ) )
				{
					var Points = context.point();

					if( Points != null )
						Value += VisitPoint( Points );

					return context.MINUS() != null ? -Value : Value;
				}
			}

			return 0;
		}

		public override Result VisitPoint( FormulasParser.PointContext context )
		{
			var Digits = context.DIGIT();

			if( Digits != null )
			{
				var Num = new StringBuilder( "0." );

				foreach( var Digit in Digits )
					Num.Append( Digit.GetText().Trim() );

				if( decimal.TryParse( Num.ToString(), out var Value ) )
					return Value;
			}

			return 0;
		}
	}
}