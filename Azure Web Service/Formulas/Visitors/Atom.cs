﻿using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitConstantPI( FormulasParser.ConstantPIContext context )
		{
			return (decimal)Math.PI;
		}

		public override Result VisitConstantE( FormulasParser.ConstantEContext context )
		{
			return (decimal)Math.E;
		}

		public override Result VisitToFunction( FormulasParser.ToFunctionContext context )
		{
			return VisitFunc( context.func() );
		}

		public override Result VisitToBrackets( FormulasParser.ToBracketsContext context )
		{
			return VisitLogicalAndOr( context.logicalAndOr() );
		}

		public override Result VisitToString( FormulasParser.ToStringContext context )
		{
			return context.STRING().GetText().Trim( '"' );
		}
	}
}