﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitIfBody( FormulasParser.IfBodyContext context )
		{
			var Value = VisitLogicalAndOr( context.logicalAndOr() );

			if( Value != 0 )
				return VisitLet( context.let() );

			var Else = context.@else();

			return Else != null ? VisitElse( Else ) : 0;
		}

		public override Result VisitElse( FormulasParser.ElseContext context )
		{
			var If = context.ifBody();

			if( If != null )
				return VisitIfBody( If );

			var Let = context.let();

			return Let != null ? VisitLet( Let ) : 0;
		}

		public override Result VisitIf( FormulasParser.IfContext context )
		{
			return VisitIfBody( context.ifBody() );
		}
	}
}