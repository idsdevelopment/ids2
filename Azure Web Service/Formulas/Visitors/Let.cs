﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitLet( FormulasParser.LetContext context )
		{
			var Value = VisitConditionalExpression( context.conditionalExpression() );

			if( context.RESULT() != null )
				HaveResult = true;
			else
			{
				var Id = context.IDENT().GetText().Trim();

				if( Variables.ContainsKey( Id ) )
					Variables[ Id ] = Value;
				else
					Variables.Add( Id, Value );
			}

			return Value;
		}
	}
}