﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		private static Result Relop( Result lval, Result rval, FormulasParser.RelopContext ctx )
		{
			var AsString = lval.IsString || rval.IsString;

			if( ctx != null )
			{
				if( ctx.EQ() != null )
				{
					if( AsString )
						return lval == (string)rval ? 1 : 0;

					return lval == rval ? 1 : 0;
				}

				if( ctx.NOT_EQ() != null )
				{
					if( AsString )
						return string.CompareOrdinal( lval, rval ) != 0 ? 1 : 0;

					return lval != rval ? 1 : 0;
				}

				if( ctx.GT() != null )
				{
					if( AsString )
						return string.CompareOrdinal( lval, rval ) > 0 ? 1 : 0;

					return lval > rval ? 1 : 0;
				}

				if( ctx.GT_EQ() != null )
				{
					if( AsString )
						return string.CompareOrdinal( lval, rval ) >= 0 ? 1 : 0;

					return lval >= rval ? 1 : 0;
				}

				if( ctx.LT() != null )
				{
					if( AsString )
						return string.CompareOrdinal( lval, rval ) < 0 ? 1 : 0;

					return lval < rval ? 1 : 0;
				}

				if( ctx.LT_EQ() != null )
				{
					if( AsString )
						return string.CompareOrdinal( lval, rval ) <= 0 ? 1 : 0;

					return lval <= rval ? 1 : 0;
				}
			}

			return 0;
		}


		public override Result VisitConditionalExpression( FormulasParser.ConditionalExpressionContext context )
		{
			while( true )
			{
				var Expression = context.plusOrMinus();

				if( Expression != null )
					return VisitPlusOrMinus( Expression );

				var Conditionals = context.conditionalExpression();

				if( Conditionals != null )
				{
					var Value = Relop( VisitConditionalExpression( Conditionals[ 0 ] ),
					                   VisitConditionalExpression( Conditionals[ 1 ] ), context.relop() );

					// Ternary
					if( context.QMARK() != null )
					{
						if( Conditionals.Length >= 4 )
						{
							context = Value != 0 ? Conditionals[ 2 ] : Conditionals[ 3 ];

							continue;
						}

						return 0;
					}

					return Value;
				}

				return 0;
			}
		}
	}
}