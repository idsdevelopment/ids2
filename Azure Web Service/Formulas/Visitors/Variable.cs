﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitVariable( FormulasParser.VariableContext context )
		{
			var Id = context.IDENT().GetText().Trim();

			if( PropertyMap.TryGetValue( Id, out var PropertyName ) )
			{
				var PropertyInfo = Record.GetType().GetProperty( PropertyName );

				if( PropertyInfo != null )
				{
					var PropertyValue = PropertyInfo.GetValue( Record, null );

					if( PropertyValue is bool BoolValue )
						return BoolValue ? 1 : 0;

					if( PropertyValue is byte ByteValue )
						return ByteValue;

					if( PropertyValue is char CharValue )
						return CharValue;

					if( PropertyValue is decimal DecimalValue )
						return DecimalValue;

					if( PropertyValue is double DoubleValue )
						return (decimal)DoubleValue;

					if( PropertyValue is float FloatValue )
						return (decimal)FloatValue;

					if( PropertyValue is int IntValue )
						return IntValue;

					if( PropertyValue is long LongValue )
						return LongValue;

					if( PropertyValue is sbyte SbyteValue )
						return SbyteValue;

					if( PropertyValue is short ShortValue )
						return ShortValue;

					if( PropertyValue is uint UIntValue )
						return UIntValue;

					if( PropertyValue is ulong ULongValue )
						return ULongValue;

					if( PropertyValue is ushort UShortValue )
						return UShortValue;

					if( PropertyValue is string StringValue )
						return StringValue;

					return 0;
				}
			}

			return Variables.TryGetValue( Id, out var Value ) ? Value : 0;
		}
	}
}