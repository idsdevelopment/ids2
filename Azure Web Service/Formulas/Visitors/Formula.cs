﻿using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitFormula( FormulasParser.FormulaContext context )
		{
			return VisitResult( context.result() );
		}
	}
}