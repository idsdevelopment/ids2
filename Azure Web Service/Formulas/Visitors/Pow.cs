﻿using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitPow( FormulasParser.PowContext context )
		{
			var Value = VisitChildren( context );
			var Pow = context.POW();

			if( Pow != null )
			{
				var Power = VisitPow( context.pow() );

				try
				{
					return (decimal)Math.Pow( (double)Value, (double)Power );
				}
				catch
				{
					return 0;
				}
			}

			return Value;
		}
	}
}