﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antlr4.Runtime;
using Formulas.Grammar;
using Formulas.Visitors;

namespace Formulas
{
	public class Formula
	{
		internal class ErrorListener : BaseErrorListener
		{
			private readonly FormulasParser Parser;

			public ErrorListener( FormulasParser parser )
			{
				Parser = parser;
			}

			public override void SyntaxError( IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e )
			{
				Parser.HasErrors = true;
				Parser.SyntaxErrors.Add( $"Line: {line},  Position: {charPositionInLine},  Symbol: '{offendingSymbol.Text.Trim()}'" );
			}
		}


		private FormulasParser Parser;

		public ( List<string> ErrorList, Result Value ) Evaluate( string formula, object record, Dictionary<string, string> propertyMap )
		{
			Result Value = 0;

			try
			{
				var InStream = new AntlrInputStream( formula.Trim() );
				Parser = new FormulasParser( new CommonTokenStream( new FormulasLexer( InStream ) ) );

				Parser.RemoveErrorListeners();
				Parser.AddErrorListener( new ErrorListener( Parser ) );

				var Tree = Parser.formula();

				if( !Parser.HasErrors )
				{
					try
					{
						Value = new FormulaVisitors( record, propertyMap ).Visit( Tree );
					}
					catch( Exception E )
					{
						Parser.SyntaxErrors.Add( E.Message );
					}
				}
			}
			catch
			{
				Parser.SyntaxErrors.Add( "Error parsing formula." );
			}

			return ( ErrorList: Parser.SyntaxErrors, Value: Value );
		}

		public (List<string> ErrorList, Result Value) Evaluate( string formula, object record )
		{
			var Map = record.GetType().GetProperties().ToDictionary( propertyInfo => propertyInfo.Name, propertyInfo => propertyInfo.Name );

			return Evaluate( formula, record, Map );
		}
	}
}