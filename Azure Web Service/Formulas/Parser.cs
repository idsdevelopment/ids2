﻿using System.Collections.Generic;
using System.Linq;

namespace Formulas.Grammar
{
	public partial class FormulasParser
	{
		internal class SyErrors : List<string>
		{
			internal new void Add( string txt )
			{
				if( this.Any( synError => synError == txt ) )
					return;

				base.Add( txt );
			}
		}

		internal bool HasErrors;
		internal SyErrors SyntaxErrors = new SyErrors();
	}
}