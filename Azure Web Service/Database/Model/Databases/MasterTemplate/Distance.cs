//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.MasterTemplate
{
    using System;
    using System.Collections.Generic;
    
    public partial class Distance
    {
        public decimal FromLatitude { get; set; }
        public decimal FromLongitude { get; set; }
        public decimal ToLatitude { get; set; }
        public decimal ToLongitude { get; set; }
        public System.DateTime DateLastAccessed { get; set; }
        public System.DateTime DateCreated { get; set; }
        public decimal DistanceInMeters { get; set; }
    }
}
