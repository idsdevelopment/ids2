//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.MasterTemplate
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inventory
    {
        public string InventoryCode { get; set; }
        public string Barcode { get; set; }
        public bool Active { get; set; }
        public bool IsContainer { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public decimal UnitVolume { get; set; }
        public decimal UnitWeight { get; set; }
        public bool DangerousGoods { get; set; }
        public string UNClass { get; set; }
        public decimal PackageQuantity { get; set; }
        public string PackageType { get; set; }
        public string BinX { get; set; }
        public string BinY { get; set; }
        public string BinZ { get; set; }
    }
}
