﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.Users;
using Interfaces.Interfaces;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public static List<Preference> GetPreference( IRequestContext context )
		{
			try
			{
				using( var Db = new UsersEntities() )
				{
					return ( from P in Db.Preferences
					         select P ).ToList();
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return null;
		}
	}
}