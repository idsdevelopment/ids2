﻿using System.Linq;
using System.Threading;
using Database.Model.Databases.Users;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public static Configuration Configuration
		{
			get
			{
				for( var I = 0; I++ < 10; ) // Running locally
				{
					try
					{
						using var Db = new UsersEntities();

						return ( from C in Db.Configurations
						         select C ).FirstOrDefault();
					}
					catch
					{
						Thread.Sleep( 10 );
					}
				}

				return null;
			}

			set
			{
				using var Db = new UsersEntities();

				var Config = ( from C in Db.Configurations
				               select C ).FirstOrDefault();

				if( Config == null )
				{
					Config = new Configuration();
					Db.Configurations.Add( Config );
				}

				Config.LastAddressCleanup    = value.LastAddressCleanup;
				Config.AverageAccessInterval = value.AverageAccessInterval;
				Db.SaveChanges();
			}
		}
	}
}