﻿#nullable enable

using System;
using System.Linq;
using Database.Model.Databases.Users;
using Interfaces.Interfaces;
using Microsoft.Azure.Management.Storage.Fluent.Models;
using Storage;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public static EndPoint? GetEndPoints( IRequestContext context )
		{
			try
			{
				var CarrierId = context.CarrierId.ToLower();

				using var Db = new UsersEntities();

				var RetVal = ( from E in Db.EndPoints
				               where E.CarrierId == CarrierId
				               select E ).FirstOrDefault();

				if( !( RetVal is null ) )
				{
					RetVal.CarrierId = CarrierId; // Make sure lower case for blob storage

					return RetVal;
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return null;
		}

		public static bool HasEndpoints( IRequestContext context ) => !( GetEndPoints( context ) is null );

		public static void DeleteEndpoint( IRequestContext context )
		{
			try
			{
				using var Db = new UsersEntities();

				var Ep        = Db.EndPoints;
				var CarrierId = context.CarrierId.ToLower();

				var Rec = ( from E in Ep
				            where E.CarrierId == CarrierId
				            select E ).FirstOrDefault();

				if( Rec != null )
				{
					Ep.Remove( Rec );
					Db.SaveChanges();
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( nameof( DeleteEndpoint ), Exception );
			}
		}

		public static EndPoint? CreateStorageEndPoint( IRequestContext context, (string AccountName, string AccountKey, Endpoints EndPoint, bool Ok) endPoint )
		{
			var (AccountName, AccountKey, EndPoint, Ok) = endPoint;

			if( Ok )
			{
				var Cid = context.CarrierId.ToLower();

				using var Db = new UsersEntities();

				var Rec = ( from U in Db.EndPoints
				            where U.CarrierId == Cid
				            select U ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					Rec.StorageAccount       = AccountName;
					Rec.StorageAccountKey    = AccountKey;
					Rec.StorageTableEndpoint = EndPoint.Table;
					Rec.StorageFileEndpoint  = EndPoint.File;
					Rec.StorageBlobEndpoint  = EndPoint.Blob;
				}
				else
				{
					Rec = new EndPoint
					      {
						      CarrierId            = Cid,
						      StorageAccount       = AccountName,
						      StorageAccountKey    = AccountKey,
						      StorageTableEndpoint = EndPoint.Table,
						      StorageFileEndpoint  = EndPoint.File,
						      StorageBlobEndpoint  = EndPoint.Blob
					      };

					Db.EndPoints.Add( Rec );
				}

				Db.SaveChanges();

				return Rec;
			}

			return null;
		}

		public static EndPoint? AddStorageEndPoint( IRequestContext context )
		{
			var Retval = GetEndPoints( context );

			return string.IsNullOrEmpty( Retval?.StorageTableEndpoint ) ? CreateStorageEndPoint( context, StorageAccount.CreateBlobStorageAccount( context ) ) : Retval;
		}
	}
}