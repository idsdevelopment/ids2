﻿using System;
using System.Linq;
using Database.Model.Databases.Users;
using Interfaces.Interfaces;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public const int PROCESS_LIMIT_IN_MINUTES = 60 * 24;

		private static long _TimeLimitTicks;

		public static long TimeLimitTicks
		{
			get
			{
				if( _TimeLimitTicks <= 0 )
					_TimeLimitTicks = new TimeSpan( 0, PROCESS_LIMIT_IN_MINUTES, 0 ).Ticks;

				return _TimeLimitTicks;
			}
		}

		private static void RemoveExpired( UsersEntities db, long nowTicks )
		{
			var Processes = db.Processes;

			Processes.RemoveRange( from P in Processes
			                       where ( nowTicks - P.TimeStarted ) >= TimeLimitTicks
			                       select P );
		}

		public static long StartProcess( IRequestContext context )
		{
			try
			{
				using var Db = new UsersEntities();

				var Processes = Db.Processes;
				var NowTicks  = DateTime.UtcNow.Ticks;

				var Proc = new Process
				           {
					           TimeStarted    = NowTicks,
					           Processing     = true,
					           ResponseString = ""
				           };

				Processes.Add( Proc );
				RemoveExpired( Db, NowTicks );
				Db.SaveChanges();

				return Proc.ProcessId;
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return 0;
		}

		public static void EndProcess( IRequestContext context, long processId )
		{
			try
			{
				using var Db = new UsersEntities();

				var Processes = Db.Processes;

				var Proc = ( from P in Processes
				             where P.ProcessId == processId
				             select P ).FirstOrDefault();

				if( Proc != null )
					Processes.Remove( Proc );
				RemoveExpired( Db, DateTime.UtcNow.Ticks );
				Db.SaveChanges();
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}
		}

		public static void EndProcess( IRequestContext context, long processId, string response )
		{
			try
			{
				using var Db = new UsersEntities();

				var Processes = Db.Processes;

				var Proc = ( from P in Processes
				             where P.ProcessId == processId
				             select P ).FirstOrDefault();

				if( Proc != null )
				{
					Proc.Processing     = false;
					Proc.ResponseString = response;
				}

				RemoveExpired( Db, DateTime.UtcNow.Ticks );
				Db.SaveChanges();
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}
		}


		public static bool IsProcessRunning( IRequestContext context, long processId )
		{
			try
			{
				using var Db = new UsersEntities();

				return ( from P in Db.Processes
				         where ( P.ProcessId == processId ) && P.Processing
				         select P ).FirstOrDefault() != null;
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return false;
		}

		public static string GetProcessingResponse( IRequestContext context, long processId )
		{
			var Result = "";

			try
			{
				using var Db = new UsersEntities();

				var Processes = Db.Processes;

				var Rec = ( from P in Processes
				            where P.ProcessId == processId
				            select P ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					Result = Rec.ResponseString;

					if( !Rec.Processing )
					{
						Processes.Remove( Rec );
						Db.SaveChanges();
					}
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return Result;
		}
	}
}