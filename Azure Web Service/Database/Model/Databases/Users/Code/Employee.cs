﻿using System;
using System.Linq;
using Database.Model.Databases.Users;
using Interfaces.Interfaces;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public static Employee Employee( IRequestContext context, string code )
		{
			try
			{
				using var Db = new UsersEntities();

				code = code.ToLower();

				return ( from E in Db.Employees
				         where E.Code.ToLower() == code
				         select E ).FirstOrDefault();
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return null;
		}
	}
}