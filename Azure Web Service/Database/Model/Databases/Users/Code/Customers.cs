﻿using System;
using System.Linq;
using System.Text;
using Database.Model.Databases.Users;
using Interfaces.Interfaces;
using Protocol.Data;
using Utils;

namespace Database.Model.Database
{
	public static partial class Users
	{
		public static ( string CarrierId, string userName, string password, bool Ok ) GetResellerFromLoginCode( string loginCode )
		{
			var Retval = ( CarrierId: "", UserName: "", Password: "", Ok: false );

			using( var Db = new UsersEntities() )
			{
				var Cust = ( from C in Db.ResellerCustomers
				             where C.LoginCode == loginCode
				             select C ).FirstOrDefault();

				if( Cust != null )
				{
					Retval.CarrierId = Cust.ResellerId;
					Retval.UserName  = Cust.UserName;
					Retval.Password  = Cust.Password;
					Retval.Ok        = true;
				}
			}

			return Retval;
		}

		public static string Take3( this string value )
		{
			return value.Substring( 0, Math.Min( 3, value.Length ) );
		}

		public static bool CanFormInteger( this string value )
		{
			var Temp = new StringBuilder();

			foreach( var C in value )
			{
				if( ( C >= '0' ) && ( C <= '9' ) )
					Temp.Append( C );
			}

			return Temp.ToString().IsInteger();
		}

		public static bool HasResellerLoginCode( string loginCode )
		{
			using var Db = new UsersEntities();

			return ( from C in Db.ResellerCustomers
			         where C.LoginCode == loginCode
			         select C ).FirstOrDefault().IsNotNull();
		}


		public static bool HasCompanyLoginCode( string company, string loginCode )
		{
			using var Db = new UsersEntities();

			return ( from C in Db.ResellerCustomers
			         where ( C.CustomerCode == company ) && ( C.LoginCode == loginCode )
			         select C ).FirstOrDefault().IsNotNull();
		}

		public static string GenerateResellerLoginCode( IRequestContext context, string firstName, string lastName, string addressLine1 )
		{
			try
			{
				firstName    = firstName.Trim();
				lastName     = lastName.Trim();
				addressLine1 = addressLine1.Trim();

				var F     = firstName.Take3();
				var L     = lastName.Take3();
				var Count = 0;
				var A     = "";

				using var Db = new UsersEntities();

				var Rc = Db.ResellerCustomers;

				while( true )
				{
					var Code = $"{F}{L}{A}";

					if( Count > 0 )
						Code += Count.ToString();

					var Rec = ( from R in Rc
					            where R.LoginCode == Code
					            select R ).FirstOrDefault();

					if( Rec == null )
						return Code;

					// Try adding Address portion to Code
					if( string.IsNullOrEmpty( A ) )
					{
						var P = addressLine1.IndexOf( ' ' );

						if( P > 0 )
							A = addressLine1.Take3();

						if( A.CanFormInteger() ) // Skip Address Number
						{
							A = addressLine1.Substring( P ).Trim().Take3();

							if( string.IsNullOrEmpty( A ) )
								++Count;
						}
						else
							++Count;
					}
					else
						++Count;
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return "";
		}

		public static string GenerateResellerLoginCode( IRequestContext context, Company address )
		{
			string F,
			       L,
			       Name = address.CompanyName;

			var CoName = Name.Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );
			var Len    = CoName.Length;

			if( Len >= 2 )
			{
				F = CoName[ 0 ];
				L = CoName[ 1 ];
			}
			else if( CoName.Length >= 2 )
			{
				var Middle = CoName.Length / 2;

				F = Name.Substring( 0, Middle );
				L = Name.Substring( Middle );
			}
			else
			{
				F = address.CompanyName;
				L = new Random().Next( 100 ).ToString();
			}

			return GenerateResellerLoginCode( context, F, L, address.AddressLine1 );
		}

		public static (string Code, bool Ok) AddResellerLoginCode( IRequestContext context, AddUpdateCustomer company )
		{
			return AddResellerLoginCode( context, company.CustomerCode, company.SuggestedLoginCode, company.Company );
		}

		public static (string Code, bool Ok) AddResellerLoginCode( IRequestContext context, string customerCode, string suggestedCode, Company company )
		{
			var Code = "";
			var Ok   = false;

			try
			{
				Code = suggestedCode;
				var Co = company;

				if( HasCompanyLoginCode( Co.UserName, Code ) )
					Ok = true;
				else
				{
					if( string.IsNullOrEmpty( Code ) || HasResellerLoginCode( Code ) )
						Code = GenerateResellerLoginCode( context, Co );

					using var Db  = new UsersEntities();
					var       RCo = Db.ResellerCustomers;

					var Rec = ( from R in RCo
					            where R.LoginCode == Code
					            select R ).FirstOrDefault();

					var Rid = context.CarrierId;

					if( Rec is null )
					{
						// Multiple unique keys
						Rec = ( from R in RCo
						        where ( R.ResellerId == Rid ) && ( R.CustomerCode == customerCode )
						        select R ).FirstOrDefault();

						if( Rec is null )
						{
							Db.ResellerCustomers.Add( new ResellerCustomer
							                          {
								                          CustomerCode = customerCode,
								                          LoginCode    = Code,
								                          Password     = Co.Password,
								                          UserName     = company.UserName,
								                          ResellerId   = Rid,
														  OriginalLoginCode = ""
							                          } );
						}
						else
						{
							Rec.LoginCode = Code;
							Rec.Password  = Co.Password;
						}
					}
					else
						Rec.Password = Co.Password;

					Db.SaveChanges();
					Ok = true;
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return ( Code, Ok );
		}

		public static bool DeleteResellerLoginCodeByCustomerCode( IRequestContext context, string resellerCustomerCode )
		{
			try
			{
				using var Db = new UsersEntities();

				var Rec = ( from Rc in Db.ResellerCustomers
				            where ( Rc.ResellerId == context.CarrierId ) && ( Rc.CustomerCode == resellerCustomerCode )
				            select Rc ).FirstOrDefault();

				if( Rec != null )
				{
					Db.ResellerCustomers.Remove( Rec );
					Db.SaveChanges();

					return true;
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return false;
		}

		public static bool DeleteAllResellerCustomers( IRequestContext context, string resellerId )
		{
			try
			{
				using var Db = new UsersEntities();

				Db.Database.ExecuteSqlCommand( "DELETE FROM dbo.ResellerCustomers WHERE ResellerId = {0}", resellerId );

				return true;
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}

			return false;
		}

		public static bool DeleteAllResellerCustomers( IRequestContext context )
		{
			return DeleteAllResellerCustomers( context, context.CarrierId );
		}
	}
}