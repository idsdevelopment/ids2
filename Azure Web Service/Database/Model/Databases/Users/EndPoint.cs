//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database.Model.Databases.Users
{
    using System;
    using System.Collections.Generic;
    
    public partial class EndPoint
    {
        public string CarrierId { get; set; }
        public string StorageAccount { get; set; }
        public string StorageAccountKey { get; set; }
        public string StorageTableEndpoint { get; set; }
        public string StorageFileEndpoint { get; set; }
        public string StorageBlobEndpoint { get; set; }
    }
}
