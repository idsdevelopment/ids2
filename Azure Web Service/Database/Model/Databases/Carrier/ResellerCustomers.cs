﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Interfaces.Interfaces;
using Protocol.Data;
using Utils;
using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Database.Model.Databases.MasterTemplate.Company;
using CompanyAddress = Protocol.Data.CompanyAddress;
using ResellerCustomerCompany = Database.Model.Databases.MasterTemplate.ResellerCustomerCompany;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public class ResellerCustomerLogger
		{
			public const string LOG_STORAGE = "Customer";

			private static CompanyLogger _BLANK_COMPANY_LOGGER;

			// ReSharper disable once InconsistentNaming
			internal static CompanyLogger BLANK_COMPANY_LOGGER => _BLANK_COMPANY_LOGGER.IsNull() ? _BLANK_COMPANY_LOGGER = new CompanyLogger( new Company(), new AddressLogger( new Address() ) ) : _BLANK_COMPANY_LOGGER;

			public string CustomerCode { get; set; }
			public string LoginCode { get; set; } = "";
			public string UserName { get; set; } = "";
			public string Password { get; set; } = "";
			public long CompanyId { get; set; }
			public long BillingCompanyId { get; set; }
			public string CompanyName { get; set; } = "";

			public CompanyLogger Address { get; set; } = BLANK_COMPANY_LOGGER;
			public CompanyLogger BillingAddress { get; set; } = BLANK_COMPANY_LOGGER;
			public List<CompanyLogger> CompanyAddresses { get; set; }


			public ResellerCustomerLogger( ResellerCustomer c )
			{
				CustomerCode     = c.CustomerCode;
				LoginCode        = c.LoginCode;
				UserName         = c.UserName;
				Password         = c.Password;
				CompanyId        = c.CompanyId;
				BillingCompanyId = c.BillingCompanyId;
				CompanyName      = c.CompanyName;
			}

			public ResellerCustomerLogger( ResellerCustomer c, CompanyLogger address, CompanyLogger billingAddress, List<CompanyLogger> companyAddresses )
				: this( c )
			{
				Address          = address;
				BillingAddress   = billingAddress;
				CompanyAddresses = companyAddresses;
			}


			public ResellerCustomerLogger( string c )
			{
				CustomerCode = c;
			}
		}

		private static ResellerCustomerCompanyLookup ConvertToCompanyLookup( string customerCode, string companyName, string locationBarcode, Address a )
		{
			return new ResellerCustomerCompanyLookup
			       {
				       Ok              = true,
				       CustomerCode    = customerCode ?? "",
				       CompanyName     = companyName ?? "",
				       LocationBarcode = locationBarcode ?? "",
				       AddressLine1    = a.AddressLine1 ?? "",
				       AddressLine2    = a.AddressLine2 ?? "",
				       Barcode         = a.Barcode ?? "",
				       City            = a.City ?? "",
				       Suite           = a.Suite ?? "",
				       SecondaryId     = a.SecondaryId ?? "",
				       Country         = a.Country ?? "",
				       CountryCode     = a.CountryCode ?? "",
				       EmailAddress    = a.EmailAddress ?? "",
				       EmailAddress1   = a.EmailAddress1 ?? "",
				       EmailAddress2   = a.EmailAddress2 ?? "",
				       Fax             = a.Fax ?? "",
				       Latitude        = a.Latitude,
				       Longitude       = a.Longitude,
				       Mobile          = a.Mobile ?? "",
				       Mobile1         = a.Mobile1 ?? "",
				       Notes           = a.Notes ?? "",
				       Phone           = a.Phone ?? "",
				       Phone1          = a.Phone1 ?? "",
				       PostalBarcode   = a.PostalBarcode ?? "",
				       PostalCode      = a.PostalCode ?? "",
				       Region          = a.Region ?? "",
				       Vicinity        = a.Vicinity ?? ""
			       };
		}

		public static bool HasCarrierId( IRequestContext context )
		{
			return Database.Users.HasEndpoints( context );
		}

		public void DeleteAllResellerCustomers()
		{
			try
			{
				Entity.Database.ExecuteSqlCommand( "DELETE FROM dbo.ResellerCustomers" );
				Database.Users.DeleteAllResellerCustomers( Context, Context.CarrierId );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( DeleteAllResellerCustomers ), E );
			}
		}

		public CustomerLookupSummary CustomerSummaryLookup( string customerAccountCode )
		{
			var Rec = ( from C in Entity.ResellerCustomers
			            where C.CustomerCode == customerAccountCode
			            select C ).FirstOrDefault();

			if( Rec != null )
			{
				var Company = GetCompany( Rec.CompanyId ).Company;
				var Address = GetCompanyPrimaryAddress( Company ).Address;

				return new CustomerLookupSummary
				       {
					       CustomerCode       = Rec.CustomerCode,
					       CompanyName        = Company.CompanyName,
					       DisplayCompanyName = Company.DisplayCompanyName,
					       AddressLine1       = Address.AddressLine1,
					       BoolOption1        = Rec.BoolOption1
				       };
			}

			return new CustomerLookupSummary { Found = false };
		}


		public CustomerLookupSummaryList CustomersLookupByAccount( CustomerLookup lookup )
		{
			var RetVal = new CustomerLookupSummaryList();

			try
			{
				var Key   = lookup.Key;
				var Count = lookup.PreFetchCount;

				IEnumerable<ResellerCustomer> Result;

				switch( lookup.Fetch )
				{
				case PreFetch.PREFETCH.FIRST:
					Result = ( from C in Entity.ResellerCustomers
					           orderby C.CustomerCode
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.LAST:
					Result = ( from C in Entity.ResellerCustomers
					           orderby C.CustomerCode descending
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_MATCH:
					Result = ( from C in Entity.ResellerCustomers
					           where C.CustomerCode.StartsWith( Key )
					           orderby C.CustomerCode
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_RANGE:
					if( Count >= 0 ) // Next
					{
						Result = ( from C in Entity.ResellerCustomers
						           where C.CustomerCode.CompareTo( Key ) >= 0
						           orderby C.CustomerCode
						           select C ).Take( Count );
					}
					else // Prior
					{
						Result = ( from C in Entity.ResellerCustomers
						           where C.CustomerCode.CompareTo( Key ) <= 0
						           orderby C.CustomerCode
						           select C ).Take( -Count );
					}

					break;

				default:
					Context.SystemLogException( nameof( CustomersLookup ), new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

					return RetVal;
				}

				RetVal.AddRange( from C in Result
				                 let Company = GetCompany( C.CompanyId ).Company
				                 let Address = GetCompanyPrimaryAddress( Company ).Address
				                 select new CustomerLookupSummary
				                        {
					                        CustomerCode       = C.CustomerCode,
					                        CompanyName        = Company.CompanyName,
					                        DisplayCompanyName = Company.DisplayCompanyName,
					                        AddressLine1       = Address.AddressLine1,
					                        BoolOption1        = C.BoolOption1
				                        } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( CustomersLookup ), E );
			}

			return RetVal;
		}

		public CustomerLookupSummaryList CustomersLookupByCompanyName( CustomerLookup lookup )
		{
			var RetVal = new CustomerLookupSummaryList();

			try
			{
				var Key   = lookup.Key;
				var Count = lookup.PreFetchCount;

				IEnumerable<ResellerCustomer> Result;

				switch( lookup.Fetch )
				{
				case PreFetch.PREFETCH.FIRST:
					Result = ( from C in Entity.ResellerCustomers
					           orderby C.CompanyName
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.LAST:
					Result = ( from C in Entity.ResellerCustomers
					           orderby C.CustomerCode descending
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_MATCH:
					Result = ( from C in Entity.ResellerCustomers
					           where C.CustomerCode.StartsWith( Key )
					           orderby C.CustomerCode
					           select C ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_RANGE:
					if( Count >= 0 ) // Next
					{
						Result = ( from C in Entity.ResellerCustomers
						           where C.CustomerCode.CompareTo( Key ) >= 0
						           orderby C.CustomerCode
						           select C ).Take( Count );
					}
					else // Prior
					{
						Result = ( from C in Entity.ResellerCustomers
						           where C.CustomerCode.CompareTo( Key ) <= 0
						           orderby C.CustomerCode
						           select C ).Take( -Count );
					}

					break;

				default:
					Context.SystemLogException( nameof( CustomersLookup ), new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

					return RetVal;
				}

				RetVal.AddRange( from C in Result
				                 let Cs = ( from C1 in Entity.ResellerCustomerCompanySummaries
				                            where C1.CompanyName == C.CompanyName
				                            select C1 ).FirstOrDefault()
				                 where Cs != null
				                 select new CustomerLookupSummary
				                        {
					                        CustomerCode       = C.CustomerCode,
					                        DisplayCompanyName = Cs.DisplayCompanyName,
					                        CompanyName        = C.CompanyName,
					                        AddressLine1       = Cs.AddressLine1
				                        } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( CustomersLookup ), E );
			}

			return RetVal;
		}

		public CustomerLookupSummaryList CustomersLookup( CustomerLookup lookup )
		{
			return lookup.ByAccountNumber ? CustomersLookupByAccount( lookup ) : CustomersLookupByCompanyName( lookup );
		}

		public bool DeleteResellerCustomer( string customerCode, string programName )
		{
			try
			{
				var E = Entity;
				var R = E.ResellerCustomers;

				var Rec = ( from Rc in R
				            where Rc.CustomerCode == customerCode
				            select Rc ).FirstOrDefault();

				if( Rec != null )
				{
					R.Remove( Rec );
					E.SaveChanges();
					Context.LogDeleted( ResellerCustomerLogger.LOG_STORAGE, programName, "", new ResellerCustomerLogger( customerCode ) );
				}

				return Database.Users.DeleteResellerLoginCodeByCustomerCode( Context, customerCode );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( DeleteResellerCustomer ), E );
			}

			return false;
		}


		public static void AddCustCompany( MasterTemplateEntities entity, long cid, long acid, string custCode, CompanyBase company )
		{
			if( custCode.IsNotNullOrWhiteSpace() )
			{
				entity.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
				                                      {
					                                      CustomerCode = custCode,
					                                      CompanyId    = cid
				                                      } );

				entity.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
				                                             {
					                                             CompanyId          = cid,
					                                             AddressId          = acid,
					                                             CompanyName        = ( company.CompanyName ?? "" ).ToUpper(),
					                                             LocationBarcode    = company.LocationBarcode ?? "",
					                                             DisplayCompanyName = company.CompanyName ?? "",
					                                             CustomerCode       = custCode ?? "",
					                                             Suite              = company.Suite ?? "",
					                                             AddressLine1       = company.AddressLine1 ?? "",
					                                             AddressLine2       = company.AddressLine2 ?? "",
					                                             City               = company.City ?? "",
					                                             Region             = company.Region ?? "",
					                                             PostalCode         = company.PostalCode ?? "",
					                                             CountryCode        = company.CountryCode ?? ""
				                                             } );
				entity.SaveChanges();
			}
		}


		public string AddUpdateResellerCustomer( AddUpdateCustomer customer )
		{
			try
			{
				var (Password, Ok) = Encryption.FromTimeLimitedToken( customer.Password );

				if( Ok )
				{
					var ProgramName = customer.ProgramName;
					var E           = Entity;
					var Rc          = E.ResellerCustomers;

					var Rec = ( from C in Rc
					            where C.CustomerCode == customer.CustomerCode
					            select C ).FirstOrDefault();

					void AddCustCompany( long cid, long acid, string custCode, CompanyBase company )
					{
						CarrierDb.AddCustCompany( E, cid, acid, custCode, company );
					}

					if( Rec is null )
					{
						// Generate unique login code system wide
						var (Code, CodeOk) = Database.Users.AddResellerLoginCode( Context, customer );

						if( CodeOk )
						{
							// Add primary company address
							var (Company, AddressLog, CompanyOk) = AddCompany( customer.Company, ProgramName );

							if( CompanyOk )
							{
								var  CId         = Company.CompanyId;
								var  Aid         = Company.PrimaryAddressId;
								long BId         = 0;
								var  BAddressLog = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );

								// Do Bill To Company
								if( customer.BillingCompany.IsNotNull() && customer.BillingCompany.CompanyNumber.IsNotNullOrWhiteSpace() )
								{
									var (BCompany, BAddressLog1, BCompanyOk) = AddCompany( customer.BillingCompany, ProgramName );

									if( BCompanyOk )
									{
										BId         = BCompany.CompanyId;
										BAddressLog = BAddressLog1;
									}
								}

								var Cust = new ResellerCustomer
								           {
									           CustomerCode     = customer.CustomerCode ?? "",
									           LoginCode        = Code ?? "",
									           Password         = Password,
									           UserName         = customer.UserName ?? "",
									           CompanyId        = CId,
									           BillingCompanyId = BId,
									           CompanyName      = Company.CompanyName ?? "",
									           StringOption1    = "",
									           StringOption2    = "",
									           StringOption3    = "",
									           StringOption4    = "",
									           StringOption5    = ""
								           };

								Rc.Add( Cust );

								AddCustCompany( CId, Aid, Code, customer.Company );

								var CompanyAddresses = new List<CompanyLogger>();

								if( customer.Companies.IsNotNull() )
								{
									var CustId = Cust.CustomerCode;

									foreach( var CustomerCompany in customer.Companies )
									{
										var (Co, __, CcOk) = AddCompany( CustomerCompany, ProgramName );

										if( CcOk )
										{
											AddCustCompany( Co.CompanyId, Co.PrimaryAddressId, Co.CompanyName, CustomerCompany );
											var Id = Co.CompanyId;

											E.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
											                                 {
												                                 CustomerCode = CustId ?? "",
												                                 CompanyId    = Id
											                                 } );

											E.SaveChanges();
										}
									}
								}

								Context.LogNew( ResellerCustomerLogger.LOG_STORAGE, ProgramName, "Add new customer", new ResellerCustomerLogger( Cust, AddressLog, BAddressLog, CompanyAddresses ) );
							}
						}

						return Code;
					}

					var (RCompany, CoOk) = GetCompany( Rec.CompanyId );

					if( CoOk )
					{
						var (CoBefore, CoAfter, CoOk1) = UpdateCompany( RCompany.CompanyId, customer.Company, ProgramName );

						if( CoOk1 )
						{
							var BCompany = GetCompany( Rec.BillingCompanyId );
							var (BBefore, BAfter, _) = UpdateCompany( BCompany.Company.CompanyId, customer.BillingCompany, ProgramName );

							var Before = new ResellerCustomerLogger( Rec, CoBefore, BBefore, new List<CompanyLogger>() );
							var After  = new ResellerCustomerLogger( Rec, CoAfter, BAfter, new List<CompanyLogger>() );
							var Added  = new List<CompanyLogger>();

							if( customer.Companies.IsNotNull() )
							{
								var CustId = Rec.CustomerCode.ToUpper();

								foreach( var CustomerCompany in customer.Companies )
								{
									var RRec = ( from R in E.ResellerCustomerCompanies
									             where R.CustomerCode == CustId
									             select R ).FirstOrDefault();

									if( RRec is null )
									{
										var (_, RCoLogger, Rok) = AddCompany( CustomerCompany, ProgramName );

										if( Rok )
											Added.Add( RCoLogger );
									}
									else
									{
										var (UBefore, UAfter, UOk) = UpdateCompany( RRec.CompanyId, CustomerCompany, ProgramName );

										if( UOk )
										{
											Before.CompanyAddresses.Add( UBefore );
											After.CompanyAddresses.Add( UAfter );
										}
									}
								}
							}

							After.CompanyAddresses.AddRange( Added );
							Context.LogDifferences( ResellerCustomerLogger.LOG_STORAGE, ProgramName, "Add/Update customer", Before, After );
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return "";
		}

		public string AddResellerCustomer( AddUpdateCustomer customer )
		{
			var PName = customer.ProgramName;
			DeleteResellerCustomer( customer.CustomerCode, PName );
			var ( Code, Ok ) = Database.Users.AddResellerLoginCode( Context, customer );

			if( Ok )
			{
				var Company = AddCompany( customer.Company, PName );

				if( Company.Ok )
				{
					var CId = Company.Company.CompanyId;
					var Aid = Company.Company.PrimaryAddressId;

					long BId = 0;

					if( customer.BillingCompany != null )
					{
						Company = AddCompany( customer.BillingCompany, PName );

						if( Company.Ok )
							BId = Company.Company.CompanyId;
					}

					var E = Entity;

					var Cust = new ResellerCustomer
					           {
						           CustomerCode     = customer.CustomerCode ?? "",
						           LoginCode        = Code ?? "",
						           Password         = customer.Password ?? "",
						           UserName         = customer.UserName ?? "",
						           CompanyId        = CId,
						           BillingCompanyId = BId,
						           CompanyName      = Company.Company.CompanyName ?? "",
						           StringOption1    = "",
						           StringOption2    = "",
						           StringOption3    = "",
						           StringOption4    = "",
						           StringOption5    = ""
					           };

					E.ResellerCustomers.Add( Cust );

					E.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
					                                 {
						                                 CustomerCode = Code ?? "",
						                                 CompanyId    = CId
					                                 } );
					var CustCo = customer.Company;

					E.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
					                                        {
						                                        CompanyId          = CId,
						                                        AddressId          = Aid,
						                                        CompanyName        = Company.Company.CompanyName ?? "",
						                                        LocationBarcode    = Company.Company.LocationBarcode ?? "",
						                                        DisplayCompanyName = Company.Company.DisplayCompanyName ?? "",
						                                        CustomerCode       = customer.CustomerCode ?? "",
						                                        Suite              = CustCo.Suite ?? "",
						                                        AddressLine1       = CustCo.AddressLine1 ?? "",
						                                        AddressLine2       = CustCo.AddressLine2 ?? "",
						                                        City               = CustCo.City ?? "",
						                                        Region             = CustCo.Region ?? "",
						                                        PostalCode         = CustCo.PostalCode ?? "",
						                                        CountryCode        = CustCo.CountryCode ?? ""
					                                        } );
					E.SaveChanges();

					if( ( customer.Companies != null ) && ( customer.Companies.Count > 0 ) )
					{
						var CustId = Cust.CustomerCode;

						new Tasks.Task<Protocol.Data.Company>().Run( customer.Companies, company =>
						                                                                 {
							                                                                 try
							                                                                 {
								                                                                 using( var Db = new CarrierDb( Context ) )
								                                                                 {
									                                                                 var E1 = Db.Entity;

									                                                                 var (Co, _, CcOk) = Db.AddCompany( company, PName );

									                                                                 if( CcOk )
									                                                                 {
										                                                                 var Id = Co.CompanyId;

										                                                                 E1.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
										                                                                                                   {
											                                                                                                   CustomerCode = CustId ?? "",
											                                                                                                   CompanyId    = Id
										                                                                                                   } );

										                                                                 E1.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
										                                                                                                          {
											                                                                                                          CompanyId          = Id,
											                                                                                                          AddressId          = Co.PrimaryAddressId,
											                                                                                                          CompanyName        = Co.CompanyName ?? "",
											                                                                                                          LocationBarcode    = Co.LocationBarcode ?? "",
											                                                                                                          DisplayCompanyName = Co.DisplayCompanyName ?? "",
											                                                                                                          CustomerCode       = CustId ?? "",
											                                                                                                          Suite              = company.Suite ?? "",
											                                                                                                          AddressLine1       = company.AddressLine1 ?? "",
											                                                                                                          AddressLine2       = company.AddressLine2 ?? "",
											                                                                                                          City               = company.City ?? "",
											                                                                                                          Region             = company.Region ?? "",
											                                                                                                          PostalCode         = company.PostalCode ?? "",
											                                                                                                          CountryCode        = company.CountryCode ?? ""
										                                                                                                          } );

										                                                                 E1.SaveChanges();
									                                                                 }
								                                                                 }
							                                                                 }
							                                                                 catch( Exception Exception )
							                                                                 {
								                                                                 Context.SystemLogException( $"{nameof( AddResellerCustomer )} - Task", Exception );
							                                                                 }
						                                                                 }, 2 );
					}

					return Code;
				}
			}

			return "";
		}

		public CompanyByAccountAndCompanyNameList FindMissingCompanies( CompanyByAccountAndCompanyNameList companyNameList )
		{
			var RetVal = new CompanyByAccountAndCompanyNameList();

			try

			{
				// Simplify the lookups
				var CoNames = new Dictionary<string, HashSet<string>>(); // CompanyName CustomerCode

				foreach( var CName in companyNameList )
				{
					var CompanyName = CName.CompanyName.ToUpper();

					if( !CoNames.TryGetValue( CompanyName, out var CustHashSet ) )
						CoNames.Add( CompanyName, CustHashSet = new HashSet<string>() );
					CustHashSet.Add( CName.CustomerCode );
				}

				var Companies  = Entity.Companies;
				var ResellerCo = Entity.ResellerCustomerCompanies;

				// Walk the company names
				foreach( var CoName in CoNames )
				{
					var CompanyName = CoName.Key;
					var Customers   = CoName.Value;

					var CoRecs = from Comp in Companies
					             where Comp.CompanyName == CompanyName
					             select Comp;

					if( !CoRecs.Any() ) // Company name not found
					{
						RetVal.AddRange( from CustomerCode in Customers
						                 select new CompanyByAccountAndCompanyName
						                        {
							                        CustomerCode = CustomerCode ?? "",
							                        CompanyName  = CompanyName ?? ""
						                        } );
					}
					else
					{
						foreach( var CustomerCode in Customers ) // HashSet of Customers
						{
							var Found = false;

							foreach( var CoRec in CoRecs ) // All the found Companies by Names
							{
								Found = ( from R in ResellerCo
								          where ( R.CustomerCode == CustomerCode ) && ( R.CompanyId == CoRec.CompanyId )
								          select R ).FirstOrDefault() != null;

								if( Found )
									break; // Not missing
							}

							if( !Found )
							{
								RetVal.Add( new CompanyByAccountAndCompanyName
								            {
									            CustomerCode = CustomerCode ?? "",
									            CompanyName  = CompanyName ?? ""
								            } );
							}
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( FindMissingCompanies ), Exception );
			}

			return RetVal;
		}

		public void AddCustomerCompany( AddCustomerCompany customerCompany )
		{
			var Rc = ( from R in Entity.ResellerCustomers
			           where R.CustomerCode == customerCompany.CustomerCode
			           select R ).FirstOrDefault();

			if( Rc != null )
			{
				try
				{
					var (Comp, _, Ok) = AddCompany( customerCompany.Company, customerCompany.ProgramName );

					if( Ok )
					{
						var Cid      = Comp.CompanyId;
						var CustCode = customerCompany.CustomerCode;

						Entity.ResellerCustomerCompanies.Add( new ResellerCustomerCompany
						                                      {
							                                      CompanyId    = Cid,
							                                      CustomerCode = CustCode
						                                      } );
						var Cc = customerCompany.Company;

						Entity.ResellerCustomerCompanySummaries.Add( new ResellerCustomerCompanySummary
						                                             {
							                                             CompanyId          = Cid,
							                                             CompanyName        = Comp.CompanyName ?? "",
							                                             LocationBarcode    = Comp.LocationBarcode ?? "",
							                                             DisplayCompanyName = Comp.DisplayCompanyName ?? "",
							                                             AddressId          = Comp.PrimaryAddressId,
							                                             CustomerCode       = CustCode ?? "",
							                                             Suite              = Cc.Suite ?? "",
							                                             AddressLine1       = Cc.AddressLine1 ?? "",
							                                             Region             = Cc.Region ?? "",
							                                             City               = Cc.City ?? "",
							                                             AddressLine2       = Cc.AddressLine2 ?? "",
							                                             CountryCode        = Cc.CountryCode ?? "",
							                                             PostalCode         = Cc.PostalCode ?? ""
						                                             } );
						Entity.SaveChanges();
					}
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( nameof( AddCustomerCompany ), Exception );
				}
			}
		}

		public void DeleteCustomerCompany( DeleteCustomerCompany customerCompany )
		{
			var Rc = ( from R in Entity.ResellerCustomers
			           where R.CustomerCode == customerCompany.CustomerCode
			           select R ).FirstOrDefault();

			if( Rc != null )
			{
				try
				{
/*					var Companies   = Entity.Companies;
					var ResellerCo  = Entity.ResellerCustomerCompanies;
					var CoAddresses = Entity.CompanyAddresses;
					var Addresses   = Entity.Addresses;
*/
					var CompanyName  = customerCompany.Company.CompanyName;
					var CustomerCode = customerCompany.CustomerCode;

					DeleteCustomerCompanyAddress( CustomerCode, CompanyName, customerCompany.ProgramName );
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( nameof( DeleteCustomerCompany ), Exception );
				}
			}
		}

		public void UpdateCustomerCompany( UpdateCustomerCompany customerCompany )
		{
			var Rc = ( from R in Entity.ResellerCustomers
			           where R.CustomerCode == customerCompany.CustomerCode
			           select R ).FirstOrDefault();

			if( Rc != null )
			{
				try
				{
					//var Companies = Entity.Companies;
					//var ResellerCo = Entity.ResellerCustomerCompanies;
					//var CoAddresses = Entity.CompanyAddresses;
					//var Addresses = Entity.Addresses;

					//var CompanyName  = customerCompany.Company.CompanyName;
					//var CustomerCode = customerCompany.CustomerCode;

					UpdateCustomerCompanyAddress( customerCompany );
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( nameof( UpdateCustomerCompany ), Exception );
				}
			}
		}

		public CustomerCompaniesList GetCustomerCompanyAddress( CompanyByAccountAndCompanyNameList companyNameList )
		{
			var RetVal = new CustomerCompaniesList();

			try

			{
				var CustomerWithinCompanyName = new Dictionary<string, HashSet<string>>();

				foreach( var CName in companyNameList )
				{
					var CompanyName = CName.CompanyName.ToUpper();

					if( !CustomerWithinCompanyName.TryGetValue( CompanyName, out var CustHashSet ) )
						CustomerWithinCompanyName.Add( CompanyName, CustHashSet = new HashSet<string>() );
					CustHashSet.Add( CName.CustomerCode );
				}

				var Companies   = Entity.Companies;
				var ResellerCo  = Entity.ResellerCustomerCompanies;
				var CoAddresses = Entity.CompanyAddresses;
				var Addresses   = Entity.Addresses;

				foreach( var Kv in CustomerWithinCompanyName )
				{
					var CompanyName = Kv.Key;

// Find all with the same CompanyName
					var CoRecs = from Co in Companies
					             where Co.CompanyName == CompanyName
					             select Co;

// Get company Record for customer
					foreach( var CoRec in CoRecs )
					{
						foreach( var CustomerCode in Kv.Value )
						{
							var Rec = ( from R in ResellerCo
							            where ( R.CompanyId == CoRec.CompanyId ) && ( R.CustomerCode == CustomerCode )
							            select R ).FirstOrDefault();

							if( Rec != null )
							{
								var CoAddrs = from Ca in CoAddresses
								              where Ca.CompanyId == CoRec.CompanyId
								              select Ca;

								if( CoAddrs.Any() )
								{
									var Cc = new CustomerCompanies
									         {
										         CustomerCode = CustomerCode
									         };
									RetVal.Add( Cc );

									foreach( var CoAddr in CoAddrs )
									{
										var Addr = ( from A in Addresses
										             where A.Id == CoAddr.AddressId
										             select A ).FirstOrDefault();

										if( Addr != null )
										{
											Cc.Companies.Add( new Protocol.Data.Company
											                  {
												                  CompanyNumber   = CoRec.CompanyNumber ?? "",
												                  CompanyName     = CoRec.DisplayCompanyName ?? "",
												                  LocationBarcode = CoRec.LocationBarcode ?? "",
												                  UserName        = CoRec.UserName ?? "",
												                  Password        = CoRec.Password ?? "",
												                  Barcode         = Addr.Barcode ?? "",
												                  Suite           = Addr.Suite ?? "",
												                  AddressLine1    = Addr.AddressLine1 ?? "",
												                  AddressLine2    = Addr.AddressLine2 ?? "",
												                  Vicinity        = Addr.Vicinity ?? "",
												                  City            = Addr.City ?? "",
												                  Country         = Addr.Country ?? "",
												                  CountryCode     = Addr.CountryCode ?? "",
												                  PostalBarcode   = Addr.PostalBarcode ?? "",
												                  Region          = Addr.Region ?? "",
												                  PostalCode      = Addr.PostalCode ?? "",
												                  Latitude        = Addr.Latitude,
												                  Longitude       = Addr.Longitude,
												                  Phone           = Addr.Phone ?? "",
												                  Phone1          = Addr.Phone1 ?? "",
												                  EmailAddress    = Addr.EmailAddress ?? "",
												                  EmailAddress1   = Addr.EmailAddress1 ?? "",
												                  EmailAddress2   = Addr.EmailAddress2 ?? "",
												                  Mobile          = Addr.Mobile ?? "",
												                  Mobile1         = Addr.Mobile1 ?? "",
												                  Fax             = Addr.Fax ?? "",
												                  Notes           = Addr.Notes ?? ""
											                  } );
										}
									}
								}
							}
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( GetCustomerCompanyAddress ), Exception );
			}

			return RetVal;
		}

		public bool CheckCustomerCompanyAndAddressLine1( string customerCode, string companyName, string suite, string addressLine1 )
		{
			try
			{
				var Cust = ( from R in Entity.ResellerCustomers
				             where R.CustomerCode == customerCode
				             select R ).FirstOrDefault();

				if( Cust != null )
				{
					var Companies = GetCompaniesByCompanyAndAddressLine1( companyName, suite, addressLine1 );
					var ReCo      = Entity.ResellerCustomerCompanies;

					foreach( var Company in Companies )
					{
						var Rec = ( from Rc in ReCo
						            where ( Rc.CustomerCode == customerCode ) && ( Rc.CompanyId == Company.CompanyId )
						            select Rc ).FirstOrDefault();

						if( Rec != null )
							return true;
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( CheckCustomerCompanyAndAddressLine1 ), Exception );
			}

			return false;
		}

		public bool CheckCustomerCompanyAndAddressLine1( CompanyByAccountSummary coData )
		{
			return CheckCustomerCompanyAndAddressLine1( coData.CustomerCode, coData.CompanyName, coData.Suite, coData.AddressLine1 );
		}

		public ResellerCustomerCompanyLookup GetCustomerCompanySecondaryId( string id )
		{
			try
			{
				var Company = GetCompanyBySecondaryId( id );

				if( Company.Ok )
				{
					var C = Company.Company;

					var Rec = ( from Rc in Entity.ResellerCustomerCompanies
					            where Rc.CompanyId == C.CompanyId
					            select Rc ).FirstOrDefault();

					if( Rec != null )
						return ConvertToCompanyLookup( Rec.CustomerCode, C.CompanyName, C.LocationBarcode, Company.Address );
				}
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( GetCustomerCompanySecondaryId ), E );
			}

			return new ResellerCustomerCompanyLookup();
		}

		public ResellerCustomerCompanyLookup GetCustomerCompanyAddress( string customerCode, string companyName )
		{
			try
			{
				var Companies = GetCompaniesByName( companyName );

				foreach( var Company in Companies )
				{
					var Rec = ( from Rc in Entity.ResellerCustomerCompanies
					            where ( Rc.CompanyId == Company.CompanyId ) && ( Rc.CustomerCode == customerCode )
					            select Rc ).FirstOrDefault();

					if( Rec != null )
					{
						var Addr = GetCompanyAddress( Company.CompanyId );

						if( Addr.Ok )
							return ConvertToCompanyLookup( customerCode, companyName, "", Addr.Address );
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return new ResellerCustomerCompanyLookup();
		}

		public void DeleteCustomerCompanyAddress( string customerCode, string companyName, string programName )
		{
			try
			{
				var Companies = GetCompaniesByName( companyName );

				foreach( var Company in Companies )
				{
					var (Address1, Ok) = GetCompanyAddress( Company.CompanyId );

					if( Ok )
					{
						// Delete and log
						Entity.Addresses.Remove( Address1 );
						Entity.SaveChanges();
						Context.LogDeleted( ResellerCustomerLogger.LOG_STORAGE, programName, "", new ResellerCustomerLogger( customerCode ) );

						break;
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void UpdateCustomerCompanyAddress( UpdateCustomerCompany customerCompany )
		{
			try
			{
				var Companies = GetCompaniesByName( customerCompany.Company.CompanyName );

				foreach( var Company in Companies )
				{
					// var Addr = GetCompanyAddress(Company.CompanyId);
					var E = Entity;

					var Addr = ( from Ca in E.CompanyAddresses
					             where Ca.CompanyId == Company.CompanyId
					             from A in E.Addresses
					             where A.Id == Ca.AddressId
					             select A ).FirstOrDefault();

					if( Addr != null )
					{
						// Update and log
						// Entity.Addresses.(Addr.Address);
						Addr.AddressLine1  = customerCompany.Company.AddressLine1;
						Addr.AddressLine2  = customerCompany.Company.AddressLine2;
						Addr.Barcode       = customerCompany.Company.Barcode;
						Addr.City          = customerCompany.Company.City;
						Addr.Country       = customerCompany.Company.Country;
						Addr.CountryCode   = customerCompany.Company.CountryCode;
						Addr.EmailAddress  = customerCompany.Company.EmailAddress;
						Addr.EmailAddress1 = customerCompany.Company.EmailAddress1;
						Addr.EmailAddress2 = customerCompany.Company.EmailAddress2;
						Addr.Fax           = customerCompany.Company.Fax;
						Addr.Latitude      = customerCompany.Company.Latitude;
						Addr.Longitude     = customerCompany.Company.Longitude;
						Addr.Mobile        = customerCompany.Company.Mobile;
						Addr.Mobile1       = customerCompany.Company.Mobile1;
						Addr.Notes         = customerCompany.Company.Notes;
						Addr.Phone         = customerCompany.Company.Phone;
						Addr.Phone1        = customerCompany.Company.Phone1;
						Addr.PostalBarcode = customerCompany.Company.PostalBarcode;
						Addr.PostalCode    = customerCompany.Company.PostalCode;
						Addr.Region        = customerCompany.Company.Region;
						Addr.SecondaryId   = customerCompany.Company.SecondaryId;
						Addr.Suite         = customerCompany.Company.Suite;
						Addr.Vicinity      = customerCompany.Company.Vicinity;

						Company.UserName        = customerCompany.Company.UserName;
						Company.LocationBarcode = customerCompany.Company.LocationBarcode;

						Entity.SaveChanges();
						Context.LogDifferences( ResellerCustomerLogger.LOG_STORAGE, customerCompany.ProgramName, "", new ResellerCustomerLogger( customerCompany.CustomerCode ) );

						break;
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public CustomerCodeList GetCustomerList()
		{
			var CustList = new CustomerCodeList();

			try
			{
				CustList.AddRange( from C in Entity.ResellerCustomers
				                   select C.CustomerCode );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return CustList;
		}


		public CompanyAddress GetResellerCustomerCompany( string customerCode )
		{
			var E = Entity;

			var Rec = ( from Rc in E.ResellerCustomers
			            where Rc.CustomerCode == customerCode
			            select Rc ).FirstOrDefault();

			if( Rec != null )
			{
				var Co = ( from C in E.Companies
				           orderby C.CompanyName
				           where C.CompanyId == Rec.CompanyId
				           select C ).FirstOrDefault();

				if( Co != null )
				{
					var Addr = ( from A in E.Addresses
					             where A.Id == Co.PrimaryAddressId
					             select A ).FirstOrDefault();

					if( Addr != null )
						return ConvertCompanyAddress( Co, Addr );
				}
			}

			return new CompanyAddress();
		}
	}
}