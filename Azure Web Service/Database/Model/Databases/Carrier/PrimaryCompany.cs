﻿using System;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Utils;
using Company = Protocol.Data.Company;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private PrimaryCompany GetCompanyDetails( ResellerCustomer cust )
		{
			var Result = new PrimaryCompany();

			try
			{
				if( !( cust is null ) )
				{
					Result.Password = cust.Password;
					Result.LoginCode = cust.LoginCode;
					Result.CustomerCode = cust.CustomerCode;
					Result.UserName = cust.UserName;

					Company GetCompanyAndAddress( long companyId )
					{
						var (Company, _) = GetCompany( companyId );
						var (Address, _) = GetCompanyAddress( companyId );

						return ConvertCompany( Company, Address );
					}

					Result.Company = GetCompanyAndAddress( cust.CompanyId );
					Result.BillingCompany = GetCompanyAndAddress( cust.BillingCompanyId );

					Result.Ok = true;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public string AddUpdatePrimaryCompany( AddUpdatePrimaryCompany customer )
		{
			try
			{
				var (Password, Ok) = Encryption.FromTimeLimitedToken( customer.Password );

				if( Ok )
				{
					Password = ( Password ?? "" ).SubStr( 0, 50 );

					var ProgramName = customer.ProgramName;
					var E = Entity;
					var Rc = E.ResellerCustomers;

					var Rec = ( from C in Rc
					            where C.CustomerCode == customer.CustomerCode
					            select C ).FirstOrDefault();

					if( Rec is null )
					{
						// Add primary company address
						var (Company, _, CompanyOk) = AddCompany( customer.Company, ProgramName );

						if( CompanyOk )
						{
							// Generate unique login code system wide
							var (Code, CodeOk) = Database.Users.AddResellerLoginCode( Context, customer.CustomerCode, customer.SuggestedLoginCode, customer.Company );

							if( CodeOk )
							{
								var CId = Company.CompanyId;
								var Aid = Company.PrimaryAddressId;
								long BId = 0;

								// Do Bill To Company
								if( customer.BillingCompany.IsNotNull() && customer.BillingCompany.CompanyNumber.IsNotNullOrWhiteSpace() )
								{
									var (BCompany, _, BCompanyOk) = AddCompany( customer.BillingCompany, ProgramName );

									if( BCompanyOk )
										BId = BCompany.CompanyId;
								}

								var Cust = new ResellerCustomer
								           {
									           CustomerCode = customer.CustomerCode ?? "",
									           LoginCode = Code ?? "",
									           Password = Password,
									           UserName = customer.UserName ?? "",
									           CompanyId = CId,
									           BillingCompanyId = BId,
									           CompanyName = Company.CompanyName ?? "",
									           StringOption1 = "",
									           StringOption2 = "",
									           StringOption3 = "",
									           StringOption4 = "",
									           StringOption5 = ""
								           };

								// cjt Copied from ResellerCustomers.AddUpdateCustomer
								Rc.Add( Cust );

								AddCustCompany( E, CId, Aid, Code, customer.Company );

								return Code;
							}
						}
					}
					else
					{
						Rec.UserName = customer.UserName; // cjt Added 20190808
						Rec.Password = Password;
						E.SaveChanges();

						if( customer.Company.CompanyName.IsNotNullOrWhiteSpace() )
							UpdateCompany( Rec.CompanyId, customer.Company, ProgramName );

						if( customer.BillingCompany.CompanyName.IsNotNullOrWhiteSpace() )
							UpdateCompany( Rec.BillingCompanyId, customer.BillingCompany, ProgramName );

						return Rec.LoginCode;
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return "";
		}

		public PrimaryCompany GetPrimaryCompanyByCompanyName( string companyName )
		{
			try
			{
				companyName = companyName.Trim();

				return GetCompanyDetails( ( from C in Entity.ResellerCustomers
				                            where C.CompanyName == companyName
				                            select C ).FirstOrDefault() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return GetCompanyDetails( null );
		}

		public PrimaryCompany GetPrimaryCompanyByCustomerCode( string customerCode )
		{
			if( customerCode.IsNotNullOrWhiteSpace() )
			{
				try
				{
					customerCode = customerCode.Trim();

					return GetCompanyDetails( ( from C in Entity.ResellerCustomers
					                            where C.CustomerCode == customerCode
					                            select C ).FirstOrDefault() );
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
				}
			}

			return GetCompanyDetails( null );
		}

		public PrimaryCompany GetPrimaryCompanyByLoginCode( string loginCode )
		{
			try
			{
				loginCode = loginCode.Trim();

				return GetCompanyDetails( ( from C in Entity.ResellerCustomers
				                            where C.LoginCode == loginCode
				                            select C ).FirstOrDefault() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return GetCompanyDetails( null );
		}
	}
}