﻿using System;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using PackageType = Protocol.Data.PackageType;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public PackageTypeList GerPackageTypes()
		{
			var PTypes = ( from Pt in Entity.PackageTypes
			               where !Pt.Deleted
			               orderby Pt.SortOrder
			               select Pt ).ToList();

			var Lookup = PTypes.ToDictionary( packageType => packageType.Id, packageType => packageType.Description );

			var Result = new PackageTypeList();

			var Cutoffs = Entity.PackageTypeCutoffs;

			foreach( var PackageType in PTypes )
			{
				var P = new PackageType
				        {
					        Description = PackageType.Description,
					        Formula = PackageType.Formula,
					        HoursXRate = PackageType.HoursXRate,
					        PiecesXRate = PackageType.PiecesXRate,
					        WeightXRate = PackageType.WeightXRate,
					        DistanceXRate = PackageType.DistanceXRate,
					        Cumulative = PackageType.Cumulative,
					        HideOnTripEntry = PackageType.HideOnTripEntry,
					        Label = PackageType.Label,
					        LimitMaxToNexCutoff = PackageType.LimitMaxToNextCutoff,
					        MaximumVisibleWeight = PackageType.MaxVisibleWeight,
					        MinimumVisibleWeight = PackageType.MinVisibleWeight,
					        UsePiecesForCutoffs = PackageType.UsePiecesForCutoff,
					        SimpleEnabled = PackageType.SimpleEnabled,
					        SortOrder = (ushort)PackageType.SortOrder
				        };

				var CutOffs = ( from C in Cutoffs
				                where C.PackageTypeId == PackageType.Id
				                select C ).ToList();

				P.Cutoffs.AddRange( CutOffs.Select( packageTypeCutoff => new Cutoff( packageTypeCutoff.PackageTypeId )
				                                                         {
					                                                         CutoffAmount = packageTypeCutoff.CuttoffAmount,
					                                                         ToPackageType = Lookup[ packageTypeCutoff.ToPackageType ]
				                                                         } ) );
				Result.Add( P );
			}

			return Result;
		}

		public bool AddUpdatePackageType( PackageType packageType )
		{
			var E = Entity;

			try
			{
				var PTypes = E.PackageTypes;

				var PType = ( from Pt in PTypes
				              where Pt.Description == packageType.Description
				              select Pt ).FirstOrDefault();

				if( PType is null )
				{
					PTypes.Add( PType = new MasterTemplate.PackageType
					                    {
						                    FormulasEnabled = packageType.FormulasEnabled,
						                    CutoffsEnabled = packageType.CutoffsEnabled,
						                    SimpleEnabled = packageType.SimpleEnabled,
						                    Description = packageType.Description,
						                    HoursXRate = packageType.HoursXRate,
						                    DistanceXRate = packageType.DistanceXRate,
						                    WeightXRate = packageType.WeightXRate,
						                    PiecesXRate = packageType.PiecesXRate,
						                    Cumulative = packageType.Cumulative,
						                    Formula = packageType.Formula,
						                    HideOnTripEntry = packageType.HideOnTripEntry,
						                    Label = packageType.Label,
						                    LimitMaxToNextCutoff = packageType.LimitMaxToNexCutoff,
						                    MaxVisibleWeight = packageType.MaximumVisibleWeight,
						                    MinVisibleWeight = packageType.MinimumVisibleWeight,
						                    UsePiecesForCutoff = packageType.UsePiecesForCutoffs,
						                    SortOrder = (short)packageType.SortOrder,
						                    Deleted = packageType.Deleted
					                    } );
				}
				else
				{
					PType.FormulasEnabled = packageType.FormulasEnabled;
					PType.CutoffsEnabled = packageType.CutoffsEnabled;
					PType.SimpleEnabled = packageType.SimpleEnabled;
					PType.Description = packageType.Description;
					PType.DistanceXRate = packageType.DistanceXRate;
					PType.HoursXRate = packageType.HoursXRate;
					PType.PiecesXRate = packageType.PiecesXRate;
					PType.WeightXRate = packageType.WeightXRate;
					PType.Cumulative = packageType.Cumulative;
					PType.Formula = packageType.Formula;
					PType.HideOnTripEntry = packageType.HideOnTripEntry;
					PType.Label = packageType.Label;
					PType.LimitMaxToNextCutoff = packageType.LimitMaxToNexCutoff;
					PType.MaxVisibleWeight = packageType.MaximumVisibleWeight;
					PType.MinVisibleWeight = packageType.MinimumVisibleWeight;
					PType.UsePiecesForCutoff = packageType.UsePiecesForCutoffs;
					PType.SortOrder = (short)packageType.SortOrder;
					PType.Deleted = packageType.Deleted;
				}

				E.SaveChanges();

				var Id = PType.Id;

				var PTypeCutoffs = E.PackageTypeCutoffs;

				var Cutoffs = ( from Ct in PTypeCutoffs
				                where Ct.PackageTypeId == Id
				                select Ct ).ToList();

				var PCutoffs = packageType.Cutoffs;

				// Remove deleted records
				var Deleted = ( from Cutoff in Cutoffs
				                where !Enumerable.Any( PTypeCutoffs, pTypeCutoff => pTypeCutoff.ToPackageType == Cutoff.ToPackageType )
				                select Cutoff ).ToList();

				PTypeCutoffs.RemoveRange( Deleted );

				// Now Adds or Modify
				foreach( var PCuttoff in PCutoffs )
				{
					var COff = ( from Ct in PTypeCutoffs
					             where Ct.ToPackageType == PCuttoff.PackageTypeId
					             select Ct ).FirstOrDefault();

					if( COff == null )
					{
						var PRec = ( from P in PTypes
						             where P.Description == PCuttoff.ToPackageType
						             select P ).FirstOrDefault();

						if( PRec != null )
						{
							PTypeCutoffs.Add( new PackageTypeCutoff
							                  {
								                  PackageTypeId = Id,
								                  CuttoffAmount = PCuttoff.CutoffAmount,
								                  ToPackageType = PRec.Id
							                  } );
						}
					}
					else
						COff.CuttoffAmount = PCuttoff.CutoffAmount;
				}

				E.SaveChanges();

				return true;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return false;
		}
	}
}