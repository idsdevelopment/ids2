﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using Interfaces.Interfaces;
using Utils;

namespace Database.Model.Databases.MasterTemplate
{
#pragma warning disable 660,661

	public static class Extensions
	{
		public static bool IsEmpty( this Address addr )
		{
			return !string.IsNullOrEmpty( addr.AddressLine1.Trim() ) || !string.IsNullOrEmpty( addr.AddressLine2.Trim() );
		}

		public static Protocol.Data.Address ToProtocolDataAddress( this Address addr )
		{
			return new Protocol.Data.Address
			       {
				       SecondaryId = addr.SecondaryId,

				       AddressLine1 = addr.AddressLine1,
				       AddressLine2 = addr.AddressLine2,
				       Barcode = addr.Barcode,
				       City = addr.City,
				       Country = addr.Country,
				       CountryCode = addr.CountryCode,
				       Latitude = addr.Latitude,
				       Longitude = addr.Longitude,
				       EmailAddress = addr.EmailAddress,
				       EmailAddress1 = addr.EmailAddress1,
				       EmailAddress2 = addr.EmailAddress2,
				       Mobile = addr.Mobile,
				       Mobile1 = addr.Mobile1,
				       Phone = addr.Phone,
				       Phone1 = addr.Phone1,
				       Fax = addr.Fax,
				       PostalBarcode = addr.PostalBarcode,
				       PostalCode = addr.PostalCode,
				       Region = addr.Region,
				       Suite = addr.Suite,
				       Vicinity = addr.Vicinity,
				       Notes = addr.Notes
			       };
		}


		public static ( Address Address, bool Ok ) Clone( this Address addr, IRequestContext context )
		{
			(Address Address, bool Ok) Retval = ( Address: null, Ok: false );

			using( var Db = new CarrierDb( context ) )
			{
				var E = Db.Entity;

				try
				{
					var A = new Address
					        {
						        SecondaryId = addr.SecondaryId,

						        AddressLine1 = addr.AddressLine1 ?? "",
						        AddressLine2 = addr.AddressLine2 ?? "",
						        Barcode = addr.Barcode ?? "",
						        City = addr.City ?? "",
						        CountryCode = addr.CountryCode ?? "",
						        Latitude = addr.Latitude,
						        Longitude = addr.Longitude,
						        EmailAddress = addr.EmailAddress ?? "",
						        EmailAddress1 = addr.EmailAddress1 ?? "",
						        EmailAddress2 = addr.EmailAddress2 ?? "",
						        Mobile = addr.Mobile ?? "",
						        Mobile1 = addr.Mobile1 ?? "",
						        Phone = addr.Phone ?? "",
						        Phone1 = addr.Phone1 ?? "",
						        Fax = addr.Fax ?? "",
						        PostalBarcode = addr.PostalBarcode ?? "",
						        PostalCode = addr.PostalCode ?? "",
						        Region = addr.Region ?? "",
						        Suite = addr.Suite ?? "",
						        Vicinity = addr.Vicinity ?? "",
						        Notes = addr.Notes ?? "",
						        LastModified = DateTime.Now
					        };
					E.Addresses.Add( A );
					E.SaveChanges();
					Retval.Address = A;
					Retval.Ok = true;
				}
				catch( Exception Ex )
				{
					context.SystemLogException( Ex );
				}
			}

			return Retval;
		}
	}

	public partial class Address
	{
		public static bool operator ==( Address a1, Address a2 )
		{
			if( a1 is null && a2 is null ) // Cast to object to stop recursion and stack overflow
				return true;

			return !( a1 is null )
			       && !( a2 is null )
			       && ( a1.SecondaryId == a2.SecondaryId )
			       && ( a1.AddressLine1 == a2.AddressLine1 )
			       && ( a1.AddressLine2 == a2.AddressLine2 )
			       && ( a1.Barcode == a2.Barcode )
			       && ( a1.City == a2.City )
			       && ( a1.CountryCode == a2.CountryCode )
			       && ( a1.EmailAddress == a2.EmailAddress )
			       && ( a1.EmailAddress1 == a2.EmailAddress1 )
			       && ( a1.EmailAddress2 == a2.EmailAddress2 )
			       && ( a1.Fax == a2.Fax )
			       && ( a1.Latitude == a2.Latitude )
			       && ( a1.Longitude == a2.Longitude )
			       && ( a1.Mobile == a2.Mobile )
			       && ( a1.Mobile1 == a2.Mobile1 )
			       && ( a1.Notes == a2.Notes )
			       && ( a1.Phone == a2.Phone )
			       && ( a1.Phone1 == a2.Phone1 )
			       && ( a1.PostalBarcode == a2.PostalBarcode )
			       && ( a1.PostalCode == a2.PostalCode )
			       && ( a1.Region == a2.Region )
			       && ( a1.Suite == a2.Suite )
			       && ( a1.Vicinity == a2.Vicinity );
		}

		public static bool operator !=( Address a1, Address a2 )
		{
			return !( a1 == a2 );
		}
	}
#pragma warning restore 660,661
}

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public class AddressLogger
		{
			public string SecondaryId { get; set; }

			public string Suite { get; set; }
			public string AddressLine1 { get; set; }
			public string AddressLine2 { get; set; }
			public string Vicinity { get; set; }
			public string City { get; set; }
			public string Region { get; set; }
			public string PostalCode { get; set; }
			public string CountryCode { get; set; }
			public string Phone { get; set; }
			public string Phone1 { get; set; }
			public string Mobile { get; set; }
			public string Mobile1 { get; set; }
			public string Fax { get; set; }
			public string EmailAddress { get; set; }
			public string EmailAddress1 { get; set; }
			public string EmailAddress2 { get; set; }
			public decimal Latitude { get; set; }
			public decimal Longitude { get; set; }
			public string Notes { get; set; }
			public string Barcode { get; set; }
			public string PostalBarcode { get; set; }
			public int GroupNumber { get; set; }

			public AddressLogger( Address addr )
			{
				SecondaryId = addr.SecondaryId;

				Suite = addr.Suite ?? "";
				AddressLine1 = addr.AddressLine1 ?? "";
				AddressLine2 = addr.AddressLine2 ?? "";
				Vicinity = addr.Vicinity ?? "";
				City = addr.City ?? "";
				Region = addr.Region ?? "";
				PostalCode = addr.PostalCode ?? "";
				CountryCode = addr.CountryCode ?? "";
				Phone = addr.Phone ?? "";
				Phone1 = addr.Phone1 ?? "";
				Mobile = addr.Mobile ?? "";
				Mobile1 = addr.Mobile1 ?? "";
				Fax = addr.Fax ?? "";
				EmailAddress = addr.EmailAddress ?? "";
				EmailAddress1 = addr.EmailAddress1 ?? "";
				EmailAddress2 = addr.EmailAddress2 ?? "";
				Latitude = addr.Latitude;
				Longitude = addr.Longitude;
				Notes = addr.Notes ?? "";
				Barcode = addr.Barcode ?? "";
				PostalBarcode = addr.PostalBarcode ?? "";
			}
		}

		public ( AddressLogger Before, AddressLogger After, long AddressId, bool Ok ) UpdateAddress( long addressId, Protocol.Data.Address addr, string programName )
		{
			try
			{
				var Rec = ( from A in Entity.Addresses
				            where A.Id == addressId
				            select A ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					var Before = new AddressLogger( Rec );

					Rec.SecondaryId = addr.SecondaryId;
					Rec.Suite = addr.Suite ?? "";
					Rec.AddressLine1 = addr.AddressLine1 ?? "";
					Rec.AddressLine2 = addr.AddressLine2 ?? "";
					Rec.Vicinity = addr.Vicinity ?? "";
					Rec.City = addr.City ?? "";
					Rec.Region = addr.Region ?? "";
					Rec.Country = addr.Country ?? "";
					Rec.CountryCode = addr.CountryCode ?? "";
					Rec.PostalCode = addr.PostalCode ?? "";
					Rec.PostalBarcode = addr.PostalBarcode ?? "";
					Rec.Barcode = addr.Barcode ?? "";
					Rec.Latitude = addr.Latitude;
					Rec.Longitude = addr.Longitude;
					Rec.Notes = addr.Notes ?? "";
					Rec.EmailAddress = addr.EmailAddress ?? "";
					Rec.EmailAddress1 = addr.EmailAddress1 ?? "";
					Rec.EmailAddress2 = addr.EmailAddress2 ?? "";
					Rec.Mobile = addr.Mobile ?? "";
					Rec.Mobile1 = addr.Mobile1 ?? "";
					Rec.Fax = addr.Fax ?? "";
					Rec.Phone = addr.Phone ?? "";
					Rec.Phone1 = addr.Phone1 ?? "";
					Rec.LastModified = DateTime.Now;

					Entity.SaveChanges();
					var After = new AddressLogger( Rec );

					return ( Before, After, addressId, true );
				}

				var (AddressId, Address, Ok) = AddAddress( addr, programName );

				if( Ok )
					return ( Before: null, After: Address, AddressId, Ok: true );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return ( Before: null, After: null, 0, Ok: false );
		}


		public (long AddressId, AddressLogger Address, bool Ok) AddAddress( Protocol.Data.Address addr, string programName )
		{
			var Retval = ( AddressId: (long)0, Address: (AddressLogger)null, Ok: false );

			try
			{
				var A = new Address
				        {
					        SecondaryId = addr.SecondaryId,
					        Suite = addr.Suite ?? "",
					        AddressLine1 = addr.AddressLine1 ?? "",
					        AddressLine2 = addr.AddressLine2 ?? "",
					        Vicinity = addr.Vicinity ?? "",
					        City = addr.City ?? "",
					        Region = addr.Region ?? "",
					        Country = addr.Country ?? "",
					        CountryCode = addr.CountryCode ?? "",
					        PostalCode = addr.PostalCode ?? "",
					        PostalBarcode = addr.PostalBarcode ?? "",
					        Barcode = addr.Barcode ?? "",
					        Latitude = addr.Latitude,
					        Longitude = addr.Longitude,
					        Notes = addr.Notes ?? "",
					        EmailAddress = addr.EmailAddress ?? "",
					        EmailAddress1 = addr.EmailAddress1 ?? "",
					        EmailAddress2 = addr.EmailAddress2 ?? "",
					        Mobile = addr.Mobile ?? "",
					        Mobile1 = addr.Mobile1 ?? "",
					        Fax = addr.Fax ?? "",
					        Phone = addr.Phone ?? "",
					        Phone1 = addr.Phone1 ?? "",
					        LastModified = DateTime.Now
				        };

				Entity.Addresses.Add( A );
				Entity.SaveChanges();
				Retval.Address = new AddressLogger( A );
				Retval.AddressId = A.Id;
				Retval.Ok = true;
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( AddAddress ), E );
			}

			return Retval;
		}


		public (Address Address, bool Ok) GetAddress( long addressId )
		{
			(Address Address, bool Ok) Retval = ( Address: null, Ok: false );

			Retval.Address = ( from A in Entity.Addresses
			                   where A.Id == addressId
			                   select A ).FirstOrDefault();
			Retval.Ok = Retval.Address.IsNotNull();

			return Retval;
		}

		public (Address Address, bool Ok) GetAddressBySecondaryId( string id )
		{
			(Address Address, bool Ok) Retval = ( Address: null, Ok: false );

			Retval.Address = ( from A in Entity.Addresses
			                   where A.SecondaryId == id
			                   select A ).FirstOrDefault();
			Retval.Ok = Retval.Address.IsNotNull();

			return Retval;
		}

		public List<string> FindMissingAddressesBySecondaryId( List<string> ids )
		{
			var Ids = new HashSet<string>( ids );

			var Addresses = Entity.Addresses;

			return ( from Id in Ids
			         let Rec = ( from A in Addresses
			                     where A.SecondaryId == Id
			                     select A ).FirstOrDefault()
			         where Rec == null
			         select Id ).ToList();
		}

		public List<Address> CheckSuiteAndAddressLine1( string suite, string addressLine1 )
		{
			try
			{
				return ( from A in Entity.Addresses
				         where ( A.Suite == suite ) && ( A.AddressLine1 == addressLine1 )
				         select A ).ToList();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( CheckSuiteAndAddressLine1 ), Exception );
			}

			return new List<Address>();
		}
	}
}