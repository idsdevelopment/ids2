﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data;
using Charge = Database.Model.Databases.MasterTemplate.Charge;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public void AddUpdateCharges( Charges charges )
		{
			try
			{
				var E = Entity;

				foreach( var C in charges )
				{
					var Rec = ( from Chg in E.Charges
					            where Chg.ChargeId == C.ChargeId
					            select Chg ).FirstOrDefault();

					if( Rec is null )
					{
						E.Charges.Add( new Charge
						               {
							               ChargeId = C.ChargeId ?? "",
							               AccountId = C.AccountId ?? "",
							               AlwaysApplied = C.AlwaysApplied,
							               ChargeType = C.ChargeType,
							               DisplayOnDriversScreen = C.DisplayOnDriversScreen,
							               DisplayOnEntry = C.DisplayOnEntry,
							               DisplayOnInvoice = C.DisplayOnInvoice,
							               DisplayOnWaybill = C.DisplayOnWaybill,
							               DisplayType = C.DisplayType,
							               ExcludeFromFuelSurcharge = C.ExcludeFromFuelSurcharge,
							               ExcludeFromTaxes = C.ExcludeFromTaxes,
							               Formula = C.Formula ?? "",
							               IsText = C.IsText,
							               IsDiscountable = C.IsDiscountable,
							               IsFormula = C.IsFormula,
							               IsFuelSurcharge = C.IsFuelSurcharge,
							               IsMultiplier = C.IsMultiplier,
							               IsOverride = C.IsOverride,
							               IsTax = C.IsTax,
							               IsVolumeCharge = C.IsVolumeCharge,
							               IsWeightCharge = C.IsWeightCharge,
							               Label = C.Label ?? "",
							               Max = C.Max,
							               Min = C.Min,
							               OverridenPackage = C.OverridenPackage,
							               SortIndex = C.SortIndex,
							               Text = C.Text ?? "",
							               Value = C.Value
						               } );
					}
					else
					{
						Rec.AccountId = C.AccountId ?? "";
						Rec.AlwaysApplied = C.AlwaysApplied;
						Rec.ChargeType = C.ChargeType;
						Rec.DisplayOnDriversScreen = C.DisplayOnDriversScreen;
						Rec.DisplayOnEntry = C.DisplayOnEntry;
						Rec.DisplayOnInvoice = C.DisplayOnInvoice;
						Rec.DisplayOnWaybill = C.DisplayOnWaybill;
						Rec.DisplayType = C.DisplayType;
						Rec.ExcludeFromFuelSurcharge = C.ExcludeFromFuelSurcharge;
						Rec.ExcludeFromTaxes = C.ExcludeFromTaxes;
						Rec.Formula = C.Formula ?? "";
						Rec.IsText = C.IsText;
						Rec.IsDiscountable = C.IsDiscountable;
						Rec.IsFormula = C.IsFormula;
						Rec.IsFuelSurcharge = C.IsFuelSurcharge;
						Rec.IsMultiplier = C.IsMultiplier;
						Rec.IsOverride = C.IsOverride;
						Rec.IsTax = C.IsTax;
						Rec.IsVolumeCharge = C.IsVolumeCharge;
						Rec.IsWeightCharge = C.IsWeightCharge;
						Rec.Label = C.Label ?? "";
						Rec.Max = C.Max;
						Rec.Min = C.Min;
						Rec.OverridenPackage = C.OverridenPackage;
						Rec.SortIndex = C.SortIndex;
						Rec.Text = C.Text ?? "";
						Rec.Value = C.Value;
					}

					E.SaveChanges();
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}