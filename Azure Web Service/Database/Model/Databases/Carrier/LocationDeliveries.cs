﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data;
using Utils;
using Company = Database.Model.Databases.MasterTemplate.Company;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public const string MISSING_LOCATION = "MISSING LOCATION",
		                    DEFAULT_COMPANY = "DEFAULT COMPANY",
		                    DEFAULT_PICKUP_COMPANY = "DEFAULT PICKUP COMPANY",
		                    CREATED = "Created";

		public void LocationDeliveries( LocationDeliveries deliveries )
		{
			const string PROGRAM = "Location Deliveries";

			try
			{
				(Company Missing, Company Pickup, bool Ok) GetDefaultCompanies()
				{
					var Recs = ( from Cs in Entity.ResellerCustomerCompanySummaries
					             where ( Cs.CompanyName == MISSING_LOCATION ) || ( Cs.CompanyName == DEFAULT_PICKUP_COMPANY )
					             select Cs ).ToList();

					Company GetCompany( string company )
					{
						var Co = ( from R in Recs
						           where R.CompanyName == company
						           select R ).FirstOrDefault();

						if( !( Co is null ) )
						{
							var (Company1, Ok3) = this.GetCompany( Co.CompanyId );

							if( Ok3 )
								return Company1;
						}

						return null;
					}

					var Missing1 = GetCompany( MISSING_LOCATION );
					var Pickup1  = GetCompany( DEFAULT_PICKUP_COMPANY );

					return ( Missing1, Pickup1, !( Missing1 is null ) && !( Pickup1 is null ) );
				}

				var Cid = Context.CarrierId;

				var (MissingCompany, PickupCompany, CompaniesOk) = GetDefaultCompanies();

				if( !CompaniesOk ) // Probably the first time. Create the companies
				{
					var P = Encryption.ToTimeLimitedToken( "" );

					var Co = new AddUpdateCustomer( PROGRAM )
					         {
						         CustomerCode = Cid,
						         Password     = P
					         };

					var C = Co.Company;
					C.CompanyName = DEFAULT_COMPANY;
					C.UserName    = Cid;
					C.Password    = P;

					Co.Companies.Add( new Protocol.Data.Company
					                  {
						                  CompanyName  = MISSING_LOCATION,
						                  AddressLine1 = MISSING_LOCATION,
						                  Password     = P
					                  } );

					Co.Companies.Add( new Protocol.Data.Company
					                  {
						                  CompanyName  = DEFAULT_PICKUP_COMPANY,
						                  AddressLine1 = DEFAULT_PICKUP_COMPANY,
						                  Password     = P
					                  } );

					AddUpdateResellerCustomer( Co );

					( MissingCompany, PickupCompany, CompaniesOk ) = GetDefaultCompanies();

					if( !CompaniesOk )
						throw new Exception( "Cannot Create Default Missing Companies." );
				}

				var ( LocationCompany, Ok ) = GetCompanyByLocation( deliveries.Location );

				var DeliveryNote = "";

				if( !Ok )
				{
					LocationCompany = MissingCompany;
					DeliveryNote    = $"Missing Location: {deliveries.Location}";
				}

				static void NoAddress( string companyName )
				{
					throw new Exception( $"Cannot Find Company Delivery Address. Company: {companyName}" );
				}

				var (La, Ok1) = GetCompanyAddress( LocationCompany.CompanyId );

				if( !Ok1 )
					NoAddress( LocationCompany.CompanyName );

				var (Pa, Ok2) = GetCompanyAddress( PickupCompany.CompanyId );

				if( !Ok2 )
					NoAddress( PickupCompany.CompanyName );

				var Trip = new TripUpdate( PROGRAM )
				           {
					           LastModified = DateTime.UtcNow,

					           Status1 = STATUS.VERIFIED,

					           Driver   = deliveries.Driver,
					           Location = deliveries.Location,

					           PackageType  = CREATED,
					           ServiceLevel = CREATED,

					           Packages = new List<TripPackage>
					                      {
						                      new TripPackage
						                      {
							                      PackageType = CREATED,
							                      Pieces      = deliveries.Items.Sum( i => i.Quantity ),
							                      Items = ( from I in deliveries.I
							                                select new Protocol.Data.TripItem
							                                       {
								                                       Barcode  = I.Barcode,
								                                       ItemCode = I.Barcode,

								                                       Description = CREATED,

								                                       Original = I.Quantity,
								                                       Pieces   = I.Quantity
							                                       } ).ToList()
						                      }
					                      },

					           AccountId = LocationCompany.CompanyName,

					           BillingAccountId = PickupCompany.CompanyName,
					           CallTime         = deliveries.DeliveryTime,

					           DeliveryCompanyName          = LocationCompany.CompanyName,
					           DeliveryAddressAddressLine1  = La.AddressLine1,
					           DeliveryAddressAddressLine2  = La.AddressLine2,
					           DeliveryAddressBarcode       = La.Barcode,
					           DeliveryAddressCity          = La.City,
					           DeliveryAddressCountry       = La.Country,
					           DeliveryAddressCountryCode   = La.CountryCode,
					           DeliveryAddressEmailAddress  = La.EmailAddress,
					           DeliveryAddressEmailAddress1 = La.EmailAddress1,
					           DeliveryAddressEmailAddress2 = La.EmailAddress2,
					           DeliveryAddressFax           = La.Fax,
					           DeliveryAddressLatitude      = (decimal)deliveries.Latitude,
					           DeliveryAddressLongitude     = (decimal)deliveries.Longitude,
					           DeliveryAddressMobile        = La.Mobile,
					           DeliveryAddressMobile1       = La.Mobile1,
					           DeliveryAddressNotes         = DeliveryNote,
					           DeliveryAddressPhone         = La.Phone,
					           DeliveryAddressPhone1        = La.Phone1,
					           DeliveryAddressPostalBarcode = La.PostalBarcode,
					           DeliveryAddressRegion        = La.Region,
					           DeliveryAddressSuite         = La.Suite,
					           DeliveryAddressVicinity      = La.Vicinity,
					           DeliveryTime                 = deliveries.DeliveryTime,
					           DeliveryAddressPostalCode    = La.PostalCode,
					           DueTime                      = deliveries.DeliveryTime,

					           PickupAccountId            = LocationCompany.CompanyName,
					           PickupAddressAddressLine1  = Pa.AddressLine1,
					           PickupAddressAddressLine2  = Pa.AddressLine2,
					           PickupAddressBarcode       = Pa.Barcode,
					           PickupAddressCity          = Pa.City,
					           PickupAddressCountry       = Pa.Country,
					           PickupAddressCountryCode   = Pa.CountryCode,
					           PickupAddressEmailAddress  = Pa.EmailAddress,
					           PickupAddressEmailAddress1 = Pa.EmailAddress1,
					           PickupAddressEmailAddress2 = Pa.EmailAddress2,
					           PickupAddressFax           = Pa.Fax,
					           PickupAddressMobile        = Pa.Mobile,
					           PickupAddressMobile1       = Pa.Mobile1,
					           PickupAddressPhone         = Pa.Phone,
					           PickupAddressPhone1        = Pa.Phone1,
					           PickupAddressPostalBarcode = Pa.PostalBarcode,
					           PickupAddressPostalCode    = Pa.PostalCode,
					           PickupAddressRegion        = Pa.Region,
					           PickupAddressSuite         = Pa.Suite,
					           PickupAddressVicinity      = Pa.Vicinity,
					           PickupCompanyName          = PickupCompany.CompanyName,
					           PickupTime                 = deliveries.DeliveryTime,
					           ReadyTime                  = deliveries.DeliveryTime,

					           VerifiedLatitude  = deliveries.Latitude,
					           VerifiedLongitude = deliveries.Longitude,
					           VerifiedTime      = deliveries.DeliveryTime
				           };

				Trip.OriginalPieceCount = Trip.Packages.Sum( p => p.Original = p.Pieces );

				AddUpdateTrip( Trip );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}