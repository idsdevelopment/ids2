﻿using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Database.Utils;
using Interfaces.Interfaces;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb : ContextDb<MasterTemplateEntities>
	{
		private const string CONNECTION_ID = "MasterTemplateEntities",
		                     CACHE_KEY = "~~Carrier~~Db~~";

		private const int CURRENT_VERSION = 0;

		public CarrierDb( IRequestContext context ) : base( context )
		{
		}

		protected override MasterTemplateEntities CreateEntity( string connectionString )
		{
			return new MasterTemplateEntities( connectionString );
		}

		protected override void GetConnection( out string databaseName, out string connectionName, out string cacheKey )
		{
			databaseName = DbUtils.MASTER_DATABASE_NAME;
			connectionName = CONNECTION_ID;
			cacheKey = CACHE_KEY;
		}

		protected override bool UpgradeToVersion( MasterTemplateEntities entity, int toVersion )
		{
			return false;
		}

		protected override int GetDatabaseVersion( MasterTemplateEntities entity )
		{
			var Cfg = ( from Conf in entity.Configurations
			            select Conf ).FirstOrDefault();

			if( Cfg == null )
			{
				var Config = new Configuration
				             {
					             CurrentVersion = CURRENT_VERSION
				             };

				entity.Configurations.Add( Config );
				entity.SaveChanges();

				return CURRENT_VERSION;
			}

			return Cfg.CurrentVersion;
		}

		protected override void SetDatabaseVersion( MasterTemplateEntities entity, int version )
		{
			var Config = ( from Conf in entity.Configurations
			               select Conf ).FirstOrDefault();

			if( Config == null )
			{
				Config = new Configuration
				         {
					         CurrentVersion = version
				         };
				entity.Configurations.Add( Config );
			}
			else
				Config.CurrentVersion = version;

			entity.SaveChanges();
		}

		public override int CurrentVersion => CURRENT_VERSION;
	}
}