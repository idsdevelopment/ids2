﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Role = Database.Model.Databases.MasterTemplate.Role;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public IDictionary<string, Role> GetRolesAsDictionary()
		{
			return ( from R in Entity.Roles
			         select R ).ToDictionary( role => role.RoleName, role => role );
		}

		public IDictionary<int, Role> GetRolesAsDictionaryById()
		{
			return ( from R in Entity.Roles
			         select R ).ToDictionary( role => role.Id, role => role );
		}

		public Roles GetStaffMemberRoles( string staffId )
		{
			var Result = new Roles();

			try
			{
				var Roles = GetRolesAsDictionaryById();

				var Rls = from R in Entity.StaffRoles
				          where R.StaffId == staffId
				          select R;

				foreach( var StaffRole in Rls )
				{
					if( Roles.TryGetValue( StaffRole.RoleId, out var Rle ) )
					{
						Result.Add( new Protocol.Data.Role
						            {
							            Name = Rle.RoleName,
							            Mandatory = Rle.Mandatory,
							            IsAdministrator = Rle.IsAdministrator,
							            IsDriver = Rle.IsDriver,
							            JsonObject = Rle.JsonObject
						            } );
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public void AddStaffRoles( string staffId, List<Protocol.Data.Role> roles )
		{
			var E = Entity;

			try
			{
				var Rls = E.Roles;

				// Get Valid Roles
				var Roles = roles.Select( r => ( from R in Rls
				                                 where R.RoleName == r.Name
				                                 select R.RoleName ).FirstOrDefault() ).Where( rle => rle != null ).ToList();

				var Sf = Entity.StaffRoles;

				foreach( var Role in Roles )
				{
					var Rle = ( from R in Rls
					            where R.RoleName == Role
					            select R ).FirstOrDefault();

					if( Rle != null )
						Sf.Add( new StaffRole { StaffId = staffId, RoleId = Rle.Id } );
				}

				E.SaveChanges();
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
		}

		public (IList<StaffRole> Before, IList<StaffRole> After, bool Ok ) AddUpdateStaffRoles( string staffId, List<Protocol.Data.Role> roles )
		{
			var Before = ( from R in Entity.StaffRoles
			               where R.StaffId == staffId
			               select R ).ToList();

			var E = Entity;

			try
			{
				var StaffRoles = Entity.StaffRoles;

				StaffRoles.RemoveRange( from R in StaffRoles
				                        where R.StaffId == staffId
				                        select R );

				var DictRoles = GetRolesAsDictionary();

				foreach( var Role in roles )
				{
					if( DictRoles.TryGetValue( Role.Name, out var Rle ) )
						StaffRoles.Add( new StaffRole { StaffId = staffId, RoleId = Rle.Id } );
				}

				E.SaveChanges();

				var After = ( from R in Entity.StaffRoles
				              where R.StaffId == staffId
				              select R ).ToList();

				return ( Before: Before, After: After, Ok: true );
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}

			return ( Before: null, After: null, Ok: false );
		}


		public bool IsStaffMemberADriver( string staffId )
		{
			var DictRoles = GetRolesAsDictionaryById();

			var Sr = from S in Entity.StaffRoles
			         where S.StaffId == staffId
			         select S;

			foreach( var StaffRole in Sr )
			{
				if( DictRoles.TryGetValue( StaffRole.RoleId, out var Rle ) )
				{
					if( Rle.IsDriver )
						return true;
				}
			}

			return false;
		}
	}
}