﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Protocol.Data._Customers.Pml;
using Utils;
using Inventory = Database.Model.Databases.MasterTemplate.Inventory;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public List<Trip> PML_ClaimTrips( ClaimTrips requestObject )
		{
			var Result = new List<Trip>();

			try
			{
				var E = Entity;
				var TripIds = requestObject.TripIds;

				do
				{
					try
					{
						var Trips = ( from T in E.Trips
						              where TripIds.Contains( T.TripId )
						              select T ).ToList();

						// Only process the ones that are found
                        TripIds = ( from T in Trips
						            select T.TripId ).ToList();

						foreach( var Trip in Trips )
						{
							var Before = new TripLogger( Trip );

							Trip.DriverCode = requestObject.UserName;
							Trip.DeliveryNotes = $"Cage: {requestObject.Cage}\r\n{Trip.DeliveryNotes}";
							Trip.ClaimedTime = requestObject.ClaimTime;
							Trip.ClaimedLatitude = requestObject.Latitude;
							Trip.ClaimedLongitude = requestObject.Longitude;
							Trip.Status2 = (int)STATUS1.CLAIMED;

							E.SaveChanges();

							var After = new TripLogger( Trip );
							LogTrip( requestObject.ProgramName, Before, After );

							TripIds.Remove( Trip.TripId );
							Result.Add( Trip );
						}
					}

					catch( DbUpdateConcurrencyException )
					{
					}
				} while( TripIds.Count > 0 );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}


		public TripList PML_FindTripsDetailedUpdatedSince( PmlFindSince requestObject )
		{
			var RetVal = new TripList();

			try
			{
				var ServiceLevels = requestObject.ServiceLevels.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );

				for( var I = 0; I < ServiceLevels.Length; I++ )
					ServiceLevels[ I ] = ServiceLevels[ I ].Trim();

				var StartTime = DateTime.UtcNow.AddSeconds( -Math.Abs( requestObject.SinceSeconds ) );
				var FromStatus = (int)requestObject.FromStatus;
				var ToStatus = (int)requestObject.ToStatus;

				List<Trip> Trips;

				if( requestObject.AllZones )
				{
					Trips = ( from T in Entity.Trips
					          where ( T.LastModified >= StartTime )
					                && ServiceLevels.Contains( T.ServiceLevel )
					                && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
					          select T ).ToList();
				}
				else
				{
					var Zones = requestObject.Zones.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );

					for( var I = 0; I < Zones.Length; I++ )
						Zones[ I ] = Zones[ I ].Trim();

					Trips = ( from T in Entity.Trips
					          where ( T.LastModified >= StartTime )
					                && Zones.Contains( T.CurrentZone )
					                && ServiceLevels.Contains( T.ServiceLevel )
					                && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
					          select T ).ToList();
				}

				foreach( var Trip in Trips )
					RetVal.Add( Trip.ToTrip() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return RetVal;
		}


		public TripUpdate PML_updateTripDetailedQuick( TripUpdate requestObject )
		{
			try
			{
				if( requestObject.TripId.IsNullOrEmpty() )
					requestObject.TripId = NextTripId( Context );

				requestObject.BroadcastToDispatchBoard = true;
				requestObject.BroadcastToDriver = false;
				requestObject.BroadcastToDriverBoard = false;

				var E = Entity;

				// Auto dispatch
				var PostalRec = ( from P in E.PostalCodeToRoutes
				                  where P.PostalCode == requestObject.PickupAddressPostalCode
				                  select P ).FirstOrDefault();

				if( !( PostalRec is null ) )
				{
					var Route = ( from R in E.Routes
					              where R.RouteId == PostalRec.RouteId
					              select R.Name ).FirstOrDefault();

					if( !( Route is null ) )
					{
						requestObject.Status1 = STATUS.DISPATCHED;
						requestObject.Driver = Route;
						requestObject.PickupZone = PostalRec.Zone4;
						requestObject.BroadcastToDispatchBoard = false;
						requestObject.BroadcastToDriver = true;
						requestObject.BroadcastToDriverBoard = true;
					}
				}

				var (LastModified, Ok) = AddUpdateTrip( requestObject );

				if( Ok )
				{
					requestObject.LastModified = LastModified;

					// Update the item barcodes and descriptions
					var Inv = E.Inventories;

					foreach( var Package in requestObject.Packages )
					{
						foreach( var Item in Package.Items )
						{
							var Rec = ( from I in Inv
							            where I.Barcode == Item.Barcode
							            select I ).FirstOrDefault();

							if( Rec is null )
							{
								Inv.Add( new Inventory
								         {
									         Barcode = Item.Barcode,
									         InventoryCode = Item.Barcode,
									         ShortDescription = Item.Description,
									         LongDescription = Item.Description,
									         Active = true,
									         PackageQuantity = Item.Pieces,
									         UnitWeight = Item.Weight,

									         UNClass = "",
									         PackageType = "",
									         BinX = "",
									         BinY = "",
									         BinZ = "",
									         DangerousGoods = false,
									         IsContainer = false,
									         UnitVolume = 0
								         } );
							}
							else
							{
								Rec.ShortDescription = Item.Description;
								Rec.LongDescription = Item.Description;
								Rec.PackageQuantity = Item.Pieces;
								Rec.UnitWeight = Item.Weight;
							}

							E.SaveChanges();
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return requestObject;
		}
	}
}