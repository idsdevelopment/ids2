﻿#nullable enable

using System;
using System.Linq;
using Protocol.Data;
using Protocol.Data._Customers.Pml;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private static InventoryList ToInventoryList( PmlInventoryList inventory )
		{
			var Result = new InventoryList();

			Result.AddRange( from I in inventory
			                 select (Inventory)I );

			return Result;
		}

		public void PML_AddUpdateInventory( PmlInventoryList inventory )
		{
			AddUpdateInventory( ToInventoryList( inventory ) );
		}

		public void PML_DeleteInventory( PmlInventoryList inventory )
		{
			DeleteInventory( ToInventoryList( inventory ) );
		}


		public PmlInventoryList PML_GetInventoryList()
		{
			var Result = new PmlInventoryList();

			try
			{
				Result.AddRange( from I in GetInventoryList()
				                 select new PmlInventory( I ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}
}