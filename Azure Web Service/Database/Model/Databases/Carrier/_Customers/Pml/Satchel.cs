﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Newtonsoft.Json;
using Protocol.Data;
using Protocol.Data._Customers.Pml;
using Storage.Carrier;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public List<Trip> PML_ClaimSatchels( ClaimSatchels satchels )
		{
			var Result = new List<Trip>();

			try
			{
				var E          = Entity;
				var SatchelIds = satchels.SatchelIds;

				do
				{
					try
					{
						var Trips = ( from T in E.Trips
						              where SatchelIds.Contains( T.TripId )
						              select T ).ToList();

						// Only process the ones that are found
						SatchelIds = ( from T in Trips
						               select T.TripId ).ToList();

						foreach( var T in Trips )
						{
							var Before = new TripLogger( T );

							new Signatures( Context ).AddUpdate( T.TripId, satchels.Signature );

							T.DriverCode       = satchels.UserName;
							T.DeliveryNotes    = $"Cage: {satchels.Cage}\r\n{T.DeliveryNotes}";
							T.ClaimedTime      = satchels.ClaimTime;
							T.ClaimedLatitude  = satchels.Latitude;
							T.ClaimedLongitude = satchels.Longitude;
							T.Status2          = (int)STATUS1.CLAIMED;
							E.SaveChanges();

							var After = new TripLogger( T );

							LogTrip( satchels.ProgramName, Before, After );

							Result.Add( T );
							SatchelIds.Remove( T.TripId );
						}
					}
					catch( DbUpdateConcurrencyException )
					{
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );

						break;
					}
				} while( SatchelIds.Count > 0 );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}


		public Trip PML_VerifySatchel( VerifySatchel verify )
		{
			while( true )
			{
				try
				{
					var E = Entity;

					var Satchel = ( from S in E.Trips
					                where S.TripId == verify.TripId
					                select S ).FirstOrDefault();

					if( !( Satchel is null ) )
					{
						var Before = new TripLogger( Satchel );

						Satchel.POD     = verify.UserName;
						Satchel.Status1 = (int)STATUS.VERIFIED;

						Satchel.VerifiedTime      = verify.DateTime.DateTime;
						Satchel.VerifiedLatitude  = verify.Latitude;
						Satchel.VerifiedLongitude = verify.Longitude;

						E.SaveChanges();

						var After = new TripLogger( Satchel );

						LogTrip( verify.ProgramName, Before, After );

						return Satchel;
					}

					throw new Exception( $"Satchel Id: \"{verify.TripId}\" Not Found." );
				}
				catch( DbUpdateConcurrencyException )
				{
					continue;
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
				}

				return null;
			}
		}


		public ( List<Trip> Deleted, Trip Satchel ) PML_UpdateSatchel( Satchel s )
		{
			var Result = ( Deleted: new List<Trip>(), Satchel: (Trip)null );

			try
			{
				var  E          = Entity;
				var  Trips      = E.Trips;
				var  TripIds    = new List<string>();
				Trip PickupTrip = null;

				var Now = DateTimeOffset.Now;

				foreach( var TripId in s.TripIds )
				{
					var Rec = ( from T in Trips
					            where T.TripId == TripId
					            select T ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						PickupTrip = Rec;

						TripIds.Add( TripId );

						new Signatures( Context ).AddUpdate( TripId, s.Signature );

						var Before = new TripLogger( Rec );

						Rec.Reference = s.SatchelId;
						Rec.POP       = s.POP;

						var After = new TripLogger( Rec );
						E.SaveChanges();

						LogTrip( s.ProgramName, Before, After );
						DeleteTrip( s.ProgramName, Rec, DateTime.Now );
						Rec.Status1 = (int)STATUS.DELETED;
						Result.Deleted.Add( Rec );
					}
				}

				if( !( PickupTrip is null ) )
				{
					var Sid = s.SatchelId;

					string NextId( string sid )
					{
						var P = sid.LastIndexOf( '-' );

						if( P > 0 )
						{
							var Temp = sid.Split( new[] { "-" }, StringSplitOptions.RemoveEmptyEntries );
							var Ndx  = Temp.Length - 1;
							var Num  = int.Parse( Temp[ Ndx ] );
							Array.Resize( ref Temp, Ndx );

							var Tmp = string.Join( "-", Temp );

							if( ( Num % 10 ) != 9 )
								return $"{Tmp}-{Num + 1}";
						}

						return $"{sid}-1";
					}

					// Make sure satchel id is unique
					// LastOrDefault not supported by EF.
					var SatchelId = ( from T in Trips
					                  where T.TripId.StartsWith( Sid )
					                  orderby T.TripId descending
					                  select T.TripId ).FirstOrDefault();

					if( !( SatchelId is null ) )
						Sid = NextId( SatchelId );
					else
					{
						// LastOrDefault not supported by EF.
						SatchelId = ( from T in E.TripStorageIndexes
						              where T.TripId.StartsWith( Sid )
						              orderby T.TripId descending
						              select T.TripId ).FirstOrDefault();

						if( !( SatchelId is null ) )
							Sid = NextId( SatchelId );
					}

					var STrip = new Trip( PickupTrip )
					            {
						            TripId          = Sid,
						            Reference       = string.Join( ",", TripIds ),
						            Status1         = (int)STATUS.PICKED_UP,
						            PackageType     = "Satchel",
						            TripItemsAsJson = JsonConvert.SerializeObject( s.Packages ),
						            PickupTime      = s.PickupTime,
						            PickupLatitude  = s.PickupLatitude,
						            PickupLongitude = s.PickupLongitude,
						            CurrentZone     = PickupTrip.PickupZone,
						            POP             = s.POP
					            };

					Trips.Add( STrip );
					E.SaveChanges();

					new Signatures( Context ).AddUpdate( Sid, s.Signature );

					LogTrip( s.ProgramName, new TripLogger( STrip ) );

					Result.Satchel = STrip;
				}
				else
					throw new Exception( "No trip ids" );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}
}