﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Utils;
using Utils.Csv;
using Role = Protocol.Data.Role;
using Staff = Protocol.Data.Staff;
using Zone = Database.Model.Databases.MasterTemplate.Zone;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private const int POST_CODE = 0,
		                  OPERATION = 1,
		                  REGION = 2,
		                  ROUTE = 3;

		private const string PML_POSTAL_CODE_IMPORT = "PML Post Codes";

		public void PML_PostCodeImport( string csv )
		{
			try
			{
				var DriverRole = ( from R in GetRoles()
				                   where R.IsDriver
				                   select R ).FirstOrDefault();

				if( !( DriverRole is null ) )
				{
					var Csv = new Csv( csv );

					var E = Entity;
					var PCodes = E.PostalCodeToRoutes;
					var Routes = E.Routes;
					var Zones = E.Zones;

					var RouteNames = new HashSet<string>();

					foreach( Row Row in Csv )
					{
						var PostCode = Row[ POST_CODE ].AsString.Trim();

						if( PostCode.IsNotNullOrWhiteSpace() )
						{
							var Operation = Row[ OPERATION ].AsString.Trim();

							if( Operation.IsNotNullOrWhiteSpace() )
							{
								var Region = Row[ REGION ].AsString.Trim();

								if( Region.IsNotNullOrWhiteSpace() )
								{
									var Route = Row[ ROUTE ].AsString.Trim();

									RouteNames.Add( Route );

									if( Route.IsNotNullOrWhiteSpace() )
									{
										var Zone = $"{Operation}+{Region}";

										var ZRec = ( from Z in Zones
										             where Z.ZoneName == Zone
										             select Z ).FirstOrDefault();

										if( ZRec is null )
										{
											ZRec = new Zone
											       {
												       ZoneName = Zone,
												       Abbreviation = $"{Operation.SubStr( 0, 9 )}+{Region.SubStr( 0, 9 )}"
											       };
											Zones.Add( ZRec );
										}

										E.SaveChanges();

										var RouteId = ( from R in Routes
										                where R.Name == Route
										                select R.RouteId ).DefaultIfEmpty( -1 ).FirstOrDefault();

										if( RouteId == -1 )
										{
											var (Id, Ok) = AddUpdateRoute( PML_POSTAL_CODE_IMPORT, Route );

											if( !Ok )
												continue;

											RouteId = Id;
										}

										var Rec = ( from P in PCodes
										            where PostCode == P.PostalCode
										            select P ).FirstOrDefault();

										if( Rec is null )
										{
											Rec = new PostalCodeToRoute
											      {
												      PostalCode = PostCode,
												      RouteId = RouteId,
												      Zone1 = Operation,
												      Zone2 = Region,
												      Zone3 = "",
												      Zone4 = Zone // So Zone delete follows through
											      };

											PCodes.Add( Rec );
										}
										else
										{
											Rec.RouteId = RouteId;
											Rec.Zone1 = Operation;
											Rec.Zone2 = Region;
										}

										E.SaveChanges();
									}
								}
							}
						}
					}

					// Create the staff records
					var StaffIds = ( from S in E.Staffs
					                 select S.StaffId ).ToList();

					var NewIds = ( from R in RouteNames
					               where !StaffIds.Contains( R )
					               select R ).ToList();

					foreach( var Id in NewIds )
					{
						var Staff = new UpdateStaffMemberWithRolesAndZones( PML_POSTAL_CODE_IMPORT, new Staff
						                                                                            {
							                                                                            StaffId = Id,
							                                                                            Enabled = true,
							                                                                            Password = Id,
							                                                                            FirstName = Id
						                                                                            } );
						Staff.Roles.Add( DriverRole );
						AddUpdateStaffMember( Staff, false );
					}
				}
				else
					throw new Exception( "No Driver Roles defined" );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}