﻿using System;
using System.Linq;
using Protocol.Data;
using Protocol.Data.Customers.Priority;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private static string GetLocationCode( string coName )
		{
			if( !string.IsNullOrWhiteSpace( coName ) )
			{
				var Ndx = coName.LastIndexOf( ')' );

				if( Ndx > 0 )
				{
					var Ndx1 = coName.LastIndexOf( '(', Ndx - 1 );
					var Len = Ndx - Ndx1 - 1;

					if( Len > 0 )
						return coName.Substring( Ndx1 + 1, Len );
				}
			}

			return "";
		}


		public PriorityDeviceRoutes PriorityGetRouteNamesForDevice()
		{
			var Result = new PriorityDeviceRoutes();

			try
			{
				var E = Entity;

				var PharmacyRoutes = ( from Pharmacy in ( from Customer in E.ResellerCustomers
				                                          where Customer.BoolOption1
				                                          let CoName = ( from Co in E.ResellerCustomerCompanySummaries
				                                                         where Co.CompanyId == Customer.CompanyId
				                                                         select Co.CompanyName ).FirstOrDefault()
				                                          select new { Customer, CoName } )
				                       let RouteNames = ( from Route in E.Routes
				                                          where Route.ShowOnMobileApp && ( Route.CustomerCode == Pharmacy.Customer.CustomerCode )
				                                          select Route.Name ).ToList()
				                       where RouteNames.Any()
				                       select new { Pharmacy, RouteNames } ).ToList();

				Result.AddRange( PharmacyRoutes.Select( pharmacyRoute => new PriorityPharmacyRoutes
				                                                         {
					                                                         Pharmacy = pharmacyRoute.Pharmacy.Customer.CustomerCode,
					                                                         LocationCode = GetLocationCode( pharmacyRoute.Pharmacy.CoName ),
					                                                         RouteNames = pharmacyRoute.RouteNames
				                                                         } ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( PriorityGetRouteNamesForDevice ), Exception );
			}

			return Result;
		}

		public void EnableRouteForCustomer( EnableRouteForeCustomer requestObject )
		{
			try
			{
				var Cust = ( from C in Entity.ResellerCustomers
				             where C.CustomerCode == requestObject.CustomerCode
				             select C ).FirstOrDefault();

				if( !( Cust is null ) )
				{
					Cust.BoolOption1 = requestObject.Enable;
					Entity.SaveChanges();
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( EnableRouteForCustomer ), Exception );
			}
		}
	}
}