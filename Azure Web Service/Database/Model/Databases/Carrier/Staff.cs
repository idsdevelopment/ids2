﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Utils;
using Address = Database.Model.Databases.MasterTemplate.Address;
using Staff = Protocol.Data.Staff;
using Zone = Database.Model.Databases.MasterTemplate.Zone;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private static Staff Convert( MasterTemplate.Staff s )
		{
			return new Staff
			       {
				       Ok = true,
				       StaffId = s.StaffId,
				       MainCommission1 = s.MainCommission1,
				       FirstName = s.FirstName,
				       LastName = s.LastName,
				       EmergencyLastName = s.EmergencyLastName,
				       EmergencyEmail = s.EmergencyEmail,
				       EmergencyMobile = s.EmergencyMobile,
				       EmergencyPhone = s.EmergencyPhone,
				       OverrideCommission1 = s.OverrideCommission1,
				       EmergencyFirstName = s.EmergencyFirstName,
				       MainCommission2 = s.MainCommission2,
				       Enabled = s.Enabled,
				       OverrideCommission2 = s.OverrideCommission2,
				       OverrideCommission3 = s.OverrideCommission3,
				       TaxNumber = s.TaxNumber,
				       MiddleNames = s.MiddleNames,
				       Password = Encryption.ToTimeLimitedToken( s.Password ),
				       MainCommission3 = s.MainCommission3,
				       OverrideCommission10 = s.OverrideCommission10,
				       OverrideCommission11 = s.OverrideCommission11,
				       OverrideCommission12 = s.OverrideCommission12,
				       OverrideCommission4 = s.OverrideCommission4,
				       OverrideCommission5 = s.OverrideCommission5,
				       OverrideCommission6 = s.OverrideCommission6,
				       OverrideCommission7 = s.OverrideCommission7,
				       OverrideCommission8 = s.OverrideCommission8,
				       OverrideCommission9 = s.OverrideCommission9
			       };
		}

		public class StaffLogger
		{
			public string StaffId { get; set; }
			public bool Enabled { get; set; }
			public string Password { get; set; }
			public string TaxNumber { get; set; }
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string MiddleNames { get; set; }
			public string CommissionFormula { get; set; }

			public string EmergencyFirstName { get; set; }
			public string EmergencyLastName { get; set; }
			public string EmergencyPhone { get; set; }
			public string EmergencyMobile { get; set; }
			public string EmergencyEmail { get; set; }

			public decimal MainCommission1 { get; set; }
			public decimal MainCommission2 { get; set; }
			public decimal MainCommission3 { get; set; }

			public decimal OverrideCommission1 { get; set; }
			public decimal OverrideCommission2 { get; set; }
			public decimal OverrideCommission3 { get; set; }
			public decimal OverrideCommission4 { get; set; }
			public decimal OverrideCommission5 { get; set; }
			public decimal OverrideCommission6 { get; set; }
			public decimal OverrideCommission7 { get; set; }
			public decimal OverrideCommission8 { get; set; }
			public decimal OverrideCommission9 { get; set; }
			public decimal OverrideCommission10 { get; set; }
			public decimal OverrideCommission11 { get; set; }
			public decimal OverrideCommission12 { get; set; }

			public string Roles { get; set; } = "";
			public string Zones { get; set; } = "";

			public AddressLogger Before { get; set; }
			public AddressLogger After { get; set; }
			private readonly CarrierDb Db;

			public void UpdateRoles( IEnumerable<StaffRole> roles )
			{
				var Rls = new StringBuilder();

				var First = true;

				var DictRoles = Db.GetRolesAsDictionaryById();

				foreach( var Role in roles )
				{
					if( First )
						First = false;
					else
						Rls.Append( ", " );

					Rls.Append( DictRoles.TryGetValue( Role.RoleId, out var Rle ) ? Rle.RoleName : "????" );
				}

				Roles = Rls.ToString();
			}


			public void UpdateZones( IEnumerable<Zone> zones )
			{
				var Zns = new StringBuilder();

				var First = true;

				foreach( var Zone in zones )
				{
					if( First )
						First = false;
					else
						Zns.Append( ", " );

					Zns.Append( Zone.ZoneName );
				}

				Zones = Zns.ToString();
			}

			public StaffLogger( CarrierDb db, MasterTemplate.Staff s )
			{
				Db = db;

				StaffId = s.StaffId;
				Enabled = s.Enabled;
				Password = s.Password;
				TaxNumber = s.TaxNumber;
				FirstName = s.FirstName;
				LastName = s.LastName;
				MiddleNames = s.MiddleNames;
				CommissionFormula = s.CommissionFormula;

				EmergencyFirstName = s.EmergencyFirstName;
				EmergencyLastName = s.EmergencyLastName;
				EmergencyPhone = s.EmergencyPhone;
				EmergencyMobile = s.EmergencyMobile;
				EmergencyEmail = s.EmergencyEmail;

				MainCommission1 = s.MainCommission1;
				MainCommission2 = s.MainCommission2;
				MainCommission3 = s.MainCommission3;

				OverrideCommission1 = s.OverrideCommission1;
				OverrideCommission2 = s.OverrideCommission2;
				OverrideCommission3 = s.OverrideCommission3;
				OverrideCommission4 = s.OverrideCommission4;
				OverrideCommission5 = s.OverrideCommission5;
				OverrideCommission6 = s.OverrideCommission6;
				OverrideCommission7 = s.OverrideCommission7;
				OverrideCommission8 = s.OverrideCommission8;
				OverrideCommission9 = s.OverrideCommission9;
				OverrideCommission10 = s.OverrideCommission10;
				OverrideCommission11 = s.OverrideCommission11;
				OverrideCommission12 = s.OverrideCommission12;

				var Addr = new AddressLogger( new Address() );
				Before = Addr;
				After = Addr;
			}


			public StaffLogger( CarrierDb db, MasterTemplate.Staff s, AddressLogger before ) : this( db, s )
			{
				Before = before;
			}

			public StaffLogger( CarrierDb db, MasterTemplate.Staff s, AddressLogger before, AddressLogger after ) : this( db, s, before )
			{
				After = after;
			}

			public StaffLogger( CarrierDb db, MasterTemplate.Staff staff, IEnumerable<StaffRole> roles ) : this( db, staff )
			{
				UpdateRoles( roles );
			}

			public StaffLogger( CarrierDb db, MasterTemplate.Staff staff, IEnumerable<StaffRole> roles, IEnumerable<Zone> zones ) : this( db, staff, roles )
			{
				UpdateZones( zones );
			}
		}


		public StaffLookupSummaryList StaffLookup( StaffLookup lookup )
		{
			var RetVal = new StaffLookupSummaryList();

			try
			{
				var Key = lookup.Key;
				var Count = lookup.PreFetchCount;

				IEnumerable<MasterTemplate.Staff> Result;

				switch( lookup.Fetch )
				{
				case PreFetch.PREFETCH.FIRST:
					Result = ( from S in Entity.Staffs
					           orderby S.StaffId
					           select S ).Take( Count );

					break;

				case PreFetch.PREFETCH.LAST:
					Result = ( from S in Entity.Staffs
					           orderby S.StaffId descending
					           select S ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_MATCH:
					Result = ( from S in Entity.Staffs
					           where S.StaffId.StartsWith( Key )
					           orderby S.StaffId
					           select S ).Take( Count );

					break;

				case PreFetch.PREFETCH.FIND_RANGE:
					if( Count >= 0 ) // Next
					{
						Result = ( from S in Entity.Staffs
						           where S.StaffId.CompareTo( Key ) >= 0
						           orderby S.StaffId
						           select S ).Take( Count );
					}
					else // Prior
					{
						Result = ( from S in Entity.Staffs
						           where S.StaffId.CompareTo( Key ) <= 0
						           orderby S.StaffId
						           select S ).Take( -Count );
					}

					break;

				case PreFetch.PREFETCH.OFFSET:
					Result = ( from S in Entity.Staffs
					           orderby S.StaffId
					           select S ).Skip( lookup.Offset ).Take( -Count );

					break;

				default:
					Context.SystemLogException( new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

					return RetVal;
				}

				RetVal.AddRange( from S in Result
				                 let Addr = GetAddress( S.AddressId )
				                 let A = Addr.Ok ? Addr.Address : new Address()
				                 select new StaffLookupSummary
				                        {
					                        AddressLine1 = A.AddressLine1 ?? "",
					                        StaffId = S.StaffId,
					                        FirstName = S.FirstName,
					                        LastName = S.LastName
				                        } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( CustomersLookup ), E );
			}

			return RetVal;
		}

		public bool StaffMemberExists( string staffId )
		{
			return ( from Staff in Entity.Staffs
			         where Staff.StaffId == staffId
			         select Staff.StaffId ).FirstOrDefault().IsNotNullOrEmpty();
		}

		public ( bool UserNameOk, bool PasswordOk, int UserId, bool Ok ) LoginOk( string staffId, string password )
		{
			var Result = ( UserNameOk: false, PasswordOk: false, UserId: -1, Ok: false );

			staffId = staffId.ToLower();

			var Rec = ( from Staff in Entity.Staffs
			            where Staff.StaffId.ToLower() == staffId
			            select Staff ).FirstOrDefault();

			if( Rec != null )
			{
				Result.UserId = Rec.Id;
				Result.UserNameOk = true;

				if( Rec.Password == password )
				{
					Result.PasswordOk = true;
					Result.Ok = true;
				}
			}

			return Result;
		}

		public List<MasterTemplate.Staff> GetStaff()
		{
			return ( from S in Entity.Staffs
			         select S ).ToList();
		}


		public StaffList GetStaffAsProtocolStaff()
		{
			var Result = new StaffList();
			Result.AddRange( GetStaff().Select( Convert ) );

			return Result;
		}


		public Staff GetStaffMember( string staffId )
		{
			var Rec = ( from S in Entity.Staffs
			            where S.StaffId == staffId
			            select S ).FirstOrDefault();

			return Rec == null ? new Staff() : Convert( Rec );
		}

		public int GetStaffMemberInternalId( string staffId )
		{
			return ( from S in Entity.Staffs
			         where S.StaffId == staffId
			         select S.Id ).FirstOrDefault();
		}


		public void AddUpdateStaffMember( UpdateStaffMemberWithRolesAndZones staffMember, bool encryptedPassword = true )
		{
			try
			{
				var Prog = staffMember.ProgramName;
				var S = staffMember.Staff;
				var A = S.Address;

				if( encryptedPassword )
				{
					var (Value, Ok) = Encryption.FromTimeLimitedToken( S.Password );

					if( !Ok )
						throw new Exception( "Invalid password" );

					S.Password = Value;
				}

				var Stf = Entity.Staffs;

				var Rec = ( from St in Stf
				            where St.StaffId == staffMember.Staff.StaffId
				            select St ).FirstOrDefault();

				if( Rec is null )
				{
					var (AddressId, Address, AddressOk) = AddAddress( A, staffMember.ProgramName );

					if( AddressOk )
					{
						var Staff = new MasterTemplate.Staff
						            {
							            AddressId = AddressId,
							            StaffId = S.StaffId,
							            Enabled = S.Enabled,

							            FirstName = S.FirstName,
							            LastName = S.LastName,
							            MiddleNames = S.MiddleNames,
							            Password = S.Password,

							            EmergencyFirstName = S.EmergencyFirstName,
							            EmergencyLastName = S.EmergencyLastName,
							            EmergencyMobile = S.EmergencyMobile,
							            EmergencyPhone = S.EmergencyPhone,
							            EmergencyEmail = S.EmergencyEmail,

							            TaxNumber = S.TaxNumber,
							            CommissionFormula = "",

							            MainCommission1 = S.MainCommission1,
							            MainCommission2 = S.MainCommission2,
							            MainCommission3 = S.MainCommission3,

							            OverrideCommission1 = S.OverrideCommission1,
							            OverrideCommission2 = S.OverrideCommission2,
							            OverrideCommission3 = S.OverrideCommission3,
							            OverrideCommission4 = S.OverrideCommission4,
							            OverrideCommission5 = S.OverrideCommission5,
							            OverrideCommission6 = S.OverrideCommission6,
							            OverrideCommission7 = S.OverrideCommission7,
							            OverrideCommission8 = S.OverrideCommission8,
							            OverrideCommission9 = S.OverrideCommission9,
							            OverrideCommission10 = S.OverrideCommission10,
							            OverrideCommission11 = S.OverrideCommission11,
							            OverrideCommission12 = S.OverrideCommission12
						            };
						Stf.Add( Staff );
						Entity.SaveChanges();
						Context.LogNew( Context.GetStorageId( LOG.STAFF ), Prog, "Add staff member", new StaffLogger( this, Staff, Address ) );

						AddUpdateStaffRoles( S.StaffId, staffMember.Roles );
					}
				}
				else
				{
					var (AddressBefore, AddressAfter, AddressId, AddressOk) = UpdateAddress( Rec.AddressId, A, Prog );

					if( AddressOk )
					{
						var Before = new StaffLogger( this, Rec );

						Rec.AddressId = AddressId;
						Rec.Enabled = S.Enabled;
						Rec.FirstName = S.FirstName;
						Rec.LastName = S.LastName;
						Rec.MiddleNames = S.MiddleNames;
						Rec.Password = S.Password;
						Rec.EmergencyFirstName = S.EmergencyFirstName;
						Rec.EmergencyLastName = S.EmergencyLastName;
						Rec.EmergencyMobile = S.EmergencyMobile;
						Rec.EmergencyPhone = S.EmergencyPhone;
						Rec.EmergencyEmail = S.EmergencyEmail;
						Rec.TaxNumber = S.TaxNumber;
						Rec.CommissionFormula = "";
						Rec.MainCommission1 = S.MainCommission1;
						Rec.MainCommission2 = S.MainCommission2;
						Rec.MainCommission3 = S.MainCommission3;
						Rec.OverrideCommission1 = S.OverrideCommission1;
						Rec.OverrideCommission2 = S.OverrideCommission2;
						Rec.OverrideCommission3 = S.OverrideCommission3;
						Rec.OverrideCommission4 = S.OverrideCommission4;
						Rec.OverrideCommission5 = S.OverrideCommission5;
						Rec.OverrideCommission6 = S.OverrideCommission6;
						Rec.OverrideCommission7 = S.OverrideCommission7;
						Rec.OverrideCommission8 = S.OverrideCommission8;
						Rec.OverrideCommission9 = S.OverrideCommission9;
						Rec.OverrideCommission10 = S.OverrideCommission10;
						Rec.OverrideCommission11 = S.OverrideCommission11;
						Rec.OverrideCommission12 = S.OverrideCommission12;

						Entity.SaveChanges();

						var After = new StaffLogger( this, Rec, AddressBefore, AddressAfter );

						var (BFore, Afr, IsOk) = AddUpdateStaffRoles( S.StaffId, staffMember.Roles );

						if( IsOk )
						{
							Before.UpdateRoles( BFore );
							After.UpdateRoles( Afr );
						}

						Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), Prog, "Update staff member", Before, After );
					}
				}
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( AddUpdateStaffMember ), E );
			}
		}

		public StaffLookupSummaryList GetDrivers()
		{
			var Result = new StaffLookupSummaryList();

			try
			{
				var E = Entity;

				Result.AddRange( from S in E.Staffs
				                 where S.Enabled && ( from Sr in E.StaffRoles
				                                      where ( from R in E.Roles
				                                              where R.IsDriver
				                                              select R.Id ).Contains( Sr.RoleId )
				                                      select Sr.StaffId ).Contains( S.StaffId )
				                 orderby S.StaffId
				                 select new StaffLookupSummary
				                        {
					                        FirstName = S.FirstName,
					                        LastName = S.LastName,
					                        StaffId = S.StaffId,

					                        AddressLine1 = ( from A in E.Addresses
					                                         where A.Id == S.AddressId
					                                         select A.AddressLine1 ).FirstOrDefault() ?? ""
				                        } );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}
}