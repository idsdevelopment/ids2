﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure_Web_Service;
using Database.Utils;
using Microsoft.Azure.Management.Sql.Fluent;
using Microsoft.Azure.Management.Sql.Fluent.Models;
using Protocol.Data;
using ResourceGroups;
using Storage;
using Storage.Carrier;
using Utils;
using Constants = Storage.Constants;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		internal const string COMPLETED = "Completed";

		public long DeleteTestCarrier( string carrierId )
		{
			var Pid = Database.Users.StartProcess( Context );

			Task.Run( () =>
			          {
				          try
				          {
					          if( carrierId.StartsWith( Constants.TEST, StringComparison.OrdinalIgnoreCase ) )
					          {
						          var TestContext = Context.Clone( carrierId );

						          // Delete the Blob Storage
						          StorageAccount.DeleteStorageAccount( TestContext );

						          // Delete the Database
						          using var Db = new CarrierDb( TestContext );
						          Db.Entity.Database.Delete();

						          // Delete the Endpoints
						          Database.Users.DeleteEndpoint( TestContext );

						          // Delete Logins
						          Database.Users.DeleteAllResellerCustomers( TestContext );
					          }
				          }
				          catch( Exception E )
				          {
					          Context.SystemLogException( E );
				          }
				          finally
				          {
					          Database.Users.EndProcess( Context, Pid );
				          }
			          } );

			return Pid;
		}


		public BackupList BackupList()
		{
			var Result = new BackupList();

			try
			{
				var Backups = new Backups( Context );

				Result.AddRange( Backups.BlobList );
			}
			catch // Probably no storage created yet
			{
			}

			return Result;
		}

		public bool DeleteBackups( BackupList backups )
		{
			try
			{
				foreach( var BackupName in backups )
				{
					var Backups = new Backups( Context, BackupName );
					Backups.Blob?.Delete();
				}

				return true;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return false;
		}

		private static bool StatusOk( ImportExportResponseInner status )
		{
			return status.Status.Compare( COMPLETED, StringComparison.OrdinalIgnoreCase ) == 0;
		}

		public long BackupRestore( BackupRestore request )
		{
			var Pid = Database.Users.StartProcess( Context );

			try
			{
				Task.Run( async () =>
				          {
					          const string OK = "Ok";

					          var Response = "FAIL";

					          try
					          {
						          var SqlManagementClient = new SqlManagementClient( Subscription.GetRestClient( Context ) )
						                                    {
							                                    SubscriptionId = Subscription.ID
						                                    };

						          var Connection   = Entity.Database.Connection;
						          var DatabaseName = Connection.Database;
						          var Server       = Connection.DataSource;
						          var OldEdition   = DbUtils.GetEdition( Context, Entity );

						          if( Server != null )
						          {
							          var P = Server.IndexOf( '.' );

							          if( P > 0 )
								          Server = Server.Substring( 0, P );
						          }

						          var Backups = new Backups( Context, request.Name );

						          void ResponseOk( ImportExportResponseInner status )
						          {
							          if( StatusOk( status ) )
								          Response = OK;
						          }

						          async Task<IEnumerable<DatabaseInner>> DataBaseList()
						          {
							          var Temp = await SqlManagementClient.Databases.ListByServerWithHttpMessagesAsync( Groups.DATABASE_RESOURCE_GROUP, Server );

							          return Temp.Body;
						          }

						          Task DeleteDatabase( string name )
						          {
							          return SqlManagementClient.Databases.DeleteAsync( Groups.DATABASE_RESOURCE_GROUP, Server, name );
						          }

						          void RenameDatabase( string oldName, string newName )
						          {
							          DbUtils.RenameDatabase( Context, Connection, oldName, newName );
						          }

						          if( request.IsRestore )
						          {
							          var SaveName = DatabaseName + "_Save";

							          if( !request.IsTestRestore )
							          {
								          var TempRec = ( from D in await DataBaseList()
								                          where D.Name == SaveName
								                          select D ).FirstOrDefault();

								          if( !( TempRec is null ) )
									          await DeleteDatabase( SaveName );

								          RenameDatabase( DatabaseName, SaveName ); // Rename Current Database to _Save
							          }

							          var TempDb = DatabaseName + "_Temp";
							          await DeleteDatabase( TempDb );

							          var ImportRequestParameters = new ImportRequest
							                                        {
								                                        AdministratorLogin         = Subscription.ADMINISTRATOR_NAME,
								                                        AdministratorLoginPassword = Subscription.ADMINISTRATOR_PASSWORD,
								                                        AuthenticationType         = AuthenticationType.SQL,

								                                        StorageKey     = Context.StorageAccountKey,
								                                        StorageKeyType = StorageKeyType.StorageAccessKey,
								                                        StorageUri     = Backups.Blob?.Uri.ToString(),

								                                        DatabaseName         = TempDb,
								                                        Edition              = DatabaseEdition.Standard,
								                                        ServiceObjectiveName = ServiceObjectiveName.S2,
								                                        MaxSizeBytes         = "268435456000" // 250Gb
							                                        };

							          // Restore to Temp
							          if( StatusOk( await SqlManagementClient.Databases.ImportAsync( Groups.DATABASE_RESOURCE_GROUP, Server, ImportRequestParameters ) ) )
							          {
								          try
								          {
									          if( request.IsTestRestore )
									          {
										          // Fudge test database context
										          const string TEST = Constants.TEST;

										          var SaveCid = Context.CarrierId;
										          DatabaseName      = TEST + SaveCid;
										          Context.CarrierId = DatabaseName;

										          try
										          {
											          // New set of blob storage
											          if( Database.Users.CreateStorageEndPoint( Context, StorageAccount.ReCreateBlobStorageAccount( Context ) ) is null )
											          {
												          await DeleteDatabase( TempDb );

												          return;
											          }

											          Database.Users.CloneResellerCustomer( Context, SaveCid, TEST );
										          }
										          finally
										          {
											          Context.CarrierId = SaveCid;
										          }
									          }

									          RenameDatabase( TempDb, DatabaseName );

									          if( !request.IsTestRestore )
										          await DeleteDatabase( SaveName );
								          }
								          finally
								          {
									          DbUtils.SetEdition( Context, Entity, OldEdition );
								          }

								          Response = OK;
							          }
						          }
						          else
						          {
							          try
							          {
								          DbUtils.SetEdition( Context, Entity, DbUtils.EDITION.S2 );

								          var ExportRequestParameters = new ExportRequest
								                                        {
									                                        AdministratorLogin         = Subscription.ADMINISTRATOR_NAME,
									                                        AdministratorLoginPassword = Subscription.ADMINISTRATOR_PASSWORD,
									                                        AuthenticationType         = AuthenticationType.SQL,

									                                        StorageKey     = Context.StorageAccountKey,
									                                        StorageKeyType = StorageKeyType.StorageAccessKey,
									                                        StorageUri     = Backups.Blob?.Uri.ToString()
								                                        };

								          ResponseOk( await SqlManagementClient.Databases.ExportAsync( Groups.DATABASE_RESOURCE_GROUP, Server, DatabaseName, ExportRequestParameters ) );
							          }
							          finally
							          {
								          DbUtils.SetEdition( Context, Entity, OldEdition );
							          }
						          }
					          }
					          catch( Exception E )
					          {
						          Context.SystemLogException( E );
					          }
					          finally
					          {
						          // ReSharper disable once AccessToModifiedClosure
						          Database.Users.EndProcess( Context, Pid, Response );
					          }
				          } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
				Database.Users.EndProcess( Context, Pid );

				return -1;
			}

			return Pid;
		}
	}
}