﻿using System;
using System.Linq;
using Database.Model.Databases.MasterTemplate;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public Configuration GetConfiguration()
		{
			return ( from C in Entity.Configurations
			         select C ).FirstOrDefault();
		}

		public void UpdateConfiguration( Configuration c )
		{
			try
			{
				var Configurations = Entity.Configurations;

				var Cfg = ( from C in Configurations
				            select C ).FirstOrDefault();

				if( !( Cfg is null ) )
				{
					Cfg.CompanyAddressId = c.CompanyAddressId;
					Cfg.StorageAccount = c.StorageAccount;
					Cfg.StorageAccountKey = c.StorageAccountKey;
					Cfg.StorageBlobEndpoint = c.StorageBlobEndpoint;
					Cfg.StorageFileEndpoint = c.StorageFileEndpoint;
					Cfg.StorageTableEndpoint = c.StorageTableEndpoint;
				}
				else
					Configurations.Add( c );

				Entity.SaveChanges();
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}
	}
}