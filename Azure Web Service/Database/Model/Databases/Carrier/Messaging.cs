﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using Database.Model.Databases.MasterTemplate;
using Utils;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public class DataAndIdType
		{
			public string Data;
			public long Id;
			public string Type;
		}

		public class Listener
		{
			public string Topic,
			              Queue,
			              MessageType;
		}


		public void DeleteMessagesById( List<long> idList )
		{
			while( idList.Count > 0 )
			{
				try
				{
					var E         = Entity;
					var Messaging = E.Messagings;

					Messaging.RemoveRange( from M in Messaging
					                       where idList.Contains( M.Id )
					                       select M );

					E.SaveChanges();
				}
				catch // Possibly deleted by someone else
				{
					continue;
				}

				return;
			}
		}

		public void SendToQueue( string topic, string queue, List<DataAndIdType> packets )
		{
			try
			{
				var E    = Entity;
				var Time = DateTime.UtcNow;

				topic = topic.TrimToLower();
				queue = queue.TrimToLower();

				List<string> QueueList;

				if( queue.IsNullOrWhiteSpace() )
				{
					// To all queues within the topic
					QueueList = ( from M in E.Messagings
					              where M.IsHeader && ( M.Topic == topic )
					              select M.Queue ).ToList();
				}
				else
				{
					var Rec = ( from M in E.Messagings
					            where M.IsHeader && ( M.Topic == topic ) && ( M.Queue == queue )
					            select M ).FirstOrDefault();

					if( Rec is null )
						return; // No Listener

					QueueList = new List<string> { queue };
				}

				var RecList = new List<Messaging>();

				foreach( var DataType in packets )
				{
					foreach( var Queue in QueueList )
					{
						RecList.Add( new Messaging
						             {
							             DataType = DataType.Type,
							             DateTime = Time,
							             Queue    = Queue,
							             Topic    = topic,
							             Packet   = DataType.Data
						             } );
					}
				}

				E.Messagings.AddRange( RecList );
				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}


		public void UpdateListener( List<Listener> listeners )
		{
			foreach( var Listener in listeners )
				UpdateListener( Listener.Topic, Listener.Queue, Listener.MessageType );
		}

		public void UpdateListener( string topic, string queue, string messageType )
		{
			topic = topic.TrimToLower();
			queue = queue.TrimToLower();

			void LogException( Exception ex )
			{
				if( !( ex is ThreadAbortException ) ) // Possibly IIS shutting down
				{
					var ExceptionType = ex.GetType().FullName;
					Context.SystemLogException( $"UpdateListener\r\nExceptionType: {ExceptionType}\r\nMessage: {ex.Message}" );
				}
			}

			while( true )
			{
				try
				{
					var E        = Entity;
					var Messages = E.Messagings;
					var Now      = DateTime.UtcNow;

					var Rec = ( from M in Messages
					            where M.IsHeader && ( M.DataType == messageType ) && ( M.Topic == topic ) && ( M.Queue == queue )
					            select M ).FirstOrDefault();

					if( Rec is null )
					{
						Rec = new Messaging
						      {
							      IsHeader = true,
							      DataType = messageType,
							      Queue    = queue,
							      Topic    = topic,
							      DateTime = Now,
							      Packet   = ""
						      };
						Messages.Add( Rec );
					}
					else
						Rec.DateTime = Now;

					E.SaveChanges();

					return;
				}
				catch( DbUpdateConcurrencyException )
				{
				}
				catch( CommitFailedException )
				{
				}
				catch( Exception Exception )
				{
					LogException( Exception );

					return;
				}

				Thread.Sleep( 1000 );
			}
		}

		public List<(string Topic, string Queue, string Packet, long Id, string Type)> GetPendingMessages()
		{
			try
			{
				var Start = DateTimeOffset.UtcNow.AddMinutes( -10 );

				var Messaging = Entity.Messagings;

				// Get Active Listeners

				// ------- !!!!!!!!!!!! Don't bring to list leave in the connector !!!!!!!!!!!!!! -----------
				var Temp = from L in from M in Messaging
				                     where M.IsHeader && ( M.DateTime >= Start )
				                     select M
				           let Mgs = from M1 in Messaging
				                     where !M1.IsHeader && ( M1.Topic == L.Topic ) && ( M1.Queue == L.Queue )
				                     orderby M1.Id
				                     select M1
				           select Mgs;

				var Result = new List<(string Topic, string Queue, string Packet, long Id, string Type)>();

				foreach( var T in Temp )
				{
					// ReSharper disable once LoopCanBeConvertedToQuery
					foreach( var M1 in T )
						Result.Add( ( M1.Topic, M1.Queue, M1.Packet, M1.Id, M1.DataType ) );
				}

				return Result;
			}
			catch( ThreadAbortException ) // IIS Timeout
			{
			}

			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return new List<(string, string, string, long, string)>();
		}

		public void ClearQueue( string topic, string queue )
		{
			try
			{
				var E         = Entity;
				var Messaging = E.Messagings;

				topic = topic.TrimToLower();
				queue = queue.TrimToLower();

				Messaging.RemoveRange( from M in Messaging
				                       where !M.IsHeader && ( M.Topic == topic ) && ( M.Queue == queue )
				                       select M );
				E.SaveChanges();
			}
			catch( ThreadAbortException ) // IIS Timeout
			{
			}

			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void CleanUpMessages()
		{
			try
			{
				while( true )
				{
					try
					{
						using var Db = new CarrierDb( Context );

						var E1 = Db.Entity;
						var M1 = E1.Messagings;

						var DateLimit = DateTime.UtcNow.AddDays( -3 );

						M1.RemoveRange( from M in M1
						                where M.DateTime < DateLimit
						                select M );
						E1.SaveChanges();

						break;
					}
					catch( DbUpdateConcurrencyException )
					{
					}
					catch( Exception Exception )
					{
						Context.SystemLogException( Exception );

						break;
					}
				}
			}
			catch( ThreadAbortException ) // IIS Timeout
			{
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}