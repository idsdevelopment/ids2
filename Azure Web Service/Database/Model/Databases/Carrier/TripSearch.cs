﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Model.Databases.MasterTemplate;
using Interfaces.Interfaces;
using Protocol.Data;
using Storage.Carrier;
using Utils;
using Constants = Storage.Constants;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private IEnumerable<Trip> FindTripsForCompany( SearchTrips requestObject )
		{
			var trips = new List<Trip>();

			if( requestObject.SelectedCompanyPUAddressName.IsNotNullOrWhiteSpace() && requestObject.SelectedCompanyDelAddressName.IsNotNullOrWhiteSpace() )
				trips.AddRange( FindTripsForCompanyPUAddressAndDelAddress( requestObject ) );

			else if( requestObject.SelectedCompanyPUAddressName.IsNotNullOrWhiteSpace() )
				trips.AddRange( FindTripsForCompanyPUAddress( requestObject ) );

			else if( requestObject.SelectedCompanyDelAddressName.IsNotNullOrWhiteSpace() )
				trips.AddRange( FindTripsForCompanyDelAddress( requestObject ) );
			else if( requestObject.CompanyCode.IsNotNullOrWhiteSpace() )
				trips.AddRange( FindTripsForCompanyAddress( requestObject ) );

			return trips;
		}

		private IEnumerable<Trip> FindTripsForCompanyPUAddressAndDelAddress( SearchTrips requestObject )
		{
			var Trips = new List<Trip>();

			var CompanyCode                   = requestObject.CompanyCode;
			var SelectedCompanyPuAddressName  = requestObject.SelectedCompanyPUAddressName;
			var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
			var FromStatus                    = requestObject.StartStatus;
			var ToStatus                      = requestObject.EndStatus;
			var PackageType                   = requestObject.PackageType;
			var ServiceLevel                  = requestObject.ServiceLevel;

			var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
			var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

			if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.PackageType == PackageType )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.PackageType == PackageType )
				                select T );
			}
			else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPuAddressName )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                select T );
			}

			return Trips;
		}

		// ReSharper disable once InconsistentNaming
		private IEnumerable<Trip> FindTripsForCompanyPUAddress( SearchTrips requestObject )
		{
			var Trips = new List<Trip>();

			var CompanyCode = requestObject.CompanyCode;

			// ReSharper disable once InconsistentNaming
			var SelectedCompanyPUAddressName = requestObject.SelectedCompanyPUAddressName;
			var FromStatus                   = requestObject.StartStatus;
			var ToStatus                     = requestObject.EndStatus;
			var PackageType                  = requestObject.PackageType;
			var ServiceLevel                 = requestObject.ServiceLevel;

			var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
			var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

			if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                             && ( T.PackageType == PackageType )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                             && ( T.PackageType == PackageType )
				                select T );
			}
			else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PickupCompanyName == SelectedCompanyPUAddressName )
				                select T );
			}

			return Trips;
		}

		private IEnumerable<Trip> FindTripsForCompanyDelAddress( SearchTrips requestObject )
		{
			var Trips = new List<Trip>();

			var CompanyCode                   = requestObject.CompanyCode;
			var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
			var FromStatus                    = requestObject.StartStatus;
			var ToStatus                      = requestObject.EndStatus;
			var PackageType                   = requestObject.PackageType;
			var ServiceLevel                  = requestObject.ServiceLevel;

			var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
			var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

			if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.PackageType == PackageType )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.PackageType == PackageType )
				                select T );
			}
			else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.DeliveryCompanyName == SelectedCompanyDelAddressName )
				                select T );
			}

			return Trips;
		}

		/// <summary>
		///     Don't care about Pickup or Delivery addresses.
		/// </summary>
		/// <param name="requestObject"></param>
		/// <returns></returns>
		private IEnumerable<Trip> FindTripsForCompanyAddress( SearchTrips requestObject )
		{
			var Trips = new List<Trip>();

			var CompanyCode = requestObject.CompanyCode;

			//var SelectedCompanyPUAddressName = requestObject.SelectedCompanyPUAddressName;
			//var SelectedCompanyDelAddressName = requestObject.SelectedCompanyDelAddressName;
			var FromStatus   = requestObject.StartStatus;
			var ToStatus     = requestObject.EndStatus;
			var PackageType  = requestObject.PackageType;
			var ServiceLevel = requestObject.ServiceLevel;

			var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
			var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

			if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PackageType == PackageType )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.PackageType == PackageType )
				                select T );
			}
			else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.AccountId == CompanyCode )
				                select T );
			}

			return Trips;
		}

		private IEnumerable<Trip> FindTripsForPackageTypeAndOrServiceLevel( SearchTrips requestObject )
		{
			var Trips = new List<Trip>();

			var FromStatus   = requestObject.StartStatus;
			var ToStatus     = requestObject.EndStatus;
			var PackageType  = requestObject.PackageType;
			var ServiceLevel = requestObject.ServiceLevel;

			var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
			var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

			if( requestObject.PackageType.IsNotNullOrWhiteSpace() && requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.PackageType == PackageType )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else if( requestObject.PackageType.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.PackageType == PackageType )
				                select T );
			}
			else if( requestObject.ServiceLevel.IsNotNullOrWhiteSpace() )
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                                             && ( T.ServiceLevel == ServiceLevel )
				                select T );
			}
			else
			{
				Trips.AddRange( from T in Entity.Trips
				                where ( T.CallTime >= From ) && ( T.CallTime <= To )
				                                             && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
				                select T );
			}

			return Trips;
		}

		public TripList GetTripsStartingWith( SearchTripsByStatus request )
		{
			var Result = new TripList();

			try
			{
				var FromStatus = (int)request.StartStatus;
				var ToStatus   = (int)request.EndStatus;

				var Tasks = new[]
				            {
					            Task.Run( () =>
					                      {
						                      try
						                      {
							                      using var Db = new CarrierDb( Context );

							                      var Trips = ( from T in Db.Entity.Trips
							                                    where T.TripId.StartsWith( request.TripId ) && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
							                                    select T ).ToList();

							                      lock( Result )
							                      {
								                      Result.AddRange( from T in Trips
								                                       select T.ToTrip() );
							                      }
						                      }
						                      catch( Exception Exception )
						                      {
							                      Context.SystemLogException( Exception );
						                      }
					                      } ),

					            Task.Run( () =>
					                      {
						                      try
						                      {
							                      if( request.EndStatus >= STATUS.PAID )
							                      {
								                      using var Db = new CarrierDb( Context );

								                      var Trips = ( from T in Db.Entity.TripStorageIndexes
								                                    where T.TripId.StartsWith( request.TripId ) && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
								                                    select T ).ToList();

								                      var OrigContext = Context.Debug ? OriginalCarrierContext() : null;

								                      foreach( var StorageIndex in Trips )
								                      {
									                      Task[] Tsks;

									                      void GetTrips( IRequestContext ctx )
									                      {
										                      var TripList = TripsByDate.GetTrips( ctx, StorageIndex.CallTime, StorageIndex.TripId );

										                      lock( Result )
											                      Result.AddRange( TripList );
									                      }

									                      if( !( OrigContext is null ) )
									                      {
										                      Tsks = new Task[ 2 ];

										                      Tsks[ 1 ] = Task.Run( () =>
										                                            {
											                                            GetTrips( OrigContext );
										                                            } );
									                      }
									                      else
										                      Tsks = new Task[ 1 ];

									                      Tsks[ 0 ] = Task.Run( () =>
									                                            {
										                                            GetTrips( Context );
									                                            } );

									                      Task.WaitAll( Tsks );
								                      }
							                      }
						                      }
						                      catch( Exception Exception )
						                      {
							                      Context.SystemLogException( Exception );
						                      }
					                      } )
				            };

				Task.WaitAll( Tasks );

				var Temp = ( from T in Result
				             orderby T.TripId
				             select T ).ToList();
				Result.Clear();
				Result.AddRange( Temp );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}


		public TripList DbSearchTrips( SearchTrips requestObject )
		{
			var Result = new TripList();

			void AddTrips( IEnumerable<Trip> trips )
			{
				Result.AddRange( from T in trips
				                 select T.ToTrip() );
			}

			try
			{
				if( requestObject.ByReference )
				{
					AddTrips( from T in Entity.Trips
					          where T.Reference == requestObject.Reference
					          select T );
				}
				else if( requestObject.ByTripId )
				{
					AddTrips( from T in Entity.Trips
					          where T.TripId == requestObject.TripId
					          select T );
				}
				else
				{
					AddTrips( requestObject.CompanyCode.IsNotNullOrWhiteSpace()
						          ? FindTripsForCompany( requestObject )
						          : FindTripsForPackageTypeAndOrServiceLevel( requestObject )
					        );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}


		public TripList StorageSearchTrips( SearchTrips requestObject )
		{
			var RetVal = new TripList();

			var              ToStatus = requestObject.EndStatus;
			TripStorageIndex Rec      = null;

			if( requestObject.ByTripId || requestObject.ByReference )
			{
				using var Db                 = new CarrierDb( Context );
				var       TripStorageIndices = Db.Entity.TripStorageIndexes;

				if( requestObject.ByTripId )
				{
					Rec = ( from Ndx in TripStorageIndices
					        where Ndx.TripId == requestObject.TripId
					        select Ndx ).FirstOrDefault();
				}
				else if( requestObject.ByReference )
				{
					Rec = ( from Ndx in TripStorageIndices
					        where Ndx.Reference == requestObject.Reference
					        select Ndx ).FirstOrDefault();
				}

				if( !( Rec is null ) )
					RetVal = TripsByDate.GetTrips( Context, Rec.CallTime, Rec.TripId );

				return RetVal;
			}

			switch( ToStatus )
			{
			case STATUS.PAID:
			case STATUS.DELETED:
				{
					using var Db = new CarrierDb( Context );

					var TNdx = Db.Entity.TripStorageIndexes;

					var CompanyCode                = requestObject.CompanyCode;
					var SelectedCompanyAddressName = requestObject.SelectedCompanyPUAddressName;

					var From = new DateTimeOffset( requestObject.FromDate.Date, requestObject.FromDate.Offset );
					var To   = new DateTimeOffset( requestObject.ToDate.Date.AddDays( 1 ).AddMilliseconds( -1 ), requestObject.ToDate.Offset );

					var FromStatus = requestObject.StartStatus;

					if( FromStatus < STATUS.PAID )
						FromStatus = STATUS.PAID;

					IQueryable<TripStorageIndex> Recs;

					if( CompanyCode.IsNotNullOrWhiteSpace() )
					{
						if( SelectedCompanyAddressName.IsNotNullOrWhiteSpace() )
						{
							Recs = from T in TNdx
							       where ( T.CallTime >= From ) && ( T.CallTime <= To )
							                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
							                                    && ( T.AccountId == CompanyCode )
							                                    && ( ( T.PickupCompanyName == SelectedCompanyAddressName ) || ( T.DeliveryCompanyName == SelectedCompanyAddressName ) )
							       select T;
						}
						else
						{
							Recs = from T in TNdx
							       where ( T.CallTime >= From ) && ( T.CallTime <= To )
							                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
							                                    && ( T.AccountId == CompanyCode )
							       select T;
						}
					}
					else
					{
						// All companies
						Recs = from T in TNdx
						       where ( T.CallTime >= From ) && ( T.CallTime <= To )
						                                    && ( T.Status1 >= (sbyte)FromStatus ) && ( T.Status1 <= (sbyte)ToStatus )
						       select T;
					}

					foreach( var StorageIndex in Recs )
						RetVal.AddRange( TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId ) );
				}

				break;
			}

			return RetVal;
		}

		public IRequestContext OriginalCarrierContext()
		{
			var Cid = Context.CarrierId.TrimStart( Constants.TEST, StringComparison.OrdinalIgnoreCase );

			return Context.Clone( Cid );
		}

		public TripList SearchTrips( SearchTrips requestObject )
		{
			var Result = new TripList();

			Task.WaitAll( Task.Run( () =>
			                        {
				                        var RetVal = DbSearchTrips( requestObject );

				                        lock( Result )
					                        Result.AddRange( RetVal );
			                        } ),
			              Task.Run( () =>
			                        {
				                        Task[] Tasks;

				                        // If debug also search original carrier
				                        if( Context.Debug )
				                        {
					                        Tasks = new Task[ 2 ];

					                        Tasks[ 1 ] = Task.Run( () =>
					                                               {
						                                               using var Db      = new CarrierDb( OriginalCarrierContext() );
						                                               var       RetVal1 = Db.StorageSearchTrips( requestObject );

						                                               lock( Result )
							                                               Result.AddRange( RetVal1 );
					                                               } );
				                        }
				                        else
					                        Tasks = new Task[ 1 ];

				                        Tasks[ 0 ] = Task.Run( () =>
				                                               {
					                                               var RetVal = StorageSearchTrips( requestObject );

					                                               lock( Result )
						                                               Result.AddRange( RetVal );
				                                               } );

				                        Task.WaitAll( Tasks );
			                        } ) );

			Result.Sort( ( trip, trip1 ) => trip.CallTime.CompareTo( trip1.CallTime ) );

			return Result;
		}
	}
}