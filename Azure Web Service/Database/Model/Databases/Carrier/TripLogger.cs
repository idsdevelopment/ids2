﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Protocol.Data;
using Storage.AuditTrails;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public class TripLogger
		{
			public string TripId { get; set; }

			public string Location { get; set; }

			public DateTime LastModified { get; set; }

			public int Status1 { get; set; }
			public string Status1AsText => ( (STATUS)Status1 ).AsString();
			public int Status2 { get; set; }
			public string Status2AsText => ( (STATUS1)Status2 ).AsString();
			public int Status3 { get; set; }
			public string Status3AsText => "";

			public bool ReceivedByDevice { get; set; }
			public bool ReadByDriver { get; set; }

			public string Barcode { get; set; }
			public string AccountId { get; set; }

			public string ContainerId { get; set; }

			public string Reference { get; set; }

			public DateTimeOffset CallTime { get; set; }
			public string CallerPhone { get; set; }
			public string CallerEmail { get; set; }

			public DateTimeOffset ReadyTime { get; set; }
			public DateTimeOffset DueTime { get; set; }

			public string DriverCode { get; set; }
			public string Board { get; set; }
			public string PackageType { get; set; }
			public string ServiceLevel { get; set; }

			public string PickupAccountId { get; set; }
			public long PickupCompanyId { get; set; }
			public string PickupCompanyName { get; set; }
			public string PickupAddressBarcode { get; set; }
			public string PickupAddressPostalBarcode { get; set; }
			public string PickupAddressSuite { get; set; }
			public string PickupAddressAddressLine1 { get; set; }
			public string PickupAddressAddressLine2 { get; set; }
			public string PickupAddressVicinity { get; set; }
			public string PickupAddressCity { get; set; }
			public string PickupAddressRegion { get; set; }
			public string PickupAddressPostalCode { get; set; }
			public string PickupAddressCountry { get; set; }
			public string PickupAddressCountryCode { get; set; }
			public string PickupAddressPhone { get; set; }
			public string PickupAddressPhone1 { get; set; }
			public string PickupAddressMobile { get; set; }
			public string PickupAddressMobile1 { get; set; }
			public string PickupAddressFax { get; set; }
			public string PickupAddressEmailAddress { get; set; }
			public string PickupAddressEmailAddress1 { get; set; }
			public string PickupAddressEmailAddress2 { get; set; }
			public string PickupContact { get; set; }
			public decimal PickupAddressLatitude { get; set; }
			public decimal PickupAddressLongitude { get; set; }
			public string PickupAddressNotes { get; set; }
			public string PickupNotes { get; set; }


			public string DeliveryAccountId { get; set; }
			public long DeliveryCompanyId { get; set; }
			public string DeliveryCompanyName { get; set; }
			public string DeliveryAddressBarcode { get; set; }
			public string DeliveryAddressPostalBarcode { get; set; }
			public string DeliveryAddressSuite { get; set; }
			public string DeliveryAddressAddressLine1 { get; set; }
			public string DeliveryAddressAddressLine2 { get; set; }
			public string DeliveryAddressVicinity { get; set; }
			public string DeliveryAddressCity { get; set; }
			public string DeliveryAddressRegion { get; set; }
			public string DeliveryAddressPostalCode { get; set; }
			public string DeliveryAddressCountry { get; set; }
			public string DeliveryAddressCountryCode { get; set; }
			public string DeliveryAddressPhone { get; set; }
			public string DeliveryAddressPhone1 { get; set; }
			public string DeliveryAddressMobile { get; set; }
			public string DeliveryAddressMobile1 { get; set; }
			public string DeliveryAddressFax { get; set; }
			public string DeliveryAddressEmailAddress { get; set; }
			public string DeliveryAddressEmailAddress1 { get; set; }
			public string DeliveryAddressEmailAddress2 { get; set; }
			public string DeliveryContact { get; set; }
			public decimal DeliveryAddressLatitude { get; set; }
			public decimal DeliveryAddressLongitude { get; set; }
			public string DeliveryAddressNotes { get; set; }
			public string DeliveryNotes { get; set; }

			public string BillingAccountId { get; set; }
			public long BillingCompanyId { get; set; }
			public string BillingCompanyName { get; set; }
			public string BillingAddressBarcode { get; set; }
			public string BillingAddressPostalBarcode { get; set; }
			public string BillingAddressSuite { get; set; }
			public string BillingAddressAddressLine1 { get; set; }
			public string BillingAddressAddressLine2 { get; set; }
			public string BillingAddressVicinity { get; set; }
			public string BillingAddressCity { get; set; }
			public string BillingAddressRegion { get; set; }
			public string BillingAddressPostalCode { get; set; }
			public string BillingAddressCountry { get; set; }
			public string BillingAddressCountryCode { get; set; }
			public string BillingAddressPhone { get; set; }
			public string BillingAddressPhone1 { get; set; }
			public string BillingAddressMobile { get; set; }
			public string BillingAddressMobile1 { get; set; }
			public string BillingAddressFax { get; set; }
			public string BillingAddressEmailAddress { get; set; }
			public string BillingAddressEmailAddress1 { get; set; }
			public string BillingAddressEmailAddress2 { get; set; }
			public string BillingContact { get; set; }
			public decimal BillingAddressLatitude { get; set; }
			public decimal BillingAddressLongitude { get; set; }
			public string BillingAddressNotes { get; set; }
			public string BillingNotes { get; set; }

			public string CurrentZone { get; set; }
			public string PickupZone { get; set; }
			public string DeliveryZone { get; set; }
			public decimal Weight { get; set; }
			public decimal Pieces { get; set; }
			public decimal Volume { get; set; }
			public string Measurement { get; set; }
			public decimal OriginalPieceCount { get; set; }

			public byte[] MissingPieces { get; set; }


			public bool DangerousGoods { get; set; }
			public bool HasItems { get; set; }
			public string UNClass { get; set; }
			public bool HasDangerousGoodsDocuments { get; set; }
			public string POP { get; set; }
			public string POD { get; set; }
			public DateTimeOffset PickupTime { get; set; }
			public double PickupLatitude { get; set; }
			public double PickupLongitude { get; set; }
			public DateTimeOffset DeliveredTime { get; set; }
			public double DeliveredLatitude { get; set; }
			public double DeliveredLongitude { get; set; }
			public DateTimeOffset VerifiedTime { get; set; }
			public double VerifiedLatitude { get; set; }
			public double VerifiedLongitude { get; set; }
			public DateTimeOffset ClaimedTime { get; set; }
			public double ClaimedLatitude { get; set; }
			public double ClaimedLongitude { get; set; }
			public List<TripPackage> TripPackages { get; set; }
			public List<TripCharge> TripCharges { get; set; }

			public string CallerName { get; set; }
			public bool IsQuote { get; set; }
			public string UndeliverableNotes { get; set; }

			public TripLogger( Trip t )
			{
				TripId = t.TripId;
				Location = t.Location;
				Status1 = t.Status1;
				LastModified = t.LastModified;
				Status2 = t.Status2;
				Status3 = t.Status3;

				ReceivedByDevice = t.ReceivedByDevice;
				ReadByDriver = t.ReadByDriver;

				Barcode = t.Barcode;
				AccountId = t.AccountId;
				ContainerId = t.ContainerId;
				Reference = t.Reference;
				CallTime = t.CallTime;
				CallerPhone = t.CallerPhone;
				CallerEmail = t.CallerEmail;
				ReadyTime = t.ReadyTime;
				DueTime = t.DueTime;
				DriverCode = t.DriverCode;

				Board = t.Board;
				PackageType = t.PackageType;
				ServiceLevel = t.ServiceLevel;

				PickupCompanyId = t.PickupCompanyId;
				PickupCompanyName = t.PickupCompanyName;
				PickupAddressBarcode = t.PickupAddressBarcode;
				PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
				PickupAddressSuite = t.PickupAddressSuite;
				PickupAddressAddressLine1 = t.PickupAddressAddressLine1;
				PickupAddressAddressLine2 = t.PickupAddressAddressLine2;
				PickupAddressVicinity = t.PickupAddressVicinity;
				PickupAddressCity = t.PickupAddressCity;
				PickupAddressRegion = t.PickupAddressRegion;
				PickupAddressPostalCode = t.PickupAddressPostalCode;
				PickupAddressCountry = t.PickupAddressCountry;
				PickupAddressCountryCode = t.PickupAddressCountryCode;
				PickupAddressPhone = t.PickupAddressPhone;
				PickupAddressPhone1 = t.PickupAddressPhone1;
				PickupAddressMobile = t.PickupAddressMobile;
				PickupAddressMobile1 = t.PickupAddressMobile1;
				PickupAddressFax = t.PickupAddressFax;
				PickupAddressEmailAddress = t.PickupAddressEmailAddress;
				PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
				PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
				PickupContact = t.PickupContact;
				PickupAddressLatitude = t.PickupAddressLatitude;
				PickupAddressLongitude = t.PickupAddressLongitude;
				PickupAddressNotes = t.PickupAddressNotes;
				PickupNotes = t.PickupNotes;

				DeliveryCompanyId = t.DeliveryCompanyId;
				DeliveryCompanyName = t.DeliveryCompanyName;
				DeliveryAddressBarcode = t.DeliveryAddressBarcode;
				DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
				DeliveryAddressSuite = t.DeliveryAddressSuite;
				DeliveryAddressAddressLine1 = t.DeliveryAddressAddressLine1;
				DeliveryAddressAddressLine2 = t.DeliveryAddressAddressLine2;
				DeliveryAddressVicinity = t.DeliveryAddressVicinity;
				DeliveryAddressCity = t.DeliveryAddressCity;
				DeliveryAddressRegion = t.DeliveryAddressRegion;
				DeliveryAddressPostalCode = t.DeliveryAddressPostalCode;
				DeliveryAddressCountry = t.DeliveryAddressCountry;
				DeliveryAddressCountryCode = t.DeliveryAddressCountryCode;
				DeliveryAddressPhone = t.DeliveryAddressPhone;
				DeliveryAddressPhone1 = t.DeliveryAddressPhone1;
				DeliveryAddressMobile = t.DeliveryAddressMobile;
				DeliveryAddressMobile1 = t.DeliveryAddressMobile1;
				DeliveryAddressFax = t.DeliveryAddressFax;
				DeliveryAddressEmailAddress = t.DeliveryAddressEmailAddress;
				DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
				DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
				DeliveryContact = t.DeliveryContact;
				DeliveryAddressLatitude = t.DeliveryAddressLatitude;
				DeliveryAddressLongitude = t.DeliveryAddressLongitude;
				DeliveryAddressNotes = t.DeliveryAddressNotes;
				DeliveryNotes = t.DeliveryNotes;

				BillingCompanyId = t.BillingCompanyId;
				BillingCompanyName = t.BillingCompanyName;
				BillingAddressBarcode = t.BillingAddressBarcode;
				BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
				BillingAddressSuite = t.BillingAddressSuite;
				BillingAddressAddressLine1 = t.BillingAddressAddressLine1;
				BillingAddressAddressLine2 = t.BillingAddressAddressLine2;
				BillingAddressVicinity = t.BillingAddressVicinity;
				BillingAddressCity = t.BillingAddressCity;
				BillingAddressRegion = t.BillingAddressRegion;
				BillingAddressPostalCode = t.BillingAddressPostalCode;
				BillingAddressCountry = t.BillingAddressCountry;
				BillingAddressCountryCode = t.BillingAddressCountryCode;
				BillingAddressPhone = t.BillingAddressPhone;
				BillingAddressPhone1 = t.BillingAddressPhone1;
				BillingAddressMobile = t.BillingAddressMobile;
				BillingAddressMobile1 = t.BillingAddressMobile1;
				BillingAddressFax = t.BillingAddressFax;
				BillingAddressEmailAddress = t.BillingAddressEmailAddress;
				BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
				BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
				BillingContact = t.BillingContact;
				BillingAddressLatitude = t.BillingAddressLatitude;
				BillingAddressLongitude = t.BillingAddressLongitude;
				BillingAddressNotes = t.BillingAddressNotes;
				BillingNotes = t.BillingNotes;

				CurrentZone = t.CurrentZone;
				PickupZone = t.PickupZone;
				DeliveryZone = t.DeliveryZone;

				Weight = t.Weight;
				Pieces = t.Pieces;
				Volume = t.Volume;
				Measurement = t.Measurement;
				OriginalPieceCount = t.OriginalPieceCount;
				MissingPieces = t.MissingPieces;

				DangerousGoods = t.DangerousGoods;
				HasItems = t.HasItems;
				UNClass = t.UNClass;
				HasDangerousGoodsDocuments = t.HasDangerousGoodsDocuments;
				POP = t.POP;
				POD = t.POD;
				PickupTime = t.PickupTime;
				PickupLatitude = t.PickupLatitude;
				PickupLongitude = t.PickupLongitude;
				DeliveredTime = t.DeliveredTime;
				DeliveredLatitude = t.DeliveredLatitude;
				DeliveredLongitude = t.DeliveredLongitude;
				VerifiedTime = t.VerifiedTime;
				VerifiedLatitude = t.VerifiedLatitude;
				VerifiedLongitude = t.VerifiedLongitude;
				ClaimedTime = t.ClaimedTime;
				ClaimedLatitude = t.ClaimedLatitude;
				ClaimedLongitude = t.ClaimedLongitude;

				TripCharges = JsonConvert.DeserializeObject<List<TripCharge>>( t.TripChargesAsJson ) ?? new List<TripCharge>();
				TripPackages = JsonConvert.DeserializeObject<List<TripPackage>>( t.TripItemsAsJson ) ?? new List<TripPackage>();

				CallerName = t.CallerName;
				IsQuote = t.IsQuote;
				UndeliverableNotes = t.UndeliverableNotes;

				PickupAccountId = t.PickupCompanyAccountId;
				DeliveryAccountId = t.DeliveryCompanyAccountId;
				BillingAccountId = t.BillingCompanyAccountId;
			}

			public TripLogger()
			{
			}
		}

		private static readonly Random Rand = new Random();


		private void LogTrip( string prog, TripLogger logger )
		{
			TripAuditTrail.LogNew( Context, prog, logger.TripId, logger );
		}

		private void LogTrip( string prog, Trip t )
		{
			LogTrip( prog, new TripLogger( t ) );
		}

		private void LogTrip( string prog, TripLogger before, TripLogger after )
		{
			TripAuditTrail.LogDifferences( Context, prog, before.TripId, before, after );
		}

		private void LogTrip( string prog, Trip before, Trip after )
		{
			LogTrip( prog, new TripLogger( before ), new TripLogger( after ) );
		}

		private void LogDeletedTrip( string prog, TripLogger after )
		{
			TripAuditTrail.LogDeleted( Context, prog, after.TripId, after );
		}

		private void LogDeletedTrip( string prog, Trip after )
		{
			LogDeletedTrip( prog, new TripLogger( after ) );
		}
	}
}