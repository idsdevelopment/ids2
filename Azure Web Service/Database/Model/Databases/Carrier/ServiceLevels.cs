﻿#nullable enable

using System;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Utils;
using ServiceLevel = Protocol.Data.ServiceLevel;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public ServiceLevelColours GetServiceLevelColours()
		{
			var Result = new ServiceLevelColours();

			try
			{
				foreach( var S in from S in Entity.ServiceLevels
				                  select new { S.Name, S.Background_ARGB, S.Foreground_ARGB } )
				{
					Result.Add( new ServiceLevelColour
					            {
						            ServiceLevel    = S.Name,
						            Background_ARGB = (uint)S.Background_ARGB,
						            Foreground_ARGB = (uint)S.Foreground_ARGB
					            } );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public ServiceLevelListDetailed GetServiceLevelsDetailed()
		{
			var Result = new ServiceLevelListDetailed();

			try
			{
				var Temp = ( from Sl in Entity.ServiceLevels
				             select Sl ).ToList();

				Result.AddRange( from L in Temp
				                 select Convert( L ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}


		public void AddUpdateServiceLevel( ServiceLevel requestObject )
		{
			try
			{
				MasterTemplate.ServiceLevel UpdateServiceLevel( MasterTemplate.ServiceLevel s )
				{
					s.Background_ARGB = (int)requestObject.BackgroundARGB;
					s.Foreground_ARGB = (int)requestObject.ForegroundARGB;
					s.BeyondHours     = requestObject.BeyondHours;
					s.CriticalHours   = requestObject.CriticalHours;
					s.DeliveryHours   = requestObject.DeliveryHours;
					s.WarningHours    = requestObject.WarningHours;
					s.StartTime       = requestObject.StartTime;
					s.EndTime         = requestObject.EndTime;
					s.Monday          = requestObject.Monday;
					s.Tuesday         = requestObject.Tuesday;
					s.Wednesday       = requestObject.Wednesday;
					s.Thursday        = requestObject.Thursday;
					s.Friday          = requestObject.Friday;
					s.Saturday        = requestObject.Saturday;
					s.Sunday          = requestObject.Sunday;
					s.SortOrder       = (short)requestObject.SortOrder;

					return s;
				}

				var E = Entity;

				void UpdatePackageTypes( int sid )
				{
					var Spt = E.ServiceLevelPackageTypes;

					Spt.RemoveRange( from St in E.ServiceLevelPackageTypes
					                 where St.ServiceLevelId == sid
					                 select St );

					var PTypes = from P in E.PackageTypes
					             where requestObject.PackageTypes.Contains( P.Description )
					             select P;

					foreach( var PackageType in PTypes )
					{
						Spt.Add( new ServiceLevelPackageType
						         {
							         PackageTypeId  = PackageType.Id,
							         ServiceLevelId = sid
						         } );
					}
				}

				var OldName = requestObject.OldName.Trim();
				var NewName = requestObject.NewName.Trim();

				var Sv = E.ServiceLevels;

				if( OldName.IsNullOrWhiteSpace() ) // New
				{
					if( NewName.IsNotNullOrWhiteSpace() )
					{
						var Rec = ( from S in E.ServiceLevels
						            where S.Name == NewName
						            select S ).FirstOrDefault();

						if( Rec == null )
						{
							Rec = UpdateServiceLevel( new MasterTemplate.ServiceLevel { Name = NewName.Trim() } );
							Sv.Add( Rec );
							UpdatePackageTypes( Rec.Id );
							E.SaveChanges();
						}
					}
				}
				else if( NewName.IsNotNullOrWhiteSpace() ) // Rename / Modify
				{
					var Rec = ( from S in E.ServiceLevels
					            where S.Name == OldName
					            select S ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						Rec.Name = NewName.Trim();
						UpdateServiceLevel( Rec );
						UpdatePackageTypes( Rec.Id );

						E.SaveChanges();
					}
				}
				else // Delete
				{
					var Rec = ( from S in E.ServiceLevels
					            where S.Name == OldName
					            select S ).FirstOrDefault();

					if( !( Rec is null ) )
					{
						Sv.Remove( Rec );
						E.SaveChanges();
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public ServiceLevelList GetServiceLevels()
		{
			var Result = new ServiceLevelList();

			try
			{
				Result.AddRange( from S in Entity.ServiceLevels
				                 select S.Name
				               );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public static ServiceLevel Convert( MasterTemplate.ServiceLevel s ) =>
			new ServiceLevel
			{
				NewName = s.Name.Trim(),
				OldName = s.Name.Trim(),

				BackgroundARGB = (uint)s.Background_ARGB,
				ForegroundARGB = (uint)s.Foreground_ARGB,

				BeyondHours   = s.BeyondHours,
				CriticalHours = s.CriticalHours,
				DeliveryHours = s.DeliveryHours,
				StartTime     = s.StartTime,
				EndTime       = s.EndTime,
				WarningHours  = s.WarningHours,

				Monday    = s.Monday,
				Tuesday   = s.Tuesday,
				Wednesday = s.Wednesday,
				Thursday  = s.Thursday,
				Friday    = s.Friday,
				Saturday  = s.Saturday,
				Sunday    = s.Sunday,

				SortOrder = (ushort)s.SortOrder
			};


		public ServiceLevelResult GetServiceLevel( string requestObject )
		{
			var Result = new ServiceLevelResult();

			try
			{
				var E = Entity;

				var ServiceLevel = ( from S in E.ServiceLevels
				                     where S.Name == requestObject
				                     select S ).FirstOrDefault();

				if( !( ServiceLevel is null ) )
				{
					Result.ServiceLevel = Convert( ServiceLevel );

					Result.ServiceLevel.PackageTypes.AddRange( from PackageType in E.PackageTypes
					                                           where ( from ServiceLevelPackageType in E.ServiceLevelPackageTypes
					                                                   where ServiceLevelPackageType.ServiceLevelId == ServiceLevel.Id
					                                                   select ServiceLevelPackageType.PackageTypeId ).Contains( PackageType.Id )
					                                           select PackageType.Description );
					Result.Ok = true;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}
}