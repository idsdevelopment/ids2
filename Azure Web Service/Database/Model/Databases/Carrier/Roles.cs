﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public const string ADMIN = "Administrator";

		private readonly string[] RolesArray =
		{
//			ADMIN
		};

		public Roles GetRoles()
		{
			var Result = new Roles();

			try
			{
				var DbRoles = Entity.Roles;

				var Roles = ( from Role in DbRoles
				              select Role ).ToList();

				// Make sure mandatory roles exist
				var Changed = false;

				foreach( var Role in RolesArray.Where( role => Roles.All( r => r.RoleName != role ) ) )
				{
					DbRoles.Add( new MasterTemplate.Role
					             {
						             RoleName = Role,
						             Mandatory = true,
						             JsonObject = "",
						             IsAdministrator = true
					             } );

					Changed = true;
				}

				if( Changed )
				{
					Entity.SaveChanges();

					Roles = ( from Role in DbRoles
					          select Role ).ToList();
				}

				Result.AddRange( from Role in Roles
				                 select new Role
				                        {
					                        Mandatory = Role.Mandatory,
					                        Name = Role.RoleName,
					                        IsAdministrator = Role.IsAdministrator,
					                        IsDriver = Role.IsDriver,
					                        JsonObject = Role.JsonObject
				                        } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}

			return Result;
		}

		public void AddRoles( List<Role> roles )
		{
			try
			{
				var E = Entity;

				var DbRoles = E.Roles;

				foreach( var Rle in roles )
				{
					var Role = ( from R in DbRoles
					             where R.RoleName == Rle.Name
					             select R ).FirstOrDefault();

					if( Role is null )
					{
						Role = new MasterTemplate.Role
						       {
							       RoleName = Rle.Name,
							       Mandatory = Rle.Mandatory,
							       JsonObject = Rle.JsonObject,
							       IsAdministrator = Rle.IsAdministrator,
							       IsDriver = Rle.IsDriver
						       };
						DbRoles.Add( Role );
					}
					else
					{
						Role.Mandatory = Rle.Mandatory;
						Role.JsonObject = Rle.JsonObject;
					}
				}

				E.SaveChanges();
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
		}

		public void AddDefaultRoles()
		{
			var E = Entity;

			try
			{
				var DbRoles = E.Roles;

				DbRoles.RemoveRange( from R in DbRoles
				                     where R.Mandatory
				                     select R );

				foreach( var R in RolesArray )
					DbRoles.Add( new MasterTemplate.Role { Mandatory = true, RoleName = R } );

				E.SaveChanges();
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
		}


		public void AddUpdateRole( Role role )
		{
			try
			{
				var E = Entity;
				var Roles = E.Roles;
				var RoleName = role.Name.Trim();

				var Rec = ( from R in Roles
				            where R.RoleName == RoleName
				            select R ).FirstOrDefault();

				if( Rec is null )
				{
					Rec = new MasterTemplate.Role
					      {
						      RoleName = RoleName,
						      IsAdministrator = role.IsAdministrator,
						      IsDriver = role.IsDriver,
						      JsonObject = role.JsonObject
					      };
					Roles.Add( Rec );
				}
				else
				{
					Rec.IsAdministrator = role.IsAdministrator;
					Rec.IsDriver = role.IsDriver;
					Rec.JsonObject = role.JsonObject;
				}

				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void RenameRole( RoleRename role )
		{
			try
			{
				if( role.Name != role.NewName )
				{
					var E = Entity;
					var Roles = E.Roles;

					// Make sure new name doesn't exist already
					if( ( from R in Roles
					      where R.RoleName == role.NewName
					      select R ).FirstOrDefault() is null )
					{
						var Rec = ( from R in Roles
						            where R.RoleName == role.Name
						            select R ).FirstOrDefault();

						if( !( Rec is null ) )
						{
							Rec.RoleName = role.NewName;
							Rec.IsAdministrator = role.IsAdministrator;
							Rec.IsDriver = role.IsDriver;

							E.SaveChanges();
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void DeleteRoles( DeleteRoles roles )
		{
			try
			{
				var E = Entity;
				var Roles = E.Roles;

				Roles.RemoveRange( from R in Roles
				                   where !R.Mandatory && roles.Contains( R.RoleName )
				                   select R );

				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}