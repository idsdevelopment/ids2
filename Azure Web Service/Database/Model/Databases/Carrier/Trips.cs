﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Protocol.Data;
using Storage.Carrier;
using Utils;
using Trip = Protocol.Data.Trip;
using TripCharge = Protocol.Data.TripCharge;

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.MasterTemplate
{
	public static class TripExtensions
	{
		public static Protocol.Data.Trip ToTrip( this Trip t )
		{
			var Charges = t.TripChargesAsJson.IsNullOrWhiteSpace()
				              ? new List<TripCharge>()
				              : JsonConvert.DeserializeObject<List<TripCharge>>( t.TripChargesAsJson );

			var Packages = t.TripItemsAsJson.IsNullOrWhiteSpace()
				               ? new List<TripPackage>()
				               : JsonConvert.DeserializeObject<List<TripPackage>>( t.TripItemsAsJson );

			return new Protocol.Data.Trip
			       {
				       Ok = true,

				       TripId          = t.TripId,
				       ExtensionAsJson = t.ExtensionAsJson,
				       AccountId       = t.AccountId,
				       Barcode         = t.Barcode,
				       Board           = t.Board,

				       BillingAccountId            = t.BillingCompanyAccountId,
				       BillingAddressAddressLine1  = t.BillingAddressAddressLine1,
				       BillingAddressAddressLine2  = t.BillingAddressAddressLine2,
				       BillingNotes                = t.BillingNotes,
				       BillingAddressBarcode       = t.BillingAddressBarcode,
				       BillingAddressCity          = t.BillingAddressCity,
				       BillingAddressCountry       = t.BillingAddressCountry,
				       BillingAddressCountryCode   = t.BillingAddressCountryCode,
				       BillingAddressEmailAddress  = t.BillingAddressEmailAddress,
				       BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1,
				       BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2,
				       BillingAddressFax           = t.BillingAddressFax,
				       BillingAddressLatitude      = t.BillingAddressLatitude,
				       BillingAddressLongitude     = t.BillingAddressLongitude,
				       BillingAddressMobile        = t.BillingAddressMobile,
				       BillingAddressMobile1       = t.BillingAddressMobile1,
				       BillingAddressNotes         = t.BillingAddressNotes,
				       BillingAddressPhone         = t.BillingAddressPhone,
				       BillingAddressPhone1        = t.BillingAddressPhone1,
				       BillingAddressPostalBarcode = t.BillingAddressPostalBarcode,
				       BillingAddressPostalCode    = t.BillingAddressPostalCode,
				       BillingAddressRegion        = t.BillingAddressRegion,
				       BillingAddressSuite         = t.BillingAddressSuite,
				       BillingAddressVicinity      = t.BillingAddressVicinity,
				       BillingCompanyName          = t.BillingCompanyName,
				       BillingContact              = t.BillingContact,

				       PickupAccountId            = t.PickupCompanyAccountId,
				       PickupAddressAddressLine1  = t.PickupAddressAddressLine1,
				       PickupAddressAddressLine2  = t.PickupAddressAddressLine2,
				       PickupNotes                = t.PickupNotes,
				       PickupAddressBarcode       = t.PickupAddressBarcode,
				       PickupAddressCity          = t.PickupAddressCity,
				       PickupAddressCountry       = t.PickupAddressCountry,
				       PickupAddressCountryCode   = t.PickupAddressCountryCode,
				       PickupAddressEmailAddress  = t.PickupAddressEmailAddress,
				       PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1,
				       PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2,
				       PickupAddressFax           = t.PickupAddressFax,
				       PickupAddressLatitude      = t.PickupAddressLatitude,
				       PickupAddressLongitude     = t.PickupAddressLongitude,
				       PickupAddressMobile        = t.PickupAddressMobile,
				       PickupAddressMobile1       = t.PickupAddressMobile1,
				       PickupAddressNotes         = t.PickupAddressNotes,
				       PickupAddressPhone         = t.PickupAddressPhone,
				       PickupAddressPhone1        = t.PickupAddressPhone1,
				       PickupAddressPostalBarcode = t.PickupAddressPostalBarcode,
				       PickupAddressPostalCode    = t.PickupAddressPostalCode,
				       PickupAddressRegion        = t.PickupAddressRegion,
				       PickupAddressSuite         = t.PickupAddressSuite,
				       PickupAddressVicinity      = t.PickupAddressVicinity,
				       PickupCompanyName          = t.PickupCompanyName,
				       PickupContact              = t.PickupContact,

				       DeliveryAccountId            = t.DeliveryCompanyAccountId,
				       DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1,
				       DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2,
				       DeliveryNotes                = t.DeliveryNotes,
				       DeliveryAddressBarcode       = t.DeliveryAddressBarcode,
				       DeliveryAddressCity          = t.DeliveryAddressCity,
				       DeliveryAddressCountry       = t.DeliveryAddressCountry,
				       DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode,
				       DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress,
				       DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1,
				       DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2,
				       DeliveryAddressFax           = t.DeliveryAddressFax,
				       DeliveryAddressLatitude      = t.DeliveryAddressLatitude,
				       DeliveryAddressLongitude     = t.DeliveryAddressLongitude,
				       DeliveryAddressMobile        = t.DeliveryAddressMobile,
				       DeliveryAddressMobile1       = t.DeliveryAddressMobile1,
				       DeliveryAddressNotes         = t.DeliveryAddressNotes,
				       DeliveryAddressPhone         = t.DeliveryAddressPhone,
				       DeliveryAddressPhone1        = t.DeliveryAddressPhone1,
				       DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode,
				       DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode,
				       DeliveryAddressRegion        = t.DeliveryAddressRegion,
				       DeliveryAddressSuite         = t.DeliveryAddressSuite,
				       DeliveryAddressVicinity      = t.DeliveryAddressVicinity,
				       DeliveryCompanyName          = t.DeliveryCompanyName,
				       DeliveryContact              = t.DeliveryContact,

				       CallTime           = t.CallTime,
				       CallerEmail        = t.CallerEmail,
				       CallerName         = t.CallerName,
				       CallerPhone        = t.CallerPhone,
				       ClaimLatitude      = t.ClaimedLatitude,
				       ClaimLongitude     = t.ClaimedLongitude,
				       ClaimTime          = t.ClaimedTime,
				       ContainerId        = t.ContainerId,
				       CurrentZone        = t.CurrentZone,
				       DangerousGoods     = t.DangerousGoods,
				       DeliveryLatitude   = t.DeliveredLatitude,
				       DeliveryLongitude  = t.DeliveredLongitude,
				       DeliveryTime       = t.DeliveredTime,
				       DeliveryZone       = t.DeliveryZone,
				       DueTime            = t.DueTime,
				       Driver             = t.DriverCode,
				       HasDocuments       = t.HasDangerousGoodsDocuments,
				       IsQuote            = t.IsQuote,
				       LastModified       = t.LastModified,
				       Location           = t.Location,
				       Measurement        = t.Measurement,
				       MissingPieces      = t.MissingPieces,
				       OriginalPieceCount = t.OriginalPieceCount,
				       POD                = t.POD,
				       POP                = t.POP,
				       PackageType        = t.PackageType,
				       PickupLatitude     = t.PickupLatitude,
				       PickupLongitude    = t.PickupLongitude,
				       PickupTime         = t.PickupTime,
				       PickupZone         = t.PickupZone,
				       Pieces             = t.Pieces,
				       ReadByDriver       = t.ReadByDriver,
				       ReceivedByDevice   = t.ReceivedByDevice,
				       ReadyTime          = t.ReadyTime,
				       Reference          = t.Reference,

				       Status1 = (STATUS)t.Status1,
				       Status2 = (STATUS1)t.Status2,
				       Status3 = (STATUS2)t.Status3,

				       ServiceLevel       = t.ServiceLevel,
				       Signatures         = new List<Signature>(),
				       TripCharges        = Charges,
				       Packages           = Packages,
				       UNClass            = t.UNClass,
				       UndeliverableNotes = t.UndeliverableNotes,
				       VerifiedLatitude   = t.VerifiedLatitude,
				       VerifiedLongitude  = t.VerifiedLongitude,
				       VerifiedTime       = t.VerifiedTime,
				       Volume             = t.Volume,
				       Weight             = t.Weight
			       };
		}

		public static TripUpdate ToTripUpdate( this Trip t, string program, bool toDispatchBoard, bool toDriversBoard, bool toDriver )
		{
			var Charges = t.TripChargesAsJson.IsNullOrWhiteSpace()
				              ? new List<TripCharge>()
				              : JsonConvert.DeserializeObject<List<TripCharge>>( t.TripChargesAsJson );

			var Packages = t.TripItemsAsJson.IsNullOrWhiteSpace()
				               ? new List<TripPackage>()
				               : JsonConvert.DeserializeObject<List<TripPackage>>( t.TripItemsAsJson );

			return new TripUpdate
			       {
				       TripId                   = t.TripId,
				       BroadcastToDispatchBoard = toDispatchBoard,
				       BroadcastToDriver        = toDriver,
				       BroadcastToDriverBoard   = toDriversBoard,

				       PackageType = t.PackageType,
				       PickupTime  = t.PickupTime,
				       Status1     = (STATUS)t.Status1,
				       Status2     = (STATUS1)t.Status2,
				       Status3     = (STATUS2)t.Status3,

				       ReceivedByDevice = t.ReceivedByDevice,
				       ReadByDriver     = t.ReadByDriver,
				       AccountId        = t.AccountId,
				       Barcode          = t.Barcode,
				       Board            = t.Board,

				       BillingAccountId            = t.BillingCompanyAccountId,
				       BillingAddressAddressLine1  = t.BillingAddressAddressLine1,
				       BillingAddressAddressLine2  = t.BillingAddressAddressLine2,
				       BillingAddressBarcode       = t.BillingAddressBarcode,
				       BillingAddressCity          = t.BillingAddressCity,
				       BillingAddressCountry       = t.BillingAddressCountry,
				       BillingAddressCountryCode   = t.BillingAddressCountryCode,
				       BillingAddressEmailAddress  = t.BillingAddressEmailAddress,
				       BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1,
				       BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2,
				       BillingAddressFax           = t.BillingAddressFax,
				       BillingAddressLatitude      = t.BillingAddressLatitude,
				       BillingAddressLongitude     = t.BillingAddressLongitude,
				       BillingAddressMobile        = t.BillingAddressMobile,
				       BillingAddressMobile1       = t.BillingAddressMobile1,
				       BillingAddressNotes         = t.BillingAddressNotes,
				       BillingAddressPhone         = t.BillingAddressPhone,
				       BillingAddressPhone1        = t.BillingAddressPhone1,
				       BillingAddressPostalBarcode = t.BillingAddressPostalBarcode,
				       BillingAddressPostalCode    = t.BillingAddressPostalCode,
				       BillingAddressRegion        = t.BillingAddressRegion,
				       BillingAddressSuite         = t.BillingAddressSuite,
				       BillingAddressVicinity      = t.BillingAddressVicinity,
				       BillingCompanyName          = t.BillingCompanyName,
				       BillingContact              = t.BillingContact,
				       BillingNotes                = t.BillingNotes,

				       PickupAccountId            = t.PickupCompanyAccountId,
				       PickupAddressAddressLine1  = t.PickupAddressAddressLine1,
				       PickupAddressAddressLine2  = t.PickupAddressAddressLine2,
				       PickupAddressBarcode       = t.PickupAddressBarcode,
				       PickupAddressCity          = t.PickupAddressCity,
				       PickupAddressCountry       = t.PickupAddressCountry,
				       PickupAddressCountryCode   = t.PickupAddressCountryCode,
				       PickupAddressEmailAddress  = t.PickupAddressEmailAddress,
				       PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1,
				       PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2,
				       PickupAddressFax           = t.PickupAddressFax,
				       PickupAddressLatitude      = t.PickupAddressLatitude,
				       PickupAddressLongitude     = t.PickupAddressLongitude,
				       PickupAddressMobile        = t.PickupAddressMobile,
				       PickupAddressMobile1       = t.PickupAddressMobile1,
				       PickupAddressNotes         = t.PickupAddressNotes,
				       PickupAddressPhone         = t.PickupAddressPhone,
				       PickupAddressPhone1        = t.PickupAddressPhone1,
				       PickupAddressPostalBarcode = t.PickupAddressPostalBarcode,
				       PickupAddressPostalCode    = t.PickupAddressPostalCode,
				       PickupAddressRegion        = t.PickupAddressRegion,
				       PickupAddressSuite         = t.PickupAddressSuite,
				       PickupAddressVicinity      = t.PickupAddressVicinity,
				       PickupCompanyName          = t.PickupCompanyName,
				       PickupContact              = t.PickupContact,
				       PickupNotes                = t.PickupNotes,
				       PickupZone                 = t.PickupZone,
				       PickupLongitude            = t.PickupLongitude,
				       PickupLatitude             = t.PickupLatitude,

				       DeliveryAccountId            = t.DeliveryCompanyAccountId,
				       DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1,
				       DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2,
				       DeliveryAddressBarcode       = t.DeliveryAddressBarcode,
				       DeliveryAddressCity          = t.DeliveryAddressCity,
				       DeliveryAddressCountry       = t.DeliveryAddressCountry,
				       DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode,
				       DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress,
				       DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1,
				       DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2,
				       DeliveryAddressFax           = t.DeliveryAddressFax,
				       DeliveryAddressLatitude      = t.DeliveryAddressLatitude,
				       DeliveryAddressLongitude     = t.DeliveryAddressLongitude,
				       DeliveryAddressMobile        = t.DeliveryAddressMobile,
				       DeliveryAddressMobile1       = t.DeliveryAddressMobile1,
				       DeliveryAddressNotes         = t.DeliveryAddressNotes,
				       DeliveryAddressPhone         = t.DeliveryAddressPhone,
				       DeliveryAddressPhone1        = t.DeliveryAddressPhone1,
				       DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode,
				       DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode,
				       DeliveryAddressRegion        = t.DeliveryAddressRegion,
				       DeliveryAddressSuite         = t.DeliveryAddressSuite,
				       DeliveryAddressVicinity      = t.DeliveryAddressVicinity,
				       DeliveryCompanyName          = t.DeliveryCompanyName,
				       DeliveryContact              = t.DeliveryContact,
				       DeliveryNotes                = t.DeliveryNotes,
				       DeliveryLatitude             = t.DeliveredLatitude,
				       DeliveryLongitude            = t.DeliveredLongitude,
				       DeliveryTime                 = t.DeliveredTime,
				       DeliveryZone                 = t.DeliveryZone,

				       CallerEmail        = t.CallerEmail,
				       CallerName         = t.CallerName,
				       CallerPhone        = t.CallerPhone,
				       CallTime           = t.CallTime,
				       ClaimLatitude      = t.ClaimedLatitude,
				       ClaimLongitude     = t.ClaimedLongitude,
				       ClaimTime          = t.ClaimedTime,
				       ContainerId        = t.ContainerId,
				       CurrentZone        = t.CurrentZone,
				       DangerousGoods     = t.DangerousGoods,
				       Driver             = t.DriverCode,
				       DueTime            = t.DueTime,
				       ExtensionAsJson    = t.ExtensionAsJson,
				       HasDocuments       = t.HasDangerousGoodsDocuments,
				       IsQuote            = t.IsQuote,
				       Location           = t.Location,
				       Measurement        = t.Measurement,
				       MissingPieces      = t.MissingPieces,
				       OriginalPieceCount = t.OriginalPieceCount,
				       POD                = t.POD,
				       POP                = t.POP,
				       Pieces             = t.Pieces,
				       Program            = program,
				       ReadyTime          = t.ReadyTime,
				       Reference          = t.Reference,
				       ServiceLevel       = t.ServiceLevel,
				       TripCharges        = Charges,
				       Packages           = Packages,
				       UNClass            = t.UNClass,

				       UndeliverableNotes = t.UndeliverableNotes,
				       VerifiedLongitude  = t.VerifiedLongitude,
				       VerifiedLatitude   = t.VerifiedLatitude,
				       VerifiedTime       = t.VerifiedTime
			       };
		}
	}

	public partial class Trip
	{
		public Trip()
		{
		}


		public Trip( Trip t )
		{
			Location     = t.Location;
			LastModified = t.LastModified;
			TripId       = t.TripId;
			ContainerId  = t.ContainerId;
			Barcode      = t.Barcode;
			AccountId    = t.AccountId;

			Status1 = t.Status1;
			Status2 = t.Status2;
			Status3 = t.Status3;
			Status4 = t.Status4;
			Status5 = t.Status5;
			Status6 = t.Status6;

			Reference                  = t.Reference;
			CallTime                   = t.CallTime;
			CallerPhone                = t.CallerPhone;
			CallerEmail                = t.CallerEmail;
			ReadyTime                  = t.ReadyTime;
			DueTime                    = t.DueTime;
			DriverCode                 = t.DriverCode;
			Weight                     = t.Weight;
			Pieces                     = t.Pieces;
			Volume                     = t.Volume;
			Measurement                = t.Measurement;
			OriginalPieceCount         = t.OriginalPieceCount;
			MissingPieces              = t.MissingPieces;
			ReceivedByDevice           = t.ReceivedByDevice;
			ReadByDriver               = t.ReadByDriver;
			DangerousGoods             = t.DangerousGoods;
			HasItems                   = t.HasItems;
			UNClass                    = t.UNClass;
			HasDangerousGoodsDocuments = t.HasDangerousGoodsDocuments;
			Board                      = t.Board;
			PackageType                = t.PackageType;
			ServiceLevel               = t.ServiceLevel;
			POP                        = t.POP;
			POD                        = t.POD;

			PickupCompanyId            = t.PickupCompanyId;
			PickupCompanyName          = t.PickupCompanyName;
			PickupAddressBarcode       = t.PickupAddressBarcode;
			PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
			PickupAddressSuite         = t.PickupAddressSuite;
			PickupAddressAddressLine1  = t.PickupAddressAddressLine1;
			PickupAddressAddressLine2  = t.PickupAddressAddressLine2;
			PickupAddressVicinity      = t.PickupAddressVicinity;
			PickupAddressCity          = t.PickupAddressCity;
			PickupAddressRegion        = t.PickupAddressRegion;
			PickupAddressPostalCode    = t.PickupAddressPostalCode;
			PickupAddressCountry       = t.PickupAddressCountry;
			PickupAddressCountryCode   = t.PickupAddressCountryCode;
			PickupAddressPhone         = t.PickupAddressPhone;
			PickupAddressPhone1        = t.PickupAddressPhone1;
			PickupAddressMobile        = t.PickupAddressMobile;
			PickupAddressMobile1       = t.PickupAddressMobile1;
			PickupAddressFax           = t.PickupAddressFax;
			PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
			PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
			PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
			PickupContact              = t.PickupContact;
			PickupAddressLatitude      = t.PickupAddressLatitude;
			PickupAddressLongitude     = t.PickupAddressLongitude;
			PickupAddressNotes         = t.PickupAddressNotes;
			PickupNotes                = t.PickupNotes;

			DeliveryCompanyId            = t.DeliveryCompanyId;
			DeliveryCompanyName          = t.DeliveryCompanyName;
			DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
			DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
			DeliveryAddressSuite         = t.DeliveryAddressSuite;
			DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1;
			DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2;
			DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
			DeliveryAddressCity          = t.DeliveryAddressCity;
			DeliveryAddressRegion        = t.DeliveryAddressRegion;
			DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
			DeliveryAddressCountry       = t.DeliveryAddressCountry;
			DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
			DeliveryAddressPhone         = t.DeliveryAddressPhone;
			DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
			DeliveryAddressMobile        = t.DeliveryAddressMobile;
			DeliveryAddressMobile1       = t.DeliveryAddressMobile1;
			DeliveryAddressFax           = t.DeliveryAddressFax;
			DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
			DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
			DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
			DeliveryContact              = t.DeliveryContact;
			DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
			DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
			DeliveryAddressNotes         = t.DeliveryAddressNotes;
			DeliveryNotes                = t.DeliveryNotes;

			BillingCompanyId            = t.BillingCompanyId;
			BillingCompanyName          = t.BillingCompanyName;
			BillingAddressBarcode       = t.BillingAddressBarcode;
			BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
			BillingAddressSuite         = t.BillingAddressSuite;
			BillingAddressAddressLine1  = t.BillingAddressAddressLine1;
			BillingAddressAddressLine2  = t.BillingAddressAddressLine2;
			BillingAddressVicinity      = t.BillingAddressVicinity;
			BillingAddressCity          = t.BillingAddressCity;
			BillingAddressRegion        = t.BillingAddressRegion;
			BillingAddressPostalCode    = t.BillingAddressPostalCode;
			BillingAddressCountry       = t.BillingAddressCountry;
			BillingAddressCountryCode   = t.BillingAddressCountryCode;
			BillingAddressPhone         = t.BillingAddressPhone;
			BillingAddressPhone1        = t.BillingAddressPhone1;
			BillingAddressMobile        = t.BillingAddressMobile;
			BillingAddressMobile1       = t.BillingAddressMobile1;
			BillingAddressFax           = t.BillingAddressFax;
			BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
			BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
			BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
			BillingContact              = t.BillingContact;
			BillingAddressLatitude      = t.BillingAddressLatitude;
			BillingAddressLongitude     = t.BillingAddressLongitude;
			BillingAddressNotes         = t.BillingAddressNotes;
			BillingNotes                = t.BillingNotes;

			CurrentZone        = t.CurrentZone;
			PickupZone         = t.PickupZone;
			DeliveryZone       = t.DeliveryZone;
			PickupTime         = t.PickupTime;
			PickupLatitude     = t.PickupLatitude;
			PickupLongitude    = t.PickupLongitude;
			DeliveredTime      = t.DeliveredTime;
			DeliveredLatitude  = t.DeliveredLatitude;
			DeliveredLongitude = t.DeliveredLongitude;
			VerifiedTime       = t.VerifiedTime;
			VerifiedLatitude   = t.VerifiedLatitude;
			VerifiedLongitude  = t.VerifiedLongitude;
			ClaimedTime        = t.ClaimedTime;
			ClaimedLatitude    = t.ClaimedLatitude;
			ClaimedLongitude   = t.ClaimedLongitude;
			TripChargesAsJson  = t.TripChargesAsJson;
			TripItemsAsJson    = t.TripItemsAsJson;
			CallerName         = t.CallerName;

			PickupCompanyAccountId   = t.PickupCompanyAccountId;
			DeliveryCompanyAccountId = t.DeliveryCompanyAccountId;
			BillingCompanyAccountId  = t.BillingCompanyAccountId;

			ExtensionAsJson    = t.ExtensionAsJson;
			UndeliverableNotes = t.UndeliverableNotes;
		}
	}
}

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		// Must map to Protocol.Data.TripItem for JsonConvert to work
		public class TripItem
		{
			public string  ItemCode     { get; set; } = "";
			public string  ItemCode1    { get; set; } = "";
			public string  Barcode      { get; set; } = "";
			public string  Barcode1     { get; set; } = "";
			public string  Description  { get; set; } = "";
			public string  Reference    { get; set; } = "";
			public decimal Pieces       { get; set; }
			public decimal Height       { get; set; }
			public decimal Width        { get; set; }
			public decimal Length       { get; set; }
			public decimal Volume       { get; set; }
			public decimal Weight       { get; set; }
			public decimal Value        { get; set; }
			public decimal Tax1         { get; set; }
			public decimal Tax2         { get; set; }
			public bool    HasDocuments { get; set; }
			public decimal Original     { get; set; }
			public decimal QH           { get; set; }
		}

		// Must map to Protocol.Data.TripCharge for JsonConvert to work
		public class TripCharge
		{
			public string  ChargeId { get; set; } = "";
			public decimal Quantity { get; set; }
			public decimal Value    { get; set; }
			public string  Text     { get; set; } = "";
		}


		public static string NextTripId( IRequestContext context )
		{
			try
			{
				lock( Rand )
				{
					static char GetChar()
					{
						return (char)( (uint)Rand.Next( 26 ) + 'A' );
					}

					static string GetInt()
					{
						return Rand.Next( 100000 ).ToString( "00000" );
					}

					return $"Z{GetChar()}{GetChar()}{GetInt()}{GetInt()}";
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}

			return "?????????????";
		}

		public bool TripExists( string tripId ) =>
			!( ( from T in Entity.Trips
			     where T.TripId == tripId
			     select T ).FirstOrDefault() is null );

		public TripList GetTripsByStatus( StatusRequest requestObject )
		{
			var Temp = from S in requestObject
			           select (int)S;

			var Result = new TripList();

			try
			{
				var Recs = from T in Entity.Trips
				           where Temp.Contains( T.Status1 )
				           select T;

				if( Recs.Any() )
				{
					foreach( var Trip in Recs )
						Result.Add( Trip.ToTrip() );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public List<Trip> GetTripsForDriver( ref string driverCode )
		{
			if( driverCode.IsNullOrWhiteSpace() )
				driverCode = Context.UserName;

			var Dc = driverCode;

			var Result = new List<Trip>();

			try
			{
				var Stat = new List<int>
				           {
					           (int)STATUS.DISPATCHED,
					           (int)STATUS.PICKED_UP
				           };

				foreach( var Trip in from T in Entity.Trips
				                     where ( T.DriverCode == Dc ) && Stat.Contains( T.Status1 ) && ( T.Status2 == (int)STATUS1.UNSET )
				                     select T )
					Result.Add( Trip.ToTrip() );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public void DeleteTrip( string program, MasterTemplate.Trip t, DateTime lastUpdated )
		{
			try
			{
				if( !( t is null ) )
				{
					var E     = Entity;
					var Trips = E.Trips;

					var SaveFailed = false;

					do
					{
						try
						{
							Trips.Remove( t );
							E.SaveChanges();
						}
						catch( DbUpdateConcurrencyException Exception )
						{
							SaveFailed = true;

							// Get the current entity values and the values in the database
							var Entry          = Exception.Entries.Single();
							var DatabaseValues = Entry.GetDatabaseValues();

							//Record deleted ??
							if( DatabaseValues is null )
								break;

							t = (MasterTemplate.Trip)DatabaseValues.ToObject();
						}
					} while( SaveFailed );

					t.Status1 = (int)STATUS.DELETED;
					Storage.Carrier.Trips.Add( Context, t.ToTrip() );

					var NdxRec = new TripStorageIndex
					             {
						             CallTime            = t.CallTime,
						             TripId              = t.TripId,
						             Reference           = t.Reference,
						             Status1             = (byte)t.Status1,
						             Status2             = (byte)t.Status2,
						             Status3             = (byte)t.Status3,
						             AccountId           = t.AccountId,
						             DeliveryCompanyName = t.DeliveryCompanyName,
						             PickupCompanyName   = t.PickupCompanyName
					             };
					E.TripStorageIndexes.Add( NdxRec );
					E.SaveChanges();
					t.LastModified = lastUpdated;

					LogDeletedTrip( program, t );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}


		public (DateTime LastModified, bool Ok) AddUpdateTrip( TripUpdate t )
		{
			var LastModified = DateTime.UtcNow;

			var Retval = ( LastModified, Ok: true );

			try
			{
				t.TripId = t.TripId is null ? "" : t.TripId.Trim();

				var TripCharges = JsonConvert.SerializeObject( t.TripCharges ?? new List<Protocol.Data.TripCharge>() );
				var Packages    = JsonConvert.SerializeObject( t.Packages ?? new List<TripPackage>() );

				MasterTemplate.Trip? Rec = null;

				bool New;

				if( string.IsNullOrWhiteSpace( t.TripId ) )
					New = true;
				else
				{
					Rec = ( from T in Entity.Trips
					        where T.TripId == t.TripId
					        select T ).FirstOrDefault();

					New = Rec is null && ( t.Status1 != STATUS.DELETED );
				}

				if( New )
				{
					var Trip = new MasterTemplate.Trip
					           {
						           TripId   = string.IsNullOrWhiteSpace( t.TripId ) ? NextTripId( Context ) : t.TripId,
						           Location = t.Location,

						           ReceivedByDevice = t.ReceivedByDevice,
						           ReadByDriver     = t.ReadByDriver,

						           Barcode                    = t.Barcode ?? "",
						           Board                      = t.Board,
						           CallTime                   = t.CallTime,
						           CallerName                 = t.CallerName ?? "",
						           CallerPhone                = t.CallerPhone ?? "",
						           CallerEmail                = t.CallerEmail ?? "",
						           ContainerId                = t.ContainerId ?? "",
						           DangerousGoods             = t.DangerousGoods,
						           DeliveryCompanyId          = -1,
						           DriverCode                 = t.Driver ?? "",
						           DueTime                    = t.DueTime,
						           HasDangerousGoodsDocuments = t.HasDocuments,
						           LastModified               = LastModified,
						           Measurement                = t.Measurement ?? "",
						           MissingPieces              = t.MissingPieces,
						           OriginalPieceCount         = t.OriginalPieceCount,
						           PickupCompanyId            = -1,
						           Pieces                     = t.Pieces,
						           ReadyTime                  = t.ReadyTime,
						           Reference                  = t.Reference ?? "",
						           AccountId                  = t.AccountId ?? "",

						           Status1 = (int)t.Status1,
						           Status2 = (int)t.Status2,
						           Status3 = (int)t.Status3,
						           Status4 = 0,
						           Status5 = 0,
						           Status6 = 0,

						           UNClass      = t.UNClass ?? "",
						           Volume       = t.Volume,
						           Weight       = t.Weight,
						           PackageType  = t.PackageType ?? "",
						           ServiceLevel = t.ServiceLevel ?? "",

						           POP = t.POP ?? "",
						           POD = t.POD ?? "",

						           DeliveryCompanyAccountId     = t.DeliveryAccountId ?? "",
						           DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1 ?? "",
						           DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2 ?? "",
						           DeliveryAddressMobile1       = t.DeliveryAddressMobile ?? "",
						           DeliveryAddressPhone         = t.DeliveryAddressPhone ?? "",
						           DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress ?? "",
						           DeliveryAddressCity          = t.DeliveryAddressCity ?? "",
						           DeliveryAddressBarcode       = t.DeliveryAddressBarcode ?? "",
						           DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode ?? "",
						           DeliveryAddressCountry       = t.DeliveryAddressCountry ?? "",
						           DeliveryAddressLatitude      = t.DeliveryAddressLatitude,
						           DeliveryAddressLongitude     = t.DeliveryAddressLongitude,
						           DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1 ?? "",
						           DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2 ?? "",
						           DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode ?? "",
						           DeliveryAddressMobile        = t.DeliveryAddressMobile ?? "",
						           DeliveryAddressFax           = t.DeliveryAddressFax ?? "",
						           DeliveryAddressPhone1        = t.DeliveryAddressPhone1 ?? "",
						           DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode ?? "",
						           DeliveryAddressRegion        = t.DeliveryAddressRegion ?? "",
						           DeliveryAddressSuite         = t.DeliveryAddressSuite ?? "",
						           DeliveryAddressVicinity      = t.DeliveryAddressVicinity,
						           DeliveryCompanyName          = t.DeliveryCompanyName ?? "",
						           DeliveryContact              = t.DeliveryContact ?? "",
						           DeliveryAddressNotes         = t.DeliveryAddressNotes ?? "",
						           DeliveryNotes                = t.DeliveryNotes ?? "",

						           PickupCompanyAccountId     = t.PickupAccountId ?? "",
						           PickupAddressAddressLine1  = t.PickupAddressAddressLine1 ?? "",
						           PickupAddressAddressLine2  = t.PickupAddressAddressLine2 ?? "",
						           PickupAddressMobile1       = t.PickupAddressMobile ?? "",
						           PickupAddressPhone         = t.PickupAddressPhone ?? "",
						           PickupAddressEmailAddress  = t.PickupAddressEmailAddress ?? "",
						           PickupAddressCity          = t.PickupAddressCity ?? "",
						           PickupAddressBarcode       = t.PickupAddressBarcode ?? "",
						           PickupAddressPostalCode    = t.PickupAddressPostalCode ?? "",
						           PickupAddressCountry       = t.PickupAddressCountry ?? "",
						           PickupAddressLatitude      = t.PickupAddressLatitude,
						           PickupAddressLongitude     = t.PickupAddressLongitude,
						           PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1 ?? "",
						           PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2 ?? "",
						           PickupAddressCountryCode   = t.PickupAddressCountryCode ?? "",
						           PickupAddressMobile        = t.PickupAddressMobile ?? "",
						           PickupAddressFax           = t.PickupAddressFax ?? "",
						           PickupAddressPhone1        = t.PickupAddressPhone1 ?? "",
						           PickupAddressPostalBarcode = t.PickupAddressPostalBarcode ?? "",
						           PickupAddressRegion        = t.PickupAddressRegion ?? "",
						           PickupAddressSuite         = t.PickupAddressSuite ?? "",
						           PickupAddressVicinity      = t.PickupAddressVicinity,
						           PickupCompanyName          = t.PickupCompanyName ?? "",
						           PickupContact              = t.PickupContact ?? "",
						           PickupAddressNotes         = t.PickupAddressNotes ?? "",
						           PickupNotes                = t.PickupNotes ?? "",

						           BillingCompanyAccountId     = t.BillingAccountId ?? "",
						           BillingAddressAddressLine1  = t.BillingAddressAddressLine1 ?? "",
						           BillingAddressAddressLine2  = t.BillingAddressAddressLine2 ?? "",
						           BillingAddressBarcode       = t.BillingAddressBarcode ?? "",
						           BillingAddressCity          = t.BillingAddressCity ?? "",
						           BillingAddressCountry       = t.BillingAddressCountry ?? "",
						           BillingAddressCountryCode   = t.BillingAddressCountryCode ?? "",
						           BillingAddressEmailAddress  = t.BillingAddressEmailAddress ?? "",
						           BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1 ?? "",
						           BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2 ?? "",
						           BillingAddressFax           = t.BillingAddressFax ?? "",
						           BillingAddressLatitude      = t.BillingAddressLatitude,
						           BillingAddressLongitude     = t.BillingAddressLongitude,
						           BillingAddressMobile        = t.BillingAddressMobile ?? "",
						           BillingAddressMobile1       = t.BillingAddressMobile1 ?? "",
						           BillingAddressPhone         = t.BillingAddressPhone ?? "",
						           BillingAddressPhone1        = t.BillingAddressPhone1 ?? "",
						           BillingAddressPostalBarcode = t.BillingAddressPostalBarcode ?? "",
						           BillingAddressPostalCode    = t.BillingAddressPostalCode ?? "",
						           BillingAddressRegion        = t.BillingAddressRegion ?? "",
						           BillingAddressSuite         = t.BillingAddressSuite ?? "",
						           BillingAddressVicinity      = t.BillingAddressVicinity ?? "",
						           BillingCompanyName          = t.BillingCompanyName ?? "",
						           BillingContact              = t.BillingContact ?? "",
						           BillingAddressNotes         = t.BillingAddressNotes ?? "",
						           BillingNotes                = t.BillingNotes ?? "",

						           CurrentZone  = t.CurrentZone ?? "",
						           PickupZone   = t.PickupZone ?? "",
						           DeliveryZone = t.DeliveryZone ?? "",

						           PickupTime      = t.PickupTime,
						           PickupLatitude  = t.PickupLatitude,
						           PickupLongitude = t.PickupLongitude,

						           DeliveredTime      = t.DeliveryTime,
						           DeliveredLatitude  = t.DeliveryLatitude,
						           DeliveredLongitude = t.DeliveryLongitude,

						           VerifiedTime      = t.VerifiedTime,
						           VerifiedLatitude  = t.VerifiedLatitude,
						           VerifiedLongitude = t.VerifiedLongitude,

						           ClaimedTime      = t.ClaimTime,
						           ClaimedLatitude  = t.ClaimLatitude,
						           ClaimedLongitude = t.ClaimLongitude,

						           TripChargesAsJson = TripCharges,
						           TripItemsAsJson   = Packages,

						           IsQuote            = t.IsQuote,
						           UndeliverableNotes = t.UndeliverableNotes ?? "",
						           ExtensionAsJson    = t.ExtensionAsJson
					           };

					Entity.Trips.Add( Trip );
					LogTrip( t.Program, Trip );
					Entity.SaveChanges();
				}
				else if( t.Status1 == STATUS.DELETED )
				{
					var SaveFailed = false;

					do
					{
						try
						{
							Entity.Trips.Remove( Rec ?? throw new NullReferenceException() );
							Entity.SaveChanges();
						}
						catch( DbUpdateConcurrencyException Exception )
						{
							SaveFailed = true;

							// Get the current entity values and the values in the database
							var Entry          = Exception.Entries.Single();
							var DatabaseValues = Entry.GetDatabaseValues();

							//Record deleted ??
							if( DatabaseValues is null )
								break;

							Rec = (MasterTemplate.Trip)DatabaseValues.ToObject();
						}
					} while( SaveFailed );

					if( Rec != null )
					{
						Rec.Status1 = (int)STATUS.DELETED;
						Trips.Add( Context, Rec.ToTrip() );

						var NdxRec = new TripStorageIndex
						             {
							             CallTime            = Rec.CallTime,
							             TripId              = Rec.TripId,
							             Reference           = Rec.Reference ?? "",
							             Status1             = (byte)Rec.Status1,
							             Status2             = (byte)Rec.Status2,
							             Status3             = (byte)Rec.Status3,
							             AccountId           = Rec.AccountId,
							             DeliveryCompanyName = Rec.DeliveryCompanyName ?? "",
							             PickupCompanyName   = Rec.PickupCompanyName ?? ""
						             };
						Entity.TripStorageIndexes.Add( NdxRec );
						Entity.SaveChanges();
						Rec.LastModified = LastModified;
						LogDeletedTrip( t.Program, Rec );
					}
				}
				else if( !( Rec is null ) )
				{
					var SaveFailed = false;
					var Before     = new TripLogger( Rec );

					do
					{
						try
						{
							if( t.Status1 == STATUS.DISPATCHED )
								t.Status2 = STATUS1.UNSET;

							Rec.Location = t.Location;

							Rec.ReceivedByDevice = t.ReceivedByDevice;
							Rec.ReadByDriver     = t.ReadByDriver;

							Rec.Barcode                    = t.Barcode ?? "";
							Rec.Board                      = t.Board ?? "";
							Rec.CallTime                   = t.CallTime;
							Rec.CallerName                 = t.CallerName ?? "";
							Rec.CallerPhone                = t.CallerPhone ?? "";
							Rec.CallerEmail                = t.CallerEmail ?? "";
							Rec.ContainerId                = t.ContainerId ?? "";
							Rec.DangerousGoods             = t.DangerousGoods;
							Rec.DriverCode                 = t.Driver ?? "";
							Rec.DueTime                    = t.DueTime;
							Rec.HasDangerousGoodsDocuments = t.HasDocuments;
							Rec.LastModified               = LastModified;
							Rec.Measurement                = t.Measurement ?? "";
							Rec.MissingPieces              = t.MissingPieces;
							Rec.OriginalPieceCount         = t.OriginalPieceCount;
							Rec.Pieces                     = t.Pieces;
							Rec.ReadyTime                  = t.ReadyTime;
							Rec.Reference                  = t.Reference ?? "";
							Rec.AccountId                  = t.AccountId ?? "";
							Rec.Status1                    = (int)t.Status1;
							Rec.Status2                    = (int)t.Status2;
							Rec.Status3                    = (int)t.Status3;
							Rec.UNClass                    = t.UNClass ?? "";
							Rec.Volume                     = t.Volume;
							Rec.Weight                     = t.Weight;
							Rec.PackageType                = t.PackageType ?? "";
							Rec.ServiceLevel               = t.ServiceLevel ?? "";

							Rec.POP = t.POP ?? "";
							Rec.POD = t.POD ?? "";

							Rec.DeliveryCompanyAccountId     = t.DeliveryAccountId ?? "";
							Rec.DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1 ?? "";
							Rec.DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2 ?? "";
							Rec.DeliveryAddressMobile1       = t.DeliveryAddressMobile ?? "";
							Rec.DeliveryAddressPhone         = t.DeliveryAddressPhone ?? "";
							Rec.DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress ?? "";
							Rec.DeliveryAddressCity          = t.DeliveryAddressCity ?? "";
							Rec.DeliveryAddressBarcode       = t.DeliveryAddressBarcode ?? "";
							Rec.DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode ?? "";
							Rec.DeliveryAddressCountry       = t.DeliveryAddressCountry ?? "";
							Rec.DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
							Rec.DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
							Rec.DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1 ?? "";
							Rec.DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2 ?? "";
							Rec.DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode ?? "";
							Rec.DeliveryAddressMobile        = t.DeliveryAddressMobile ?? "";
							Rec.DeliveryAddressFax           = t.DeliveryAddressFax ?? "";
							Rec.DeliveryAddressPhone1        = t.DeliveryAddressPhone1 ?? "";
							Rec.DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode ?? "";
							Rec.DeliveryAddressRegion        = t.DeliveryAddressRegion ?? "";
							Rec.DeliveryAddressSuite         = t.DeliveryAddressSuite ?? "";
							Rec.DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
							Rec.DeliveryCompanyName          = t.DeliveryCompanyName ?? "";
							Rec.DeliveryContact              = t.DeliveryContact ?? "";
							Rec.DeliveryAddressNotes         = t.DeliveryAddressNotes ?? "";
							Rec.DeliveryNotes                = t.DeliveryNotes ?? "";

							Rec.PickupCompanyAccountId     = t.PickupAccountId ?? "";
							Rec.PickupAddressAddressLine1  = t.PickupAddressAddressLine1 ?? "";
							Rec.PickupAddressAddressLine2  = t.PickupAddressAddressLine2 ?? "";
							Rec.PickupAddressMobile1       = t.PickupAddressMobile ?? "";
							Rec.PickupAddressPhone         = t.PickupAddressPhone ?? "";
							Rec.PickupAddressEmailAddress  = t.PickupAddressEmailAddress ?? "";
							Rec.PickupAddressCity          = t.PickupAddressCity ?? "";
							Rec.PickupAddressBarcode       = t.PickupAddressBarcode ?? "";
							Rec.PickupAddressPostalCode    = t.PickupAddressPostalCode ?? "";
							Rec.PickupAddressCountry       = t.PickupAddressCountry ?? "";
							Rec.PickupAddressLatitude      = t.PickupAddressLatitude;
							Rec.PickupAddressLongitude     = t.PickupAddressLongitude;
							Rec.PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1 ?? "";
							Rec.PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2 ?? "";
							Rec.PickupAddressCountryCode   = t.PickupAddressCountryCode ?? "";
							Rec.PickupAddressMobile        = t.PickupAddressMobile ?? "";
							Rec.PickupAddressFax           = t.PickupAddressFax ?? "";
							Rec.PickupAddressPhone1        = t.PickupAddressPhone1 ?? "";
							Rec.PickupAddressPostalBarcode = t.PickupAddressPostalBarcode ?? "";
							Rec.PickupAddressRegion        = t.PickupAddressRegion ?? "";
							Rec.PickupAddressSuite         = t.PickupAddressSuite ?? "";
							Rec.PickupAddressVicinity      = t.PickupAddressVicinity;
							Rec.PickupCompanyName          = t.PickupCompanyName ?? "";
							Rec.PickupContact              = t.PickupContact ?? "";
							Rec.PickupAddressNotes         = t.PickupAddressNotes ?? "";
							Rec.PickupNotes                = t.PickupNotes ?? "";

							Rec.BillingCompanyAccountId     = t.BillingAccountId;
							Rec.BillingAddressAddressLine1  = t.BillingAddressAddressLine1 ?? "";
							Rec.BillingAddressAddressLine2  = t.BillingAddressAddressLine2 ?? "";
							Rec.BillingAddressBarcode       = t.BillingAddressBarcode ?? "";
							Rec.BillingAddressCity          = t.BillingAddressCity ?? "";
							Rec.BillingAddressCountry       = t.BillingAddressCountry ?? "";
							Rec.BillingAddressCountryCode   = t.BillingAddressCountryCode ?? "";
							Rec.BillingAddressEmailAddress  = t.BillingAddressEmailAddress ?? "";
							Rec.BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1 ?? "";
							Rec.BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2 ?? "";
							Rec.BillingAddressFax           = t.BillingAddressFax ?? "";
							Rec.BillingAddressLatitude      = t.BillingAddressLatitude;
							Rec.BillingAddressLongitude     = t.BillingAddressLongitude;
							Rec.BillingAddressMobile        = t.BillingAddressMobile ?? "";
							Rec.BillingAddressMobile1       = t.BillingAddressMobile1 ?? "";
							Rec.BillingAddressPhone         = t.BillingAddressPhone ?? "";
							Rec.BillingAddressPhone1        = t.BillingAddressPhone1 ?? "";
							Rec.BillingAddressPostalBarcode = t.BillingAddressPostalBarcode ?? "";
							Rec.BillingAddressPostalCode    = t.BillingAddressPostalCode ?? "";
							Rec.BillingAddressRegion        = t.BillingAddressRegion ?? "";
							Rec.BillingAddressSuite         = t.BillingAddressSuite ?? "";
							Rec.BillingAddressVicinity      = t.BillingAddressVicinity ?? "";
							Rec.BillingCompanyName          = t.BillingCompanyName ?? "";
							Rec.BillingContact              = t.BillingContact ?? "";
							Rec.BillingAddressNotes         = t.BillingAddressNotes ?? "";
							Rec.BillingNotes                = t.BillingNotes ?? "";

							Rec.CurrentZone  = t.CurrentZone ?? "";
							Rec.PickupZone   = t.PickupZone ?? "";
							Rec.DeliveryZone = t.DeliveryZone ?? "";

							Rec.PickupTime      = t.PickupTime;
							Rec.PickupLatitude  = t.PickupLatitude;
							Rec.PickupLongitude = t.PickupLongitude;

							Rec.DeliveredTime      = t.DeliveryTime;
							Rec.DeliveredLatitude  = t.DeliveryLatitude;
							Rec.DeliveredLongitude = t.DeliveryLongitude;

							Rec.VerifiedTime      = t.VerifiedTime;
							Rec.VerifiedLatitude  = t.VerifiedLatitude;
							Rec.VerifiedLongitude = t.VerifiedLongitude;

							Rec.ClaimedTime      = t.ClaimTime;
							Rec.ClaimedLatitude  = t.ClaimLatitude;
							Rec.ClaimedLongitude = t.ClaimLongitude;

							Rec.TripChargesAsJson = TripCharges;
							Rec.TripItemsAsJson   = Packages;

							Rec.IsQuote            = t.IsQuote;
							Rec.UndeliverableNotes = t.UndeliverableNotes ?? "";

							Rec.ExtensionAsJson = t.ExtensionAsJson;

							Entity.SaveChanges();
						}
						catch( DbUpdateConcurrencyException Exception )
						{
							SaveFailed = true;

							// Get the current entity values and the values in the database
							var Entry          = Exception.Entries.Single();
							var DatabaseValues = Entry.GetDatabaseValues();

							//Record deleted ??
							if( DatabaseValues is null )
								break;

							Rec = (MasterTemplate.Trip)DatabaseValues.ToObject();
						}
					} while( SaveFailed );

					var After = new TripLogger( Rec );

					LogTrip( t.Program, Before, After );
				}
				else
					throw new Exception( "Unexpected null" );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
				Retval.Ok = false;
			}

			return Retval;
		}

		// Returns the board and device status
		public ( string Board, bool ReceivedByDevice, bool ReadByDriver, STATUS Status, STATUS1 Status1) TripReceivedOrRead( string program, string tripId, bool isReceived )
		{
			var E      = Entity;
			var RetVal = ( Board: "", ReceivedByDevice: false, ReadByDriver: false, Status: STATUS.UNSET, Status1: STATUS1.UNSET );

			try
			{
				var Rec = ( from T in E.Trips
				            where T.TripId == tripId
				            select T ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					var Before = new TripLogger( Rec );

					Rec.LastModified = DateTime.UtcNow;

					RetVal.Board  = Rec.Board;
					RetVal.Status = (STATUS)Rec.Status1;

					if( isReceived )
					{
						RetVal.ReadByDriver     = Rec.ReadByDriver;
						RetVal.ReceivedByDevice = true;
						Rec.ReceivedByDevice    = true;
					}
					else
					{
						RetVal.ReceivedByDevice = Rec.ReceivedByDevice;
						RetVal.ReadByDriver     = true;
						Rec.ReadByDriver        = true;
					}

					bool SaveFailed;

					do
					{
						SaveFailed = false;

						try
						{
							E.SaveChanges();
						}
						catch( DbUpdateConcurrencyException Exception )
						{
							SaveFailed = true;

							// Get the current entity values and the values in the database
							var Entry          = Exception.Entries.Single();
							var DatabaseValues = Entry.GetDatabaseValues();

							//Record deleted ??
							if( DatabaseValues is null )
								break;

							Rec = (MasterTemplate.Trip)DatabaseValues.ToObject();
						}
					} while( SaveFailed );

					var After = new TripLogger( Rec );

					LogTrip( program, Before, After );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return RetVal;
		}

		public (string Board, bool ReceivedByDevice, bool ReadByDriver, STATUS Status, STATUS1 Status1) TripReceivedByDevice( string program, string tripId ) => TripReceivedOrRead( program, tripId, true );

		public (string Board, bool ReceivedByDevice, bool ReadByDriver, STATUS Status, STATUS1 Status1 ) TripReadByDriver( string program, string tripId ) => TripReceivedOrRead( program, tripId, false );

		public void AddUpdateTrip( TripUpdateList trips )
		{
			foreach( var Trip1 in trips.Trips )
			{
				Trip1.Program = trips.Program;
				AddUpdateTrip( Trip1 );
			}
		}


		public (string Board, bool ReceivedByDevice, bool ReadByDriver, STATUS Status, STATUS1 Status1) UpdateTripFromDevice( DeviceTripUpdate u )
		{
			var RetVal = ( Board: "", ReceivedByDevice: false, ReadByDriver: false, Status: STATUS.UNSET, Status1: STATUS1.UNSET );

			try

			{
				var TripId = u.TripId;

				var E = Entity;

				var Rec = ( from T in E.Trips
				            where T.TripId == TripId
				            select T ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					var Before = new TripLogger( Rec );

					Rec.LastModified = DateTime.UtcNow;

					RetVal.Board            = Rec.Board;
					RetVal.Status           = u.Status;
					RetVal.Status1          = u.Status1;
					RetVal.ReadByDriver     = Rec.ReadByDriver;
					RetVal.ReceivedByDevice = Rec.ReceivedByDevice;

					Rec.Status1 = (int)u.Status;
					Rec.Status2 = (int)u.Status1;

					if( u.Status1 == STATUS1.UNDELIVERED )
					{
						var Dp = DevicePreferences();

						if( u.Status < STATUS.PICKED_UP ) // Not Picked Up
						{
							if( Dp.DeleteCannotPickup )
							{
								DeleteTrip( u.Program, Rec, Rec.LastModified );

								RetVal.Status = STATUS.DELETED;

								return RetVal;
							}
						}

						if( Dp.UndeliverableBackToDispatch )
						{
							Rec.Status1   = (int)STATUS.ACTIVE;
							Rec.Status3 |= (int)STATUS2.WAS_UNDELIVERABLE;

							if( u.Status >= STATUS.PICKED_UP )
								Rec.Status3 |= (int)STATUS2.WAS_PICKED_UP;

							RetVal.Status = STATUS.ACTIVE;
						}
					}

					switch( u.Status )
					{
					case STATUS.PICKED_UP:
						Rec.POP             = u.PopPod;
						Rec.PickupTime      = u.UpdateTime;
						Rec.PickupLatitude  = u.UpdateLatitude;
						Rec.PickupLongitude = u.UpdateLongitude;
						goto case STATUS.DISPATCHED;

					case STATUS.DISPATCHED:
						if( u.Status1 == STATUS1.CLAIMED )
						{
							Rec.ClaimedTime     = u.UpdateTime;
							Rec.ClaimedLatitude = u.UpdateLatitude;
							Rec.ClaimedLatitude = u.UpdateLongitude;
						}

						break;

					case STATUS.DELIVERED:
						Rec.POD                = u.PopPod;
						Rec.DeliveredTime      = u.UpdateTime;
						Rec.DeliveredLatitude  = u.UpdateLatitude;
						Rec.DeliveredLongitude = u.UpdateLongitude;

						break;

					case STATUS.VERIFIED:
						Rec.POD               = u.PopPod;
						Rec.VerifiedTime      = u.UpdateTime;
						Rec.VerifiedLatitude  = u.UpdateLatitude;
						Rec.VerifiedLongitude = u.UpdateLongitude;

						break;
					}

					Rec.Pieces               = u.Pieces;
					Rec.Weight               = u.Weight;
					Rec.PickupAddressNotes   = u.PickupNotes;
					Rec.DeliveryAddressNotes = u.DeliveryNotes;
					Rec.BillingAddressNotes  = u.BillingNotes;
					Rec.UndeliverableNotes   = u.UndeliverableNotes;

					Rec.Reference = u.Reference;
					E.SaveChanges();

					new Signatures( Context ).AddUpdate( TripId, u.Signature );

					var After = new TripLogger( Rec );

					LogTrip( u.Program, Before, After );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return RetVal;
		}

		public Trip GeTrip( GetTrip requestObject )
		{
			try
			{
				var Rec = ( from T in Entity.Trips
				            where T.TripId == requestObject.TripId
				            select T ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					var T = Rec.ToTrip();

					if( requestObject.Signatures )
						T.Signatures = new Signatures( Context ).GetSignatures( requestObject.TripId );

					return T;
				}
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return new Trip();
		}
	}
}