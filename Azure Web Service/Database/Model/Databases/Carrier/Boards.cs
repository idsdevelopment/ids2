﻿#nullable enable

using System.Collections.Generic;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Protocol.Data;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		private static void BroadcastMessage<T>( IRequestContext context, string board, string queue, T data )
		{
			using var Db = new CarrierDb( context );

			Db.SendToQueue( board, queue, new List<DataAndIdType>
			                              {
				                              new DataAndIdType
				                              {
					                              Data = JsonConvert.SerializeObject( data ),
					                              Type = typeof( T ).FullName
				                              }
			                              } );
		}

		public static void DriversBoard_BroadcastMessage( IRequestContext context, string board, TripUpdateMessage message )
		{
			BroadcastMessage( context, Messaging.DRIVERS_BOARD, board, message );
		}

		public static void DriversBoard_BroadcastStatusMessage( IRequestContext context, string board, TripUpdateMessage status )
		{
			BroadcastMessage( context, Messaging.DRIVERS_BOARD, board, status );
		}

		public static void DispatchBoard_BroadcastMessage( IRequestContext context, string board, TripUpdateMessage message )
		{
			BroadcastMessage( context, Messaging.DISPATCH_BOARD, board, message );
		}


		public static void Driver_BroadcastMessage( IRequestContext context, string driver, TripUpdateMessage message )
		{
			BroadcastMessage( context, Messaging.DRIVERS, driver, message );
		}

		public static void Driver_BroadcastMessage( IRequestContext context, string driver, List<TripUpdateMessage> messages )
		{
			foreach( var Message in messages )
				BroadcastMessage( context, Messaging.DRIVERS, driver, Message );
		}
	}
}