﻿using System;
using System.Linq;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using CompanyAddressGroup = Protocol.Data.CompanyAddressGroup;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public void AddUpdateCompanyAddressGroup( CompanyAddressGroup requestObject )
		{
			try
			{
				var Num = requestObject.GroupNumber;
				var E = Entity;
				var Groups = E.CompanyAddressGroupNames;

				var Rec = ( from G in Groups
				            where G.GroupNumber == Num
				            select G ).FirstOrDefault();

				if( Rec is null )
				{
					Rec = new CompanyAddressGroupName
					      {
						      GroupNumber = Num,
						      Description = requestObject.Description.Trim()
					      };
					Groups.Add( Rec );
				}
				else
					Rec.Description = requestObject.Description.Trim();

				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void AddCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
		{
			try
			{
				var Co = requestObject.CompanyName.Trim().ToUpper();

				var E = Entity;

				var CoRec = ( from C in E.Companies
				              where C.CompanyName == Co
				              select C ).FirstOrDefault();

				if( !( CoRec is null ) )
				{
					var Id = CoRec.CompanyId;
					var Num = requestObject.GroupNumber;
					var Cag = E.CompanyAddressGroups;

					var Rec = ( from Ca in Cag
					            where ( Ca.GroupNumber == Num ) && ( Ca.CompanyId == Id )
					            select Ca ).FirstOrDefault();

					if( Rec is null )
					{
						Rec = new MasterTemplate.CompanyAddressGroup
						      {
							      GroupNumber = Num,
							      CompanyId = Id
						      };
						Cag.Add( Rec );
						E.SaveChanges();
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void DeleteCompanyAddressGroup( int requestObject )
		{
			try
			{
				var E = Entity;
				var Groups = E.CompanyAddressGroupNames;

				var Rec = ( from G in Groups
				            where G.GroupNumber == requestObject
				            select G ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					Groups.Remove( Rec );
					E.SaveChanges();
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void DeleteCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
		{
			try
			{
				var Co = requestObject.CompanyName.Trim().ToUpper();

				var E = Entity;

				var CoRec = ( from C in E.Companies
				              where C.CompanyName == Co
				              select C ).FirstOrDefault();

				if( !( CoRec is null ) )
				{
					var Id = CoRec.CompanyId;
					var Num = requestObject.GroupNumber;
					var Cag = E.CompanyAddressGroups;

					var Rec = ( from Ca in Cag
					            where ( Ca.GroupNumber == Num ) && ( Ca.CompanyId == Id )
					            select Ca ).FirstOrDefault();

					//if( Rec is null ) // TODO should be !(Rec is null)
					if( !( Rec is null ) )
					{
						Cag.Remove( Rec );
						E.SaveChanges();
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public CompanyAddressGroups GetCompanyAddressGroups()
		{
			var Result = new CompanyAddressGroups();

			try
			{
				foreach( var G in ( from G in Entity.CompanyAddressGroupNames
				                    orderby G.GroupNumber
				                    select G ).ToList() )
				{
					Result.Add( new CompanyAddressGroup
					            {
						            Description = G.Description.Trim(),
						            GroupNumber = G.GroupNumber
					            } );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public CompaniesWithinAddressGroups GetCompaniesWithinAddressGroup( int companyNumber )
		{
			var Result = new CompaniesWithinAddressGroups();

			try
			{
				var E = Entity;

				var CoNames = ( from G in E.CompanyAddressGroupNames
				                orderby G.GroupNumber
				                where ( companyNumber < 0 ) || ( G.GroupNumber == companyNumber )
				                select G ).ToList();

				var Recs = ( from G in CoNames
				             let GroupNumber = G.GroupNumber
				             select new
				                    {
					                    GroupNumber,
					                    G.Description,
					                    CompanyNames = from Ga in E.CompanyAddressGroups
					                                   where Ga.GroupNumber == GroupNumber
					                                   select ( from C in E.Companies
					                                            where C.CompanyId == Ga.CompanyId
					                                            select C.CompanyName ).FirstOrDefault() ?? ""
				                    } ).ToList();

				foreach( var Rec in Recs )
				{
					Result.Add( new CompaniesWithinAddressGroup
					            {
						            CompanyNumber = Rec.GroupNumber,
						            Companies = ( from C in Rec.CompanyNames
						                          select C ).ToList()
					            } );
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}
}