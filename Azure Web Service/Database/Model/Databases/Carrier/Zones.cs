﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public const string ZONE_LOG_STORAGE = "Zone";

		public Zones GetZones()
		{
			var Result = new Zones();

			try
			{
				Result.AddRange( from Z in Entity.Zones
				                 select new Zone { Abbreviation = Z.Abbreviation, Name = Z.ZoneName, SortIndex = Z.SortIndex } );
			}
			catch( Exception E )
			{
				Context.SystemLogException( nameof( GetZones ), E );
			}

			return Result;
		}


		public void AddZones( List<Zone> zones, string programName )
		{
			try
			{
				var Zones = Entity.Zones;

				foreach( var Zone in zones )
				{
					var Zne = ( from Z in Zones
					            where Z.ZoneName == Zone.Name
					            select Z ).FirstOrDefault();

					if( Zne == null )
					{
						var Z = new MasterTemplate.Zone
						        {
							        ZoneName = Zone.Name,
							        Abbreviation = Zone.Abbreviation,
							        SortIndex = Zone.SortIndex
						        };

						Zones.Add( Z );

						Context.LogDifferences( ZONE_LOG_STORAGE, programName, nameof( AddZones ), Z );
					}
				}

				Entity.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( AddZones ), Exception );
			}
		}


		public void AddUpdateZones( List<Zone> zones, string programName )
		{
			try
			{
				var Zones = Entity.Zones;
                // Populate with all Zones in db
                var ToBeDeleted = (from Z in Zones
                                          select Z).ToList();

                foreach ( var Zone in zones )
				{
                    foreach (var z in ToBeDeleted)
                    {
                        if (z.ZoneName == Zone.Name)
                        {
                            // Take out - leaving Zones to be deleted
                            ToBeDeleted.Remove(z);
                            break;
                        }
                    }

					var Zne = ( from Z in Zones
					            where Z.ZoneName == Zone.Name
					            select Z ).FirstOrDefault();

					if( Zne == null )
					{
						var Z = new MasterTemplate.Zone
						        {
							        ZoneName = Zone.Name,
							        Abbreviation = Zone.Abbreviation,
							        SortIndex = Zone.SortIndex
						        };

						Zones.Add( Z );

						Context.LogDifferences( ZONE_LOG_STORAGE, programName, nameof( AddUpdateZones ), Z );
					}
					else
					{
						Zne.SortIndex = Zone.SortIndex;

						Context.LogDifferences( ZONE_LOG_STORAGE, programName,
						                        nameof( AddUpdateZones ),
						                        new Zone { Name = Zne.ZoneName, SortIndex = Zne.SortIndex, Abbreviation = Zne.Abbreviation },
						                        Zone );
					}
				}

                if (ToBeDeleted.Count > 0)
                {
                    // There are deletions
                    foreach (var z in ToBeDeleted)
                    {
                        Context.LogDeleted(ZONE_LOG_STORAGE, programName, nameof(AddUpdateZones), z);
                    }

                    Zones.RemoveRange(ToBeDeleted);
                }

				Entity.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}