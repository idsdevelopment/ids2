﻿using System;
using System.Collections.Generic;
using System.Linq;
using Protocol.Data;
using Utils;
using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Database.Model.Databases.MasterTemplate.Company;
using CompanyAddress = Protocol.Data.CompanyAddress;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public class CompanyLogger
		{
			public AddressLogger Address { get; set; }

			public string CompanyName { get; set; }
			public string LocationBarcode { get; set; }
			public string DisplayCompanyName { get; set; }
			public string CompanyNumber { get; set; }
			public string UserName { get; set; }
			public string Password { get; set; }

			public CompanyLogger( Company company, AddressLogger address )
			{
				CompanyName        = company.CompanyName;
				DisplayCompanyName = company.DisplayCompanyName;
				CompanyNumber      = company.CompanyNumber;
				Password           = company.Password;
				UserName           = company.UserName;
				Address            = address;
			}
		}

		private static Protocol.Data.Company ConvertCompany( Company co, Address addr )
		{
			var RetVal = new Protocol.Data.Company
			             {
				             CompanyName     = co.CompanyName,
				             CompanyNumber   = co.CompanyNumber,
				             LocationBarcode = co.LocationBarcode,

				             Password = co.Password,
				             UserName = co.UserName,

				             Country       = addr.Country,
				             CountryCode   = addr.CountryCode,
				             Suite         = addr.Suite,
				             AddressLine1  = addr.AddressLine1,
				             AddressLine2  = addr.AddressLine2,
				             Barcode       = addr.Barcode,
				             City          = addr.City,
				             EmailAddress  = addr.EmailAddress,
				             EmailAddress1 = addr.EmailAddress1,
				             EmailAddress2 = addr.EmailAddress2,
				             Fax           = addr.Fax,
				             Latitude      = addr.Latitude,
				             Longitude     = addr.Longitude,
				             Mobile        = addr.Mobile,
				             Mobile1       = addr.Mobile1,
				             Notes         = addr.Notes,
				             Phone         = addr.Phone,
				             Phone1        = addr.Phone1,
				             PostalBarcode = addr.PostalBarcode,
				             PostalCode    = addr.PostalCode,
				             Region        = addr.Region,
				             Vicinity      = addr.Vicinity
			             };

			return RetVal;
		}

		private static CompanyAddress ConvertCompanyAddress( Company co, Address addr )
		{
			var RetVal = new CompanyAddress
			             {
				             CompanyName     = co.CompanyName ?? "",
				             CompanyNumber   = co.CompanyNumber ?? "",
				             LocationBarcode = co.LocationBarcode ?? "",

				             Country       = addr.Country ?? "",
				             CountryCode   = addr.CountryCode ?? "",
				             Suite         = addr.Suite ?? "",
				             AddressLine1  = addr.AddressLine1 ?? "",
				             AddressLine2  = addr.AddressLine2 ?? "",
				             Barcode       = addr.Barcode ?? "",
				             City          = addr.City ?? "",
				             EmailAddress  = addr.EmailAddress ?? "",
				             EmailAddress1 = addr.EmailAddress1 ?? "",
				             EmailAddress2 = addr.EmailAddress2 ?? "",
				             Fax           = addr.Fax ?? "",
				             Latitude      = addr.Latitude,
				             Longitude     = addr.Longitude,
				             Mobile        = addr.Mobile ?? "",
				             Mobile1       = addr.Mobile1 ?? "",
				             Notes         = addr.Notes ?? "",
				             Phone         = addr.Phone ?? "",
				             Phone1        = addr.Phone1 ?? "",
				             PostalBarcode = addr.PostalBarcode ?? "",
				             PostalCode    = addr.PostalCode ?? "",
				             Region        = addr.Region ?? "",
				             Vicinity      = addr.Vicinity ?? ""
			             };

			return RetVal;
		}


		public (CompanyLogger Before, CompanyLogger After, bool Ok) UpdateCompany( long companyId, Protocol.Data.Company co, string programName )
		{
			CompanyLogger Before,
			              After;
			var Ok = false;

			try
			{
				var (PValue, POk ) = Encryption.FromTimeLimitedToken( co.Password );

				var E = Entity;

				var Rec = ( from C in E.Companies
				            where C.CompanyId == companyId
				            select C ).FirstOrDefault();

				if( !( Rec is null ) )
				{
					var (ABefore, AAfter, _, _) = UpdateAddress( Rec.PrimaryAddressId, co, programName );

					Before = new CompanyLogger( Rec, ABefore );

					Rec.LastModified = DateTime.UtcNow;

					Rec.DisplayCompanyName = co.CompanyName;

					//Rec.CompanyName = co.CompanyName.ToUpper();
					Rec.CompanyName     = co.CompanyName; // cjt 20190808 
					Rec.CompanyNumber   = co.CompanyNumber;
					Rec.LocationBarcode = co.LocationBarcode;

					if( POk )
						Rec.Password = PValue;
					Rec.UserName = co.UserName;

					E.SaveChanges();

					After = new CompanyLogger( Rec, AAfter );
					Ok    = true;
				}
				else
					Before = After = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
				Before = After = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );
			}

			return ( Before, After, Ok );
		}

		public ( Company Company, CompanyLogger Log, bool Ok ) AddCompany( Protocol.Data.Company company, string programName )
		{
			( Company Company, CompanyLogger Log, bool Ok ) Retval = ( Company: null, Log: null, Ok: false );
			var (Value, POk) = Encryption.FromTimeLimitedToken( company.Password );

			var E = Entity;

			try
			{
				var (AddressId, Address, Ok) = AddAddress( company, programName );

				if( Ok )
				{
					var Company = new Company
					              {
						              CompanyName     = company.CompanyName ?? "".ToUpper(),
						              LocationBarcode = company.LocationBarcode ?? "",

						              DisplayCompanyName = company.CompanyName ?? "",
						              CompanyNumber      = company.CompanyNumber ?? "",
						              UserName           = company.UserName ?? "",
						              Password           = POk ? Value : "",
						              PrimaryAddressId   = AddressId,
						              LastModified       = DateTime.Now
					              };

					E.Companies.Add( Company );
					E.SaveChanges();

					E.CompanyAddresses.Add( new MasterTemplate.CompanyAddress
					                        {
						                        CompanyId = Company.CompanyId,
						                        AddressId = AddressId
					                        } );
					E.SaveChanges();
					Retval.Company = Company;
					Retval.Log     = new CompanyLogger( Company, Address );
					Retval.Ok      = true;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( AddCompany ), Exception );
			}

			return Retval;
		}

		public (Company Company, bool Ok) ImportCompany( Protocol.Data.Company company, string programName )
		{
			(Company Company, bool Ok) Retval = ( Company: null, Ok: false );
			var                        E      = Entity;

			try
			{
				var (AddressId, Address, Ok) = AddAddress( company, programName );

				if( Ok )
				{
					var Company = new Company
					              {
						              CompanyName        = company.CompanyName ?? "".ToUpper(),
						              LocationBarcode    = company.LocationBarcode ?? "",
						              DisplayCompanyName = company.CompanyName ?? "",
						              CompanyNumber      = company.CompanyNumber ?? "",
						              UserName           = company.UserName ?? "",
						              Password           = company.Password ?? "",
						              PrimaryAddressId   = AddressId,
						              LastModified       = DateTime.Now
					              };

					E.Companies.Add( Company );
					E.SaveChanges();

					E.CompanyAddresses.Add( new MasterTemplate.CompanyAddress
					                        {
						                        CompanyId = Company.CompanyId,
						                        AddressId = AddressId
					                        } );
					E.SaveChanges();
					Context.LogDifferences( "CompanyImport", programName, "Import company", new CompanyLogger( Company, Address ) );
					Retval.Company = Company;
					Retval.Ok      = true;
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( ImportCompany ), Exception );
			}

			return Retval;
		}

		public (Company Company, bool Ok ) GetCompany( long companyId )
		{
			var Comp = ( from Co in Entity.Companies
			             where Co.CompanyId == companyId
			             select Co ).FirstOrDefault();

			return ( Company: Comp ?? new Company(), Ok: Comp != null );
		}

		public (Address Address, bool Ok ) GetCompanyAddress( long companyId )
		{
			var E = Entity;

			var Addr = ( from Ca in E.CompanyAddresses
			             where Ca.CompanyId == companyId
			             from A in E.Addresses
			             where A.Id == Ca.AddressId
			             select A ).FirstOrDefault();

			return ( Address: Addr ?? new Address(), Ok: Addr != null );
		}

		public (Address Address, bool Ok) GetCompanyPrimaryAddress( Company company )
		{
			return GetAddress( company.PrimaryAddressId );
		}

		public ( Company Company, bool Ok) GetCompanyByName( string companyName )
		{
			companyName = companyName.ToUpper();

			var Comp = ( from C in Entity.Companies
			             where C.CompanyName == companyName
			             select C ).FirstOrDefault();

			return ( Company: Comp ?? new Company(), Ok: Comp != null );
		}

		public ( Company Company, bool Ok ) GetCompanyByLocation( string locationCode )
		{
			locationCode = locationCode.TrimStart();

			var Co = ( from C in Entity.Companies
			           where C.LocationBarcode == locationCode
			           select C ).FirstOrDefault();

			return ( Co, !( Co is null ) );
		}

		public List<Company> GetCompaniesByName( string companyName )
		{
			companyName = companyName.ToUpper();

			return ( from C in Entity.Companies
			         where C.CompanyName == companyName
			         select C ).ToList();
		}

		public List<Address> GetCompanyAddresses( long companyId )
		{
			var E = Entity;

			return ( from Ca in E.CompanyAddresses
			         where Ca.CompanyId == companyId
			         from A in E.Addresses
			         where A.Id == Ca.AddressId
			         select A ).ToList();
		}

		public List<Address> GetCompanyAddresses( long companyId, int take )
		{
			var E = Entity;

			return ( from Ca in E.CompanyAddresses
			         where Ca.CompanyId == companyId
			         from A in E.Addresses
			         where A.Id == Ca.AddressId
			         select A ).Take( take ).ToList();
		}


		public List<Company> GetCompaniesByCompanyAndAddressLine1( string companyName, string suite, string addressLine1 )
		{
			var RetVal = new List<Company>();

			try
			{
				var Addresses = CheckSuiteAndAddressLine1( suite, addressLine1 );

				if( Addresses.Count > 0 )
				{
					var E = Entity;

					companyName = companyName.ToUpper();

					var Companies = from C in E.Companies
					                where C.CompanyName == companyName
					                select C;

					if( Companies.Any() )
					{
						foreach( var Company in Companies )
						{
							foreach( var Address in Addresses )
							{
								var Rec = ( from CompanyAddress in E.CompanyAddresses
								            where ( CompanyAddress.AddressId == Address.Id ) && ( CompanyAddress.CompanyId == Company.CompanyId )
								            select CompanyAddress ).FirstOrDefault();

								if( Rec != null )
									RetVal.Add( Company );
							}
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( GetCompaniesByCompanyAndAddressLine1 ), Exception );
			}

			return RetVal;
		}


		public (Company Company, Address Address, bool Ok) GetCompanyBySecondaryId( string id )
		{
			Company Company = null;
			Address Address = null;

			var Addr = GetAddressBySecondaryId( id );

			if( Addr.Ok )
			{
				Address = Addr.Address;

				var CompanyAddress = ( from Ca in Entity.CompanyAddresses
				                       where Ca.AddressId == Addr.Address.Id
				                       select Ca ).FirstOrDefault();

				if( CompanyAddress != null )
				{
					Company = ( from C in Entity.Companies
					            where C.CompanyId == CompanyAddress.CompanyId
					            select C ).FirstOrDefault();
				}
			}

			return ( Company, Address, Ok: Company != null );
		}

		public CompanySummaryList GetCustomerCompaniesSummary( string customerCode )
		{
			var RetVal = new CompanySummaryList();

			try
			{
				var Recs = from Rc in Entity.ResellerCustomerCompanySummaries
				           where Rc.CustomerCode == customerCode
				           orderby Rc.CompanyName
				           select Rc;

				RetVal.AddRange( Recs.Select( rec => new CompanyByAccountSummary
				                                     {
					                                     CustomerCode    = customerCode ?? "",
					                                     CompanyName     = rec.CompanyName ?? "",
					                                     LocationBarcode = rec.LocationBarcode ?? "",
					                                     Suite           = rec.Suite ?? "",
					                                     AddressLine1    = rec.AddressLine1 ?? "",
					                                     AddressLine2    = rec.AddressLine2 ?? "",
					                                     City            = rec.City ?? "",
					                                     Region          = rec.Region ?? "",
					                                     PostalCode      = rec.PostalCode ?? "",
					                                     CountryCode     = rec.CountryCode ?? ""
				                                     } ) );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( nameof( GetCustomerCompaniesSummary ), Exception );
			}

			return RetVal;
		}

		public CompanyDetailList GetCustomerCompaniesDetailed( string customerCode )
		{
			var Result = new CompanyDetailList();

			try
			{
				var E = Entity;

				var CustomerCompanies = ( from C in E.ResellerCustomerCompanies
				                          where C.CustomerCode == customerCode
				                          let Comp = ( from C1 in E.Companies
				                                       where C1.CompanyId == C.CompanyId
				                                       let Address = ( from A in E.Addresses
				                                                       where A.Id == C1.PrimaryAddressId
				                                                       select A ).ToList()
				                                       select new { Company = C1, Address } ).FirstOrDefault()
				                          select Comp ).ToList();

				foreach( var Co in CustomerCompanies )
				{
					if( !( Co is null ) )
					{
						foreach( var Address in Co.Address )
						{
							Result.Add( new CompanyDetail
							            {
								            Company = ConvertCompany( Co.Company, Address ),
								            Address = ConvertCompanyAddress( Co.Company, Address )
							            } );
						}
					}
				}
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}

			return Result;
		}
	}
}