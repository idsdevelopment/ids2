﻿#nullable enable

using System;
using System.Linq;
using Protocol.Data;
using Utils;

namespace Database.Model.Databases.Carrier
{
	public partial class CarrierDb
	{
		public const string EDIT_PIECES_WEIGHT             = "EDIT_PIECES_WEIGHT",
		                    EDIT_REFERENCE                 = "EDIT_REFERENCE",
		                    BY_PIECES                      = "BY_PIECES",
		                    BY_LOCATION                    = "BY_LOCATION",
		                    ALLOW_MANUAL_BARCODE           = "ALLOW_MANUAL_BARCODE",
		                    ADMIN_EMAIL                    = "ADMIN_EMAIL",
		                    ALLOW_UNDELIVERABLE            = "ALLOW_UNDELIVERABLE",
		                    UNDELIVERABLE_BACK_TO_DISPATCH = "UNDELIVERABLE_BACK_TO_DISPATCH",
		                    UNDELIVERABLE_DELETE_NO_PICKUP = "UNDELIVERABLE_DELETE_NO_PICKUP";

		public DevicePreferences DevicePreferences()
		{
			var Result = new DevicePreferences();
			var Prefs  = GetPreferences();

			foreach( var P in Prefs )
			{
				switch( P.DevicePreference.Trim() )
				{
				case EDIT_PIECES_WEIGHT:
					Result.EditPiecesWeight = P.Enabled;

					break;

				case EDIT_REFERENCE:
					Result.EditReference = P.Enabled;

					break;

				case BY_PIECES:
					Result.ByPiece = P.Enabled;

					break;

				case BY_LOCATION:
					Result.ByLocation = P.Enabled;

					break;

				case ALLOW_MANUAL_BARCODE:
					Result.AllowManualBarcodeInput = P.Enabled;

					break;

				case ALLOW_UNDELIVERABLE:
					Result.AllowUndeliverable = P.Enabled;

					break;

				case UNDELIVERABLE_BACK_TO_DISPATCH:
					Result.UndeliverableBackToDispatch = P.Enabled;

					break;

				case UNDELIVERABLE_DELETE_NO_PICKUP:
					Result.DeleteCannotPickup = P.Enabled;

					break;
				}
			}

			return Result;
		}


		public ClientPreferences ClientPreferences() => new ClientPreferences();


		public Preferences GetPreferences()
		{
			var MasterPreferences = Database.Users.GetPreference( Context );

			var CarrierPreferences = MasterPreferences.Select( carrierPreference => ( from Up in Entity.Preferences
			                                                                          where Up.UsersPreferenceId == carrierPreference.Id
			                                                                          select Up ).FirstOrDefault() ).Where( rec => rec != null ).ToList();

			var IsIds   = Context.IsIds;
			var Toffset = Context.TimeZoneOffset;

			var Result = ( from MasterPreference in MasterPreferences
			               where !MasterPreference.IdsOnly || ( IsIds && MasterPreference.IdsOnly )
			               where ( MasterPreference.ResellerId == "" ) || ( string.Compare( MasterPreference.ResellerId, Context.CarrierId, StringComparison.OrdinalIgnoreCase ) == 0 )
			               let CarrierRec = ( from Cp in CarrierPreferences
			                                  where Cp.UsersPreferenceId == MasterPreference.Id
			                                  select Cp ).FirstOrDefault()
			               let Enabled = CarrierRec?.Enabled ?? MasterPreference.Default
			               let StringValue = CarrierRec?.StringValue ?? MasterPreference.DefaultStringValue
			               let InvValue = CarrierRec?.IntValue ?? MasterPreference.DefaultIntValue
			               let DoubleValue = (decimal)( CarrierRec?.DoubleValue ?? MasterPreference.DefaultDoubleValue )
			               let DateTimeValue = ( CarrierRec?.DateTimeValue ?? DateTime.UtcNow ).AddHours( Toffset )
			               orderby MasterPreference.IdsOnly descending, MasterPreference.DisplayOrder descending, MasterPreference.SubDisplayOrder, MasterPreference.Description
			               select new Preference
			                      {
				                      DataType         = (byte)MasterPreference.DataType,
				                      Description      = MasterPreference.Description,
				                      Enabled          = Enabled,
				                      DevicePreference = MasterPreference.DevicePreference,
				                      IdsOnly          = MasterPreference.IdsOnly,
				                      Id               = MasterPreference.Id,
				                      DisplayOrder     = MasterPreference.DisplayOrder,
				                      DecimalValue     = DoubleValue,
				                      StringValue      = StringValue,
				                      IntValue         = InvValue,
				                      DateTimeValue    = DateTimeValue
			                      } ).ToList();

			var Reslt = new Preferences();
			Reslt.AddRange( Result );

			return Reslt;
		}

		public void UpdatePreference( Preference preference )
		{
			var Rec = ( from P in Entity.Preferences
			            where P.UsersPreferenceId == preference.Id
			            select P ).FirstOrDefault();

			if( Rec == null )
			{
				Rec = new MasterTemplate.Preference { UsersPreferenceId = preference.Id };
				Entity.Preferences.Add( Rec );
			}

			Rec.Enabled       = preference.Enabled;
			Rec.DateTimeValue = preference.DateTimeValue.AddHours( -Context.TimeZoneOffset );
			Rec.DoubleValue   = (float)preference.DecimalValue;
			Rec.IntValue      = preference.IntValue;
			Rec.StringValue   = preference.StringValue;

			Entity.SaveChanges();
		}

		public bool IsPasswordValid( string password )
		{
			try
			{
				var (Password, Ok) = Encryption.FromTimeLimitedToken( password );

				if( !Ok )
					throw new Exception( "Invalid password" );

				var Pl = Password.Length;

				var Preferences = ( from P in GetPreferences()
				                    where P.DevicePreference.IsNotNullOrWhiteSpace()
				                    select P ).ToDictionary( preference => preference.DevicePreference.Trim(), preference => preference.IntValue );

				if( Preferences.TryGetValue( "PASSWORD_MIN_LENGTH", out var PLength ) && ( Pl >= PLength ) )
				{
					var Upper   = 0;
					var Lower   = 0;
					var Numeric = 0;
					var Symbols = 0;

					foreach( var C in Password )
					{
						if( ( C >= '0' ) && ( C <= '9' ) )
							Numeric++;
						else if( ( C >= 'A' ) && ( C <= 'Z' ) )
							Upper++;
						else if( ( C >= 'a' ) && ( C <= 'z' ) )
							Lower++;
						else
							Symbols++;
					}

					if( Preferences.TryGetValue( "PASSWORD_MIN_NUMERIC", out var PNum ) && ( Numeric >= PNum ) )
					{
						if( Preferences.TryGetValue( "PASSWORD_MIN_UPPER", out var PUpper ) && ( Upper >= PUpper ) )
						{
							if( Preferences.TryGetValue( "PASSWORD_MIN_LOWER", out var PLower ) && ( Lower >= PLower ) )
								return Preferences.TryGetValue( "PASSWORD_MIN_SYMBOL", out var PSymbol ) && ( Symbols >= PSymbol );
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return false;
		}
	}
}