﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using Interfaces.Interfaces;
using Protocol.Data;
using Utils;
using Role = Protocol.Data.Role;
using Staff = Protocol.Data.Staff;

namespace Database.Utils
{
	public class Company
	{
		public static void CreateCompanyStorage( IRequestContext context, CarrierDb db )
		{
			var E = Users.AddStorageEndPoint( context );

			if( !( E is null ) )
			{
				var Cfg = new Configuration
				          {
					          CurrentVersion       = db.CurrentVersion,
					          StorageAccount       = E.StorageAccount,
					          StorageAccountKey    = E.StorageAccountKey,
					          StorageBlobEndpoint  = E.StorageBlobEndpoint,
					          StorageFileEndpoint  = E.StorageFileEndpoint,
					          StorageTableEndpoint = E.StorageTableEndpoint
				          };

				db.UpdateConfiguration( Cfg );
			}
		}

		// Disposes returned Db
		public static bool CreateCompany( IRequestContext context, Func<CarrierDb> dbCreateFunc )
		{
			try
			{
				var Db = dbCreateFunc();
				CreateCompanyStorage( context, Db );
				Db.AddDefaultRoles();
				Db.Dispose();

				return true;
			}
			catch( Exception E )
			{
				context.SystemLogException( nameof( CreateCompany ), E );
			}

			return false;
		}

		public static long CreateCompany( IRequestContext context, string carrierId, string adminId, string adminPassword )
		{
			var Pid = Users.StartProcess( context );

			var SaveId = context.CarrierId;
			context.CarrierId = carrierId.Trim().Capitalise();

			Task.Run( () =>
			          {
				          try
				          {
					          var Ok = CreateCompany( context, () =>
					                                           {
						                                           var Db = new CarrierDb( context ) { IgnoreUpgrade = true };

						                                           if( !Db.Exists )
						                                           {
							                                           DbUtils.CreateIfNew( context, Db.Entity ); // Disposes Entity
							                                           Db.Dispose();

							                                           Db = new CarrierDb( context ) { IgnoreUpgrade = true };
						                                           }

						                                           return Db;
					                                           } );

					          if( Ok )
					          {
						          using var Db = new CarrierDb( context );

						          // Make sure there is a default Config record
						          Db.UpdateConfiguration( new Configuration
						                                  {
							                                  CompanyAddressId     = Guid.Empty,
							                                  Id                   = Guid.Empty,
							                                  CurrentVersion       = 0,
							                                  StorageAccount       = "",
							                                  StorageAccountKey    = "",
							                                  StorageBlobEndpoint  = "",
							                                  StorageFileEndpoint  = "",
							                                  StorageTableEndpoint = ""
						                                  } );

						          Db.AddRoles( new List<Role>
						                       {
							                       new Role
							                       {
								                       Name            = CarrierDb.ADMIN,
								                       IsAdministrator = true,
								                       JsonObject      = "",
								                       IsDriver        = false,
								                       Mandatory       = false
							                       }
						                       } );

						          var UpdRec = new UpdateStaffMemberWithRolesAndZones( "Carrier create wizard.", new Staff
						                                                                                         {
							                                                                                         StaffId  = adminId,
							                                                                                         Password = Encryption.ToTimeLimitedToken( adminPassword ),
							                                                                                         Enabled  = true
						                                                                                         } );

						          UpdRec.Roles.Add( new Role
						                            {
							                            IsAdministrator = true,
							                            Name            = CarrierDb.ADMIN
						                            } );

						          Db.AddUpdateStaffMember( UpdRec );
					          }
				          }
				          catch( Exception Exception )
				          {
					          context.SystemLogException( Exception );
				          }
				          finally
				          {
					          context.CarrierId = SaveId;
					          Users.EndProcess( context, Pid );
				          }
			          } );

			return Pid;
		}
	}

	public class DbUtils
	{
		public const string MASTER_DATABASE_NAME = "MasterTemplate";

		private const string BASIC    = "Basic",
		                     STANDARD = "Standard";

		public enum EDITION
		{
			BASIC,
			S0,
			S1,
			S2
		}

		public enum INITIALISE_MODULE
		{
			NONE,
			CREATE,
			DELETE
		}

		public class AzureEdition
		{
			public string Version { get; set; } = "";
			public string Edition { get; set; } = "";
			public string Tier { get; set; } = "";
		}

		public static string GetDatabaseName( DbContext db )
		{
			return new SqlConnectionStringBuilder( db.Database.Connection.ConnectionString ).InitialCatalog;
		}

		public static void GetDatabaseSettings( DbContext db, out string dbName, out string userName, out string password )
		{
			var Conn = new SqlConnectionStringBuilder( db.Database.Connection.ConnectionString );
			dbName   = Conn.InitialCatalog;
			userName = Conn.UserID;
			password = Conn.Password;
		}

		public static void GetDatabaseSettings( string connectionString, out string dbName, out string userName,
		                                        out string password )
		{
			var Conn = new SqlConnectionStringBuilder( connectionString );
			dbName   = Conn.InitialCatalog;
			userName = Conn.UserID;
			password = Conn.Password;
		}

		public static bool Exists( DbContext entity )
		{
			return entity.Database.Exists();
		}

		public static void Clone( IRequestContext context, DbContext entity, string dbToCopy, string newDatabaseName )
		{
			try
			{
				entity.Database.ExecuteSqlCommand( TransactionalBehavior.DoNotEnsureTransaction, $"CREATE DATABASE {newDatabaseName} AS COPY OF {dbToCopy}" );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( nameof( Clone ), Exception );
			}
		}

		public static AzureEdition GetEdition( IRequestContext context, DbContext entity )
		{
			var DbName = GetDatabaseName( entity );

			var Sql = $"SELECT @@VERSION AS Version, DATABASEPROPERTYEX( '{DbName}', 'Edition' ) AS Edition, COALESCE(DATABASEPROPERTYEX( '{DbName}', 'ServiceObjective' ), 'N/A in v11') AS Tier";

			return entity.Database.SqlQuery<AzureEdition>( Sql ).FirstOrDefault();
		}

		public static AzureEdition SetEdition( IRequestContext context, DbContext entity, string edition, string tier, string maxSizeInGb )
		{
			var OldEdition = GetEdition( context, entity );
			var DbName     = GetDatabaseName( entity );
			entity.Database.ExecuteSqlCommand( TransactionalBehavior.DoNotEnsureTransaction, $"ALTER DATABASE {DbName} MODIFY (EDITION = '{edition}',  SERVICE_OBJECTIVE = '{tier}',  MAXSIZE = {maxSizeInGb} GB)" );

			return OldEdition;
		}


		public static AzureEdition SetEdition( IRequestContext context, DbContext entity, AzureEdition edition )
		{
			var MaxSize = edition.Edition.Contains( BASIC, StringComparison.OrdinalIgnoreCase ) ? "2" : "250";

			return SetEdition( context, entity, edition.Edition, edition.Tier, MaxSize );
		}


		public static AzureEdition SetEdition( IRequestContext context, DbContext entity, EDITION edition )
		{
			try
			{
				string MaxSize = "250",
				       Edition = STANDARD,
				       Tier;

				switch( edition )
				{
				case EDITION.S0:
					Tier = "S0";

					break;

				case EDITION.S1:
					Tier = "S1";

					break;
				case EDITION.S2:
					Tier = "S2";

					break;
				default:
					MaxSize = "2";
					Edition = BASIC;
					Tier    = BASIC;

					break;
				}

				return SetEdition( context, entity, Edition, Tier, MaxSize );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}

			return new AzureEdition
			       {
				       Edition = BASIC,
				       Tier    = BASIC,
				       Version = ""
			       };
		}

		public static void Clone( IRequestContext context, DbContext entity, string newDatabaseName )
		{
			Clone( context, entity, entity.Database.Connection.Database, newDatabaseName );
		}

		public static bool CreateIfNew( IRequestContext context, DbContext entity )
		{
			if( context == null )
				throw new ArgumentNullException( nameof( context ) );

			using( entity )
			{
				var DBase = entity.Database;

				if( !DBase.Exists() )
				{
					var SaveTimeOut = DBase.CommandTimeout;
					var Error       = false;

					try
					{
						using( var MDb = new MasterTemplateEntities() )
						{
							MDb.Database.CommandTimeout = 10 * 60;
							Clone( context, MDb, MASTER_DATABASE_NAME, entity.Database.Connection.Database );

							while( !DBase.Exists() )
								Thread.Sleep( 2000 );
						}

						return true;
					}
					catch( Exception Ex )
					{
						Error = true;
						context.SystemLogException( nameof( CreateIfNew ), Ex );
					}
					finally
					{
						if( !Error )
						{
							var DbName = GetDatabaseName( entity );

							try
							{
								var SqlCommand = $"ALTER DATABASE {DbName} MODIFY (EDITION = 'basic')";
								DBase.ExecuteSqlCommand( TransactionalBehavior.DoNotEnsureTransaction, SqlCommand );
							}
							catch // Throw away probably local Db
							{
							}
						}

						DBase.CommandTimeout = SaveTimeOut;
					}
				}
			}

			return false;
		}

		public static bool DeleteIfExists( IRequestContext context, DbContext entity )
		{
			using( entity )
			{
				var DBase       = entity.Database;
				var SaveTimeOut = DBase.CommandTimeout;

				try
				{
					DBase.CommandTimeout = 5 * 60;
					DBase.Delete();

					return true;
				}
				catch
				{
				}
				finally
				{
					DBase.CommandTimeout = SaveTimeOut;
				}
			}

			return false;
		}

		public static bool DeleteIfExists( IRequestContext context, string connectionString )
		{
			using var Db = new DbContext( connectionString );

			return DeleteIfExists( context, Db );
		}

		public static string RenameDatabase( IRequestContext context, string connectionString, string oldName, string newName )
		{
			using var Connection   = new SqlConnection( connectionString );
			var       ConnectionDb = Connection.Database;

			var       NewConnection = new StringBuilder( connectionString ).Replace( $"catalog={ConnectionDb}", $"catalog={oldName}" ).ToString();
			using var Connection1   = new SqlConnection( NewConnection );

			try
			{
				Connection1.Open();

				var Command = new SqlCommand( $"ALTER DATABASE {oldName} MODIFY NAME = {newName};", Connection1 );

				Command.ExecuteNonQuery();

				Connection1.Close();
			}
			catch // Rename appears to close the connection
			{
			}

			NewConnection = new StringBuilder( NewConnection ).Replace( $"catalog={oldName}", $"catalog={newName}" ).ToString();

			try
			{
				//Thread.Sleep( 30_000 );
				using var Connection2 = new SqlConnection( NewConnection );

				Connection2.Open();
				Connection2.Close();
			}
			catch( Exception E )
			{
				context.SystemLogException( E );

				return "";
			}

			return NewConnection;
		}


		public static string RenameDatabase( IRequestContext context, SqlConnection connection, string oldName, string newName )
		{
			return RenameDatabase( context, connection.ConnectionString, oldName, newName );
		}

		public static string RenameDatabase( IRequestContext context, DbConnection connection, string oldName, string newName )
		{
			return RenameDatabase( context, connection.ConnectionString, oldName, newName );
		}
	}
}