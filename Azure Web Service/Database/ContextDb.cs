﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Text;
using Database.Utils;
using Interfaces.Interfaces;
using Storage;
using Utils;
using File = Utils.File;

// ReSharper disable StaticMemberInGenericType

#pragma warning disable 169

namespace Database.Model.Databases
{
	public class DataContextConfiguration : DbConfiguration
	{
		public DataContextConfiguration()
		{
			SetExecutionStrategy( "System.Data.SqlClient", () => new SqlAzureExecutionStrategy( 120, TimeSpan.FromMilliseconds( 500 ) ) );
		}
	}

	[DbConfigurationType( typeof( DataContextConfiguration ) )]
	public abstract class ContextDb<TE> : ADisposable where TE : DbContext, new()
	{
		private const string
			CATALOG_STRING = "initial catalog=",
			UPGRADE_PATH = @"\Upgrades\";

		private const int SQL_TIMEOUT = 1200; // In Seconds

		//
		private static volatile bool CheckVersion = true;

		private static readonly object LockObject = new object();

		private bool InternalIgnoreUpgrade;
		private DbContextTransaction Transaction;

		private TE _Entity;

		//
		protected abstract TE CreateEntity( string connectionString );

		protected abstract void GetConnection( out string databaseName, out string connectionName, out string cacheKey );

		protected abstract bool UpgradeToVersion( TE entity, int toVersion ); // Returns false if completed early

		//
		protected abstract int GetDatabaseVersion( TE entity );

		protected abstract void SetDatabaseVersion( TE entity, int version );

		public abstract int CurrentVersion { get; }

		protected override void OnDispose( bool systemDisposing )
		{
			Rollback();
			_Entity?.Dispose();
		}

		protected void SqlCommand( string command )
		{
			var SaveIgnoreUpgrade = InternalIgnoreUpgrade;
			InternalIgnoreUpgrade = true;

			try
			{
				Entity.Database.ExecuteSqlCommand( TransactionalBehavior.DoNotEnsureTransaction, command );
			}
			finally
			{
				InternalIgnoreUpgrade = SaveIgnoreUpgrade;
			}
		}

		protected void RunUpgradeScript( string scriptName )
		{
			using var ScriptStream = new FileStream( UPGRADE_PATH + scriptName, FileMode.Open, FileAccess.Read, FileShare.Read );

			var Len = (int)ScriptStream.Length;
			var Bytes = new byte[ Len ];
			ScriptStream.Read( Bytes, 0, Len );

			var Script = Encoding.UTF8.GetString( Bytes )
			                     .Replace( "\r", "" )
			                     .Replace( '\t', ' ' );

			var Temp = new StringBuilder();

			foreach( var C in Script )
			{
				var C1 = C;

				if( C1 != '\n' )
				{
					if( ( C1 < ' ' ) || ( C1 > '~' ) )
						C1 = ' ';
				}

				Temp.Append( C1 );
			}

			Script = Temp.ToString();

			// Remove Comments
			int P;

			while( true )
			{
				P = Script.IndexOf( "/*", StringComparison.Ordinal );

				if( P >= 0 )
				{
					var P2 = Script.IndexOf( "*/", P + 2, StringComparison.Ordinal );

					if( P2 >= 2 )
						Script = Script.Remove( P, ( P2 - P ) + 2 );
				}
				else
					break;
			}

			var Lines = Script.Split( new[]
			                          {
				                          '\n'
			                          }, StringSplitOptions.RemoveEmptyEntries );

			Lines = ( from L in Lines
			          let L1 = L.Trim()
			          where !string.IsNullOrWhiteSpace( L1 )
			                && !L1.StartsWith( "PRINT ", StringComparison.OrdinalIgnoreCase )
			                && !string.Equals( L1, "GO", StringComparison.OrdinalIgnoreCase )
			                && !L1.StartsWith( ":setvar ", StringComparison.OrdinalIgnoreCase )
			                && !L1.StartsWith( ":on ", StringComparison.OrdinalIgnoreCase )
			                && !L1.StartsWith( "USE [", StringComparison.OrdinalIgnoreCase )
			          select L1 ).ToArray();

			Script = string.Join( " ", Lines );
			P = Script.IndexOf( "IF N'$(__IsSqlCmdEnabled)'", StringComparison.OrdinalIgnoreCase );

			if( P >= 0 )
			{
				var P1 = Script.IndexOf( " END ", P, StringComparison.OrdinalIgnoreCase );

				if( P1 > P )
					Script = Script.Remove( P, ( P1 - P ) + 5 ); // Including END
			}

			// Make Multi User
			var Replace = "tmp_ms_" + Guid.NewGuid().ToString().Replace( '-', '_' ) + '_';
			Script = Script.Replace( "tmp_ms_xx_", Replace );

			SqlCommand( Script ); // Use space don't join statements
		}

		public int TransactionDepth { get; private set; }

		public void BeginTransaction()
		{
			if( !( _Entity is null ) && ( TransactionDepth++ == 0 ) )
				Transaction = Entity.Database.BeginTransaction();
		}

		public void Commit()
		{
			if( ( TransactionDepth > 0 ) && ( --TransactionDepth == 0 ) && ( Transaction != null ) )
			{
				Transaction.Commit();
				Transaction.Dispose();
				Transaction = null;
			}
		}

		public void Rollback()
		{
			if( Transaction != null )
			{
				TransactionDepth = 0;
				Transaction.Rollback();
				Transaction.Dispose();
				Transaction = null;
			}
		}

		public bool Exists => DbUtils.Exists( Entity );

		public int DatabaseVersion
		{
			get
			{
				using var Ent = Entity;

				return GetDatabaseVersion( Ent );
			}
			set
			{
				using var Ent = Entity;
				SetDatabaseVersion( Ent, value );
			}
		}

		public TE Entity
		{
			get
			{
				if( _Entity != null ) return _Entity;

				string CString = null;

				GetConnection( out var DatabaseName, out var ConnectionName, out var CacheKey );

				var Variables = Context.Variables; // Context Key Cache

				if( !Variables.ContainsKey( CacheKey ) )
				{
					var Config = ConfigurationManager.ConnectionStrings[ ConnectionName ];

					if( Config == null )
						Context.SystemLogException( "Missing connection string: " + ConnectionName );

					if( Config != null )
						CString = Config.ConnectionString;

					var ReplaceString = File.MakeIdentFileName( Context.CarrierId );

					var CatalogString = CATALOG_STRING + DatabaseName;

					if( CString != null )
					{
						var UserDb = $"{CATALOG_STRING}{ReplaceString.Capitalise()}";
						CString = CString.Replace( CatalogString, UserDb );
						Variables[ CacheKey ] = CString;
					}
				}
				else
					CString = Variables[ CacheKey ];

				var E = _Entity = CreateEntity( CString );

				// Increase timeout for low DTU systems
				E.Database.CommandTimeout = SQL_TIMEOUT;
				var SqlContext = ( (IObjectContextAdapter)E ).ObjectContext;
				SqlContext.CommandTimeout = SQL_TIMEOUT;
			#if SHOW_SQL
                E.Database.Log = s => Debug.WriteLine( s );
			#endif

				if( !InternalIgnoreUpgrade && !IgnoreUpgrade )
				{
					lock( LockObject ) // Keep Locked until upgrade is finished
					{
						if( CheckVersion )
						{
							CheckVersion = false;
							var Vsn = CurrentVersion;
							var DbVsn = GetDatabaseVersion( E );

							try
							{
								while( DbVsn++ < Vsn )
								{
									// Don't set version if update errro
									var SetVersion = true;

									try
									{
										if( !UpgradeToVersion( E, DbVsn ) )
											break;
									}
									catch
									{
										SetVersion = false;

										throw;
									}
									finally
									{
										if( SetVersion )
											SetDatabaseVersion( E, Vsn );
									}
								}
							}
							catch( Exception Exception )
							{
								Context.SystemLogException( "Database Upgrade Error: " + DbUtils.GetDatabaseName( E ), Exception );
							}
						}
					}
				}

				return E;
			}
		}

//
		protected readonly IRequestContext Context;
		public bool IgnoreUpgrade;

		protected ContextDb( IRequestContext context )
		{
			Context = context;
		}
	}
}