﻿using System;

namespace MapsApi.GeoLocation
{
	internal struct Ellipsoid
	{
		internal double A,
		                B,
		                F;
	}

	public enum ELLIPSOID_MODEL
	{
		WGS_84,
		GRS_80,
		AIRY_1830,
		INTL_1924,
		CLARKE_1880,
		GRS_67
	}

	public class Point
	{
		public decimal Latitude { get; set; }

		public decimal Longitude { get; set; }

		public override string ToString()
		{
			return Latitude + "," + Longitude;
		}

		public double Distance { get; set; }

		public bool Locked // Usually flags start and end
		{
			get;
			set;
		}
	}

	public class Points
	{
		private static readonly Ellipsoid[] Ellipsoids =
		{
			new Ellipsoid { A = 6378137, B = 6356752.3142, F = 1 / 298.257223563 },  // WGS-84
			new Ellipsoid { A = 6378137, B = 6356752.3141, F = 1 / 298.257222101 },  // GRS-80
			new Ellipsoid { A = 6377563.396, B = 6356256.909, F = 1 / 299.3249646 }, // Airy (1830)
			new Ellipsoid { A = 6378388, B = 6356911.946, F = 1 / 297.0 },           // Intl (1929)
			new Ellipsoid { A = 6378249.145, B = 6356514.86955, F = 1 / 293.465 },   // Clark (1880)
			new Ellipsoid { A = 6378160, B = 6356774.719, F = 1 / 298.25 }           // GRS-67
		};

		public Point From = new Point(),
		             To = new Point();

		public static double DegreesToRadians( decimal angle )
		{
			return Math.PI / 180 * (double)angle;
		}

/*
		//  public-domain code by Darel Rex Finley,
		//  2010.  See diagrams at http://
		//  alienryderflex.com/point_left_of_ray

		bool isPointLeftOfRay( double x, double y, double raySx, double raySy, double rayEx, double rayEy )
		{
			return ( y - raySy ) * ( rayEx - raySx )
  >      ( x - raySx )  *( rayEy - raySy );
		}
*/

		public bool IsPointLeftOfLine( Point point )
		{
			return ( point.Longitude - From.Longitude ) * ( To.Latitude - From.Latitude ) >
			       ( point.Latitude - From.Latitude ) * ( To.Longitude - From.Longitude );
		}

/* Calculate geodesic distance (in m) between two points specified by latitude/longitude (in numeric degrees)
	using Vincenty inverse formula for ellipsoids
*/

		public double DistaceVincenty( ELLIPSOID_MODEL model = ELLIPSOID_MODEL.WGS_84 )
		{
			var Elipse = Ellipsoids[ (int)model ];

			double SinSigma = 0,
			       Cos2SigmaM = 0,
			       CosSigma = 0,
			       CosSqAlpha = 0,
			       Sigma = 0;

			var L = DegreesToRadians( To.Longitude - From.Longitude );

			var U1 = Math.Atan( ( 1 - Elipse.F ) * Math.Tan( DegreesToRadians( From.Latitude ) ) );
			var SinU1 = Math.Sin( U1 );
			var CosU1 = Math.Cos( U1 );

			var U2 = Math.Atan( ( 1 - Elipse.F ) * Math.Tan( DegreesToRadians( To.Latitude ) ) );
			var SinU2 = Math.Sin( U2 );
			var CosU2 = Math.Cos( U2 );

			var Lambda = L;
			var LambdaP = 2 * Math.PI;

			var IterLimit = 20;

			for( ; IterLimit > 0; IterLimit-- )
			{
				if( Math.Abs( Lambda - LambdaP ) <= 1e-12 )
					break;

				var SinLambda = Math.Sin( Lambda );
				var CosLambda = Math.Cos( Lambda );

				var CosU2SinLambda = CosU2 * SinLambda;
				var CosU1SinU2SinU1CosU2CosLambda = CosU1 * SinU2 - SinU1 * CosU2 * CosLambda;

				SinSigma =
					Math.Sqrt( CosU2SinLambda * CosU2SinLambda + CosU1SinU2SinU1CosU2CosLambda * CosU1SinU2SinU1CosU2CosLambda );

				if( Math.Abs( SinSigma ) < 1e-12 )
					return 0; // co-incident points

				CosSigma = SinU1 * SinU2 + CosU1 * CosU2 * CosLambda;
				Sigma = Math.Atan2( SinSigma, CosSigma );

				var SinAlpha = CosU1 * CosU2 * SinLambda / SinSigma;
				CosSqAlpha = 1 - SinAlpha * SinAlpha;

				try
				{
					Cos2SigmaM = CosSigma - 2 * SinU1 * SinU2 / CosSqAlpha;
				}
				catch
				{
					Cos2SigmaM = 0; // equatorial line
				}

				var C = Elipse.F / 16 * CosSqAlpha * ( 4 + Elipse.F * ( 4 - 3 * CosSqAlpha ) );

				LambdaP = Lambda;

				Lambda = L +
				         ( 1 - C ) * Elipse.F * SinAlpha *
				         ( Sigma + C * SinSigma * ( Cos2SigmaM + C * CosSigma * ( -1 + 2 * Cos2SigmaM * Cos2SigmaM ) ) );
			}

			if( IterLimit == 0 )
				return 0; // formula failed to converge

			var EaSq = Elipse.A * Elipse.A;
			var EbSq = Elipse.B * Elipse.B;

			var Usq = CosSqAlpha * ( EaSq - EbSq ) / EbSq;

			var A1 = 1 + Usq / 16384 * ( 4096 + Usq * ( -768 + Usq * ( 320 - 175 * Usq ) ) );
			var B1 = Usq / 1024 * ( 256 + Usq * ( -128 + Usq * ( 74 - 47 * Usq ) ) );

			var DeltaSigma = B1 * SinSigma * ( Cos2SigmaM + B1 / 4 * ( CosSigma * ( -1 + 2 * Cos2SigmaM * Cos2SigmaM )
			                                                           - B1 / 6 * Cos2SigmaM * ( -3 + 4 * SinSigma * SinSigma ) * ( -3 + 4 * Cos2SigmaM * Cos2SigmaM ) ) );

			return Elipse.B * A1 * ( Sigma - DeltaSigma );
		}

		public enum MEASUREMENT
		{
			KILOMETRES,
			MILES,
			NAUTICAL_MILES
		}

		public double DistanceTo( MEASUREMENT measurement = MEASUREMENT.KILOMETRES )
		{
			var Rlat1 = Math.PI * (double)From.Latitude / 180;
			var Rlat2 = Math.PI * (double)To.Latitude / 180;
			var Theta = From.Longitude - To.Longitude;
			var Rtheta = Math.PI * (double)Theta / 180;

			var Dist =
				Math.Sin( Rlat1 ) * Math.Sin( Rlat2 ) + Math.Cos( Rlat1 ) *
				Math.Cos( Rlat2 ) * Math.Cos( Rtheta );
			Dist = Math.Acos( Dist );
			Dist = Dist * 180 / Math.PI;
			Dist = Dist * 60 * 1.1515;

			if( double.IsNaN( Dist ) )
				Dist = 0;

			switch( measurement )
			{
			case MEASUREMENT.KILOMETRES: //Kilometres -> default
				return Dist * 1.609344;

			case MEASUREMENT.NAUTICAL_MILES: //Nautical Miles 
				return Dist * 0.8684;

			default: //Miles
				return Dist;
			}
		}
	}
}