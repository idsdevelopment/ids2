﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using MapsApi.GeoLocation;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace Maps.BingMapsApi
{
	internal class BingMaps<T> : BingAMaps
	{
		private readonly BingAMaps Owner;

		public BingMaps( BingAMaps owner )
			: base( owner.Context )
		{
			Owner = owner;
		}

		public class CommonResponse
		{
			public class ResourceSet
			{
				public long estimatedTotal = -1;
				public T[] resources = null;
			}

			public string[] errorDetails = null;

			public ResourceSet[] resourceSets = null;
			public int statusCode = -1;

			public string statusDescription = null,
			              authenticationResultCode = null,
			              traceId = null,
			              copyright = null,
			              brandLogoUri = null;
		}

		private static CommonResponse GetResponse( string url, out bool rateLimited )
		{
			const string RATE_LIMIT = "X-MS-BM-WS-INFO";

			try
			{
				WebHeaderCollection Headers;
				var ResponseString = GetResponse( url, out Headers );

				if( Headers != null )
				{
					var Limit = Headers[ RATE_LIMIT ];
					rateLimited = ( Limit != null ) && ( Limit.Trim() == "1" );
				}
				else
					rateLimited = true;

				return JsonConvert.DeserializeObject<CommonResponse>( ResponseString );
			}
			catch( Exception )
			{
				rateLimited = false;

				return null;
			}
		}

		private static CommonResponse.ResourceSet[] GetResources( string url, out bool rateLimited )
		{
			var Data = GetResponse( url, out rateLimited );

			return ( Data != null ) && ( Data.resourceSets.Length > 0 ) ? Data.resourceSets : null;
		}

		internal CommonResponse GetResponse( string queryType, string queryParameters, out bool rateLimited,
		                                     bool errorDetail = false )
		{
			return GetResponse( Url( queryType, queryParameters, errorDetail ), out rateLimited );
		}

		internal CommonResponse GetResponse( string queryType, Dictionary<string, string> queryParameters,
		                                     out bool rateLimited, bool errorDetail = false )
		{
			return GetResponse( Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ), errorDetail ),
			                    out rateLimited );
		}

		public CommonResponse.ResourceSet[] GetResources( string queryType, string queryParameters, out bool rateLimited,
		                                                  bool errorDetail = false )
		{
			return GetResources( Url( queryType, queryParameters, errorDetail ), out rateLimited );
		}

		public CommonResponse.ResourceSet[] GetResources( string queryType, Dictionary<string, string> queryParameters,
		                                                  out bool rateLimited, bool errorDetail = false )
		{
			return GetResources( Url( queryType, queryParameters, errorDetail ), out rateLimited );
		}

		public override List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited )
		{
			throw new NotImplementedException();
		}

		public override void ShowDirections( MapsApi.Directions directions )
		{
			throw new NotImplementedException();
		}

		public override string ShowMapLink => throw new NotImplementedException();

		internal override string BaseUrl => Owner.BaseUrl;

		internal override string Version => Owner.Version;
	}

	public abstract class BingAMaps : AMapsApi
	{
		internal BingAMaps( RequestContext.RequestContext context ) : base( context )
		{
		}

		protected override string Key => Context.Configuration.BingMapsApiKey;

		internal abstract string BaseUrl { get; }

		internal abstract string Version { get; }

		public class Address
		{
			public string addressLine = null,
			              locality = null,
			              neighborhood = null,
			              adminDistrict = null,
			              adminDistrict2 = null,
			              formattedAddress = null,
			              postalCode = null,
			              countryRegion = null;

			public MapsApi.AddressData ToAddressData()
			{
				return new MapsApi.AddressData
				       {
					       PostalCode = postalCode,
					       StateCode = adminDistrict,
					       City = locality,
					       FormattedAddress = formattedAddress,
					       CountryCode = countryRegion,
					       Vicinity = neighborhood,
					       Country = countryRegion
				       };
			}
		}

		public class BingPoint
		{
			public double[] coordinates = null;
			public string type = null;
		}

		public class GeocodeBingPoints : BingPoint
		{
			public string calculationMethod = null;
			public string[] usageTypes = null;
		}

		public class LocationResource
		{
			public string __type = null;
			public Address address = null;
			public double[] bbox = null;
			public GeocodeBingPoints[] geocodePoints = null;
			public string[] matchCodes = null;

			public string name = null,
			              entityType = null,
			              confidence = null;
		}

		public string Url( string queryType, string queryParameters, bool errorDetail = false )
		{
			var Result = new StringBuilder( BaseUrl ).Append( Version ).Append( '/' ).Append( queryType )
			                                         .Append( '?' ).Append( queryParameters ).Append( "&key=" ).Append( Key );

			if( errorDetail )
				Result.Append( "&ed=true" );

			return Result.ToString();
		}

		public string Url( string queryType, Dictionary<string, string> queryParameters, bool errorDetail = false )
		{
			return Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ), errorDetail );
		}

		public override MapsApi.Directions GetDirections( List<Point> points )
		{
			throw new NotImplementedException();
		}
	}
}