﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MapsApi.GeoLocation;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace Maps.GoogleMapsApi
{
	public class GoogleMapsApiV3 : GoogleMaps
	{
		private const string BASE_URL = "https://maps.googleapis.com/maps/api/";

		private const string
			GEOCODE_QUERY = "geocode",
			DIRECTIONS_QUERY = "directions",
			DEFAULT_TRAVEL_MODE = "DRIVING";

		private readonly int WAYPOINT_LIMIT;

		public GoogleMapsApiV3( RequestContext.RequestContext context ) : base( context )
		{
			WAYPOINT_LIMIT = 10;
		}

		internal override string BaseUrl => BASE_URL;

		public override string ShowMapLink => @"Maps\GoogleIFrame.html";


		public static List<Location> DecodeEncodedPoints( string encodedPoints )
		{
			var Result = new List<Location>();

			if( string.IsNullOrEmpty( encodedPoints ) )
				throw new ArgumentNullException( nameof( DecodeEncodedPoints ) );

//			encodedPoints = encodedPoints.Replace( @"\\", @"\" );

			var PolylineChars = encodedPoints.ToCharArray();
			var Index = 0;

			var CurrentLat = 0;
			var CurrentLng = 0;

			while( Index < PolylineChars.Length )
			{
				// calculate next latitude
				var Sum = 0;
				var Shifter = 0;
				int Next5Bits;

				do
				{
					Next5Bits = PolylineChars[ Index++ ] - 63;
					Sum |= ( Next5Bits & 31 ) << Shifter;
					Shifter += 5;
				} while( Next5Bits >= 32 && Index < PolylineChars.Length );

				if( Index >= PolylineChars.Length )
					break;

				CurrentLat += ( Sum & 1 ) == 1 ? ~( Sum >> 1 ) : Sum >> 1;

				//calculate next longitude
				Sum = 0;
				Shifter = 0;

				do
				{
					Next5Bits = PolylineChars[ Index++ ] - 63;
					Sum |= ( Next5Bits & 31 ) << Shifter;
					Shifter += 5;
				} while( Next5Bits >= 32 && Index < PolylineChars.Length );

				if( Index >= PolylineChars.Length && Next5Bits >= 32 )
					break;

				CurrentLng += ( Sum & 1 ) == 1 ? ~( Sum >> 1 ) : Sum >> 1;

				Result.Add( new Location
				            {
					            lat = (decimal)( Convert.ToDouble( CurrentLat ) / 1E5 ),
					            lng = (decimal)( Convert.ToDouble( CurrentLng ) / 1E5 )
				            } );
			}

			return Result;
		}


		public static string EncodePoints( List<Location> points )
		{
			var Str = new StringBuilder();

			void EncodeDiff( int diff )
			{
				var Shifted = diff << 1;

				if( diff < 0 )
					Shifted = ~Shifted;

				var Rem = Shifted;

				while( Rem >= 0x20 )
				{
					Str.Append( (char)( ( 0x20 | ( Rem & 0x1f ) ) + 63 ) );
					Rem >>= 5;
				}

				Str.Append( (char)( Rem + 63 ) );
			}

			var LastLat = 0;
			var LastLng = 0;

			foreach( var Point in points )
			{
				var Lat = (int)Math.Round( (double)Point.lat * 1E5 );
				var Lng = (int)Math.Round( (double)Point.lng * 1E5 );

				EncodeDiff( Lat - LastLat );
				EncodeDiff( Lng - LastLng );

				LastLat = Lat;
				LastLng = Lng;
			}

			return Str.ToString();
		}

		public static PolyLine CombineEncodedPoints( List<PolyLine> points )
		{
			var Points = new List<Location>();

			foreach( var Point in points )
				Points.AddRange( DecodeEncodedPoints( Point.points ) );

			return new PolyLine
			       {
				       points = EncodePoints( Points )
			       };
		}

		public override MapsApi.Directions GetDirections( List<Point> points )
		{
			var Result = new MapsApi.Directions();

			var Copy = ( from P in points
			             select P ).ToList();

			while( true )
			{
				var Count = Math.Min( Copy.Count, WAYPOINT_LIMIT );

				if( Count <= 1 )
					break;

				var Points = Copy.GetRange( 0, Count );
				var PointsCopy = Copy.GetRange( 0, Count );

				Copy.RemoveRange( 0, Count - 1 ); // Leave last point as start point for next

				var Start = Points[ 0 ];
				var End = Points[ Count - 1 ];
				Points.RemoveAt( Count - 1 );
				Points.RemoveAt( 0 );

				var Arg = new StringBuilder();
				Arg.Append( "origin=" ).Append( Start ).Append( "&destination=" ).Append( End ).Append( "&units=metric" ).Append( "&trafficModel=optimistic" );

				if( Points.Count > 0 )
				{
					Arg.Append( "&waypoints=optimize:false" );

					foreach( var P in Points )
						Arg.Append( '|' ).Append( P );
				}

				RouteResults Data;
				string Response;

				while( true )
				{
					Response = GetResponse( Url( DIRECTIONS_QUERY, Arg.ToString() ) );

					Data = JsonConvert.DeserializeObject<RouteResults>( Response );

					if( Data.status == "OVER_QUERY_LIMIT" )
						Thread.Sleep( 1000 );
					else
						break;
				}

				if( Data.status == "OK" )
				{
					Result.Json.Add( Response );

					foreach( var d in Data.routes )
					{
						var R = new MapsApi.Route();
						Result.Routes.Add( R );

						var Ndx = -1;

						foreach( var l in d.legs )
						{
							++Ndx;

							var L = new MapsApi.Leg
							        {
								        OriginalStartLocation = PointsCopy[ Ndx ],
								        OriginalEndLocation = PointsCopy[ Ndx + 1 ],
								        Distance = new MapsApi.DistanceDuration
								                   {
									                   Text = l.distance.text.Trim(),
									                   Value = l.distance.value / 1000.0 // To Km's
								                   },
								        Duration = new MapsApi.DistanceDuration
								                   {
									                   Text = l.duration.text.Trim(),
									                   Value = l.duration.value / 60.0 // To Mins
								                   },
								        StartAddress = l.start_address.Trim(),
								        StartLocation = new Point
								                        {
									                        Latitude = l.start_location.lat,
									                        Longitude = l.start_location.lng
								                        },
								        EndAddress = l.end_address.Trim(),
								        EndLocation = new Point
								                      {
									                      Latitude = l.end_location.lat,
									                      Longitude = l.end_location.lng
								                      }
							        };

							R.Legs.Add( L );

							// ReSharper disable once LoopCanBePartlyConvertedToQuery
							foreach( var s in l.steps )
							{
								var S = new MapsApi.Step
								        {
									        Distance = new MapsApi.DistanceDuration
									                   {
										                   Text = s.distance.text.Trim(),
										                   Value = s.distance.value / 1000.0
									                   },
									        Duration = new MapsApi.DistanceDuration
									                   {
										                   Text = s.duration.text.Trim(),
										                   Value = s.duration.value / 60.0
									                   },
									        Instructions = s.html_instructions.Trim(),
									        StartLocation = new Point
									                        {
										                        Latitude = s.start_location.lat,
										                        Longitude = s.start_location.lng
									                        },
									        EndLocation = new Point
									                      {
										                      Latitude = s.end_location.lat,
										                      Longitude = s.end_location.lng
									                      }
								        };

								L.Steps.Add( S );
							}
						}
					}
				}
			}

			return Result;
		}

		public override List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited )
		{
			rateLimited = false;
			address.Trim();

			var Arg = new StringBuilder();

			if( address.Street != "" )
			{
				if( address.StreetNumber != "" )
					Arg.Append( address.StreetNumber ).Append( '+' );

				Arg.Append( address.Street );
			}

			if( address.City != "" )
				Arg.Append( ",+" ).Append( address.City );

			if( address.StateCode != "" )
				Arg.Append( ",+" ).Append( address.StateCode );

			if( address.State != "" )
				Arg.Append( ",+" ).Append( address.State );

			if( address.PostalCode != "" && address.PostalCode.Length >= 4 )
				Arg.Append( ",+" ).Append( address.PostalCode );

			if( address.CountryCode != "" )
				Arg.Append( ",+" ).Append( address.CountryCode );

			if( address.Country != "" )
				Arg.Append( ",+" ).Append( address.Country );

			var Args = new Dictionary<string, string> { { "address", Arg.ToString().Replace( ' ', '+' ) } };

			var Response = GetResponse( Url( GEOCODE_QUERY, Args ) );

			var Data = JsonConvert.DeserializeObject<GeocodeResults>( Response );

			var RetVal = new List<MapsApi.AddressCoordinate>();

			if( Data.status == "OK" )
				foreach( var LocationResult in Data.results )
				{
					var Loc = LocationResult.geometry.location;

					var FAddress = LocationResult.formatted_address.Trim();

					var CCount = FAddress.Count( C => C == ',' );

					if( CCount <= 1 ) // Probably only state and country
						return null;

					var Addr = new MapsApi.AddressCoordinate
					           {
						           Latitude = Loc.lat,
						           Longitude = Loc.lng,
						           Address = new MapsApi.AddressData
						                     {
							                     FormattedAddress = FAddress,
							                     RateLimited = false,
							                     Latitude = Loc.lat,
							                     Longitude = Loc.lng
						                     }
					           };

					var A = Addr.Address;

					foreach( var Ac in LocationResult.address_components )
						switch( Ac.types[ 0 ] )
						{
						case "street_number":
							A.StreetNumber = Ac.long_name;

							break;

						case "route":
							A.Street = Ac.long_name;

							break;

						case "locality":
							A.City = Ac.long_name;

							break;

						case "administrative_area_level_3":
							break;

						case "administrative_area_level_2":
							break;

						case "administrative_area_level_1":
							A.State = Ac.long_name;
							A.StateCode = Ac.short_name;

							break;

						case "country":
							A.Country = Ac.long_name;
							A.CountryCode = Ac.short_name;

							break;

						case "postal_code":
							A.PostalCode = Ac.long_name;

							break;
						}

					RetVal.Add( Addr );
				}

			return RetVal;
		}

		// Convert to Javascript format
		public override void ShowDirections( MapsApi.Directions directions )
		{
			// Combine original API results
			var Results = new RouteResults();
			var Status = "";

			if( directions.Routes.Count > 0 )
			{
				foreach( var TempRoute in directions.Json.Select( JsonConvert.DeserializeObject<RouteResults> ) )
				{
					Status = TempRoute.status;

					Results.geocoded_waypoints.AddRange( TempRoute.geocoded_waypoints );

					Results.routes.AddRange( TempRoute.routes );
				}

				// Flatten the Google legs
				var GoogleLegs = new List<Leg>();

				foreach( var R in Results.routes )
					GoogleLegs.AddRange( R.legs );

				JavaScriptRouteResults JResult;

				if( GoogleLegs.Count > 0 )
				{
					// Now flatten my legs
					var MyLegs = new List<MapsApi.Leg>();

					foreach( var R in directions.Routes )
						MyLegs.AddRange( R.Legs );

					//Convert Google addresses to trip addresses
					( from M in MyLegs
					  from G in GoogleLegs
					  where MapsApi.IsCordinateEqual( M.StartLocation.Latitude, G.start_location.lat ) &&
					        MapsApi.IsCordinateEqual( M.StartLocation.Longitude, G.start_location.lng )
					  select new { G, M } ).ToList().ForEach( e =>
					                                          {
						                                          e.G.start_address = e.M.StartAddress;
					                                          } );

					var StartAddress = directions.Routes[ 0 ].Legs[ 0 ].OriginalStartLocation.ToString();
					var EndLegs = directions.Routes[ directions.Routes.Count - 1 ].Legs;
					var EndAddress = EndLegs[ EndLegs.Count - 1 ].OriginalEndLocation.ToString();

					// Convert to JavaScript object
					JResult = new JavaScriptRouteResults
					          {
						          status = Status,
						          geocoded_waypoints = Results.geocoded_waypoints,
						          request = new JavaScriptRequest
						                    {
							                    destination = EndAddress,
							                    origin = StartAddress
						                    }
					          };

					var Polly = Results.routes.Select( R => R.overview_polyline ).ToList();

					var R0 = Results.routes[ 0 ];

					JResult.routes.Add( new JavaScriptRoute
					                    {
						                    copyrights = R0.copyrights,
						                    legs = GoogleLegs,
						                    waypoint_order = new int[ 0 ],
						                    summary = R0.summary,
						                    warnings = R0.warnings,
						                    overview_polyline = CombineEncodedPoints( Polly )
					                    } );
				}
				else
				{
					JResult = new JavaScriptRouteResults();
				}
			#if DEBUG
				var Json = JsonConvert.SerializeObject( JResult, Formatting.Indented );
			#else
				var Json = JsonConvert.SerializeObject( JResult );
			#endif
			}
		}

		public class AddressComponents
		{
			public string
				long_name = null,
				short_name = null;

			public string[] types = null;
		}

		public class Location
		{
			public decimal lat,
			               lng;
		}

		public class ViewPort
		{
			public Location
				northeast = null,
				southwest = null;
		}

		public class Geometry
		{
			public Location location = null;
			public string location_type = null;
			public ViewPort viewport = null;
		}


		public class LocationResult
		{
			public AddressComponents[] address_components = null;
			public string formatted_address = null;
			public Geometry geometry = null;
			public string place_id = null;
			public string[] types = null;
		}


		public class GeocodeResults
		{
			public LocationResult[] results = null;
			public string status = null;
		}


		public class GeocodedWaypoints
		{
			public string
				geocoder_status = null,
				place_id = null;

			public string[] types = null;
		}

		public class DistanceDuration
		{
			public string text = null;
			public int value = 0;
		}

		public class PolyLine
		{
			public string points;
		}

		public class Step
		{
			public DistanceDuration distance = null,
			                        duration = null;

			public Location end_location = null;
			public string html_instructions = null;
			public PolyLine polyline = null;
			public Location start_location = null;
			public string travel_mode = DEFAULT_TRAVEL_MODE;
		}

		public class ViaWaypoint
		{
		}

		public class Leg
		{
			public DistanceDuration distance = null,
			                        duration = null;

			public string end_address = null;
			public Location end_location = null;

			public string start_address;
			public Location start_location = null;

			public List<Step> steps = null;

			public ViaWaypoint[] via_waypoint = null;
		}

		public class Route
		{
			public ViewPort bounds = null;
			public string copyrights = null;
			public List<Leg> legs = null;
			public PolyLine overview_polyline = null;

			public string summary = null;
			public string[] warnings = null;
			public int[] waypoint_order = null;
		}

		public class JavaScriptRoute
		{
			public string copyrights;
			public List<Leg> legs;
			public PolyLine overview_polyline;

			public string summary;
			public string[] warnings;
			public int[] waypoint_order;
		}


		public class RouteResults
		{
			public List<GeocodedWaypoints> geocoded_waypoints = new List<GeocodedWaypoints>();
			public List<Route> routes = new List<Route>();

			public string status = null;
		}

		public class JavaScriptRequest
		{
			public string
				destination,
				origin,
				travelMode = DEFAULT_TRAVEL_MODE;
		}

		public class JavaScriptRouteResults
		{
			public List<GeocodedWaypoints> geocoded_waypoints = new List<GeocodedWaypoints>();

			public JavaScriptRequest request = new JavaScriptRequest();
			public List<JavaScriptRoute> routes = new List<JavaScriptRoute>();
			public string status;
		}
	}
}