﻿using System.Collections.Generic;
using System.Text;
using Interfaces.Interfaces;

namespace Maps.GoogleMapsApi
{
	public abstract class GoogleMaps : AMapsApi
	{
		internal GoogleMaps( RequestContext.RequestContext context )
			: base( context )
		{
		}

		internal abstract string BaseUrl { get; }

		protected override string Key => Context.Configuration.GoogleMapsApiKey;

		public string Url( string queryType, string queryParameters )
		{
			var Result =
				new StringBuilder( BaseUrl ).Append( queryType ).Append( "/json?" ).Append( queryParameters ).Append( "&key=" )
				                            .Append( Key );

			return Result.ToString();
		}

		public string Url( string queryType, Dictionary<string, string> queryParameters )
		{
			return Url( queryType, MapsApi.DictionaryToQueryParameters( queryParameters ) );
		}
	}
}