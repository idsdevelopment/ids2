﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using Maps.BingMapsApi;
using Maps.GoogleMapsApi;
using MapsApi.GeoLocation;
using Utils;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Maps
{
	public abstract class AMapsApi
	{
		public const int DISTANCE_MAX_MONTHS = 1;
		public const int DISTANCE_MAX_MONTHS_LIMIT = 2;

		protected abstract string Key { get; }

		public abstract string ShowMapLink // Required because needed IFrame to show Google Map
		{
			get;
		}

		public readonly RequestContext.RequestContext Context;

		public abstract List<MapsApi.AddressCoordinate> FindLocationByAddress( MapsApi.AddressData address, out bool rateLimited );
		public abstract MapsApi.Directions GetDirections( List<Point> list );
		public abstract void ShowDirections( MapsApi.Directions directions );

		protected static string GetResponse( string url, out WebHeaderCollection headers )
		{
			try
			{
				var Wc = new WebClient();
				var Result = Encoding.ASCII.GetString( Wc.DownloadData( url ) );
				headers = Wc.ResponseHeaders;

				return Result;
			}
			catch
			{
				headers = null;

				return null;
			}
		}

		protected static string GetResponse( string url )
		{
			try
			{
				return GetResponse( url, out _ );
			}
			catch
			{
				return null;
			}
		}

		protected AMapsApi( RequestContext.RequestContext context )
		{
			Context = context;
		}
	}

	public class MapsApi : AMapsApi
	{
		public enum OPTIMISATION_METHOD
		{
			FURTHEST,
			CURCULAR,
			TO_END_POINT
		}

		public class OptimiseEntry
		{
			public double Distance,
			              TotalDistance;

			public object Object;

			public Point Point;

			public OptimiseEntry()
			{
			}

			public OptimiseEntry( OptimiseEntry e )
			{
				Distance = e.Distance;
				TotalDistance = e.TotalDistance;
				Point = e.Point;
				Object = e.Object;
			}
		}

		public abstract class OptimiseBase
		{
			protected readonly RequestContext.RequestContext Context;
			protected List<OptimiseEntry> Points;

			public abstract double CalculateDistance( Point from, Point to );

			protected OptimiseBase( RequestContext.RequestContext context, List<OptimiseEntry> points )
			{
				Context = context;
				Points = points;
			}
		}


		public abstract class OptimiseFurthest : OptimiseBase
		{
			private static readonly object LockObject = new object();


			public List<OptimiseEntry> Optimise()
			{
				double TotalDistance = 0;
				var Result = new List<OptimiseEntry>();

				var Now = DateTime.Now;

				try
				{
					double DoDistance( Point frm, Point to )
					{
						frm.Latitude = Math.Round( frm.Latitude, 6, MidpointRounding.AwayFromZero );
						frm.Longitude = Math.Round( frm.Longitude, 6, MidpointRounding.AwayFromZero );
						to.Latitude = Math.Round( to.Latitude, 6, MidpointRounding.AwayFromZero );
						to.Longitude = Math.Round( to.Longitude, 6, MidpointRounding.AwayFromZero );

						lock( LockObject )
						{
							using( var Db = new CarrierDb( Context ) )
							{
								var Entity = Db.Entity;

								try
								{
									var Rec = ( from D in Entity.Distances
									            where ( D.FromLatitude == frm.Latitude ) && ( D.FromLongitude == frm.Longitude )
									                                                     && ( D.ToLatitude == to.Latitude )
									                                                     && ( D.ToLongitude == to.Longitude )
									            select D ).FirstOrDefault();

									if( Rec == null )
									{
										var Distance = CalculateDistance( frm, to );

										Entity.Distances.Add( new Distance
										                      {
											                      FromLongitude = frm.Longitude,
											                      FromLatitude = frm.Latitude,
											                      ToLongitude = to.Longitude,
											                      ToLatitude = to.Latitude,
											                      DistanceInMeters = (decimal)Distance * 1000,
											                      DateLastAccessed = Now,
											                      DateCreated = Now
										                      } );

										return Distance;
									}

									Rec.DateLastAccessed = DateTime.Now;

									return (double)Rec.DistanceInMeters / 1000;
								}
								finally
								{
									Entity.SaveChanges();
								}
							}
						}
					}

					while( true )
					{
						var Cnt = Points.Count;

						if( Cnt > 1 )
						{
							var CurrentFrom = Points[ 0 ];
							Points.RemoveAt( 0 );

							TotalDistance += CurrentFrom.Distance;
							CurrentFrom.TotalDistance = TotalDistance;

							Result.Add( CurrentFrom );

							var CurrentFromPoint = CurrentFrom.Point;

							Cnt = Points.Count;

							for( var I = 0; I < Cnt; I++ )
							{
								var CurrentTo = Points[ I ];
								CurrentTo.Distance = DoDistance( CurrentFromPoint, CurrentTo.Point );
							}

							Points = OrderByDistance( Points );
						}
						else
						{
							if( Cnt == 1 )
							{
								Cnt = Result.Count;

								if( Cnt > 0 ) // Shouldn't happen (less <= 0) added in pairs
								{
									var Last = Result[ Cnt - 1 ].Point;
									var CurrentTo = Points[ 0 ];

									CurrentTo.Distance = DoDistance( Last, CurrentTo.Point );
									CurrentTo.TotalDistance = TotalDistance + CurrentTo.Distance;
									Result.Add( CurrentTo );
								}
							}

							Points = Result;

							break;
						}
					}
				}
				finally
				{
					Task.Run( () =>
					          {
						          // ReSharper disable once InconsistentlySynchronizedField
						          using( var Db = new CarrierDb( Context ) )
						          {
							          var E = Db.Entity;

							          var Config = ( from C in E.Configurations
							                         select C ).FirstOrDefault();

							          if( Config != null )
							          {
								          var Months = ( Now - Config.LastDistanceCleanup ).TotalDays / 30;

								          if( ( Months >= DISTANCE_MAX_MONTHS ) || ( Months >= DISTANCE_MAX_MONTHS_LIMIT ) )
								          {
									          Config.LastDistanceCleanup = Now;
									          var Dist = E.Distances;

									          Dist.RemoveRange( from D in Dist
									                            let M = Math.Abs( (int)DbFunctions.DiffMonths( Now, D.DateLastAccessed ) )
									                            where ( M >= DISTANCE_MAX_MONTHS ) || ( M >= DISTANCE_MAX_MONTHS_LIMIT )
									                            select D );

									          E.SaveChanges();
								          }
							          }
						          }
					          } );
				}

				return Points;
			}

			protected OptimiseFurthest( RequestContext.RequestContext context, List<OptimiseEntry> points ) : base( context, points )
			{
			}
		}

		public abstract class OptimiseCircular : OptimiseFurthest
		{
			// Giva mapping a reasonable hint on how to optimise
			public new List<OptimiseEntry> Optimise()
			{
				var Result = new List<OptimiseEntry>();

				if( Points.Count > 0 )
				{
					var FurthestEntries = base.Optimise();

					TripletEntry PopEntry()
					{
						if( FurthestEntries.Count > 0 )
						{
							var RetVal = FurthestEntries[ 0 ];
							FurthestEntries.RemoveAt( 0 );

							return new TripletEntry( RetVal );
						}

						return null;
					}

					var TrList = new TripletList();

					// First Break into triplets
					while( FurthestEntries.Count > 0 )
						TrList.Add( new TripletEntry( PopEntry(), PopEntry(), PopEntry() ) );

					var Current = TrList[ 0 ];

					// Join the triplets
					for( int Ndx = 1,
					         C = TrList.Count;
					     Ndx < C;
					     Ndx++ )
					{
						var Next = TrList[ Ndx ];
						Current.Furthest.Closest = Next;
						var Cl = Next.Closest ?? Next;
						Current.Closest.Furthest = Cl;
						Next.Closest = null;
						Current = Next;
					}
				}

				return Result;
			}

			protected OptimiseCircular( RequestContext.RequestContext context, List<OptimiseEntry> points ) : base( context, points )
			{
			}
		}

		public class DistanceDuration
		{
			public string Text;
			public double Value;
		}

		public class Step
		{
			public DistanceDuration Distance,
			                        Duration;

			public Point EndLocation;
			public string Instructions;
			public Point StartLocation;
		}

		public class Leg
		{
			public DistanceDuration Distance,
			                        Duration;

			public string EndAddress;
			public Point EndLocation;

			public int Order; // Used to reorder route

			public Point OriginalStartLocation,
			             OriginalEndLocation;

			public string StartAddress;
			public Point StartLocation;

			public List<Step> Steps = new List<Step>();
		}

		public class Route
		{
			public List<Leg> Legs = new List<Leg>();
		}

		public class Directions
		{
			public List<string> Json = new List<string>();
			public List<Route> Routes = new List<Route>();
		}

		public class AddressData
		{
			public decimal Latitude
			{
				get => Point.Latitude;
				set => Point.Latitude = value;
			}

			public decimal Longitude
			{
				get => Point.Longitude;
				set => Point.Longitude = value;
			}

			public string CompanyName { get; set; }

			public string Suite { get; set; }

			public string StreetNumber { get; set; }

			public string Street { get; set; }

			public string Vicinity { get; set; }

			public string City { get; set; }

			public string State { get; set; }

			public string StateCode { get; set; }

			public string PostalCode { get; set; }

			public string Country { get; set; }

			public string CountryCode { get; set; }

			public string FormattedAddress { get; set; }

			public string MultiLineAddress => DisplayAddress.Replace( ",", "\r\n" );

			public string HtmlMultiLineAddress => DisplayAddress.Replace( ",", "<br>" );

			public string DbLookupKey { get; set; }

			public string DisplayAddress
			{
				get
				{
					Trim();

					var Result = new StringBuilder();

					var HasSuite = Suite != "";

					Result.Append( Suite );

					if( HasSuite )
						Result.Append( '/' );

					Result.Append( StreetNumber );

					if( Street != "" )
						Result.Append( ' ' ).Append( Street );

					if( City != "" )
						Result.Append( ' ' ).Append( City );

					Result.Append( ',' );

					var HaveState = State != "";

					if( HaveState )
						Result.Append( State );

					if( StateCode != "" )
					{
						if( HaveState )
							Result.Append( '(' ).Append( StateCode ).Append( ')' );
						else
							Result.Append( StateCode );
					}

					if( PostalCode != "" )
						Result.Append( ' ' ).Append( PostalCode );

					if( Country != "" )
						Result.Append( ',' ).Append( Country );

					return Util.NormaliseString( Result.ToString().Trim() );
				}
			}

			public bool NotFound { get; set; }

			// Rate limited by maps
			public bool RateLimited { get; set; }
			private bool Trimmed;
			public Point Point = new Point();

			public AddressData Trim()
			{
				if( !Trimmed )
				{
					Trimmed = true;

					CompanyName = CompanyName.Trim();
					Suite = Suite.Trim();
					StreetNumber = StreetNumber.Trim();
					Street = Street.Trim();
					Vicinity = Vicinity.Trim();
					City = City.Trim();
					State = State.Trim();
					StateCode = StateCode.Trim();
					PostalCode = PostalCode.Trim();
					Country = Country.Trim();
					CountryCode = CountryCode.Trim();

					// Has No Spaces
					DbLookupKey = Util.NormaliseString( string.Join( "", new StringBuilder().Append( CompanyName )
					                                                                        .Append( Suite ).Append( StreetNumber )
					                                                                        .Append( Street ).Append( Vicinity ).Append( City )
					                                                                        .Append( State ).Append( StateCode ).Append( PostalCode )
					                                                                        .Append( Country )
					                                                                        .Append( CountryCode )
					                                                                        .ToString()
					                                                                        .ToLower()
					                                                                        .Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) ) );
				}

				return this;
			}

			public AddressData ReTrim()
			{
				Trimmed = false;

				return Trim();
			}

			public AddressData()
			{
				CompanyName =
					Suite = StreetNumber = Street = Vicinity = City = State = StateCode = PostalCode = Country = CountryCode = "";
			}
		}

		public class AddressCoordinate
		{
			public AddressData Address;

			public decimal Latitude,
			               Longitude;
		}

		protected override string Key => throw new NotImplementedException();

		public override string ShowMapLink => new GoogleMapsApiV3( Context ).ShowMapLink;

		private class TripletEntry : OptimiseEntry
		{
			internal TripletEntry Furthest,
			                      Closest;

			internal TripletEntry( OptimiseEntry point, TripletEntry closestEntry, TripletEntry furthestEntry ) : base( point )
			{
				Closest = closestEntry;
				Furthest = furthestEntry;
			}

			internal TripletEntry( OptimiseEntry point ) : base( point )
			{
			}
		}

		private class TripletList : List<TripletEntry>
		{
		}

		public static double StraightLineDistance( OptimiseEntry p1, OptimiseEntry p2 )
		{
			var P1 = p1.Point;
			var P2 = p2.Point;

			var D1 = P2.Latitude - P1.Latitude;
			var D2 = P2.Longitude - P1.Longitude;

			return Math.Abs( Math.Sqrt( (double)( ( D1 * D1 ) + ( D2 * D2 ) ) ) );
		}

		protected static List<OptimiseEntry> OrderByDistance( List<OptimiseEntry> points )
		{
			return ( from P in points
			         orderby P.Distance
			         select P ).ToList();
		}

		// To 11 metres
		public static int IntCordinate( decimal c )
		{
			return (int)( ( ( c * 100000 ) + 5 ) / 10 );
		}

		public static bool IsCordinateEqual( decimal v1, decimal v2 )
		{
			return IntCordinate( v1 ) == IntCordinate( v2 );
		}

		public static bool IsCordinateEqual( Point p1, Point p2 )
		{
			return IsCordinateEqual( p1.Latitude, p2.Latitude ) && IsCordinateEqual( p1.Longitude, p2.Longitude );
		}

		public override List<AddressCoordinate> FindLocationByAddress( AddressData address, out bool rateLimited )
		{
			var Result = new GoogleMapsApiV3( Context ).FindLocationByAddress( address, out rateLimited );

			if( ( Result == null ) || ( Result.Count == 0 ) )
				return new BingMapsV1( Context ).FindLocationByAddress( address, out rateLimited );

			return Result;
		}

		public override Directions GetDirections( List<Point> list )
		{
			var Count = list.Count;

			if( Count >= 2 )
			{
				var SaveStart = list[ 0 ];

				list.RemoveAt( 0 ); // Remove Start

				// Remove duplicates but leave start and end points
				var NewList = ( from L in list
				                where L.Distance != 0
				                group L by new { Latitude = IntCordinate( L.Latitude ), Longitude = IntCordinate( L.Longitude ) }
				                into P
				                select P.First()
				              ).ToList();

				NewList.Insert( 0, SaveStart );

				NewList = ( from L in NewList
				            orderby L.Distance
				            select L ).ToList();

				return new GoogleMapsApiV3( Context ).GetDirections( NewList );
			}

			return new Directions();
		}

		public override void ShowDirections( Directions directions )
		{
			new GoogleMapsApiV3( Context ).ShowDirections( directions );
		}

		public static string DictionaryToQueryParameters( Dictionary<string, string> queryParameters )
		{
			var Params = new StringBuilder();
			var First = true;

			foreach( var Parameter in queryParameters )
			{
				if( !First )
					Params.Append( '&' );
				else
					First = false;

				Params.Append( Uri.EscapeDataString( Parameter.Key ) )
				      .Append( '=' )
				      .Append( Uri.EscapeDataString( Parameter.Value ) );
			}

			return Params.ToString();
		}

		public MapsApi( RequestContext.RequestContext context ) : base( context )
		{
		}
	}
}