﻿using System;
using System.Collections.Concurrent;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Protocol.Data;
using Utils;

namespace Interfaces.Abstracts
{
	// ReSharper disable once InconsistentNaming
	public abstract class AIRequestInterface : IRequestContext
	{
		public static NewContextDelegate NewContext;


		[JsonIgnore]
		public int UserId { get; set; }

		[JsonIgnore]
		public string UserName { get; set; }

		[JsonIgnore]
		public string Password { get; set; }

		[JsonIgnore]
		public string CarrierId { get; set; }

		[JsonIgnore]
		public sbyte TimeZoneOffset { get; set; }

		[JsonIgnore]
		public bool Debug { get; set; }

		[JsonIgnore]
		public bool IsIds { get; set; }

		[JsonIgnore]
		public bool IsNewCarrier { get; set; }

		[JsonIgnore]
		public abstract string IpAddress { get; set; }

		[JsonIgnore]
		public ConcurrentDictionary<string, dynamic> Variables { get; protected set; } = new ConcurrentDictionary<string, dynamic>();

		[JsonIgnore]
		public abstract (string Account, string Key) BlobStorageAccount { get; }

		[JsonIgnore]
		public string UserStorageId => _UserStorageId.IsNotNullOrEmpty() ? _UserStorageId : _UserStorageId = ToStorageId( UserName, UserId );


		[JsonIgnore]
		public abstract string StorageAccount { get; }

		[JsonIgnore]
		public abstract string StorageAccountKey { get; }

		[JsonIgnore]
		public abstract string StorageTableEndpoint { get; }

		[JsonIgnore]
		public abstract string StorageFileEndpoint { get; }

		[JsonIgnore]
		public abstract string StorageBlobEndpoint { get; }

		[JsonIgnore]
		public abstract string ServiceBusPrimaryConnectionString { get; }

		[JsonIgnore]
		public abstract string ServiceBusSecondaryConnectionString { get; }

		[JsonIgnore]
		public abstract string SendGridApiKey { get; }

		private string _UserStorageId;

		public delegate IRequestContext NewContextDelegate( string carrierId, string userName );

		protected static string ToStorageId( string userName, int id )
		{
			return $"{userName.ToAlphaNumeric()}{id:D}";
		}

		public abstract IRequestContext Clone( string carrierId = "" );

		public abstract void SystemLogException( string message );
		public abstract void SystemLogException( Exception ex, string caller = "", string fileName = "", int lineNumber = 0 );

		public abstract void SystemLogException( string message, Exception ex );

		public abstract void LogDifferences( string storageArea, string program, string description, object before, object after );
		public abstract void LogDifferences( string storageArea, string program, string description, object before );

		public abstract void LogDeleted( string storageArea, string program, string description, object value1 );
		public abstract void LogNew( string storageArea, string program, string description, object value1 );

		public abstract string GetUserStorageId( string userName );

		public string GetStorageId( LOG storageId )
		{
			switch( storageId )
			{
			case LOG.LOGIN:
				return "Login";

			case LOG.STAFF:
				return "Staff";

			case LOG.TRIP:
				return "Trip";

			default:
				throw new ArgumentException( "Invalid storageId" );
			}
		}

		public abstract void LogProgramStarted( string program, string description = "" );
		public abstract void LogProgramEnded( string program, string description = "" );
	}
}