﻿using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using Protocol.Data;

namespace Interfaces.Interfaces
{
	public interface IRequestContext
	{
		int UserId { get; set; }

		string UserName { get; set; }
		string Password { get; set; }
		string CarrierId { get; set; }
		sbyte TimeZoneOffset { get; set; }
		bool Debug { get; set; }
		bool IsIds { get; set; }
		bool IsNewCarrier { get; set; }

		string IpAddress { get; set; }

		ConcurrentDictionary<string, dynamic> Variables { get; }

		( string Account, string Key) BlobStorageAccount { get; }

		string UserStorageId { get; }
		string StorageAccount { get; }
		string StorageAccountKey { get; }
		string StorageTableEndpoint { get; }
		string StorageFileEndpoint { get; }
		string StorageBlobEndpoint { get; }

		string ServiceBusPrimaryConnectionString { get; }
		string ServiceBusSecondaryConnectionString { get; }

		string SendGridApiKey { get; }
		IRequestContext Clone( string carrierId = "" );
		void SystemLogException( string message );
		void SystemLogException( Exception ex, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 );
		void SystemLogException( string message, Exception ex );

		void LogProgramStarted( string program, string description = "" );
		void LogProgramEnded( string program, string description = "" );

		void LogDifferences( string storageArea, string program, string description, object before, object after );
		void LogDifferences( string storageArea, string program, string description, object before );

		void LogDeleted( string storageArea, string program, string description, object value1 );
		void LogNew( string storageArea, string program, string description, object value1 );

		string GetUserStorageId( string userName );
		string GetStorageId( LOG storage );
	}
}