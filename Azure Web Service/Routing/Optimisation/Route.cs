﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using MapsApi.GeoLocation;
using Protocol.Data;
using RequestContext;
using Company = Protocol.Data.Company;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace RoutingModule.Optimisation
{
	public class Routing
	{
		private readonly OptimisedList _Route;
		private readonly RequestContext.RequestContext Context;

		public string FileName;

		public List<string> OriginalJson = new List<string>();

		public Routing( RequestContext.RequestContext context )
		{
			Context = context;

			_Route = new OptimisedList( context )
			         {
				         Route = new List<OptimisedEntry>()
			         };
		}

		public Routing( RequestContext.RequestContext context, List<OptimisedEntry> oList ) : this( context )
		{
			_Route.Route = oList;
		}

		public List<OptimisedEntry> Route
		{
			get => _Route.Route;
			set => _Route.Route = value;
		}

		public void Optimise( Maps.MapsApi.OPTIMISATION_METHOD method )
		{
			_Route.Optimise( method );
		}

		public void Add( RouteSegemnt segment )
		{
			_Route.Add( segment );
		}

		public void AddSingle( RouteSegemnt segment )
		{
			_Route.AddSingle( segment );
		}

		public void RemoveNotFound()
		{
			Route = ( from Oe in Route
			          let P = Oe.Point
			          where ( Oe.WorkingAddress.NotFound == false ) && ( ( P.Latitude != 0 ) || ( P.Longitude != 0 ) )
			          select Oe ).ToList();
		}

		public Maps.MapsApi.Directions ToDirections()
		{
			var Retval = new Maps.MapsApi.Directions
			             {
				             Json = OriginalJson
			             };

			Retval.Routes.Add( new Maps.MapsApi.Route() );
			var Legs = Retval.Routes[ 0 ].Legs;

			for( int I = 0,
			         C = Route.Count - 1;
			     I < C;
			     I++ )
			{
				var From = Route[ I ];
				var To = Route[ I + 1 ];

				Legs.Add( new Maps.MapsApi.Leg
				          {
					          OriginalStartLocation = From.Point,
					          OriginalEndLocation = To.Point,
					          StartAddress = From.WorkingAddress.FormattedAddress,
					          EndAddress = To.WorkingAddress.FormattedAddress,
					          StartLocation = From.WorkingAddress.Point,
					          EndLocation = To.WorkingAddress.Point
				          } );
			}

			return Retval;
		}

		private static string GetStreetNumber( ref string addr )
		{
			var Num = new StringBuilder();
			addr = addr.Trim();
			var L = addr.Length;

			if( ( L > 0 ) && ( addr[ 0 ] >= '0' ) && ( addr[ 0 ] <= '9' ) )
				for( var I = 0; I < L; I++ )
				{
					var C = addr[ I ];

					if( C == ' ' )
						break;
					Num.Append( C );
				}

			var Number = Num.ToString();
			addr = addr.Substring( Number.Length ).Trim();

			return Number;
		}

		internal static Maps.MapsApi.AddressData ToAddressData( Company company )
		{
			var Addr = company.AddressLine1;

			return new Maps.MapsApi.AddressData
			       {
				       City = company.City,
				       CompanyName = company.CompanyName,
				       Country = company.Country,
				       CountryCode = company.CountryCode,
				       Longitude = company.Longitude,
				       Latitude = company.Latitude,
				       PostalCode = company.PostalCode,
				       State = company.Region,
				       StateCode = "",
				       StreetNumber = GetStreetNumber( ref Addr ), // Must be before street
				       Street = ( Addr + ' ' + company.AddressLine2 ).Trim(),
				       Suite = company.Suite,
				       Vicinity = ""
			       };
		}

		internal static Maps.MapsApi.AddressData ToAddressData( UserMapAddress a )
		{
			var Addr = a.AddressLine1;

			return new Maps.MapsApi.AddressData
			       {
				       Longitude = a.Longitude,
				       Latitude = a.Lattitude,
				       CompanyName = a.CompanyName,
				       PostalCode = a.PostalCode,
				       Suite = a.Suite,
				       City = a.City,
				       Country = a.Country,
				       CountryCode = "",
				       State = a.Region,
				       StateCode = "",
				       StreetNumber = GetStreetNumber( ref Addr ), // Must be before street
				       Street = ( Addr + ' ' + a.AddressLine2 ).Trim(),
				       Vicinity = ""
			       };
		}

		internal static UserMapAddress ToMapAddress( Company companyAddress, Maps.MapsApi.AddressCoordinate a )
		{
			return new UserMapAddress
			       {
				       Longitude = a.Longitude,
				       Lattitude = a.Latitude,
				       CompanyName = companyAddress.CompanyName.Trim().ToUpper(),
				       AddressLine1 = companyAddress.AddressLine1.Trim().ToUpper(),
				       AddressLine2 = companyAddress.AddressLine2.Trim().ToUpper(),
				       Country = companyAddress.Country.Trim().ToUpper(),
				       City = companyAddress.City.Trim().ToUpper(),
				       PostalCode = companyAddress.PostalCode.Trim().ToUpper(),
				       Region = companyAddress.Region.Trim().ToUpper(),
				       Suite = companyAddress.Suite.Trim().ToUpper(),
				       DateLastAccessed = DateTime.UtcNow
			       };
		}

		public static List<UserMapAddress> GetAddresses( RequestContext.RequestContext context, Companies companies )
		{
			var TotalAccess = 0;
			var ACount = 0;
			var Recs = new List<UserMapAddress>();

			try
			{
				var Maps = new Maps.MapsApi( context );

				var NewRecs = new List<UserMapAddress>();

				using( var Db = new CarrierDb( context ) )
				{
					var Modified = false;

					var MapAddresses = Db.Entity.UserMapAddresses;

					foreach( var Addr in companies )
					{
						var HaveAddress = false;

						// Check local Db first
						if( ( Addr.Longitude != 0 ) && ( Addr.Latitude != 0 ) )
						{
							Recs = ( from A in MapAddresses
							         where ( A.Longitude == Addr.Longitude ) && ( A.Lattitude == Addr.Latitude )
							         select A ).ToList();

							if( Recs.Count > 0 )
							{
								foreach( var Rec in Recs )
								{
									Rec.DateLastAccessed = DateTime.UtcNow; // Changed last accessed for deleteions
									Modified = true;
								}

								HaveAddress = true;
							}
						}

						if( !HaveAddress )
						{
							Addr.Suite = Addr.Suite.Trim().ToUpper();
							Addr.AddressLine1 = Addr.AddressLine1.Trim().ToUpper();
							Addr.City = Addr.City.Trim().ToUpper();
							Addr.Region = Addr.Region.Trim().ToUpper();
							Addr.Country = Addr.Country.Trim().ToUpper();

							var Rec = ( from A in MapAddresses
							            where A.AddressLine1.Contains( Addr.AddressLine1 )
							                  && ( A.City == Addr.City )
							                  && ( A.Region == Addr.Region )
							                  && ( A.Country == Addr.Country )
							            select A ).FirstOrDefault();

							if( Rec != null )
							{
								Recs.Add( Rec );

								Addr.Latitude = Rec.Lattitude;
								Addr.Longitude = Rec.Longitude;

								var Now1 = DateTime.UtcNow;

								TotalAccess += ( Now1 - Rec.DateLastAccessed ).Minutes;
								ACount++;
								Rec.DateLastAccessed = Now1;
								Modified = true;
								HaveAddress = true;
							}
						}

						if( !HaveAddress )
						{
							List<Maps.MapsApi.AddressCoordinate> MapAddress;

							while( true )
							{
								MapAddress = Maps.FindLocationByAddress( ToAddressData( Addr ), out var RateLimited );

								if( !RateLimited )
									break;
								Thread.Sleep( 100 );
							}

							if( MapAddress.Count > 0 )
							{
								Modified = true;
								var R = ToMapAddress( Addr, MapAddress[ 0 ] );
								NewRecs.Add( R );
							}
						}
					}

					if( Modified )
					{
						MapAddresses.AddRange( NewRecs );

						Db.Entity.SaveChanges();

						Recs.AddRange( NewRecs );
					}
				}

				var Config = context.Configuration;
				var Now = DateTime.UtcNow;
				var DoCleanup = ( Now - DateTime.UtcNow ).Minutes > Config.AddressCacheCleanUpIntervalInMinutes;

				if( DoCleanup )
					Config.LastAddressCleanup = Now;

				if( ACount > 0 )
					Config.AverageAccessInterval = ( Config.AverageAccessInterval + ( TotalAccess / ACount ) ) / 2;

				context.Configuration = Config;

				if( DoCleanup )
					Task.Run( () =>
					          {
						          try
						          {
							          using( var Db = new CarrierDb( context ) )
							          {
								          var MapAddresses = Db.Entity.UserMapAddresses;
								          TotalAccess = Config.AverageAccessInterval * 2;

								          var CleanupRecs = ( from R in MapAddresses
								                              where Math.Abs( (int)DbFunctions.DiffMinutes( Now, R.DateLastAccessed ) ) > TotalAccess
								                              select R ).ToList();

								          MapAddresses.RemoveRange( CleanupRecs );
								          Db.Entity.SaveChanges();
							          }
						          }
						          catch( Exception Exception )
						          {
							          context.SystemLogException( $"{nameof( GetAddresses )} - Task", Exception );
						          }
					          } );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( nameof( GetAddresses ), Exception );
			} // Do Cleanups

			return Recs;
		}

		public Distances GetDistances( RequestContext.RequestContext context, RouteOptimisationCompanyAddresses companyAddresses )
		{
			var Result = new Distances();

			var Recs = GetAddresses( context, companyAddresses );

			var L = Recs.Count;

			if( L >= 2 ) // Need at least 2 addresses
				if( !companyAddresses.Optimise )
				{
					L -= 1;

					for( var I = 0; I < L; I++ )
					{
						var A1 = Recs[ I ];
						var A2 = Recs[ I + 1 ];
						Route = new List<OptimisedEntry>();
						Add( new RouteSegemnt { From = ToAddressData( A1 ), To = ToAddressData( A2 ) } );
						Optimise( Maps.MapsApi.OPTIMISATION_METHOD.FURTHEST );
						Result.Add( Route[ 1 ].Distance );
					}
				}

			return Result;
		}

		public class RouteSegemnt
		{
			public string TripId { get; set; }

			public string AccountId { get; set; }

			public Maps.MapsApi.AddressData Account { get; set; }

			public Maps.MapsApi.AddressData From { get; set; }

			public Maps.MapsApi.AddressData To { get; set; }

			public string Reference { get; set; }

			public decimal Pieces { get; set; }

			public decimal Weight { get; set; }

			public string Notes { get; set; }
		}

		public class OptimisedEntry
		{
			public double Distance { get; set; }

			public double TotalDistance { get; set; }

			public double MapDistance { get; set; }

			public double MapTotalDistance { get; set; }

			public double MapDuration { get; set; }

			public double MapTotalDuration { get; set; }

			public int Order { get; set; }

			public RouteSegemnt Segment { get; set; }

			public Maps.MapsApi.AddressData WorkingAddress { get; set; }

			public Point Point { get; internal set; }
		}

		public class OptimisedList
		{
			private readonly RequestContext.RequestContext Context;

			public OptimisedList( RequestContext.RequestContext context )
			{
				Context = context;
				Route = new List<OptimisedEntry>();
			}

			public OptimisedList( RequestContext.RequestContext context, List<OptimisedEntry> oList )
			{
				Context = context;
				Route = oList;
			}

			public int Count => Route.Count;

			public List<OptimisedEntry> Route { get; protected internal set; }

			public OptimisedEntry this[ int ndx ] => Route[ ndx ];

			public void UpdateFromDirections( Maps.MapsApi.Directions directions )
			{
				var Cnt = Count;

				if( Cnt > 0 )
				{
					// Flatten Leg list
					var Legs = new List<Maps.MapsApi.Leg>();

					foreach( var R in directions.Routes )
						Legs.AddRange( R.Legs );

					// Start Address Is always 0
					for( int I = 0,
					         C = Legs.Count;
					     I < C;
					     I++ )
						Legs[ I ].Order = I + 1;

					// Make sure starting address appears
					Route[ 0 ].Point.Locked = true;

					// Group same cordinates
					var Rte = ( from R in Route
					            from L in Legs
					            let P = R.Point
					            where P.Locked || Maps.MapsApi.IsCordinateEqual( P, L.OriginalEndLocation )
					            group R by new { P.Latitude, P.Longitude }
					            into G
					            select G.First() ).ToList();

					( from Trip in Rte
					  from Leg in Legs
					  where Maps.MapsApi.IsCordinateEqual( Leg.OriginalEndLocation, Trip.WorkingAddress.Point )
					  select new { Leg, Trip } ).ToList().ForEach( e =>
					                                               {
						                                               var T = e.Trip;
						                                               var L = e.Leg;

						                                               T.MapDistance = L.Distance.Value;
						                                               T.MapDuration = L.Duration.Value;

						                                               if( !T.Point.Locked )
							                                               T.Order = L.Order;
					                                               } );

					Route = ( from R in Rte
					          orderby R.Order
					          select R ).ToList();

					double TotalDistance = 0,
					       TotalDuration = 0;

					foreach( var R in Route )
					{
						TotalDistance += R.MapDistance;
						R.MapTotalDistance = TotalDistance;

						TotalDuration += R.MapDuration;
						R.MapTotalDuration = TotalDuration;
					}
				}
			}

			public void Add( RouteSegemnt segment )
			{
				var F = segment.From;

				Route.Add( new OptimisedEntry
				           {
					           Segment = segment,
					           WorkingAddress = F,
					           Point = F.Point
				           } );

				var T = segment.To;

				Route.Add( new OptimisedEntry
				           {
					           Segment = segment,
					           WorkingAddress = T,
					           Point = T.Point
				           } );
			}

			public void AddSingle( RouteSegemnt segment )
			{
				Route.Add( new OptimisedEntry
				           {
					           Segment = segment
				           } );
			}

			public void OrderByDistance()
			{
				var C = Count;

				if( C > 0 )
					Route = ( from Trip in Route
					          orderby Trip.Distance
					          select Trip ).ToList();
			}

			private void OFurthest()
			{
				var NewRoute = new RouteOptimiseFurthest( Context, Route ).Optimise();
				Route = new List<OptimisedEntry>();

				foreach( var R in NewRoute )
				{
					var O = (OptimisedEntry)R.Object;
					O.Distance = R.Distance;
					O.TotalDistance = R.TotalDistance;
					Route.Add( O );
				}
			}

			public void Optimise( Maps.MapsApi.OPTIMISATION_METHOD method )
			{
				switch( method )
				{
				case Maps.MapsApi.OPTIMISATION_METHOD.FURTHEST:
					OFurthest();

					break;
				}
			}

			private class RouteOptimiseFurthest : Maps.MapsApi.OptimiseFurthest
			{
				public RouteOptimiseFurthest( RequestContext.RequestContext context, IEnumerable<OptimisedEntry> points )
					: base( context, ToPoints( points ) )
				{
				}

				private static List<Maps.MapsApi.OptimiseEntry> ToPoints( IEnumerable<OptimisedEntry> points )
				{
					return points.Select( p => new Maps.MapsApi.OptimiseEntry
					                           {
						                           Object = p,
						                           Point = p.Point
					                           } ).ToList();
				}

				public override double CalculateDistance( Point from, Point to )
				{
					return new Points { From = from, To = to }.DistanceTo();
				}
			}
		}
	}
}