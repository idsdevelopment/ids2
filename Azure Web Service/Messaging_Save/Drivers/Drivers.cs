﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;
using Protocol.Data;
using Utils;
using Message = Microsoft.Azure.ServiceBus.Message;

namespace Messaging.Drivers
{
	public class Driver
	{
		private static readonly Dictionary<string, CacheEntry> ConnectionCache = new Dictionary<string, CacheEntry>();

		public string Id { get; }
		public bool Connected { get; }
		private readonly RequestContext.RequestContext Context;
		private const int CONNECTION_CACHE_PERIOD = 60_000;

		private class CacheEntry : Timer
		{
			internal readonly string Id;
			internal QueueClient Client;

			internal CacheEntry( string id ) : base( CONNECTION_CACHE_PERIOD )
			{
				Id = id;
			}

			internal CacheEntry( string id, QueueClient client ) : this( id )
			{
				Client = client;
			}
		}

		private static QueueDescription DriverQueueDescription( string queueName )
		{
			return new QueueDescription( queueName )
			       {
				       // The duration of a peek lock; that is, the amount of time that a message is locked from other receivers.
				       LockDuration = TimeSpan.FromSeconds( 5 ),

				       // Size of the Queue. For non-partitioned entity, this would be the size of the queue. 
				       // For partitioned entity, this would be the size of each partition.
				       //The property MaxSizeInMegabytes, must be one of the following values: 1024;2048;3072;4096;5120

				       MaxSizeInMB = 1024,

				       // This value indicates if the queue requires guard against duplicate messages. 
				       // Find out more in DuplicateDetection sample
				       RequiresDuplicateDetection = false,

				       //Since RequiresDuplicateDetection is false, the following need not be specified and will be ignored.
				       DuplicateDetectionHistoryTimeWindow = TimeSpan.FromMinutes( 2 ),

				       // This indicates whether the queue supports the concept of session.
				       // Find out more in "Session and Workflow Management Features" sample
				       RequiresSession = false,

				       // The default time to live value for the messages
				       // Find out more in "TimeToLive" sample.
				       DefaultMessageTimeToLive = TimeSpan.FromDays( 3 ),

				       // Duration of idle interval after which the queue is automatically deleted. 
				       AutoDeleteOnIdle = TimeSpan.FromDays( 60 ),

				       // Decides whether an expired message due to TTL should be dead-letterd
				       // Find out more in "TimeToLive" sample.
				       EnableDeadLetteringOnMessageExpiration = false,

				       // The maximum delivery count of a message before it is dead-lettered
				       // Find out more in "DeadletterQueue" sample
				       MaxDeliveryCount = 10,

				       // Creating only one partition. 
				       // Find out more in PartitionedQueues sample.
				       EnablePartitioning = false
			       };
		}

		private QueueClient GetClient()
		{
			QueueClient Client = null;

			lock( ConnectionCache )
			{
				if( ConnectionCache.TryGetValue( Id, out var Entry ) )
				{
					if( !( Entry.Client is null ) )
						Client = Entry.Client;
				}
				else
					throw new Exception( "Driver cache entry is missing." );
			}

			if( Client is null )
				throw new Exception( "Driver Client is null." );

			return Client;
		}

		public void Send( AMessage message )
		{
			try
			{
				var Message = new Message( Encoding.UTF8.GetBytes( message.ToJson ) );
				GetClient().SendAsync( Message ).Wait();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void Send( Trip trip )
		{
			Send( new MTrip( trip ) );
		}

		public Driver( RequestContext.RequestContext context, string driverId = "" )
		{
			try
			{
				Context = context;
				var IsServer = driverId.IsNotNullOrWhiteSpace();
				var Driver = IsServer ? driverId : context.UserName;
				var Customer = context.CarrierId;
				var Messaging = new ServiceBus.Messaging( context );
				Id = $"{Customer}_driver_{Driver}";

				CacheEntry Entry;

				lock( ConnectionCache )
				{
					if( ConnectionCache.TryGetValue( Id, out Entry ) )
					{
						Entry.Stop();
						Entry.Start();
						Connected = true;
					}
				}

				if( !Connected )
					Connected = Messaging.CreateQueueAsync( DriverQueueDescription( Id ) ).ConfigureAwait( false ).GetAwaiter().GetResult();

				if( Connected && ( Entry is null || ( IsServer && Entry.Client is null ) ) )
				{
					QueueClient Client;
					if( IsServer )
					{
						var Pk = context.ServiceBusPrimaryConnectionString;
						Client = new QueueClient( Pk, Id );
					}
					else
						Client = null;

					lock( ConnectionCache )
					{
						if( ConnectionCache.TryGetValue( Id, out Entry ) )
						{
							var C = Entry.Client;
							C?.CloseAsync().Wait();

							Entry.Client = Client;
						}
						else
						{
							ConnectionCache.Add( Id, Entry = IsServer ? new CacheEntry( Id, Client ) : new CacheEntry( Id ) );
							Entry.Elapsed += ( sender, args ) =>
							                 {
								                 if( sender is CacheEntry E )
								                 {
									                 lock( ConnectionCache )
									                 {
										                 ConnectionCache.Remove( E.Id );
										                 E.Dispose();
									                 }
								                 }
							                 };
						}

						Entry.Stop();
						Entry.Start();
					}
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}
	}
}