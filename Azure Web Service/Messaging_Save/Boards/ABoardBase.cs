﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;
using Protocol.Data;
using Utils;
using Message = Microsoft.Azure.ServiceBus.Message;

namespace Messaging.Boards
{
	public abstract class ABoardBase
	{
		private static readonly Dictionary<string, CacheEntry> ConnectionCache = new Dictionary<string, CacheEntry>();

		public string Id { get; }
		public bool Connected { get; }

		public string SubscriptionId => _SubscriptionId.IsNullOrWhiteSpace() ? _SubscriptionId = $"{Context.UserName}_{Guid.NewGuid()}" : _SubscriptionId;


		private readonly RequestContext.RequestContext Context;

		private readonly string Board;

		private class CacheEntry : Timer
		{
			private const int CONNECTION_CACHE_PERIOD = 60_000;

			private CacheEntry( string id ) : base( CONNECTION_CACHE_PERIOD )
			{
				Id = id;
			}

			internal readonly string Id;
			internal TopicClient Client;

			internal CacheEntry( string id, TopicClient client ) : this( id )
			{
				Client = client;
			}
		}

		private string _SubscriptionId;

		protected abstract ABoardBase CreateInstanceOfSelf( RequestContext.RequestContext context, bool isServer );

		public void Send( AMessage message )
		{
			try
			{
				var Message = new Message( Encoding.UTF8.GetBytes( message.ToJson ) );
				GetClient().SendAsync( Message ).Wait();

				if( Board.IsNotNullOrEmpty() ) // Also send to Global Board
					CreateInstanceOfSelf( Context, true ).Send( message );
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}

		public void Send( Trip trip )
		{
			Send( new MTrip( trip ) );
		}

		public void Send( TripStatusUpdate tUpdate )
		{
			Send( new MTripStatusUpdate( tUpdate ) );
		}

		protected ABoardBase( RequestContext.RequestContext context, string boardName, bool isServer, string board = "" )
		{
			try
			{
				Context = context;
				Board = board;

				var Customer = context.CarrierId;
				var Messaging = new ServiceBus.Messaging( context );
				Id = $"{Customer}_{boardName}_{board}";

				CacheEntry Entry;

				lock( ConnectionCache )
				{
					if( ConnectionCache.TryGetValue( Id, out Entry ) )
					{
						Entry.Stop();
						Entry.Start();
						Connected = true;
					}
				}

				if( !Connected )
					Connected = Messaging.CreateTopicAsync( DispatcherTopicDescription( Id ) ).Result;

				if( !isServer && Connected )
					Connected = Messaging.CreateSubscription( DispatcherSubscriptionDescription( Id, SubscriptionId ) ).Result;

				if( Connected && Entry is null ) // New Connection
				{
					var Pk = context.ServiceBusPrimaryConnectionString;
					var Client = new TopicClient( Pk, Id );

					lock( ConnectionCache )
					{
						if( ConnectionCache.TryGetValue( Id, out Entry ) )
						{
							var C = Entry.Client;
							C?.CloseAsync().Wait();

							Entry.Client = Client;
						}
						else
						{
							ConnectionCache.Add( Id, Entry = new CacheEntry( Id, Client ) );
							Entry.Elapsed += ( sender, args ) =>
							                 {
								                 if( sender is CacheEntry E )
								                 {
									                 lock( ConnectionCache )
									                 {
										                 ConnectionCache.Remove( E.Id );
										                 E.Dispose();
									                 }
								                 }
							                 };
						}

						Entry.Stop();
						Entry.Start();
					}
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}

		private TopicClient GetClient()
		{
			TopicClient Client = null;

			lock( ConnectionCache )
			{
				if( ConnectionCache.TryGetValue( Id, out var Entry ) )
				{
					if( !( Entry.Client is null ) )
						Client = Entry.Client;
				}
				else
					throw new Exception( "Dispatcher cache entry is missing." );
			}

			if( Client is null )
				throw new Exception( "Dispatcher Client is null." );

			return Client;
		}

		private static TopicDescription DispatcherTopicDescription( string topicName )
		{
			return new TopicDescription( topicName )
			       {
				       // Size of the Queue. For non-partitioned entity, this would be the size of the queue. 
				       // For partitioned entity, this would be the size of each partition.
				       //The property MaxSizeInMegabytes, must be one of the following values: 1024;2048;3072;4096;5120
				       MaxSizeInMB = 1024,

				       // This value indicates if the queue requires guard against duplicate messages. 
				       // Find out more in DuplicateDetection sample
				       RequiresDuplicateDetection = false,

				       //Since RequiresDuplicateDetection is false, the following need not be specified and will be ignored.
				       DuplicateDetectionHistoryTimeWindow = TimeSpan.FromMinutes( 2 ),

				       // The default time to live value for the messages
				       // Find out more in "TimeToLive" sample.
				       DefaultMessageTimeToLive = TimeSpan.FromDays( 3 ),

				       // Duration of idle interval after which the queue is automatically deleted. 
				       AutoDeleteOnIdle = TimeSpan.FromDays( 60 ),

				       // Creating only one partition. 
				       // Find out more in PartitionedQueues sample.
				       EnablePartitioning = false,

				       SupportOrdering = true
			       };
		}

		private static SubscriptionDescription DispatcherSubscriptionDescription( string topicName, string subscriptionName )
		{
			return new SubscriptionDescription( topicName, subscriptionName )
			       {
				       // The default time to live value for the messages
				       // Find out more in "TimeToLive" sample.
				       DefaultMessageTimeToLive = TimeSpan.FromHours( 1 ),

				       // Duration of idle interval after which the queue is automatically deleted. 
				       AutoDeleteOnIdle = TimeSpan.FromDays( 1 ),

				       LockDuration = TimeSpan.FromSeconds( 10 ),
				       MaxDeliveryCount = 10
			       };
		}
	}
}