﻿namespace Messaging.Boards
{
	public class DispatchBoard : ABoardBase
	{
		protected override ABoardBase CreateInstanceOfSelf( RequestContext.RequestContext context, bool isServer )
		{
			return new DispatchBoard( context, isServer );
		}

		public DispatchBoard( RequestContext.RequestContext context, bool isServer, string board = "" ) : base( context, "dispatchers", isServer, board )
		{
		}
	}
}