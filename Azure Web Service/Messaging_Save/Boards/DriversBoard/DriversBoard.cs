﻿namespace Messaging.Boards
{
	public class DriversBoard : ABoardBase
	{
		protected override ABoardBase CreateInstanceOfSelf( RequestContext.RequestContext context, bool isServer )
		{
			return new DriversBoard( context, isServer );
		}

		public DriversBoard( RequestContext.RequestContext context, bool isServer, string board = "" ) : base( context, "drivers", isServer, board )
		{
		}
	}
}