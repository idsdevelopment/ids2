﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus.Management;
using Utils;

namespace ServiceBus
{
	public class Messaging
	{
		private ManagementClient _ManagementClient;
		private readonly RequestContext.RequestContext Context;
		private readonly string PrimaryConnectionString;

		public Messaging( RequestContext.RequestContext context )
		{
			Context = context;
			PrimaryConnectionString = context.ServiceBusPrimaryConnectionString;
		}

		private void Error( Exception e, [CallerMemberName] string name = "" )
		{
			Context.SystemLogException( $"Messaging - {name}", e );
		}

		public static string ValidName( string name )
		{
			var Result = new StringBuilder( name.ToLower() );
			var Count = Result.Length;
			for( var I = 0; I < Count; I++ )
			{
				var C = Result[ I ];
				switch( C )
				{
				case '_':
				case '-':
					continue;

				default:
					if( ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( ( C >= '0' ) && ( C <= '9' ) ) )
						continue;

					Result[ I ] = '_';

					break;
				}
			}

			return Result.ToString();
		}

		private ManagementClient ManagementClient => _ManagementClient ?? ( _ManagementClient = new ManagementClient( PrimaryConnectionString ) );


		/// <summary>
		///     Retrieve a queue
		/// </summary>
		private async Task<(QueueDescription Queue, bool Ok)> GetQueueAsync( string queueName )
		{
			try
			{
				var Queue = await ManagementClient.GetQueueAsync( ValidName( queueName ) ).ConfigureAwait( false );

				return ( Queue: Queue, Ok: !( Queue is null ) );
			}
			catch
			{
				return ( Queue: null, Ok: false );
			}
		}


		// Note - the following properties cannot be updated once created -
		// - Path
		// - RequiresSession
		// - EnablePartitioning
		private async Task<bool> UpdateQueueAsync( QueueDescription queueDescription )
		{
			for( var I = 10;; )
			{
				try
				{
					var Result = await ManagementClient.UpdateQueueAsync( queueDescription ).ConfigureAwait( false );

					return !( Result is null );
				}
				catch( Exception Ex )
				{
					if( --I >= 0 )
					{
						Thread.Sleep( 1_000 );

						continue;
					}

					Error( Ex );

					break;
				}
			}

			return false;
		}

		/// <summary>
		///     Creates a new Queue using the managementClient with the name provided.
		/// </summary>
		public async Task<bool> CreateQueueAsync( QueueDescription queueDescription )
		{
			queueDescription.Path = ValidName( queueDescription.Path );

			var (Queue, Ok) = await GetQueueAsync( queueDescription.Path ).ConfigureAwait( false );
			if( Ok )
			{
				queueDescription.Path = Queue.Path;

				return await UpdateQueueAsync( queueDescription ).ConfigureAwait( false );
			}

			try
			{
				await ManagementClient.CreateQueueAsync( queueDescription ).ConfigureAwait( false );

				return true;
			}
			catch( Exception Ex )
			{
				Error( Ex );
			}

			return false;
		}

		/// <summary>
		///     Retrieve a topic
		/// </summary>
		private async Task<(TopicDescription Topic, bool Ok)> GetTopicAsync( string topicName )
		{
			try
			{
				var TopicDescription = await ManagementClient.GetTopicAsync( ValidName( topicName ) ).ConfigureAwait( false );

				return ( Topic: TopicDescription, Ok: !( TopicDescription is null ) );
			}
			catch
			{
				return ( Topic: null, Ok: false );
			}
		}


		// Note - the following properties cannot be updated once created -
		// - Path
		// - RequiresSession
		// - EnablePartitioning
		private async Task<bool> UpdateTopicAsync( TopicDescription topicDescription )
		{
			try
			{
				var Result = await ManagementClient.UpdateTopicAsync( topicDescription ).ConfigureAwait( false );

				return !Result.IsNull();
			}
			catch( Exception Ex )
			{
				Error( Ex );
			}

			return false;
		}

		public async Task<bool> CreateTopicAsync( TopicDescription topicDescription )
		{
			topicDescription.Path = ValidName( topicDescription.Path );

			var (Topic, Ok) = await GetTopicAsync( topicDescription.Path ).ConfigureAwait( false );
			if( Ok )
			{
				topicDescription.Path = Topic.Path;

				return await UpdateTopicAsync( topicDescription ).ConfigureAwait( false );
			}

			try
			{
				await ManagementClient.CreateTopicAsync( topicDescription ).ConfigureAwait( false );

				return true;
			}
			catch( Exception Ex )
			{
				Error( Ex );
			}

			return false;
		}

		private async Task<(SubscriptionDescription SubscriptionDescription, bool Ok)> UpdateSubscriptionAsync( SubscriptionDescription subscriptionDescription )
		{
			try
			{
				var Result = await ManagementClient.UpdateSubscriptionAsync( subscriptionDescription ).ConfigureAwait( false );

				return ( Result, !Result.IsNull() );
			}
			catch( Exception Exception )
			{
				Error( Exception );
			}

			return ( null, false );
		}

		public async Task<bool> CreateSubscription( SubscriptionDescription subscriptionDescription )
		{
			subscriptionDescription.SubscriptionName = ValidName( subscriptionDescription.SubscriptionName );
			subscriptionDescription.TopicPath = ValidName( subscriptionDescription.TopicPath );

			try
			{
				var Result = await ManagementClient.SubscriptionExistsAsync( subscriptionDescription.TopicPath, subscriptionDescription.SubscriptionName ).ConfigureAwait( false );
				if( !Result )
				{
					var Tmp1 = ManagementClient.CreateSubscriptionAsync( subscriptionDescription ).ConfigureAwait( false );

					return !Tmp1.IsNull();
				}

				var Tmp2 = await UpdateSubscriptionAsync( subscriptionDescription );

				return Tmp2.Ok;
			}
			catch( Exception Exception )
			{
				Error( Exception );
			}

			return false;
		}
	}
}