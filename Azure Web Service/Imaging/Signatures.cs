﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Protocol.Data;

namespace Imaging
{
	public static class Signatures
	{
		private const int PADDING = 3;
		private const int VERTICAL_SPACING = 20;

		private const int TEXT_HEIGHT = 30;
		private const int TEXT_PADDING = 5;

		public static string ToPngBase64( this List<Signature> sigs, int textHeight, int textPadding, int verticalSpacing, int padding )
		{
			using var Image = sigs.ToPng( textHeight, textPadding, verticalSpacing, padding );

			return Convert.ToBase64String( Image.GetBuffer() );
		}

		public static string ToPngBase64( this List<Signature> sigs, bool showText = false )
		{
			return sigs.ToPngBase64( showText ? TEXT_HEIGHT : 0, TEXT_PADDING, VERTICAL_SPACING, PADDING );
		}


		public static MemoryStream ToPng( this List<Signature> sigs, bool showText = false )
		{
			return sigs.ToPng( showText ? TEXT_HEIGHT : 0, TEXT_PADDING, VERTICAL_SPACING, PADDING );
		}

		public static MemoryStream ToPng( this List<Signature> sigs, int textHeight, int textPadding, int verticalSpacing, int padding )
		{
			var MaxHeight = 0.0;
			var MaxWidth = 0.0;

			foreach( var Signature in sigs )
			{
				MaxHeight = Math.Max( Signature.Height, MaxHeight );
				MaxWidth = Math.Max( Signature.Width, MaxWidth );
			}

			var UnitHeight = (int)MaxHeight + textHeight + textPadding + verticalSpacing;
			var UnitWidth = (int)MaxWidth;

			var Count = sigs.Count;
			var Height = ( UnitHeight * Count ) + ( 2 * padding );
			var BMap = new Bitmap( UnitWidth + ( 2 * padding ), Height, PixelFormat.Format32bppArgb );

			using( var G = Graphics.FromImage( BMap ) )
			{
				G.Clear( Color.FromArgb( 0, Color.Red ) ); // Transparent red
				var Font = new Font( "Segoe UI", 10 );
				var Pen = new Pen( Color.Black, 2 );

				var J = 0;

				// Display most recent signature first
				for( var I = sigs.Count; --I >= 0; ++J )
				{
					var Signature = sigs[ I ];

					var Top = ( UnitHeight * J ) + padding;

					G.Clip = new Region( new Rectangle( padding, Top, UnitWidth, UnitHeight ) );

					if( textHeight > 0 )
					{
						var SigText = Signature.Status1 != STATUS1.UNSET ? $"{Signature.Status1.AsString()}, " : "";
						SigText = $"{SigText}{Signature.Status.AsString()}";

						var Date = $"{SigText}  {Signature.Date:g}";
						G.DrawString( Date, Font, Brushes.Black, 0, Top );
					}

					var Points = Signature.Points.Split( ',' );

					var PointList = ( from P in Points
					                  select P.Split( ':' )
					                  into TurtlePoint
					                  where TurtlePoint.Length == 3
					                  let X = int.Parse( TurtlePoint[ 1 ] )
					                  let Y = int.Parse( TurtlePoint[ 2 ] )
					                  select ( MouseDown: TurtlePoint[ 0 ] == "1", X, Y ) ).ToList();

					// Move signatures up
					var MinTop = int.MaxValue;

					foreach( var (_, _, Y) in PointList )
						MinTop = Math.Min( MinTop, Y );

					Top += ( textHeight + textPadding ) - MinTop;

					var XScale = MaxWidth / Signature.Width;
					var YScale = MaxHeight / Signature.Height;
					var Scale = Math.Min( XScale, YScale );

					var PenPos = new Point( padding, Top );

					foreach( var (MouseDown, X, Y) in PointList )
					{
						var P = new Point
						        {
							        X = (int)( X * Scale ) + padding,
							        Y = (int)( Y * Scale ) + Top
						        };

						if( MouseDown )
							G.DrawLine( Pen, PenPos, P );

						PenPos = P;
					}
				}
			}

			var Stream = new MemoryStream();
			BMap.Save( Stream, ImageFormat.Png );
			Stream.Position = 0;

			return Stream;
		}
	}
}