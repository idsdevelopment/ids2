﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Timer = System.Timers.Timer;

namespace MessageBroker
{
    public abstract class Broker<TMessage, TToken>
    {
        private readonly LinkedList<Message> MessageList;
        private readonly Timer CleanUpTimer;

        private class Message
        {
            internal readonly DateTime CreationTime = DateTime.Now;
            internal uint TtlMs;
            internal TToken Token;
            internal TMessage Content;
        }


        private class SubscriptionEntry
        {
            internal List<TMessage> Messages;
        }

        private readonly Dictionary<TToken, SubscriptionEntry> Subscriptions = new Dictionary<TToken, SubscriptionEntry>();

        protected abstract bool CompareTokensEqual( TToken a, TToken b );

        public List<TMessage> Select( TToken token, Func<TMessage, bool> filter )
        {
            var Result = new List<TMessage>();
            lock(MessageList)
            {
                if( MessageList.Count > 0 )
                {
                    var Messages = new List<Message>();

                    Messages.AddRange( MessageList.Where( message => CompareTokensEqual( message.Token, token ) && filter( message.Content ) ) );
                    foreach( var Message in Messages )
                    {
                        MessageList.Remove( Message );
                        Result.Add( Message.Content );
                    }
                }
                return Result;
            }
        }

        public List<TMessage> Wait( TToken token, Func<TMessage, bool> filter )
        {
            SubscriptionEntry Entry = null;

            try
            {
                bool MainThread;

                lock( Subscriptions )
                {
                    MainThread = !Subscriptions.TryGetValue( token, out Entry );
                    if( MainThread )
                    {
                        Entry = new SubscriptionEntry();
                        Monitor.Enter( Entry ); // lock before adding and becoming public

                        Subscriptions.Add( token, Entry );
                    }
                }

                if( !MainThread )
                    Monitor.Enter( Entry ); // All other threads will wait here

                // Main thread or in case the main thread aborts then anothher thread takes over
                while( Entry.Messages == null )
                {
                    var Messages = Select( token, filter );
                    if( Messages.Count == 0 )
                        Thread.Sleep( 1 );
                    else
                    {
                        Entry.Messages = Messages;
                        lock( Subscriptions )
                        {
                            Subscriptions.Remove( token );
                        }
                    }
                }
            }
            finally
            {
                Monitor.Exit( Entry ); // Allow other threads to get the Subscription entry
            }
            return Entry.Messages;
        }

        protected Broker()
        {
            MessageList = new LinkedList<Message>();
            CleanUpTimer = new Timer( 100 );
            CleanUpTimer.Elapsed += ( sender, args ) =>
            {
                CleanUpTimer.Enabled = false;
                try
                {
                    var Now = DateTime.Now;
                    var DeleteList = new List<Message>();
                    lock(MessageList)
                    {
                        lock( Subscriptions )
                        {
                            DeleteList.AddRange( from Message in MessageList let Diff = Now - Message.CreationTime where Diff.TotalMilliseconds >= Message.TtlMs select Message );
                            foreach( var Message in DeleteList )
                                MessageList.Remove( Message );
                        }
                    }
                }
                finally
                {
                    CleanUpTimer.Enabled = true;
                }
            };

            CleanUpTimer.Enabled = true;
        }

        ~Broker()
        {
            CleanUpTimer.Enabled = false;
            CleanUpTimer.Dispose();
        }

        public void Add( TToken token, TMessage message, uint ttlMs = 60 * 1000 )
        {
            lock(MessageList)
            {
                MessageList.AddLast( new LinkedListNode<Message>( new Message { Token = token, Content = message, TtlMs = ttlMs } ) );
            }
        }
    }
}