﻿using System.Collections.Generic;
using Protocol.Data;
using Storage.Carrier;
using Utils;

namespace MessageBroker.Carrier
{
    public class TripMessage
    {
        public int Board;
        public string TripId;
    }

    public class Trips : Broker<TripMessage, int>
    {
        public static Trips Notifications = new Trips();

        protected override bool CompareTokensEqual( int a, int b )
        {
            return a == b;
        }

        public TripList Wait( RequestContext.RequestContext context, int board )
        {
            var Result = new TripList();
            var TripIds = base.Wait( board, message => true );
            new Tasks.Task<TripMessage>().Run( TripIds, message =>
            {
                var Trip = Storage.Carrier.Trips.Get( context, message.TripId );
                if( Trip != null )
                {
                    lock( Result )
                    {
                        Result.Add( new Trip { Board = Trip.Board, TripId = Trip.TripId } );
                    }
                }
            }, 10 );
            return Result;
        }
    }
}