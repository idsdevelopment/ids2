﻿using System;
using System.Collections.Concurrent;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.Users;
using Exceptions;
using Interfaces.Abstracts;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Storage.AuditTrails;
using Utils;

//using Databases.Models;

namespace RequestContext
{
	public class RequestContext : AIRequestInterface
	{
		/// <summary>
		///     Auth Token
		/// </summary>
		[JsonProperty( Order = 0 )]
		public string R { get; } = new Random().Next( 100_000_000 ).ToString(); // Make sure random is first (Greatest change in key)

		[JsonProperty( Order = 1 )]
		public string C // Carrier Id
		{
			get => CarrierId;
			set => CarrierId = value;
		}

		[JsonProperty( Order = 2 )]
		public string U // User Name
		{
			get => UserName;
			set => UserName = value;
		}

		[JsonProperty( Order = 3 )]
		public string P
		{
			get => Password;
			set => Password = value;
		} // Password

		[JsonProperty( Order = 4 )]
		public sbyte T // Timezone offset
		{
			get => TimeZoneOffset;
			set => TimeZoneOffset = value;
		}

		[JsonProperty( Order = 5 )]
		public bool D // Debug server
		{
			get => Debug;
			set => Debug = value;
		}

		[JsonProperty( Order = 6 )]
		public bool I // Is Ids
		{
			get => IsIds;
			set => IsIds = value;
		}

		[JsonProperty( Order = 7 )]
		public bool N // New Carrier
		{
			get => IsNewCarrier;
			set => IsNewCarrier = value;
		}

		[JsonProperty( Order = 8 )]
		public override string IpAddress
		{
			get => string.IsNullOrWhiteSpace( _IpAddress ) ? "Unknown" : _IpAddress;
			set => _IpAddress = value;
		}


		[JsonProperty( Order = 9 )]
		public new int UserId
		{
			get => base.UserId;
			set => base.UserId = value;
		}

		/// <summary>
		///     Not Part Of Auth Token
		/// </summary>
		[JsonIgnore]
		public Configuration Configuration
		{
			get => Users.Configuration;
			set => Users.Configuration = value;
		}

		public override (string Account, string Key) BlobStorageAccount
		{
			get
			{
				var Cfg = Configuration;

				return ( Account: Cfg.BlobStorageAccount, Key: Cfg.BlobStoragePrimaryKey );
			}
		}

		[JsonIgnore]
		public override string StorageAccount => GetEndPoint().StorageAccount;

		[JsonIgnore]
		public override string StorageAccountKey => GetEndPoint().StorageAccountKey;

		[JsonIgnore]
		public override string StorageTableEndpoint => GetEndPoint().StorageTableEndpoint;

		[JsonIgnore]
		public override string StorageFileEndpoint => GetEndPoint().StorageFileEndpoint;

		[JsonIgnore]
		public override string StorageBlobEndpoint => GetEndPoint().StorageBlobEndpoint;

		[JsonIgnore]
		public override string ServiceBusPrimaryConnectionString => Configuration.ServiceBusPrimaryConnectionString;

		[JsonIgnore]
		public override string ServiceBusSecondaryConnectionString => Configuration.ServiceBusSecondaryConnectionString;

		[JsonIgnore]
		public override string SendGridApiKey => Configuration.SendGridKey;

		private class EPoints
		{
			[JsonIgnore] public string StorageAccount;
			[JsonIgnore] public string StorageAccountKey;
			[JsonIgnore] public string StorageBlobEndpoint;
			[JsonIgnore] public string StorageFileEndpoint;
			[JsonIgnore] public string StorageTableEndpoint;
		}

		private string _IpAddress;

		[JsonIgnore] private EPoints EndPoints;

		private static IRequestContext GetContext( string carrierId, string userName )
		{
			return new RequestContext
			       {
				       CarrierId = carrierId,
				       UserName  = userName
			       };
		}

		private EPoints GetEndPoint()
		{
			if( EndPoints is null )
			{
				var Ep = Users.GetEndPoints( this );

				EndPoints = new EPoints
				            {
					            StorageAccount       = Ep.StorageAccount,
					            StorageAccountKey    = Ep.StorageAccountKey,
					            StorageTableEndpoint = Ep.StorageTableEndpoint,
					            StorageBlobEndpoint  = Ep.StorageBlobEndpoint,
					            StorageFileEndpoint  = Ep.StorageFileEndpoint
				            };

				return EndPoints;
			}

			return EndPoints;
		}

		private static string ToAuthToken( RequestContext context )
		{
			return Encryption.ToTimeLimitedToken( Encryption.Encrypt( JsonConvert.SerializeObject( context ) ) );
		}

		private static RequestContext FromAuthToken( string encryptedJsonToken )
		{
			try
			{
				var (Value, Ok, TimeError, Base64Error, Diff, Expires) = Encryption.FromTimeLimitedTokenDetailed( encryptedJsonToken );

				if( Ok )
					return JsonConvert.DeserializeObject<RequestContext>( Encryption.Decrypt( Value ) );

				string Error;

				if( TimeError )
				{
					var Neg = Diff < 0 ? "-" : "";
					var T   = new TimeSpan( Math.Abs( Diff ) );
					Error = $"Time Error. Diff={Neg}{T.Days}.{T.Hours}/{T.Minutes}/{T.Seconds}.{T.Milliseconds} ({Diff})\r\nNow: {DateTimeOffset.UtcNow:G}\r\nExpires: {new DateTime( Expires ).ToUniversalTime():G}";
				}
				else if( Base64Error )
					Error = "Base 64 Error";
				else
					Error = "Could Not Decrypt";

				throw new SystemException( $"{Error} Auth Token: {encryptedJsonToken}" );
			}
			catch( Exception Exception )
			{
				throw new SystemException( $"Auth Token Exception: {Exception.Message}" );
			}
		}


		public static string ToIdsAuthToken( string carrierId, string userName, string password )
		{
			return $"{carrierId}-{userName}-{password}";
		}

		public static string ToAuthToken( string carrierId, string userName, string password, int timeZoneOffset, bool debug, int userId, string ipAddress )
		{
			return ToAuthToken( new RequestContext
			                    {
				                    CarrierId = carrierId,
				                    UserName  = userName,
				                    P         = password,
				                    T         = (sbyte)timeZoneOffset,
				                    D         = debug,
				                    UserId    = userId,
				                    IpAddress = ipAddress
			                    } );
		}

		public string ToAuthToken()
		{
			return Encryption.Encrypt( JsonConvert.SerializeObject( this ) );
		}

		public static RequestContext ToRequestContext( string encryptedJsonToken )
		{
			return FromAuthToken( encryptedJsonToken );
		}

		public static RequestContext ToRequestContextNotTimeLimited( string encryptedJsonToken )
		{
			try
			{
				return JsonConvert.DeserializeObject<RequestContext>( Encryption.Decrypt( encryptedJsonToken ) );
			}
			catch
			{
			}

			return null;
		}


		public static RequestContext ToRequestContext( string carrierId, string userName, string password, int timeZoneOffset, bool debug, string ipAddress )
		{
			return new RequestContext
			       {
				       C         = carrierId,
				       U         = userName,
				       P         = password,
				       T         = (sbyte)timeZoneOffset,
				       D         = debug,
				       IpAddress = ipAddress
			       };
		}

		public static string ToIdsAuthToken( string jsonToken )
		{
			var Token = ToRequestContext( jsonToken );

			return ToIdsAuthToken( Token.C, Token.U, Token.P );
		}

		public override IRequestContext Clone( string carrierId = "" )
		{
			var NewCarrier = carrierId.IsNotNullOrWhiteSpace();

			var Result = new RequestContext
			             {
				             CarrierId      = NewCarrier ? carrierId : CarrierId,
				             EndPoints      = NewCarrier ? null : EndPoints,
				             Configuration  = Configuration,
				             IpAddress      = IpAddress,
				             UserId         = UserId,
				             Password       = Password,
				             UserName       = UserName,
				             Debug          = Debug,
				             IsIds          = IsIds,
				             IsNewCarrier   = NewCarrier,
				             TimeZoneOffset = TimeZoneOffset,
				             Variables      = new ConcurrentDictionary<string, dynamic>( Variables )
			             };
			Result.GetEndPoint();

			return Result;
		}


		public override void SystemLogException( string message )
		{
			new SystemLogException( this, message );
		}

		public override void SystemLogException( Exception ex, string caller = "", string fileName = "", int lineNumber = 0 )
		{
			if( !caller.IsNullOrWhiteSpace() )
				SystemLogException( $"Method: {caller}  \r\nFile name: {fileName}\r\nLine number: {lineNumber:D}", ex );
			else
				new SystemLogException( this, ex );
		}

		public override void SystemLogException( string message, Exception ex )
		{
			new SystemLogException( this, message, ex );
		}


		public override void LogDifferences( string storageArea, string program, string description, object before, object after )
		{
			UserAuditTrail.LogDifferences( this, storageArea, program, description, before, after );
		}

		public override void LogDifferences( string storageArea, string program, string description, object before )
		{
			UserAuditTrail.LogDifferences( this, storageArea, program, description, before );
		}

		public override void LogDeleted( string storageArea, string program, string description, object value1 )
		{
			UserAuditTrail.LogDeleted( this, storageArea, program, description, value1 );
		}

		public override void LogNew( string storageArea, string program, string description, object value1 )
		{
			UserAuditTrail.LogNew( this, storageArea, program, description, value1 );
		}

		public override string GetUserStorageId( string userName )
		{
			using( var Db = new CarrierDb( this ) )
			{
				var Id = Db.GetStaffMemberInternalId( userName );

				return ToStorageId( userName, Id );
			}
		}

		public override void LogProgramStarted( string program, string description = "" )
		{
			UserAuditTrail.LogProgram( this, "Program", program, AuditTrailEntryBase.OPERATION.STARTED, description );
		}

		public override void LogProgramEnded( string program, string description = "" )
		{
			UserAuditTrail.LogProgram( this, "Program", program, AuditTrailEntryBase.OPERATION.ENDED, description );
		}

		public RequestContext()
		{
			if( NewContext is null )
				NewContext = GetContext;
		}
	}
}