﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Interfaces.Interfaces;
using Microsoft.Azure;
using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Authentication;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

namespace Azure_Web_Service
{
	public static class Subscription
	{
		public const string SITE_NAME = "IdsRoute",
		                    ID = "cc2c6e2a-1ab8-4e08-9b03-59af90955a7e",
		                    THUMB_PRINT = "CB1CD94F87D9DFC9AD3D51E7D5FAEF69706E4BAC",
		                    TENANT_ID = "766705db-ecbe-4350-8f27-fadbbb6d7e3b", // Active Directory Id
		                    PRINCIPAL_NAME = "AzureWebPrincipal",
		                    PRINCIPAL_ID = "8c05417c-b133-4a7a-ba81-b134752c236d",
		                    PRINCIPAL_KEY = "/79u8SgfBtkTdTUbc0IvMKebjPa3pNORYI0gmUW8DUM=",
		                    ADMINISTRATOR_NAME = "Terry",
		                    ADMINISTRATOR_PASSWORD = "Zaphod2015";


		public static readonly Region DEFAULT_REGION = Region.USNorthCentral;

		private static readonly object X509CertLock = new object();
		private static X509Certificate2 X509Cert;

		private static readonly object CertLock = new object();
		private static CertificateCloudCredentials Cert;

		private static readonly object AzCredLock = new object();
		private static AzureCredentials AzCred;


		public static X509Certificate2 GetStoreCertificate( string thumbprint )
		{
			var Locations = new List<StoreLocation>
			                {
				                StoreLocation.CurrentUser,
				                StoreLocation.LocalMachine
			                };

			foreach( var Location in Locations )
			{
				using( var Store = new X509Store( StoreName.My, Location ) )
				{
					try
					{
						Store.Open( OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly );
						var Certificates = Store.Certificates.Find( X509FindType.FindByThumbprint, thumbprint, false );

						if( Certificates.Count > 0 )
							return Certificates[ 0 ];
					}
					finally
					{
						Store.Close();
					}
				}
			}

			throw new ApplicationException( "No Certificate found" );
		}

		public static X509Certificate2 GetStoreCertificate()
		{
			lock( X509CertLock )
				return X509Cert ?? ( X509Cert = GetStoreCertificate( THUMB_PRINT ) );
		}

		public static CertificateCloudCredentials GetSubscriptionCredentials( IRequestContext context )
		{
			lock( CertLock )
			{
				try
				{
					return Cert ?? ( Cert = new CertificateCloudCredentials( ID, GetStoreCertificate() ) );
				}
				catch( Exception E )
				{
					context.SystemLogException( E );

					throw;
				}
			}
		}

		public static AzureCredentials GetAzureCredentials( IRequestContext context )
		{
			try
			{
				lock( AzCredLock )
					return AzCred ?? ( AzCred = SdkContext.AzureCredentialsFactory.FromServicePrincipal( PRINCIPAL_ID, PRINCIPAL_KEY, TENANT_ID, AzureEnvironment.AzureGlobalCloud ) );
			}
			catch( Exception E )
			{
				context.SystemLogException( E );

				throw;
			}
		}

		public static IAzure AzureAuthenticate( IRequestContext context )
		{
			try
			{
				return Azure.Authenticate( GetAzureCredentials( context ) ).WithSubscription( ID );
			}
			catch( Exception E )
			{
				context.SystemLogException( E );

				throw;
			}
		}

		public static RestClient GetRestClient( IRequestContext context )
		{
			var Client = RestClient.Configure()
			                       .WithEnvironment( AzureEnvironment.AzureGlobalCloud )
			                       .WithLogLevel( HttpLoggingDelegatingHandler.Level.Basic )
			                       .WithCredentials( GetAzureCredentials( context ) )
			                       .Build();

			if( Client is null )
			{
				const string MSG = "Cannot obtain RestClient";
				context.SystemLogException( MSG );

				throw new Exception( MSG );
			}

			return Client;
		}
	}
}