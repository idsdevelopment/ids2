﻿
Login-AzureRmAccount

$Password = ConvertTo-SecureString "Zaphod2015" –AsPlainText –Force

# Create the service principal (use a strong password)
$sp = New-AzureRmADServicePrincipal -DisplayName "AzureWebPrincipal" -Password $Password
Sleep 20
# Give it the permissions it needs...
New-AzureRmRoleAssignment -RoleDefinitionName Owner -ServicePrincipalName $sp.ApplicationId

$sp | Select DisplayName, ApplicationId