﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using Utils;

namespace SendGridEmail
{
	public partial class Client : SendGridMessage
	{
		public async Task<Response> SendHtml( string html, string plainText = "" )
		{
			if( html.IsNotNullOrWhiteSpace() )
			{
				HtmlContent = html;
				PlainTextContent = plainText ?? "";

				return await SClient.SendEmailAsync( this );
			}

			return null;
		}

		public async Task<Response> SendHtmlAndPdf( string fileName, string html, byte[] pdf, string plainText = "" )
		{
			if( html.IsNotNullOrWhiteSpace() )
			{
				HtmlContent = html;
				PlainTextContent = plainText ?? "";

				var HtmlBytes = Encoding.UTF8.GetBytes( html );

				Attachments = new List<Attachment>
				              {
					              new Attachment { Filename = fileName, Content = Convert.ToBase64String( pdf ), Type = "application/pdf", ContentId = $"ContentIdPdf.{fileName}" },
				              };

				var Response = await SClient.SendEmailAsync( this );

				return Response;
			}

			return null;
		}

		public async Task<Response> SendHtmlAsPdf( string fileName, string html, string plainText = "" )
		{
			return await SendHtmlAndPdf( fileName, html, Pdf.Pdf.HtmlToPdf( html ), plainText );
		}
	}
}