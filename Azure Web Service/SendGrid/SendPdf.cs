﻿using System;
using System.Threading.Tasks;
using SendGrid.Helpers.Mail;

namespace SendGridEmail
{
	public partial class Client : SendGridMessage
	{
		public async Task SendPdf( string pdfName, byte[] pdf )
		{
			if( !( pdf is null ) && ( pdf.Length > 0 ) )
			{
				var B64 = Convert.ToBase64String( pdf );
				AddAttachment( pdfName, B64, "pdf", "inline" );
				await SClient.SendEmailAsync( this );
			}
		}
	}
}