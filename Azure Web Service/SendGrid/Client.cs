﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interfaces.Interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;
using Utils;

namespace SendGridEmail
{
	public partial class Client : SendGridMessage
	{
		private const string DO_NOT_REPLY = "Do Not Reply";
		private static string ApiKey = "";
		private static object LockObject = new object();
		private SendGridClient SClient;

		private static Dictionary<string, string> SplitEmailAddress( string recipients )
		{
			return ( from Email in recipients.Split( new[] { ";" }, StringSplitOptions.RemoveEmptyEntries )
			         select Email.Split( ',' )
			       ).ToDictionary( names => names[ 0 ].Trim(), names => names.Length >= 2 ? names[ 1 ].Trim() : "" );
		}


		public Client( IRequestContext context, string fromAddress, Dictionary<string, string> recipients, string subject, string fromName = DO_NOT_REPLY )
		{
			if( !( recipients is null ) && ( recipients.Count > 0 ) && fromAddress.IsNotNullOrWhiteSpace() )
			{
				var FromAddress = SplitEmailAddress( fromAddress ).FirstOrDefault();
				fromAddress = FromAddress.Key;

				if( FromAddress.Value.IsNotNullOrWhiteSpace() )
					fromName = FromAddress.Value;

				string Key;

				lock( LockObject )
				{
					if( ApiKey.IsNullOrWhiteSpace() )
						ApiKey = context.SendGridApiKey;

					Key = ApiKey;
				}

				SClient = new SendGridClient( Key );

				From = new EmailAddress( fromAddress, fromName );

				AddTos( ( from R in recipients
				          select new EmailAddress( R.Key, R.Value ) ).ToList() );

				Subject = subject;
			}
		}

		public Client( IRequestContext context, string fromAddress, string recipients, string subject, string fromName = DO_NOT_REPLY )
			: this( context, fromAddress, SplitEmailAddress( recipients ), subject, fromName )
		{
		}
	}
}