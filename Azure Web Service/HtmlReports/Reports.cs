﻿using System;
using DataSourceModule;
using Interfaces.Interfaces;
using PulsarV3;

namespace ReportsModule
{
	public abstract partial class Reports
	{
		private const string
			REPORT_DATE = "REPORT_DATE",
			PAGE_NUMBER = "PAGE_NUMBER",
			EOF = "EOF",
			TOTAL_PAGE_TOKEN = "~!@TOTAL_PAGES@!~";

		protected IRequestContext Context { get; }

		protected Pulsar Pulsar { get; }

		protected abstract string TemplateName { get; }

		protected Reports( IRequestContext context, DataSource dataSource, PAPER_SIZES paperSize = PAPER_SIZES.A4,
		                   ORIENTATION orientation = ORIENTATION.PORTRAIT )
		{
			Context = context;

			DefaultPaperSize = paperSize;
			DefaultOrientation = orientation;

			Data = new ReportData
			       {
				       PaperSize = DefaultPaperSize,
				       Orientation = DefaultOrientation,
					   Source = dataSource
			       };

			( Pulsar = new Pulsar() ).RegisterBlockFunction( "REPORT", DoReport )
			                         .RegisterBlockFunction( "PAGE", DoPage )
			                         .RegisterBlockFunction( "HEADER", DoHeader )
			                         .RegisterBlockFunction( "FOOTER", DoFooter )
			                         .RegisterBlockFunction( "BAND", DoBand )

			                         //
			                         .RegisterFunction( "NEXT", DoNext )

			                         //
			                         .Assign( REPORT_DATE, DateTime.Now.ToUniversalTime().ToString( "u" ) )
			                         .Assign( "USER_ID", Context.UserName )
			                         .Assign( "TOTAL_PAGES", TOTAL_PAGE_TOKEN )
			                         .Assign( PAGE_NUMBER, 1 );
		}
	}
}