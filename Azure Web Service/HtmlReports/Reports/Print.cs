﻿using System.Web.Hosting;

namespace ReportsModule
{
	public partial class Reports
	{
		public string Print()
		{
			var Path = HostingEnvironment.MapPath( $"\\Reports\\{TemplateName}" );

			return Pulsar.Fetch( Path, true ).Replace( TOTAL_PAGE_TOKEN, Data.PageNumber.ToString() );
		}
	}
}