﻿using System.Collections.Generic;
using DataSourceModule;

namespace ReportsModule
{
	public partial class Reports
	{
		public class ReportData
		{
			public bool NeedHeader => PaperHeightLessHeaderAndFooterCm == RemainingHeightCm;

			public object CurrentRecord;

			public string DataSourceName = null,
			              Header = "",
			              Footer = "";

			public object Globals;

			public string GlobalsName;

			public bool LastRecord,
			            PrintedHeaderOrBand;

			public ORIENTATION Orientation;

			public PaperSize PageSize = new PaperSize();


			// ReSharper disable once MemberHidesStaticFromOuterClass
			public PAPER_SIZES PaperSize;

			public decimal

				// Real size of paper
				PaperSizeCm,

				// Less Size of Footer & Header
				PaperHeightLessHeaderAndFooterCm,
				RemainingHeightCm,
				HeaderSizeCm;

			public int
				RecordCount,
				CurrentIndex,
				PageNumber;

			public DataSource Source;

			public bool NeedFooter( decimal bandHeight )
			{
				return ( RemainingHeightCm < bandHeight ) || LastRecord || ( RemainingHeightCm <= 0 );
			}
		}
	}
}