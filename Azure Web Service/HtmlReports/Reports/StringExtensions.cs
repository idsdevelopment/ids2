﻿namespace ReportsModule
{
	public static class StringExtensions
	{
		private const string
			HEIGHT = "~HEIGHT~",
			WIDTH = "~WIDTH~",
			SIZE = "~SIZE~",
			ORIENTATION = "~ORIENT~",
			CONTENT = "~CONTENT~",
			MARGIN = "~MARGIN~",
			MARGIN_4 = "~MARGIN_4~";

		public static string ToCm( decimal sizeCm )
		{
			return sizeCm.ToString( "G" ) + "cm";
		}

		private static string DoReplace( string str, string token, string value )
		{
			return str.Replace( token, value );
		}

		public static string ReplaceHeight( this string str, string strHeight )
		{
			return DoReplace( str, HEIGHT, strHeight );
		}

		public static string ReplaceHeight( this string str, decimal height )
		{
			return str.ReplaceHeight( ToCm( height ) );
		}

		public static string ReplaceWidth( this string str, string strWidth )
		{
			return DoReplace( str, WIDTH, strWidth );
		}

		public static string ReplaceWidth( this string str, decimal width )
		{
			return str.ReplaceWidth( ToCm( width ) );
		}

		public static string ReplaceSize( this string str, string strSize ) // A4 / Letter
		{
			return DoReplace( str, SIZE, strSize );
		}

		public static string ReplaceOrientation( this string str, string orientation ) // Portrait / Landscape
		{
			return DoReplace( str, ORIENTATION, orientation );
		}

		public static string ReplaceContent( this string str, string content )
		{
			return DoReplace( str, CONTENT, content );
		}

		public static string ReplaceMargin( this string str, decimal margin )
		{
			return DoReplace( str, MARGIN, ToCm( margin ) );
		}

		public static string ReplaceMargin4( this string str, decimal margin )
		{
			return DoReplace( str, MARGIN_4, ToCm( margin / 4 ) );
		}
	}
}