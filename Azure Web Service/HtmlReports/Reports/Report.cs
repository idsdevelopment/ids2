﻿using System.Runtime.InteropServices;
using PulsarV3;

namespace ReportsModule
{
	public partial class Reports
	{
		protected readonly ReportData Data;

		private string DoReport( Pulsar pulsar, string content, Pulsar.FunctionArgs args )
		{
			if( content is null )
			{
				var Eof = true;

				foreach( var Arg in args )
				{
					switch( Arg.Key.ToUpper() )
					{
					case "DATASOURCE":
						var DataSourceName = Arg.Value.Trim();
						Data.DataSourceName = DataSourceName;

						if( Data.Source.Count > 0 )
						{
							var Record = Data.Source[ 0 ];
							Data.CurrentRecord = Record;
							pulsar.Assign( DataSourceName, Record );
							Eof = false;
						}

						break;

					case "GLOBALS":
						var GlobalsName = Arg.Value.Trim();
						Data.GlobalsName = GlobalsName;
						pulsar.Assign( GlobalsName, Data.Source.Globals );

						break;

					case "ORIENTATION":
						Data.Orientation = Arg.Value.Trim().ToLower() == "landscape" ? ORIENTATION.LANDSCAPE : ORIENTATION.PORTRAIT;

						break;
					}
				}

				Data.LastRecord = Eof;
				pulsar.Assign( EOF, Eof ? "1" : "0" );

				string Orientation;

				var PaperSize = PaperSizes[ Data.PaperSize ];

				string Size;

				switch( PaperSize.Size )
				{
				case PAPER_SIZES.A4:
					Size = "A4";

					break;

				case PAPER_SIZES.LETTER:
					Size = "letter";

					break;

				default:
					return "Unknown paper size";
				}

				switch( Data.Orientation )
				{
				case ORIENTATION.LANDSCAPE:
					Orientation = "landscape";
					var Temp = PaperSize.HeightCm;
					PaperSize.HeightCm = PaperSize.WidthCm;
					PaperSize.WidthCm = Temp;

					break;

				default:
					Orientation = "portrait";

					break;
				}

				Data.PageSize = PaperSize;

				Data.PaperHeightLessHeaderAndFooterCm = Data.RemainingHeightCm = Data.PaperSizeCm = PaperSize.HeightCm;

				const string STYLE = "\r\n<style>\r\n"
				                     + "@page{ size:A4; margin:0; padding:0;}\r\n"
				                     + "body,html{padding:0;margin:0;}\r\n"
				                     + "html,body,header,footer{page-break-before:avoid;page-break-after:avoid;}\r\n"
				                     + "header > *{margin:0;}\r\n"
				                     + "footer > *{margin:0;}\r\n"
				                     + "article{page-break-after:always;page-break-before:avoid;page-break-inside:avoid;margin:~MARGIN~;width: calc( ~WIDTH~ - 2 * ~MARGIN~ );}\r\n"
				                     + "footer{position:absolute;bottom:0;width:calc(~WIDTH~ - 2 * ~MARGIN~);margin-right:~MARGIN~;overflow:hidden;}\r\n\r\n"
                                     + "</style>\r\n";

				var RetVal = STYLE.ReplaceWidth( PaperSize.WidthCm )
				                  .ReplaceHeight( PaperSize.HeightCm )
				                  .ReplaceSize( Size )
				                  .ReplaceOrientation( Orientation )
				                  .ReplaceMargin4( PAGE_MARGIN_CM ) // Must be before ~MARGIN~ (longer sequence)
				                  .ReplaceMargin( PAGE_MARGIN_CM );

				return RetVal;
			}

			return content + ( Data.PrintedHeaderOrBand ? GetFooter( pulsar ) : "" );
		}
	}
}