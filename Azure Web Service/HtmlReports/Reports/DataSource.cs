﻿using PulsarV3;

namespace ReportsModule
{
	public partial class Reports
	{
		private string DoNext( Pulsar pulsar, Pulsar.FunctionArgs args )
		{
			string Eof;

			var Ndx = ++Data.CurrentIndex;

			if( Ndx < Data.RecordCount )
			{
				Data.CurrentRecord = Data.Source[ Ndx ];
				pulsar.Assign( Data.DataSourceName, Data.CurrentRecord );

				Eof = "0";
			}
			else
			{
				Eof = "1";
				Data.LastRecord = true;
			}

			pulsar.Assign( EOF, Eof );

			return "";
		}
	}
}