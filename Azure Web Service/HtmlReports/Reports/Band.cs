﻿using System;
using PulsarV3;

namespace ReportsModule
{
	public partial class Reports
	{
		private const string HEIGHT = "HEIGHT";


		private static decimal GetHeightName( Pulsar.FunctionArgs args, string bandType )
		{
			string Height = null;

			foreach( var Arg in args )
			{
				switch( Arg.Key.Trim().ToUpper() )
				{
				case HEIGHT:
					Height = Arg.Value.Trim();

					break;
				}

				if( !( Height is null ) )
					break;
			}

			if( string.IsNullOrEmpty( Height ) )
				throw new Exception( bandType + " has no height" );

			if( !decimal.TryParse( Height, out var H ) )
				throw new Exception( bandType + " band has an invalid height" );

			return H;
		}

		private string GetFooter( Pulsar pulsar )
		{
			Data.PrintedHeaderOrBand = false;
			Data.RemainingHeightCm = Data.PaperHeightLessHeaderAndFooterCm;
			pulsar.Assign( PAGE_NUMBER, Data.PageNumber );

			return ( !string.IsNullOrEmpty( Data.Footer ) ? new Pulsar( pulsar ).Fetch( Data.Footer ) : "" )
			       + "\r\n</article>\r\n";
		}


		private string GetHeader( Pulsar pulsar )
		{
			pulsar.Assign( PAGE_NUMBER, ++Data.PageNumber );
			Data.RemainingHeightCm = Data.PaperHeightLessHeaderAndFooterCm;

			return "\r\n<article>\r\n"
			       + ( !string.IsNullOrEmpty( Data.Header ) ? new Pulsar( pulsar ).Fetch( Data.Header ) : "" );
		}


		private string DoBand( Pulsar pulsar, string content, Pulsar.FunctionArgs args )
		{
			const string
				BAND = "Band",
				SECTION = "\r\n<section style=\"height:~HEIGHT~;max-height:~HEIGHT~;\">\r\n~CONTENT~\r\n</section>\r\n";

			if( content != null )
			{
				var Header = Data.NeedHeader ? GetHeader( pulsar ) : "";

				var BandHeight = GetHeightName( args, BAND );

				var Band = Header + SECTION.ReplaceHeight( BandHeight ).ReplaceContent( content );

				// Does then band fit?
				if( Data.NeedFooter( BandHeight ) )
					Band = GetFooter( pulsar ) + GetHeader( pulsar ) + Band;

				Data.RemainingHeightCm -= BandHeight;

				Data.PrintedHeaderOrBand = true;

				return Band;
			}

			return null;
		}
	}
}