﻿using PulsarV3;

namespace ReportsModule
{
	public partial class Reports
	{
		private string DoHeaderFooter( string content, Pulsar.FunctionArgs args, string headerFooter, string desc )
		{
			if( content != null )
			{
				var Height = Data.HeaderSizeCm = GetHeightName( args, desc );
				Data.PaperHeightLessHeaderAndFooterCm -= Height;
				Data.RemainingHeightCm = Data.PaperHeightLessHeaderAndFooterCm;

				return headerFooter.ReplaceHeight( Height ).ReplaceContent( content );
			}

			return "";
		}

		private string DoHeader( Pulsar pulsar, string content, Pulsar.FunctionArgs args )
		{
			if( content != null )
				Data.Header = DoHeaderFooter( content, args, "\r\n<header style=\"height:~HEIGHT~;\">\r\n~CONTENT~\r\n</header>\r\n", "Header" );

			return "";
		}

		private string DoFooter( Pulsar pulsar, string content, Pulsar.FunctionArgs args )
		{
			if( content != null )
				Data.Footer = DoHeaderFooter( content, args, "\r\n<footer style=\"height:~HEIGHT~;\">\r\n~CONTENT~\r\n</footer>\r\n", "Footer" );

			return "";
		}
	}
}