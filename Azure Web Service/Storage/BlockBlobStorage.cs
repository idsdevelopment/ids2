﻿using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Storage
{
	public class BlockBlobStorage : BlobStorage<CloudBlockBlob>
	{
		protected override CloudBlockBlob CreateBlobReference( string blobName )
		{
			return Container.GetBlockBlobReference( blobName );
		}

		public BlockBlobStorage( IRequestContext context, string containerName, string path, string blobName ) : base( context, containerName, path, blobName )
		{
		}
	}
}