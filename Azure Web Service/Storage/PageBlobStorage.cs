﻿using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Storage
{
	public class PageBlobStorage : BlobStorage<CloudPageBlob>
	{
		private long Size;

		protected override CloudPageBlob CreateBlobReference( string blobName )
		{
			return Container.GetPageBlobReference( blobName );
		}

		public PageBlobStorage( IRequestContext context, string containerName, string path, string blobName, long size ) : base( context, containerName, path, blobName )
		{
			Size = size;

			if( !( Blob is null ) && !Blob.Exists() )
				Blob.Create( size );
		}
	}
}