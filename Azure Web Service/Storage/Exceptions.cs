﻿using System;
using Interfaces.Interfaces;
using Logs;

namespace Exceptions
{
	public class SystemLogException : Exception
	{
		public SystemLogException( IRequestContext context, string message, Exception e ) : base( message, e )
		{
			new SystemLog( context, message, e );
		}

		public SystemLogException( IRequestContext context, string message ) : base( message )
		{
			new SystemLog( context, message );
		}

		public SystemLogException( IRequestContext context, Exception e ) : base( e.Message, e )
		{
			new SystemLog( context, e );
		}
	}


	public class NoDatabaseConnectionException : SystemLogException
	{
		public NoDatabaseConnectionException( IRequestContext context ) : base( context, "No database connection" )
		{
		}
	}
}