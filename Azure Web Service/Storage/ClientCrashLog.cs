﻿#nullable enable

using System;
using System.IO;
using System.Text;
using Interfaces.Abstracts;
using Interfaces.Interfaces;
using Protocol.Data;

namespace Storage
{
	public class ClientCrashLog : AppendBlobStorage
	{
		private ClientCrashLog( IRequestContext context, string carrierId, string userName, sbyte tz ) : base( context, "ClientCrashLog", carrierId, MakeBlobName( userName, tz ), true )
		{
		}

		private void InternalAdd( CrashReport log )
		{
			var Data = $"Account: {log.Account}\r\nUser: {log.User}\r\n\r\n{log.CrashData}";

			// Don't use Blob.AppendText
			using var Stream = new MemoryStream( Encoding.UTF8.GetBytes( Data ) ) { Position = 0 };
			Blob?.AppendBlock( Stream );
		}

		private static string MakeBlobName( string userName, sbyte tz )
		{
			return $"{userName}_{DateTime.UtcNow:yyyy-MM-dd_hh-mm-ss}_Tz_{tz:D2}.log";
		}

		public static void Add( CrashReport log )
		{
			var Context = AIRequestInterface.NewContext?.Invoke( "idsroute", "" );

			if( !( Context is null ) )
			{
				try
				{
					new ClientCrashLog( Context, log.Account, log.User, log.TSbyte ).InternalAdd( log );
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
				}
			}
		}
	}
}