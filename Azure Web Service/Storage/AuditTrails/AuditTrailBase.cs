﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Interfaces;
using Utils;

namespace Storage.AuditTrails
{
	public class AuditTrailEntryBase : TableEntry
	{
		public const string UNKNOWN = "Unknown";

		// ReSharper disable once InconsistentNaming

		public enum OPERATION
		{
			UNKNOWN,
			NEW,
			MODIFY,
			DELETE,
			FAILED,
			INFORM,
			SIGN_IN,
			SIGN_OUT,
			STARTED,
			ENDED
		}

		public string Operation { get; set; }

		public string Description { get; set; }

		public string Program { get; set; }

		public string Data { get; set; }

		public int TimeZoneOffset { get; set; }

		protected void XlateOp( OPERATION op )
		{
			switch( op )
			{
			case OPERATION.NEW:
				Operation = "NEW";

				break;

			case OPERATION.MODIFY:
				Operation = "MODIFY";

				break;

			case OPERATION.DELETE:
				Operation = "DELETE";

				break;

			case OPERATION.FAILED:
				Operation = "FAILED";

				break;

			case OPERATION.SIGN_IN:
				Operation = "SIGN IN";

				break;

			case OPERATION.SIGN_OUT:
				Operation = "SIGN OUT";

				break;

			case OPERATION.INFORM:
				Operation = "INFORM";

				break;

			case OPERATION.STARTED:
				Operation = "STARTED";

				break;

			case OPERATION.ENDED:
				Operation = "ENDED";

				break;

			default:
				Operation = "??????";

				break;
			}
		}

		protected void Init( OPERATION op, string description, string data )
		{
			if( Context == null )
				throw new Exception( "AuditTrailBase: No request context" );

			TimeZoneOffset = Context.TimeZoneOffset;

			Description = description ?? "";
			Data = data ?? "";

			XlateOp( op );
		}


		public AuditTrailEntryBase( IRequestContext context ) : base( context )
		{
		}

		public AuditTrailEntryBase( IRequestContext context, string partitionKey, string rowKey ) : base( context, partitionKey, rowKey )
		{
		}

		public AuditTrailEntryBase( IRequestContext context, OPERATION op, string description, string data )
			: base( context )
		{
			Init( op, description, data );
		}

		public AuditTrailEntryBase( IRequestContext context, string partitionKey, string rowKey, OPERATION op, string description, string data )
			: base( context, partitionKey, rowKey )
		{
			Init( op, description, data );
		}

		public AuditTrailEntryBase( IRequestContext context, OPERATION op, string program, string description, string data ) : this( context, op, description, data )
		{
			Program = program.IsNullOrWhiteSpace() ? UNKNOWN : program;
		}
	}

	public class AuditTrailBase : TableStorage
	{
		private static string ValueToString( object value )
		{
			switch( value )
			{
			case DateTime Dt:
				return Dt.ToString( "s" );

			case DateTimeOffset Dto:
				return Dto.ToString( "s" );

			default:
				return value.ToString();
			}
		}

		protected static string MakeAuditTrailName( IRequestContext context, string prefix, string suffix )
		{
			var StorageName = new StringBuilder();

			foreach( var C in $"{prefix}AuditTrail{suffix}" )
			{
				if( C.IsAlphaNumeric() )
					StorageName.Append( C );
			}

			return MakeCarrierStorageName( context, StorageName.ToString() );
		}

		public static string DifferencesAsString( AuditTrailEntryBase.OPERATION op, List<PropertyDifferences> entries )
		{
			var Text = new StringBuilder();

			switch( op )
			{
			case AuditTrailEntryBase.OPERATION.NEW:
			case AuditTrailEntryBase.OPERATION.DELETE:
				{
					foreach( var Entry in entries )
						Text.Append( $"{Entry.ParantClass}{Entry.Info.Name}: {ValueToString( Entry.Value1 )}\r\n" );

					break;
				}

			default:
				{
					foreach( var Entry in entries )
						Text.Append( $"{Entry.ParantClass}{Entry.Info.Name}: {ValueToString( Entry.Value1 )} -> {ValueToString( Entry.Value2 )} \r\n" );

					break;
				}
			}

			return Text.ToString().Trim();
		}

		public AuditTrailBase( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, toIdsStorage )
		{
		}
	}
}