﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces.Interfaces;
using Protocol.Data;
using Utils;

namespace Storage.AuditTrails
{
	public class TripAuditTrailEntry : AuditTrailEntryBase
	{
		private static string MakeRowKey( IRequestContext context )
		{
			return $"{DateTime.UtcNow.ToRowKey()}--{context.UserName}";
		}

		public TripAuditTrailEntry( IRequestContext context, string tripId, OPERATION op, string description, string data ) : base( context, tripId, MakeRowKey( context ), op, description, data )
		{
		}

		public TripAuditTrailEntry() : base( null )
		{
		}
	}

	public class TripAuditTrail : AuditTrailBase

	{
		private static string MakeName( IRequestContext context )
		{
			return MakeAuditTrailName( context, "Trip", "" );
		}

		public static void Add( TripAuditTrailEntry entry )
		{
			Task.Run( () =>
			          {
				          var Context = entry.Context;

				          ( (TableStorage)new TripAuditTrail( Context, MakeName( Context ) ) ).Add( entry );
			          } );
		}

		public static void LogDifferences( IRequestContext context, string program, string tripId, AuditTrailEntryBase.OPERATION op, List<PropertyDifferences> entries )
		{
			if( entries.Count > 0 )
			{
				Task.Run( () =>
				          {
					          string Desc;

					          switch( op )
					          {
					          case AuditTrailEntryBase.OPERATION.DELETE:
						          Desc = "Delete trip";

						          break;

					          case AuditTrailEntryBase.OPERATION.MODIFY:
						          Desc = "Modify trip";

						          break;

					          case AuditTrailEntryBase.OPERATION.NEW:
						          Desc = "New trip";

						          break;

					          default:
						          Desc = "??????";

						          break;
					          }

					          Add( new TripAuditTrailEntry( context, tripId, op, Desc, DifferencesAsString( op, entries ) ) { Program = program } );
				          } );
			}
		}

		public static void LogNew( IRequestContext context, string program, string tripId, object value1 )
		{
			LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.NEW, Properties.GetValues( value1 ) );
		}

		public static void LogDifferences( IRequestContext context, string program, string tripId, object value1 )
		{
			LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.NEW, Properties.GetValues( value1 ) );
		}

		public static void LogDifferences( IRequestContext context, string program, string tripId, object value1, object value2 )
		{
			LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.MODIFY, Properties.GetDifferences( value1, value2 ) );
		}

		public static void LogDeleted( IRequestContext context, string program, string tripId, object value1 )
		{
			LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.DELETE, Properties.GetValues( value1 ) );
		}

		public static Log GetLog( IRequestContext context, LogLookup lookup )
		{
			var Result = new Log();

			try
			{
				var Key = lookup.Key.Trim();

				if( !Key.IsNullOrWhiteSpace() )
				{
					var Audit = new TripAuditTrail( context, MakeName( context ) );

					var Items = from Entity in Audit.Table.CreateQuery<TripAuditTrailEntry>()
					            where string.Compare( Entity.PartitionKey, Key, StringComparison.Ordinal ) == 0
					            select Entity;

					foreach( var Item in Items )
					{
						Result.Add( new LogEntry
						            {
							            PartitionKey = Item.PartitionKey,
							            RowKey = Item.RowKey,
							            Timestamp = Item.Timestamp,
							            Operation = Item.Operation,
							            Program = Item.Program,
							            Description = Item.Description,
							            Data = Item.Data,
							            TimeZoneOffset = Item.TimeZoneOffset
						            } );
					}
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}

			return Result;
		}

		public TripAuditTrail( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, toIdsStorage )
		{
		}
	}
}