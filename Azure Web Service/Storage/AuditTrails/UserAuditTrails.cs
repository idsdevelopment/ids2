﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces.Interfaces;
using Protocol.Data;
using Utils;

namespace Storage.AuditTrails
{
	public class UserAuditTrailEntryEntry : AuditTrailEntryBase
	{
		public UserAuditTrailEntryEntry() : base( null )
		{
		}

		public UserAuditTrailEntryEntry( IRequestContext context, OPERATION op, string program, string description, string data )
			: base( context, op, program, description, data )
		{
		}


		public UserAuditTrailEntryEntry( IRequestContext context, OPERATION op, string data )
			: base( context, op, "", data )
		{
		}
	}

	public class UserAuditTrail : AuditTrailBase
	{
		private static string MakeName( IRequestContext context, string storageArea, string userName )
		{
			var StorageArea = storageArea.IsNullOrWhiteSpace() ? "" : storageArea.Trim().Capitalise();
			var UserStorage = userName.IsNullOrWhiteSpace() ? StorageArea : $"{userName.Trim().Capitalise()}{StorageArea}";

			return MakeAuditTrailName( context, "User", UserStorage );
		}


		public static void Add( string storageArea, UserAuditTrailEntryEntry entryEntry )
		{
			Task.Run( () =>
			          {
				          // Primary audit trail
				          new UserAuditTrail( entryEntry.Context, storageArea, "" ).Add( entryEntry );

				          // Primary who did the changes audit trail
				          new UserAuditTrail( entryEntry.Context, storageArea, entryEntry.Context.UserStorageId ).Add( entryEntry );
			          } );
		}


		public static void LogProgram( IRequestContext context, string storageArea, string program, AuditTrailEntryBase.OPERATION op, string description = "" )
		{
			Add( storageArea, new UserAuditTrailEntryEntry( context, op, description ) { Program = program } );
		}

		public static void LogDifferences( IRequestContext context, string storageArea, string program, AuditTrailEntryBase.OPERATION op, string description, List<PropertyDifferences> entries )
		{
			Task.Run( () =>
			          {
				          Add( storageArea, new UserAuditTrailEntryEntry( context, op, program, description, DifferencesAsString( op, entries ) ) { Program = program } );
			          } );
		}


		public static void LogDifferences( IRequestContext context, string storageArea, string program, string description, object value1, object value2 )
		{
			LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.MODIFY, description, Properties.GetDifferences( value1, value2 ) );
		}

		public static void LogDifferences( IRequestContext context, string storageArea, string program, string description, object value1 )
		{
			LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.NEW, description, Properties.GetValues( value1 ) );
		}

		public static void LogDeleted( IRequestContext context, string storageArea, string program, string description, object value1 )
		{
			LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.DELETE, description, Properties.GetValues( value1 ) );
		}

		public static void LogNew( IRequestContext context, string storageArea, string program, string description, object value1 )
		{
			LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.NEW, description, Properties.GetValues( value1 ) );
		}

		public static Log GetLog( IRequestContext context, LogLookup lookup )
		{
			var Result = new Log();

			try
			{
				var FromDate = lookup.FromDate.ToMinRowKey();
				var ToDate = lookup.ToDate.ToMaxRowKey();
				var ByUser = lookup.Key.IsNotNullOrWhiteSpace();

				var Audit = new UserAuditTrail( context, context.GetStorageId( lookup.Log ), ByUser ? context.GetUserStorageId( lookup.Key ) : "" );

				IQueryable<UserAuditTrailEntryEntry> Items;

				if( !ByUser )
				{
					Items = from Entity in Audit.Table.CreateQuery<UserAuditTrailEntryEntry>()
					        where ( string.Compare( Entity.RowKey, FromDate, StringComparison.Ordinal ) >= 0 ) && ( string.Compare( Entity.RowKey, ToDate, StringComparison.Ordinal ) <= 0 )
					        select Entity;
				}
				else
				{
					Items = from Entity in Audit.Table.CreateQuery<UserAuditTrailEntryEntry>()
					        where ( Entity.PartitionKey == lookup.Key )
					              && ( string.Compare( Entity.RowKey, FromDate, StringComparison.Ordinal ) >= 0 ) && ( string.Compare( Entity.RowKey, ToDate, StringComparison.Ordinal ) <= 0 )
					        select Entity;
				}

				foreach( var Item in Items )
				{
					Result.Add( new LogEntry
					            {
						            PartitionKey = Item.PartitionKey,
						            RowKey = Item.RowKey,
						            Timestamp = Item.Timestamp,
						            Operation = Item.Operation,
						            Program = Item.Program,
						            Description = Item.Description,
						            Data = Item.Data,
						            TimeZoneOffset = Item.TimeZoneOffset
					            } );
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}

			return Result;
		}

		public UserAuditTrail( IRequestContext context, string storageArea, string userName ) : base( context, MakeName( context, storageArea, userName ) )
		{
		}
	}
}