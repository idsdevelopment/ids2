﻿using System;
using System.Collections.Generic;
using System.Threading;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;
using Storage.Interfaces;

// ReSharper disable InconsistentNaming

namespace Storage
{
	public class TableStorage : StorageAccount, ITableStorage
	{
		private static readonly HashSet<string> TableNames = new HashSet<string>();
		private static readonly object TableNamesLockObject = new object();

		public CloudTable Table { get; private set; }

		private readonly TimeSpan Timeout = new TimeSpan( 0, 0, 30 );

		public readonly string TableName;

		private bool Do404( Exception E, ref bool doInit )
		{
			if( doInit && E.Message.Contains( "(404)" ) ) // Happens if I Manually delete SystemLog while running
			{
				doInit = false;

				lock( TableNamesLockObject )
					TableNames.Remove( TableName );
				Init();

				return true;
			}

			return false;
		}


		private Exception Do400or409( ITableEntity entity, Exception e )
		{
			if( e.Message.Contains( "(409)" ) )
				e = new Exception( $"Duplicate key: T={Table.Name}, P={entity.PartitionKey}, R={entity.RowKey}", e );

			else if( e.Message.Contains( "(400)" ) )
				e = new Exception( $"400 Error: T={Table.Name}, P={entity.PartitionKey}, R={entity.RowKey}", e );

			return e;
		}

		private void Init()
		{
			var TableClient = Account.CreateCloudTableClient();

			TableClient.DefaultRequestOptions = new TableRequestOptions
			                                    {
				                                    RetryPolicy   = ExponentialRetryPolicy,
				                                    ServerTimeout = new TimeSpan( 0, 5, 0 )
			                                    };

			Table = TableClient.GetTableReference( TableName );
			bool HasTable;

			lock( TableNamesLockObject )
				HasTable = TableNames.Contains( TableName );

			if( !HasTable )
			{
				var Creating = false;

				// Maybe in the process of deletion / Creation
				for( var I = 300;; ) // 5 minutes
				{
					try
					{
						if( !Table.Exists() )
						{
							if( !Creating )
							{
								Creating = true;

								if( Table.CreateIfNotExists() )
									continue;
							}
						}
						else
							break;

						throw new Exception( "Creating table storage timeout" );
					}
					catch( Exception E )
					{
						if( I-- <= 0 )
						{
							lock( TableNamesLockObject )
								Context.SystemLogException( $"Cannot create blob table: {TableName}", E );

							throw;
						}
					}

					Thread.Sleep( 1000 );
				}
			}

			lock( TableNamesLockObject )
				TableNames.Add( TableName );
		}

		public void AddOrReplace( TableEntity entity )
		{
			var DoInit = true;

			while( true )
			{
				try
				{
					// Create the TableOperation that inserts the entity.
					var InsertOperation = TableOperation.InsertOrReplace( entity );

					// Execute the insert operation.
					Table.Execute( InsertOperation, new TableRequestOptions
					                                {
						                                ServerTimeout        = Timeout,
						                                MaximumExecutionTime = Timeout
					                                } );
				}
				catch( Exception E )
				{
					if( Do404( E, ref DoInit ) )
						continue;

					try
					{
						Context.SystemLogException( Do400or409( entity, E ) );
					}
					catch
					{
					}
				}

				break;
			}
		}

		public TableStorage( IRequestContext context, string tableName, bool toIdsStorage = false )
			: base( context, toIdsStorage )
		{
			TableName = tableName;
			Init();
		}


		public void Add( TableEntity entity )
		{
			var DoInit = true;

			while( true )
			{
				try
				{
					// Create the TableOperation that inserts the entity.
					var InsertOperation = TableOperation.Insert( entity );

					// Execute the insert operation.
					Table.Execute( InsertOperation, new TableRequestOptions
					                                {
						                                ServerTimeout        = Timeout,
						                                MaximumExecutionTime = Timeout
					                                } );
				}
				catch( Exception E )
				{
					if( Do404( E, ref DoInit ) )
					{
						Context.SystemLogException( $" \"{TableName}\". Table Storage (404): {E}" );

						continue;
					}

					try
					{
						Context.SystemLogException( Do400or409( entity, E ) );
					}
					catch
					{
					}
				}

				break;
			}
		}

		public void Add( List<TableEntity> entities )
		{
			if( ( entities != null ) && ( entities.Count > 0 ) )
			{
				var E    = entities[ 0 ];
				var PKey = E.PartitionKey; // Batch operations must have the same partition key

				try
				{
					// Create the batch operation.
					var BatchOperation = new TableBatchOperation();
					var Ndx            = 0;

					foreach( var Entity in entities )
					{
						Entity.PartitionKey = PKey;
						Entity.RowKey       = $"{Entity.RowKey}-{Ndx++}";
						BatchOperation.Insert( Entity );
					}

					Table.ExecuteBatch( BatchOperation, new TableRequestOptions
					                                    {
						                                    MaximumExecutionTime = Timeout,
						                                    ServerTimeout        = Timeout
					                                    } );
				}
				catch( Exception Ex )
				{
					try
					{
						Context.SystemLogException( Ex );
					}
					catch
					{
					}
				}
			}
		}
	}
}