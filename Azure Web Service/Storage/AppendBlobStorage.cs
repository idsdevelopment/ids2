﻿using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Storage
{
	public class AppendBlobStorage : BlobStorage<CloudAppendBlob>
	{
		protected override CloudAppendBlob CreateBlobReference( string blobName )
		{
			return Container.GetAppendBlobReference( blobName );
		}

		protected override void CreateBlob()
		{
			if( !Blob.Exists() )
				Blob.CreateOrReplace();
		}

		public AppendBlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false ) : base( context, containerName, path, blobName, toIdsStorage )
		{
		}
	}
}