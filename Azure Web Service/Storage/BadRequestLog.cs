﻿using System;
using Interfaces.Interfaces;
using Logs;

namespace Storage
{
	public class BadRequestLog : SystemLog
	{
		public BadRequestLog( IRequestContext context ) : base( "BadRequestLog", context )
		{
		}

		public BadRequestLog( IRequestContext context, Exception e ) : this( context )
		{
			AddToLog( context, e );
		}

		public BadRequestLog( IRequestContext context, string message ) : this( context )
		{
			AddToLog( context, message );
		}

		public BadRequestLog( IRequestContext context, string message, string details ) : this( context )
		{
			AddToLog( context, message, details );
		}

		public BadRequestLog( IRequestContext context, string message, Exception e ) : this( context )
		{
			AddToLog( context, message, e );
		}

		public new static void Add( IRequestContext context )
		{
			new BadRequestLog( context );
		}

		public new static void Add( IRequestContext context, Exception e )
		{
			new BadRequestLog( context, e );
		}

		public new static void Add( IRequestContext context, string message )
		{
			new BadRequestLog( context, message );
		}

		public new static void Add( IRequestContext context, string message, string details )
		{
			new BadRequestLog( context, message, details );
		}

		public new static void Add( IRequestContext context, string message, Exception e )
		{
			new BadRequestLog( context, message, e );
		}
	}
}