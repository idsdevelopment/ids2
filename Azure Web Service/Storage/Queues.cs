﻿using System;
using System.Threading;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Storage
{
	public class QueueStorage : StorageAccount
	{
		private readonly CloudQueue Queue;

		public void Add( string id, string message, TimeSpan? ttl = null )
		{
			try
			{
				var M = new CloudQueueMessage( id, message );
				M.SetMessageContent( message );
				Queue.AddMessage( M, ttl );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public void AddAsync( string id, string message, TimeSpan? ttl = null )
		{
			try
			{
				var M = new CloudQueueMessage( id, message );
				M.SetMessageContent( message );

				Queue.AddMessageAsync( M, ttl, null, new QueueRequestOptions(), new OperationContext() );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public void Add( string message, TimeSpan? ttl = null )
		{
			try
			{
				var M = new CloudQueueMessage( message );
				M.SetMessageContent( message );
				Queue.AddMessage( new CloudQueueMessage( message ), ttl );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public void AddAsync( string message, TimeSpan? ttl = null )
		{
			try
			{
				var M = new CloudQueueMessage( message );
				M.SetMessageContent( message );

				Queue.AddMessageAsync( new CloudQueueMessage( message ), ttl, null, new QueueRequestOptions(),
				                       new OperationContext() );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public CloudQueueMessage Peek()
		{
			try
			{
				return Queue.PeekMessage();
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}

			return default( CloudQueueMessage );
		}

		public CloudQueueMessage GetMessage()
		{
			try
			{
				var Retval = Queue.GetMessage();

				try
				{
					Queue.DeleteMessageAsync( Retval );
				}
				catch( Exception E )
				{
					try
					{
						Context.SystemLogException( E );
					}
					catch
					{
					}
				}

				return Retval;
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}

			return default( CloudQueueMessage );
		}

		public void DeleteMessage( CloudQueueMessage message )
		{
			try
			{
				Queue.DeleteMessage( message );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public void DeleteMessageAsync( CloudQueueMessage message )
		{
			try
			{
				Queue.DeleteMessageAsync( message );
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public void Delete()
		{
			try
			{
				Queue.Delete();
			}
			catch( Exception E )
			{
				try
				{
					Context.SystemLogException( E );
				}
				catch
				{
				}
			}
		}

		public QueueStorage( IRequestContext context, string queueName ) : base( context )
		{
			// Create the queue client
			var QueueClient = Account.CreateCloudQueueClient();

			QueueClient.DefaultRequestOptions = new QueueRequestOptions
			                                    {
				                                    RetryPolicy = ExponentialRetryPolicy
			                                    };

			// Create queue if it doesn't exist. 
			Queue = QueueClient.GetQueueReference( MakeContainerName( queueName ) );

			// Maybe in the process of deletion
			for( var I = 10;; )
			{
				try
				{
					Queue.CreateIfNotExists();
				}
				catch
				{
					if( I-- <= 0 )
						throw;
					Thread.Sleep( 60000 ); // Sleep 1 min

					continue;
				}

				break;
			}
		}
	}
}