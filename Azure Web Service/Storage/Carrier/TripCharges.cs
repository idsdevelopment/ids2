﻿using System;
using System.Threading.Tasks;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;

namespace Storage.Carrier
{
	public class TripCharge : TableEntry
	{
		[IgnoreProperty]
		public string AccountCode
		{
			get => PartitionKey;
			set => PartitionKey = value;
		}

		[IgnoreProperty]
		public string TripId
		{
			get => RowKey;
			set => RowKey = value;
		}

		public string ChargeId { get; set; }

		public double DValue { get; set; }

		[IgnoreProperty]
		public decimal Value
		{
			get => (decimal)DValue;
			set => DValue = (double)value;
		}

		public bool ChargeApplied { get; set; }
		public string PackageType { get; set; }

		public double DResult { get; set; }

		[IgnoreProperty]
		public decimal Result
		{
			get => (decimal)DResult;
			set => DResult = (double)value;
		}

		public TripCharge( IRequestContext context ) : base( context )
		{
		}
	}

	public class TripCharges : TableStorage
	{
		public static Task Add( IRequestContext context, TripCharge charge )
		{
			return Task.Run( () =>
			                 {
				                 charge.Timestamp = DateTime.UtcNow;
				                 new TripCharges( context ).Add( charge );
			                 } );
		}

		public TripCharges( IRequestContext context ) : base( context, MakeCarrierStorageName( context, "TripCharges" ) )
		{
		}
	}
}