﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Protocol.Data;
using Utils;

namespace Storage.Carrier
{
	public class Signatures : TableStorage
	{
		public class SignatureEntry : TableEntity
		{
			[IgnoreProperty]
			public List<Signature> Signatures { get; set; } = new List<Signature>();


			public string SignaturesAsJson
			{
				get => JsonConvert.SerializeObject( Signatures );
				set => Signatures = JsonConvert.DeserializeObject<List<Signature>>( value );
			}

			[IgnoreProperty]
			public string TripId
			{
				get => PartitionKey;
				set => PartitionKey = value;
			}

			public SignatureEntry()
			{
			}


			public SignatureEntry( IRequestContext context, string tripId )
			{
				RowKey = context.CarrierId;
				PartitionKey = tripId;
			}
		}

		public void AddUpdate( string tripId, Signature sig )
		{
			try
			{
				if( ( sig.Height > 0 ) && ( sig.Width > 0 ) && sig.Points.IsNotNullOrWhiteSpace() )
				{
					for( var I = 0; I++ < 10; )
					{
						try
						{
							var Rec = ( from Sig in Table.CreateQuery<SignatureEntry>()
							            where Sig.PartitionKey == tripId
							            select Sig ).FirstOrDefault() ?? new SignatureEntry( Context, tripId );

							Rec.Signatures.Add( sig );
							AddOrReplace( Rec );

							break;
						}
						catch( Exception E )
						{
							Console.WriteLine( E.Message );
							Thread.Sleep( 100 );
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}


		public List<Signature> GetSignatures( string tripId )
		{
			try
			{
				var Rec = ( from Sig in Table.CreateQuery<SignatureEntry>()
				            where Sig.PartitionKey == tripId
				            select Sig ).FirstOrDefault();

				if( !( Rec is null ) )
					return Rec.Signatures;
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}

			return new List<Signature>();
		}


		public Signatures( IRequestContext context, bool toIdsStorage = false ) : base( context, "Signatures", toIdsStorage )
		{
		}
	}
}