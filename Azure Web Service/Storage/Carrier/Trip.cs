﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Protocol.Data;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Storage.Carrier
{
	public class TripDate : TableEntry
	{
		public string TripAsJson { get; set; }

		public static string ToPartitionKey( DateTimeOffset date )
		{
			return date.ToUniversalTime().Date.ToString( "s" );
		}

		public TripDate() : base( null )
		{
		}

		public TripDate( IRequestContext context, DateTimeOffset date, string tripId, string tripAsJson ) : base( context )
		{
			PartitionKey = ToPartitionKey( date );
			RowKey       = tripId;
			TripAsJson   = tripAsJson;
		}
	}

	public class TripsByDate : TableStorage
	{
		public static Task Add( IRequestContext context, string tripId, DateTimeOffset callDate, string tripAsJson )
		{
			return Task.Run( () =>
			                 {
				                 try
				                 {
					                 var TripStorage = new TripsByDate( context );
					                 var Trip        = new TripDate( context, callDate, tripId, tripAsJson );

					                 TripStorage.Add( Trip );
				                 }
				                 catch( Exception Exception )
				                 {
					                 context.SystemLogException( Exception );
				                 }
			                 } );
		}

		public static Task Add( IRequestContext context, Trip trip )
		{
			return Add( context, trip.TripId, trip.CallTime, JsonConvert.SerializeObject( trip ) );
		}

		public static List<TripDate> Get( IRequestContext context, string fromDate, string toDate )
		{
			return ( from T in new TripsByDate( context ).Table.CreateQuery<TripDate>().AsQueryable()
			         where ( T.PartitionKey.CompareTo( fromDate ) >= 0 ) && ( T.PartitionKey.CompareTo( toDate ) <= 0 )
			         select T ).ToList();
		}

		public static List<TripDate> Get( IRequestContext context, string fromDate, string toDate, string tripId )
		{
			return ( from T in new TripsByDate( context ).Table.CreateQuery<TripDate>().AsQueryable()
			         where ( string.Compare( T.PartitionKey, fromDate, StringComparison.Ordinal ) >= 0 ) && ( T.PartitionKey.CompareTo( toDate ) <= 0 ) && ( T.RowKey == tripId )
			         select T ).ToList();
		}


		public static TripList GetTrips( IRequestContext context, string fromDate, string toDate )
		{
			var Result = new TripList();

			foreach( var T in Get( context, fromDate, toDate ) )
				Result.Add( JsonConvert.DeserializeObject<Trip>( T.TripAsJson ) );

			return Result;
		}

		public static TripList GetTrips( IRequestContext context, string fromDate, string toDate, string tripId )
		{
			var Result = new TripList();

			foreach( var T in Get( context, fromDate, toDate, tripId ) )
				Result.Add( JsonConvert.DeserializeObject<Trip>( T.TripAsJson ) );

			return Result;
		}


		public static List<TripDate> Get( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate )
		{
			var FromDate = TripDate.ToPartitionKey( fromDate );
			var ToDate   = TripDate.ToPartitionKey( toDate );

			return Get( context, FromDate, ToDate );
		}

		public static List<TripDate> Get( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate, string tripId )
		{
			var FromDate = TripDate.ToPartitionKey( fromDate );
			var ToDate   = TripDate.ToPartitionKey( toDate );

			return Get( context, FromDate, ToDate, tripId );
		}


		public static TripList GetTrips( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate )
		{
			var FromDate = TripDate.ToPartitionKey( fromDate );
			var ToDate   = TripDate.ToPartitionKey( toDate );

			return GetTrips( context, FromDate, ToDate );
		}


		public static TripList GetTrips( IRequestContext context, DateTimeOffset fromDate, DateTimeOffset toDate, string tripId )
		{
			var FromDate = TripDate.ToPartitionKey( fromDate );
			var ToDate   = TripDate.ToPartitionKey( toDate );

			return GetTrips( context, FromDate, ToDate, tripId );
		}


		public static List<TripDate> Get( IRequestContext context, DateTimeOffset date )
		{
			var Date = date.ToUniversalTime().Date;

			return Get( context, Date, Date );
		}

		public static List<TripDate> Get( IRequestContext context, DateTimeOffset date, string tripId )
		{
			var Date = date.ToUniversalTime().Date;

			return Get( context, Date, Date, tripId );
		}


		public static TripList GetTrips( IRequestContext context, DateTimeOffset date )
		{
			var Date = date.ToUniversalTime().Date;

			return GetTrips( context, Date, Date );
		}

		public static TripList GetTrips( IRequestContext context, DateTimeOffset date, string tripId )
		{
			var Date = date.ToUniversalTime().Date;

			return GetTrips( context, Date, Date, tripId );
		}

		public TripsByDate( IRequestContext context ) : base( context, MakeCarrierStorageName( context, "TripsByDate" ) )
		{
		}
	}


	public class Trips
	{
		public static void Add( IRequestContext context, Trip trip )
		{
			TripsByDate.Add( context, trip );
		}
	}
}