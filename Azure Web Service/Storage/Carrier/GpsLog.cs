﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Interfaces.Interfaces;

namespace Storage.Carrier
{
	public class GpsLog : AppendBlobStorage
	{
		public class GpsPoint
		{
			public DateTimeOffset DateTime;

			public double Latitude,
			              Longitude;

			public override string ToString() => $";{DateTime:O},{Latitude},{Longitude}";
		}

		public void Append( IList<GpsPoint> points )
		{
			try
			{
				if( points.Count > 0 )
				{
					var AppendData = new StringBuilder();

					foreach( var Point in points )
						AppendData.Append( Point );

					// Don't use Blob.AppendText
					using var Stream = new MemoryStream( Encoding.UTF8.GetBytes( AppendData.ToString() ) ) { Position = 0 };

					Blob.AppendBlock( Stream );
				}
			}
			catch( Exception E )
			{
				Context.SystemLogException( E );
			}
		}

		public GpsLog( IRequestContext context ) : base( context, MakeCarrierStorageName( context, "GpsLogs" ), context.UserName.ToLower(), $"{DateTime.UtcNow:yyyy-MM-dd}.log" )
		{
		}
	}
}