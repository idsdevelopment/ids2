﻿using Interfaces.Interfaces;

namespace Storage.Carrier
{
	public class Backups : BlockBlobStorage
	{
		public Backups( IRequestContext context, string backupName = "" ) : base( context, MakeCarrierStorageName( context, "Backups", false ), "", backupName )
		{
		}
	}
}