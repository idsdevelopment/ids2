﻿using System;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;

namespace Storage
{
	public static class TableEntryExtensions
	{
		private static readonly object LockObject = new object();
		private static ulong Cntr;

		public static string ToRowKey( this DateTime time )
		{
			ulong C;

			lock( LockObject )
				C = Cntr++;

			return $"{time:yyyy-MM-dd HH:mm:ss:FFFFFFF} [[{C}]]";
		}

		public static string ToMinRowKey( this DateTimeOffset time )
		{
			return time.UtcDateTime.ToRowKey();
		}

		public static string ToMaxRowKey( this DateTimeOffset time )
		{
			return time.Add( new TimeSpan( 0, 23, 59, 59, 999 ) ).UtcDateTime.ToRowKey();
		}
	}

	public class TableEntry : TableEntity
	{
		protected static DateTime AzureStorageMinDateTime = new DateTime( 1601, 1, 1 );

		public IRequestContext Context { get; }

		public TableEntry( IRequestContext context, string partitionKey, string rowKey )
		{
			Context = context;
			PartitionKey = partitionKey;
			RowKey = rowKey;
		}

		public TableEntry( IRequestContext context ) : this( context, context is null ? "NULL Context" : context.UserName, DateTime.UtcNow.ToRowKey() )
		{
		}
	}
}