﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Interfaces.Interfaces;
using Microsoft.WindowsAzure.Storage.Blob;
using Utils;

namespace Storage
{
	public abstract class BlobStorage<T> : StorageAccount where T : CloudBlob
	{
		public List<string> BlobList => ( from Item in Container.ListBlobs().OfType<T>()
		                                  select Item.Name ).ToList();

		public string BlobName { get; } = "";

		public CloudBlobContainer Container { get; }

		public T? Blob { get; }

		private BlobStorage( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
			: base( context, toIdsStorage )
		{
			var BlobClient = Account.CreateCloudBlobClient();

			BlobClient.DefaultRequestOptions = new BlobRequestOptions
			                                   {
				                                   RetryPolicy = ExponentialRetryPolicy
			                                   };

			// Create the blob if it doesn't exist.
			containerName = MakeContainerName( containerName );
			Container     = BlobClient.GetContainerReference( containerName );

			// Maybe in the process of deletion
			for( var I = 10;; )
			{
				try
				{
					Container.CreateIfNotExists();
				}
				catch
				{
					if( I-- <= 0 )
						throw;
					Thread.Sleep( 60000 ); // Sleep 1 min

					continue;
				}

				break;
			}

			if( blobName.IsNotNullOrEmpty() )
			{
				Blob = CreateBlobReference( BlobName = blobName );
				CreateBlob();
			}
		}


		protected abstract T CreateBlobReference( string blobName );

		protected virtual void CreateBlob()
		{
		}

		public void Delete()
		{
			Blob?.Delete();
		}

		public List<string> GetBlobListStartingWith( string startsWith )
		{
			return ( from Item in BlobList
			         where Item.StartsWith( startsWith )
			         select Item ).ToList();
		}

		protected BlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false )
			: this( context, containerName, CombinePathAndName( path, blobName ), toIdsStorage )
		{
		}
	}
}