﻿using System;
using System.Linq;
using System.Net;
using Azure_Web_Service;
using Interfaces.Interfaces;
using Microsoft.Azure.Management.Storage.Fluent.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using ResourceGroups;
using Utils;

namespace Storage
{
	public class StorageAccount
	{
		protected readonly CloudStorageAccount Account;
		protected readonly IRequestContext Context;

		protected IRetryPolicy ExponentialRetryPolicy = new ExponentialRetry( TimeSpan.FromSeconds( 2 ), 100 );

		private static string MakeStorageAccountName( IRequestContext context )
		{
			// Space in middle for  capitalisation
			var RetVal = $"{Subscription.SITE_NAME} {context.CarrierId}".ToLower().ToAlphaNumeric();

			if( RetVal.Length > 24 )
				RetVal = RetVal.Substring( 0, 24 );

			return RetVal;
		}

		protected static string CombinePathAndName( string path, string name )
		{
			if( ( path != "" ) && !path.EndsWith( "/" ) )
				path += "/";

			return path + Uri.EscapeDataString( name );
		}

		protected static string CombinePaths( string basePath, string path, bool leadingSlash = true )
		{
			if( leadingSlash && !basePath.StartsWith( "/" ) )
				basePath = "/" + basePath;

			if( !basePath.EndsWith( "/" ) )
				basePath += "/";

			if( !string.IsNullOrEmpty( path ) )
			{
				if( path.StartsWith( "/" ) )
					path = path.Substring( 1 );

				basePath += path;

				if( !basePath.EndsWith( "/" ) )
					basePath += "/";
			}

			return basePath;
		}

		public static (string AccountName, string AccountKey, Endpoints EndPoint, bool Ok) ReCreateBlobStorageAccount( IRequestContext context )
		{
			DeleteStorageAccount( context );

			return CreateBlobStorageAccount( context );
		}


		public static ( string AccountName, string AccountKey, Endpoints EndPoint, bool Ok) CreateBlobStorageAccount( IRequestContext context )
		{
			try
			{
				var (GroupName, Ok) = Resource.CreateResourceGroup( context );

				if( Ok )
				{
					var Acc = Subscription.AzureAuthenticate( context ).StorageAccounts
					                      .Define( MakeStorageAccountName( context ) )
					                      .WithRegion( Subscription.DEFAULT_REGION )
					                      .WithExistingResourceGroup( GroupName )
					                      .Create();

					var StorageAccountKeys = Acc.GetKeys();
					var PrimaryKey         = StorageAccountKeys[ 0 ].Value;

					return ( AccountName: Acc.Name, AccountKey: PrimaryKey, EndPoint: Acc.EndPoints.Primary, Ok: true );
				}
			}
			catch( Exception E )
			{
				context.SystemLogException( nameof( CreateBlobStorageAccount ), E );
			}

			return ( AccountName: "", AccountKey: "", EndPoint: null, Ok: false );
		}

		public static void DeleteStorageAccount( IRequestContext context )
		{
			try
			{
				var Name    = MakeStorageAccountName( context );
				var Acc     = Subscription.AzureAuthenticate( context ).StorageAccounts;
				var AccList = Acc.List().ToList();

				var Rec = ( from A in AccList
				            where A.Name.Compare( Name, StringComparison.OrdinalIgnoreCase ) == 0
				            select A ).FirstOrDefault();

				if( Rec != null )
					Acc.DeleteById( Rec.Id );
			}
			catch( Exception Exception )
			{
				context.SystemLogException( nameof( DeleteStorageAccount ), Exception );
			}
		}

		public static string MakeCarrierStorageName( IRequestContext context, string storageName, bool allowDebug = true )
		{
			var Debug = allowDebug && context.Debug ? Constants.TEST : "";

			return $"{Debug}{storageName}";
		}

		public static string MakeContainerName( string name )
		{
			name = File.MakeIdentFileName( name, '-' );

			while( name.Contains( "--" ) )
				name = name.Replace( "--", "-" );

			if( name.Length > 63 )
				name = name.Substring( 0, 63 );

			return name.Trim().ToLower();
		}

		public StorageAccount( IRequestContext context, bool toIdsStorage = false )
		{
			try
			{
				Context = context;

				string StorageAccount,
				       StorageKey;

				if( toIdsStorage )
				{
					var (Acnt, Key) = Context.BlobStorageAccount;
					StorageAccount  = Acnt;
					StorageKey      = Key;
				}
				else
				{
					StorageAccount = context.StorageAccount;
					StorageKey     = context.StorageAccountKey;
				}

				var ConfigString = $"DefaultEndpointsProtocol=https;AccountName={StorageAccount};AccountKey={StorageKey}";
				Account = CloudStorageAccount.Parse( ConfigString );
				var TableServicePoint = ServicePointManager.FindServicePoint( Account.TableEndpoint );
				TableServicePoint.UseNagleAlgorithm = false;
			}
			catch( Exception E )
			{
				Console.WriteLine( E );

				throw;
			}
		}
	}
}