﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using Interfaces.Interfaces;
using Storage;

// ReSharper disable ObjectCreationAsStatement

namespace Logs
{
	public class SystemLog : TableStorage
	{
		public class SystemLogEntry : TableEntry
		{
			public SystemLogEntry( IRequestContext context, string tableType ) : base( context )
			{
				IpAddress = context.IpAddress;
				PartitionKey = $"{context.CarrierId}__{tableType}";
				RowKey = $"{context.UserName}_{RowKey}";
			}

			public string Reason { get; set; } = "";

			public string Details { get; set; } = "";

			public string SubDetails { get; set; } = "";

			public string CallStack { get; set; } = Environment.StackTrace;

			public string IpAddress { get; set; }
		}

		protected SystemLog( string logName, IRequestContext context )
			: base( context, logName, true )
		{
		}

		public SystemLog( IRequestContext context )
			: this( "SystemLog", context )
		{
		}

		public SystemLog( IRequestContext context, Exception e )
			: this( context )
		{
			AddToLog( context, e );
		}

		public SystemLog( IRequestContext context, string message )
			: this( context )
		{
			AddToLog( context, message );
		}

		public SystemLog( IRequestContext context, string message, string details )
			: this( context )
		{
			AddToLog( context, message, details );
		}

		public SystemLog( IRequestContext context, string message, Exception e )
			: this( context )
		{
			AddToLog( context, message, e );
		}

		private void AddEntity( IRequestContext context, Exception e )
		{
			switch( e )
			{
			case DbEntityValidationException E:
				foreach( var Error in E.EntityValidationErrors.SelectMany( validationError => validationError.ValidationErrors ) )
				{
					Add( new SystemLogEntry( context, "Entity Validation Error" )
					     {
						     Reason = Error.ToString(),
						     Details = Error.ErrorMessage,
						     SubDetails = ""
					     } );
				}

				break;

			case DbUpdateException _:
				var Details = "";
				var SubDetails = "";

				if( e.InnerException != null )
				{
					Details = e.InnerException.Message;

					if( e.InnerException.InnerException != null )
						SubDetails = e.InnerException.InnerException.Message;
				}

				Add( new SystemLogEntry( context, "Entity Update Error" )
				     {
					     Reason = e.Message,
					     Details = Details,
					     SubDetails = SubDetails
				     } );

				break;
			}
		}

		protected void AddToLog( IRequestContext context, Exception e )
		{
			Add( new SystemLogEntry( context, "Exception" )
			     {
				     Reason = $"Exception: {e.GetType().Name}",
				     Details = e.Message,
				     SubDetails = e.InnerException?.Message ?? ""
			     } );
			AddEntity( context, e );
		}


		protected void AddToLog( IRequestContext context, string message )
		{
			Add( new SystemLogEntry( context, "Message" )
			     {
				     Reason = message,
				     Details = "",
				     SubDetails = ""
			     } );
		}

		protected void AddToLog( IRequestContext context, string message, string details )
		{
			Add( new SystemLogEntry( context, "Message" )
			     {
				     Reason = message,
				     Details = details,
				     SubDetails = ""
			     } );
		}


		protected void AddToLog( IRequestContext context, string message, Exception e )
		{
			Add( new SystemLogEntry( context, "Exception" )
			     {
				     Reason = message,
				     Details = e.Message,
				     SubDetails = e.InnerException?.Message ?? ""
			     } );
			AddEntity( context, e );
		}

		public static void Add( IRequestContext context )
		{
			new SystemLog( context );
		}

		public static void Add( IRequestContext context, Exception e )
		{
			new SystemLog( context, e );
		}

		public static void Add( IRequestContext context, string message )
		{
			new SystemLog( context, message );
		}

		public static void Add( IRequestContext context, string message, string details )
		{
			new SystemLog( context, message, details );
		}

		public static void Add( IRequestContext context, string message, Exception e )
		{
			new SystemLog( context, message, e );
		}
	}
}