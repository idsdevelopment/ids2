﻿using IronPdf;

namespace Pdf
{
	public class Pdf
	{
		public static byte[] HtmlToPdf( string html )
		{
			var Convertor = new HtmlToPdf( new PdfPrintOptions
			                               {
				                               PaperSize = PdfPrintOptions.PdfPaperSize.A4,
				                               MarginBottom = 0,
				                               MarginLeft = 0,
				                               MarginRight = 0,
				                               MarginTop = 0,
			                               } );

			var Pdf = Convertor.RenderHtmlAsPdf( html );

			return Pdf.BinaryData;
		}
	}
}