﻿using System;
using System.ServiceModel;
using IdsRemoteService.PeekServiceReference;
using RemoteService;
using RemoteService.Logging;
using IdsDSWSClient = IdsRemoteService.IdsServiceReference.DSWSClient;
using PeekDSWSClient = IdsRemoteService.PeekServiceReference.PeekManagerWSClient;

namespace IdsRemote
{
    public class IdsClient : IdsDSWSClient
    {
        private LogFile _LogFile;

        public IdsClient( bool useDebugServer )
        {
            InnerChannel.OperationTimeout = TimeSpan.FromSeconds( RemmoteService.OperationTimeoutInSeconds );
            Endpoint.Address = new EndpointAddress( useDebugServer ? Globals.EndPoints.JBTEST : Globals.EndPoints.Servers[ 0 ].Main );
        }

        public IdsClient() : this( Globals.EndPoints.UseTestingServer )
        {
        }

        protected LogFile LogFile => _LogFile ?? ( _LogFile = new LogFile() );

        public void Log( string message )
        {
            LogFile.Log( message );
        }

        public void Log( Exception e )
        {
            LogFile.Log( e );
        }
    }

    public class PeekClient : PeekDSWSClient
    {
        private LogFile _LogFile;

        public PeekClient()
        {
            string GetServer()
            {
                InnerChannel.OperationTimeout = TimeSpan.FromSeconds( 1 );

                bool PingIt( string address )
                {
                    Endpoint.Address = new EndpointAddress( address );
                    var Resp = ping( new ping() );

                    var Ok = Resp?.@return;
                    return !string.IsNullOrEmpty( Ok ) && ( Ok == "OK" );
                }

                if( PingIt( Globals.EndPoints.PEEK_APP ) )
                    return Globals.EndPoints.TEST;

                if( PingIt( Globals.EndPoints.PEEK_TEST ) )
                    return Globals.EndPoints.TEST;

                throw new Exception( "No IDS Peek Server Available" );
            }

            InnerChannel.OperationTimeout = TimeSpan.FromMinutes( RemmoteService.OperationTimeoutInSeconds );
            Endpoint.Address = new EndpointAddress( GetServer() );
        }

        protected LogFile LogFile => _LogFile ?? ( _LogFile = new LogFile() );

        public void Log( string message )
        {
            LogFile.Log( message );
        }

        public void Log( Exception e )
        {
            LogFile.Log( e );
        }
    }
}