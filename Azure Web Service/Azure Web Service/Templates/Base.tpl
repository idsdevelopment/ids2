﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="/css/Main.css" rel="stylesheet" type="text/css">
{if $DEBUG}
    <script src="/javascripts/bridge/BridgeViews.js"></script>
{else}
    <script src="/javascripts/bridge/BridgeViews.min.js"></script>
{/if}
    {block name="head"}{/block}
</head>
<body>
    {block name="body"}{/block}
    {block name="script"}{/block}
</body>
</html>