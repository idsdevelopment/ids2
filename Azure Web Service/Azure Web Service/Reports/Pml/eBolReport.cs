﻿using System;
using System.Linq;
using Database.Model.Databases.Carrier;
using Interfaces.Interfaces;
using Protocol.Data;
using SendGridEmail;
using Storage.Carrier;
using Utils;

namespace Azure_Web_Service.Reports.Pml
{
	public static class PmlExtensions
	{
		public static string ToAurtNumber( this string txt )
		{
			var BLines = txt.Trim().Replace( "\r", "" ).Split( new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries );

			var Result = BLines.Length > 0 ? BLines[ 0 ].Trim() : "";

			return Result.StartsWith( "AURT" ) ? Result : "Missing Aurt Number";
		}
	}


	// ReSharper disable once InconsistentNaming
	public class eBolReport : ReportsModule.Reports
	{
		protected override string TemplateName => "Pml\\eBol.html";

		public eBolDataSource DataSource => Data.Source as eBolDataSource;


		public static async void PML_eBOL( IRequestContext context, string tripId, string emailAddresses = "" )
		{
			try
			{
				using var Db = new CarrierDb( context );

				var Trip = Db.SearchTrips( new SearchTrips
				                           {
					                           ByTripId = true,
					                           TripId = tripId
				                           } );

				if( Trip.Count > 0 )
				{
					var FromEmail = ( from P in Db.GetPreferences()
					                  where P.DevicePreference == CarrierDb.ADMIN_EMAIL
					                  select P.StringValue ).FirstOrDefault();

					if( FromEmail.IsNullOrWhiteSpace() )
						FromEmail = "NoReply@idsapp.com";

					var T = Trip[ 0 ];

					if( emailAddresses.IsNullOrWhiteSpace() )
					{
						emailAddresses = T.PickupAddressEmailAddress.Trim();

						if( emailAddresses.IsNullOrWhiteSpace() )
							return;
					}

					var Sigs = new Signatures( context ).GetSignatures( T.TripId );

					var EBol = new eBolReport( context );
					EBol.DataSource.AddTrip( T, "\\Reports\\Pml\\PmlLogo.png", Sigs );
					var Html = EBol.Print();
					var Aurt = T.BillingNotes.ToAurtNumber();

					var SClient = new Client( context, FromEmail, emailAddresses, $"PML Returns {Aurt}" );
					await SClient.SendHtmlAsPdf( $"{Aurt}.pdf", Html );
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}

		public eBolReport( IRequestContext context, PAPER_SIZES paperSize = PAPER_SIZES.A4, ORIENTATION orientation = ORIENTATION.PORTRAIT ) : base( context, new eBolDataSource(), paperSize, orientation )
		{
		}
	}
}