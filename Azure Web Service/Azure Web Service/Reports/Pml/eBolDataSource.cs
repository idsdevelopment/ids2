﻿using System;
using System.Collections.Generic;
using System.Text;
using DataSourceModule;
using Imaging;
using Protocol.Data;
using Utils;

// ReSharper disable InconsistentNaming

namespace Azure_Web_Service.Reports.Pml
{
	public sealed class eBolDataSource : DataSource
	{
		private const char MARKER = char.MaxValue;

		public class eBolItem
		{
			public string Description { get; set; } = "";
			public string Quantity { get; set; } = "";
		}

		public class Global
		{
			public string AurtNumber { get; set; } = "";
			public string NameAndAddress { get; set; } = "";
			public string Route { get; set; } = "";
			public string Date { get; set; } = "";
			public string CustomerNumber { get; set; } = "";

			public string POP { get; set; } = "";
			public string SignaturePngBase64 { get; set; } = "";
			public string LogoPngBase64 { get; set; } = "";
		}

		private readonly string MARKER_MARKER = MARKER + MARKER.ToString();
		private readonly string STR_MARKER = MARKER.ToString();

		public void AddTrip( Trip t, string logo, List<Signature> sigs )
		{
			if( Globals is Global G )
			{
				G.AurtNumber = t.BillingNotes.ToAurtNumber();

				G.CustomerNumber = t.AccountId;
				G.Route = t.Driver;
				G.Date = t.PickupTime.ToString( "d" );
				G.POP = t.POP;

				var Address = new StringBuilder();

				//Company Name
				Address.Append( t.PickupCompanyName.Trim() ).Append( MARKER );

				// Suite
				var Temp = t.PickupAddressSuite;

				if( Temp.IsNotNullOrWhiteSpace() )
					Address.Append( '#' ).Append( Temp.Trim() ).Append( ' ' );

				// Address Line 1 & 2
				Address.Append( t.PickupAddressAddressLine1.Trim() ).Append( MARKER );
				Address.Append( t.PickupAddressAddressLine2.Trim() ).Append( MARKER );

				// City & Region
				Address.Append( t.PickupAddressCity.Trim() ).Append( MARKER ).Append( t.PickupAddressRegion.Trim() ).Append( ' ' );

				// Postal Code
				Address.Append( t.PickupAddressPostalCode.Trim() );

				var Addr = Address.ToString();

				while( Addr.Contains( MARKER_MARKER ) )
					Addr = Addr.Replace( MARKER_MARKER, STR_MARKER );

				var AddressLines = Addr.Split( new[] { STR_MARKER }, StringSplitOptions.RemoveEmptyEntries );
				G.NameAndAddress = string.Join( "<br />", AddressLines );

				G.SignaturePngBase64 = sigs.ToPngBase64();

				G.LogoPngBase64 = FromFileSystem.GetImageBase64( logo );

				foreach( var Package in t.Packages )
				{
					foreach( var TripItem in Package.Items )
					{
						Add( new eBolItem
						     {
							     Description = TripItem.Description,
							     Quantity = TripItem.Pieces.ToString( "N0" )
						     } );
					}
				}
			}
		}


		public eBolDataSource()
		{
			Globals = new Global();
		}
	}
}