﻿#nullable enable

using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddUpdateInventory( InventoryList requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddUpdateInventory( requestObject );
			}

			public void ResponseAddUpdateInventoryItem( Inventory requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddUpdateInventory( requestObject );
			}

			public void ResponseDeleteInventory( InventoryList requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.DeleteInventory( requestObject );
			}

			public void ResponseDeleteInventoryItem( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.DeleteInventory( requestObject );
			}

			public InventoryList ResponseGetInventory()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetInventoryList();
			}

			public InventoryBarcode ResponseGetInventoryBarcode( string requestObject )
			{
				return Users.GetInventoryBarcode( requestObject );
			}
		}
	}
}