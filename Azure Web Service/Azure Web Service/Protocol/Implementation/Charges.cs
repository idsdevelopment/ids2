﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddUpdateCharges( Charges requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddUpdateCharges( requestObject );
			}
		}
	}
}