﻿using Database.Utils;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public bool ResponseIsIds()
			{
				return Context.IsIds;
			}

			public bool ResponseIsNewCarrier()
			{
				return Context.IsNewCarrier;
			}

			public long ResponseCreateCarrier( string carrierId, string adminId, string adminPassword )
			{
				return Company.CreateCompany( Context, carrierId, adminId, adminPassword );
			}
		}
	}
}