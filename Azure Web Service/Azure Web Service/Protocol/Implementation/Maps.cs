﻿using Protocol.Data;
using RoutingModule.Optimisation;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public Distances ResponseDistance( RouteOptimisationCompanyAddresses requestObject )
			{
				return new Routing( Context ).GetDistances( Context, requestObject );
			}
		}
	}
}