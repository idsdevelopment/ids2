﻿using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public long ResponseBeginCustomerImport( string requestObject )
			{
				var Retval = Users.StartProcess( Context );

				Task.Run( () =>
				          {
					          try
					          {
						          Task.Run( () =>
						                    {
							                    Context.LogProgramStarted( requestObject );
						                    } );

						          using var Db = new CarrierDb( Context );

						          Db.DeleteAllResellerCustomers();
					          }
					          finally
					          {
						          Users.EndProcess( Context, Retval );
					          }
				          } );

				return Retval;
			}

			public long ResponseImportCustomers( AddUpdateCustomer requestObject )
			{
				var Retval = Users.StartProcess( Context );

				Task.Run( () =>
				          {
					          try
					          {
						          ResponseAddUpdateCustomer( requestObject );
					          }
					          finally
					          {
						          Users.EndProcess( Context, Retval );
					          }
				          } );

				return Retval;
			}

			public string ResponseGenerateLoginCode( string firstName, string lastName, string addressLine1 )
			{
				return Users.GenerateResellerLoginCode( Context, firstName, lastName, addressLine1 );
			}

			public ResellerCustomerCompanyLookup ResponseGetCustomerCompanyAddress( string customerCode, string companyName )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerCompanyAddress( customerCode, companyName );
			}

			public string ResponseAddUpdateCustomer( AddUpdateCustomer requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.AddUpdateResellerCustomer( requestObject );
			}

			public CustomerLookupSummaryList ResponseCustomerLookup( CustomerLookup requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.CustomersLookup( requestObject );
			}

			public CustomerLookupSummary ResponseCustomerSummaryLookup( string customerAccountCode )
			{
				using var Db = new CarrierDb( Context );

				return Db.CustomerSummaryLookup( customerAccountCode );
			}

			public CompanyAddress ResponseGetResellerCustomerCompany( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetResellerCustomerCompany( requestObject );
			}

			public CompanyByAccountAndCompanyNameList ResponseCheckMissingCompanies( CompanyByAccountAndCompanyNameList requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.FindMissingCompanies( requestObject );
			}

			public bool ResponseCheckCompanyByCompanyAndAddressLine1( CompanyByAccountSummary requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.CheckCustomerCompanyAndAddressLine1( requestObject );
			}

			public ResellerCustomerCompanyLookup ResponseGetCustomerCompanyBySecondaryId( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerCompanySecondaryId( requestObject );
			}

			public CustomerCodeList ResponseGetCustomerCodeList()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerList();
			}
		}
	}
}