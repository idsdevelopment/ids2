﻿using System.Threading;
using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public bool ResponseIsDebug()
			{
				return Context.Debug;
			}
		}
	}
}
