﻿using Database.Model.Databases.Carrier;
using Protocol.Data.Customers.Priority;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public PriorityDeviceRoutes ResponsePriorityGetRouteNamesForDevice()
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.PriorityGetRouteNamesForDevice();
			}
		}
	}
}