﻿using Azure_Web_Service.Reports.Pml;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Protocol.Data._Customers.Pml;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public void ResponsePML_ClaimSatchels( ClaimSatchels requestObject )
			{
				using var Db = new CarrierDb( Context );
				var Satchels = Db.PML_ClaimSatchels( requestObject );

				foreach( var S in Satchels )
				{
					var Trip = S.ToTripUpdate( requestObject.ProgramName, false, true, true );

					// Remove the old trip
					var Save = Trip.Status1;
					Trip.Status1 = STATUS.NEW;
					BroadCast( Trip );

					// Move the trip
					Trip.Status1 = Save;
					BroadCast( Trip );
				}
			}

			public void ResponsePML_UpdateSatchel( Satchel requestObject )
			{
				using var Db = new CarrierDb( Context );
				var (Deleted, Satchel) = Db.PML_UpdateSatchel( requestObject );

				if( !( Deleted is null ) )
				{
					foreach( var Trip in Deleted )
						BroadCast( Trip.ToTripUpdate( requestObject.ProgramName, false, true, true ) );
				}

				if( !( Satchel is null ) )
					BroadCast( Satchel.ToTripUpdate( requestObject.ProgramName, false, true, true ) );

				if( !( Satchel is null ) )
					eBolReport.PML_eBOL( Context, Satchel.TripId );
			}

			public void ResponsePML_VerifySatchel( VerifySatchel requestObject )
			{
				using var Db      = new CarrierDb( Context );
				var       Satchel = Db.PML_VerifySatchel( requestObject );

				if( !( Satchel is null ) )
					BroadCast( Satchel.ToTripUpdate( requestObject.ProgramName, false, true, true ) );
			}

            public void ResponsePML_VerifySatchel( string programName, string satchelId, string userId )
			{
			}
		}
	}
}