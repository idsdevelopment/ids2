﻿using System.Threading.Tasks;
using Database.Model.Databases.Carrier;
using Database.Model.Databases.MasterTemplate;
using Protocol.Data;
using Protocol.Data._Customers.Pml;
using Address = Protocol.Data.Address;
using Company = Protocol.Data.Company;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public void ResponsePML_ClaimTrips( ClaimTrips requestObject )
			{
				using var Db = new CarrierDb( Context );

                foreach( var ClaimTrip in Db.PML_ClaimTrips( requestObject ) )
                {
	                var UpdateTrip = ClaimTrip.ToTripUpdate( requestObject.ProgramName, false, true, false );
	                var SaveStatus = UpdateTrip.Status1;
	                
					// Remove Current Trip From Drivers Board and Driver
	                UpdateTrip.Status1 = STATUS.NEW;
	                BroadCast( UpdateTrip );

	                UpdateTrip.Status1 = SaveStatus;
	                BroadCast( UpdateTrip );
                }
            }

			public TripList ResponsePML_FindTripsDetailedUpdatedSince( PmlFindSince requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.PML_FindTripsDetailedUpdatedSince( requestObject );
			}

			public TripUpdate ResponsePML_updateTripDetailedQuick( TripUpdate r )
			{
				using var Db = new CarrierDb( Context );

				void UpdateAccount( string customerCode, string barcode,
				                    string companyName, string suite, string addressLine1, string addressLine2, string city, string region, string postalCode, string country, string countryCode,
				                    string notes )
				{
					Task.Run( () =>
					          {
						          var Co = new AddUpdatePrimaryCompany( r.Program )
						                   {
							                   CustomerCode = customerCode,
							                   UserName = customerCode,
							                   SuggestedLoginCode = customerCode,
							                   Password = "",
							                   Company = new Company( new Address
							                                          {
								                                          Barcode = barcode,

								                                          Suite = suite,
								                                          AddressLine1 = addressLine1,
								                                          AddressLine2 = addressLine2,

								                                          City = city,
								                                          Region = region,
								                                          Country = country,
								                                          CountryCode = countryCode,
								                                          PostalCode = postalCode,

								                                          Notes = notes
							                                          } )
							                             {
								                             CompanyName = companyName,
								                             LocationBarcode = barcode
							                             }
						                   };

						          ResponseAddUpdatePrimaryCompany( Co );
					          } );
				}

				UpdateAccount( r.BillingAccountId, r.BillingAddressBarcode,
				               r.BillingCompanyName, r.BillingAddressSuite, r.BillingAddressAddressLine1, r.BillingAddressAddressLine2, r.BillingAddressCity, r.BillingAddressRegion, r.BillingAddressPostalCode, r.BillingAddressCountry,
				               r.BillingAddressCountryCode,
				               r.BillingAddressNotes );

				UpdateAccount( r.PickupAccountId, r.PickupAddressBarcode,
				               r.PickupCompanyName, r.PickupAddressSuite, r.PickupAddressAddressLine1, r.BillingAddressAddressLine2, r.PickupAddressCity, r.PickupAddressRegion, r.PickupAddressPostalCode, r.PickupAddressCountry,
				               r.PickupAddressCountryCode,
				               r.PickupAddressNotes );

				UpdateAccount( r.DeliveryAccountId, r.DeliveryAddressBarcode,
				               r.DeliveryCompanyName, r.DeliveryAddressSuite, r.DeliveryAddressAddressLine1, r.BillingAddressAddressLine2, r.DeliveryAddressCity, r.DeliveryAddressRegion, r.DeliveryAddressPostalCode, r.DeliveryAddressCountry,
				               r.DeliveryAddressCountryCode,
				               r.DeliveryAddressNotes );

				return BroadCast( Db.PML_updateTripDetailedQuick( r ) );
			}
		}
	}
}