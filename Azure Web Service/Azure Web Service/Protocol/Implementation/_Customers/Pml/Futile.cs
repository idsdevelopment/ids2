﻿using Database.Model.Databases.Carrier;
using Protocol.Data._Customers.Pml;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public void ResponsePML_Futile( Futile requestObject )
			{
				using var Db = new CarrierDb( Context );
				var TripList = Db.PML_Futile( requestObject );

				foreach( var Update in TripList.Trips )
					BroadCast( Update );
			}
		}
	}
}