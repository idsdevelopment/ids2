﻿using Database.Model.Databases.Carrier;
using Protocol.Data;
using Protocol.Data._Customers.Pml;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IIdsServer
		{
			public void ResponsePML_AddUpdateInventory( PmlInventoryList requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.PML_AddUpdateInventory( requestObject );
			}

			public void ResponsePML_DeleteInventory( PmlInventoryList requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.PML_DeleteInventory( requestObject );
			}

			public PmlInventoryList ResponsePML_GetInventory()
			{
				using var Db = new CarrierDb( Context );

				return Db.PML_GetInventoryList();
			}
		}
	}
}