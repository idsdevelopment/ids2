﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public long ResponsePML_PostCodeToRouteImport( string requestObject )
			{
				var Retval = Users.StartProcess( Context );

				Task.Run( () =>
				          {
					          try
					          {
						          using( var Db = new CarrierDb( Context ) )
							          Db.PML_PostCodeImport( requestObject );
					          }
					          finally
					          {
						          Users.EndProcess( Context, Retval );
					          }
				          } );

				return Retval;
			}
		}
	}
}