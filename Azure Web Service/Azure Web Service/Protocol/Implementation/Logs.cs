﻿using Logs;
using Protocol.Data;
using Storage.AuditTrails;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			private const string LOG_STORAGE = "Program";

			private void AuditTrailOp( string program, string text, string comment, AuditTrailEntryBase.OPERATION op )
			{
				UserAuditTrail.Add( LOG_STORAGE, new UserAuditTrailEntryEntry( Context, op, program, text, comment ) );
			}

			public void ResponseErrorLog( string requestObject )
			{
				new SystemLog( Context, requestObject );
				;
			}

			public void ResponseAuditTrailNew( string program, string text, string comment )
			{
				AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.NEW );
			}

			public void ResponseAuditTrailModify( string program, string text, string comment )
			{
				AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.MODIFY );
			}

			public void ResponseAuditTrailDelete( string program, string text, string comment )
			{
				AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.DELETE );
			}

			public void ResponseAuditTrailInform( string program, string text, string comment )
			{
				AuditTrailOp( program, text, comment, AuditTrailEntryBase.OPERATION.INFORM );
			}

			public Log ResponseGetLog( LogLookup requestObject )
			{
				switch( requestObject.Log )
				{
				case LOG.TRIP:
					return TripAuditTrail.GetLog( Context, requestObject );

				default:
					return UserAuditTrail.GetLog( Context, requestObject );
				}
			}
		}
	}
}