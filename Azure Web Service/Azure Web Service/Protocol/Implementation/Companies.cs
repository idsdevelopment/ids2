﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddCustomerCompany( AddCustomerCompany requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddCustomerCompany( requestObject );
			}

			public void ResponseDeleteCustomerCompany( DeleteCustomerCompany requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.DeleteCustomerCompany( requestObject );
			}

			public void ResponseUpdateCustomerCompany( UpdateCustomerCompany requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.UpdateCustomerCompany( requestObject );
			}

			public CustomerCompaniesList ResponseGetCustomerCompanyAddressList( CompanyByAccountAndCompanyNameList requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerCompanyAddress( requestObject );
			}


			public CompanyDetailList ResponseGetCustomerCompaniesDetailed( string customerName )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerCompaniesDetailed( customerName );
			}

			public CompanySummaryList ResponseGetCustomerCompaniesSummary( string companyName )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerCompaniesSummary( companyName );
			}
		}
	}
}