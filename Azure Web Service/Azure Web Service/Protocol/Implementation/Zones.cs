﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddUpdateZones( Zones requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.AddUpdateZones( requestObject, requestObject.ProgramName );
			}

			public Zones ResponseZones()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetZones();
			}
		}
	}
}