﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public PackageTypeList ResponseGetPackageTypes()
			{
				using var Db = new CarrierDb( Context );

				return Db.GerPackageTypes();
			}

			public void ResponseAddUpdatePackageType( PackageType requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddUpdatePackageType( requestObject );
			}
		}
	}
}