﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public BackupList ResponseBackupList()
			{
				using var Db = new CarrierDb( Context );

				return Db.BackupList();
			}

			public long ResponseBackupRestore( BackupRestore requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.BackupRestore( requestObject );
			}

			public bool ResponseDeleteBackups( BackupList requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.DeleteBackups( requestObject );
			}

			public long ResponseDeleteTestCarrier( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.DeleteTestCarrier( requestObject );
			}
		}
	}
}