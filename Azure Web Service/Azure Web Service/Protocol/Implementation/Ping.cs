﻿using System;
using Database.Model.Database;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public string ResponsePing()
			{
				return "OK";
			}

			public long ResponsePingTime()
			{
				return DateTime.UtcNow.Ticks;
			}

			public string ResponseStartUpSite()
			{
				// ReSharper disable once UnusedVariable
				var Cfg = Users.Configuration;

				return ResponsePing();
			}
		}
	}
}