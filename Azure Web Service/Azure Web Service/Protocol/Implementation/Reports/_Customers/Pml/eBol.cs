﻿using Azure_Web_Service.Reports.Pml;

// ReSharper disable IdentifierTypo

namespace Ids
{
    public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public void ResponsePML_ResendeBOL( string tripId, string emailAddress )
			{
				eBolReport.PML_eBOL( Context, tripId, emailAddress );
			}
		}
	}
}