﻿using System;
using System.Text;
using Interfaces.Interfaces;
using Logs;
using Storage;

#if !DISABLE_TELEMETRY
using Microsoft.ApplicationInsights.Extensibility;

#endif

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public IRequestContext ExceptionContext
			{
				get
				{
					var Ctx = Context ?? new RequestContext.RequestContext
					                     {
						                     CarrierId = RequestType,
						                     UserName = UserHostName,
						                     IpAddress = UserHostAddress
					                     };

					return Ctx;
				}
			}

		#if !DISABLE_TELEMETRY
			private static void DisableApplicationInsightsOnDebug()
			{
				TelemetryConfiguration.Active.DisableTelemetry = true;
			}
		#endif

			private Exception DoNoKeyException( Exception exception )
			{
				if( exception is IdsServerExtensions.KeyNotPresentException )
				{
					var Temp = new StringBuilder( $"{exception.Message}\r\n" );

					var Request = HttpContext.Request;
					var Keys = Request.Form.AllKeys;

					var Form = Request.Form;

					foreach( var Key in Keys )
						Temp.Append( $"Key: {Key} Post Data: {Form[ Key ]}\r\n" );

					exception = new Exception( Temp.ToString().Trim(), exception );
				}

				return exception;
			}

			partial void InitialiseExceptions()
			{
			#if !DISABLE_TELEMETRY
				DisableApplicationInsightsOnDebug();
			#endif
				OnAbortConnectionException = exception =>
				                             {
					                             var Request = HttpContext.Request;
					                             BadRequestLog.Add( ExceptionContext, Request.Url.OriginalString, DoNoKeyException( exception ) );
				                             };

				OnException = exception =>
				              {
					              SystemLog.Add( Context ?? ExceptionContext, HttpContext.Request.Url.OriginalString, DoNoKeyException( exception ) );
				              };
			}
		}
	}
}