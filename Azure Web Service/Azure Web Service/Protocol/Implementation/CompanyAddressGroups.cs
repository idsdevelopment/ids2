﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddUpdateCompanyAddressGroup( CompanyAddressGroup requestObject )
			{
                using var Db = new CarrierDb( Context );
                Db.AddUpdateCompanyAddressGroup( requestObject );
            }

			public void ResponseAddCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.AddCompanyAddressGroupEntry( requestObject );
			}

			public void ResponseDeleteCompanyAddressGroup( int requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.DeleteCompanyAddressGroup( requestObject );
			}

			public void ResponseDeleteCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.DeleteCompanyAddressGroupEntry( requestObject );
			}

			public CompaniesWithinAddressGroups ResponseGetCompaniesWithinAddressGroup( int requestObject )
			{
				using var Db = new CarrierDb( Context );
				return Db.GetCompaniesWithinAddressGroup( requestObject );
			}

			public CompanyAddressGroups ResponseGetCompanyAddressGroups()
			{
				using var Db = new CarrierDb( Context );
				return Db.GetCompanyAddressGroups();
			}
		}
	}
}