﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public Settings ResponseImportSettings()
			{
				return new CarrierDb( Context ).GetImportSettings();
			}

			public void ResponseUpdateImportSettings( Settings requestObject )
			{
				new CarrierDb( Context ).UpdateImportSettings( requestObject );
			}
		}
	}
}