﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using Database.Model.Databases.Carrier;
using Protocol.Data;
using Utils;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			private TripUpdate BroadCast( TripUpdate t )
			{
				if( t.BroadcastToDispatchBoard || t.BroadcastToDriver || t.BroadcastToDriverBoard )
				{
					var Msg = new TripUpdateMessage
					          {
						          Action     = TripUpdateMessage.UPDATE.TRIP,
						          TripIdList = new List<string> { t.TripId }
					          };

					if( t.BroadcastToDriver )
					{
						var Driver = t.Driver.Trim();

						if( Driver.IsNotNullOrWhiteSpace() )
							CarrierDb.Driver_BroadcastMessage( Context, Driver, Msg );
					}

					if( t.BroadcastToDispatchBoard )
						CarrierDb.DispatchBoard_BroadcastMessage( Context, t.Board, Msg );

					if( t.BroadcastToDriverBoard )
						CarrierDb.DriversBoard_BroadcastMessage( Context, t.Board, Msg );
				}

				return t;
			}

			private void SendStatus( string board, TripUpdateMessage statusUpdate )
			{
				CarrierDb.DriversBoard_BroadcastStatusMessage( Context, board, statusUpdate );
			}


			private void SendStatus( string tripId, (string Board, bool ReceivedByDevice, bool ReadByDriver, STATUS Status, STATUS1 Status1 ) status )
			{
				var (Board, ReceivedByDevice, ReadByDriver, Status, Status1) = status;

				SendStatus( Board, new TripUpdateMessage
				                   {
					                   Action = TripUpdateMessage.UPDATE.TRIP_STATUS,

					                   TripIdList       = new List<string> { tripId },
					                   Status           = Status,
					                   Status1          = Status1,
					                   ReadByDriver     = ReadByDriver,
					                   ReceivedByDevice = ReceivedByDevice
				                   } );
			}

			private static void FixStatus( TripUpdate t )
			{
				if( t.Status1 == STATUS.DISPATCHED )
					t.Status2 = STATUS1.UNSET;
			}

			public string ResponseGetNextTripId() => CarrierDb.NextTripId( Context );

			public bool ResponseTripExits( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.TripExists( requestObject );
			}

			public void ResponseTripReadByDriver( string program, string tripId )
			{
				using var Db = new CarrierDb( Context );

				SendStatus( tripId, Db.TripReadByDriver( program, tripId ) );
			}

			public void ResponseTripReceivedByDevice( string program, string tripId )
			{
				using var Db = new CarrierDb( Context );

				SendStatus( tripId, Db.TripReceivedByDevice( program, tripId ) );
			}

			public void ResponseAddUpdateTrip( TripUpdate requestObject )
			{
				FixStatus( requestObject );
				BroadCast( requestObject );

				using var Db = new CarrierDb( Context );

				Db.AddUpdateTrip( requestObject );
			}

			public void ResponseAddUpdateTrips( TripUpdateList requestObject )
			{
				using var Db = new CarrierDb( Context );

				foreach( var Update in requestObject.Trips )
				{
					Update.Program = requestObject.Program;
					FixStatus( Update );
					BroadCast( Update );

					if( Update.Status1 != STATUS.DELETED )
						Db.AddUpdateTrip( Update );
				}
			}

			public TripList ResponseSearchTrips( SearchTrips requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.SearchTrips( requestObject );
			}

			public Trip ResponseGetTrip( GetTrip requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GeTrip( requestObject );
			}

			public TripList ResponseGetTripsByStatus( StatusRequest requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetTripsByStatus( requestObject );
			}

			public void ResponsePushTripsToDriver( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				var Trips = Db.GetTripsForDriver( ref requestObject );

				CarrierDb.Driver_BroadcastMessage( Context, requestObject, new TripUpdateMessage
				                                                           {
					                                                           Action = TripUpdateMessage.UPDATE.TRIP,
					                                                           TripIdList = ( from T in Trips
					                                                                          select T.TripId ).ToList()
				                                                           } );
			}

			public TripList ResponseGetTripsForDriver( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				var Result = new TripList();
				Result.AddRange( Db.GetTripsForDriver( ref requestObject ) );

				return Result;
			}

			public TripList ResponseGetTripsStartingWith( SearchTripsByStatus requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetTripsStartingWith( requestObject );
			}

			public void ResponseUpdateTripFromDevice( DeviceTripUpdate requestObject )
			{
				using var Db = new CarrierDb( Context );

				SendStatus( requestObject.TripId, Db.UpdateTripFromDevice( requestObject ) );
			}
		}
	}
}