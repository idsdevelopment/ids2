﻿using Protocol.Data;
using Storage;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseClientCrashLog( CrashReport requestObject )
			{
				ClientCrashLog.Add( requestObject );
			}
		}
	}
}