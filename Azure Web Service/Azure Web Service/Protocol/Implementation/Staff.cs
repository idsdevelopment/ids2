﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public StaffLookupSummaryList ResponseStaffLookup( StaffLookup requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.StaffLookup( requestObject );
			}

			public bool ResponseStaffMemberExists( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.StaffMemberExists( requestObject );
			}

			public StaffList ResponseGetStaff()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetStaffAsProtocolStaff();
			}

			public Staff ResponseGetStaffMember( string staffId )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetStaffMember( staffId );
			}

			public void ResponseUpdateStaffMember( UpdateStaffMemberWithRolesAndZones requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddUpdateStaffMember( requestObject );
			}

			public Notes ResponseGetStaffNotes( string staffId )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetStaffNotes( staffId );
			}

			public void ResponseRenameStaffNote( string programName, string staffId, string oldNoteName, string newNoteName )
			{
				using var Db = new CarrierDb( Context );

				Db.RenameStaffNote( programName, staffId, oldNoteName, newNoteName );
			}

			public void ResponseAddUpdateStaffNote( string programName, string staffId, string noteName, string note )
			{
				using var Db = new CarrierDb( Context );

				Db.AddStaffUpdateNote( programName, staffId, noteName, note );
			}

			public void ResponseDeleteStaffNote( string programName, string staffId, string noteName )
			{
				using var Db = new CarrierDb( Context );

				Db.DeleteStaffNote( programName, staffId, noteName );
			}

			public StaffLookupSummaryList ResponseGetDrivers()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetDrivers();
			}
		}
	}
}