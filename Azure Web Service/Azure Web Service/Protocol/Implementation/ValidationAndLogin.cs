﻿using System;
using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Interfaces.Interfaces;
using Protocol.Data;
using Storage.AuditTrails;
using Utils;

// ReSharper disable UseDeconstruction

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			private const string TIME_ERROR = "TimeError";

			private const int SECONDS_THRESHOLD = 5 * 60;

			private static readonly long SecondsThresholdTicks = new TimeSpan( 0, 0, SECONDS_THRESHOLD ).Ticks;

			private RequestContext.RequestContext Context;

			private bool IdsSpecialLogin( string employee, out bool isNewCarrier )
			{
				isNewCarrier = false;

				var Emp = Users.Employee( Context, employee );

				if( !( Emp is null ) )
				{
					var EPoints = Users.GetEndPoints( Context );

					if( EPoints is null )
					{
						isNewCarrier = true;

						return Emp.AllowCreation;
					}

					return true;
				}

				return false;
			}

			private (bool UserNameOk, bool PasswordOk, int UserId, bool Ok) UserLogin( IRequestContext context )
			{
				using var Db = new CarrierDb( Context );

				return Db.LoginOk( context.UserName, Context.Password );
			}

			private static bool CheckUserTime( string userTime )
			{
				var (Value, Ok) = Encryption.FromTimeLimitedToken( userTime );

				if( Ok )
				{
					if( long.TryParse( Value, out var UserTicks ) )
					{
						var NowTicks = DateTime.UtcNow.Ticks;
						var Diff     = Math.Abs( UserTicks - NowTicks );

						return Diff <= SecondsThresholdTicks;
					}
				}

				return false;
			}

			public string ResponseLogin( string pgm, string carrierId, string userName, string password, string timeZoneOffset, string version, string isDebugServer, Func<string, string, bool> validate = null )
			{
				if( !sbyte.TryParse( timeZoneOffset, out var TimeZoneOffset ) )
					throw new AbortConnectionException();

				var Debug     = isDebugServer.ToBool();
				var TimeError = true;

				var UserName = Encryption.FromTimeLimitedToken( userName );

				if( UserName.Ok )
				{
					var Password = Encryption.FromTimeLimitedToken( password );

					if( Password.Ok )
					{
						var CarrierId = Encryption.FromTimeLimitedToken( carrierId );

						if( CarrierId.Ok )
						{
							TimeError = false;

							var P = Password.Value;
							var U = UserName.Value;
							var C = CarrierId.Value;

							var CidUsr = C.ToLower().Split( '/', '\\' );

							// Special IDS Login
							if( ( CidUsr.Length == 2 ) && ( CidUsr[ 0 ] == "ids" ) )
							{
								Context = new RequestContext.RequestContext
								          {
									          CarrierId      = CidUsr[ 1 ],
									          Password       = P,
									          UserName       = U,
									          TimeZoneOffset = TimeZoneOffset,
									          Debug          = Debug
								          };

								if( IdsSpecialLogin( U, out var IsNewCarrier ) )
								{
									Context.IsIds        = true;
									Context.IsNewCarrier = IsNewCarrier;

									return Encryption.ToTimeLimitedToken( Context.ToAuthToken() );
								}
							}

							var Ip = HttpContext.Request.UserHostAddress;
							Context = RequestContext.RequestContext.ToRequestContext( C, U, P, TimeZoneOffset, Debug, Ip );

							if( CarrierDb.HasCarrierId( Context ) )
							{
								var (_, _, UserId, Ok) = UserLogin( Context );

								if( Ok && ( ( validate == null ) || validate( C, U ) ) )
								{
									Context.UserId = UserId;

									Task.Run( () => { UserAuditTrail.Add( Context.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Context, AuditTrailEntryBase.OPERATION.SIGN_IN, $"{pgm} - {version}" ) ); } );

									return RequestContext.RequestContext.ToAuthToken( C, U, P, TimeZoneOffset, Debug, UserId, HttpContext.Request.UserHostAddress );
								}
							}
						}
					}
				}

				return TimeError ? TIME_ERROR : "";
			}

			public string ResponseResellerCustomerLogin( string pgm, string c, string u, string p, string timeZoneOffset, string version, string isDebugServer )
			{
				if( !sbyte.TryParse( timeZoneOffset, out var TimeZoneOffset ) )
					throw new AbortConnectionException();

				var Debug = isDebugServer.ToBool();

				var TimeError = false;

				var Company = Encryption.FromTimeLimitedToken( c );

				if( Company.Ok )
				{
					var (CarrierId, UserName, Password, Ok) = Users.GetResellerFromLoginCode( Company.Value );

					if( Ok )
					{
						var UName = Encryption.FromTimeLimitedToken( u );

						if( UName.Ok )
						{
							if( string.Compare( UName.Value, UserName, StringComparison.InvariantCultureIgnoreCase ) == 0 )
							{
								var PWord = Encryption.FromTimeLimitedToken( p );

								if( PWord.Ok )
								{
									var C = CarrierId;
									var U = UName.Value;
									var P = PWord.Value;

									if( P == Password )
									{
										var Ip = HttpContext.Request.UserHostAddress;
										Context = RequestContext.RequestContext.ToRequestContext( C, U, P, TimeZoneOffset, Debug, Ip );

										Task.Run( () => { UserAuditTrail.Add( Context.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Context, AuditTrailEntryBase.OPERATION.SIGN_IN, version ) ); } );

										return Context.ToAuthToken();
									}
								}
								else
									TimeError = true;
							}
						}
						else
							TimeError = true;
					}
				}
				else
					TimeError = true;

				return TimeError ? TIME_ERROR : "";
			}

			public bool ValidateAuthorisation( string authToken )
			{
				try
				{
					Context = RequestContext.RequestContext.ToRequestContext( authToken );

					if( Context is null )
						throw new AbortConnectionException( "Request Context NULL" );

					return true;
				}
				catch( Exception E )
				{
					throw new AbortConnectionException( E.Message );
				}
			}

			public string ResponseLogin( string pgm, string c, string u, string p, string t, string v, string d, string ut ) => CheckUserTime( ut ) ? ResponseLogin( pgm, c, u, p, t, v, d ) : TIME_ERROR;

			public string ResponseLoginAsDriver( string pgm, string c, string u, string p, string t, string v, string d, string ut )
			{
				var Temp = ResponseLogin( pgm, c, u, p, t, v, d, ( c1, u1 ) =>
				                                                 {
					                                                 using var Db = new CarrierDb( Context );

					                                                 return Db.IsStaffMemberADriver( u1 );
				                                                 } );

				if( Temp.IsNotNullOrWhiteSpace() && ( Temp != TIME_ERROR ) )
				{
					ClearTripUpdateQueue( Messaging.DRIVERS, u );
				}

				return Temp;
			}

			public string ResponseResellerCustomerLogin( string pgm, string c, string u, string p, string t, string v, string d, string ut ) => CheckUserTime( ut ) ? ResponseResellerCustomerLogin( pgm, c, u, p, t, v, d ) : TIME_ERROR;

			public void ResponseSignOut( SignOut requestObject )
			{
				Task.Run( () =>
				          {
					          var Data = $"{requestObject.Hours:D2}:{requestObject.Minutes:D2}:{requestObject.Seconds:D2}";
					          UserAuditTrail.Add( Context.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Context, AuditTrailEntryBase.OPERATION.SIGN_OUT, Data ) );
				          } );
			}
		}
	}
}