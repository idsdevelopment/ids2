﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public string ResponseAddUpdatePrimaryCompany( AddUpdatePrimaryCompany requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.AddUpdatePrimaryCompany( requestObject );
			}

			public PrimaryCompany ResponseGetPrimaryCompanyByCompanyName( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetPrimaryCompanyByCompanyName( requestObject );
			}

			public PrimaryCompany ResponseGetPrimaryCompanyByCustomerCode( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetPrimaryCompanyByCustomerCode( requestObject );
			}

			public PrimaryCompany ResponseGetPrimaryCompanyByLoginCode( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetPrimaryCompanyByLoginCode( requestObject );
			}

			public CustomerCodeList ResponseGetPrimaryCompanyList()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetCustomerList();
			}
		}
	}
}