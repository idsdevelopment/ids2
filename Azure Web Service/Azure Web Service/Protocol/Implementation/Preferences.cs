﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public ClientPreferences ResponseClientPreferences()
			{
				using var Db = new CarrierDb( Context );

				return Db.ClientPreferences();
			}

			public DevicePreferences ResponseDevicePreferences()
			{
				using var Db = new CarrierDb( Context );

				return Db.DevicePreferences();
			}

			public bool ResponseIsPasswordValid( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.IsPasswordValid( requestObject );
			}

			public Preferences ResponsePreferences()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetPreferences();
			}

			public void ResponseUpdatePreference( Preference requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.UpdatePreference( requestObject );
			}
		}
	}
}