﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public Roles ResponseRoles()
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.GetRoles();
			}

			public Roles ResponseStaffMemberRoles( string requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.GetStaffMemberRoles( requestObject );
			}

			public void ResponseAddUpdateRole( Role requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddUpdateRole( requestObject );
			}

			public void ResponseRenameRole( RoleRename requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.RenameRole( requestObject );
			}

			public void ResponseDeleteRoles( DeleteRoles requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.DeleteRoles( requestObject );
			}
		}
	}
}