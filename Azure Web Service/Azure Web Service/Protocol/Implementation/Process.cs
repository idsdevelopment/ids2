﻿using Database.Model.Database;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public string ResponseGetProcessingResponse( long requestObject )
			{
				return Users.GetProcessingResponse( Context, requestObject );
			}

			public bool ResponseIsProcessRunning( long requestObject )
			{
				return Users.IsProcessRunning( Context, requestObject );
			}
		}
	}
}