﻿#nullable enable

using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseAddUpdateServiceLevel( ServiceLevel requestObject )
			{
				using var Db = new CarrierDb( Context );

				Db.AddUpdateServiceLevel( requestObject );
			}

			public ServiceLevelColours ResponseGetServiceLevelColours()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetServiceLevelColours();
			}

			public ServiceLevelList ResponseGetServiceLevels()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetServiceLevels();
			}

			public ServiceLevelListDetailed ResponseGetServiceLevelsDetailed()
			{
				using var Db = new CarrierDb( Context );

				return Db.GetServiceLevelsDetailed();
			}

			public ServiceLevelResult ResponseGetServiceLevel( string requestObject )
			{
				using var Db = new CarrierDb( Context );

				return Db.GetServiceLevel( requestObject );
			}
		}
	}
}