﻿using System.Linq;
using Protocol.Data;
using Storage.Carrier;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public void ResponseGps( GpsPoints requestObject )
			{
				if( requestObject.Count > 0 )
				{
					var Points = ( from Ro in requestObject
					               select new GpsLog.GpsPoint { Longitude = Ro.Longitude, Latitude = Ro.Longitude, DateTime = Ro.LocalDateTime } ).ToList();
					new GpsLog( Context ).Append( Points );
				}
			}
		}
	}
}