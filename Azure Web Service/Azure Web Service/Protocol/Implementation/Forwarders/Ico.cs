﻿using System.Web;
using Azure_Web_Service.Protocol.Implementation.Forwarders;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public byte[] Ico_Default_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				return Cache.Read( path );
			}
		}
	}
}