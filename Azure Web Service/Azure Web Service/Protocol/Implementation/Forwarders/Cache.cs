﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using System.Web.Hosting;

namespace Azure_Web_Service.Protocol.Implementation.Forwarders
{
	public static class FileExtensions
	{
		public static byte[] ReadAllBytes( this string path )
		{
			try
			{
				var Path = HostingEnvironment.MapPath( path );

				return !( Path is null ) ? File.ReadAllBytes( Path ) : new byte[ 0 ];
			}
			catch
			{
			}

			return new byte[ 0 ];
		}
	}

	public class CacheEntry
	{
		public byte[] Page;
		public DateTime TimeStamp = DateTime.UtcNow;
	}

	public class Cache : Dictionary<string, CacheEntry>
	{
		private static readonly Cache Cach = new Cache();

#if DEBUG
		private static int _TimeoutInMinutes = 1;
#else
		private static int _TimeoutInMinutes = 10;
#endif

		// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
		private static Timer CleanUpTimer;

		public static int TimeoutInMinutes
		{
			get
			{
				lock( Cach )
					return _TimeoutInMinutes;
			}

			set
			{
				lock( Cach )
					_TimeoutInMinutes = value;
			}
		}

		private Cache()
		{
			CleanUpTimer = new Timer( 60 * 1000 ) { AutoReset = true };

			CleanUpTimer.Elapsed += ( sender, args ) =>
			                        {
				                        var Now = DateTime.UtcNow;

				                        lock( Cach )
				                        {
					                        foreach( var Key in from Kv in Cach
					                                            where ( Now - Kv.Value.TimeStamp ).Minutes >= _TimeoutInMinutes
					                                            select Kv.Key )
						                        Cach.Remove( Key );
				                        }
			                        };
			CleanUpTimer.Start();
		}


		private static bool TryGetValue( string pageId, out byte[] page )
		{
			lock( Cach )
			{
				if( ( (Dictionary<string, CacheEntry>)Cach ).TryGetValue( pageId, out var Entry ) )
				{
#if !DEBUG
					Entry.TimeStamp = DateTime.UtcNow;
#endif
					page = Entry.Page;

					return true;
				}

				page = null;

				return false;
			}
		}

		private static void Add( string pageId, byte[] page )
		{
			lock( Cach )
			{
				Cach[ pageId ] = new CacheEntry
				                 {
					                 Page = page
				                 };
			}
		}

		public static byte[] Read( string fileName )
		{
			lock( Cach )
			{
				if( TryGetValue( fileName, out var Entry ) )
					return Entry;
			}

			var Bytes = fileName.ReadAllBytes();

			if( Bytes.Length > 0 )
				Add( fileName, Bytes );

			return Bytes;
		}
	}
}