﻿using System.Text;
using System.Web;
using Azure_Web_Service.Protocol.Implementation.Forwarders;
using PulsarV3;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			private const string SESSION_ID = "Plugh_Xyzzy";

			// No Session Checking
			public byte[] Index_Tpl_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				var Bytes = Cache.Read( path );

				var Page   = Encoding.UTF8.GetString( Bytes );
				var Pulsar = new Pulsar { Reader = Cache.Read };

				var Dirs = Pulsar.TemplateDirectories;
				Dirs.Add( "\\Templates" );

#if DEBUG
				Pulsar.Assign( "DEBUG", true );
#endif
				Page = Pulsar.Fetch( Page );

				return Encoding.UTF8.GetBytes( Page );
			}

			// No Session Checking
			public byte[] Signin_Tpl_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				return Index_Tpl_Forwarder( path, request, response );
			}

			public byte[] Tpl_Default_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				var Cookie = request.Cookies[ SESSION_ID ];

				if( !( Cookie is null ) )
				{
					try
					{
						var Ctx = RequestContext.RequestContext.ToRequestContextNotTimeLimited( Cookie.Value );

						if( !( Ctx is null ) )
							return Index_Tpl_Forwarder( path, request, response );
					}
					catch
					{
					}
				}

				return new byte[ 0 ];
			}
		}
	}
}