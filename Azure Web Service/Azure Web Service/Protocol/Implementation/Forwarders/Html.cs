﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using Azure_Web_Service.Protocol.Implementation.Forwarders;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public byte[] Html_Default_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				return Cache.Read( path );
			}
		}
	}
}