﻿using System.Text;
using System.Web;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public byte[] Robots_Txt_Forwarder( string path, HttpRequest request, HttpResponse response )
			{
				return Encoding.UTF8.GetBytes( "User-agent: *\r\nDisallow: /" );
			}
		}
	}
}