﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public AddressIdList ResponseCheckMissingAddressesBySecondaryId( AddressIdList requestObject )
			{
				using var Db = new CarrierDb( Context );

				return new AddressIdList( Db.FindMissingAddressesBySecondaryId( requestObject ) );
			}
		}
	}
}