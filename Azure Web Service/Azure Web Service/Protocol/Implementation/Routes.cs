﻿using System.Threading.Tasks;
using Database.Model.Database;
using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
			public long ResponseBeginRouteImport( string requestObject )
			{
				var Retval = Users.StartProcess( Context );

				Task.Run( () =>
				          {
					          try
					          {
						          using( var Db = new CarrierDb( Context ) )
							          Db.DeleteAllRoutes();
					          }
					          finally
					          {
						          Users.EndProcess( Context, Retval );
					          }
				          } );

				return Retval;
			}

			public long ResponseImportRoutes( RouteAndCompanyAddresses requestObject )
			{
				var Retval = Users.StartProcess( Context );

				Task.Run( () =>
				          {
					          try
					          {
						          using( var Db = new CarrierDb( Context ) )
							          Db.AddUpdateRoute( requestObject );
					          }
					          finally
					          {
						          Users.EndProcess( Context, Retval );
					          }
				          } );

				return Retval;
			}

			public void ResponseAddUpdateRoute( Route requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddUpdateRoute( requestObject );
			}

			public RouteLookupSummaryList ResponseRouteLookup( RouteLookup requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.RouteLookup( requestObject );
			}

			public void ResponseUpdateRouteSchedule( RouteScheduleUpdate requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.UpdateRouteSchedule( requestObject );
			}

			public RouteCompanySummaryList ResponseGetCompanySummaryInRoute( string customerCode, string routeName )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.GetCompanySummaryInRoute( customerCode, routeName );
			}

			public bool ResponseHasRoute( string customerCode, string routeName )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.HasRoute( customerCode, routeName );
			}

			public void ResponseAddRoute( string programName, string customerCode, string routeName )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddRoute( programName, customerCode, routeName );
			}

			public void ResponseDeleteRoute( string programName, string customerCode, string routeName )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.DeleteRoute( programName, customerCode, routeName );
			}

			public void ResponseRenameRoute( string programName, string customerCode, string oldRouteName, string newRouteName )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.RenameRoute( programName, customerCode, oldRouteName, newRouteName );
			}

			public void ResponseRemoveCompaniesFromRoute( RouteCompanyList requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.RemoveCompaniesFromRoute( requestObject );
			}

			public void ResponseAddCompaniesToRoute( RouteCompanyList requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddCompaniesToRoute( requestObject );
			}

			public void ResponseUpdateOptionsInRoute( RouteUpdate requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.UpdateOptionsInRoute( requestObject );
			}

			public CustomerRoutesScheduleList ResponseGetRoutesForCustomers( CustomerCodeList requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.GetRoutesForCustomers( requestObject );
			}

			public void ResponseEnableRouteForCustomer( EnableRouteForeCustomer requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.EnableRouteForCustomer( requestObject );
			}

			public CustomersRoutesBasic ResponseGetCustomersRoutesBasic( CustomerCodeList requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					return Db.GetCustomersRoutesBasic( requestObject );
			}

			public void ResponseAddUpdateRenameRouteBasic( AddUpdateRenameRouteBasic requestObject )
			{
				using( var Db = new CarrierDb( Context ) )
					Db.AddUpdateRenameRouteBasic( requestObject );
			}
		}
	}
}