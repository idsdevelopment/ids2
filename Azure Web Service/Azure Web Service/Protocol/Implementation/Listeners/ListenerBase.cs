﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Database.Model.Databases.Carrier;
using Interfaces.Interfaces;
using Newtonsoft.Json;
using Utils;

// ReSharper disable MemberHidesStaticFromOuterClass

// ReSharper disable StaticMemberInGenericType

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation
		{
		#region MessageBase

			public abstract class AMessaging<TData> where TData : class
			{
				public class Packet
				{
					public TData Data { get; set; }
					public long Id { get; set; }
				}

				private static Packet Convert( string data, long id )
				{
					return new Packet
					       {
						       Id   = id,
						       Data = JsonConvert.DeserializeObject<TData>( data )
					       };
				}

				private static string Convert( TData data )
				{
					return JsonConvert.SerializeObject( data );
				}

				public void RemoveMessages( IRequestContext context, IEnumerable<long> messageIds )
				{
					MessageBase.CarrierMessageTasks.RemoveMessages( context, messageIds );
				}

				public void ListenAck( IRequestContext context, List<long> messageIds )
				{
					MessageBase.CarrierMessageTasks.ListenAck( context, messageIds );
				}

				public void Listen( IRequestContext context, string topic, string queue, out List<Packet> data )
				{
					var Result = new List<Packet>();
					data = Result;

					if( MessageBase.CarrierMessageTasks.Listen( context, topic, queue, typeof( TData ).FullName, out var Data ) && !( Data is null ) )
					{
						foreach( var DataAndIdType in Data )
							Result.Add( Convert( DataAndIdType.Data, DataAndIdType.Id ) );
					}
				}

				public void SendToQueue( IRequestContext context, string topic, string queue, IEnumerable<TData> data )
				{
					MessageBase.CarrierMessageTasks.SendToQueue( context, topic, queue, typeof( TData ).FullName, from D in data
					                                                                                              select new CarrierDb.DataAndIdType
					                                                                                                     {
						                                                                                                     Data = Convert( D ),
						                                                                                                     Type = typeof( TData ).FullName
					                                                                                                     } );
				}

				internal void ClearQueue( IRequestContext context, string topic, string queue )
				{
					MessageBase.CarrierMessageTasks.ClearQueue( context, topic, queue );
				}
			}

			private static class MessageBase
			{
				private const int CONNECTION_TIME_LIMIT_IN_MINUTES = 5;

				internal class CarrierMessageTasks : Dictionary<string, Task>
				{
					private static DateTime LastCleanupTime = DateTime.UtcNow;
					private static readonly object CleanupLock = new object();


					private static readonly Dictionary<string, CarrierContext> CarrierContexts = new Dictionary<string, CarrierContext>();
					private static readonly Dictionary<IRequestContext, List<long>> ToDelete = new Dictionary<IRequestContext, List<long>>();


					private static readonly Dictionary<IRequestContext, List<AddEntry>> ToAdd = new Dictionary<IRequestContext, List<AddEntry>>();

					private static readonly CarrierMessage PendingMessages = new CarrierMessage();

					private static readonly Dictionary<IRequestContext, List<CarrierDb.Listener>> ListenerUpdate = new Dictionary<IRequestContext, List<CarrierDb.Listener>>();

					// ReSharper disable once UnusedMember.Local
					private static Task Process = Task.Run( () =>
					                                        {
						                                        while( true )
						                                        {
							                                        try
							                                        {
								                                        while( true )
								                                        {
									                                        //Update Listeners First
									                                        lock( ListenerUpdate )
									                                        {
										                                        foreach( var Lu in ListenerUpdate )
										                                        {
											                                        using var Db = new CarrierDb( Lu.Key );
											                                        Db.UpdateListener( Lu.Value );
										                                        }

										                                        ListenerUpdate.Clear();
									                                        }

									                                        // Do Deletes First
									                                        Monitor.Enter( ToDelete );

									                                        while( ToDelete.Count > 0 )
									                                        {
										                                        var Entry   = ToDelete.First();
										                                        var Context = Entry.Key;
										                                        var List    = Entry.Value;

										                                        Monitor.Exit( ToDelete );

										                                        using var Db = new CarrierDb( Context );
										                                        Db.DeleteMessagesById( List );

										                                        Monitor.Enter( ToDelete );

										                                        // May have had entries added in background
										                                        var NewList = ( from D in ToDelete[ Context ]
										                                                        where !List.Contains( D )
										                                                        select D ).ToList();

										                                        if( NewList.Count > 0 )
											                                        ToDelete[ Context ] = NewList;
										                                        else
											                                        ToDelete.Remove( Context );
									                                        }

									                                        Monitor.Exit( ToDelete );

									                                        //----------------- Update Pending Messages ------------------------
									                                        List<IRequestContext> Contexts;

									                                        lock( LockObject )
									                                        {
										                                        Contexts = ( from C in CarrierContexts
										                                                     select C.Value.Context ).ToList();
									                                        }

									                                        foreach( var RequestContext in Contexts )
									                                        {
										                                        using var Db       = new CarrierDb( RequestContext );
										                                        var       Messages = Db.GetPendingMessages();
										                                        var       Cid      = RequestContext.CarrierId;

										                                        lock( LockObject )
										                                        {
											                                        PendingMessages.Clear( Cid );

											                                        foreach( var (Topic, Queue, Packet, Id, Type) in Messages )
											                                        {
												                                        PendingMessages.Add( Cid, Topic, Queue, new CarrierDb.DataAndIdType
												                                                                                {
													                                                                                Data = Packet,
													                                                                                Type = Type,
													                                                                                Id   = Id
												                                                                                } );
											                                        }
										                                        }
									                                        }

									                                        //----------------- Adds ------------------------

									                                        Monitor.Enter( ToAdd );

									                                        if( ToAdd.Count > 0 )
									                                        {
										                                        var Entry      = ToAdd.First();
										                                        var Context    = Entry.Key;
										                                        var AddEntries = Entry.Value;

										                                        var Entries = new Dictionary<string, Dictionary<string, List<CarrierDb.DataAndIdType>>>();

										                                        // Consolidate into Topic, Queue
										                                        foreach( var AddEntry in AddEntries )
										                                        {
											                                        var T = AddEntry.Topic;
											                                        var Q = AddEntry.Queue;

											                                        if( !Entries.TryGetValue( T, out var Queue ) )
												                                        Entries.Add( T, Queue = new Dictionary<string, List<CarrierDb.DataAndIdType>>() );

											                                        if( !Queue.TryGetValue( Q, out var List ) )
												                                        Queue.Add( Q, List = new List<CarrierDb.DataAndIdType>() );

											                                        List.Add( AddEntry );
										                                        }

										                                        Monitor.Exit( ToAdd );

										                                        var Ids = new List<long>();

										                                        using var Db = new CarrierDb( Context );

										                                        foreach( var Entry1 in Entries )
										                                        {
											                                        var Topic = Entry1.Key;

											                                        foreach( var Entry2 in Entry1.Value )
											                                        {
												                                        var Queue = Entry2.Key;
												                                        var Data  = Entry2.Value;

												                                        Db.SendToQueue( Topic, Queue, Data );

												                                        foreach( var DataAndIdType in Data )
													                                        Ids.Add( DataAndIdType.Id );
											                                        }
										                                        }

										                                        Monitor.Enter( ToAdd );

										                                        // May have had entries added in background
										                                        var NewList = ( from D in ToAdd[ Context ]
										                                                        where !Ids.Contains( D.Id )
										                                                        select D ).ToList();

										                                        if( NewList.Count > 0 )
											                                        ToAdd[ Context ] = NewList;
										                                        else
											                                        ToAdd.Remove( Context );
									                                        }

									                                        Monitor.Exit( ToAdd );

									                                        // Cleanup
									                                        var  Now = DateTime.UtcNow;
									                                        bool DoCleanup;

									                                        lock( CleanupLock )
									                                        {
										                                        DoCleanup = ( Now - LastCleanupTime ).Hours >= 1;

										                                        if( DoCleanup )
											                                        LastCleanupTime = Now;
									                                        }

									                                        if( DoCleanup )
									                                        {
										                                        foreach( var RequestContext in Contexts )
										                                        {
											                                        using var Db = new CarrierDb( RequestContext );
											                                        Db.CleanUpMessages();
										                                        }
									                                        }

									                                        //Cleanup Contexts
									                                        Now = Now.AddMinutes( -10 );

									                                        lock( CarrierContexts )
									                                        {
										                                        var ToDel = new List<string>();

										                                        foreach( var Ctx in CarrierContexts )
										                                        {
											                                        if( Ctx.Value.DateTime < Now )
											                                        {
												                                        var Context = Ctx.Value.Context;

												                                        foreach( var Add in ToAdd )
												                                        {
													                                        if( Add.Key == Context )
														                                        goto Next;
												                                        }

												                                        foreach( var Del in ToDelete )
												                                        {
													                                        if( Del.Key == Context )
														                                        goto Next;
												                                        }

												                                        ToDel.Add( Ctx.Key );
											                                        }

											                                        Next: ;
										                                        }

										                                        foreach( var Key in ToDel )
											                                        CarrierContexts.Remove( Key );
									                                        }

									                                        Thread.Sleep( 2000 );
								                                        }
							                                        }
							                                        catch( ThreadAbortException ) // IIS Timeout
							                                        {
								                                        return;
							                                        }
							                                        catch
							                                        {
							                                        }

							                                        Thread.Sleep( 1000 );
						                                        }
					                                        } );


					private class CarrierContext
					{
						internal IRequestContext Context;
						internal DateTime DateTime;
					}

					private class AddEntry : CarrierDb.DataAndIdType
					{
						internal string Topic,
						                Queue;
					}

					private static void UpdateListener( IRequestContext context, string topic, string queue, string messageType )
					{
						lock( ListenerUpdate )
						{
							if( !ListenerUpdate.TryGetValue( context, out var L ) )
								ListenerUpdate.Add( context, L = new List<CarrierDb.Listener>() );

							foreach( var Listener in L )
							{
								if( ( Listener.Topic == topic ) && ( Listener.Queue == queue ) && ( Listener.MessageType == messageType ) )
									return;
							}

							L.Add( new CarrierDb.Listener
							       {
								       Topic       = topic,
								       Queue       = queue,
								       MessageType = messageType
							       } );
						}
					}

					private static IRequestContext AddCarrierContext( IRequestContext context )
					{
						var CarrierId = context.CarrierId.TrimToLower();

						lock( CarrierContexts )
						{
							if( !CarrierContexts.TryGetValue( CarrierId, out var Context ) )
								CarrierContexts.Add( CarrierId, Context = new CarrierContext { Context = context } );

							Context.DateTime = DateTime.UtcNow;

							return Context.Context;
						}
					}


					internal static void RemoveMessages( IRequestContext context, IEnumerable<long> messageIds )
					{
						context = AddCarrierContext( context );

						lock( ToDelete )
						{
							if( !ToDelete.TryGetValue( context, out var List ) )
								ToDelete.Add( context, List = new List<long>() );

							List.AddRange( messageIds );
						}
					}

					internal static void ListenAck( IRequestContext context, ICollection<long> packetIds )
					{
						context = AddCarrierContext( context );

						lock( LockObject )
						{
							// Delete pending messages with these Id's
							foreach( var Topics in PendingMessages )
							{
								foreach( var Queues in Topics.Value )
								{
									foreach( var DataPair in Queues.Value )
									{
										var Data = DataPair.Value;

										if( !( Data is null ) )
										{
											for( var Ndx = Data.Count; --Ndx >= 0; )
											{
												if( packetIds.Contains( Data[ Ndx ].Id ) )
													Data.RemoveAt( Ndx );
											}
										}
									}
								}
							}

							if( !ToDelete.TryGetValue( context, out var Packets ) )
								ToDelete.Add( context, Packets = new List<long>() );

							Packets.AddRange( packetIds );
						}
					}

					// Waits for packets in memory
					// Memory gets loaded from background task.
					// Only return packets of the same type
					internal static bool Listen( IRequestContext context, string topic, string queue, string messageType, out List<CarrierDb.DataAndIdType> data )
					{
						var Result = new List<CarrierDb.DataAndIdType>();
						data = Result;

						try
						{
							context = AddCarrierContext( context );

							topic = topic.TrimToLower();
							queue = queue.TrimToLower();

							UpdateListener( context, topic, queue, messageType );

							for( var I = CONNECTION_TIME_LIMIT_IN_MINUTES * 60; I-- >= 0; )
							{
								lock( LockObject )
								{
									if( PendingMessages.TryGetValue( context.CarrierId, topic, queue, out var Packets ) )
									{
										if( !( Packets is null ) && ( Packets.Count > 0 ) )
										{
											string Type = null;

											foreach( var Packet in Packets )
											{
												if( Type is null )
													Type = Packet.Type;
												else if( Type != Packet.Type )
													break;

												Result.Add( Packet );
											}

											return true;
										}
									}
								}

								Thread.Sleep( 1000 );
							}
						}
						catch( ThreadAbortException ) // Probably connection timed out
						{
						}
						catch( Exception Exception )
						{
							context.SystemLogException( Exception );
						}

						return false;
					}

					internal static void SendToQueue( IRequestContext context, string topic, string queue, string messageType, IEnumerable<CarrierDb.DataAndIdType> packets )
					{
						context = AddCarrierContext( context );

						lock( ToAdd )
						{
							if( !ToAdd.TryGetValue( context, out var List ) )
								ToAdd.Add( context, List = new List<AddEntry>() );

							List.AddRange( from Packet in packets
							               select new AddEntry
							                      {
								                      Data  = Packet.Data,
								                      Topic = topic.TrimToLower(),
								                      Queue = queue.TrimToLower(),
								                      Type  = messageType.TrimToLower()
							                      } );
						}
					}

					internal static void ClearQueue( IRequestContext context, string topic, string queue )
					{
						context = AddCarrierContext( context );
						using var Db = new CarrierDb( context );
						Db.ClearQueue( topic, queue );
					}
				}

				private static readonly object LockObject = new object();

				// ReSharper disable once UnusedMember.Global
				// ReSharper disable once UnusedMember.Local
				internal static readonly CarrierMessageTasks CarrierTasks = new CarrierMessageTasks();

				private class Queue : Dictionary<string, List<CarrierDb.DataAndIdType>>
				{
					internal void Add( string queue, CarrierDb.DataAndIdType data )
					{
						lock( LockObject )
						{
							if( TryGetValue( queue, out var Data ) && !( Data is null ) )
								Data.Add( data );
							else
								base.Add( queue, new List<CarrierDb.DataAndIdType> { data } );
						}
					}
				}

				private class Topic : Dictionary<string, Queue>
				{
					internal void Add( string topic, string queue, CarrierDb.DataAndIdType data )
					{
						lock( LockObject )
						{
							if( !TryGetValue( topic, out var Queue ) )
								Add( topic, Queue = new Queue() );
							Queue.Add( queue, data );
						}
					}

					internal bool TryGetValue( string topic, string queue, out List<CarrierDb.DataAndIdType> data )
					{
						lock( LockObject )
						{
							if( TryGetValue( topic, out var Queue ) )
							{
								if( !( Queue is null ) )
									return Queue.TryGetValue( queue, out data );
							}
						}

						data = null;

						return false;
					}
				}


				private class CarrierMessage : Dictionary<string, Topic>
				{
					internal void Add( string carrierId, string topic, string queue, CarrierDb.DataAndIdType data )
					{
						carrierId = carrierId.TrimToLower();
						topic     = topic.TrimToLower();
						queue     = queue.TrimToLower();

						lock( LockObject )
						{
							if( !TryGetValue( carrierId, out var Topic ) )
								Add( carrierId, Topic = new Topic() );

							Topic.Add( topic, queue, data );
						}
					}

					internal bool TryGetValue( string carrierId, string topic, string queue, out List<CarrierDb.DataAndIdType> data )
					{
						carrierId = carrierId.TrimToLower();
						topic     = topic.TrimToLower();
						queue     = queue.TrimToLower();

						lock( LockObject )
						{
							if( TryGetValue( carrierId, out var Topic ) )
							{
								if( !( Topic is null ) )
									return Topic.TryGetValue( topic, queue, out data );
							}
						}

						data = null;

						return false;
					}

					internal void Clear( string carrierId )
					{
						carrierId = carrierId.TrimToLower();

						lock( LockObject )
						{
							if( TryGetValue( carrierId, out var Topic ) )
								Topic?.Clear();
						}
					}
				}
			}
		}

	#endregion
	}
}