﻿using System.Collections.Generic;
using System.Linq;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public class ListenTripUpdate : AMessaging<TripUpdateMessage>
			{
				internal static ListenTripUpdate TripUpdateListener = new ListenTripUpdate();
			}

			public void ClearTripUpdateQueue( string topic, string queue )
			{
				ListenTripUpdate.TripUpdateListener.ClearQueue( Context, topic, queue );
			}

			public List<TripUpdatePacket> ResponseListenTripUpdate( string topic, string queue )
			{
				var Result = new List<TripUpdatePacket>();

				try
				{
					ListenTripUpdate.TripUpdateListener.Listen( Context, topic, queue, out var Packets );

					if( !( Packets is null ) )
					{
						Result.AddRange( from P in Packets
						                 select new TripUpdatePacket
						                        {
							                        Data = P.Data,
							                        Id   = P.Id
						                        } );
					}
				}
				catch
				{
				}

				return Result;
			}

			public bool ResponseListenTripUpdateAck( List<long> idList )
			{
				ListenTripUpdate.TripUpdateListener.ListenAck( Context, idList );

				return true;
			}

			public bool ResponseTripUpdateReceiveTripUpdateMessage( string t, string q, List<TripUpdateMessage> receivedObject )
			{
				ListenTripUpdate.TripUpdateListener.SendToQueue( Context, t, q, receivedObject );

				return true;
			}
		}
	}
}