﻿using Database.Model.Databases.Carrier;
using Protocol.Data;

namespace Ids
{
	public partial class IdsServer
	{
		public partial class IdsServerImplementation : IdsServer
		{
			public void ResponseLocationDeliveries( LocationDeliveries requestObject )
			{
				using var Db = new CarrierDb( Context );
				Db.LocationDeliveries( requestObject );
			}
		}
	}
}