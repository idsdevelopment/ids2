﻿using System.Linq;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitUsing( SendReceiveParser.UsingContext context )
		{
			var Result = new UsingResult();
			var U = context.USING();
			if( U != null )
				Result.Name = U.GetText();

			return Result;
		}

		public override Result VisitUsings( SendReceiveParser.UsingsContext context )
		{
			var Usings = new UsingsResult();
			var U = context.USING();
			if( U != null )
			{
				Usings.Names.AddRange( from Node in U
				                       let Text = Node.GetText().Trim()
				                       where !string.IsNullOrEmpty( Text )
				                       select Text );
			}

			return Usings;
		}
	}
}