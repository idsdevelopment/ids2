﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitArgs( SendReceiveParser.ArgsContext context )
		{
			var Result = new ArgsResult();

			var Args = context.ARG();
			foreach( var Arg in Args )
				Result.Args.Add( Arg.GetText().Trim() );

			return Result;
		}
	}
}