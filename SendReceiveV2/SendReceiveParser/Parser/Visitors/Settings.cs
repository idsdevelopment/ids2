﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitSettings( SendReceiveParser.SettingsContext context )
		{
			var Result = new SettingsResult();
			var Settings = context.setting();
			if( Settings != null )
			{
				foreach( var Setting in Settings )
				{
					var MaxR = Setting.MAX_REQUESTS();
					if( MaxR != null )
					{
						if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var MaxRequests ) )
							Result.Settings.MaxRequests = MaxRequests;
					}
					else
					{
						var RTimeout = Setting.REQUEST_TIMEOUT();
						if( RTimeout != null )
						{
							if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var RequestTimeout ) )
								Result.Settings.RequestTimeout = RequestTimeout;
						}
						else
						{
							var Page = Setting.PAGE();
							if( Page != null )
							{
								var Id = Setting.IDENT();
								if( Id != null )
									Result.Settings.Page = Id.GetText().Trim();
							}
							else
							{
								var Connections = Setting.MAX_CONNECTIONS();
								if( Connections != null )
								{
									if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var MaxConnections ) )
										Result.Settings.MaxConnections = MaxConnections;
								}
								else
								{
									var ETimeout = Setting.EXECUTION_TIMEOUT();
									if( ETimeout != null )
									{
										if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var ExecutionTimeout ) )
											Result.Settings.ExecutionTimeout = ExecutionTimeout;
									}
									else
									{
										var Rtys = Setting.RETRIES();
										if( Rtys != null )
										{
											if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var Retries ) )
												Result.Settings.Retries = Retries;
										}
										else
										{
											var Delay = Setting.RETRY_DELAY();
											if( Delay != null )
											{
												int.TryParse( Setting.NUMBER().GetText().Trim(), out var RetryDelay );
												Result.Settings.RetryDelay = RetryDelay;
											}
											else
											{
												var Path = Setting.PATH();
												if( !( Path is null ) )
												{
													var PName = Setting.PATH_NAME();
													if( !( PName is null ) )
														Result.Settings.Path = PName.GetText().Trim();
												}
												else
												{
													var BPath = Setting.BASE_PATH();
													if( !( BPath is null ) )
													{
														var PName = Setting.PATH_NAME();
														if( !( PName is null ) )
															Result.Settings.BasePath = PName.GetText().Trim();
													}
													else
													{
														var LDelay = Setting.LISTENER_RETRY_DELAY();
														if( !( LDelay is null ) )
														{
															if( int.TryParse( Setting.NUMBER().GetText().Trim(), out var ListenerRetryDelay ) )
																Result.Settings.ListenerRetryDelay = ListenerRetryDelay;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			return Result;
		}
	}
}