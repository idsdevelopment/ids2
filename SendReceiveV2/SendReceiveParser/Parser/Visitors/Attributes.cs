﻿using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitProtocolObjectAttribute( SendReceiveParser.ProtocolObjectAttributeContext context )
		{
			var Summary = (ProtocolSummary)VisitProtocolSummary( context.protocolSummary() );

			var Result = new ProtocolAttribute
			             {
							 Summary = Summary.Summary
			             };

			var Auth = context.AUTH_TOKEN();
			if( Auth != null )
			{
				Result.IsAuthorisationAttribute = true;
				Result.Name = Auth.GetText().Trim();
			}
			else
			{
				Result.NoAuthorisationRequired = context.NO_AUTH_TOKEN() != null;

				var Attr = context.ATTRIBUTE();

				if( Attr != null )
					Result.Name = Attr.GetText().Trim();
			}

			return Result;
		}

		public override Result VisitProtocolAttribute( SendReceiveParser.ProtocolAttributeContext context )
		{
			return new AuthorisationAttribute { AuthorisationRequired = context.AUTH_TOKEN() != null };
		}
	}
}