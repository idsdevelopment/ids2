﻿using System.Collections.Generic;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitParse( SendReceiveParser.ParseContext context )
		{
			var Name = (NameResult)VisitName( context.name() );

			var Settings = ( (SettingsResult)VisitSettings( context.settings() ) ).Settings;

			var Result = new ParseResult
			             {
				             Name = Name.Name,
				             Setting =
				             {
					             MaxRequests        = Settings.MaxRequests,
					             RequestTimeout     = Settings.RequestTimeout,
					             MaxConnections     = Settings.MaxConnections,
					             ExecutionTimeout   = Settings.ExecutionTimeout,
					             ListenerRetryDelay = Settings.ListenerRetryDelay,
					             Retries            = Settings.Retries,
					             RetryDelay         = Settings.RetryDelay,
					             Page               = Settings.Page,
					             Path               = Settings.Path,
					             BasePath           = Settings.BasePath
				             },
				             Usings     = ( (UsingsResult)VisitUsings( context.usings() ) ).Names,
				             Protocols  = (ProtocolsResult)VisitProtocols( context.protocols() ),
				             Forwarders = ( (ForwarderResult)VisitForwarders( context.forwarders() ) ).Forwarders
			             };

			return Result;
		}
	}
}