﻿using System;
using System.Collections.Generic;
using SendReceive.Parser.Visitors;

namespace SendReceive.Parser
{
	internal partial class SendReceiveVisitors
	{
		public override Result VisitForwarders( SendReceiveParser.ForwardersContext context )
		{
			var Result = new ForwarderResult();

			if( !( context is null ) )
			{
				foreach( var Forwarder in context.forwarder() )
				{
					var Fords = new List<Forwarder>();
					Result.Forwarders.Add( Fords );

					var F = Forwarder.FORWARDER().GetText().Trim();

					var Types = F.Split( ',', StringSplitOptions.RemoveEmptyEntries );

					var Attribute = Forwarder.FORWARDER_ATTRIBUTE();
					var Attr      = Attribute?.GetText().Trim().Trim( '[', ']' ).Trim();

					// ReSharper disable once LoopCanBeConvertedToQuery
					foreach( var Type in Types )
					{
						var Frd = new Forwarder
						          {
							          Name = Type.Trim( ' ', '"', ';' )
						          };

						if( !( Attr is null ) )
							Frd.MimeType = Attr;

						Fords.Add( Frd );
					}
				}
			}

			return Result;
		}
	}
}