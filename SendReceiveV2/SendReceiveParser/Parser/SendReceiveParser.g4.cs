﻿using System.Collections.Generic;

namespace SendReceive.Parser
{
    public partial class SendReceiveParser
    {
	    public bool HasErrors;
	    public List<string> SyntaxErrors = new List<string>();
    }
}
