﻿using System.Collections.Generic;
using System.IO;
using Antlr4.Runtime;
using SendReceive.Lexer;
using SendReceive.Parser;
using SendReceive.Parser.Visitors;

namespace SendReceiveParser
{
	public class SendReceive
	{
		internal class ErrorListener : BaseErrorListener
		{
			private readonly global::SendReceive.Parser.SendReceiveParser Parser;

			public override void SyntaxError( IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg,
			                                  RecognitionException e )
			{
				Parser.HasErrors = true;
				Parser.SyntaxErrors.Add( $"Line: {line},  Position: {charPositionInLine},  Symbol: '{offendingSymbol.Text.Trim()}'  Error: {msg.Trim()}" );
			}

			public ErrorListener( global::SendReceive.Parser.SendReceiveParser parser )
			{
				Parser = parser;
			}
		}
		public List<string> ErrorList;
		public bool HasErrors;

		public ParseResult Parse( string text )
		{
			var InStream = new AntlrInputStream( text );
			var Parser = new global::SendReceive.Parser.SendReceiveParser( new CommonTokenStream( new SendReceiveLexer( InStream ) ) );
			Parser.RemoveErrorListeners();
			Parser.AddErrorListener( new ErrorListener( Parser ) );

			var Tree = Parser.parse();
			HasErrors = Parser.HasErrors;
			ErrorList = Parser.SyntaxErrors;

			if( !Parser.HasErrors )
			{
				var Result = (ParseResult)new SendReceiveVisitors().Visit( Tree );
				return Result;
			}

			return null;
		}

		public void Parse( Stream inputStream )
		{
			Parse( new StreamReader( inputStream ).ReadToEnd() );
		}
	}
}