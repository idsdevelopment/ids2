﻿using System;

namespace PulsarV3
{
	public class SilentException : Exception
	{
	}

	public class PulsarException : Exception
	{
		public PulsarException( string messaage ) : base( messaage )
		{
		}
	}
}