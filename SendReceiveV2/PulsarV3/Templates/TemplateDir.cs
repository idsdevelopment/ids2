﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PulsarV3.Utils;

namespace PulsarV3
{
	public partial class Pulsar
	{
		public List<string> TemplateDirectories { get; private set; } = new List<string>();

		public string TemplateDirectory => _TemplateDirectory;

		public string CurrentDir => _TemplateDirectory;

		public string TemplateName => _TemplateName;

		public string template => _TemplateName;

		public Func<string, byte[]> Reader;

		public void RegisterTemplate( string fileName, string template )
		{
			TemplateCache.Add( fileName, Encoding.UTF8.GetBytes( template ) );
		}

		internal MemoryStream GetTemplate( string path, string fileName, out string templatePath )
		{
			var Path = Util.AddPathSeparator( path ) + fileName;

			if( TemplateCache.ContainsKey( Path ) )
			{
				templatePath = Path;

				return new MemoryStream( TemplateCache[ Path ] );
			}

			try
			{
				byte[] Buffer;

				if( !( Reader is null ) )
					Buffer = Reader( Path );
				else
				{
					if( File.Exists( Path ) )
					{
						using var Template = new FileStream( Path, FileMode.Open, FileAccess.Read, FileShare.Read );
						var       Len      = (int)Template.Length;
						Buffer = new byte[ Len ];
						Template.Read( Buffer, 0, Len );
					}
					else
					{
						templatePath = null;

						return null;
					}
				}

				TemplateCache.Add( Path, Buffer );

				templatePath = Path;

				return new MemoryStream( Buffer );
			}
			catch
			{
				throw new PulsarException( "Cannot open file: " + fileName );
			}
		}

		internal MemoryStream GetTemplate( string fileName, out string templatePath )
		{
			fileName = fileName.Trim( '"', '\'' );

			if( fileName.IndexOf( "..", StringComparison.Ordinal ) >= 0 )
				throw new PulsarException( "File name: " + fileName + "cannot contain \"..\"" );

			if( Path.IsPathRooted( fileName ) )
				return GetTemplate( Path.GetDirectoryName( fileName ), Path.GetFileName( fileName ), out templatePath );

			if( TemplateCache.TryGetValue( fileName, out var Buffer ) )
			{
				templatePath = fileName;

				return new MemoryStream( Buffer );
			}

			var Ndx = 0;

			foreach( var Dir in TemplateDirectories )
			{
				var Temp = GetTemplate( Dir, fileName, out templatePath );

				if( Temp != null )
				{
					if( Ndx != 0 )
					{
						TemplateDirectories.RemoveAt( Ndx );
						TemplateDirectories.Insert( 0, Dir );
					}

					return Temp;
				}

				++Ndx;
			}

			throw new PulsarException( "Template not found: " + fileName );
		}
	}
}