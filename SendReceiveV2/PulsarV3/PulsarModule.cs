﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace PulsarV3
{
	public abstract class PulsarModule
	{
		public string AssemblyName
		{
			get
			{
				if( MyType is null )
					GetMyType();

				return _AssemblyName;
			}
		}

		public string ModuleClass
		{
			get
			{
				if( MyType is null )
					GetMyType();

				return _ModuleClass;
			}
		}

		private Type MyType;

		private string _AssemblyName,
		               _ModuleClass;


		private void GetMyType()
		{
			MyType = GetType();
			_AssemblyName = MyType.Assembly.GetName().Name;
			_ModuleClass = MyType.FullName;
		}


		public abstract void InitialiseModule( Pulsar pulsar, Dictionary<string, string> args );

		public virtual string GetModuleTemplate()
		{
			return "";
		}


		protected string GetStringResource( string resourceName )
		{
			try
			{
				var Asmb = Assembly.GetExecutingAssembly();

				// ReSharper disable once AssignNullToNotNullAttribute
				var TextStreamReader = new StreamReader( Asmb.GetManifestResourceStream( resourceName ) );

				return TextStreamReader.ReadToEnd();
			}
			catch
			{
				throw new Exception( "Error accessing resources: " + resourceName );
			}
		}
	}
}