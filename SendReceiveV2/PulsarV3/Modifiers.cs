﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace PulsarV3
{
	public partial class Pulsar
	{
		public Pulsar RegisterModifier( string name, Func<object, List<string>, string> func )
		{
			if( Modifiers.ContainsKey( name ) )
				Modifiers[ name ] = func;
			else
				Modifiers.Add( name, func );

			return this;
		}

		internal Func<object, List<string>, string> GetModifier( string name )
		{
			var P = this;

			do
			{
				var M = P.Modifiers;

				if( M.ContainsKey( name ) )
					return M[ name ];

				P = P.Parent;
			} while( P != null );

			return null;
		}

		internal object ModifyValue( string modifierName, object value, List<string> args )
		{
			var Func = GetModifier( modifierName );

			return Func != null ? Func( value, args ) : value;
		}

		internal void SetDefaultModifiers()
		{
			RegisterModifier( "Length", ( value, args ) => value.ToString().Length.ToString( CultureInfo.InvariantCulture ) );

			RegisterModifier( "count_characters",
			                  ( value, args ) => value.ToString().Length.ToString( CultureInfo.InvariantCulture ) );

			RegisterModifier( "capitalise",
			                  ( value, args ) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase( value.ToString().ToLower() ) );

			RegisterModifier( "lower", ( value, args ) => value.ToString().ToLower() );

			RegisterModifier( "ToLower", ( value, args ) => value.ToString().ToLower() );

			RegisterModifier( "upper", ( value, args ) => value.ToString().ToUpper() );

			RegisterModifier( "ToUpper", ( value, args ) => value.ToString().ToUpper() );

			RegisterModifier( "Trim", ( value, args ) => value.ToString().Trim() );

			RegisterModifier( "TrimEnd", ( value, args ) => value.ToString().TrimEnd() );

			RegisterModifier( "TrimStart", ( value, args ) => value.ToString().TrimStart() );

			RegisterModifier( "Substring", ( value, args ) =>
			                               {
				                               var Str = value.ToString();

				                               var Count = args.Count;

				                               if( Count > 0 )
				                               {
					                               int Arg0;

					                               if( !int.TryParse( args[ 0 ], out Arg0 ) )
						                               Arg0 = 0;

					                               if( Count > 1 )
					                               {
						                               int Arg1;

						                               if( !int.TryParse( args[ 0 ], out Arg1 ) )
							                               Arg1 = 0;

						                               return Str.Substring( Arg0, Arg1 );
					                               }

					                               return Str.Substring( Arg0 );
				                               }

				                               return "";
			                               } );

			RegisterModifier( "Format",
			                  ( value, args ) => args.Count > 0
				                                     ? string.Format( args[ 0 ], value )
				                                     : value.ToString()
			                );
		}

		public Dictionary<string, Func<object, List<string>, string>> Modifiers { get; private set; }
	}
}