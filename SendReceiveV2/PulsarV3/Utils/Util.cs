﻿using System.Collections.Generic;
using System.IO;

namespace PulsarV3.Utils
{
	public class Util
	{
		public class Stack<T> : List<T>
		{
			public T Peek
			{
				get
				{
					var Ndx = Count - 1;

					return Ndx >= 0 ? this[ Ndx ] : default;
				}

				set
				{
					var Ndx = Count - 1;

					if( Ndx >= 0 )
						this[ Ndx ] = value;
					else
						Add( value );
				}
			}

			public void Push( T t )
			{
				Add( t );
			}

			public T Pop()
			{
				var Ndx = Count - 1;

				if( Ndx >= 0 )
				{
					var RetVal = this[ Ndx ];
					RemoveAt( Ndx );

					return RetVal;
				}

				return default;
			}
		}

		public static string AddPathSeparator( string FullPath )
		{
			return FullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar ) + Path.DirectorySeparatorChar;
		}
	}
}