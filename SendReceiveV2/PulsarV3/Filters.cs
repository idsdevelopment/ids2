﻿using System;

namespace PulsarV3
{
	public partial class Pulsar
	{
		public Func<string, Pulsar, string> RegisterPreFilter( Func<string, Pulsar, string> func )
		{
			var Retval = PreFilter;
			PreFilter = func;

			return Retval;
		}

		public Func<string, Pulsar, string> RegisterPosFilter( Func<string, Pulsar, string> func )
		{
			var Retval = PostFilter;
			PostFilter = func;

			return Retval;
		}
	}
}