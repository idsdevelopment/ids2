﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PulsarV3
{
	public partial class Pulsar
	{
		internal Pulsar Parent;

		internal string _TemplateDirectory = "";
		internal string _TemplateName = "";

		internal NewOutputBuffer Output = new NewOutputBuffer();

		public List<string> SyntaxErrors;
		public bool HasErrors => SyntaxErrors.Count > 0;

		private Func<string, Pulsar, string> PreFilter;
		private Func<string, Pulsar, string> PostFilter;

		private Dictionary<string, byte[]> TemplateCache;

		internal enum TEMPLATE_SCOPE
		{
			TEMPLATE,
			THIS,
			PARENT,
			GLOBAL
		}

	#if DEBUG
		public uint Timeout = 1000000;
	#else
        public uint Timeout = 5000;
	#endif

		public string this[ string variableName ]
		{
			get => GetAssign( variableName );

			set => Assign( variableName, value );
		}

		internal string GetVariableName( string identString )
		{
			var Id = new StringBuilder();

			var InIdent = false;

			foreach( var C in identString )
			{
				if( ( ( C >= 'A' ) && ( C <= 'Z' ) ) || ( ( C >= 'a' ) && ( C <= 'z' ) ) || ( ( C >= '0' ) && ( C <= '9' ) ) || ( C == '_' ) )
				{
					InIdent = true;
					Id.Append( C );
				}
				else if( InIdent )
					break;
			}

			return Id.ToString();
		}

		internal bool IsVariableName( string identString )
		{
			identString = identString.Trim();

			return identString.StartsWith( "$" ) || identString.StartsWith( "{$" );
		}

		internal string GetVariable( string identString, TEMPLATE_SCOPE scope )
		{
			identString = GetVariableName( identString ); // Might start with $

			switch( scope )
			{
			case TEMPLATE_SCOPE.PARENT:
				if( Parent != null )
					return Parent[ identString ];
				goto default;

			case TEMPLATE_SCOPE.GLOBAL:
				var P = this;

				while( P.Parent != null )
					P = P.Parent;

				return P[ identString ];

			default:
				return this[ identString ];
			}
		}

		internal object GetVariableAsObject( string identString, TEMPLATE_SCOPE scope )
		{
			identString = GetVariableName( identString ); // Might start with $

			switch( scope )
			{
			case TEMPLATE_SCOPE.PARENT:
				if( Parent != null )
					return Parent.GetAssignAsObject( identString );
				goto default;

			case TEMPLATE_SCOPE.GLOBAL:
				var P = this;

				while( P.Parent != null )
					P = P.Parent;

				return P.GetAssignAsObject( identString );

			case TEMPLATE_SCOPE.THIS:
				return GetAssignAsObject1Level( identString );

			default:
				return GetAssignAsObject( identString );
			}
		}


		internal void SetVariable( string identString, string value, TEMPLATE_SCOPE scope )
		{
			identString = GetVariableName( identString ); // Might start with $

			switch( scope )
			{
			case TEMPLATE_SCOPE.PARENT:
				if( Parent != null )
					Parent[ identString ] = value;
				goto default;

			case TEMPLATE_SCOPE.GLOBAL:
				var P = this;

				while( P.Parent != null )
					P = P.Parent;
				P[ identString ] = value;

				break;

			case TEMPLATE_SCOPE.THIS:
				Assign( identString, value );

				break;

			default:
				this[ identString ] = value;

				break;
			}
		}

		internal void SetVariableAsObject( string identString, object value, TEMPLATE_SCOPE scope )
		{
			identString = GetVariableName( identString ); // Might start with $

			switch( scope )
			{
			case TEMPLATE_SCOPE.PARENT:
				Parent?.Assign( identString, value );
				goto default;

			case TEMPLATE_SCOPE.GLOBAL:
				var P = this;

				while( P.Parent != null )
					P = P.Parent;
				P.Assign( identString, value );

				break;

			default:
				Assign( identString, value );

				break;
			}
		}
	}
}