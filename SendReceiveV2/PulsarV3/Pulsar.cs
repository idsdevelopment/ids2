﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Antlr4.Runtime;
using PulsarV3.Lexer;
using PulsarV3.Parser;
using PulsarV3.Parser.Visitors;

namespace PulsarV3
{
	public partial class Pulsar
	{
		internal class ErrorListener : BaseErrorListener
		{
			private readonly PulsarParser Parser;

			public override void SyntaxError( IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg,
			                                  RecognitionException e )
			{
				Parser.HasErrors = true;
				var S = Parser.Pulsar;

				S.SyntaxErrors.Add( $"{S.TemplateName}  Line: {line},  Position: {charPositionInLine},  Symbol: '{offendingSymbol.Text.Trim()}'  Error: {msg.Trim()}" );
			}

			public ErrorListener( PulsarParser parser )
			{
				Parser = parser;
			}
		}

		public Pulsar TopMostParent
		{
			get
			{
				var Result = this;

				while( Result.Parent != null )
					Result = Result.Parent;

				return Result;
			}
		}

		internal void SetParent( Pulsar parent )
		{
			Parent = parent;

			if( parent != null )
			{
				TemplateDirectories = parent.TemplateDirectories;
				ModuleDirectories = parent.ModuleDirectories;
				TemplateCache = parent.TemplateCache;
				Reader = parent.Reader;

				SyntaxErrors = parent.SyntaxErrors;
				PreFilter = parent.PreFilter;
				PostFilter = parent.PostFilter;
				Timeout = parent.Timeout;
				Modifiers = parent.Modifiers;
			}
			else
			{
				Modifiers = new Dictionary<string, Func<object, List<string>, string>>();

				TemplateDirectories = new List<string>();
				ModuleDirectories = new List<string>();

				TemplateCache = new Dictionary<string, byte[]>();

				SetDefaultModifiers();
			}
		}

		public string Fetch( Stream stream, bool buildOutput = true )
		{
			return Fetch( new AntlrInputStream( stream ), buildOutput );
		}

		public string Fetch( string text, bool isFileName = false, bool buildOutput = true )
		{
			var SaveName = _TemplateName;

			try
			{
				AntlrInputStream Stream;

				if( isFileName )
				{
					Stream = new AntlrInputStream( GetTemplate( text, out _TemplateDirectory ) );
					_TemplateName = text;
				}
				else
					Stream = new AntlrInputStream( text );

				return Fetch( Stream, buildOutput );
			}
			finally
			{
				_TemplateName = SaveName;
			}
		}

		public void ClearCache()
		{
			TemplateCache.Clear();
		}

		public Pulsar( Pulsar parent )
		{
			SetParent( parent );
		}

		internal Pulsar( Pulsar parent, NewOutputBuffer buffer )
		{
			SetParent( parent );
			Output.Clear( buffer );
		}

		public Pulsar() : this( null )
		{
		}

		private string Fetch( AntlrInputStream stream, bool buildOutput = true )
		{
			if( SyntaxErrors == null )
				SyntaxErrors = new List<string>();

			if( buildOutput )
				Output = new NewOutputBuffer();

			var Result = "";

			var T = new Thread( () =>
			                    {
				                    if( PreFilter != null )
					                    stream = new AntlrInputStream( PreFilter( stream.ToString(), this ) );

				                    var Lexer = new PulsarLexer( stream ) { Pulsar = this };

				                    var Tokens = new CommonTokenStream( Lexer );

				                    var Parser = new PulsarParser( Tokens )
				                                 {
					                                 Pulsar = this,
					                                 Lexer = Lexer
				                                 };

				                    Parser.RemoveErrorListeners();
				                    Parser.AddErrorListener( new ErrorListener( Parser ) );

				                    var Tree = Parser.parse();

				                    if( !Parser.HasErrors )
				                    {
					                    try
					                    {
						                    var Visitor = new PulsarBaseVisitor( this, Parser, Lexer, Tokens );
						                    Visitor.Visit( Tree );
					                    }
					                    catch( Exception E )
					                    {
						                    SyntaxErrors.Add( E.Message );
					                    }
				                    }

				                    if( buildOutput )
				                    {
					                    var RetVal = SyntaxErrors.Count == 0 ? Output.ToString() : "";

					                    Result = PostFilter != null ? PostFilter( RetVal, this ) : RetVal;
				                    }
				                    else
					                    Result = "";
			                    } );

			var Timer = new Timer( state =>
			                       {
				                       T.Abort();
				                       T.Join( 5 * 60 * 1000 ); // Max 5 mins to abort thread
			                       }, null, Timeout, System.Threading.Timeout.Infinite );

			try
			{
				T.Start();
				T.Join();
			}
			finally
			{
				Timer.Dispose();
			}

			return Result;
		}
	}
}