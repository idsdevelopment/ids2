﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace PulsarV3.Parser.Result
{
	internal class ExpressionResultEntry
	{
		internal class Modifier
		{
			internal List<string> Args = new List<string>();
			internal string Name;
		}

		internal bool IsTrue
		{
			get
			{
				if( IsNumber )
					return Math.Abs( NumberValue ) > 0;

				return !string.IsNullOrEmpty( StringValue ) && ( StringValue.Trim().ToLower() != "false" );
			}
		}

		internal string AsString
		{
			get => IsNumber ? NumberValue.ToString( CultureInfo.InvariantCulture ) : StringValue;

			set
			{
				StringValue = value;
				HasNumber = false;
			}
		}

		internal double AsNumber
		{
			get
			{
				if( HasNumber )
					return NumberValue;

				if( !double.TryParse( StringValue, out var RetVal ) )
					RetVal = 0;

				HasNumber = true;
				NumberValue = RetVal;

				return RetVal;
			}

			set
			{
				HasNumber = true;
				NumberValue = value;
				StringValue = value.ToString( CultureInfo.InvariantCulture );
			}
		}

		internal bool IsNumber
		{
			get
			{
				if( !HasNumber )
				{
					if( !double.TryParse( StringValue, out var Value ) )
						return false;

					HasNumber = true;
					NumberValue = Value;
				}

				return true;
			}
		}

		private bool HasNumber;
		private double NumberValue;
		private string StringValue = "";

		internal Pulsar.FunctionArgs FunctionArgs;
		internal Modifier Modifiers;

		internal object ObjectVariable;
		internal bool ObjectVariableHasProperties;
		internal string ObjectVariableName;
	}
}