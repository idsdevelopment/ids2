﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitCapture( PulsarParser.CaptureContext context )
		{
			var Tags = context.tags();

			if( ( Tags != null ) && ( Tags.Length > 0 ) )
			{
				PushOutput();

				foreach( var Tag in Tags )
					Visit( Tag );

				var Text = PopOutput();

				var Variable = context.VARIABLE().GetText().Trim();

				if( Variable.StartsWith( "$" ) )
					Variable = Variable.Substring( 1 );

				Pulsar.Assign( Variable, Text );
			}

			return null;
		}
	}
}