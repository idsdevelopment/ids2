﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitLiteralText( PulsarParser.LiteralTextContext context )
		{
			var LiteralText = context.LITERAL_TEXT();

			if( LiteralText == null )
			{
				LiteralText = context.LITERAL_BRACE();

				if( LiteralText == null )
				{
					LiteralText = context.NESTED_LITERAL();

					if( LiteralText == null )
						return null;
				}
			}

			Output = LiteralText.GetText();

			return null;
		}
	}
}