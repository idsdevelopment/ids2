﻿using System;
using System.Text;
using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitIgnoreCRLF( PulsarParser.IgnoreCRLFContext context )
		{
			PushOutput();

			foreach( var Tag in context.tags() )
				Visit( Tag );

			var T = PopOutput();
			var Text = new StringBuilder();

			var Temp = T.Replace( "\r", "" ).Split( new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );

			foreach( var Line in Temp )
				Text.Append( Line.StartsWith( "{\\s}" ) ? Line.Substring( "{\\s}".Length ).TrimStart() : Line );

			Temp = Text.ToString().Split( new[] { "{\\n}" }, StringSplitOptions.None );
			Text.Clear();

			foreach( var Line in Temp )
				Text.Append( Line.TrimEnd() ).Append( "\r\n" );

			Output = Text.ToString().TrimEnd();

			return null;
		}
	}
}