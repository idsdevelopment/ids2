﻿using PulsarV3.Parser.Result;

// ReSharper disable once CheckNamespace

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitExtends( PulsarParser.ExtendsContext context )
		{
			string Text;
			var    TemplateName = context.FILENAME().GetText();

			var Type = context.UTF16();

			if( Type != null )
				Text = Pulsar.GetIncludeFileUtf16( TemplateName, out _ );
			else
			{
				Type = context.ASCII();

				Text = Type != null
					       ? Pulsar.GetIncludeFileAscii( TemplateName, out _ )
					       : Pulsar.GetIncludeFileUtf8( TemplateName, out _ );
			}

			var P = new Pulsar( Pulsar, Pulsar.Output ) { _TemplateName = TemplateName };

			var FArgs = context.functionArgs();

			if( FArgs != null )
			{
				var Args = Visit( FArgs ).FunctionArgs;

				foreach( var Arg in Args )
					P.Assign( Arg.Key, Arg.Value );
			}

			P.Fetch( Text, buildOutput: false );

			Pulsar.Output.Clear( P.Output );

			foreach( var Block in context.overrideBlock() )
				Visit( Block );

			return null;
		}
	}
}