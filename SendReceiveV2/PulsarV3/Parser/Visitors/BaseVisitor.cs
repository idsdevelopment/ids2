﻿using Antlr4.Runtime;
using PulsarV3.Lexer;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		internal string Output
		{
			set => Pulsar.Output.Append( value );

			get => Pulsar.Output.ToString();
		}

		private readonly Pulsar Pulsar;
		private PulsarParser Parser;
		private PulsarLexer Lexer;
		private readonly CommonTokenStream Tokens;

		internal void PushOutput()
		{
			Pulsar.PushOutputBuffer();
		}

		internal string PopOutput()
		{
			return Pulsar.PopOutputBuffer();
		}

		internal PulsarBaseVisitor( Pulsar smarty, PulsarParser parser, PulsarLexer lexer, CommonTokenStream tokens )
		{
			Pulsar = smarty;

			Parser = parser;
			Lexer = lexer;
			Tokens = tokens;
		}
	}
}