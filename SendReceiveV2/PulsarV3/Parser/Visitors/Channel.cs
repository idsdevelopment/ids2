﻿using System.Text;
using Antlr4.Runtime;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		internal string GetTextChannelRight( IToken startSymbol, int channel )
		{
			var Channel = Tokens.GetHiddenTokensToRight( startSymbol.TokenIndex, channel );

			if( Channel != null )
			{
				var Text = new StringBuilder();

				foreach( var C in Channel )
					Text.Append( C.Text );

				return Text.ToString();
			}

			return "";
		}

		internal string GetTextChannelLeft( IToken startSymbol, int channel )
		{
			var Channel = Tokens.GetHiddenTokensToLeft( startSymbol.TokenIndex, channel );

			if( Channel != null )
			{
				var Text = new StringBuilder();

				foreach( var C in Channel )
					Text.Append( C.Text );

				return Text.ToString();
			}

			return "";
		}
	}
}