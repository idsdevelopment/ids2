﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitClear( PulsarParser.ClearContext context )
		{
			Pulsar.Output.Clear();

			return null;
		}
	}
}