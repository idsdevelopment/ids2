﻿using System.Collections.Generic;
using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		private readonly List<ExpressionResultEntry> SwitchStack = new List<ExpressionResultEntry>();
		private ExpressionResultEntry CurrentSwitchValue;

		public override ExpressionResultEntry VisitSwitch( PulsarParser.SwitchContext context )
		{
			CurrentSwitchValue = Visit( context.expressionVariable() );
			SwitchStack.Add( CurrentSwitchValue );

			try
			{
				foreach( var Case in context.@case() )
					Visit( Case );
			}
			finally
			{
				var Ndx = SwitchStack.Count;

				if( Ndx > 0 )
				{
					CurrentSwitchValue = SwitchStack[ --Ndx ];
					SwitchStack.RemoveAt( Ndx );
				}
			}

			return null;
		}

		public override ExpressionResultEntry VisitCase( PulsarParser.CaseContext context )
		{
			var IsTrue = context.DEFAULT_CASE() != null;

			if( !IsTrue )
			{
				var StrValue = "";

				var Value = context.NUMBER();

				if( Value != null )
					StrValue = Value.GetText();
				else
				{
					Value = context.STRING();

					if( Value != null )
					{
						StrValue = Value.GetText();
						StrValue = StrValue.Substring( 1, StrValue.Length - 2 ); // Remove Quotes
					}
				}

				if( CurrentSwitchValue != null )
				{
					var CaseValue = new ExpressionResultEntry
					                {
						                AsString = StrValue
					                };

					if( CaseValue.IsNumber && CurrentSwitchValue.IsNumber )

						// ReSharper disable once CompareOfFloatsByEqualityOperator
						IsTrue = CaseValue.AsNumber == CurrentSwitchValue.AsNumber;
					else
						IsTrue = CaseValue.AsString == CurrentSwitchValue.AsString;
				}

				if( !IsTrue )
					return null;
			}

			foreach( var T in context.tags() )
				Visit( T );

			return null;
		}
	}
}