﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitFetch( PulsarParser.FetchContext context )
		{
			string Text,
			       TemplateDirectory;
			var TemplateName = context.FILENAME().GetText();

			var Type = context.ASCII();

			if( Type != null )
				Text = Pulsar.GetIncludeFileAscii( TemplateName, out TemplateDirectory );
			else
			{
				Type = context.UTF16();

				Text = Type != null
					       ? Pulsar.GetIncludeFileUtf16( TemplateName, out TemplateDirectory )
					       : Pulsar.GetIncludeFileUtf8( TemplateName, out TemplateDirectory );
			}

			Output = Text;

			return null;
		}

		public override ExpressionResultEntry VisitInclude( PulsarParser.IncludeContext context )
		{
			string Text,
			       TemplateDirectory;
			var TemplateName = context.FILENAME().GetText();

			var Type = context.UTF8();

			if( Type != null )
				Text = Pulsar.GetIncludeFileUtf8( TemplateName, out TemplateDirectory );
			else
			{
				Type = context.UTF16();

				Text = Type != null
					       ? Pulsar.GetIncludeFileUtf16( TemplateName, out TemplateDirectory )
					       : Pulsar.GetIncludeFileAscii( TemplateName, out TemplateDirectory );
			}

			var P = new Pulsar( Pulsar, Pulsar.Output ) { _TemplateName = TemplateName };

			var FArgs = context.functionArgs();

			if( FArgs != null )
			{
				var Args = Visit( FArgs ).FunctionArgs;

				foreach( var Arg in Args )
					P.Assign( Arg.Key, Arg.Value );
			}

			P.Fetch( Text, buildOutput: false );
			Pulsar.Output.Clear( P.Output );

			return null;
		}
	}
}