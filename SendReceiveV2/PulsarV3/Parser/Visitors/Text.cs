﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitText( PulsarParser.TextContext context )
		{
			var BText = context.BRACE_TEXT();

			if( BText != null )
				Output = '{' + BText.GetText();
			else
				Output = context.TEXT().GetText();

			return null;
		}

		public override ExpressionResultEntry VisitBraceChar( PulsarParser.BraceCharContext context )
		{
			Output = "{";

			return null;
		}
	}
}