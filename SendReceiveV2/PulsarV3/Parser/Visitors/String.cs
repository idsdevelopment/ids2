﻿using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public override ExpressionResultEntry VisitString( PulsarParser.StringContext context )
		{
			var RetVal = new ExpressionResultEntry();
			var Text = context.STRING().GetText();
			Text = Text.Substring( 1, Text.Length - 2 ); // Remove Quotes

			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach( var Modifier in context.modifiers() )
			{
				var Modifiers = Visit( Modifier ).Modifiers;
				Text = ModifyValue( Modifiers.Name, Text, Modifiers.Args ).ToString();
			}

			RetVal.AsString = Text;

			return RetVal;
		}
	}
}