﻿using System;
using PulsarV3.Parser.Result;

namespace PulsarV3.Parser.Visitors
{
	internal partial class PulsarBaseVisitor
	{
		public static object GetProperty( object obj, string propertyName, out bool isObject )
		{
			isObject = false;

			if( obj != null )
			{
				if( propertyName.StartsWith( "." ) )
					propertyName = propertyName.Remove( 0, 1 );

				try
				{
					var Property = obj.GetType().GetProperty( propertyName.Trim() );
					var Value = Property?.GetValue( obj );

					if( Value != null )
					{
						var PType = Property.PropertyType;

						if( PType.IsPrimitive || PType == typeof( string ) || PType == typeof( decimal ) || PType == typeof( float ) )
						{
							if( PType == typeof( bool ) )
							{
								var Val = Value.ToString();

								return Val == "True" ? "1" : "0";
							}

							return Value.ToString();
						}

						isObject = true;

						return Value;
					}
				}
				catch
				{
				}
			}

			return null;
		}

		public static Pulsar.TEMPLATE_SCOPE GetVariableScope( PulsarParser.VariableScopeContext context )
		{
			if( context != null )
			{
				if( context.MODIFIER_SCOPE_GLOBAL() != null )
					return Pulsar.TEMPLATE_SCOPE.GLOBAL;

				if( context.MODIFIER_SCOPE_PARENT() != null )
					return Pulsar.TEMPLATE_SCOPE.PARENT;

				if( context.MODIFIER_SCOPE_THIS() != null )
					return Pulsar.TEMPLATE_SCOPE.THIS;
			}

			return Pulsar.TEMPLATE_SCOPE.TEMPLATE;
		}

		private static void AutoIncError( string variable )
		{
			throw new ArgumentException( "Pre Increment/Decrement not allowed on variable with property. '" + variable + "'" );
		}


		public override ExpressionResultEntry VisitObjectVariable( PulsarParser.ObjectVariableContext context )
		{
			var Variable = context.VARIABLE().GetText().Trim();
			object VariableObject;

			var Scope = GetVariableScope( context.variableScope() );

			var Properties = context.variableProperty();
			var HasProperties = Properties != null && Properties.Length > 0;

			if( HasProperties )
			{
				var Obj = Pulsar.GetVariableAsObject( Variable, Scope );

				foreach( var Property in Properties )
				{
					Obj = GetProperty( Obj, Property.GetText(), out var IsObject ) ?? "";

					if( !IsObject )
						break;
				}

				VariableObject = Obj;
			}
			else
				VariableObject = Pulsar.GetVariableAsObject( Variable, Scope );

			return new ExpressionResultEntry
			       {
				       ObjectVariable = VariableObject,
				       ObjectVariableName = Variable,
				       ObjectVariableHasProperties = HasProperties
			       };
		}

		public override ExpressionResultEntry VisitExpressionVariable( PulsarParser.ExpressionVariableContext context )
		{
			var RetVal = new ExpressionResultEntry();
			var Variable = context.VARIABLE().GetText();
			object VariableObject;

			var Scope = GetVariableScope( context.variableScope() );

			var Properties = context.variableProperty();
			var HasProperties = Properties != null && Properties.Length > 0;

			if( HasProperties )
			{
				var Obj = Pulsar.GetVariableAsObject( Variable, Scope );

				foreach( var Property in Properties )
				{
					Obj = GetProperty( Obj, Property.GetText(), out var IsObject ) ?? "";

					if( !IsObject )
						break;
				}

				// ReSharper disable once LoopCanBeConvertedToQuery
				foreach( var Modifier in context.modifiers() )
				{
					var Modifiers = Visit( Modifier ).Modifiers;
					Obj = ModifyValue( Modifiers.Name, Obj, Modifiers.Args );
				}

				VariableObject = Obj;
			}
			else
				VariableObject = Pulsar.GetVariableAsObject( Variable, Scope );

			if( VariableObject != null )
			{
				var Type = VariableObject.GetType();
				var IsStruct = Type.IsValueType && !Type.IsEnum;

				if( !Type.IsPrimitive && ( Type.IsClass || IsStruct ) && !( VariableObject is string ) )
				{
					RetVal.ObjectVariable = VariableObject;

					return RetVal;
				}
			}

			RetVal.AsString = VariableObject != null ? VariableObject.ToString() : "";

			var PreAuto = context.expressionPreAuto();

			if( PreAuto != null )
			{
				if( HasProperties )
					AutoIncError( Variable );

				if( PreAuto.PRE_INCREMENT_VARIABLE() != null )
					RetVal.AsNumber = RetVal.AsNumber + 1;
				else
					RetVal.AsNumber = RetVal.AsNumber - 1;

				Pulsar.SetVariable( Variable, RetVal.AsString, Scope );
			}

			// Post
			var PostAuto = context.expressionPostAuto();

			if( PostAuto != null )
			{
				if( HasProperties )
					AutoIncError( Variable );

				var Temp = new ExpressionResultEntry { AsString = RetVal.AsString };

				if( PostAuto.POST_INCREMENT_VARIABLE() != null )
					Temp.AsNumber = Temp.AsNumber + 1;
				else
					Temp.AsNumber = Temp.AsNumber - 1;

				Pulsar.SetVariable( Variable, Temp.AsString, Scope );
			}

			var Text = RetVal.AsString;

			if( !HasProperties )
			{
				foreach( var Modifier in context.modifiers() )
				{
					var Modifiers = Visit( Modifier ).Modifiers;
					Text = ModifyValue( Modifiers.Name, VariableObject, Modifiers.Args ).ToString();
				}
			}

			RetVal.AsString = Text;

			return RetVal;
		}

		public override ExpressionResultEntry VisitVariable( PulsarParser.VariableContext context )
		{
			Output = Visit( context.expressionVariable() ).AsString;

			return null;
		}
	}
}