﻿using System;
using System.Collections.Generic;

namespace PulsarV3
{
	public partial class Pulsar
	{
		public class FunctionArgs : Dictionary<string, string>
		{
		}

		private readonly Dictionary<string, Func<Pulsar, string, FunctionArgs, string>> BlockFunctions =
			new Dictionary<string, Func<Pulsar, string, FunctionArgs, string>>();

		private readonly List<string> BlockFunctionStack = new List<string>();

		private readonly Dictionary<string, Func<Pulsar, FunctionArgs, string>> Functions =
			new Dictionary<string, Func<Pulsar, FunctionArgs, string>>();

		internal void PushBlockFunctionName( string name )
		{
			BlockFunctionStack.Add( name.Trim() );
		}

		internal string BlockFunctionTopOfStack()
		{
			var Ndx = BlockFunctionStack.Count - 1;

			return Ndx >= 0 ? BlockFunctionStack[ Ndx ] : "";
		}

		internal string PopBlockFunctionName()
		{
			var Ndx = BlockFunctionStack.Count - 1;

			if( Ndx >= 0 )
			{
				var RetVal = BlockFunctionStack[ Ndx ];
				BlockFunctionStack.RemoveAt( Ndx );

				return RetVal;
			}

			return "";
		}

		public Pulsar RegisterFunction( string functionName, Func<Pulsar, FunctionArgs, string> callback )
		{
			functionName = functionName.Trim();

			if( callback == null )
			{
				if( Functions.ContainsKey( functionName ) )
					Functions.Remove( functionName );
			}
			else if( Functions.ContainsKey( functionName ) )
				Functions[ functionName ] = callback;
			else
				Functions.Add( functionName, callback );

			return this;
		}

		internal string ExecuteFunction( string functionName, FunctionArgs args )
		{
			functionName = functionName.Trim();

			var P = this;

			do
			{
				var F = P.Functions;

				if( F.ContainsKey( functionName ) )
					return F[ functionName ]( this, args );

				P = P.Parent;
			} while( P != null );

			return "";
		}

		public Pulsar RegisterBlockFunction( string functionName, Func<Pulsar, string, FunctionArgs, string> callback )
		{
			functionName = functionName.Trim();

			if( callback == null )
			{
				if( BlockFunctions.ContainsKey( functionName ) )
					BlockFunctions.Remove( functionName );
			}
			else if( BlockFunctions.ContainsKey( functionName ) )
				BlockFunctions[ functionName ] = callback;
			else
				BlockFunctions.Add( functionName, callback );

			return this;
		}

		internal bool IsBlockFunction( string functionName )
		{
			functionName = functionName.Trim();

			var P = this;

			do
			{
				var F = P.BlockFunctions;

				if( F.ContainsKey( functionName ) )
					return true;

				P = P.Parent;
			} while( P != null );

			return false;
		}

		internal string ExecuteBlockFunction( string functionName, string context, FunctionArgs args )
		{
			functionName = functionName.Trim();

			var P = this;

			do
			{
				var F = P.BlockFunctions;

				if( F.ContainsKey( functionName ) )
					return F[ functionName ]( this, context, args );

				P = P.Parent;
			} while( P != null );

			return "";
		}
	}
}