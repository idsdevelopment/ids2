﻿using System.Collections.Generic;

namespace PulsarV3
{
	public partial class Pulsar
	{
		private readonly Dictionary<string, object> Variables = new Dictionary<string, object>();

		public Pulsar Assign( string variableName, bool value )
		{
			return Assign( variableName, value ? "1" : "0" );
		}


		public Pulsar Assign( string variableName, object value )
		{
			variableName = GetVariableName( variableName );

			if( Variables.ContainsKey( variableName ) )
			{
				if( value != null )
					Variables[ variableName ] = value;
				else
					Variables.Remove( variableName );
			}
			else if( value != null )
				Variables.Add( variableName, value );

			return this;
		}

		public object GetAssignAsObject1Level( string variableName )
		{
			variableName = GetVariableName( variableName );
			var V = Variables;

			return V.ContainsKey( variableName ) ? V[ variableName ] : null;
		}

		public object GetAssignAsObject( string variableName )
		{
			variableName = GetVariableName( variableName );

			var P = this;

			do
			{
				var V = P.Variables;

				if( V.ContainsKey( variableName ) )
					return V[ variableName ];

				P = P.Parent;
			} while( P != null );

			return null;
		}


		public string GetAssign( string variableName )
		{
			var Temp = GetAssignAsObject( variableName );

			return Temp?.ToString() ?? "";
		}
	}
}