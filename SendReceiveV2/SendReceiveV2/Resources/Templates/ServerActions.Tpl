﻿
{ignoreCRLF}
{\s} {if $HAS_NO_AUTHORISATION_PROTOCOLS}
				switch( Action ){\n}
				{ {\n}
{\s}	{foreach $PROTOCOL in $NO_AUTHORISATION_PROTOCOLS}
				case "{$PROTOCOL.Name}":{\n}
{\s}	{/foreach}
					break;{\n}
				default:{\n}
					if( !Implementation.ValidateAuthorisation( Uri.UnescapeDataString( context.Request.QueryString[ "Auth" ] ) ) ){\n}
						throw new AbortConnectionException( "Invalid Auth Token" );{\n}
					break;{\n}
				}{\n}{\n}
{\s} {/if}	
				switch( Action ){\n}
				{ {\n}
{\s} {foreach $PROTOCOL in $PROTOCOLS}
{\s}	{assign $PNAME = $PROTOCOL.Name}
{\s}	{assign $IS_LISTENER = $PROTOCOL.IsListener}
{\s}	{capture $RESPONSE_TYPE}{if $IS_LISTENER}List<{$PROTOCOL.ResponseType}>{else}{$PROTOCOL.ResponseType}{/if}{/capture}
{\s}	{capture $RESPONSE_NAME}{if $IS_LISTENER}ResponseListen{else}Response{/if}{$PNAME}{/capture}
{\s}	{if $IS_LISTENER}
				case "Listen{$PNAME}{$PROTOCOL.ResponseType}":{\n}
					IsListener = true;{\n}
{\s}	{else}
				case "{$PNAME}":{\n}
{\s}	{/if}
{\s}	{if $PROTOCOL.IsResponseVoid}		{*		Response void run under task	*}
{\s}		{if  $PROTOCOL.IsCommand}
					Dict = ToObject<Dictionary<string,string>>( context );{\n}
					Task.Run( () =>{\n}
						{ {\n}
							try{\n}
							{ {\n}
								Implementation.{$RESPONSE_NAME}( {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}Dict.GetValue( "{$ARG}" ){assign $FIRST = 0}{/foreach} );{\n}
							}{\n}
							catch( Exception E ){\n}
							{ {\n}
								Implementation.OnException?.Invoke( E );{\n}
							}{\n}
						} );{\n}
						OReply = "";{\n}
						break;{\n}
{\s}		{else}							{*  IsCommand and response void  and no args   *}
{\s}			{if $PROTOCOL.IsRequestVoid}		
					Task.Run( () =>{\n}
						{ {\n}
							try{\n}
							{ {\n}
								Implementation.{$RESPONSE_NAME}();{\n}
							} {\n}
							catch( Exception E ){\n}
							{ {\n}
								Implementation.OnException?.Invoke( E );{\n}
							} {\n}
						} );{\n}
						OReply = "";{\n}
						break;{\n}
{\s}			{else}
					OReply = RunVoid<{$PROTOCOL.RequestType}>( Implementation.{$RESPONSE_NAME} );{\n}
					break;{\n}
{\s}			{/if}
{\s}		{/if}							{* End  IsCommand  *}
{\s}	{else}								{* Has return type *}
{\s}		{if $PROTOCOL.IsRequestVoid}
{\s}			{if $PROTOCOL.IsAuthorisation}
{\s}				{if $PROTOCOL.IsCommand}
					Dict = ToObject<Dictionary<string,string>>( context );{\n}
                    OReply = Implementation.{$RESPONSE_NAME}(  {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}Dict.GetValue( "{$ARG}" ){assign $FIRST = 0}{/foreach} );{\n}
					break;{\n}
{\s}				{else}
                    OReply = Implementation.{$RESPONSE_NAME}();{\n}
					break;{\n}
{\s}				{/if}
{\s}			{else}
{\s}			{if $PROTOCOL.IsCommand}
					Dict = ToObject<Dictionary<string,string>>( context );{\n}
					OReply = Implementation.{$RESPONSE_NAME}( {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}Dict.GetValue( "{$ARG}" ){assign $FIRST = 0}{/foreach} );{\n}
					break;{\n}
{\s}			{else}
					OReply = Implementation.{$RESPONSE_NAME}();{\n}
					break;{\n}
{\s}			{/if}
{\s}		{/if}
{\s}		{else}
{\s}			{if $PROTOCOL.IsCommand}
					Dict = ToObject<Dictionary<string,string>>( context );{\n}
					OReply = Implementation.{$RESPONSE_NAME}( {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}Dict.GetValue( "{$ARG}" ){assign $FIRST = 0}{/foreach} );{\n}
{\s}			{else}
					OReply = Implementation.{$RESPONSE_NAME}( ToObject<{$PROTOCOL.RequestType}>( context ) );{\n}
					break;{\n}
{\s}			{/if}
{\s}		{/if}
{\s}	{/if}
{\s}	{if $IS_LISTENER}
{\s}		{if $PROTOCOL.IsCommand}
{capture $OBJECT_TYPE}Send{$PROTOCOL.Name}{$PROTOCOL.ResponseType}Container{/capture}
{\s}		{else}
{capture $OBJECT_TYPE}List<{$PROTOCOL.ResponseType}>{/capture}
{\s}		{/if}
{\s}		{if $PROTOCOL.IsCommand}
				case "Send{$PNAME}{$PROTOCOL.ResponseType}":{\n}
					{{{\n}
						var Data = ToObject<{$OBJECT_TYPE}>( context );{\n}
						var D = Data.A;{\n}
						OReply = Implementation.Response{$PNAME}Receive{$PROTOCOL.ResponseType}(  {assign $FIRST = 1}{foreach $ARG in $PROTOCOL.Args}{if !$FIRST}, {/if}D.GetValue( "{$ARG}" ){assign $FIRST = 0}{/foreach}, Data.D );{\n}
					}{\n}
{\s}			{else}
				case "Send{$PNAME}{$PROTOCOL.ResponseType}":{\n}
					OReply = Implementation.Response{$PNAME}Receive{$PROTOCOL.ResponseType}( ToObject<{$OBJECT_TYPE}>( context ) );{\n}
{\s}		{/if}
					break;{\n}
				case "Listen{$PNAME}{$PROTOCOL.ResponseType}Ack":{\n}
					Implementation.ResponseListen{$PROTOCOL.Name}Ack( ToObject<List<long>>( context ) );{\n}
					OReply = "";{\n}
					break;{\n}{\n}
{\s}	{/if}
{\s} {/foreach}
{/ignoreCRLF}
				default:
					throw new AbortConnectionException( $"Unknown request : {Action}" );
				}
